import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
    drdoQuestionairreList: {},
    drdoQuestionairreDetail:{},
    drdoParameterDetail:{},
    drdoQuestionairreChangesDetail:[]
};


const getDrdoQuestionnaireList = (state, action) => {
    return updateObject(state, {
      drdoQuestionairreList: action.payload,
    });
  };
  const getDrdoQuestionairreDetail = (state, action) => {
    return updateObject(state, {
      drdoQuestionairreDetail: action.payload,
    });
  };
  const getDrdoQuestionairreParameterDetail=(state , action)=>{
    return updateObject(state, {
      drdoParameterDetail: action.payload,
  });
  }
  const getDrdoQuestionairreChangesDetail=(state , action)=>{
    return updateObject(state, {
      drdoQuestionairreChangesDetail: action.payload,
  });
  }


const drdoQuestionairreReducer = (state = initialState, action = {}) => {
    if (action.type === types.GET_DRDO_QUESTIONAIRRE_LIST) {
      return getDrdoQuestionnaireList(state, action);
    }
    if(action.type==types.GET_DRDO_QUESTIONAIRRE_DETAIL){
        return getDrdoQuestionairreDetail(state , action)
    }
    if(action.type==types.GET_DRDO_QUESTIONAIRRE_PARAMETER_DETAIL){
      return getDrdoQuestionairreParameterDetail(state , action)
    }
    if(action.type==types.GET_DRDO_QUESTIONAIRRE_CHANGES_DETAIL){
      return getDrdoQuestionairreChangesDetail(state , action)
    }
     else {
      return state;
    }
  };
  
  export default drdoQuestionairreReducer;