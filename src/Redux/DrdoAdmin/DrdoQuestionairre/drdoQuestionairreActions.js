import * as types from "./types";
import call from "../../../Api";
import { showToast } from "../../../Components/Toast/Toast";
import axios from "axios";
import { getItem } from "../../../Services/localStorageService";
import { showLoader } from "../../Loader/loaderActions";

export const getDrdoQuestionairreList = (value) => {
  return {
    type: types.GET_DRDO_QUESTIONAIRRE_LIST,
    payload: value,
  };
};

export const getDrdoQuestionairreDetail = (value) => {
  return {
    type: types.GET_DRDO_QUESTIONAIRRE_DETAIL,
    payload: value,
  };
};
export const getDrdoQuestionairreParameterDetail = (value) => {
  return {
    type: types.GET_DRDO_QUESTIONAIRRE_PARAMETER_DETAIL,
    payload: value,
  };
};

export const getDrdoQuestionairreChangesDetail = (value) => {
  return {
    type: types.GET_DRDO_QUESTIONAIRRE_CHANGES_DETAIL,
    payload: value,
  };
};

export const getDrdoQuestionairreListAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "drdo/questionnaires",
      dispatch,
      query: query,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getDrdoQuestionairreList(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
        showToast(err);
      });
  };
};

export const getDrdoQuestionairreDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `drdo/questionnaire/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getDrdoQuestionairreDetail(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
        showToast(err);
      });
  };
};

//   export const cloneDrdoQuestionairreAction = (navigate , id)=>{
//     return (dispatch) => {
//       call({
//         method: "get",
//         endpoint: `drdo/questionnaire/${id}/clone`,
//         dispatch
//       })
//         .then((res) => {
//           if (res.status === 200) {
//             let id=res?.data?.id
//             showToast("Cloned Successfully !", "success");
//             navigate(`questionairre-view/${id}`)
//           }
//         })
//         .catch((err) => {
//           showToast(err.response, "error");
//         });
//     };
//   }

// // action-for questionairre upload (will complete once receive api)__________________________________________________-
//   export const uploadDrdoQuestionairreAction = (payload, dispatch,closeModal) => {
//     dispatch(showLoader(true));
//     return () => {
//       axios
//         .post(
//           `https://qci-api.cloudzmall.com/api/v1/drdo/questionnaire/import`,
//           payload,
//           {
//             headers: {
//               "Content-type": "multipart/form-data",
//               Accept: "multipart/form-data",
//               Authorization: `Bearer ${getItem("token")}`,
//             },
//           }
//         )
//         .then((res) => {
//           dispatch(showLoader(false));
//           if (res.status === 200) {
//             // navigate(`/step3-details/${id}`);
//             showToast("Questionairre Uploaded Successfully!", "success");
//             closeModal(false)
//             dispatch(getDrdoQuestionairreListAction({start:1}))
//           }
//         })
//         .catch((err) => {
//           // showToast("Something went wrong", "error");
//           showToast(err?.response?.body?.message, "error");
//           dispatch(showLoader(false));
//           // if (Object.keys(err?.response?.data?.errors)?.length > 0) {
//           //   const currentError = Object.keys(err?.response?.data?.errors);
//           //   let tempError = "";
//           //   if (currentError?.length > 0) tempError = currentError[0];

//           //   if (
//           //     err?.response?.data?.errors[tempError] &&
//           //     err?.response?.data?.errors[tempError][0]
//           //   ) {
//           //     showToast(err?.response?.data?.errors[tempError][0], "error");
//           //   }
//           // }
//         });
//     };
//   };

export const getDrdoQuestionairreParameterAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `drdo/questionnaire/parameter/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getDrdoQuestionairreParameterDetail(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast(err.response, "error");
      });
  };
};

export const getDrdoQuestionairreChangesAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `drdo/questionnaire/${id}/auditlogs`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getDrdoQuestionairreChangesDetail(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast(err.response, "error");
      });
  };
};

export const drdoApproveQuestionairreAction = (id, payload) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `drdo/questionnaire/${id}/approval`,
      dispatch,
      payload,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Success!", "success");
          dispatch(getDrdoQuestionairreDetailAction(id));
          dispatch(getDrdoQuestionairreChangesAction(id));
        }
      })
      .catch((err) => {
        showToast(err.response, "error");
      });
  };
};
