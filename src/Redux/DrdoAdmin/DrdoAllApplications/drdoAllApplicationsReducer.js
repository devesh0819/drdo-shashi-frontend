import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
  allApplications: [],
  applicationDetail: {},
  approveApplication: null,
  chartsData: null,
};

const getAllApplication = (state, action) => {
  return updateObject(state, {
    allApplications: action.payload,
  });
};

const getApplicationDetail = (state, action) => {
  return updateObject(state, {
    applicationDetail: action.payload,
  });
};

const approveApplication = (state, action) => {
  return updateObject(state, {
    approveApplication: action.payload,
  });
};
const getDrdoChartsData = (state, action) => {
  return updateObject(state, {
    chartsData: action.payload,
  });
};

const drdoAllApplicationsReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_DRDO_ALL_APPLICATIONS) {
    return getAllApplication(state, action);
  } else if (action.type === types.GET_DRDO_ALL_APPLICATIONS_DETAIL) {
    return getApplicationDetail(state, action);
  } else if (action.type === types.APPROVE_APPLICATION) {
    return approveApplication(state, action);
  } else if (action.type === types.GET_DRDO_CHARTS_DATA) {
    return getDrdoChartsData(state, action);
  } else {
    return state;
  }
};

export default drdoAllApplicationsReducer;
