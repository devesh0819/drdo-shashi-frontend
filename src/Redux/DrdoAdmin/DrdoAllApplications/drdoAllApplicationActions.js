import call from "../../../Api/index";
import { showToast } from "../../../Components/Toast/Toast";
import {
  APPROVE_APPLICATION,
  GET_DRDO_ALL_APPLICATIONS,
  GET_DRDO_ALL_APPLICATIONS_DETAIL,
  GET_DRDO_CHARTS_DATA,
} from "./types";

export const getAllApplications = (value) => {
  return {
    type: GET_DRDO_ALL_APPLICATIONS,
    payload: value,
  };
};

export const getApplicationDetail = (value) => {
  return {
    type: GET_DRDO_ALL_APPLICATIONS_DETAIL,
    payload: value,
  };
};

export const approveApplication = (value) => {
  return {
    type: APPROVE_APPLICATION,
    payload: value,
  };
};
export const getDrdoChartsData = (value) => {
  return {
    type: GET_DRDO_CHARTS_DATA,
    payload: value,
  };
};

export const getDrdoChartsDataAction = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `drdo/getStats`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getDrdoChartsData(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const getAllApplicationsAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `drdo/application`,
      dispatch,
      query,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAllApplications(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const getApplicationDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `drdo/application/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getApplicationDetail(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const approveApplicationAction = (id, payload) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `drdo/application/${id}/drdo-approval`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(approveApplication(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};
