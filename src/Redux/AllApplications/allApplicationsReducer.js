import { updateObject } from "../utility";
import * as types from "./types";

const initialState = {
  allApplications: null,
  applicationDetail: {},
  image: null,
};

const getAllApplication = (state, action) => {
  return updateObject(state, {
    allApplications: action.payload,
  });
};
const getApplicationDetail = (state, action) => {
  return updateObject(state, {
    applicationDetail: action.payload,
  });
};
const getDrdoApplicationDetail = (state, action) => {
  return updateObject(state, {
    drdoApplicationDetail: action.payload,
  });
};
const getImage = (state, action) => {
  return updateObject(state, {
    image: action.payload,
  });
};
const allApplicationsReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_ALL_APPLICATIONS) {
    return getAllApplication(state, action);
  }
  if (action.type === types.GET_APPLICATION_VIEW) {
    return getApplicationDetail(state, action);
  }
  if (action.type === types.GET_DRDO_APPLICATION_VIEW) {
    return getDrdoApplicationDetail(state, action);
  }
  if (action.type === types.GET_IMAGE) {
    return getImage(state, action);
  } else {
    return state;
  }
};

export default allApplicationsReducer;
