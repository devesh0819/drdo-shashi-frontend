import call from "../../Api/index";
import { showToast } from "../../Components/Toast/Toast";
import { showLoader } from "../Loader/loaderActions";
import {
  GET_ALL_APPLICATIONS,
  GET_APPLICATION_VIEW,
  GET_IMAGE,
  GET_DRDO_APPLICATION_VIEW,
} from "./types";
import axios from "axios";
import { getItem } from "../../Services/localStorageService";
import { DRDO_ALL_APPLICATIONS } from "../../Routes/Routes";
import { APP_STAGE } from "../../Constant/RoleConstant";

export const getAllApplications = (value) => {
  return {
    type: GET_ALL_APPLICATIONS,
    payload: value,
  };
};
export const getApplicationDetail = (value) => {
  return {
    type: GET_APPLICATION_VIEW,
    payload: value,
  };
};
export const getDrdoApplicationDetail = (value) => {
  return {
    type: GET_DRDO_APPLICATION_VIEW,
    payload: value,
  };
};
export const getImage = (value) => {
  return {
    type: GET_IMAGE,
    payload: value,
  };
};
export const downloadReportAction = (
  id,
  application_number,
  dispatch,
  type
) => {
  dispatch(showLoader(true));
  return () => {
    axios
      .get(`${process.env.REACT_APP_BASE_URL}/application/${id}/${type}`, {
        headers: {
          "Content-type": "applicatin/json",
          Authorization: `Bearer ${getItem("token")}`,
        },
        responseType: "blob",
      })
      .then((res) => {
        if (res.status === 200) {
          dispatch(showLoader(false));
          const url = window.URL.createObjectURL(new Blob([res.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", `${application_number}.pdf`);
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      })
      .catch((err) => {
        dispatch(showLoader(false));
        showToast("Something went wrong", "error");
      });
  };
};
export const downloadAction = (
  application_number,
  dispatch,
  url,
  file_type,
  file_extension
) => {
  dispatch(showLoader(true));
  return () => {
    axios
      .get(`${url}`, {
        headers: {
          "Content-type": "applicatin/json",
          Authorization: `Bearer ${getItem("token")}`,
        },
        responseType: "blob",
      })
      .then((res) => {
        if (res.status === 200) {
          dispatch(showLoader(false));
          const url = window.URL.createObjectURL(new Blob([res.data]));
          const link = document.createElement("a");
          link.href = url;
          if (file_type) {
            link.setAttribute(
              "download",
              `${application_number}.${file_extension}`,
              `${file_type}`
            );
          } else {
            link.setAttribute(
              "download",
              `${application_number}.${file_extension}`
            );
          }
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      })
      .catch((err) => {
        dispatch(showLoader(false));
        showToast("Something went wrong", "error");
      });
  };
};
export const getAllApplicationsAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `applicant/applications`,
      query: query,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAllApplications(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err.response, "error");
      });
  };
};

export const setDrdoApplicationViewFormAction = (payload, navigate, id) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `drdo/application/${id}/drdo-approval`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Submitted Successfully !", "success");
          navigate(DRDO_ALL_APPLICATIONS);
        }
      })
      .catch((err) => {
        showToast(err.response, "error");
      });
  };
};

export const getApplicationDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `applicant/application/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getApplicationDetail(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};
export const getDrdoApplicationDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `drdo/application/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getDrdoApplicationDetail(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const getImageAction = (path, dispatch, setPreviewPdf) => {
  dispatch(showLoader(true));
  return () => {
    axios
      .get(path, {
        headers: {
          "Content-type": "image/jpeg",
          Authorization: `Bearer ${getItem("token")}`,
        },
        responseType: "blob",
      })
      .then((res) => {
        if (res.status === 200) {
          dispatch(showLoader(false));
          const imageObjectURL = URL.createObjectURL(res.data);
          dispatch(getImage(imageObjectURL));
          if (setPreviewPdf) {
            setPreviewPdf(imageObjectURL);
          }
        }
      })
      .catch((err) => {
        dispatch(showLoader(false));
        showToast("Something went wrong", "error");
      });
  };
};
