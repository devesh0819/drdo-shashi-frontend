import call from "../../Api";
import { showToast } from "../../Components/Toast/Toast";
import { CHANGE_PASSWORD } from "../../Routes/Routes";
import { encrypt, logout } from "../../Services/localStorageService";
import { logoutAction } from "../Login/loginAction";
import * as types from "./types";

export const getUserProfile = (payload) => {
  return {
    type: types.GET_USER_PROFILE,
    payload: payload,
  };
};

export const editProfileAction = (payload, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "applicant/update-profile",
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Profile updated successfully", "success");
          localStorage.setItem(
            "user",
            encrypt(
              JSON.stringify(res.body.data),
              process.env.REACT_APP_PASSWORD_SECRET_KEY
            )
          );
          dispatch(getUserProfile(res.body?.data));
        }
      })
      .catch((err) => {
        if (
          err?.response?.status === 401 &&
          err?.response?.statusText === "Unauthorized"
        ) {
          logout();
        }
        if (Object.keys(err?.response?.body?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.body?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];

          if (
            err?.response?.body?.errors[tempError] &&
            err?.response?.body?.errors[tempError][0]
          ) {
            showToast(err?.response?.body?.errors[tempError][0], "error");
          }
        }
      });
  };
};
