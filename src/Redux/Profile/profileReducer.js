import { updateObject } from "../utility";
import * as types from "./types";

const initialState = {
  profileData: null,
  userProfile: null,
};

const profileData = (state, action) => {
  return updateObject(state, {
    profileData: action.payload,
  });
};

const getUserProfile = (state, action) => {
  return updateObject(state, {
    userProfile: action.payload,
  });
};

const profileReducer = (state = initialState, action = {}) => {
  if (action.type === types.EDIT_PROFILE) {
    return profileData(state, action);
  }
  if (action.type === types.GET_USER_PROFILE) {
    return getUserProfile(state, action);
  } else {
    return state;
  }
};

export default profileReducer;
