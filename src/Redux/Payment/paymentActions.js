import call from "../../Api";
import { showToast } from "../../Components/Toast/Toast";
import { ALL_APPLICATIONS } from "../../Routes/Routes";
import { showLoader } from "../Loader/loaderActions";
import * as types from "./types";

export const getPaymentHistoryList = (value) => {
  return {
    type: types.GET_PAYMENT_HISTORY_LIST,
    payload: value,
  };
};

export const getPaymentEncRequest = (value) => {
  return {
    type: types.GET_PAYMENT_ENC_REQUEST,
    payload: value,
  };
};

export const getPaymentBreakupDetail = (value) => {
  return {
    type: types.PAYMENT_BREAKUP_DETAIL,
    payload: value,
  };
};

export const getPaymentBreakupDetailAction = (
  applicationId,
  payload,
  navigate
) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `admin/application/${applicationId}/approvePayment`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        dispatch(getPaymentBreakupDetail(res.body?.data));
        showToast("Payment approved successfully", "success");
        navigate(ALL_APPLICATIONS);
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};

export const getPaymentHistoryListAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "applicant/payment-history",
      query: query,
      dispatch,
    })
      .then((res) => {
        dispatch(getPaymentHistoryList(res.body?.data));
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};

export const getPaymentEncRequestAction = (id, orderNumber, query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `applicant/application/${id}/payment-initiate`,
      dispatch,
      query: query,
    })
      .then((res) => {
        if (res.status === 200) {
          // dispatch(getPaymentEncRequest(res.body.data.encRequest));
          const req = res?.body?.data?.encRequest;
          if (process.env.REACT_APP_TEST_FLAG === "TRUE") {
            window.open(
              `${process.env.REACT_APP_BASE_URL}/applicant/payment/success?orderNo=${orderNumber}`,
              "_self"
            );
          } else {
            window.open(
              `${process.env.REACT_APP_PAYMENT_URL}/payment/create?encRequest=${req}`,
              "_self"
            );
          }
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};

export const makePaymentAction = (orderNumber, navigate) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `applicant/payment/success`,
      query: { orderNo: orderNumber },
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Payment completed successfully", "success");
          // navigate(`payment-successful/${orderNumber}`);
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};
