import { updateObject } from "../utility";
import * as types from "./types";

const initialState = {
  paymentHistoryList: null,
  paymentEncRequest: null,
  paymentBreakupDetail: null,
};

const getPaymentHistoryList = (state, action) => {
  return updateObject(state, {
    paymentHistoryList: action.payload,
  });
};
const getPaymentEncRequest = (state, action) => {
  return updateObject(state, {
    paymentEncRequest: action.payload,
  });
};

const getPaymentBreakupDetail = (state, action) => {
  return updateObject(state, {
    paymentBreakupDetail: action.payload,
  });
};

const paymentReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_PAYMENT_HISTORY_LIST) {
    return getPaymentHistoryList(state, action);
  }
  if (action.type === types.GET_PAYMENT_ENC_REQUEST) {
    return getPaymentEncRequest(state, action);
  }
  if (action.type === types.PAYMENT_BREAKUP_DETAIL) {
    return getPaymentBreakupDetail(state, action);
  } else {
    return state;
  }
};

export default paymentReducer;
