import call from "../../Api";
import { showLoader } from "../Loader/loaderActions";
import * as types from "./types";
import { showToast } from "../../Components/Toast/Toast";

export const getCaptcha = (value) => {
  return {
    type: types.GET_CAPTCHA,
    payload: value,
  };
};

export const getCaptchaAction = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "captcha",
      dispatch,
    })
      .then((res) => {
        dispatch(getCaptcha(res.body));
      })
      .catch((err) => {
        showToast("Captcha not available", "error");
      });
  };
};
