import { updateObject } from "../utility";
import * as types from "./types";

const initialState = {
  captchaData: {},
};

const getCaptcha = (state, action) => {
  return updateObject(state, {
    captchaData: action.payload,
  });
};

const getCaptchaReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_CAPTCHA) {
    return getCaptcha(state, action);
  } else {
    return state;
  }
};

export default getCaptchaReducer;
