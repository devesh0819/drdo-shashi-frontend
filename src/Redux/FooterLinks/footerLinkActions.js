import { GET_STANDARD_QUESTIONAIRRE, SET_CERTIFIED_ENTERPRISES } from "./types";
import call from "../../Api/index";
import { showToast } from "../../Components/Toast/Toast";
// import { showLoader } from "../Loader/loaderActions";

export const getStandardQuestionairre = (value) => {
  return {
    type: GET_STANDARD_QUESTIONAIRRE,
    payload: value,
  };
};

export const setCertifiedEnterprises = (value) => {
  return {
    type: SET_CERTIFIED_ENTERPRISES,
    payload: value,
  };
};

export const getStandardQuestionairreAction = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `qci_master_values?type=standards`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getStandardQuestionairre(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const getCertifiedEnterprises = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `qci_master_values?type=certified_vendors`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(setCertifiedEnterprises(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err?.message, "error");
      });
  };
};
