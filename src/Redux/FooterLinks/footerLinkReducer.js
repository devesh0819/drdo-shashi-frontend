import { updateObject } from "../utility";
import * as types from "./types";

const initialState = {
  standardQuestionairre: null,
  certifiedEnterprises: null,
};

const getStandardQuestionairre = (state, action) => {
  return updateObject(state, {
    standardQuestionairre: action.payload,
  });
};
const setCertifiedEnterprises = (state, action) => {
  return updateObject(state, {
    certifiedEnterprises: action.payload,
  });
};

const footerLinkReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_STANDARD_QUESTIONAIRRE) {
    return getStandardQuestionairre(state, action);
  }
  if (action.type === types.SET_CERTIFIED_ENTERPRISES) {
    return setCertifiedEnterprises(state, action);
  } else {
    return state;
  }
};

export default footerLinkReducer;
