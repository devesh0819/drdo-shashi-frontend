import { updateObject } from "../utility";
import * as types from "./types";

const initialState = {
  generalInfoData: {},
  processDetailsData: {},
  ownershipDetailsData: {},
  step2FormData: {},
  step3FormData: {},
  submitApplicationStatus: null,
  previewApplication: null,
};

const getGeneralInfo = (state, action) => {
  return updateObject(state, {
    generalInfoData: action.payload,
  });
};

const getProcessInfo = (state, action) => {
  return updateObject(state, {
    processDetailsData: action.payload,
  });
};

const getOwnershipInfo = (state, action) => {
  return updateObject(state, {
    ownershipDetailsData: action.payload,
  });
};

const getStep2FormInfo = (state, action) => {
  return updateObject(state, {
    step2FormData: action.payload,
  });
};

const getStep3FormInfo = (state, action) => {
  return updateObject(state, {
    step3FormData: action.payload,
  });
};

const submitApplication = (state, action) => {
  return updateObject(state, {
    submitApplicationStatus: action.payload,
  });
};

const previewApplication = (state, action) => {
  return updateObject(state, {
    previewApplication: action.payload,
  });
};

const applicantFormReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_GENERAL_INFO_DATA) {
    return getGeneralInfo(state, action);
  }
  if (action.type === types.GET_PROCESS_DETAILS_DATA) {
    return getProcessInfo(state, action);
  }
  if (action.type === types.GET_OWNERSHIP_DETAILS_DATA) {
    return getOwnershipInfo(state, action);
  }
  if (action.type === types.GET_STEP2_DETAILS_DATA) {
    return getStep2FormInfo(state, action);
  }
  if (action.type === types.GET_STEP3_DETAILS_DATA) {
    return getStep3FormInfo(state, action);
  }
  if (action.type === types.PREVIEW_APPLICATION) {
    return previewApplication(state, action);
  }
  if (action.type === types.SUBMIT_APPLICATION) {
    return submitApplication(state, action);
  } else {
    return state;
  }
};

export default applicantFormReducer;
