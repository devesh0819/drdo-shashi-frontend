import * as types from "./types";
import call from "../../Api/index";
import { showToast } from "../../Components/Toast/Toast";
import {
  HOME,
  PAYMENT,
  PREVIEW,
  STEP2_DETAILS,
  STEP3_DETAILS,
} from "../../Routes/Routes";
import axios from "axios";
import { getItem, logout } from "../../Services/localStorageService";
import { showLoader } from "../Loader/loaderActions";
import { APP_STAGE } from "../../Constant/RoleConstant";
import { getApplicationDetail } from "../AllApplications/allApplicationsActions";
import { logoutAction } from "../Login/loginAction";

export const saveGeneralInfo = (value) => {
  return {
    type: types.GET_GENERAL_INFO_DATA,
    payload: value,
  };
};

export const saveProcessDetails = (value) => {
  return {
    type: types.GET_PROCESS_DETAILS_DATA,
    payload: value,
  };
};

export const saveOwnershipDetails = (value) => {
  return {
    type: types.GET_OWNERSHIP_DETAILS_DATA,
    payload: value,
  };
};

export const saveStep2FormDetails = (value) => {
  return {
    type: types.GET_STEP2_DETAILS_DATA,
    payload: value,
  };
};
export const saveStep3FormDetails = (value) => {
  return {
    type: types.GET_STEP3_DETAILS_DATA,
    payload: value,
  };
};
export const submitApplication = (value) => {
  return {
    type: types.SUBMIT_APPLICATION,
    payload: value,
  };
};

export const previewApplication = (value) => {
  return {
    type: types.PREVIEW_APPLICATION,
    payload: value,
  };
};

export const scheduleAppointment = (value) => {
  return {
    type: types.SCHEDULE_APPOINTMENT,
    payload: value,
  };
};

export const saveGeneralInfoAction = (payload, navigate, id, dispatch) => {
  dispatch(showLoader(true));
  return () => {
    axios
      .post(
        id
          ? `${process.env.REACT_APP_BASE_URL}/applicant/application/${id}`
          : `${process.env.REACT_APP_BASE_URL}/applicant/application`,
        payload,
        {
          headers: {
            "Content-type": "multipart/form-data",
            Accept: "application/json",
            Authorization: `Bearer ${getItem("token")}`,
          },
        }
      )
      .then((res) => {
        dispatch(showLoader(false));
        if (res.status === 200) {
          navigate(`/step2-details/${res?.data?.data?.application_uuid}`);
        }
      })
      .catch((err) => {
        dispatch(showLoader(false));
        if (
          err?.response?.status === 401 &&
          err?.response?.statusText === "Unauthorized"
        ) {
          logout();
        }
        if (Object.keys(err?.response?.data?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.data?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];

          if (
            err?.response?.data?.errors[tempError] &&
            err?.response?.data?.errors[tempError][0]
          ) {
            showToast(err?.response?.data?.errors[tempError][0], "error");
          }
        }
      });
  };
};

export const saveStep2FormAction = (id, payload, navigate, dispatch) => {
  dispatch(showLoader(true));
  return () => {
    axios
      .post(
        `${process.env.REACT_APP_BASE_URL}/applicant/application/${id}/financial`,
        payload,
        {
          headers: {
            "Content-type": "multipart/form-data",
            Accept: "application/json",
            Authorization: `Bearer ${getItem("token")}`,
          },
        }
      )
      .then((res) => {
        dispatch(showLoader(false));
        if (res.status === 200) {
          // navigate(`/step3-details/${id}`);
          // dispatch(saveStep2FormDetails(res.data.data.financial));
          navigate(`/preview/${id}`);
          dispatch(getApplicationDetail({}));
        }
      })
      .catch((err) => {
        dispatch(showLoader(false));
        if (
          err?.response?.status === 401 &&
          err?.response?.statusText === "Unauthorized"
        ) {
          logout();
        }
        if (Object.keys(err?.response?.data?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.data?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];

          if (
            err?.response?.data?.errors[tempError] &&
            err?.response?.data?.errors[tempError][0]
          ) {
            showToast(err?.response?.data?.errors[tempError][0], "error");
          }
        }
      });
  };
};

export const saveStep3FormAction = (id, payload, navigate, dispatch) => {
  dispatch(showLoader(true));
  return () => {
    axios
      .post(
        `${process.env.REACT_APP_BASE_URL}/applicant/application/${id}/ownership`,
        payload,
        {
          headers: {
            "Content-type": "multipart/form-data",
            Accept: "application/json",
            Authorization: `Bearer ${getItem("token")}`,
          },
        }
      )
      .then((res) => {
        dispatch(showLoader(false));
        if (res.status === 200) {
          // navigate(`/home/${id}`);
          navigate(`/preview/${id}`);
          dispatch(getApplicationDetail({}));
          // dispatch(saveStep3FormDetails(res?.data?.data?.ownership));
          // dispatch(previewApplication(res?.data?.data));
        }
      })
      .catch((err) => {
        dispatch(showLoader(false));
        if (
          err?.response?.status === 401 &&
          err?.response?.statusText === "Unauthorized"
        ) {
          logout();
        }
        if (Object.keys(err?.response?.data?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.data?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];

          if (
            err?.response?.data?.errors[tempError] &&
            err?.response?.data?.errors[tempError][0]
          ) {
            showToast(err?.response?.data?.errors[tempError][0], "error");
          }
        }
      });
  };
};

export const submitApplicationAction = (id, navigate) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `applicant/application/${id}/submit-application`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Application submitted successfully", "success");
          dispatch(submitApplication(res.body.data));
          navigate(`/payment/${res.body.data?.application_uuid}`);
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const scheduleApoointmentAction = (id, payload, navigate, dispatch) => {
  dispatch(showLoader(true));
  return () => {
    axios
      .post(
        `${process.env.REACT_APP_BASE_URL}/applicant/application/${id}/schedule-assessment`,
        payload,
        {
          headers: {
            "Content-type": "multipart/form-data",
            Accept: "application/json",
            Authorization: `Bearer ${getItem("token")}`,
          },
        }
      )
      .then((res) => {
        dispatch(showLoader(false));
        if (res.status === 200) {
          showToast("Assessment schdeuled successfully", "success");
          navigate(HOME);
        }
      })
      .catch((err) => {
        dispatch(showLoader(false));
        showToast(err, "error");
      });
  };
};

export const deleteImageAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `file/${id}/delete`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Image deleted successfully", "success");
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};
