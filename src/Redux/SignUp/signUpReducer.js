import { updateObject } from "../utility";
import * as types from "./types";

const initialState = {
  signUpData: {},
  otpValue: null,
  userData: null,
};

const signUpData = (state, action) => {
  return updateObject(state, {
    signUpData: action.payload,
  });
};

const getOtpValue = (state, action) => {
  return updateObject(state, {
    otpValue: action.payload,
  });
};

const setUserData = (state, action) => {
  return updateObject(state, {
    userData: action.payload,
  });
};

const signUpReducer = (state = initialState, action = {}) => {
  if (action.type === types.SIGNUP) {
    return signUpData(state, action);
  }
  if (action.type === types.OTP_VALUE) {
    return getOtpValue(state, action);
  }
  if (action.type === types.SET_USER_DATA) {
    return setUserData(state, action);
  } else {
    return state;
  }
};

export default signUpReducer;
