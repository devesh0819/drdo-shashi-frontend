import call from "../../Api";
import { ENTER_OTP, OTP_VERIFIED } from "../../Routes/Routes";
import * as types from "./types";
import { showToast } from "../../Components/Toast/Toast";
import { encrypt } from "../../Services/localStorageService";
import { PASSWORD_ENCRYPTION_SECRET } from "../../Constant/AppConstant";

export const signUpData = (value) => {
  return {
    type: types.SIGNUP,
    payload: value,
  };
};

export const getOtpValue = (value) => {
  return {
    type: types.OTP_VALUE,
    payload: value,
  };
};

export const setUserData = (value) => {
  return {
    type: types.SET_USER_DATA,
    payload: value,
  };
};

export const signUpAction = (reqBody, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "applicant/signup",
      payload: reqBody,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          localStorage.setItem("token", res.body.data.token);
          if (res.body.data.user) {
            const userDetail = JSON.stringify(res.body.data.user);
            localStorage.setItem(
              "user",
              encrypt(userDetail, process.env.REACT_APP_PASSWORD_SECRET_KEY)
            );
            dispatch(setUserData(res.body.data.user));
          }
          navigate(ENTER_OTP);
        }
      })
      .catch((err) => {
        // showToast(err?.response?.body?.errors?.email[0], "error");
        if (Object.keys(err?.response?.body?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.body?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];
          if (
            err?.response?.body?.errors[tempError] &&
            err?.response?.body?.errors[tempError][0]
          ) {
            showToast(err?.response?.body?.errors[tempError][0], "error");
          }
        }
      });
  };
};

export const verifyEmailOtpAction = (reqBody, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "verify-email",
      payload: reqBody,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          if (res.body.data.error) {
            showToast(res.body.data.error, "error");
          } else {
            dispatch(getOtpValue(reqBody));
            showToast(res.body.data.message, "success");
            navigate(OTP_VERIFIED);
          }
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.data?.error, "error");
      });
  };
};
