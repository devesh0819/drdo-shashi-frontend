import { updateObject } from "../utility";
import * as types from "./types";

const initialState = {
  allAssessments: {},
  assessmentDetail: {},
  assessmentParameter:[],
  parameterDetail:{},
  currentTabAssessor:"1",
};

const getAllApplication = (state, action) => {
  return updateObject(state, {
    allAssessments: action.payload,
  });
};

const getApplicationDetail = (state, action) => {
  return updateObject(state, {
    assessmentDetail: action.payload,
  });
};

const getAssessmentParameter = (state, action) => {
    return updateObject(state, {
        assessmentParameter: action.payload,
    });
  };

const getAssessorParameterDetail=(state , action)=>{
  return updateObject(state, {
    parameterDetail: action.payload,
});
}

const getCurrentTabAssessor=(state , action)=>{
  return updateObject(state,{
    currentTabAssessor:action.payload,
  })
}



const assessorAllApplicationsReducers = (state = initialState, action = {}) => {
  if (action.type === types.GET_ASSESSOR_ALL_APPLICATIONS) {
    return getAllApplication(state, action);
  } else if (action.type === types.GET_ASSESSOR_APPLICATION_DETAIL) {
    return getApplicationDetail(state, action);
    }
    else if (action.type === types.GET_ASSESSOR_APPLICATION_PARAMETER) {
        return getAssessmentParameter(state, action);
  } else if(action.type === types.GET_ASSESSOR_APPLICATION_PARAMETER_DETAIL){
    return getAssessorParameterDetail(state ,action);
  }
  else if (action.type === types.SET_ASSESSOR_TAB){
    return getCurrentTabAssessor(state ,action)
  }
  else {
    return state;
  }
};

export default assessorAllApplicationsReducers;