import call from "../../Api/index";
import { showToast } from "../../Components/Toast/Toast.js";
import { APP_STAGE } from "../../Constant/RoleConstant";
import {
  GET_ASSESSOR_ALL_APPLICATIONS,
  GET_ASSESSOR_APPLICATION_DETAIL,
  GET_ASSESSOR_APPLICATION_PARAMETER,
  GET_ASSESSOR_APPLICATION_PARAMETER_DETAIL,
  SET_ASSESSOR_TAB,
} from "./types";

export const getAllApplications = (value) => {
  return {
    type: GET_ASSESSOR_ALL_APPLICATIONS,
    payload: value,
  };
};

export const getApplicationDetail = (value) => {
  return {
    type: GET_ASSESSOR_APPLICATION_DETAIL,
    payload: value,
  };
};

export const getApplicationParameter = (value) => {
  return {
    type: GET_ASSESSOR_APPLICATION_PARAMETER,
    payload: value,
  };
};

export const getAssessorParameterDetail = (value) => {
  return {
    type: GET_ASSESSOR_APPLICATION_PARAMETER_DETAIL,
    payload: value,
  };
};
export const setAssessorTab = (tab) => {
  return {
    type: SET_ASSESSOR_TAB,
    payload: tab,
  };
};

//___________________________________________________________________________________
//___________________________________________________________________________________

export const getAllApplicationsAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `assessor/assessments`,
      dispatch,
      query,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAllApplications(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const getApplicationDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `assessor/assessment/${id}/show`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getApplicationDetail(res.body.data));
          if (
            res.body.data?.progress === APP_STAGE.ASSESSORS_ASSIGNED ||
            res.body.data?.progress === APP_STAGE.PAREMETER_ALLOCATED ||
            res.body.data?.progress === APP_STAGE.ASSESSMENT_SUBMITTED
          ) {
            dispatch(getApplicationParameterAction(id));
          }
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const getApplicationParameterAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `assessor/assessment/${id}/parameters`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getApplicationParameter(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const assignAssessorToParameterAction = (
  payload,
  parameter_id,
  assessment_id,
  isMultiple = false,
  setCheckedParamIds
) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: !isMultiple
        ? `assessor/parameter/${parameter_id}/assign-parameters-assessor`
        : `assessor/parameter/bulk/assign-parameters-assessor`,
      dispatch,
      payload,
    })
      .then((res) => {
        if (res.status === 200) {
          // dispatch(getApplicationParameter(res.body.data));
          showToast("Assessor Assigned Successfully!", "success");
          dispatch(getApplicationDetailAction(assessment_id));
          dispatch(getApplicationParameterAction(assessment_id));
          isMultiple && setCheckedParamIds([]);
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const getAssessorParameterDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `assessor/parameter/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAssessorParameterDetail(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const addCommentToParameterAction = (
  payload,
  parameter_id,
  assessment_id
) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `assessor/parameter/${parameter_id}/store-parameters-comment`,
      dispatch,
      payload,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Comment Added Successfully!", "success");
          dispatch(getAssessorParameterDetailAction(parameter_id));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const startAssessmentLeadAction = (assessment_id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `assessor/assessment/${assessment_id}/start`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Assessment Synced!", "success");
          dispatch(getApplicationDetailAction(assessment_id));
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const submitAssessmentLeadAction = (assessment_id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `assessor/assessment/${assessment_id}/submit`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Assessment Submitted Successfully!", "success");
          dispatch(getApplicationDetailAction(assessment_id));
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const setAssessorTabAction = (tab) => {
  return (dispatch) => {
    dispatch(setAssessorTab(tab));
  };
};
