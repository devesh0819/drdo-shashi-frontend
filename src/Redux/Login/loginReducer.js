import { updateObject } from "../utility";
import * as types from "./types";

const initialState = {
  loginData: {},
  forgotPasswordData: null,
};

const loginData = (state, action) => {
  return updateObject(state, {
    loginData: action.payload,
  });
};

const forgotPasswordData = (state, action) => {
  return updateObject(state, {
    forgotPasswordData: action.payload,
  });
};

const loginReducer = (state = initialState, action = {}) => {
  if (action.type === types.LOGIN) {
    return loginData(state, action);
  }
  if (action.type === types.FORGOT_PASSWORD) {
    return forgotPasswordData(state, action);
  } else {
    return state;
  }
};

export default loginReducer;
