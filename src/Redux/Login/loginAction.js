import { useSearchParams } from "react-router-dom";
import call from "../../Api";
import { showToast } from "../../Components/Toast/Toast";
import { PASSWORD_ENCRYPTION_SECRET } from "../../Constant/AppConstant";
import { ROLES } from "../../Constant/RoleConstant";
import {
  CHANGE_PASSWORD,
  DASHBOARD,
  FEEDBACK_SUBMITTED,
  HOME,
  LOGIN,
  PASSWORD_CHANGED,
  RESEND_LINK,
} from "../../Routes/Routes";
import { getDefaultPath } from "../../Services/commonService";
import { encrypt } from "../../Services/localStorageService";
import { getCaptchaAction } from "../GetCaptcha/getCaptchaActions";
import * as types from "./types";

export const loginData = (value) => {
  return {
    type: types.LOGIN,
    payload: value,
  };
};
export const forgotPasswordData = (value) => {
  return {
    type: types.FORGOT_PASSWORD,
    payload: value,
  };
};

export const loginAction = (reqBody, navigate, url) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "login",
      payload: reqBody,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Logged in successfully", "success");
          localStorage.setItem("token", res.body.data.token);
          if (localStorage.getItem("token")) {
            const userDetail = JSON.stringify(res.body.data.user);
            localStorage.setItem(
              "user",
              encrypt(userDetail, process.env.REACT_APP_PASSWORD_SECRET_KEY)
            );

            if (url) {
              window.location.href = url;
            } else navigate(getDefaultPath(JSON.parse(userDetail)?.roles[0]));
          }
        }
      })
      .catch((err) => {
        if (Object.keys(err?.response?.body?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.body?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];
          if (
            err?.response?.body?.errors[tempError] &&
            err?.response?.body?.errors[tempError][0]
          ) {
            showToast(err?.response?.body?.errors[tempError][0], "error");
          }
        }
        dispatch(getCaptchaAction());
      });
  };
};

export const logoutAction = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "logout",
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          localStorage.clear();
          window.location.href = LOGIN;
          showToast("Logged out successfully", "success");
        }
      })
      .catch((err) => {
        localStorage.clear();
        window.location.href = LOGIN;
        showToast("Logged out successfully", "success");
      });
  };
};

export const forgotPasswordAction = (reqBody, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "forgot-password",
      payload: reqBody,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(forgotPasswordData(reqBody));
          showToast("Link sent successfully", "success");
          navigate(`/resend-link/${btoa(reqBody?.email)}`);
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.data?.error, "error");
      });
  };
};

export const requestLinkAction = (setTime) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "resend-verify-email",
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast(res.body.data.message, "success");
          if (setTime) setTime(15);
        }
      })
      .catch((err) => {
        showToast(
          "Some error occured. Please try again after some time.",
          "error"
        );
      });
  };
};

export const changePasswordAction = (reqBody, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "change-password",
      payload: reqBody,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          // showToast(res.body.data.message, "success");
          showToast("Password changed successfully", "success");
          if (navigate) {
            navigate(CHANGE_PASSWORD);
          }
        }
      })
      .catch((err) => {
        showToast(
          err?.response?.body?.data?.message ||
            err?.response?.body?.errors?.captcha[0],
          "error"
        );
        dispatch(getCaptchaAction());
      });
  };
};

export const resetPasswordAction = (reqBody, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "reset-password",
      payload: reqBody,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          // showToast(res.body.data.message, "success");
          showToast(res.body?.data?.message, "success");
          navigate(PASSWORD_CHANGED);
        }
      })
      .catch((err) => {
        showToast("This password reset token is invalid", "error");
        // showToast(err?.response?.body?.data?.message, "error");
      });
  };
};

export const submitFeedbackAction = (
  token,
  reqBody,
  navigate,
  application_id
) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `applicant/application/${application_id}/feedback`,
      payload: reqBody,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Feedback submitted successfully", "success");
          navigate(FEEDBACK_SUBMITTED);
        }
      })
      .catch((err) => {
        showToast("Feedback already submitted", "error");
      });
  };
};
