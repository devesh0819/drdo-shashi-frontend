import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
  feedbackConfigurationData: null,
  applicationFeedbackConfig: null,
};

const getFeedbackConfigurationData = (state, action) => {
  return updateObject(state, {
    feedbackConfigurationData: action.payload,
  });
};

const getApplicationFeedbackConfigData = (state, action) => {
  return updateObject(state, {
    applicationFeedbackConfig: action.payload,
  });
};

const feedbackConfigurationReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_FEEDBACK_CONFIGURATION_DATA) {
    return getFeedbackConfigurationData(state, action);
  }
  if (action.type === types.GET_APPLICATION_FEEDBACK_CONFIG_DATA) {
    return getApplicationFeedbackConfigData(state, action);
  } else {
    return state;
  }
};

export default feedbackConfigurationReducer;
