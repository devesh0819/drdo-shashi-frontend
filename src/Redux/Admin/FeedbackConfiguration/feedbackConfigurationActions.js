import call from "../../../Api/index";
import { showToast } from "../../../Components/Toast/Toast";
import { logout } from "../../../Services/localStorageService";
import { logoutAction } from "../../Login/loginAction";
import {
  GET_APPLICATION_FEEDBACK_CONFIG_DATA,
  GET_FEEDBACK_CONFIGURATION_DATA,
} from "./types";

export const getFeedbackConfigurationData = (value) => {
  return {
    type: GET_FEEDBACK_CONFIGURATION_DATA,
    payload: value,
  };
};

export const getApplicationFeedbackConfigData = (value) => {
  return {
    type: GET_APPLICATION_FEEDBACK_CONFIG_DATA,
    payload: value,
  };
};

export const getApplicationFeedbackConfigAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `applicant/application/${id}/feedback`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getApplicationFeedbackConfigData(res.body?.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const getFeedbackConfigurationAction = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/feedback/get-config`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getFeedbackConfigurationData(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const addFeedbackConfigurationAction = (payload) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `admin/feedback/save-config`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getFeedbackConfigurationAction());
          showToast("Question added successfully", "success");
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const deleteFeedbackConfigurationAction = (id) => {
  return (dispatch) => {
    call({
      method: "delete",
      endpoint: `admin/feedback/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getFeedbackConfigurationAction());
          showToast("Question deleted successfully", "success");
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};
