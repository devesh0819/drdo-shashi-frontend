import call from "../../../Api/index";
import { showToast } from "../../../Components/Toast/Toast";
import { ALL_APPLICATIONS, ASSIGN_AGENCY } from "../../../Routes/Routes";
import { SET_CHARTS_DATA } from "./types";

export const setChartsData = (value) => {
  return {
    type: SET_CHARTS_DATA,
    payload: value,
  };
};

export const getChartsData = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/getStats`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(setChartsData(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};
