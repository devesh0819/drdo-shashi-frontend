import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
  chartsData: {},
};

const setChartsData = (state, action) => {
  return updateObject(state, {
    chartsData: action.payload,
  });
};

const dashboardReducer = (state = initialState, action = {}) => {
  if (action.type === types.SET_CHARTS_DATA) {
    return setChartsData(state, action);
  } else {
    return state;
  }
};

export default dashboardReducer;
