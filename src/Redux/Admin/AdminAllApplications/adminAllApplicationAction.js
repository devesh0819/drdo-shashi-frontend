import call from "../../../Api/index";
import { showToast } from "../../../Components/Toast/Toast";
import { APP_STAGE } from "../../../Constant/RoleConstant";
import { ALL_APPLICATIONS, ASSIGN_AGENCY } from "../../../Routes/Routes";
import {
  GET_ADMIN_APPLICATION_LIST,
  GET_ADMIN_APPLICATION_VIEW,
  GET_ASSESSMENT_APPLICATION_VIEW,
  GET_AGENCY_APPLICATION_VIEW,
  GET_ADMIN_ASSESSMENT_PARAMETER,
  GET_ADMIN_ASSESSMENT_PARAMETER_DETAIL,
  SET_ADMIN_TAB,
} from "./types";

export const getAdminAllApplications = (value) => {
  return {
    type: GET_ADMIN_APPLICATION_LIST,
    payload: value,
  };
};

export const getAdminApplicationDetail = (value) => {
  return {
    type: GET_ADMIN_APPLICATION_VIEW,
    payload: value,
  };
};

export const getAssessmentApplicationDetail = (value) => {
  return {
    type: GET_ASSESSMENT_APPLICATION_VIEW,
    payload: value,
  };
};

export const getAgencyApplicationDetail = (value) => {
  return {
    type: GET_AGENCY_APPLICATION_VIEW,
    payload: value,
  };
};

export const assignAgency = (value) => {
  return {
    type: ASSIGN_AGENCY,
    payload: value,
  };
};
export const getAdminAssessmentParameter = (value) => {
  return {
    type: GET_ADMIN_ASSESSMENT_PARAMETER,
    payload: value,
  };
};
export const getAdminParameterDetail = (value) => {
  return {
    type: GET_ADMIN_ASSESSMENT_PARAMETER_DETAIL,
    payload: value,
  };
};

export const setAdminTab = (tab) => {
  return {
    type: SET_ADMIN_TAB,
    payload: tab,
  };
};

export const getAdminAllApplicationsAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/applications`,
      dispatch,
      query,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAdminAllApplications(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const getAdminApplicationDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/application/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAdminApplicationDetail(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const getAssessmentApplicationDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/assessment/${id}/show`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAssessmentApplicationDetail(res.body.data));
          if (
            res?.body?.data?.progress === APP_STAGE.ASSESSMENT_SUBMITTED ||
            res?.body?.data?.progress === APP_STAGE.PAREMETER_ALLOCATED
          ) {
            dispatch(getAdminAssessmentParameterAction(id));
          }
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const getAgencyApplicationDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `agency/assessment/${id}/show`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAgencyApplicationDetail(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const assignAgencyAction = (id, payload, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `admin/application/${id}/assessment`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Assigned agency successfully", "success");
          dispatch(assignAgency(res.body.data));
          navigate(ALL_APPLICATIONS);
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.errors?.application_id[0], "error");
      });
  };
};

export const getAdminAssessmentParameterAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/assessment/${id}/parameters`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAdminAssessmentParameter(res.body.data));
        }
      })
      .catch((err) => {
        // showToast(err, "error");
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const getAdminParameterDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/parameter/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAdminParameterDetail(res.body.data));
        }
      })
      .catch((err) => {
        // showToast(err, "error");
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const addCommentToAdminParameterAction = (
  payload,
  parameter_id,
  assessment_id
) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `admin/parameter/${parameter_id}/store-parameters-comment`,
      dispatch,
      payload,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Comment Added Successfully!", "success");
          dispatch(getAdminParameterDetailAction(parameter_id));
        }
      })
      .catch((err) => {
        // showToast(err, "error");
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const changeOptionAdminParameterAction = (
  payload,
  parameter_id,
  assessment_id
) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `admin/parameter/${parameter_id}/submit-parameters-response`,
      dispatch,
      payload,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Responses Updated Successfully!", "success");
          dispatch(getAdminParameterDetailAction(parameter_id));
        }
      })
      .catch((err) => {
        // showToast(err, "error");
        showToast(err?.response?.body?.message, "error");
      });
  };
};

//--** complete-assessment by admin**--//

export const completeAdminAssessmentAction = (assessment_id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/assessment/${assessment_id}/complete`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Assessment Completed Successfully!", "success");
          dispatch(getAssessmentApplicationDetailAction(assessment_id));
          dispatch(setAdminTabAction("1"));
          // dispatch(getAdminAssessmentParameterAction(assessment_id))
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const setAdminTabAction = (tab) => {
  return (dispatch) => {
    dispatch(setAdminTab(tab));
  };
};
