export const GET_ADMIN_APPLICATION_LIST = "GET_ADMIN_APPLICATION_LIST";
export const GET_ADMIN_APPLICATION_VIEW = "GET_ADMIN_APPLICATION_VIEW";
export const ASSIGN_AGENCY = "ASSIGN_AGENCY";
export const GET_ASSESSMENT_APPLICATION_VIEW =
  "GET_ASSESSMENT_APPLICATION_VIEW";
export const GET_AGENCY_APPLICATION_VIEW = "GET_AGENCY_APPLICATION_VIEW";
export const GET_ADMIN_ASSESSMENT_PARAMETER="GET_ADMIN_ASSESSMENT_PARAMETER";
export const GET_ADMIN_ASSESSMENT_PARAMETER_DETAIL="GET_ADMIN_ASSESSMENT_PARAMETER_DETAIL";
export const SET_ADMIN_TAB="SET_ADMIN_TAB";
