import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
  allAdminApplications: [],
  adminApplicationDetail: null,
  assignAgencyData: null,
  assessmentApplicationDetail: null,
  agencyApplicationDetail: null,
  adminAssessmentParameter:[],
  adminParameterDetail:{},
  currentTabAdmin:"1",
};

const getAdminAllApplication = (state, action) => {
  return updateObject(state, {
    allAdminApplications: action.payload,
  });
};

const getAdminApplicationDetail = (state, action) => {
  return updateObject(state, {
    adminApplicationDetail: action.payload,
  });
};

const getAssessmentApplicationDetail = (state, action) => {
  return updateObject(state, {
    assessmentApplicationDetail: action.payload,
  });
};

const getAgencyApplicationDetail = (state, action) => {
  return updateObject(state, {
    agencyApplicationDetail: action.payload,
  });
};

const assignAgency = (state, action) => {
  return updateObject(state, {
    assignAgencyData: action.payload,
  });
};

const getAdminAssessmentParameter = (state, action) => {
  return updateObject(state, {
    adminAssessmentParameter: action.payload,
  });
};
const getAdminParameterDetail=(state , action)=>{
  return updateObject(state, {
    adminParameterDetail: action.payload,
});
}

const getCurrentTabAdmin=(state , action)=>{
  return updateObject(state,{
    currentTabAdmin:action.payload,
  })
}

const adminAllApplicationsReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_ADMIN_APPLICATION_LIST) {
    return getAdminAllApplication(state, action);
  } else if (action.type === types.GET_ADMIN_APPLICATION_VIEW) {
    return getAdminApplicationDetail(state, action);
  } else if (action.type === types.GET_ASSESSMENT_APPLICATION_VIEW) {
    return getAssessmentApplicationDetail(state, action);
  } else if (action.type === types.GET_AGENCY_APPLICATION_VIEW) {
    return getAgencyApplicationDetail(state, action);
  } else if (action.type === types.ASSIGN_AGENCY) {
    return assignAgency(state, action);
  } else if (action.type === types.GET_ADMIN_ASSESSMENT_PARAMETER){
    return getAdminAssessmentParameter(state , action)
  }
  else if(action.type === types.GET_ADMIN_ASSESSMENT_PARAMETER_DETAIL){
    return getAdminParameterDetail(state ,action);
  }
  else if (action.type === types.SET_ADMIN_TAB){
    return getCurrentTabAdmin(state ,action)
  }
  else {
    return state;
  }
};

export default adminAllApplicationsReducer;
