import * as types from "./types";
import call from "../../../Api";
import { showToast } from "../../../Components/Toast/Toast";
import axios from "axios";
import { getItem } from "../../../Services/localStorageService";
import { showLoader } from "../../Loader/loaderActions";

export const getQuestionairreList = (value) => {
  return {
    type: types.GET_QUESTIONAIRRE_LIST,
    payload: value,
  };
};

export const getQuestionairreDetail = (value) => {
  return {
    type: types.GET_QUESTIONAIRRE_DETAIL,
    payload: value,
  };
};
export const getQuestionairreParameterDetail = (value) => {
  return {
    type: types.GET_QUESTIONAIRRE_PARAMETER_DETAIL,
    payload: value,
  };
};

export const getQuestionairreChangesDetail = (value) => {
  return {
    type: types.GET_QUESTIONAIRRE_CHANGES_DETAIL,
    payload: value,
  };
};

export const getQuestionairreListAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "admin/questionnaires",
      dispatch,
      query: query,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getQuestionairreList(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
        showToast(err);
      });
  };
};

export const getQuestionairreDetailAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/questionnaire/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getQuestionairreDetail(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
        showToast(err);
      });
  };
};

export const cloneQuestionairreAction = (navigate, id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/questionnaire/${id}/clone`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          let id = res?.data?.id;
          showToast("Cloned Successfully !", "success");
          navigate(`questionairre-view/${id}`);
        }
      })
      .catch((err) => {
        showToast(err.response, "error");
      });
  };
};

// action-for questionairre upload (will complete once receive api)__________________________________________________-
export const uploadQuestionairreAction = (
  payload,
  dispatch,
  closeModal,
  setPageNumber
) => {
  dispatch(showLoader(true));
  return () => {
    axios
      .post(
        `${process.env.REACT_APP_BASE_URL}/admin/questionnaire/import`,
        payload,
        {
          headers: {
            "Content-type": "multipart/form-data",
            Accept: "application/json",
            Authorization: `Bearer ${getItem("token")}`,
          },
        }
      )
      .then((res) => {
        dispatch(showLoader(false));
        if (res.status === 200) {
          // navigate(`/step3-details/${id}`);
          dispatch(getQuestionairreListAction({ start: 1 }));
          showToast("Questionairre Uploaded Successfully!", "success");
          setPageNumber(1);
          closeModal(false);
        }
      })
      .catch((err) => {
        showToast(
          "Something went wrong ,Kindly upload proper excel file",
          "error"
        );
        // showToast(err?.response?.body?.message, "error");
        dispatch(showLoader(false));
        // if (Object.keys(err?.response?.data?.errors)?.length > 0) {
        //   const currentError = Object.keys(err?.response?.data?.errors);
        //   let tempError = "";
        //   if (currentError?.length > 0) tempError = currentError[0];

        //   if (
        //     err?.response?.data?.errors[tempError] &&
        //     err?.response?.data?.errors[tempError][0]
        //   ) {
        //     showToast(err?.response?.data?.errors[tempError][0], "error");
        //   }
        // }
      });
  };
};

// action gor getting dta from parameter api - will properly implment once api received___________________________________

export const getQuestionairreParameterAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/questionnaire/parameter/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getQuestionairreParameterDetail(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast(err.response, "error");
      });
  };
};

export const getQuestionairreChangesAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/questionnaire/${id}/auditlogs`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getQuestionairreChangesDetail(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast(err.response, "error");
      });
  };
};

export const sendForApprovalAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/questionnaire/${id}/sendApproval`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Sent for Approval!", "success");
          dispatch(getQuestionairreDetailAction(id));
          dispatch(getQuestionairreChangesAction(id));
        }
      })
      .catch((err) => {
        showToast(err.response, "error");
      });
  };
};
