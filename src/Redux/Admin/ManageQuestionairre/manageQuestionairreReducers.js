import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
    questionairreList: {},
    questionairreDetail:{},
    parameterDetail:{},
    questionairreChangesDetail:[]
};


const getQuestionnaireList = (state, action) => {
    return updateObject(state, {
      questionairreList: action.payload,
    });
  };
  const getQuestionairreDetail = (state, action) => {
    return updateObject(state, {
        questionairreDetail: action.payload,
    });
  };
  const getQuestionairreParameterDetail=(state , action)=>{
    return updateObject(state, {
      parameterDetail: action.payload,
  });
  }
  const getQuestionairreChangesDetail=(state , action)=>{
    return updateObject(state, {
      questionairreChangesDetail: action.payload,
  });
  }


const manageQuestionairreReducer = (state = initialState, action = {}) => {
    if (action.type === types.GET_QUESTIONAIRRE_LIST) {
      return getQuestionnaireList(state, action);
    }
    if(action.type==types.GET_QUESTIONAIRRE_DETAIL){
        return getQuestionairreDetail(state , action)
    }
    if(action.type==types.GET_QUESTIONAIRRE_PARAMETER_DETAIL){
      return getQuestionairreParameterDetail(state , action)
    }
    if(action.type==types.GET_QUESTIONAIRRE_CHANGES_DETAIL){
      return getQuestionairreChangesDetail(state , action)
    }
     else {
      return state;
    }
  };
  
  export default manageQuestionairreReducer;