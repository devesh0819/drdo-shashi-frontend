import * as types from "./types";

import { updateObject } from "../../utility";

const initialState = {
  masterData: null,
  statesData: [],
  districtData: [],
  organisationType: [],
  unitDistrictData: null,
  countriesData: null,
};

const getMasterData = (state, action) => {
  return updateObject(state, {
    masterData: action.payload,
  });
};

const getStates = (state, action) => {
  return updateObject(state, {
    statesData: action.payload,
  });
};

const getCountries = (state, action) => {
  return updateObject(state, {
    countriesData: action.payload,
  });
};

const getDistrict = (state, action) => {
  return updateObject(state, {
    districtData: action.payload,
  });
};
const getUnitDistrict = (state, action) => {
  return updateObject(state, {
    unitDistrictData: action.payload,
  });
};
const getOrganisationType = (state, action) => {
  return updateObject(state, {
    organisationType: action.payload,
  });
};

const getMasterDataReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_MASTER_DATA) {
    return getMasterData(state, action);
  }
  if (action.type === types.GET_STATES) {
    return getStates(state, action);
  }
  if (action.type === types.GET_COUNTRIES) {
    return getCountries(state, action);
  }
  if (action.type === types.GET_DISTRICT) {
    return getDistrict(state, action);
  }
  if (action.type === types.GET_UNIT_DISTRICT) {
    return getUnitDistrict(state, action);
  }
  if (action.type === types.GET_ORGANISATION_TYPE) {
    return getOrganisationType(state, action);
  } else {
    return state;
  }
};

export default getMasterDataReducer;
