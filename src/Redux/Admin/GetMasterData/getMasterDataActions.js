import call from "../../../Api";
import { showToast } from "../../../Components/Toast/Toast";
import * as types from "./types";

export const getMasterData = (data) => {
  return {
    type: types.GET_MASTER_DATA,
    payload: data,
  };
};

export const getStates = (data) => {
  return {
    type: types.GET_STATES,
    payload: data,
  };
};

export const getCountries = (data) => {
  return {
    type: types.GET_COUNTRIES,
    payload: data,
  };
};

export const getDistrict = (data) => {
  return {
    type: types.GET_DISTRICT,
    payload: data,
  };
};

export const getUnitDistrict = (data) => {
  return {
    type: types.GET_UNIT_DISTRICT,
    payload: data,
  };
};

export const getOrganisationType = (data) => {
  return {
    type: types.GET_ORGANISATION_TYPE,
    payload: data,
  };
};

export const getMasterDataAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `qci_master_values`,
      query: query,
      dispatch,
    })
      .then((res) => {
        dispatch(getMasterData(res.body));
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const getStatesAction = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `qci_master_values?type=state`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getStates(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const getCountriesAction = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `qci_master_values?type=country`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getCountries(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const getDistrictAction = (stateId) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `qci_master_values?stateId=${stateId}&type=district`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getDistrict(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const getUnitDistrictAction = (stateId) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `qci_master_values?stateId=${stateId}&type=district`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getUnitDistrict(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const getOrganisationTypeAction = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `qci_master_values?type=organisation_type`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getOrganisationType(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};
