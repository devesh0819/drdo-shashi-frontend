import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
  feeConfigurationData: [],
};

const setFeeConfigurationData = (state, action) => {
  return updateObject(state, {
    feeConfigurationData: action.payload,
  });
};

const feeConfigurationReducer = (state = initialState, action = {}) => {
  if (action.type === types.SET_FEE_CONFIGURATION_DATA) {
    return setFeeConfigurationData(state, action);
  } else {
    return state;
  }
};

export default feeConfigurationReducer;
