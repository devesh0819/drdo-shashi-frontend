import call from "../../../Api/index";
import { showToast } from "../../../Components/Toast/Toast";
import { logout } from "../../../Services/localStorageService";
import { logoutAction } from "../../Login/loginAction";
import { SET_FEE_CONFIGURATION_DATA } from "./types";

export const setFeeConfigurationData = (value) => {
  return {
    type: SET_FEE_CONFIGURATION_DATA,
    payload: value,
  };
};

export const getFeeConfigurationData = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/payment/get-config`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(setFeeConfigurationData(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err, "error");
      });
  };
};

export const updateFeeConfigurationAction = (payload) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `admin/payment/save-config`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Fee configuration updated successfully", "success");
          dispatch(setFeeConfigurationData(res.body.data));
        }
      })
      .catch((err) => {
        if (
          err?.response?.status === 401 &&
          err?.response?.statusText === "Unauthorized"
        ) {
          logout();
        }
        if (Object.keys(err?.response?.body?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.body?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];

          if (
            err?.response?.body?.errors[tempError] &&
            err?.response?.body?.errors[tempError][0]
          ) {
            showToast(err?.response?.body?.errors[tempError][0], "error");
          }
        }
      });
  };
};
