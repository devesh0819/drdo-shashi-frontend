import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
  agencyList: [],
  assessorDetail: {},

  assessorList: null,
};
const getAgencyList = (state, action) => {
  return updateObject(state, {
    agencyList: action.payload,
  });
};
const getAssessorList = (state, action) => {
  return updateObject(state, {
    assessorList: action.payload,
  });
};
const addAgency = (state, action) => {
  return updateObject(state, {
    // agencyList: action.payload,
  });
};
const getAssessorDetail = (state, action) => {
  return updateObject(state, {
    assessorDetail: action.payload,
  });
};

const agencyAssessorReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET_AGENCY_ASSESSOR_LIST) {
    return getAgencyList(state, action);
  }
  if (action.type === types.GET_ASSESSOR_LIST) {
    return getAssessorList(state, action);
  }
  if (action.type === types.ADD_AGENCY) {
    return addAgency(state, action);
  }
  if (action.type === types.GET_ASSESSOR_DETAIL) {
    return getAssessorDetail(state, action);
  } else {
    return state;
  }
};

export default agencyAssessorReducer;
