export const GET_AGENCY_ASSESSOR_LIST = "GET_AGENCY_ASSESSOR_LIST";
export const ADD_AGENCY = "ADD_AGENCY";
export const GET_ASSESSOR_DETAIL = "GET_ASSESSOR_DETAIL";
export const GET_ASSESSOR_LIST = "GET_ASSESSOR_LIST";
