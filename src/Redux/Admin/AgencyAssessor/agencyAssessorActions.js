import * as types from "./types";
import call from "../../../Api";
import { showToast } from "../../../Components/Toast/Toast";
import { showLoader } from "../../Loader/loaderActions";
import { logoutAction } from "../../Login/loginAction";
import axios from "axios";
import { getItem, logout } from "../../../Services/localStorageService";

export const getAgencyList = (value) => {
  return {
    type: types.GET_AGENCY_ASSESSOR_LIST,
    payload: value,
  };
};

export const addAgency = (value) => {
  return {
    type: types.ADD_AGENCY,
    payload: value,
  };
};

export const getAssessorDetail = (value) => {
  return {
    type: types.GET_ASSESSOR_DETAIL,
    payload: value,
  };
};

export const getAssessorList = (value) => {
  return {
    type: types.GET_ASSESSOR_LIST,
    payload: value,
  };
};

export const getAgencyListAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "admin/agency",
      dispatch,
      query: query,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAgencyList(res?.body?.data?.agencies));
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};

export const addAgencyAction = (payload, setAddAgency, setEditAgencyId) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "admin/agency",
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Agency added successfully", "success");
          setAddAgency(false);
          setEditAgencyId("");
          dispatch(getAgencyListAction());
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.errors?.email[0], "error");
      });
  };
};
export const deleteAgencyAction = (id) => {
  return (dispatch) => {
    call({
      method: "delete",
      endpoint: `admin/agency/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Agency deleted successfully", "success");
          dispatch(getAgencyListAction());
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};
export const updateAgencyAction = (
  id,
  payload,
  setAddAgency,
  setEditAgencyId
) => {
  return (dispatch) => {
    call({
      method: "put",
      endpoint: `admin/agency/${id}`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Agency updated successfully", "success");
          setAddAgency(false);
          setEditAgencyId("");
          dispatch(getAgencyListAction());
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};
export const getAssessorListAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "admin/assessor",
      dispatch,
      query: query,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAssessorList(res?.body?.data));
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};
export const addAssessorAction = (
  payload,
  setAddAssessor,
  setEditAssessorId,
  setEditAssessorData,
  dispatch,
  navigate
) => {
  // return (dispatch) => {
  //   call({
  //     method: "post",
  //     endpoint: "admin/assessor",
  //     payload: payload,
  //     dispatch,
  //   })
  //     .then((res) => {
  //       if (res.status === 200) {
  //         showToast("Assessor added successfully", "success");
  //         setAddAssessor(false);
  //         setEditAssessorId("");
  //         setEditAssessorData({});
  //         dispatch(getAgencyListAction());
  //       }
  //     })
  //     .catch((err) => {
  //       dispatch(showLoader(false));
  //       if (
  //         err?.response?.status === 401 &&
  //         err?.response?.statusText === "Unauthorized"
  //       ) {
  //         logout();
  //       }
  //       if (Object.keys(err?.response?.body?.errors)?.length > 0) {
  //         const currentError = Object.keys(err?.response?.body?.errors);
  //         let tempError = "";
  //         if (currentError?.length > 0) tempError = currentError[0];

  //         if (
  //           err?.response?.body?.errors[tempError] &&
  //           err?.response?.body?.errors[tempError][0]
  //         ) {
  //           showToast(err?.response?.body?.errors[tempError][0], "error");
  //         }
  //       }
  //     });
  // };

  dispatch(showLoader(true));
  return () => {
    axios
      .post(`${process.env.REACT_APP_BASE_URL}/admin/assessor`, payload, {
        headers: {
          "Content-type": "multipart/form-data",
          Accept: "application/json",
          Authorization: `Bearer ${getItem("token")}`,
        },
      })
      .then((res) => {
        dispatch(showLoader(false));
        if (res.status === 200) {
          showToast("Assessor added successfully", "success");
          dispatch(getAssessorListAction({ start: 1 }));
          setAddAssessor(false);
          setEditAssessorId("");
          setEditAssessorData({});
        }
      })
      .catch((err) => {
        dispatch(showLoader(false));
        if (
          err?.response?.status === 401 &&
          err?.response?.statusText === "Unauthorized"
        ) {
          logout();
        }
        if (Object.keys(err?.response?.data?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.data?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];

          if (
            err?.response?.data?.errors[tempError] &&
            err?.response?.data?.errors[tempError][0]
          ) {
            showToast(err?.response?.data?.errors[tempError][0], "error");
          }
        }
      });
  };
};
export const deleteAssessorAction = (id) => {
  return (dispatch) => {
    call({
      method: "delete",
      endpoint: `admin/assessor/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Assessor deleted successfully", "success");
          dispatch(getAgencyListAction());
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};
export const updateAssessorAction = (
  id,
  payload,
  setAddAssessor,
  setEditAssessorId,
  setEditAssessorData,
  dispatch
) => {
  dispatch(showLoader(true));
  return () => {
    axios
      .post(`${process.env.REACT_APP_BASE_URL}/admin/assessor/${id}`, payload, {
        headers: {
          "Content-type": "multipart/form-data",
          Accept: "application/json",
          Authorization: `Bearer ${getItem("token")}`,
        },
      })
      .then((res) => {
        dispatch(showLoader(false));
        if (res.status === 200) {
          showToast("Assesssor updated successfully", "success");
          setAddAssessor(false);
          setEditAssessorId("");
          setEditAssessorData({});
          dispatch(getAssessorListAction({ start: 1 }));
        }
      })
      .catch((err) => {
        dispatch(showLoader(false));
        if (
          err?.response?.status === 401 &&
          err?.response?.statusText === "Unauthorized"
        ) {
          logout();
        }
        if (Object.keys(err?.response?.data?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.data?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];

          if (
            err?.response?.data?.errors[tempError] &&
            err?.response?.data?.errors[tempError][0]
          ) {
            showToast(err?.response?.data?.errors[tempError][0], "error");
          }
        }
      });
  };
  // return (dispatch) => {
  //   call({
  //     method: "put",
  //     endpoint: `admin/assessor/${id}`,
  //     payload: payload,
  //     dispatch,
  //   })
  //     .then((res) => {
  //       if (res.status === 200) {
  //         showToast("Assesssor updated successfully", "success");
  //         setAddAssessor(false);
  //         setEditAssessorId("");
  //         setEditAssessorData({});
  //         dispatch(getAgencyListAction());
  //       }
  //     })
  //     .catch((err) => {
  //       dispatch(showLoader(false));
  //       if (
  //         err?.response?.status === 401 &&
  //         err?.response?.statusText === "Unauthorized"
  //       ) {
  //         logout();
  //       }
  //       if (Object.keys(err?.response?.body?.errors)?.length > 0) {
  //         const currentError = Object.keys(err?.response?.body?.errors);
  //         let tempError = "";
  //         if (currentError?.length > 0) tempError = currentError[0];

  //         if (
  //           err?.response?.body?.errors[tempError] &&
  //           err?.response?.body?.errors[tempError][0]
  //         ) {
  //           showToast(err?.response?.body?.errors[tempError][0], "error");
  //         }
  //       }
  //     });
  // };
};

export const viewAssessorAction = (id) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/assessor/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAssessorDetail(res.body.data));
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};
