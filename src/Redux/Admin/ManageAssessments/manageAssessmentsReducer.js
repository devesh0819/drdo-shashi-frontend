import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
  assessmentsListingData: {},
  agencyListingData: {},
  applicantDetailData: {},
  agencyTableListingData: {},
  assessorListData: {},
  feedbackData: null,
};
const setAssessmentsListingData = (state, action) => {
  return updateObject(state, {
    assessmentsListingData: action.payload,
  });
};
const setAgencyTableListingData = (state, action) => {
  return updateObject(state, {
    agencyTableListingData: action.payload,
  });
};
const setAgencyListingData = (state, action) => {
  return updateObject(state, {
    agencyListingData: action.payload,
  });
};
const setAssessorListData = (state, action) => {
  return updateObject(state, {
    assessorListData: action.payload,
  });
};
const setApplicantDetailData = (state, action) => {
  return updateObject(state, {
    applicantDetailData: action.payload,
  });
};
const getFeedbackData = (state, action) => {
  return updateObject(state, {
    feedbackData: action.payload,
  });
};

const getManageAssessmentsReducer = (state = initialState, action = {}) => {
  if (action.type === types.SET_ASSESSMENTS_LISTING_DATA) {
    return setAssessmentsListingData(state, action);
  }
  if (action.type === types.SET_AGENCY_TABLE_LISTING_DATA) {
    return setAgencyTableListingData(state, action);
  }
  if (action.type === types.SET_AGENCY_LISTING_DATA) {
    return setAgencyListingData(state, action);
  }
  if (action.type === types.SET_ASSESSOR_LIST) {
    return setAssessorListData(state, action);
  }
  if (action.type === types.SET_APPLICANT_DETAIL_DATA) {
    return setApplicantDetailData(state, action);
  }
  if (action.type === types.GET_FEEDBACK_DATA) {
    return getFeedbackData(state, action);
  } else {
    return state;
  }
};

export default getManageAssessmentsReducer;
