import * as types from "./types";
import call from "../../../Api";
import { showToast } from "../../../Components/Toast/Toast";
import { MANAGE_ASSESSMENTS } from "../../../Routes/Routes";
import { setAdminTabAction } from "../AdminAllApplications/adminAllApplicationAction";
export const setAssessmentsListingData = (value) => {
  return {
    type: types.SET_ASSESSMENTS_LISTING_DATA,
    payload: value,
  };
};
export const getAgencyTableListingData = (value) => {
  return {
    type: types.SET_AGENCY_TABLE_LISTING_DATA,
    payload: value,
  };
};
export const setAgencyListingData = (value) => {
  return {
    type: types.SET_AGENCY_LISTING_DATA,
    payload: value,
  };
};

export const getAssessorList = (data) => {
  return {
    type: types.SET_ASSESSOR_LIST,
    payload: data,
  };
};

export const setApplicantDetailData = (value) => {
  return {
    type: types.SET_APPLICANT_DETAIL_DATA,
    payload: value,
  };
};

export const getFeedbackData = (value) => {
  return {
    type: types.GET_FEEDBACK_DATA,
    payload: value,
  };
};

// export const getFeedbackDataAction = (application_id) => {
//   return (dispatch) => {
//     call({
//       method: "get",
//       endpoint: `applicant/application/${application_id}/feedback`,
//       dispatch,
//     })
//       .then((res) => {
//         if (res.status === 200) {
//           dispatch(getFeedbackData(res.body));
//         }
//       })
//       .catch((err) => {
//         showToast("Something went wrong", "error");
//       });
//   };
// };

export const getAssessmentsListingAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "admin/assessments",
      query: query,
      dispatch,
    })
      .then((res) => {
        dispatch(setAssessmentsListingData(res.body));
      })
      .catch((err) => {
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const getAgencyTableListingAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "agency/assessments",
      query: query,
      dispatch,
    })
      .then((res) => {
        dispatch(getAgencyTableListingData(res.body));
      })
      .catch((err) => {
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const getAgencyListingAction = () => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "qci_master_values",
      query: { type: "agency" },
      dispatch,
    })
      .then((res) => {
        dispatch(setAgencyListingData(res.body));
      })
      .catch((err) => {
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const getAssessorsListAction = (agencyId, districtId) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `qci_master_values?type=assessor&agencyId=${agencyId}&districtID=${districtId}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getAssessorList(res.body.data));
        }
      })
      .catch((err) => {
        showToast(err.message, "error");
      });
  };
};

export const updateAssignedAgency = (data, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `admin/assessment/${data.uuid}/update`,
      payload: data.body,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Assigned agency successfully", "success");
          navigate(MANAGE_ASSESSMENTS);
          dispatch(setAdminTabAction("1"));
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const updateAssignedAssessors = (data, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `agency/assessment/${data.uuid}/assign-assessors`,
      payload: data.body,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Assessors assigned successfully", "success");
          navigate(MANAGE_ASSESSMENTS);
          dispatch(setAdminTabAction("1"));
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.message, "error");
      });
  };
};

export const getApplicantDetailData = (uuid) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/assessment/${uuid}/show`,
      query: { type: "agency" },
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(setApplicantDetailData(res.body));
        }
      })
      .catch((err) => {
        showToast(err?.response?.body?.message, "error");
      });
  };
};
