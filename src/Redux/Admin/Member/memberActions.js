import * as types from "./types";
import call from "../../../Api";
import { showToast } from "../../../Components/Toast/Toast";
import { showLoader } from "../../Loader/loaderActions";
import axios from "axios";
import { getItem } from "../../../Services/localStorageService";

const getMembersListingData = (value) => {
  return {
    type: types.GET__MEMBERS_LISTING_DATA,
    payload: value,
  };
};

const getCommitteeListingData = (value) => {
  return {
    type: types.GET__COMMITTEE_LISTING_DATA,
    payload: value,
  };
};

const getCommitteeDetailData = (value) => {
  return {
    type: types.GET__COMMITTEE_DETAIL_DATA,
    payload: value,
  };
};

export const addMember = (value) => {
  return {
    type: types.ADD_MEMBER,
    payload: value,
  };
};

// Members listing and filtering apis
export const getMembersListingAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "admin/member",
      query: query,
      dispatch,
    })
      .then((res) => {
        dispatch(getMembersListingData(res.body));
      })
      .catch((err) => {
        showToast(err || "Something went wrong", "error");
      });
  };
};

export const addMemberAction = (
  payload,
  committeeId,
  committeeTitle,
  navigate,
  closeModal
) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "admin/member",
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Member added successfully", "success");
          let data = {
            title: committeeTitle,
            addNew: true,
            members: [],
          };
          data.members.push(res.body.data.id);
          dispatch(updateCommitteeAction(committeeId, data, navigate));
          closeModal(false);
        }
      })
      .catch((err) => {
        // showToast("Something went wrong", "error");
        if (Object.keys(err?.response?.body?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.body?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];
          if (
            err?.response?.body?.errors[tempError] &&
            err?.response?.body?.errors[tempError][0]
          ) {
            showToast(err?.response?.body?.errors[tempError][0], "error");
          }
        }
      });
  };
};

export const updateMemberAction = (id, payload, committeeId, closeModal) => {
  return (dispatch) => {
    call({
      method: "put",
      endpoint: `admin/member/${id}`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Member updated successfully", "success");
          dispatch(getCommitteeDetailAction(committeeId, null));
          closeModal(false);
        }
      })
      .catch((err) => {
        // showToast("Something went wrong", "error");
        if (Object.keys(err?.response?.body?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.body?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];
          if (
            err?.response?.body?.errors[tempError] &&
            err?.response?.body?.errors[tempError][0]
          ) {
            showToast(err?.response?.body?.errors[tempError][0], "error");
          }
        }
      });
  };
};

export const deleteMemberAction = (id) => {
  return (dispatch) => {
    call({
      method: "delete",
      endpoint: `admin/member/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Member deleted successfully", "success");
          dispatch(getCommitteeDetailAction(id, null));
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};

// Committees listing and filtering apis
export const getCommitteeListingAction = (query) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: "admin/committee",
      query: query,
      dispatch,
    })
      .then((res) => {
        dispatch(getCommitteeListingData(res.body));
      })
      .catch((err) => {
        showToast(err || "Something went wrong", "error");
      });
  };
};

export const getCommitteeDetailAction = (id, navigate) => {
  return (dispatch) => {
    call({
      method: "get",
      endpoint: `admin/committee/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          dispatch(getCommitteeDetailData(res.body));
          if (navigate) navigate(`/committee/edit/${id}`);
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};

export const addCommitteeAction = (payload, navigate) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: "admin/committee",
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Committee added successfully", "success");
          // dispatch(
          //   getCommitteeDetailAction(res.body.data.committee_uuid, null)
          // );
          dispatch(getCommitteeListingAction());
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};

export const updateCommitteeAction = (id, payload, navigate) => {
  return (dispatch) => {
    call({
      method: "put",
      endpoint: `admin/committee/${id}`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Committee updated successfully", "success");
          dispatch(getCommitteeDetailAction(res.body.data.uuid, navigate));
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};

export const deleteCommitteeAction = (id) => {
  return (dispatch) => {
    call({
      method: "delete",
      endpoint: `admin/committee/${id}`,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Committee deleted successfully", "success");
          dispatch(getCommitteeListingAction());
        }
      })
      .catch((err) => {
        showToast("Something went wrong", "error");
      });
  };
};

export const scheduleMeetingAction = (payload, id, navigate, closeModal) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `admin/committee/${id}/meeting`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Meeting Scheduled successfully", "success");
          dispatch(getCommitteeDetailAction(id, null));
          closeModal(false);
        }
      })
      .catch((err) => {
        // showToast("Something went wrong", "error");
        if (Object.keys(err?.response?.body?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.body?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];
          if (
            err?.response?.body?.errors[tempError] &&
            err?.response?.body?.errors[tempError][0]
          ) {
            showToast(err?.response?.body?.errors[tempError][0], "error");
          }
        }
      });
  };
};
export const updateMeetingAction = (
  payload,
  id,
  navigate,
  closeModal,
  committeeId
) => {
  return (dispatch) => {
    call({
      method: "post",
      // endpoint: `admin/committee/${id}/meeting`,
      endpoint: `admin/meeting/${id}`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Meeting updated successfully", "success");
          dispatch(getCommitteeDetailAction(committeeId, null));
          closeModal(false);
        }
      })
      .catch((err) => {
        // showToast("Something went wrong", "error");
        if (Object.keys(err?.response?.body?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.body?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];
          if (
            err?.response?.body?.errors[tempError] &&
            err?.response?.body?.errors[tempError][0]
          ) {
            showToast(err?.response?.body?.errors[tempError][0], "error");
          }
        }
      });
  };
};

export const addNoteAction = (
  id,
  payload,
  closeModal,
  committeeId,
  navigate
) => {
  return (dispatch) => {
    call({
      method: "post",
      endpoint: `admin/meeting/${id}/note`,
      payload: payload,
      dispatch,
    })
      .then((res) => {
        if (res.status === 200) {
          showToast("Note added successfully", "success");
          dispatch(getCommitteeDetailAction(committeeId, null));
          navigate(`/committee/edit/${committeeId}/meeting/${id}`);
          closeModal(false);
        }
      })
      .catch((err) => {
        // showToast("Something went wrong", "error");
        if (Object.keys(err?.response?.body?.errors)?.length > 0) {
          const currentError = Object.keys(err?.response?.body?.errors);
          let tempError = "";
          if (currentError?.length > 0) tempError = currentError[0];
          if (
            err?.response?.body?.errors[tempError] &&
            err?.response?.body?.errors[tempError][0]
          ) {
            showToast(err?.response?.body?.errors[tempError][0], "error");
          }
        }
      });
  };
};
