import { updateObject } from "../../utility";
import * as types from "./types";

const initialState = {
  membersListingData: {},
  committeeListingData : [],
  committeeDetailData : {}
};

const getMembersListingData = (state, action) => {
  return updateObject(state, {
    membersListingData: action.payload,
  });
};

const getCommitteeListingData = (state, action) => {
  return updateObject(state, {
    committeeListingData: action.payload,
  });
};

const getCommitteeDetailData = (state, action) => {
  return updateObject(state, {
    committeeDetailData: action.payload,
  });
};

const addMember = (state, action) => {
  return updateObject(state, {
    // agencyList: action.payload,
  });
};

const getMembersReducer = (state = initialState, action = {}) => {
  if (action.type === types.GET__MEMBERS_LISTING_DATA) {
    return getMembersListingData(state, action);
  }else if (action.type === types.ADD_MEMBER) {
    return addMember(state, action);
  }else if (action.type === types.GET__COMMITTEE_LISTING_DATA) {
    return getCommitteeListingData(state, action);
  }else if (action.type === types.GET__COMMITTEE_DETAIL_DATA) {
    return getCommitteeDetailData(state, action);
  }else {
    return state;
  }
};

export default getMembersReducer;
