import { combineReducers } from "redux";
import loaderReducer from "./Loader/loaderReducer";
import getCaptchaReducer from "./GetCaptcha/getCaptchaReducer";
import loginReducer from "./Login/loginReducer";
import signUpReducer from "./SignUp/signUpReducer";
import applicantFormReducer from "./ApplicantForm/applicantFormReducer";
import getManageAssessmentsReducer from "./Admin/ManageAssessments/manageAssessmentsReducer";
import agencyAssessorReducer from "./Admin/AgencyAssessor/agencyAssessorReducer";
import getMembersReducer from "./Admin/Member/memberReducer";
import getMasterDataReducer from "./Admin/GetMasterData/getMasterDataReducer";
import allApplicationsReducer from "./AllApplications/allApplicationsReducer";
import paymentReducer from "./Payment/paymentReducer";
import drdoAllApplicationsReducer from "./DrdoAdmin/DrdoAllApplications/drdoAllApplicationsReducer";
import adminAllApplicationsReducer from "./Admin/AdminAllApplications/adminAllApplicationReducer";
import manageQuestionairreReducers from "./Admin/ManageQuestionairre/manageQuestionairreReducers";
import assessorAllApplicationsReducers from "./LeadAssessor/assessorAllApplicationsReducers";
import dashboardReducer from "./Admin/Dashboard/dashboardReducer";
import drdoQuestionairreReducer from "./DrdoAdmin/DrdoQuestionairre/drdoQuestionairreReducers";
import footerLinkReducer from "./FooterLinks/footerLinkReducer";
import profileReducer from "./Profile/profileReducer";
import feeConfigurationReducer from "./Admin/FeeConfiguration/feeConfigurationReducer";
import feedbackConfigurationReducer from "./Admin/FeedbackConfiguration/feedbackConfigurationReducer";
const reducers = {
  loader: loaderReducer,
  getCaptcha: getCaptchaReducer,
  login: loginReducer,
  signUp: signUpReducer,
  applicantForm: applicantFormReducer,
  manageAssessments: getManageAssessmentsReducer,
  agencyAssessor: agencyAssessorReducer,
  members: getMembersReducer,
  getMasterData: getMasterDataReducer,
  allApplications: allApplicationsReducer,
  payment: paymentReducer,
  drdoAllApplications: drdoAllApplicationsReducer,
  adminAllApplications: adminAllApplicationsReducer,
  questionairres: manageQuestionairreReducers,
  assessorAllAssessments: assessorAllApplicationsReducers,
  adminDashboard: dashboardReducer,
  drdoQuestionairres: drdoQuestionairreReducer,
  footerLinksData: footerLinkReducer,
  profile: profileReducer,
  feeConfiguration: feeConfigurationReducer,
  feedbackConfiguration: feedbackConfigurationReducer,
};
const reducer = combineReducers(reducers);

const combined = (state, action) => reducer(state, action);
export default combined;
