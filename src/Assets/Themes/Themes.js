import { createTheme } from "@mui/material";
import * as colors from "../../Constant/ColorConstant";

// theme overriding
const theme = createTheme({
  typography: {
    fontFamily: ["Open Sans", "sans-serif"].join(","),
  },
  palette: {
    primary: {
      main: colors.SKY_BLUE,
    },
  },
});

export default theme;
