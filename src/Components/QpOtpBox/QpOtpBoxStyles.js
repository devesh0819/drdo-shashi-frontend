export const styles = {
  otpInputField: {
    width: "3rem",
    height: "3rem",
    fontWeight: "500",
    fontSize: "0.875rem",
    lineHeight: "1.313rem",
    background: "#FFFFFF",
    boxSizing: "border-box",
    borderBottom: "0.188rem solid #E7E7E7",
    "&.Mui-focused": {
      borderBottom: "0.188rem solid #A8A8A8",
    },
    // boxShadow: "0px 0.25rem 0.5rem rgba(0, 0, 0, 0.02)",
    // borderRadius: "0.5rem",
    textAlign: "center",
    input: {
      textAlign: "center",
    },
    "@media(max-width:600px)": {
      height: "2rem",
      width: "2rem",
    },
  },
  otpBoxDiv: {
    textAlign: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
};
