import React from "react";
import { Box } from "@mui/material";
import QpInputBase from "../QpInputBase/QpInputBase";
import { styles } from "./QpOtpBoxStyles";

export default function QpOtpBox(props) {
  const refs = React.useRef([]);
  const otpBoxOnChangeHandler = (e) => {
    props.otpHandle(e);
    let inputOtpValue = e.target.name;

    if (inputOtpValue === "otpBox1") {
      if (e.target.value.length === 1) refs.current[1].focus();
    } else if (inputOtpValue === "otpBox2") {
      if (e.target.value.length === 1) refs.current[2].focus();
      else refs.current[0].focus();
    } else if (inputOtpValue === "otpBox3") {
      if (e.target.value.length === 1) refs.current[3].focus();
      else refs.current[1].focus();
    } else if (inputOtpValue === "otpBox4") {
      if (e.target.value.length === 1) refs.current[4].focus();
      else refs.current[2].focus();
    } else if (inputOtpValue === "otpBox5") {
      if (e.target.value.length === 1) refs.current[5].focus();
      else refs.current[3].focus();
    } else if (inputOtpValue === "otpBox6") {
      if (e.target.value.length === 1) props.setMobileOtpError(false);
      else {
        refs.current[4].focus();
        props.setMobileOtpError(true);
      }
    } else {
      if (e.target.value.length === 0) refs.current[4].focus();
    }
  };
  return (
    <>
      <Box sx={styles.otpBoxDiv}>
        <QpInputBase
          name="otpBox1"
          maxLen={1}
          onChange={otpBoxOnChangeHandler}
          styleData={styles.otpInputField}
          inputRef={(element) => {
            refs.current[0] = element;
          }}
        />
        <QpInputBase
          name="otpBox2"
          maxLen={1}
          onChange={otpBoxOnChangeHandler}
          styleData={styles.otpInputField}
          inputRef={(element) => {
            refs.current[1] = element;
          }}
        />
        <QpInputBase
          name="otpBox3"
          maxLen={1}
          onChange={otpBoxOnChangeHandler}
          styleData={styles.otpInputField}
          inputRef={(element) => {
            refs.current[2] = element;
          }}
        />
        <QpInputBase
          name="otpBox4"
          maxLen={1}
          onChange={otpBoxOnChangeHandler}
          styleData={styles.otpInputField}
          inputRef={(element) => {
            refs.current[3] = element;
          }}
        />
        <QpInputBase
          name="otpBox5"
          maxLen={1}
          onChange={otpBoxOnChangeHandler}
          styleData={styles.otpInputField}
          inputRef={(element) => {
            refs.current[4] = element;
          }}
        />
        <QpInputBase
          name="otpBox6"
          maxLen={1}
          onChange={otpBoxOnChangeHandler}
          styleData={styles.otpInputField}
          inputRef={(element) => {
            refs.current[5] = element;
          }}
        />
      </Box>
    </>
  );
}
