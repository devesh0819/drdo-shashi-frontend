import {
  Autocomplete,
  Box,
  Checkbox,
  Grid,
  InputBase,
  MenuItem,
  Select,
  TextareaAutosize,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as _ from "lodash";
import { commonStyles } from "../../Styles/CommonStyles";
import QpButton from "../QpButton/QpButton";
import { styles } from "../QpImportQuestionModal/QpImportQuestionStyles";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import QpTypography from "../QpTypography/QpTypography";
import moment from "moment";
import { useParams } from "react-router-dom";
import InputLabel from "@mui/material/InputLabel";
import CustomImageUploader from "../CustomImageUploader/CustomImageUploader";
import { showToast } from "../Toast/Toast";

const AssignMultipleParameters = (props) => {
  const initialState = {
    questionairre_doc: "",
  };

  const dispatch = useDispatch();

  const [selectedAssessor, setSelectedAssessor] = useState("");
  const [selectedAssessorID, setSelectdAssessorID] = useState(null);
  const [assignedParametersCount, setAssignedParametersCount] = useState(0);
  const [unassignedParameters, setUnassignedParameters] = useState([]);
  const [nameError, setNameError] = useState(false);

  useEffect(() => {}, [selectedAssessor]);

  const handleInputChange = (event) => {};

  const handleSelectChange = (event) => {
    setAssignedParametersCount(props.checkedParamIds.length);
    let value = event?.target?.value;
    props.handleAssign(value);
    let res = props?.applicationDetail?.assessors?.find(
      (item) => value === item?.assessor_id
    );
    props?.applicationParameterDetail?.parameters?.disciplines?.forEach(
      (discipline) => {
        discipline?.parameters?.forEach((param) => {
          if (param.assessor_id === res.assessor_id) {
            setAssignedParametersCount((prev) => prev + 1);
          }
        });
      }
    );
    if (!res) {
      setNameError(true);
    } else {
      let name = `${res?.profile?.first_name}${" "}${res?.profile?.last_name}`;
      setSelectedAssessor(name);
      setSelectdAssessorID(value || res);
      setNameError(false);
    }
  };

  const handleCancel = () => {
    props.closeModal(false);
  };

  const onSubmit = () => {
    // if (
    //   selectedAssessor == "" ||
    //   selectedAssessor == undefined ||
    //   selectedAssessor == " "
    // )
    //   setNameError(true);
    // else {
    //   setNameError(false);
    //   props.handleAssign(selectedAssessorID);)
    // }
    if (!selectedAssessor) {
      showToast("Please select an assessor", "error");
      return;
    }
    props.handleSubmit();
  };

  const handleChange = (event, id) => {
    if (!selectedAssessor) {
      showToast("Please select an assessor", "error");
      return;
    }
    if (event.target.checked) {
      props.setCheckedParamIds((prev) => [...prev, id]);
      setAssignedParametersCount((prev) => prev + 1);
    } else {
      props.setCheckedParamIds((prev) => prev.filter((item) => item !== id));
      setAssignedParametersCount((prev) => prev - 1);
    }
  };

  const ParametersList = (props) => {
    return (
      <Box sx={styles.cardMainContainer}>
        <Checkbox
          checked={props.checkedParamIds.includes(props.id)}
          onChange={(e) => handleChange(e, props.id)}
        />
        <Box>
          <Typography sx={styles.parameterTitle}>{props.title}</Typography>
          <Typography
            sx={styles.disciplineTitle}
          >{`Discipline: ${props.disciplineTitle}`}</Typography>
        </Box>
      </Box>
    );
  };

  return (
    <Box sx={styles.formContainer}>
      <Grid container spacing={2}>
        <Grid xs={12} item>
          {/* <InputLabel id="demo-simple-select-label">Assessors<span style={{color:"red"}}>*</span></InputLabel> */}
          <Select
            id="assessor"
            name="assessor"
            MenuProps={{
              sx: {
                ...commonStyles.iconButtonStyle,
                ...commonStyles.fullWidth,
              },
            }}
            sx={styles.select}
            value={selectedAssessorID}
            onChange={handleSelectChange}
            native={false}
            renderValue={(val) => {
              if (!val)
                return (
                  <Typography sx={commonStyles.placeHolderColor}>
                    Select Assessor
                  </Typography>
                );
              else
                return (
                  <Typography sx={styles.dropdownText}>
                    {selectedAssessor}
                  </Typography>
                );
            }}
            displayEmpty={true}
          >
            {/* <MenuItem value={"Large"}  sx={styles.dropdownText}>Large</MenuItem>
                <MenuItem value={"Small/Medium"}  sx={styles.dropdownText}>Small/Medium</MenuItem> */}
            {props?.applicationDetail?.assessors?.map((item) => {
              return (
                <MenuItem
                  key={item}
                  value={item?.assessor_id}
                  sx={styles.dropdownText}
                >{`${item?.profile?.first_name}${" "}${
                  item?.profile?.last_name
                }`}</MenuItem>
              );
            })}
          </Select>
          {nameError && (
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={"Please select any one."}
            />
          )}
        </Grid>
        {selectedAssessorID && (
          <Typography
            sx={styles.countHeading}
          >{`Number of parameters assigned - ${assignedParametersCount}`}</Typography>
        )}
        <Box sx={styles.paramListContainer}>
          {props.unassignedParameters.map((parameter) => (
            <ParametersList
              key={parameter}
              title={parameter.parameter}
              id={parameter.id}
              checkedParamIds={props.checkedParamIds}
              disciplineTitle={parameter.disciplineTitle}
            />
          ))}
        </Box>

        <Box
          sx={{
            ...styles.buttonContainerMultiple,
            ...styles.buttonContainerImportModal,
          }}
        >
          <QpButton
            styleData={styles.saveButton}
            displayText="Save"
            onClick={onSubmit}
            textStyle={commonStyles.blueButtonText}
          />
          <QpButton
            displayText="Cancel"
            styleData={styles.cancelButton}
            onClick={handleCancel}
            textStyle={commonStyles.greyButtonText}
          />
        </Box>
      </Grid>
    </Box>
  );
};

export default AssignMultipleParameters;
