import React, { useEffect, useState } from "react";
import { Box } from "@mui/material";
import { commonStyles } from "../../Styles/CommonStyles";
import { useLocation, useNavigate } from "react-router-dom";
import QpTypography from "../QpTypography/QpTypography";
import { FORGOT_PASSWORD, LOGIN, RESEND_LINK } from "../../Routes/Routes";

export default function QpBoxContainer(props) {
  const { loginRequired = true } = props;
  const navigate = useNavigate();

  return (
    <Box sx={{ ...commonStyles.otpContainer, ...commonStyles.customScrollBar }}>
      <Box
        sx={{
          ...commonStyles.signupEnterOtpContainer,
          ...commonStyles.displayCenterStyle,
        }}
      >
        {props.children}
      </Box>
      {loginRequired && (
        <Box
          sx={{
            ...commonStyles.displayStyle,
            ...commonStyles.boxContainer,
          }}
        >
          <QpTypography
            displayText="Go back to  "
            styleData={{
              ...commonStyles.goToPageText,
              ...commonStyles.goToPageTextColor,
            }}
          />
          <QpTypography
            displayText="Login page"
            styleData={{
              ...commonStyles.goToPageText,
              ...commonStyles.goToLoginText,
            }}
            onClick={() => navigate(LOGIN)}
          />
        </Box>
      )}
    </Box>
  );
}
