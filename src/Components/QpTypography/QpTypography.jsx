import React from "react";
import Typography from "@mui/material/Typography";

const style = {
  color: {
    color: "#000000",
  },
};
const QpTypography = ({ displayText, styleData, ...restProps }) => {
  return (
    <Typography sx={{ ...style.color, ...styleData }} {...restProps}>
      {displayText}
    </Typography>
  );
};

export default QpTypography;
