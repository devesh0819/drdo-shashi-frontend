import React, { useEffect, useState } from "react";
import { Box } from "@mui/material";
import QpTypography from "../QpTypography/QpTypography";
import { commonStyles } from "../../Styles/CommonStyles";
import QpOtpBox from "../QpOtpBox/QpOtpBox";
import QpButton from "../QpButton/QpButton";
import { useDispatch } from "react-redux";
import { requestLinkAction } from "../../Redux/Login/loginAction";

export default function QpOtpVerification(props) {
  const { otpData, phoneNumber } = props;
  const dispatch = useDispatch();
  const [mobileOtpError, setMobileOtpError] = useState(false);
  const [time, setTime] = useState(15);
  const [resendLink, setResendLink] = useState(false);

  const [phoneOtp, setPhoneOtp] = useState({
    otpBox1: "",
    otpBox2: "",
    otpBox3: "",
    otpBox4: "",
    otpBox5: "",
    otpBox6: "",
  });
  const getMobileOtp = (e) => {
    setPhoneOtp({ ...phoneOtp, [e.target.name]: e.target.value });
  };
  const isOtpValid = (otpData) => {
    if (!otpData) {
      setMobileOtpError(true);
      return false;
    }
    if (otpData && otpData.length !== 6) {
      setMobileOtpError(true);
      return false;
    }
    if (otpData && otpData.length === 6) {
      setMobileOtpError(false);
      return true;
    }
    return false;
  };
  const concat = (data) => {
    if (!data) {
      return "";
    }
    return (
      data.otpBox1 +
      data.otpBox2 +
      data.otpBox3 +
      data.otpBox4 +
      data.otpBox5 +
      data.otpBox6
    );
  };
  const otpVerificationHandler = () => {
    let data = {};

    const mOTP = concat(phoneOtp);
    const isMobileOTPValid = isOtpValid(mOTP);
    if (isMobileOTPValid) {
      data["otp"] = mOTP;
    } else {
      return;
    }
    otpData(data);
  };
  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;

    const formattedMinutes = minutes.toString().padStart(2, "0");
    const formattedSeconds = seconds.toString().padStart(2, "0");

    return `${formattedMinutes}:${formattedSeconds}`;
  };

  useEffect(() => {
    if (time === 0) {
      setResendLink(true);
      return;
    } else {
      setResendLink(false);
    }
    const interval = setInterval(() => {
      setTime((prevTime) => prevTime - 1);
    }, 1000);

    return () => clearInterval(interval);
  }, [time]);
  return (
    <Box sx={commonStyles.enterOtpContainer}>
      <QpTypography
        displayText="Enter OTP"
        styleData={commonStyles.enterOtpText}
      />
      <QpTypography
        displayText={`Please enter the OTP sent on your email ID:  ${phoneNumber}`}
        styleData={commonStyles.verifyOtpText}
      />
      {/* <QpTypography
        displayText={`A 6 digit code has been sent to ${phoneNumber}`}
        styleData={commonStyles.digitCodeText}
      /> */}
      <QpTypography
        displayText={mobileOtpError ? "Required" : ""}
        styleData={{
          ...commonStyles.errorText,
          ...commonStyles.otpErrortext,
        }}
      />

      <QpOtpBox
        otpHandle={getMobileOtp}
        setMobileOtpError={setMobileOtpError}
      />
      <QpButton
        displayText="Validate"
        styleData={{
          ...commonStyles.validateButton,
        }}
        onClick={otpVerificationHandler}
      />
      <QpTypography
        displayText={formatTime(time)}
        styleData={{
          ...commonStyles.timer,
        }}
      />

      <QpTypography
        displayText="Resend One-Time Password"
        styleData={{
          ...commonStyles.resendPasswordText,
          ...(resendLink && commonStyles.enableResendPasswordText),
        }}
        onClick={() => resendLink && dispatch(requestLinkAction(setTime))}
      />
    </Box>
  );
}
