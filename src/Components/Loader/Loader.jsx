import React from "react";
import { Backdrop, CircularProgress } from "@mui/material";
import { useSelector } from "react-redux";

const Loader = (props) => {
  const { loader } = useSelector((state) => state.loader);

  return (
    <>
      <Backdrop
        open={loader || props.showLoader || false}
        sx={{
          zIndex: (theme) => theme.zIndex.drawer + 1,
          // display: "flex",
          // alignItems: "center",
          // justifyContent: "center",
        }}
      >
        <CircularProgress />
      </Backdrop>
    </>
  );
};

export default Loader;
