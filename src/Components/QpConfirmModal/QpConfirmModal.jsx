import React from "react";
import { commonStyles } from "../../Styles/CommonStyles";
import { Box } from "@mui/material";
import { styles } from "./QpConfirmModalStyles.js";
import QpTypography from "../QpTypography/QpTypography";
import QpButton from "../QpButton/QpButton";

const QpConfirmModal = (props) => {
  const handleClose = () => {
    props.closeModal(false);
  };
  const handleConfirm = () => {
    props.onConfirm();
    props.closeModal(false);
  };
  return (
    <>
      <Box sx={styles.modalContainer}>
        <QpTypography
          displayText={props.displayText}
          styleData={{
            ...styles.displayTextStyle,
          }}
        />
        <Box sx={styles.buttonContainer}>
          <QpButton
            displayText="Cancel"
            styleData={commonStyles.greyButton}
            onClick={handleClose}
            textStyle={commonStyles.greyButtonText}
          />

          <QpButton
            styleData={commonStyles.blueButton}
            displayText="Confirm"
            onClick={handleConfirm}
            textStyle={commonStyles.blueButtonText}
          />
        </Box>
      </Box>
    </>
  );
};

export default QpConfirmModal;
