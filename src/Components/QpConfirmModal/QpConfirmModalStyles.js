export const styles = {
  displayTextStyle: {
    fontWeight: 600,
    fontSize: "1.25rem",
    textAlign: "center",
    marginBottom: "3rem",
  },
  modalContainer: { margin: "1rem" },
  buttonContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
};
