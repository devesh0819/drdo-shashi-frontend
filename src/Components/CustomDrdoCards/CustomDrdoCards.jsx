import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import QpTypography from "../QpTypography/QpTypography";
import { styles } from "./CustomDrdoCardsStyle";

const CustomDrdoCards = (props) => {
  return (
    <Box sx={styles.cardContainer}>
      <QpTypography displayText={props.displayText} />
      <Box sx={styles.cardCountContainer}>
        <Box sx={styles.countTextContainer}>
          <QpTypography
            displayText={props.firstBoxCount}
            styleData={styles.countStyle}
          />
          <QpTypography
            displayText={props.firstBoxText}
            styleData={styles.countTextStyle}
          />
        </Box>
        <Box sx={styles.countTextContainer}>
          <QpTypography
            displayText={props.secondBoxCount}
            styleData={styles.countStyle}
          />
          <QpTypography
            displayText={props.secondBoxText}
            styleData={styles.countTextStyle}
          />
        </Box>
        <Box sx={styles.countTextContainer}>
          <QpTypography
            displayText={props.thirdBoxCount}
            styleData={styles.countStyle}
          />
          <QpTypography
            displayText={props.thirdBoxText}
            styleData={styles.countTextStyle}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default CustomDrdoCards;
