export const styles = {
  cardContainer: {
    // height: "8rem",
    padding: "0.50rem",
    margin: "1rem",
    borderRadius: "5px",
    boxShadow: "0px 0px 6px rgba(184, 184, 184, 0.25)",
  },
  countTextContainer: {
    display: "flex",
    flexDirection: "column",
    width: "6.025rem",

    // alignItem:"center",
    // justifyContent:"center"
  },
  cardCountContainer: {
    display: "flex",
    margin: "2rem 0",
  },
  countTextStyle: {
    opacity: 0.4,
    color: "black",
    fontSize: "0.75rem",
  },
  countStyle: {
    fontWeight: 700,
    fontSize: "1.5rem",
  },
};
