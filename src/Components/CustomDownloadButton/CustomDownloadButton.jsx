import React from "react";
import { ReactComponent as IconDownload } from "../../Assets/Images/iconDownload.svg";
import { Button, Typography } from "@mui/material";

const CustomDownloadButton = (props) => {
  return (
    <Button sx={props.buttonStyle}>
      <IconDownload />
      <Typography sx={props.textStyle}>{props.label}</Typography>
    </Button>
  );
};

export default CustomDownloadButton;
