import React, { useEffect, useRef, useState } from "react";
import { Box, Chip, CircularProgress, Grid, Typography } from "@mui/material";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { useDispatch, useSelector } from "react-redux";
import CustomTable from "../CustomTable/CustomTable";
import { showLoader } from "../../Redux/Loader/loaderActions";
import axios from "axios";
import { getItem } from "../../Services/localStorageService";
import { showToast } from "../Toast/Toast";
import { getImageAction } from "../../Redux/AllApplications/allApplicationsActions";
import ReactImageViewer from "../ReactImageViewer/ReactImageViewer";
import DropdownFilter from "../AllFilters/DropdownFilter";

const Row = (props) => {
  const { file, url, pdfUrl, imageArray, isDropdown, chipValue, multiOptions } =
    props;
  const dispatch = useDispatch();
  const runOnce = useRef(false);
  const runPdfOnce = useRef(false);

  const runImageOnce = useRef(false);

  const [imgUrl, setImgUrl] = useState("");
  const [imgArray, setImgArray] = useState([]);
  const [showLoader, setShowloader] = useState(false);
  const viewImage = useSelector((state) => state.allApplications.image);
  const [previewPdf, setPreviewPdf] = useState();
  const [isOpen, setIsOpen] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);

  const getImage = (path, isArray) => {
    // return () => {
    setShowloader(true);
    axios
      .get(path, {
        headers: {
          "Content-type": "image/jpeg",
          Authorization: `Bearer ${getItem("token")}`,
        },
        responseType: "blob",
      })
      .then((res) => {
        setTimeout(() => {
          setShowloader(false);
        }, 1000);
        if (res.status === 200) {
          if (!isArray) {
            setImgUrl(URL.createObjectURL(res.data));
          } else {
            setImgArray((prev) => [...prev, URL.createObjectURL(res.data)]);
          }
        }
      })
      .catch((err) => {
        setShowloader(false);
        showToast("Something went wrong", "error");
      });
    // };
  };

  useEffect(() => {
    if (pdfUrl) {
      dispatch(getImageAction(pdfUrl, dispatch, setPreviewPdf));
    }
    // return () => {
    //   runPdfOnce.current = true;
    // };
  }, [pdfUrl]);

  useEffect(() => {
    if (url) {
      getImage(url, false);
    }
    return () => {
      // runImageOnce.current = true;
    };
  }, [url]);

  useEffect(() => {
    if (imageArray && runOnce.current === false) {
      // getImagesArray(imageArray);
      imageArray.map((img) => getImage(img?.file, true));
    }
    return () => {
      runOnce.current = true;
    };
  }, [imageArray]);

  const handleImageClick = (event) => {
    setSelectedImage(event?.target?.src);
    setIsOpen(true);
  };

  const handleImageClose = () => {
    setSelectedImage(null);
    setIsOpen(false);
  };

  return (
    <>
      <Grid
        container
        columnSpacing={"2.5rem"}
        sx={commonStyles.rowMarginBottom}
      >
        <Grid
          item
          md={6}
          sm={12}
          xs={12}
          sx={isDropdown && commonStyles.rowContainer}
        >
          <Typography
            sx={
              props.applicantDetail
                ? commonStyles.applicantLabel
                : commonStyles.labelText
            }
          >
            {props.label}
          </Typography>
          {isDropdown && (
            <DropdownFilter
              // labelId={labelId}
              // id={labelId}
              value={props.dropdownValue}
              handleDropdownChange={props.handleDropdownChange}
              dataArray={props.dropdownOptions}
              dropdownStyle={commonStyles.rowDropdownStyle}
              // label={label}
              // isLocation={true}
            />
          )}
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <Typography
            sx={
              props.applicantDetail
                ? commonStyles.applicantValue
                : commonStyles.valueText
            }
          >
            {props.value}
          </Typography>
          {pdfUrl && previewPdf && (
            <span
              onClick={() => window.open(previewPdf, "_blank")}
              style={{
                ...commonStyles.cursorPointer,
                ...(props.applicantDetail
                  ? commonStyles.applicantValue
                  : commonStyles.valueText),
                ...commonStyles.linkText,
                ...commonStyles.positionRealtive,
              }}
            >
              Preview PDF
            </span>
          )}
          {imgUrl && !showLoader ? (
            <img
              // src={`data:image/*;base64,${imgUrl}`}
              // src={`${url}`}
              src={imgUrl}
              alt="url"
              style={{
                ...commonStyles.imagePreview,
                ...commonStyles.positionRealtive,
              }}
              onClick={(event) => handleImageClick(event)}
            />
          ) : (
            showLoader && url && <CircularProgress />
          )}
          {imgArray?.length === imageArray?.length && !showLoader
            ? imgArray?.map((image) => {
                return (
                  <img
                    // src={`data:image/*;base64,${image?.file}`}
                    src={image}
                    key={image}
                    alt="url"
                    style={commonStyles.imagePreview}
                    onClick={(event) => handleImageClick(event)}
                  />
                );
              })
            : null}
          {showLoader
            ? imageArray?.map((img) => {
                return <CircularProgress />;
              })
            : null}

          {props.valueArray &&
            props.valueArray.map((valueText, index) => {
              return (
                <Typography
                  sx={
                    props.applicantDetail
                      ? commonStyles.applicantValue
                      : commonStyles.valueText
                  }
                  key={valueText}
                >
                  {valueText}
                </Typography>
              );
            })}
          {chipValue?.length > 0 &&
            chipValue?.map((chipLabel, index) => {
              return (
                <Chip
                  label={chipLabel}
                  color="info"
                  sx={commonStyles.rowChipContainer}
                  key={index}
                />
              );
            })}
        </Grid>
        <Grid xs={12} md={12} sm={12} sx={commonStyles.multiSelect}>
          {multiOptions?.length > 0 &&
            multiOptions?.map((chipLabel, index) => {
              return (
                <Chip
                  label={chipLabel}
                  color="info"
                  sx={{
                    ...commonStyles.rowChipContainer,
                    ...commonStyles.multiRow,
                  }}
                  key={index}
                />
              );
            })}
        </Grid>
      </Grid>
      <ReactImageViewer
        imgs={selectedImage}
        isOpen={isOpen}
        onClose={handleImageClose}
        lat={null}
        long={null}
        timestamp={null}
      />
    </>
  );
};

export default Row;
