import React, { useState, useRef, useEffect } from "react";
import { Box, Grid } from "@mui/material";
import CustomInput from "../CustomInput/CustomInput";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import SaveNextButtons from "../SaveNextButtons/SaveNextButtons";
import { styles } from "../CustomAgencyDetails/CustomAgencyDetailsStyles.js";
import { useDispatch, useSelector } from "react-redux";
import {
  addAssessorAction,
  updateAssessorAction,
} from "../../Redux/Admin/AgencyAssessor/agencyAssessorActions";
import QpTypography from "../QpTypography/QpTypography";
import CustomDropdown from "../CustomDropdown/CustomDropdown";
import {
  getDistrictAction,
  getStatesAction,
} from "../../Redux/Admin/GetMasterData/getMasterDataActions";
import { assessorStatus } from "../../Constant/AppConstant";
import CustomImageUploader from "../CustomImageUploader/CustomImageUploader";
import * as _ from "lodash";
import { showToast } from "../Toast/Toast";

export default function CustomAssessorDetails({
  setAddAssessor,
  agencyId,
  editAssessorId,
  setEditAssessorId,
  setEditAssessorData,
  defaultFirstName,
  defaultLastName,
  defaultEmail,
  defaultPhoneNo,
  defaultAddress,
  defaultExpertise,
  defaultWorkDomain,
  defaultExperienceYear,
  defaultComment,
  defaultAlternatePhoneNo,
  defaultAssessorState,
  defaultAssessorDistrict,
  defaultAssessorStatus,
  assessorDetail,
}) {
  const initialState = {
    assessor_state: "",
    assessor_district: "",
    resume: "",
    assessor_status: "",
  };
  const [data, setData] = useState(initialState);
  const [isSubmit, setIsSubmit] = useState(false);
  const [assessorData, setAssessorData] = useState({});
  const assessorDataRef = useRef(assessorData);
  const stateOptions = useSelector((state) => state.getMasterData.statesData);
  const districtOptions = useSelector(
    (state) => state.getMasterData.districtData
  );

  const dispatch = useDispatch();
  const updateState = (newState) => {
    assessorDataRef.current = newState;
    setAssessorData(newState);
    setIsSubmit(false);
  };

  useEffect(() => {
    dispatch(getStatesAction());
  }, []);

  useEffect(() => {
    if (data?.assessor_state?.id) {
      dispatch(getDistrictAction(data.assessor_state?.id));
    }
  }, [data?.assessor_state?.id]);

  useEffect(() => {
    const {
      first_name,
      last_name,
      email,
      phone_number,
      address,
      expertise,
      assessor_state,
      assessor_district,
      work_domain,
      years_of_experience,
      assessor_status,
    } = assessorData;
    if (
      first_name &&
      last_name &&
      email &&
      phone_number &&
      assessor_state &&
      assessor_district &&
      expertise &&
      work_domain &&
      years_of_experience &&
      assessor_status
    ) {
      onSubmit();
    }
  }, [assessorData]);

  useEffect(() => {
    if (assessorDetail?.assessorMeta?.assessor_resume?.attachment_uuid) {
      setData((values) => ({
        ...values,
        resume: {
          name: `${assessorDetail?.assessorMeta?.assessor_resume?.attachment_uuid}`,
        },
      }));
    }
  }, [assessorDetail?.assessorMeta?.assessor_resume?.attachment_uuid]);

  useEffect(() => {
    if (assessorDetail?.assessorMeta?.assessor_state?.id) {
      setData((values) => ({
        ...values,
        assessor_state: assessorDetail?.assessorMeta?.assessor_state,
      }));
    }
  }, [assessorDetail?.assessorMeta?.assessor_state?.id]);

  useEffect(() => {
    if (assessorDetail?.assessorMeta?.assessor_district?.id) {
      setData((values) => ({
        ...values,
        assessor_district: assessorDetail?.assessorMeta?.assessor_district,
      }));
    }
  }, [assessorDetail?.assessorMeta?.assessor_district?.id]);

  useEffect(() => {
    if (assessorDetail?.assessorMeta?.assessor_status) {
      setData((values) => ({
        ...values,
        assessor_status: _.find(assessorStatus, {
          value: assessorDetail?.assessorMeta?.assessor_status,
        }),
      }));
    }
  }, [assessorDetail?.assessorMeta?.assessor_status]);

  const getData = (key, value) => {
    updateState({
      ...assessorDataRef.current,
      [key]: value,
    });
  };
  const saveHandler = () => {
    setIsSubmit(true);
  };
  const cancelHandler = () => {
    setAddAssessor(false);
    setEditAssessorId("");
    setEditAssessorData({});
  };

  const onSubmit = () => {
    const {
      first_name,
      last_name,
      email,
      alternate_email,
      phone_number,
      alternate_phone_number,
      address,
      expertise,
      assessor_district,
      assessor_state,
      assessor_status,
      assessor_status_comment,
      work_domain,
      years_of_experience,
    } = assessorData;
    // if (!data.resume) {
    //   showToast("Please upload resume", "error");
    //   return;
    // }
    if (alternate_email === undefined || alternate_phone_number === undefined) {
      return;
    }

    const newData = {
      first_name: first_name,
      last_name: last_name,
      email: email,
      phone_number: phone_number,
      // address: address,
      assessor_state: assessor_state?.id,
      assessor_district: assessor_district?.id,
      resume: data.resume,
      assessor_status: assessor_status?.value,
      expertise: expertise,
      work_domain: work_domain,
      years_of_experience: years_of_experience,
      agency_id: agencyId,
    };
    if (alternate_email) {
      newData["alternate_email"] = alternate_email;
    }
    if (assessor_status_comment) {
      newData["assessor_status_comment"] = assessor_status_comment;
    }
    const formData = new FormData();
    formData.append("agency_id", agencyId);
    formData.append("first_name", first_name);
    formData.append("last_name", last_name);
    formData.append("email", email);
    formData.append("phone_number", phone_number);
    formData.append("assessor_state", assessor_state?.id);
    formData.append("assessor_district", assessor_district?.id);
    // formData.append("resume", data.resume);
    if (!assessorDetail?.assessorMeta?.assessor_resume) {
      formData.append("resume", data?.resume);
    } else {
      const tempVal = data?.resume?.name;
      if (
        tempVal !==
        assessorDetail?.assessorMeta?.assessor_resume?.attachment_uuid
      ) {
        formData.append("resume", data?.resume);
      }
    }
    formData.append("assessor_status", assessor_status?.value);
    formData.append("expertise", expertise);
    formData.append("work_domain", work_domain);
    formData.append("years_of_experience", years_of_experience);
    if (alternate_email) {
      formData.append("alternate_email", alternate_email);
    }
    if (alternate_phone_number) {
      formData.append("alternate_phone_number", alternate_phone_number);
    }
    if (assessor_status_comment) {
      formData.append("assessor_status_comment", assessor_status_comment);
    }
    // if (
    //   !first_name ||
    //   !last_name ||
    //   !email ||
    //   !phone_number ||
    //   !address ||
    //   !expertise
    // ) {
    //   return;
    // }
    const completeData = {
      first_name: first_name,
      last_name: last_name,
      email: email,
      phone_number: phone_number,
      address: address,
      expertise: expertise,
      agency_id: agencyId,
    };

    if (editAssessorId) {
      dispatch(
        updateAssessorAction(
          editAssessorId,
          formData,
          setAddAssessor,
          setEditAssessorId,
          setEditAssessorData,
          dispatch
        )
      );
    } else {
      dispatch(
        addAssessorAction(
          formData,
          setAddAssessor,
          setEditAssessorId,
          setEditAssessorData,
          dispatch
        )
      );
    }
  };
  return (
    <Box sx={styles.agencyFormContainer}>
      <Grid
        container
        // rowSpacing={}
        columnSpacing={"2rem"}
        sx={commonStyles.signUpContainer}
      >
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="First Name"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="first_name"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("first_name", data?.first_name)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultFirstName}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Last Name"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="last_name"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("last_name", data?.last_name)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultLastName}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Email"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="email"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("email", data?.email)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultEmail}
            disabled={editAssessorId ? true : false}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Alternate Email"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="alternate_email"
            isRequired={false}
            submit={isSubmit}
            setData={(data) =>
              getData("alternate_email", data?.alternate_email)
            }
            boxStyle={customCommonStyles.marginBottomO}
            // defaultValue={defaultAlternateEmail}
            // disabled={editAssessorId ? true : false}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Mobile Number"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="phone_number"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("phone_number", data?.phone_number)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultPhoneNo}
            maxLength={10}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Alternate Mobile Number"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="alternate_phone_number"
            isRequired={false}
            submit={isSubmit}
            setData={(data) =>
              getData("alternate_phone_number", data?.alternate_phone_number)
            }
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultAlternatePhoneNo}
            maxLength={10}
          />
        </Grid>

        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomDropdown
            id="assessor_state"
            label="State"
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            dropdownBoxStyle={customCommonStyles.marginBottomO}
            selectStyle={commonStyles.selectStyle}
            options={stateOptions}
            setValueData={setData}
            defaultValue={data?.assessor_state}
            value={data?.assessor_state}
            name="assessor_state"
            registerName="assessor_state"
            isRequired={true}
            disableSourceForValue={true}
            source="title"
            submit={isSubmit}
            setData={(data) => getData("assessor_state", data?.assessor_state)}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomDropdown
            id="assessor_district"
            label="District"
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            dropdownBoxStyle={customCommonStyles.marginBottomO}
            options={districtOptions}
            selectStyle={commonStyles.selectStyle}
            setValueData={setData}
            defaultValue={data?.assessor_district}
            value={data?.assessor_district}
            name="assessor_district"
            registerName="assessor_district"
            isRequired={true}
            disableSourceForValue={true}
            source="title"
            submit={isSubmit}
            setData={(data) =>
              getData("assessor_district", data?.assessor_district)
            }
          />
          {/* <CustomInput
            label="Base Location"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="address"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("address", data?.address)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultAddress}
          /> */}
        </Grid>

        <Grid item xs={12} sm={12} md={12} lg={12} sx={commonStyles.gridItem}>
          <CustomInput
            label="Highest qualification with specialization"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="expertise"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("expertise", data?.expertise)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultExpertise}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Work Domain"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="work_domain"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("work_domain", data?.work_domain)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultWorkDomain}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Years of experience"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="years_of_experience"
            isRequired={true}
            submit={isSubmit}
            setData={(data) =>
              getData("years_of_experience", data?.years_of_experience)
            }
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultExperienceYear}
          />
        </Grid>

        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomDropdown
            id="assessor_status"
            label="Status"
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            dropdownBoxStyle={customCommonStyles.marginBottomO}
            selectStyle={commonStyles.selectStyle}
            options={assessorStatus}
            setValueData={setData}
            defaultValue={data.assessor_status}
            value={data.assessor_status}
            name="assessor_status"
            registerName="assessor_status"
            isRequired={true}
            disableSourceForValue={true}
            source="label"
            submit={isSubmit}
            setData={(data) =>
              getData("assessor_status", data?.assessor_status)
            }
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Comment"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="assessor_status_comment"
            isRequired={false}
            submit={isSubmit}
            setData={(data) =>
              getData("assessor_status_comment", data?.assessor_status_comment)
            }
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultComment}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12} sx={commonStyles.gridItem}>
          <CustomImageUploader
            file={data.resume}
            value={data.resume}
            setValue={setData}
            displayText="Upload image of resume"
            isLabel={true}
            name="resume"
            accept=".jpg,.jpeg,.png"
            required={false}
            docType="image"
            inputStyle={{
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
              ...commonStyles.assesorResume,
            }}
            fileButtonStyle={commonStyles.assessorFileButtonStyle}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
          />
        </Grid>
      </Grid>
      <Box sx={commonStyles.displayCenterStyle}>
        <SaveNextButtons
          blueButtonText="Save"
          greyButtonText="Cancel"
          onSaveClick={cancelHandler}
          onNextClick={saveHandler}
          buttonContainerStyle={customCommonStyles.marginTopO}
        />
      </Box>
    </Box>
  );
}
