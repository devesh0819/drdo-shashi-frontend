import React from "react";
import { Alert, AlertTitle } from "@mui/material";

export default function CustomMessage(props) {
  const { type, alertTitle, subHeading, text } = props;
  return (
    <Alert severity={type}>
      <AlertTitle>{alertTitle}</AlertTitle>
      <strong>{subHeading}</strong> {text}
    </Alert>
  );
}
