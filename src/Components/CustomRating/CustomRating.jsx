import React from "react";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import Rating from "@mui/material/Rating";
import SentimentVeryDissatisfiedIcon from "@mui/icons-material/SentimentVeryDissatisfied";
import SentimentDissatisfiedIcon from "@mui/icons-material/SentimentDissatisfied";
import SentimentSatisfiedIcon from "@mui/icons-material/SentimentSatisfied";
import SentimentSatisfiedAltIcon from "@mui/icons-material/SentimentSatisfiedAltOutlined";
import SentimentVerySatisfiedIcon from "@mui/icons-material/SentimentVerySatisfied";
import { customCommonStyles } from "../../Styles/CommonStyles";

const StyledRating = styled(Rating)(({ theme }) => ({
  "& .MuiRating-iconEmpty .MuiSvgIcon-root": {
    color: theme.palette.action.disabled,
  },
}));

const customIcons = {
  1: {
    icon: <SentimentVeryDissatisfiedIcon color="error" />,
    label: "Poor",
  },

  2: {
    icon: <SentimentSatisfiedIcon color="warning" />,
    label: "Satisfied",
  },
  3: {
    icon: <SentimentSatisfiedAltIcon color="success" />,
    label: "Good",
  },
  4: {
    icon: <SentimentVerySatisfiedIcon color="success" />,
    label: "Very Good",
  },
};

function IconContainer(props) {
  const { value, ...other } = props;
  return <span {...other}>{customIcons[value].icon}</span>;
}

IconContainer.propTypes = {
  value: PropTypes.number.isRequired,
};

export default function CustomRating(props) {
  const { onChange, defaultValue, readOnly } = props;
  return (
    <>
      <StyledRating
        name="highlight-selected-only"
        defaultValue={defaultValue}
        IconContainerComponent={IconContainer}
        getLabelText={(value) => customIcons[value]?.label}
        highlightSelectedOnly
        sx={customCommonStyles.marginLeftOne}
        max={4}
        onChange={onChange}
        readOnly={readOnly}
      />
    </>
  );
}
