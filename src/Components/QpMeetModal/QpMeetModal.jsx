import {
  Autocomplete,
  Box,
  Grid,
  InputBase,
  MenuItem,
  Select,
  TextareaAutosize,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  meetApplication,
  certificationLevel,
} from "../../Constant/AppConstant";
import * as _ from "lodash";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import QpButton from "../QpButton/QpButton";
import { styles } from "./QpMeetModalStyles";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import QpTypography from "../QpTypography/QpTypography";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import QpRadioButton from "../QpRadioButton/QpRadioButton";
import { getMasterDataAction } from "../../Redux/Admin/GetMasterData/getMasterDataActions";
import { meetModalSchema } from "../../validationSchema/meetModalSchema";
import moment from "moment";
import {
  scheduleMeetingAction,
  schuduleMeetingAction,
  updateMeetingAction,
} from "../../Redux/Admin/Member/memberActions";
import { useNavigate, useParams } from "react-router-dom";
import SaveNextButtons from "../SaveNextButtons/SaveNextButtons";
import CustomDateTime from "../CustomDateTime/CustomDateTime";
import * as dayjs from "dayjs";
import { showToast } from "../Toast/Toast";

const QpMeetModal = (props) => {
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
    getValues,
  } = useForm({
    resolver: yupResolver(meetModalSchema),
    mode: "onChange",
  });

  const [meeting_type, setMeetingType] = useState(meetApplication[0].value);
  const [selectedAssessments, setSelectedAssessments] = useState([]);
  const currentDate = new Date();
  // const dateTime = currentDate.setDate(currentDate.getDate() + 1);
  const dateTime = currentDate.setDate(currentDate.getDate());

  const tempMinDateTime = new Date(dateTime).setHours(9);
  const minDateTime = new Date(tempMinDateTime).setMinutes(0);
  const [selectedDateTime, setSelectedDateTime] = useState(minDateTime);
  const [defaultSelectedAssessments, setDefaultSelectedAssessments] = useState(
    []
  );
  const [onlineText, setOnlineText] = useState();
  const [offlineText, setOfflineText] = useState();

  const { editMeet, closeModal } = props;

  const assessmentsList = useSelector(
    (state) => state.getMasterData.masterData
  );
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    dispatch(getMasterDataAction({ type: "assessments" }));
  }, [dispatch]);

  useEffect(() => {
    if (selectedAssessments?.length > 0) {
      setValue("meeting_assessments", selectedAssessments, {
        shouldValidate: true,
      });
    }
  }, [selectedAssessments]);

  const handleInputChange = (event) => {
    setMeetingType(event.target.value);
    setValue("meeting_type", event.target.value);
  };

  const handleCancel = () => {
    closeModal(false);
  };

  const handleAssessmentChange = (event, assessments) => {
    const newAssessments = assessments.map((option) => option.id || option);
    setSelectedAssessments(newAssessments);
    setValue("meeting_assessments", newAssessments, {
      shouldValidate: true,
    });
    setDefaultSelectedAssessments(assessments);
  };

  const onSubmit = (data) => {
    if (meeting_type === "online" && !data.online_text) {
      showToast("Please enter meeting link", "error");
      return;
    }
    if (meeting_type === "offline" && !data.offline_text) {
      showToast("Please enter address", "error");
      return;
    }

    let completeData = {
      meeting_title: data.meeting_title,
      meeting_assessments: data.meeting_assessments,
      description: data.description,
      meeting_type: meeting_type,
    };
    if (selectedDateTime) {
      completeData["meeting_date_time"] = dayjs(
        new Date(selectedDateTime)
      ).format("D MMM YYYY h:mm");
    }
    if (meeting_type === "online" && data.online_text) {
      completeData["meeting_link"] = data.online_text;
    }
    if (meeting_type === "offline" && data.offline_text) {
      completeData["meeting_address"] = data.offline_text;
    }
    // completeData["meeting_application"] = "Zoom";
    if (editMeet && editMeet?.uuid) {
      dispatch(
        updateMeetingAction(
          completeData,
          editMeet?.uuid,
          navigate,
          closeModal,
          id
        )
      );
    } else
      dispatch(scheduleMeetingAction(completeData, id, navigate, closeModal));

    // props.closeModal(false);

    // props.closeModal(false);
  };

  useEffect(() => {
    if (editMeet?.meeting_title) {
      setValue("meeting_title", editMeet?.meeting_title, {
        shouldValidate: true,
      });
    }
    if (editMeet?.description) {
      setValue("description", editMeet?.description, {
        shouldValidate: true,
      });
    }
    if (editMeet?.meeting_date_time) {
      setSelectedDateTime(editMeet?.meeting_date_time);
      setValue("meeting_date_time", editMeet?.meeting_date_time, {
        shouldValidate: true,
      });
    }

    if (editMeet?.meeting_link) {
      setValue("online_text", editMeet?.meeting_link, {
        shouldValidate: true,
      });
    }
    if (editMeet?.meeting_address) {
      setValue("offline_text", editMeet?.meeting_address, {
        shouldValidate: true,
      });
    }
    if (editMeet?.meeting_type) {
      setMeetingType(editMeet?.meeting_type);
    }
  }, [editMeet]);

  useEffect(() => {
    if (editMeet?.meeting_assessments && assessmentsList?.data?.length > 0) {
      setValue("meeting_assessments", editMeet?.meeting_assessments, {
        shouldValidate: true,
      });
      setSelectedAssessments(editMeet?.meeting_assessments);
    }
  }, [editMeet?.meeting_assessments, assessmentsList?.data]);

  useEffect(() => {
    if (props.defaultAssessments?.length > 0) {
      setDefaultSelectedAssessments(props.defaultAssessments);
    }
  }, [props.defaultAssessments]);

  return (
    <Box sx={styles.formContainer}>
      <Grid container sx={commonStyles.displayCenterStyle}>
        <Grid xs={12} item sx={customCommonStyles.padding0}>
          <InputBase
            id="meeting_title"
            name="meeting_title"
            required
            placeholder="Title"
            // sx={{ ...commonStyles.inputStyle }}
            sx={commonStyles.textInputStyle}
            {...register("meeting_title")}
            error={errors.meeting_title ? true : false}
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.meeting_title?.message}
          />
        </Grid>
        <Grid xs={12} item sx={customCommonStyles.padding0}>
          <Autocomplete
            multiple
            filterSelectedOptions
            id="tags-outlined"
            options={assessmentsList?.data || []}
            onChange={(event, assessments) =>
              handleAssessmentChange(event, assessments)
            }
            getOptionLabel={(option) => option?.title}
            value={defaultSelectedAssessments}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                placeholder="Select Assessments"
              />
            )}
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.meeting_assessments?.message}
          />
        </Grid>
        <Grid xs={12} item sx={customCommonStyles.padding0}>
          <InputBase
            // maxRows={7}
            multiline
            id="description"
            name="description"
            // required
            placeholder="Description"
            sx={{
              ...commonStyles.textInputStyle,
              height: "3.8rem",
              display: "flex",
              alignItems: "start",
              paddingRight: "0",
              // overflow: "auto",
              "& .MuiInputBase-input": {
                height: "3rem !important",
                overflow: "auto !important",
                ...commonStyles.customScrollBar,
              },
            }}
            // inputProps={{
            //   height: "7rem",
            //   overflow: "auto",
            // }}
            {...register("description")}
            error={errors.description ? true : false}
            // autoFocus="off"
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.description?.message}
          />
        </Grid>

        <Grid xs={12} item sx={customCommonStyles.padding0}>
          <QpInputLabel
            displayText="Meeting Type"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <QpRadioButton
            options={meetApplication}
            name="meeting_type"
            onChange={handleInputChange}
            value={meeting_type}
          />
          {meeting_type === "online" && (
            <>
              <InputBase
                id="online"
                name="online_text"
                type="text"
                placeholder="Enter meeting link"
                required
                // sx={{ ...commonStyles.inputStyle }}
                // onChange={(event) => setOnlineText(event.target.value)}
                sx={commonStyles.textInputStyle}
                {...register("online_text")}
                error={errors.online_text ? true : false}
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={errors.online_text?.message}
              />
            </>
          )}
          {meeting_type === "offline" && (
            <>
              <InputBase
                id="offline"
                name="offline_text"
                type="text"
                placeholder="Enter address"
                required
                // sx={{ ...commonStyles.inputStyle }}
                sx={commonStyles.textInputStyle}
                // onChange={(event) => setOfflineText(event.target.value)}
                {...register("offline_text")}
                error={errors.offline_text ? true : false}
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={errors.offline_text?.message}
              />
            </>
          )}
        </Grid>
        <Grid xs={12} item sx={styles.inputWrapper}>
          <QpInputLabel
            displayText="Select date & time"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <CustomDateTime
            minDateTime={minDateTime}
            selectedDateTime={selectedDateTime}
            setSelectedDateTime={setSelectedDateTime}
            textAreaStyle={{ width: "100%" }}
          />

          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.meeting_date_time?.message}
          />
        </Grid>
        <SaveNextButtons
          blueButtonText="Save"
          greyButtonText="Cancel"
          onSaveClick={handleCancel}
          onNextClick={handleSubmit(onSubmit)}
        />
      </Grid>
    </Box>
  );
};

export default QpMeetModal;
