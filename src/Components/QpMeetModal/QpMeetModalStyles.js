import { BLACKISH } from "../../Constant/ColorConstant";
import { commonStyles } from "../../Styles/CommonStyles";
export const styles = {
  titleInput: {
    height: "2rem",
  },
  formContainer: {
    flexDirection: "column",
    padding: "0rem 2.5rem",
    paddingBottom: "1rem",
    marginTop: "1rem",
    overflow: "scroll",
    ...commonStyles.customScrollBar,
  },

  inputWrapper: {
    // paddingRight: "1rem",
    // marginRight: "2rem",
  },
  buttonContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "1rem",
  },
  select: {
    width: "100%",
    height: "2.813rem",
    border: "0.063rem solid #A9A9A9",
    borderRadius: 0,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.192rem",
    color: BLACKISH,
    // marginTop: "0.738rem",
    "@media(max-width:600px)": {
      height: "1.8rem",
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  saveButton: {
    background: "#1FBAED",
    borderRadius: "50px",
    textTransform: "none",
    width: "9rem",
    height: "3rem",
    marginRight: "2rem",
    marginLeft: "9rem",
    "&:hover": {
      background: "#1FBAED",
    },
  },
  cancelButton: {
    textTransform: "none",
    background: "#D8DCE0",
    borderRadius: "50px",
    height: "3rem",
    width: "9rem",
    "&:hover": {
      background: "#D8DCE0",
    },
  },
};
