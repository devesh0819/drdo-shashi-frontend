import { BLACK } from "../../Constant/ColorConstant";

export const styles = {
  dialog: {
    height: "37.813rem",
    width: "48.75rem",
    margin: "auto",
    color: (theme) => theme.palette.grey[500],
    "@media (max-width: 600px)": {
      maxWidth: "90vw",
      maxHeight: "95vh",
    },
  },
  title: {
    fontSize: "1.125rem",
    lineHeight: "3.5rem",
    height: "3.5rem",
    borderBottom: "0.15rem solid #F4F4F4",
    fontWeight: "700",
    paddingLeft: "2.5rem",
    color: BLACK,
  },
  closeIcon: {
    position: "absolute",
    right: 40,
    top: 20,
    color: BLACK,
    cursor: "pointer",
  },
};
