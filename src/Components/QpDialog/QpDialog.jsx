import { Dialog, Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

import { styles } from "./QpDialogStyles";

const QpDialog = ({
  children,
  open,
  closeModal,
  title,
  styleData,
  dialogSize,
  getAssessorDetail,
  dispatch,
  setEditAssessorId,
}) => {
  const handleCloseModal = () => {
    closeModal(false);
    if (getAssessorDetail) {
      dispatch(getAssessorDetail({}));
      setEditAssessorId("");
    }
  };

  return (
    <Dialog
      fullScreen={true}
      maxWidth={dialogSize ? dialogSize : "sm"}
      open={open}
      sx={{ ...styles.dialog, ...styleData }}
    >
      <Typography sx={styles.title}>{title}</Typography>
      <Typography onClick={handleCloseModal}>
        <CloseIcon sx={styles.closeIcon} />
      </Typography>
      {children}
    </Dialog>
  );
};

export default QpDialog;
