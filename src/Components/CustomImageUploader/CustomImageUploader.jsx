import React, { useState, useEffect } from "react";
import { Box } from "@mui/material";
import { commonStyles } from "../../Styles/CommonStyles";
import QpDocUploader from "../QpDocUploader/QpDocUploader";
import QpTypography from "../QpTypography/QpTypography";
import DeleteIcon from "@mui/icons-material/Delete";
import { styles } from "./CustomImageUploaderStyles.js";
import QpInputLabel from "../QpInputLabel/QpInputLabel";

export default function CustomImageUploader(props) {
  const {
    file,
    setValue,
    docType,
    name,
    inputStyle,
    fileButtonStyle,
    labelStyleData,
    typeText,
  } = props;
  const [particularFile, setParticularFile] = useState();

  useEffect(() => {
    if (file) {
      setParticularFile(file);
    }
  }, [file]);

  const handleDelete = () => {
    props.setValue((values) => ({
      ...values,
      [props.name]: "",
    }));
    setParticularFile("");
  };
  return (
    <>
      <Box
        sx={{
          ...commonStyles.displayStyle,
          ...styles.imageUploadTextContent,
        }}
      >
        <Box>
          <QpInputLabel
            displayText={props.displayText}
            styleData={labelStyleData || commonStyles.label}
            required={props.required}
          />
          <QpTypography
            displayText={typeText}
            styleData={styles.typeTextStyle}
          />
        </Box>

        {particularFile ? (
          <Box sx={commonStyles.displayFlex}>
            <QpTypography displayText={file?.name} />
            <DeleteIcon
              onClick={handleDelete}
              sx={commonStyles.cursorPointer}
            />
          </Box>
        ) : (
          <QpDocUploader
            file={particularFile}
            value={props.value}
            setValue={setValue}
            multiple={true}
            name={props.name}
            accept={props.accept}
            docType={docType}
            inputStyle={inputStyle}
            fileButtonStyle={fileButtonStyle}
          />
        )}
      </Box>
    </>
  );
}
