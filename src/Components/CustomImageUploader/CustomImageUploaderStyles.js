export const styles = {
  imageUploadTextContent: {
    marginBottom: "2rem",
    "@media(max-width: 600px)": {
      flexDirection: "column",
      alignItems: "start",
    },
  },
  typeTextStyle: {
    fontSize: "0.75rem",
    color: "black",
  },
};
