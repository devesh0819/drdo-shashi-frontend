/* eslint-disable prefer-const */
import { useState, useEffect } from "react";
import {
  Box,
  Divider,
  Grid,
  InputBase,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";
import { styles } from "./FiltersStyles";
import { commonStyles } from "../../Styles/CommonStyles";
import SaveNextButtons from "../SaveNextButtons/SaveNextButtons";
import DropdownFilter from "../AllFilters/DropdownFilter";
import DateRangeFilter from "../AllFilters/DateRangeFilter";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
import QpInputBase from "../QpInputBase/QpInputBase";
import dayjs from "dayjs";
import LocationFilter from "../AllFilters/LocationFilter";

const FilterComponent = (props) => {
  const {
    filterOptions,
    saveHandler,
    alreadySelectedFilters,
    setOpenFilterDialog,
    setNoOfFiltersApplied,
    setAlreadySelectedFilters,
    stateValue,
    districtArray,
    setState,
  } = props;
  const [activeFilter, setActiveFilter] = useState(filterOptions?.[0]);
  const [selectedFilters, setSelectedFilters] = useState(
    alreadySelectedFilters
  );

  const findFiltersApplied = (payload) => {
    let count = Object.keys(payload).length;
    filterOptions?.forEach((option) => {
      if (
        (option.filterType === "Date" ||
          option.filterType === "AssessmentDate") &&
        payload[option?.keysArray[0]]
      ) {
        count--;
      }
    });
    return count;
  };

  const applyHandler = () => {
    setOpenFilterDialog(false);
    setAlreadySelectedFilters(selectedFilters);
    let payloadCopy = { ...selectedFilters };

    filterOptions.forEach((option) => {
      if (
        option?.filterType === "Date" ||
        option?.filterType === "AssessmentDate"
      ) {
        payloadCopy[option?.keysArray[0]] =
          selectedFilters[option?.keysArray[0]] &&
          dayjs(selectedFilters[option?.keysArray[0]]).format("YYYY-MM-DD");
        payloadCopy[option?.keysArray[1]] =
          selectedFilters[option?.keysArray[1]] &&
          dayjs(selectedFilters[option?.keysArray[1]]).format("YYYY-MM-DD");
      }
      if (option?.filterType === "Dropdown") {
        payloadCopy[option.key] = selectedFilters[option.key]?.value;
      }
      if (option?.filterType === "Location") {
        payloadCopy[option.key] = selectedFilters[option.key]?.id;
      }
      if (option?.filterType === "Search") {
        if (payloadCopy[option.key]?.length === 0) {
          delete payloadCopy[option.key];
          delete selectedFilters[option.key];
        }
      }
    });
    setNoOfFiltersApplied(findFiltersApplied(selectedFilters));
    saveHandler(payloadCopy);
  };

  const resetHandler = () => {
    setSelectedFilters({});
    setOpenFilterDialog(false);
    setAlreadySelectedFilters({});
    setNoOfFiltersApplied(findFiltersApplied({}));
    saveHandler({});
    if (selectedFilters?.location) {
      setState("");
    }
  };

  const handleFilterClick = (item) => {
    setActiveFilter(item);
  };

  const handleDropdownChange = (e, activeFilter) => {
    setSelectedFilters((prev) => {
      return {
        ...prev,
        [activeFilter?.key]: e.target.value,
      };
    });
  };
  const findEndDate = (startDate, activeFilter) => {
    if (
      selectedFilters?.[activeFilter?.keysArray[1]] ||
      new Date() - startDate < 0
    ) {
      return startDate;
    } else return new Date();
  };

  const handleStartDateChange = (newVal, activeFilter) => {
    setSelectedFilters((prev) => {
      return {
        ...prev,
        [activeFilter?.keysArray[0]]: newVal.$d,
        [activeFilter?.keysArray[1]]:
          selectedFilters?.[activeFilter?.keysArray[1]] ||
          findEndDate(newVal.$d, activeFilter),
      };
    });
  };
  const handleEndDateChange = (newVal, activeFilter) => {
    setSelectedFilters((prev) => {
      return {
        ...prev,
        [activeFilter?.keysArray[1]]: newVal.$d,
      };
    });
  };
  const handleInputChange = (event, activeFilter) => {
    setSelectedFilters((prev) => {
      return {
        ...prev,
        [activeFilter?.key]: event.target.value,
      };
    });
  };

  const handleStateDropdownChange = (e) => {
    setState(e.target.value);
  };

  const handleDistrictDropdownChange = (e, activeFilter) => {
    setSelectedFilters((prev) => {
      return {
        ...prev,
        [activeFilter?.key]: e.target.value,
      };
    });
  };

  const getActiveFilter = () => {
    switch (activeFilter?.filterType) {
      case "Date":
        return (
          <DateRangeFilter
            startDate={selectedFilters?.[activeFilter?.keysArray[0]]}
            endDate={selectedFilters?.[activeFilter?.keysArray[1]]}
            handleStartDateChange={handleStartDateChange}
            handleEndDateChange={handleEndDateChange}
          />
        );
      case "AssessmentDate":
        return (
          <DateRangeFilter
            startDate={selectedFilters?.[activeFilter?.keysArray[0]]}
            endDate={selectedFilters?.[activeFilter?.keysArray[1]]}
            handleStartDateChange={handleStartDateChange}
            handleEndDateChange={handleEndDateChange}
            isAssessmentDate={true}
          />
        );
      case "Dropdown":
        return (
          <DropdownFilter
            labelId={activeFilter?.labelId}
            id={activeFilter?.labelId}
            value={selectedFilters?.[activeFilter?.key] || ""}
            handleDropdownChange={handleDropdownChange}
            dataArray={activeFilter?.dataArray}
            label={activeFilter?.label}
            isLocation={true}
            source="label"
            dropdownStyle={styles.dropdownStyle}
          />
        );
      case "Search":
        return (
          <QpInputBase
            styleData={{
              ...styles.inputBaseStyle,
              ...commonStyles.fullWidth,
            }}
            // onKeyPress={handleInputChange}
            placeholder={activeFilter?.label}
            size="small"
            value={selectedFilters?.[activeFilter?.key] || ""}
            onChange={handleInputChange}
          />
        );
      case "Location":
        return (
          <LocationFilter
            labelId={activeFilter?.labelId}
            stateValue={stateValue || ""}
            districtValue={selectedFilters?.[activeFilter?.key] || ""}
            handleStateDropdownChange={handleStateDropdownChange}
            handleDistrictDropdownChange={handleDistrictDropdownChange}
            stateArray={activeFilter?.stateArray}
            districtArray={districtArray}
            labelIdDistrict={activeFilter?.labelIdDistrict}
            labelDistrict={activeFilter?.labelDistrict}
            label={activeFilter?.label}
          />
        );
      default:
        return null;
    }
  };

  return (
    <Grid
      container
      columnSpacing={"0.5rem"}
      rowSpacing={"0.5rem"}
      sx={styles.filtersGrid}
    >
      {filterOptions?.map((filter) => {
        return (
          <>
            {filter?.filterName === "applicationNumber" && (
              <Grid item key={filter}>
                <QpInputBase
                  styleData={{
                    ...styles.inputBaseStyle,
                    // ...commonStyles.fullWidth,
                  }}
                  // onKeyPress={handleInputChange}
                  placeholder={filter?.label}
                  size="small"
                  value={selectedFilters?.[filter?.key] || ""}
                  onChange={(event) => handleInputChange(event, filter)}
                />
              </Grid>
            )}
            {filter?.filterName === "enterpriseName" && (
              <Grid item>
                <QpInputBase
                  styleData={{
                    ...styles.inputBaseStyle,
                    // ...commonStyles.fullWidth,
                  }}
                  // onKeyPress={handleInputChange}
                  placeholder={filter?.label}
                  size="small"
                  value={selectedFilters?.[filter?.key] || ""}
                  // onChange={handleInputChange}
                  onChange={(event) => handleInputChange(event, filter)}
                />
              </Grid>
            )}
            {filter?.filterName === "assessorName" && (
              <Grid item>
                <QpInputBase
                  styleData={{
                    ...styles.inputBaseStyle,
                    // ...commonStyles.fullWidth,
                  }}
                  // onKeyPress={handleInputChange}
                  placeholder={filter?.label}
                  size="small"
                  value={selectedFilters?.[filter?.key] || ""}
                  // onChange={handleInputChange}
                  onChange={(event) => handleInputChange(event, filter)}
                />
              </Grid>
            )}
            {filter?.filterName === "workDomain" && (
              <Grid item>
                <QpInputBase
                  styleData={{
                    ...styles.inputBaseStyle,
                    // ...commonStyles.fullWidth,
                  }}
                  // onKeyPress={handleInputChange}
                  placeholder={filter?.label}
                  size="small"
                  value={selectedFilters?.[filter?.key] || ""}
                  // onChange={handleInputChange}
                  onChange={(event) => handleInputChange(event, filter)}
                />
              </Grid>
            )}
            {filter?.filterName === "enterpriseType" && (
              <Grid item>
                <DropdownFilter
                  labelId={filter?.labelId}
                  id={filter?.labelId}
                  value={selectedFilters?.[filter?.key] || ""}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(event, filter)
                  }
                  dataArray={filter?.dataArray}
                  label={filter?.label}
                  isLocation={true}
                  source="label"
                  dropdownStyle={styles.dropdownStyle}
                />
              </Grid>
            )}
            {filter?.filterName === "assessmentStatus" && (
              <Grid item>
                <DropdownFilter
                  labelId={filter?.labelId}
                  id={filter?.labelId}
                  value={selectedFilters?.[filter?.key] || ""}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(event, filter)
                  }
                  dataArray={filter?.dataArray}
                  label={filter?.label}
                  isLocation={true}
                  source="label"
                  dropdownStyle={styles.dropdownStyle}
                />
              </Grid>
            )}
            {filter?.filterName === "subsidy" && (
              <Grid item>
                <DropdownFilter
                  labelId={filter?.labelId}
                  id={filter?.labelId}
                  value={selectedFilters?.[filter?.key] || ""}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(event, filter)
                  }
                  dataArray={filter?.dataArray}
                  label={filter?.label}
                  isLocation={true}
                  source="label"
                  dropdownStyle={styles.dropdownStyle}
                />
              </Grid>
            )}
            {filter?.filterName === "feedback" && (
              <Grid item>
                <DropdownFilter
                  labelId={filter?.labelId}
                  id={filter?.labelId}
                  value={selectedFilters?.[filter?.key] || ""}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(event, filter)
                  }
                  dataArray={filter?.dataArray}
                  label={filter?.label}
                  isLocation={true}
                  source="label"
                  dropdownStyle={styles.dropdownStyle}
                />
              </Grid>
            )}
            {filter?.filterName === "applicationDate" && (
              <Grid item>
                <DateRangeFilter
                  startDate={selectedFilters?.[filter?.keysArray[0]]}
                  endDate={selectedFilters?.[filter?.keysArray[1]]}
                  handleStartDateChange={(newVal) =>
                    handleStartDateChange(newVal, filter)
                  }
                  handleEndDateChange={(newVal) =>
                    handleEndDateChange(newVal, filter)
                  }
                  displayStartName={filter?.displayStartName}
                  displayLastName={filter?.displayLastName}
                />
              </Grid>
            )}
            {filter?.filterName === "paymentStatus" && (
              <Grid item>
                <DropdownFilter
                  labelId={filter?.labelId}
                  id={filter?.labelId}
                  value={selectedFilters?.[filter?.key] || ""}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(event, filter)
                  }
                  dataArray={filter?.dataArray}
                  label={filter?.label}
                  isLocation={true}
                  source="label"
                  dropdownStyle={styles.dropdownStyle}
                />
              </Grid>
            )}
            {filter?.filterName === "applicationStatus" && (
              <Grid item>
                <DropdownFilter
                  labelId={filter?.labelId}
                  id={filter?.labelId}
                  value={selectedFilters?.[filter?.key] || ""}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(event, filter)
                  }
                  dataArray={filter?.dataArray}
                  label={filter?.label}
                  isLocation={true}
                  source="label"
                  dropdownStyle={styles.dropdownStyle}
                />
              </Grid>
            )}
            {filter?.filterName === "assessmentDate" && (
              <Grid item>
                <DateRangeFilter
                  startDate={selectedFilters?.[filter?.keysArray[0]]}
                  endDate={selectedFilters?.[filter?.keysArray[1]]}
                  handleStartDateChange={(newVal) =>
                    handleStartDateChange(newVal, filter)
                  }
                  handleEndDateChange={(newVal) =>
                    handleEndDateChange(newVal, filter)
                  }
                  displayStartName={filter?.displayStartName}
                  displayLastName={filter?.displayLastName}
                />
              </Grid>
            )}
            {filter?.filterName === "rating" && (
              <Grid item>
                <DropdownFilter
                  labelId={filter?.labelId}
                  id={filter?.labelId}
                  value={selectedFilters?.[filter?.key] || ""}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(event, filter)
                  }
                  dataArray={filter?.dataArray}
                  label={filter?.label}
                  isLocation={true}
                  source="label"
                  dropdownStyle={styles.dropdownStyle}
                />
              </Grid>
            )}
            {filter?.filterName === "stateDistrict" && (
              <Grid item>
                <LocationFilter
                  labelId={filter?.labelId}
                  stateValue={stateValue || ""}
                  districtValue={selectedFilters?.[filter?.key] || ""}
                  handleStateDropdownChange={handleStateDropdownChange}
                  handleDistrictDropdownChange={(event) =>
                    handleDistrictDropdownChange(event, filter)
                  }
                  stateArray={filter?.stateArray}
                  districtArray={districtArray}
                  labelIdDistrict={filter?.labelIdDistrict}
                  labelDistrict={filter?.labelDistrict}
                  label={filter?.label}
                />
              </Grid>
            )}
          </>
        );
      })}
      <Grid item>
        <SaveNextButtons
          blueButtonText="Apply"
          greyButtonText="Reset"
          onSaveClick={resetHandler}
          onNextClick={applyHandler}
          buttonContainerStyle={commonStyles.customButtonConatinerStyle}
          greyButtonStyle={commonStyles.greyButtonStyle}
          blueButtonStyle={commonStyles.blueButtonStyle}
          blueButtonTextStyle={commonStyles.buttonTextStyle}
          greyButtonTextStyle={commonStyles.buttonTextStyle}
        />
      </Grid>
    </Grid>
    // <Box sx={commonStyles.filterBox}>
    //   <Box sx={styles.mainComponent}>
    //     <Box sx={styles.innerMainComponent}>
    //       <Box sx={styles.innerLeftContainer}>
    //         <Box sx={styles.innerScrollContainer}>{getActiveFilter()}</Box>
    //       </Box>
    //       <Box sx={styles.innerCenterContainer}>
    //         <Divider orientation="vertical" sx={styles.dividerStyle} />
    //       </Box>
    //       <Box sx={styles.innerRightContainer}>
    //         {filterOptions?.map((option) => {
    //           return (
    //             <Box
    //               sx={{
    //                 ...styles.filterList,
    //                 ...(option?.filterId === activeFilter?.filterId &&
    //                   styles.activeFilter),
    //               }}
    //               key={option?.filterId}
    //               onClick={() => handleFilterClick(option)}
    //             >
    //               <Typography
    //                 sx={{
    //                   ...styles.filterListText,
    //                   ...(option?.filterId === activeFilter?.filterId &&
    //                     styles.activeFilterListText),
    //                 }}
    //               >
    //                 {option?.displayName}
    //               </Typography>
    //               {selectedFilters?.[option?.key] && (
    //                 <FiberManualRecordIcon
    //                   htmlColor={styles.recordIconColor.color}
    //                   fontSize={styles.recordIconSize.fontSize}
    //                 />
    //               )}
    //               {option?.keysArray &&
    //                 selectedFilters?.[option?.keysArray[0]] && (
    //                   <FiberManualRecordIcon
    //                     htmlColor={styles.recordIconColor.color}
    //                     fontSize={styles.recordIconSize.fontSize}
    //                   />
    //                 )}
    //             </Box>
    //           );
    //         })}
    //       </Box>
    //     </Box>
    //     <Box sx={styles.lowerMainComponent}>
    //       <SaveNextButtons
    //         blueButtonText="Apply"
    //         greyButtonText="Reset"
    //         onSaveClick={resetHandler}
    //         onNextClick={applyHandler}
    //         buttonContainerStyle={commonStyles.customButtonConatinerStyle}
    //         greyButtonStyle={commonStyles.greyButtonStyle}
    //         blueButtonStyle={commonStyles.blueButtonStyle}
    //         blueButtonTextStyle={commonStyles.buttonTextStyle}
    //         greyButtonTextStyle={commonStyles.buttonTextStyle}
    //       />
    //     </Box>
    //   </Box>
    // </Box>
  );
};

export default FilterComponent;
