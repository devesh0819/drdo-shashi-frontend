import { WHITE, BLUE, GREY } from "../../Constant/ColorConstant";
import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  listnerContainer: {
    height: "100%",
  },
  listenerBox: {
    height: "100%",
    position: "relative",
  },
  innerAbsoluteContainer: {
    position: "absolute",
    zIndex: "3",
    top: "50px",
    right: "0px",
  },
  buttonStyle: {
    height: "40px",
  },
  mainComponent: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    minHeight: "200px",
    minWidth: "350px",
  },
  dropdownStyle: {
    width: "10.5rem",
    borderRadius: "4px",
    height: "2.45rem",
  },
  innerMainComponent: {
    display: "flex",
    justifyContent: "space-between",
  },
  innerLeftContainer: {
    margin: "10px",
    width: "55%",
  },
  innerCenterContainer: {
    width: "5%",
  },
  innerRightContainer: {
    margin: "10px",
    width: "40%",
    maxHeight: "200px",
    overflowY: "auto",
  },
  dividerStyle: {
    borderColor: GREY,
    marginTop: "20px",
    borderLeftWidth: "2px",
    borderRightWidth: "0",
    height: "190px",
  },
  innerScrollContainer: {
    width: "200px",
    // overflowY: "auto",
    maxHeight: "210px",
    ...commonStyles.scrollBar,
  },
  lowerMainComponent: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "end",
    gap: "10px",
    marginBottom: "20px",
    marginRight: "20px",
    height: "100%",
  },
  filterList: {
    display: "flex",
    justifyContent: "space-between",
    margin: "3px",
    borderRadius: "5px",
    cursor: "pointer",
    alignItems: "center",
    paddingLeft: "5px",
    paddingRight: "5px",
    paddingTop: "2px",
    paddingBottom: "2px",
  },
  filterListText: {
    marginLeft: "1px",
    fontSize: "0.8rem",
  },
  activeFilterListText: {
    color: WHITE,
  },
  activeFilter: {
    backgroundColor: BLUE,
  },
  saveButton: {
    background: "#1FBAED",
    borderRadius: "50px",
    textTransform: "none",
    width: "9rem",
    height: "3rem",
    marginRight: "2rem",
    marginLeft: "9rem",
    "&:hover": {
      background: "#1FBAED",
    },
  },
  inputBaseStyle: {
    // width: "50%",
    width: "10.5rem",
    // marginBottom: "0.5rem",
    height: "2.35rem",
    border: "0.063rem solid #A9A9A9",
    paddingX: "0.5rem",
    fontSize: "0.875rem",
    borderRadius: "4px",
  },
  filtersGrid: {
    paddingX: "2.063rem",
  },
  recordIconColor: { color: "rgb(95,197,249)" },
  recordIconSize: {
    fontSize: "1px",
  },
};
