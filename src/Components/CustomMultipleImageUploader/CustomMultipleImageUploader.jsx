import React, { useState, useEffect } from "react";

import * as _ from "lodash";
import QpDocUploader from "../QpDocUploader/QpDocUploader";
import CustomImageDisplay from "../CustomImageDisplay/CustomImageDisplay";
import { Box } from "@mui/material";
import { commonStyles } from "../../Styles/CommonStyles";
import QpTypography from "../QpTypography/QpTypography";
import { deleteImageAction } from "../../Redux/ApplicantForm/applicantFormActions";
import { useDispatch } from "react-redux";
import { showToast } from "../Toast/Toast";

export default function CustomMultipleImageUploader(props) {
  const {
    setValue,
    setNewDocArray,
    value,
    name,
    displayText,
    styleData,
    docType,
    accept,
  } = props;
  const [document, setDocument] = useState({ document: "" });
  const [docArray, setDocArray] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    if (value?.length > 0) {
      setDocArray(value);
    }
  }, [value]);

  useEffect(() => {
    if (document?.document) {
      const newDocArray = [...docArray];
      if (newDocArray?.length === 10) {
        showToast("Only 10 files can be uploaded", "error");
        return;
      }
      newDocArray.push(document?.document);
      setNewDocArray((prevState) => [...prevState, document?.document]);
      setDocArray((prevState) => [...prevState, document?.document]);
      // setValue((values) => ({
      //   ...values,
      //   [name]: newDocArray,
      // }));
    }
  }, [document?.document]);

  const handleImageDelete = (deleteIndex) => {
    const deleteUuid = docArray[deleteIndex];
    const newDocArray = docArray.filter((doc, index) => index !== deleteIndex);
    setDocArray(newDocArray);
    setNewDocArray(newDocArray);
    // setValue((values) => ({
    //   ...values,
    //   [name]: newDocArray,
    // }));
    if (!deleteUuid?.type) {
      dispatch(deleteImageAction(deleteUuid?.name));
    }
  };

  return (
    <Box
      sx={{
        ...commonStyles.displayStyle,
        ...commonStyles.multipleImageUpload,
      }}
    >
      <QpTypography displayText={displayText} styleData={styleData} />
      <Box>
        {docArray.map((doc, index) => {
          return (
            <CustomImageDisplay
              file={doc}
              key={index}
              index={index}
              onDelete={() => handleImageDelete(index)}
              isLabel={index === 0 ? true : false}
              displayText={displayText}
            />
          );
        })}
        <QpDocUploader
          file={document}
          setValue={setDocument}
          name="document"
          accept={accept}
          docType={docType}
        />
      </Box>
    </Box>
  );
}
