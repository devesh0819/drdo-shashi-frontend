import { React } from "react";
import Button from "@mui/material/Button";
import { Typography } from "@mui/material";

export default function QpButton(props) {
  return (
    <Button
      variant={props.variant || "contained"}
      sx={props.styleData}
      disableRipple={true}
      onClick={props.onClick}
      disabled={props.isDisable || false}
    >
      <Typography sx={props.textStyle}>{props.displayText}</Typography>
    </Button>
  );
}
