import React from "react";
import { useNavigate } from "react-router-dom";
import { Box } from "@mui/material";
import verifiedTick from "../../Assets/Images/greenCircleTick.png";
import { LOGIN } from "../../Routes/Routes";
import QpButton from "../QpButton/QpButton";
import QpTypography from "../QpTypography/QpTypography";
import { commonStyles } from "../../Styles/CommonStyles";
import { getDefaultPath } from "../../Services/commonService";
import { getUserData } from "../../Services/localStorageService";

export default function QpOtpVerified() {
  const navigate = useNavigate();
  const userData = getUserData();
  return (
    <Box sx={commonStyles.verifiedBoxContainer}>
      <Box>
        <img
          src={verifiedTick}
          alt="verifiedTick"
          width="105px"
          height="105px"
        />
      </Box>
      <QpTypography
        displayText="Thank You"
        styleData={commonStyles.enterOtpText}
      />
      <QpTypography
        displayText="Your registration verification has been completed successfully. The login credentials have been sent to your registered email ID."
        styleData={{
          ...commonStyles.verifyOtpText,
          ...commonStyles.registrationText,
        }}
      />
      <QpButton
        displayText="Go to Homepage"
        styleData={{
          ...commonStyles.validateButton,
          ...commonStyles.goToHomepageText,
        }}
        onClick={() => navigate(getDefaultPath(userData?.roles[0]))}
        textStyle={commonStyles.validateButtontext}
      />
    </Box>
  );
}
