import React from "react";
import InputBase from "@mui/material/InputBase";
import { FormHelperText } from "@mui/material";
import { commonStyles } from "../../Styles/CommonStyles";

const style = {
  error: {
    position: "absolute",
    top: "3.75rem",
  },
};
const QpInputBase = (props) => (
  <>
    <InputBase
      sx={{ ...props.styleData, ...commonStyles.relativePosition }}
      onChange={props.onChange}
      inputProps={{ maxLength: props.maxLen }}
      type={props.type || "text"}
      name={props.name}
      value={props.value}
      placeholder={props.placeholder}
      fullWidth={props.fullWidth}
      inputRef={props.inputRef}
      endAdornment={props.endAdornment}
      onKeyDown={
        props.onKeyPress ? (event) => props.onKeyPress(event) : () => {}
      }
      autoComplete="off"
      size={props.size}
      label="Label"
    />
    <FormHelperText sx={style.error} error={true}>
      {props.invalid}
    </FormHelperText>
  </>
);

export default QpInputBase;
