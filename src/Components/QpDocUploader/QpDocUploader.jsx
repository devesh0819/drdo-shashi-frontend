import React from "react";
import { Box, Button, InputBase } from "@mui/material";
import { styles } from "./QpDocUploaderStyles.js";
import { showToast } from "../Toast/Toast.js";
import { commonStyles } from "../../Styles/CommonStyles.js";

export default function QpDocUploader(props) {
  const { file, setValue, docType, fileButtonStyle } = props;
  const hiddenFileInput = React.useRef(null);

  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    if (fileUploaded?.size > 5242880) {
      showToast("File size must be less than 5MB", "error");
      event.target.value = "";
      return;
    } else if (
      fileUploaded &&
      docType === "image" &&
      !fileUploaded?.type?.includes("image")
    ) {
      showToast("Only images are allowed", "error");
      event.target.value = "";
      return;
    } else if (
      fileUploaded &&
      docType === "image" &&
      (fileUploaded?.type?.includes("svg") ||
        fileUploaded?.type?.includes("gif"))
    ) {
      showToast("Only images are allowed : .jpeg, .jpg, .png", "error");
      event.target.value = "";
      return;
    } else if (
      fileUploaded &&
      docType === "pdf" &&
      !fileUploaded?.type?.includes("pdf")
    ) {
      showToast("Only PDF is allowed", "error");
      event.target.value = "";
      return;
    } else if (
      fileUploaded &&
      docType === "imagePdf" &&
      !fileUploaded?.type?.includes("pdf") &&
      !fileUploaded?.type?.includes("image")
    ) {
      showToast("Only Image or PDF is allowed", "error");
      event.target.value = "";
      return;
    } else {
      setValue((values) => ({
        ...values,
        [event.target.name]: fileUploaded,
      }));
      event.target.value = "";
    }
  };
  return (
    <>
      <InputBase
        disabled={true}
        endAdornment={
          <>
            <Button
              sx={{ ...styles.chooseFileButton, ...fileButtonStyle }}
              onClick={handleClick}
            >
              <Box>Choose File</Box>
              <input
                type="file"
                alt="file"
                style={commonStyles.displayNone}
                ref={hiddenFileInput}
                onChange={handleChange}
                multiple={props.multiple || false}
                name={props.name}
                accept={props.accept}
              />
            </Button>
          </>
        }
        sx={{ ...styles.chooseFileInput, ...props.inputStyle }}
        placeholder="Select file to upload"
        value={file?.name || ""}
      />
    </>
  );
}
