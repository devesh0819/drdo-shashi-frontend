export const styles = {
  chooseFileButton: {
    width: "9.375rem",
    background: "#D4D2D2",
    border: "1px solid #A9A9A9",
    borderRadius: "4px",
    textTransform: "none",
    height: "2rem",
    "&:hover": {
      background: "#D4D2D2",
    },
    color: "black",
  },
  chooseFileInput: {
    border: "1px solid #A9A9A9",
    width: "30rem",
    height: "2.813rem",
    padding: "0.25rem",
    "@media(max-width:600px)": {
      width: "100%",
    },
  },
};
