import React from "react";
import { NavigateBefore, NavigateNext } from "@mui/icons-material";
import { Box, Pagination, PaginationItem } from "@mui/material";
import { styles } from "../CustomTable/CustomTableStyles.js";

export default function CustomPagination(props) {
  const { totalValues, handlePagination, styleData } = props;
  const navigateArrows = (next) => {
    return (
      <Box style={styles.navigateArrows}>
        {next ? <NavigateNext /> : <NavigateBefore />}
      </Box>
    );
  };
  return (
    <>
      <Pagination
        count={
          totalValues && totalValues % 10 !== 0
            ? Math.floor(totalValues / 10 + 1)
            : Math.floor(totalValues / 10)
        }
        color="primary"
        onChange={(e, val) => handlePagination(val)}
        renderItem={(item) => (
          <PaginationItem
            sx={styles.paginationItem}
            components={{
              previous: () => navigateArrows(false),
              next: () => navigateArrows(true),
            }}
            {...item}
          />
        )}
        sx={styleData}
      />
    </>
  );
}
