import React, { useEffect } from "react";
import { Box, Select, MenuItem, Typography } from "@mui/material";
// import { styles } from "./CustomDropdownStyle";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import QpTypography from "../QpTypography/QpTypography";
import { styles } from "./CustomAccordianStyles";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const CustomAccordian = (props) => {
  const { title, index, setActiveAccordian, activeAccordian } = props;
  return (
    <>
      <Box
        sx={{
          ...styles.cardMainContainer,
          // ...(editAgency && commonStyles.displayNone),
        }}
        onClick={() =>
          activeAccordian === index
            ? setActiveAccordian(-1)
            : setActiveAccordian(index)
        }
      >
        <Box sx={commonStyles.displayStyle}>
          <Box sx={styles.imageTitleContainer}>
            <QpTypography displayText={title} styleData={styles.titleText} />
          </Box>
        </Box>
        {activeAccordian === index ? <ExpandLessIcon /> : <ExpandMoreIcon />}
      </Box>
    </>
  );
};

export default CustomAccordian;
