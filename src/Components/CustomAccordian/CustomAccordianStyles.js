import {
  BACKGROUND_LIGHT_BLUE,
  BLACKISH,
  DEFAULT_GREY,
  LIGHT_BLUE,
} from "../../Constant/ColorConstant";

export const styles = {
  imageTitleContainer: {
    display: "flex",
    cursor: "pointer",
    alignItems: "center",
  },
  cardMainContainer: {
    border: "1px solid #60C5F9",
    background: "#F8FCFF",
    padding: "1rem",
    margin: "1rem",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    cursor: "pointer",
  },
  titleText: {
    marginLeft: "0.75rem",
    fontWeight: 600,
    fontSize: "1rem",
    color: BLACKISH,
  },
  itemContainer: {
    padding: "1rem",
  },
  imageContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  commentContainer: {
    display: "flex",
    alignItems: "center",
  },
  imagePreview: {
    height: "8rem",
    width: "8rem",
    cursor: "pointer",
  },
  participantTitleText: {
    fontWeight: 600,
    fontSize: "1.2rem",
    color: BLACKISH,
    marginLeft: "1rem",
    marginBottom: "1rem",
    // textAlign: "center",
  },
  tableHeading: {
    fontWeight: 600,
    fontSize: "1rem",
    color: BLACKISH,
  },
  participantItemContainer: {
    marginBottom: "0.5rem",
    background: "#F8FCFF",
    paddingLeft: "1rem",
    paddingRight: "1rem",
    paddingTop: "0.3rem",
    paddingBottom: "0.3rem",
  },
  tableContentText: {
    fontWeight: 500,
    fontSize: "0.875rem",
    color: BLACKISH,
    wordBreak: "break-word",
  },
  commentBoxesStyles: {
    padding: "1%",
    display: "flex",
    flexDirection: "column",
    border: "0.5px solid #D3D3D3 ",
    borderRadius: "4px",
    margin: "1rem",
    wordWrap: "break-word",
    boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
  },
  commentBoxInnerBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "start",
    position: "relative",
  },
  commentBoxDate: {
    fontWeight: 500,
    fontSize: "0.938rem",
    lineHeight: "1.25rem",
    color: BLACKISH,
    marginBottom: "0.438rem",
    padding: "0 2%",
    "& .MuiFormLabel-asterisk": {
      color: "red",
    },
  },
  siteTourContentContainer: {
    padding: "1rem 0",
    margin: "1rem 0",
    border: "0.5px solid #D3D3D3 ",
    borderRadius: "4px",
    boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
    // width: "100%",
  },
  noEvidenseText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
  },
  noEvidenseContainer: {
    width: "100%",
    textAlign: "center",
  },
};
