import React from "react";
import { Grid, CircularProgress } from "@mui/material";
import QpTypography from "../QpTypography/QpTypography";
import { styles } from "./CustomAccordianStyles";

const SiteTourCard = (props) => {
  const { imgObj, handleImageClick, showLoader } = props;

  return (
    <Grid container sx={styles.siteTourContentContainer}>
      <Grid item xs={12} sm={12} md={4} lg={4} sx={styles.imageContainer}>
        {!showLoader ? (
          <img
            src={imgObj?.url}
            alt="url"
            style={styles.imagePreview}
            onClick={(event) =>
              handleImageClick(
                imgObj?.url,
                imgObj?.lat,
                imgObj?.long,
                imgObj?.timestamp,
                imgObj?.fileDateTime
              )
            }
          />
        ) : (
          <CircularProgress />
        )}
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8} sx={styles.commentContainer}>
        <QpTypography
          displayText={imgObj?.caption}
          styleData={styles.tableContentText}
        />
      </Grid>
    </Grid>
  );
};

export default SiteTourCard;
