import React from "react";
import { Box, Typography, Grid, CircularProgress } from "@mui/material";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import QpTypography from "../QpTypography/QpTypography";
import { styles } from "./CustomAccordianStyles";
import moment from "moment";

const MeetingsDetailView = (props) => {
  const {
    participantsArray,
    imgObj,
    commentsArray,
    handleImageClick,
    showLoader,
    type,
  } = props;

  return (
    <>
      <Grid container columnSpacing={"1rem"} sx={styles.itemContainer}>
        {participantsArray.length > 0 ? (
          <>
            <Grid item xs={12} sm={12} md={4} lg={4} sx={styles.imageContainer}>
              {!showLoader ? (
                <img
                  src={imgObj?.url}
                  alt="url"
                  style={styles.imagePreview}
                  onClick={(event) =>
                    handleImageClick(
                      imgObj?.url,
                      imgObj?.lat,
                      imgObj?.long,
                      imgObj?.timestamp,
                      imgObj?.fileDateTime
                    )
                  }
                />
              ) : (
                <CircularProgress />
              )}
            </Grid>
            <Grid item xs={12} sm={12} md={8} lg={8}>
              <QpTypography
                displayText="Participants"
                styleData={styles.participantTitleText}
              />
              {/* <Grid container columnSpacing={"1rem"}> */}
              <Grid
                container
                columnSpacing={"1rem"}
                sx={styles.participantItemContainer}
              >
                <Grid item xs={6} sm={6} md={6} lg={6}>
                  <Typography sx={styles.tableHeading}>Name</Typography>
                </Grid>
                <Grid item xs={6} sm={6} md={6} lg={6}>
                  <Typography sx={styles.tableHeading}>Designation</Typography>
                </Grid>
              </Grid>
              {participantsArray.map((participant) => (
                <Grid
                  key={participant}
                  container
                  columnSpacing={"1rem"}
                  sx={styles.participantItemContainer}
                >
                  <Grid item xs={6} sm={6} md={6} lg={6}>
                    <Typography sx={styles.tableContentText}>
                      {participant.name}
                    </Typography>
                  </Grid>
                  <Grid item xs={6} sm={6} md={6} lg={6}>
                    <Typography sx={styles.tableContentText}>
                      {participant.designation}
                    </Typography>
                  </Grid>
                </Grid>
              ))}
              {/* </Grid> */}
            </Grid>
          </>
        ) : (
          <Box sx={styles.noEvidenseContainer}>
            <Typography sx={styles.noEvidenseText}>
              {`No ${type} Evidence`}
            </Typography>
          </Box>
        )}
      </Grid>
      {commentsArray?.length > 0 && (
        <>
          <QpTypography
            displayText="Comments"
            styleData={styles.participantTitleText}
          />
          {commentsArray.map((comment) => (
            <Box sx={styles.commentBoxesStyles} key={comment}>
              <Box sx={styles.commentBoxInnerBox}>
                <QpInputLabel
                  displayText={comment?.comment_author || "-"}
                  styleData={{ ...commonStyles.inputLabel }}
                />
                <QpTypography
                  displayText={
                    moment(Number(comment?.last_updated_at)).format(
                      "D MMM,YYYY"
                    ) || "-"
                  }
                  styleData={{
                    ...styles.commentBoxDate,
                    ...styles.dateStyle,
                  }}
                />
              </Box>
              <QpTypography displayText={comment?.comment} />
            </Box>
          ))}
        </>
      )}
    </>
  );
};

export default MeetingsDetailView;
