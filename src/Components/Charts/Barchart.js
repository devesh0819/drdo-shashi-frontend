import React from "react";
import { Bar } from "react-chartjs-2";
import "chart.js/auto";

const Barchart = (props) => {
  return <Bar data={props.chartData} options={props.options} />;
};

export default Barchart;
