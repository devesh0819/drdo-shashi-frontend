import React from "react";
import { Pie } from "react-chartjs-2";
import "chart.js/auto";

const PieChart = (props) => {
  return <Pie data={props.chartData} options={props.options} />;
};

export default PieChart;
