import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  titleInput: {
    height: "2rem",
  },
  formContainer: {
    flexDirection: "column",
    padding: "1rem 2rem",
    overflow: "scroll",
    ...commonStyles.customScrollBar,
  },
  buttonContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "0.5rem",
  },
  inputWrapper: {
    // paddingRight: "1rem",
  },
  saveButton: {
    background: "#1FBAED",
    borderRadius: "50px",
    textTransform: "none",
    width: "9rem",
    height: "3rem",
    marginRight: "2rem",
    marginLeft: "7.5rem",
    "&:hover": {
      background: "#1FBAED",
    },
  },
  cancelButton: {
    textTransform: "none",
    background: "#D8DCE0",
    borderRadius: "50px",
    height: "3rem",
    width: "9rem",
    "&:hover": {
      background: "#D8DCE0",
    },
  },
  agencyInput: {
    height: "2rem",
  },
  agencyLabel: {
    fontSize: "0.75rem",
    marginBottom: "0",
  },
};
