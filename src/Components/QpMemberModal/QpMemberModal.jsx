import { Box, Grid } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import {
  addMemberAction,
  getCommitteeDetailAction,
  getMembersListingAction,
  updateCommitteeAction,
  updateMemberAction,
} from "../../Redux/Admin/Member/memberActions";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import CustomInput from "../CustomInput/CustomInput";
import QpButton from "../QpButton/QpButton";
import { styles } from "./QpMemberModalStyles";
import SaveNextButtons from "../SaveNextButtons/SaveNextButtons";
import { roleOptionsArray } from "../../Constant/AppConstant";
import CustomDropdown from "../CustomDropdown/CustomDropdown";
import * as _ from "lodash";

const QpMemberModal = (props) => {
  const { memberDetail, closeModal } = props;
  const [isSubmit, setIsSubmit] = useState(false);
  const { id } = useParams();
  const [data, setData] = useState({
    member_role: "",
  });

  const [memberDetailData, setMemberDetailData] = useState({});
  const memberDetailDataRef = useRef(memberDetailData);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    if (memberDetail && memberDetail[0]?.member_role) {
      setData((values) => ({
        ...values,
        member_role: _.find(roleOptionsArray, {
          value: memberDetail[0]?.member_role,
        }),
      }));
    }
  }, [memberDetail]);

  useEffect(() => {
    const {
      first_name,
      last_name,
      email,
      phone_number,
      organisation,
      designation,
      member_role,
    } = memberDetailData;
    if (
      first_name &&
      last_name &&
      email &&
      phone_number &&
      organisation &&
      designation &&
      member_role
    ) {
      onSubmit();
    }
  }, [memberDetailData]);
  const updateState = (newState) => {
    memberDetailDataRef.current = newState;
    setMemberDetailData(newState);
    setIsSubmit(false);
  };

  const getData = (key, value) => {
    updateState({
      ...memberDetailDataRef.current,
      [key]: value,
    });
  };
  const handleSave = () => {
    setIsSubmit(true);
  };

  const handleCancel = () => {
    props.closeModal(false);
  };

  const onSubmit = () => {
    const {
      first_name,
      last_name,
      email,
      phone_number,
      organisation,
      designation,
      member_role,
      alternate_email,
      alternate_phone_number,
    } = memberDetailData;
    if (
      !first_name ||
      !last_name ||
      !email ||
      !phone_number ||
      !organisation ||
      !member_role ||
      !designation
    ) {
      return;
    }
    if (alternate_email === undefined) return;
    if (alternate_phone_number === undefined) return;
    const completeData = {
      first_name: first_name,
      last_name: last_name,
      email: email,
      phone_number: phone_number,
      organisation: organisation,
      designation: designation,
      member_role: member_role?.value,
    };

    if (alternate_email) {
      completeData["alternate_email"] = alternate_email;
    }
    if (alternate_phone_number) {
      completeData["alternate_phone_number"] = alternate_phone_number;
    }

    if (memberDetail) {
      const memberDetailId = memberDetail[0].uuid;
      dispatch(
        updateMemberAction(memberDetailId, completeData, id, closeModal)
      );
    } else {
      dispatch(
        addMemberAction(
          completeData,
          id,
          props.committeeTitle,
          navigate,
          closeModal
        )
      );
    }
    // props.closeModal(false);
  };

  return (
    <Box sx={styles.formContainer}>
      <Grid container spacing={1} sx={commonStyles.signUpContainer}>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="First Name"
            inputStyleData={{
              ...commonStyles.committeeInputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="first_name"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("first_name", data?.first_name)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={memberDetail && memberDetail[0]?.first_name}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Last Name"
            inputStyleData={{
              ...commonStyles.committeeInputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="last_name"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("last_name", data?.last_name)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={memberDetail && memberDetail[0]?.last_name}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Designation"
            inputStyleData={{
              ...commonStyles.committeeInputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="designation"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("designation", data?.designation)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={memberDetail && memberDetail[0]?.designation}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Organisation"
            inputStyleData={{
              ...commonStyles.committeeInputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="organisation"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("organisation", data?.organisation)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={memberDetail && memberDetail[0]?.organisation}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Email"
            inputStyleData={{
              ...commonStyles.committeeInputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="email"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("email", data?.email)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={memberDetail && memberDetail[0]?.email}
            disabled={memberDetail && memberDetail[0]?.email ? true : false}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Alternate Email"
            inputStyleData={{
              ...commonStyles.committeeInputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="alternate_email"
            isRequired={false}
            submit={isSubmit}
            setData={(data) =>
              getData("alternate_email", data?.alternate_email)
            }
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={memberDetail && memberDetail[0]?.alternate_email}
            disabled={
              memberDetail && memberDetail[0]?.alternate_email ? true : false
            }
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Phone Number"
            inputStyleData={{
              ...commonStyles.committeeInputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="phone_number"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("phone_number", data?.phone_number)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={memberDetail && memberDetail[0]?.phone_number}
            maxLength={10}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomInput
            label="Alternate Phone Number"
            inputStyleData={{
              ...commonStyles.committeeInputStyle,
              ...styles.agencyInput,
              ...commonStyles.textInputStyle,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="alternate_phone_number"
            isRequired={false}
            submit={isSubmit}
            setData={(data) =>
              getData("alternate_phone_number", data?.alternate_phone_number)
            }
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={
              memberDetail && memberDetail[0]?.alternate_phone_number
            }
            maxLength={10}
          />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
          <CustomDropdown
            id="member_role"
            label="Role"
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            dropdownBoxStyle={customCommonStyles.marginBottomO}
            selectStyle={commonStyles.selectStyle}
            options={roleOptionsArray}
            setValueData={setData}
            defaultValue={data.member_role}
            value={data.member_role}
            name="member_role"
            registerName="member_role"
            isRequired={true}
            disableSourceForValue={true}
            source="label"
            submit={isSubmit}
            setData={(data) => getData("member_role", data?.member_role)}
          />
        </Grid>
        {/* <Box sx={styles.buttonContainer}>
          <QpButton
            styleData={styles.saveButton}
            displayText="Save"
            onClick={handleSave}
            textStyle={commonStyles.blueButtonText}
          />
          <QpButton
            displayText="Cancel"
            styleData={styles.cancelButton}
            onClick={handleCancel}
            textStyle={commonStyles.greyButtonText}
          />
        </Box> */}
      </Grid>
      <Box sx={commonStyles.displayCenterStyle}>
        <SaveNextButtons
          blueButtonText="Save"
          greyButtonText="Cancel"
          onSaveClick={handleCancel}
          onNextClick={handleSave}
        />
      </Box>
    </Box>
  );
};

export default QpMemberModal;
