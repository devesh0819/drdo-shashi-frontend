import * as colors from "../../Constant/ColorConstant";

export const styles = {
  outerContainer: { marginBottom: "1.25rem" },
  select: {
    width: "100%",
    height: "2.813rem",
    // border: "0.063rem solid #A9A9A9",
    borderRadius: 0,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "1rem",
    lineHeight: "1.363rem",
    color: colors.BLACKISH,
    marginTop: "0.438rem",
    "& .MuiOutlinedInput-notchedOutline": {
      borderColor: "0.063rem solid #A9A9A9",
    },
    "& .Mui-focused": {
      border: "0.063rem solid #A9A9A9",
    },
    "& .Mui-error": {
      border: "0.063rem solid #A9A9A9",
    },
    "& .Mui-colorPrimary": {
      border: "0.063rem solid #A9A9A9",
    },
  },
  menuMaxHeight: {
    maxHeight: "12rem",
  },
};
