import React, { useEffect } from "react";
import { Box, Select, MenuItem, Typography } from "@mui/material";
import { styles } from "./CustomDropdownStyle";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import * as _ from "lodash";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import QpTypography from "../QpTypography/QpTypography";

const customDropdownSchema = (registerName, disableSourceForValue) => {
  if (disableSourceForValue) {
    return Yup.object().shape({
      [registerName]: Yup.object()
        .required(`This field is required`)
        .nullable(),
    });
  } else {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .nullable(),
    });
  }
};

const CustomDropdown = (props) => {
  const {
    disableSourceForValue,
    source,
    labelStyleData,
    dropdownBoxStyle,
    selectStyle,
  } = props;
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
  } = useForm({
    resolver: yupResolver(
      customDropdownSchema(props.registerName, disableSourceForValue)
    ),
    mode: "onChange",
  });
  const handleSelectChange = (event) => {
    if (event) {
      const tempValue = event.target.value;
      props.setValueData((values) => ({
        ...values,
        [event.target.name]: event.target.value,
      }));
      setValue(`${props.registerName}`, event.target.value, {
        shouldValidate: true,
      });
    }
  };
  useEffect(() => {
    if (props.submit) {
      handleSubmit(onSubmit, onError)();
    }
  }, [props.submit]);

  const onError = (err) => {
    props.setData(null);
  };

  const onSubmit = (data) => {
    props.setData(data);
  };

  useEffect(() => {
    if (props.defaultValue) {
      setValue(`${props.registerName}`, props.defaultValue, {
        shouldValidate: true,
      });
    }
  }, [props.defaultValue]);

  return (
    <Box sx={dropdownBoxStyle || styles.outerContainer}>
      <QpInputLabel
        displayText={props.label}
        required={props.isRequired}
        styleData={labelStyleData || commonStyles.label}
      />

      <Select
        labelId={props.id}
        id={props.id}
        MenuProps={{
          style: styles.menuMaxHeight,
        }}
        sx={{ ...styles.select, ...selectStyle }}
        onChange={handleSelectChange}
        value={props.value}
        displayEmpty
        // inputProps={{ "aria-placeholder": "Select" }}
        // label={props.placeholder}
        readOnly={props.readOnly}
        disabled={props.disabled}
        autoFocus={false}
        defaultValue={props.defaultValue}
        name={props.name}
        {...register(`${props.registerName}`, {
          onChange: handleSelectChange,
        })}
        // error={errors[props.registerName] ? true : false}
        renderValue={(val) => {
          if (!val) {
            // return <Typography>{props.defaultValue}</Typography>;
            return (
              <Typography sx={commonStyles.placeHolderColor}>
                Select{" "}
              </Typography>
            );
          }
          return (
            <Typography>{disableSourceForValue ? val[source] : val}</Typography>
          );
        }}
        // placeholder={props.placeholder}
      >
        {/* <MenuItem Disabled value="">{props.placeholder}</MenuItem> */}
        {props.options?.map((item, index) => {
          return (
            <MenuItem
              // value={item}
              key={index}
              {...{ value: disableSourceForValue ? item : item[source] }}
            >
              {item[source]}
            </MenuItem>
          );
        })}
      </Select>
      <QpTypography
        styleData={{
          ...commonStyles.errorText,
          // ...customCommonStyles.marginTopO,
        }}
        displayText={errors[props.registerName]?.message}
      />
    </Box>
  );
};

export default CustomDropdown;
