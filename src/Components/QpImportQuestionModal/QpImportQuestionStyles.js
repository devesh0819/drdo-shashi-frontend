import { BLACKISH } from "../../Constant/ColorConstant";
import { commonStyles } from "../../Styles/CommonStyles";
export const styles = {
  importInputStyle: {
    height: "2.813rem",
    border: "0.125rem solid #A9A9A9;",
    borderRadius: "0;",
    fontFamily: "Open Sans;",
    fontStyle: "normal;",
    paddingLeft: "10px",
    marginTop: "0.738rem",
  },
  uploadDiv: {
    marginTop: "0.738rem",
  },
  titleInput: {
    height: "2rem",
  },
  formContainer: {
    flexDirection: "column",
    padding: "1rem",
    // marginTop: "0.5rem",
    overflow: "scroll",
    ...commonStyles.customScrollBar,
  },
  inputWrapper: {
    paddingRight: "1rem",
    marginRight: "2rem",
  },
  buttonContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "1rem",
  },
  select: {
    width: "100%",
    height: "2.813rem",
    borderRadius: "4px",
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.192rem",
    color: BLACKISH,
    marginTop: "0.738rem",
    "@media(max-width:600px)": {
      height: "1.8rem",
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  saveButton: {
    background: "#1FBAED",
    borderRadius: "50px",
    textTransform: "none",
    width: "9rem",
    height: "3rem",
    marginRight: "2rem",
    marginLeft: "9rem",
    "&:hover": {
      background: "#1FBAED",
    },
  },
  cancelButton: {
    textTransform: "none",
    background: "#D8DCE0",
    borderRadius: "50px",
    height: "3rem",
    width: "9rem",
    "&:hover": {
      background: "#D8DCE0",
    },
  },
  buttonContainerImportModal: {
    width: "100%",
    justifyContent: "flex-end",
  },
  cardMainContainer: {
    border: "1px solid #60C5F9",
    background: "#F8FCFF",
    padding: "0.3rem",
    margin: "0.5rem",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  paramListContainer: {
    width: "100%",
    marginLeft: "0.7rem",
    marginTop: "2rem",
    minHeight: "28rem",
    overflow: "auto",
    ...commonStyles.customScrollBar,
    // border: "1px solid red",
  },
  buttonContainerMultiple: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "1rem",
    // position: "absolute",
    // bottom: "1.5rem",
    // right: "1rem",
  },
  parameterTitle: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
  },
  disciplineTitle: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.775rem",
  },
  countHeading: {
    marginLeft: "1rem",
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "0.875rem",
  },
};
