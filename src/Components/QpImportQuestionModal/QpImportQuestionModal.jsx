import {
  Autocomplete,
  Box,
  Grid,
  InputBase,
  MenuItem,
  Select,
  TextareaAutosize,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  meetApplication,
  certificationLevel,
} from "../../Constant/AppConstant";
import * as _ from "lodash";
import { commonStyles } from "../../Styles/CommonStyles";
import QpButton from "../QpButton/QpButton";
import { styles } from "./QpImportQuestionStyles";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import QpTypography from "../QpTypography/QpTypography";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import QpRadioButton from "../QpRadioButton/QpRadioButton";
import { getMasterDataAction } from "../../Redux/Admin/GetMasterData/getMasterDataActions";
import { importQuestionSchema } from "../../validationSchema/importQuestionSchema";
import moment from "moment";
import { schuduleMeetingAction } from "../../Redux/Admin/Member/memberActions";
import { useParams } from "react-router-dom";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import CustomImageUploader from "../CustomImageUploader/CustomImageUploader";
import { FormatColorReset } from "@mui/icons-material";
import { uploadQuestionairreAction } from "../../Redux/Admin/ManageQuestionairre/manageQuestionairreActions";

const QpImportQuestionModal = (props) => {
  // const {
  //   register,
  //   handleSubmit,
  //   setValue,
  //   formState: { errors },
  // } = useForm({
  //   resolver: yupResolver(importQuestionSchema),
  //   mode: "onChange",
  // });
  const initialState = {
    questionairre_doc: "",
  };

  const dispatch = useDispatch();

  const [name, setName] = useState("");
  const [selectedType, setSelectedType] = useState("");
  const [ecsFile, setEcsFile] = useState();
  const [data, setData] = useState(initialState);
  const [nameError, setNameError] = useState(false);
  const [typeError, setTypeError] = useState(false);
  const [fileError, setFileError] = useState(false);

  useEffect(() => {}, [selectedType]);

  const handleInputChange = (event) => {};

  const handleSelectChange = (event) => {
    let value = event?.target?.value;
    setSelectedType(value);
    if (value == "" || value == " " || value == undefined) setTypeError(true);
    else setTypeError(false);
  };
  const handleNameChange = (event) => {
    let name = event?.target?.value;
    setName(name);
    if (name == "" || name == " " || name == undefined) setNameError(true);
    else setNameError(false);
  };

  const handleCancel = () => {
    props.closeModal(false);
  };

  const onSubmit = () => {
    if (name == "" || name == undefined || name == " ") {
      setNameError(true);
    } else if (
      selectedType == "" ||
      selectedType == " " ||
      selectedType == undefined
    ) {
      setTypeError(true);
    } else if (
      data.questionairre_doc == "" ||
      data.questionairre_doc == undefined ||
      data.questionairre_doc == " "
    ) {
      setFileError(true);
    } else {
      setTypeError(false);
      setFileError(false);
      setNameError(false);
      let file_type;

      if (selectedType == "Large") file_type = "L";
      if (selectedType == "Small/Medium") file_type = "S&M";

      const formData = new FormData();
      formData.append("file", data?.questionairre_doc);
      formData.append("title", name);
      formData.append("type", file_type);
      // send this formData as payload in api (use axios in api)
      dispatch(
        uploadQuestionairreAction(
          formData,
          dispatch,
          props?.closeModal,
          props?.setPageNumber
        )
      );
      // props.closeModal(false);
    }
  };

  return (
    <Box sx={styles.formContainer}>
      <Grid container spacing={2}>
        <Grid xs={12} item>
          <InputLabel id="demo-simple-select-label">
            Name<span style={{ color: "red" }}>*</span>
          </InputLabel>
          <InputBase
            id="name"
            name="name"
            required
            // placeholder="Name"
            sx={{
              ...commonStyles.committeeInputStyle,
              ...styles.importInputStyle,
            }}
            onChange={handleNameChange}
            //   {...register("name")}
            //   error={errors.name ? true : false}
          />
          {nameError && (
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={"This field cannot be empty."}
            />
          )}
        </Grid>
        <Grid xs={12} item>
          <InputLabel id="demo-simple-select-label">
            Type<span style={{ color: "red" }}>*</span>
          </InputLabel>
          <Select
            id="type"
            name="type"
            MenuProps={{
              sx: {
                ...commonStyles.iconButtonStyle,
                ...commonStyles.fullWidth,
              },
            }}
            //   {...register("type")}
            //   error={errors.type ? true : false}
            sx={styles.select}
            value={selectedType}
            onChange={handleSelectChange}
            native={false}
            renderValue={(val) => {
              if (!val)
                return (
                  <Typography sx={commonStyles.placeHolderColor}>
                    Select{" "}
                  </Typography>
                );
              else
                return <Typography sx={styles.dropdownText}>{val}</Typography>;
            }}
            displayEmpty={true}
          >
            <MenuItem value={"Large"} sx={styles.dropdownText}>
              Large
            </MenuItem>
            <MenuItem value={"Small/Medium"} sx={styles.dropdownText}>
              Small/Medium
            </MenuItem>
          </Select>
          {typeError && (
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={"Please select any one."}
            />
          )}
        </Grid>
        <Grid xs={12} item style={styles.uploadDiv}>
          <CustomImageUploader
            file={ecsFile || data.questionairre_doc}
            value={data.questionairre_doc}
            setValue={setData}
            displayText="Upload----"
            isLabel={true}
            name="questionairre_doc"
            accept=".xlsx"
          />
          {fileError && (
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={"Please Upload file"}
            />
          )}
        </Grid>
        <Box
          sx={{
            ...styles.buttonContainer,
            ...styles.buttonContainerImportModal,
          }}
        >
          <QpButton
            styleData={styles.saveButton}
            displayText="Save"
            onClick={onSubmit}
            textStyle={commonStyles.blueButtonText}
            isDisable={
              name == "" ||
              name == " " ||
              selectedType == "" ||
              selectedType == " " ||
              data?.questionairre_doc == "" ||
              data?.questionairre_doc == " " ||
              data?.questionairre_doc == undefined
            }
          />
          <QpButton
            displayText="Cancel"
            styleData={styles.cancelButton}
            onClick={handleCancel}
            textStyle={commonStyles.greyButtonText}
          />
        </Box>
      </Grid>
    </Box>
  );
};

export default QpImportQuestionModal;
