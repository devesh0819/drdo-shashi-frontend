import React, { useEffect } from "react";
import { Autocomplete, Box, TextField } from "@mui/material";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import * as _ from "lodash";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import QpTypography from "../QpTypography/QpTypography";
import { drdoMultiOptions } from "../../Constant/AppConstant";
import { styles } from "./CustomMultiSelectStyles.js";

const customMultiSelectSchema = (registerName) => {
  return Yup.object().shape({
    [registerName]: Yup.array()
      .required(`This field is required`)
      .min(1, "At least 1 assessments is required"),
  });
};

export default function CustomMultiSelect({
  options,
  setValueData,
  value,
  registerName,
  label,
  isRequired,
  submit,
  setData,
  defaultValue,
  name,
}) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
  } = useForm({
    resolver: yupResolver(customMultiSelectSchema(registerName)),
    mode: "onChange",
  });
  const handleSelectChange = (event, stores) => {
    const isAllSelected = _.find(stores, { id: 0 });
    if (isAllSelected) {
      const newStores = drdoMultiOptions?.slice(1);
      setValueData((values) => ({
        ...values,
        [name]: newStores,
      }));
      setValue(`${registerName}`, newStores, {
        shouldValidate: true,
      });
    } else {
      setValueData((values) => ({
        ...values,
        [name]: stores,
      }));
      setValue(`${registerName}`, stores, {
        shouldValidate: true,
      });
    }
  };

  useEffect(() => {
    if (submit) {
      handleSubmit(onSubmit, onError)();
    }
  }, [submit]);

  const onError = (err) => {
    setData(null);
  };

  const onSubmit = (data) => {
    setData(data);
  };

  useEffect(() => {
    if (defaultValue?.length > 0) {
      setValue(`${registerName}`, defaultValue, {
        shouldValidate: true,
      });
    }
  }, [defaultValue]);
  return (
    <Box sx={styles.multiSelectContainer}>
      <QpInputLabel
        displayText={label}
        required={isRequired}
        styleData={commonStyles.label}
      />
      <Autocomplete
        multiple
        filterSelectedOptions
        id="tags-outlined"
        name={name}
        options={options}
        onChange={handleSelectChange}
        getOptionLabel={(option) => option?.title}
        value={value}
        // defaultValue={defaultValue}
        limitTags={4}
        sx={styles.multiSelect}
        isOptionEqualToValue={(option, value) => option.id === value.id}
        // getOptionSelected={(option, value) => option.title === value.title}
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            placeholder="Select Stores"
          />
        )}
      />
      <QpTypography
        styleData={{
          ...commonStyles.errorText,
          //   ...customCommonStyles.marginTopO,
        }}
        displayText={errors[registerName]?.message}
      />
    </Box>
  );
}
