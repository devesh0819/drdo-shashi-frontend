import { BLACKISH } from "../../Constant/ColorConstant";

export const styles = {
  headingContainer: {
    borderBottom: "0.063rem solid #EAEAEA",
    paddingBottom: "1.063rem",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: "1.5rem",
  },
  makePaymentContainer: {
    marginTop: "2.063rem",
  },
  heading: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1.5rem",
    lineHeight: "2.043rem",
    color: BLACKISH,
    "@media(max-width:700px)": {
      fontSize: "0.85rem",
      fontWeight: 600,
      lineHeight: "1.043rem",
    },
  },
  taxInfoContainer: {
    display: "flex",
    alignItems: "center",
    marginBottom: "1rem",
  },
  inputField: {
    minWidth: "200px",
  },
};
