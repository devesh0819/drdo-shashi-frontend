import React, { useEffect } from "react";
import { Box, Grid, InputBase, Typography } from "@mui/material";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";

import QpInputLabel from "../QpInputLabel/QpInputLabel";
import QpRadioButton from "../QpRadioButton/QpRadioButton";
import QpTypography from "../QpTypography/QpTypography";
import { yesNo } from "../../Constant/AppConstant";
import { paymentSchema } from "../../validationSchema/paymentSchema";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { styles } from "./TaxInformationStyles.js";

export default function TaxInformation(props) {
  const { data, setData } = props;
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(paymentSchema),
    mode: "onChange",
  });
  const handleRadioButton = (event, name) => {
    setData((values) => ({
      ...values,
      [name]: event.target.value,
    }));
    setValue(`${name}`, event.target.value, { shouldValidate: true });
  };
  const handleInputChange = (event) => {
    setData((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };
  useEffect(() => {
    if (props.submit) {
      handleSubmit(onSubmit, onError)();
    }
  }, [props.submit]);

  const onError = (err) => {
    props.setPaymentData(null);
  };

  const onSubmit = (data) => {
    props.setPaymentData(data);
  };

  const setDefaultValues = () => {
    if (props.defaultValue) {
      setValue(`${props.registerName}`, props.defaultValue, {
        shouldValidate: true,
      });
    }
  };

  useEffect(() => {
    setDefaultValues();
  }, [props.defaultValue]);

  return (
    <>
      <Box
        sx={{
          ...styles.headingContainer,
          ...styles.makePaymentContainer,
        }}
      >
        <Typography sx={styles.heading}>Tax Information</Typography>
      </Box>
      <Grid container>
        <Grid item sx={styles.taxInfoContainer} sm={12} md={12} xs={12}>
          <QpInputLabel
            displayText="GSTIN Applicable?"
            required={true}
            styleData={{
              ...commonStyles.inputLabel,
              ...customCommonStyles.marginBottomOne,
              ...styles.inputField,
            }}
          />
          <QpRadioButton
            options={yesNo}
            name="gstApplicable"
            onChange={(event) => handleRadioButton(event, "gstApplicable")}
            value={data.gstApplicable || "0"}
            styleData={customCommonStyles.marginBottomOne}
          />
          {data?.gstApplicable == 1 && (
            <Box>
              <InputBase
                sx={{
                  ...commonStyles.inputBaseStyle,
                }}
                name="gst_number"
                onChange={handleInputChange}
                {...register("gst_number")}
                error={errors.gst_number ? true : false}
                placeholder="Enter GST Number"
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={errors.gst_number?.message}
                // displayText="This field is required"
              />
            </Box>
          )}
        </Grid>
        <Grid item sx={styles.taxInfoContainer} sm={12} md={12} xs={12}>
          <QpInputLabel
            displayText="TDS Deductible?"
            required={true}
            styleData={{
              ...commonStyles.inputLabel,
              ...customCommonStyles.marginBottomOne,
              ...styles.inputField,
            }}
          />

          <QpRadioButton
            options={yesNo}
            name="tdsApplicable"
            onChange={(event) => handleRadioButton(event, "tdsApplicable")}
            value={data.tdsApplicable || "0"}
            styleData={customCommonStyles.marginBottomOne}
          />
          {data?.tdsApplicable == 1 && (
            <Box>
              <InputBase
                sx={{
                  ...commonStyles.inputBaseStyle,
                }}
                name="tan_number"
                onChange={handleInputChange}
                {...register("tan_number")}
                error={errors.tan_number ? true : false}
                placeholder="Enter TAN Number"
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={errors.tan_number?.message}
                // displayText="vibha"
              />
            </Box>
          )}
        </Grid>
      </Grid>
    </>
  );
}
