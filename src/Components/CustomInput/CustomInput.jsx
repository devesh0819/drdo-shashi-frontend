import React, { useEffect } from "react";
import { Typography, Box, InputBase, Link } from "@mui/material";
import { styles } from "./CustomInputStyle";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import QpTypography from "../QpTypography/QpTypography";
import { customInputSchema } from "../../validationSchema/customInputSchema";
import * as moment from "moment";
import QpRadioButton from "../QpRadioButton/QpRadioButton";
import { leasedOwned, yesNo } from "../../Constant/AppConstant";
import { formatData } from "../../Services/commonService";

const CustomInput = (props) => {
  const {
    isOption = false,
    optionValue,
    handleRadioButton,
    optionName,
    optionStyleData,
  } = props;
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(
      customInputSchema(props.registerName, props.isRequired)
    ),
    mode: "onChange",
  });
  const handleInputChange = (event) => {
    if (props.setValueData) {
      if (props.registerName === "entrepreneur_aadhar_number") {
        const targetValue = event.target.value;
        const formatText = formatData(targetValue.toString());

        props.setValueData((values) => ({
          ...values,
          [event.target.name]: formatText,
        }));
      } else {
        props.setValueData((values) => ({
          ...values,
          [event.target.name]: event.target.value,
        }));
      }
      setValue(`${props.registerName}`, event.target.value, {
        shouldValidate: true,
      });
    }
  };
  useEffect(() => {
    if (props.submit) {
      handleSubmit(onSubmit, onError)();
    }
  }, [props.submit]);

  const onError = (err) => {
    props.setData(null);
  };

  const onSubmit = (data) => {
    props.setData(data);
  };

  const setDefaultValues = () => {
    if (props.defaultValue) {
      setValue(`${props.registerName}`, props.defaultValue, {
        shouldValidate: true,
      });
    }
  };

  useEffect(() => {
    setDefaultValues();
  }, [props.defaultValue]);

  return (
    <Box sx={props.boxStyle || customCommonStyles.marginBottomOne}>
      <Box
        sx={{
          ...styles.labelContainer,
          // ...(isOption && { justifyContent: "space-between" }),
        }}
      >
        <QpInputLabel
          styleData={props.labelStyleData || commonStyles.label}
          displayText={props.label}
          required={
            props.labelRequired
              ? props.labelRequired
              : props.isRequired || false
          }
        />
        {props.hasLink && (
          <Box sx={commonStyles.linkContainer}>
            <Link
              href={props.href}
              sx={commonStyles.link}
              underline="none"
              target="_blank"
            >
              {props.linkText}
            </Link>
          </Box>
        )}
        {isOption && (
          <QpRadioButton
            options={leasedOwned}
            name={optionName}
            onChange={handleRadioButton}
            value={optionValue}
            styleData={optionStyleData}
          />
        )}
      </Box>
      <Box>
        <InputBase
          id={props.inputId}
          sx={props.inputStyleData || styles.inputBase}
          placeholder={props.placeholder}
          onChange={handleInputChange}
          // value={props.value}
          // value={props.defaultValue}
          defaultValue={props.defaultValue}
          readOnly={props.readOnly}
          type={props.type}
          name={props.name}
          {...register(`${props.registerName}`, {
            onChange: handleInputChange,
          })}
          // error={errors[props.registerName] ? true : false}
          disabled={props.disabled}
          autoComplete="off"
          inputProps={{
            maxLength: props.maxLength,
            max: moment(new Date().setDate(new Date().getDate() - 1)).format(
              "YYYY-MM-DD"
            ),
          }}
          endAdornment={props.endAdornment}
          multiline={props.multiline}
          startAdornment={props.startAdornment}
        />
        <QpTypography
          styleData={{ ...commonStyles.errorText, ...props.errorTextStyle }}
          displayText={errors[props.registerName]?.message}
        />
      </Box>
    </Box>
  );
};

export const NumberFields = (props) => {
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(
      customInputSchema(props.registerName, props.isRequired)
    ),
    mode: "onChange",
  });
  const handleMobileChange = (event) => {
    props.setValueData((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
    setValue(`${props.registerName}`, event.target.value, {
      shouldValidate: true,
    });
  };
  useEffect(() => {
    if (props.submit) {
      handleSubmit(onSubmit, onError)();
    }
  }, [props.submit]);

  const onError = (err) => {
    props.setData(null);
  };

  const onSubmit = (data) => {
    props.setData(data);
  };
  const setDefaultValues = () => {
    if (props.defaultValue) {
      setValue(`${props.registerName}`, props.defaultValue, {
        shouldValidate: true,
      });
    }
  };

  useEffect(() => {
    setDefaultValues();
  }, [props.defaultValue]);
  return (
    <Box sx={styles.numberFieldsContainer}>
      <Box sx={styles.mobileNumber}>
        <QpInputLabel
          styleData={commonStyles.label}
          displayText={props.mobileDisplayText}
          required={props.isRequired || false}
        />
        <Box sx={styles.mobileFlex}>
          <Box sx={styles.codeContainer}>
            <Typography>+91</Typography>
          </Box>
          <InputBase
            id={props.mobileNumberId}
            sx={styles.mobileInput}
            onChange={handleMobileChange}
            // value={props.defaultValue}
            defaultValue={props.defaultValue}
            readOnly={props.readOnly}
            name={props.mobileName}
            {...register(`${props.registerName}`, {
              onChange: handleMobileChange,
            })}
            error={errors[props.registerName] ? true : false}
            disabled={props.disabled}
            autoComplete="off"
            inputProps={{ maxLength: props.maxLength }}
          />
        </Box>
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={errors[props.registerName]?.message}
        />
      </Box>
      {/* <Box sx={styles.landlineContainer}>
        <CustomInput
          label="Landline Number"
          id={props.landlineNumberId}
          setValueData={props.setValue}
          // value={props.landlineValue}
          defaultValue={props.landlineValue}
          readOnly={props.readOnly}
          name={props.landlineName}
          isRequired={false}
          registerName="registered_landline_number"
          submit={isSubmit}
        setData={(data) =>
          getData("registered_mobile_number", data?.registered_mobile_number)
        }
        />
      </Box> */}
    </Box>
  );
};

export default CustomInput;
