import * as colors from "../../Constant/ColorConstant";

export const styles = {
  inputBase: {
    width: "100%",
    height: "2.813rem",
    padding: 0,
    border: "0.063rem solid #A9A9A9",
    marginTop: "0.438rem",
    paddingLeft: "0.938rem",
    paddingRight: "0.938rem",
    // marginBottom: "1.25rem",
  },
  numberFieldsContainer: {
    display: "flex",
    // justifyContent: "space-between",
    alignItems: "center",
    "@media(max-width:600px)": {
      display: "flex",
      flexDirection: "column",
      alignItem: "flex-start",
    },
  },
  mobileNumber: {
    width: "100%",
    "@media(max-width:600px)": {
      width: "100%",
    },
  },
  mobileFlex: {
    display: "flex",
    // justifyContent: "flex-start",
    alignItems: "center",
    height: "2.813rem",
    marginTop: "0.438rem",
    // marginBottom: "1.25rem",
  },
  codeContainer: {
    border: "0.063rem solid #A9A9A9",
    height: "2.713rem",
    width: "7%",
    // marginRight: "1rem",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginRight: "1.625rem",
    "@media(max-width:600px)": {
      width: "15%",
    },
    "@media(max-width:1000px)": {
      width: "15%",
    },
  },
  mobileInput: {
    width: "100%",
    height: "2.813rem",
    padding: 0,
    paddingLeft: "0.938rem",
    border: "0.063rem solid #A9A9A9",
    "@media(max-width:600px)": {
      width: "90%",
    },
  },
  landlineContainer: {
    width: "45%",
    "@media(max-width:600px)": {
      width: "100%",
    },
  },
  labelContainer: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    "@media(max-width:600px)": {
      flexDirection: "column",
      alignItems: "flex-start",
    },
  },
  link: {
    color: colors.BLACK,
    "@media(max-width:600px)": {
      fontSize: "0.675rem",
      fontWeight: 400,
    },
  },
  wordBreak: {
    wordWrap: "break-word",
    wordBreak: "break-all",
    height: "80px",
  },
};
