import React from "react";
import { Grid, MenuItem, Select, Typography } from "@mui/material";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import DropdownFilter from "./DropdownFilter";
import { styles } from "./AllFiltersStyles.js";

const LocationFilter = (props) => {
  const {
    labelId,
    stateArray,
    districtArray,
    labelIdDistrict,
    labelDistrict,
    label,
    handleStateDropdownChange,
    handleDistrictDropdownChange,
    stateValue,
    districtValue,
  } = props;

  return (
    <Grid container columnSpacing={"0.5rem"}>
      <Grid item>
        <DropdownFilter
          labelId={labelId}
          id={labelId}
          value={stateValue}
          handleDropdownChange={handleStateDropdownChange}
          dataArray={stateArray}
          label={label}
          isLocation={true}
          source="title"
          dropdownStyle={styles.stateDistrictDropdown}
        />
      </Grid>
      <Grid item>
        <DropdownFilter
          labelId={labelIdDistrict}
          id={labelIdDistrict}
          value={districtValue}
          handleDropdownChange={handleDistrictDropdownChange}
          dataArray={districtArray}
          label={labelDistrict}
          isLocation={true}
          source="title"
          dropdownStyle={styles.stateDistrictDropdown}
          // dropdownStyle={customCommonStyles.marginTopTwo}
        />
      </Grid>
    </Grid>
  );
};

export default LocationFilter;
