import React from "react";
import { Box, Grid } from "@mui/material";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import CustomDatePicker from "../CustomDatePicker/CustomDatePicker";
import { styles } from "./AllFiltersStyles.js";

const DateRangeFilter = (props) => {
  const {
    startDate,
    handleStartDateChange,
    handleEndDateChange,
    endDate,
    isAssessmentDate,
    displayStartName,
    displayLastName,
  } = props;

  const findEndDate = () => {
    if (endDate - startDate < 0) {
      return startDate;
    } else return endDate;
  };
  return (
    <Grid
      sx={{ ...commonStyles.displayStyle }}
      container
      columnSpacing={"0.5rem"}
    >
      <Grid item>
        <CustomDatePicker
          value={startDate}
          handleDateChange={handleStartDateChange}
          label={displayStartName}
          maxDate={!isAssessmentDate ? endDate || new Date() : findEndDate()}
          textFieldStyle={styles.textFieldStyle}
        />
      </Grid>

      {/* <Box sx={commonStyles.middleBox}></Box> */}
      <Grid item>
        <CustomDatePicker
          value={!isAssessmentDate ? endDate : findEndDate()}
          handleDateChange={handleEndDateChange}
          label={displayLastName}
          minDate={startDate}
          maxDate={!isAssessmentDate ? new Date() : null}
          disabled={startDate ? false : true}
          textFieldStyle={styles.textFieldStyle}
        />
      </Grid>
    </Grid>
  );
};

export default DateRangeFilter;
