export const styles = {
  menuProps: {
    maxHeight: "12rem",
    "@media(max-width:600px)": {
      maxHeight: "9rem",
    },
  },
  menuItemStyle: {
    fontSize: "0.8rem",
  },
  dropdownStyle: {
    height: "2.45rem",
  },
  stateDistrictDropdown: {
    width: "10.5rem",
    borderRadius: "4px",
    height: "2.45rem",
  },
  textFieldStyle: {
    maxWidth: "10.5rem",
  },
};
