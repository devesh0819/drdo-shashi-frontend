import React from "react";
import { MenuItem, Select, Typography } from "@mui/material";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { styles } from "./AllFiltersStyles.js";

const DropdownFilter = (props) => {
  const {
    labelId,
    id,
    value,
    handleDropdownChange,
    dataArray,
    isLocation,
    source = "label",
    name,
    label,
  } = props;
  return (
    <Select
      labelId={labelId}
      id={id}
      name={name}
      value={value}
      MenuProps={{
        sx: styles.menuProps,
      }}
      displayEmpty
      onChange={handleDropdownChange}
      sx={{
        ...commonStyles.select,
        ...customCommonStyles.marginTopO,
        ...props.dropdownStyle,
      }}
      renderValue={(val) => {
        if (!val) {
          return (
            <Typography
              sx={{
                ...commonStyles.placeHolderColor,
                ...commonStyles.placeHolderText,
              }}
            >
              {props.label}
            </Typography>
          );
        }
        return <Typography>{isLocation ? val[source] : val}</Typography>;
      }}
    >
      {dataArray?.map((item, index) => (
        <MenuItem
          value={item}
          key={index}
          sx={{ ...commonStyles.dropdownText, ...styles.menuItemStyle }}
        >
          {isLocation ? item[source] : item}
          {/* {item} */}
        </MenuItem>
      ))}
    </Select>
  );
};

export default DropdownFilter;
