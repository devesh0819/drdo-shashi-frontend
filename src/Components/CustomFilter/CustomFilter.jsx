import React from "react";
import { Box, MenuItem, Select, Typography } from "@mui/material";
import CustomDatePicker from "../CustomDatePicker/CustomDatePicker";
import { commonStyles } from "../../Styles/CommonStyles";
import CloseIcon from "@mui/icons-material/Close";
import SaveNextButtons from "../SaveNextButtons/SaveNextButtons";

export default function CustomFilter(props) {
  const {
    startDate,
    setStartDate,
    endDate,
    setEndDate,
    display,
    resetHandler,
    saveHandler,
    setOpenFilterDialog,
    dropDownValue,
    setDropdownValue,
  } = props;

  return (
    <Box
      sx={{
        ...commonStyles.filterBox,
        ...(display ? commonStyles.displayFlex : commonStyles.displayNone),
      }}
    >
      <Box sx={commonStyles.filterHeader}>
        <Typography sx={commonStyles.filterText}>Filters</Typography>
        <CloseIcon onClick={() => setOpenFilterDialog(false)} />
      </Box>
      <Box sx={commonStyles.bottomFilterContainer}>
        <Box sx={commonStyles.dateFilter}>
          <CustomDatePicker
            value={startDate}
            setValue={setStartDate}
            label="Start Date"
            maxDate={endDate || new Date()}
          />
          <Box sx={commonStyles.middleBox}></Box>
          <CustomDatePicker
            value={endDate}
            setValue={setEndDate}
            label="End Date"
            minDate={startDate}
            maxDate={new Date()}
            disabled={startDate ? false : true}
          />
        </Box>
        <Box sx={commonStyles.filterByStatus}>
          <Select
            labelId="Agency Name"
            id="Agency Name"
            disabled={props.disabled}
            value={dropDownValue}
            MenuProps={{
              sx: commonStyles.menuProps,
            }}
            displayEmpty
            onChange={(event) => setDropdownValue(event.target.value)}
            sx={commonStyles.select}
            renderValue={(val) => {
              if (!val) {
                return (
                  <Typography sx={commonStyles.placeHolderColor}>
                    Select Status
                  </Typography>
                );
              }
              return <Typography>{val?.label}</Typography>;
            }}
          >
            {props.selectArray.map((item, index) => (
              <MenuItem value={item} key={index} sx={commonStyles.dropdownText}>
                {item.label}
              </MenuItem>
            ))}
          </Select>
        </Box>
        <Box sx={commonStyles.buttonsContainer}>
          <SaveNextButtons
            blueButtonText="Save"
            greyButtonText="Reset"
            onSaveClick={resetHandler}
            onNextClick={saveHandler}
            buttonContainerStyle={commonStyles.customButtonConatinerStyle}
            greyButtonStyle={commonStyles.greyButtonStyle}
            blueButtonStyle={commonStyles.blueButtonStyle}
          />
        </Box>
      </Box>
    </Box>
  );
}
