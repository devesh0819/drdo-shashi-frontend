import { Box } from "@mui/material";
import React from "react";
import QpTypography from "../QpTypography/QpTypography";
import DeleteIcon from "@mui/icons-material/Delete";
import { commonStyles } from "../../Styles/CommonStyles";

export default function CustomImageDisplay(props) {
  const { file } = props;
  const handleDelete = (index) => {
    props.onDelete(index);
  };

  return (
    <>
      {file && (
        <Box sx={commonStyles.customImageDisplay}>
          <QpTypography displayText={file?.name} />
          <DeleteIcon
            sx={commonStyles.cursorPointer}
            onClick={(index) => handleDelete(index)}
          />
        </Box>
      )}
    </>
  );
}
