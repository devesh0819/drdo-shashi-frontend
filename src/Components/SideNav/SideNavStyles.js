import * as colors from "../../Constant/ColorConstant";
import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  outerContainer: {
    border: "0.063rem solid #255491",
    borderRadius: "0.313rem",
    height: "calc(100vh - 9.5rem)",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    backgroundColor: colors.WHITE,
    ...commonStyles.customScrollBar,
    overflow: "scroll",
    "@media(max-width:900px)": {
      flexDirection: "row",
      height: "unset",
    },
  },
  upperContainer: {
    "@media(max-width:900px)": {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },
  },
  leftArrowBox: {
    color: "#255491",
    backgroundColor: "white",
    width: "24px",
    height: "24px",
    // padding: "0.2rem",
    borderRadius: "4px 0 0 4px",
    cursor: "pointer",
  },
  leftArrowIcon: {
    transform: "rotate(180deg)",
  },
  headingContainer: {
    backgroundColor: colors.DARKER_BLUE,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    border: "0.063rem solid #000000",
    paddingLeft: "0.75rem",
    paddingY: "1.125rem",
    "@media(max-width:900px)": {
      paddingY: "0rem",
    },
  },
  rightArrowStyle: {
    color: "white",
  },
  headingTitleContainer: { paddingLeft: "0.625rem" },
  nameText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1rem",
    lineHeight: "1.375rem",
    color: colors.WHITE,
    "@media(max-width:900px)": {
      fontSize: "0.7rem",
    },
  },
  emailText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.688rem",
    lineHeight: "0.938rem",
    color: colors.WHITE,
    opacity: 0.5,
    "@media(max-width:900px)": {
      fontSize: "0.5rem",
    },
  },
  sideBarDataContainer: {
    paddingLeft: "0.625rem",
    paddingRight: "0.625rem",
    "@media(max-width:900px)": {
      display: "flex",
      padding: "0rem",
      alignItems: "center",
    },
  },
  signOutButton: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: "0.625rem",
    paddingTop: "1rem",
    paddingBottom: "1rem",
    width: "100%",
  },
  iconSignOut: {
    minWidth: "2.5rem",
    paddingLeft: "0.438rem",
    "@media(max-width:900px)": {
      paddingLeft: "0rem",
      "& .svgIcon": {
        height: "1rem",
        width: "1rem",
      },
    },
  },
  signOutText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    color: colors.RED,
    textTransform: "none",
    "@media(max-width:900px)": {
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  footerContent: {
    paddingLeft: "1.375rem",
    paddingBottom: "1.188rem",
    "@media(max-width:900px)": {
      paddingLeft: "0rem",
      paddingBottom: 0,
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      marginLeft: "1rem",
    },
  },
  contactUs: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.75rem",
    lineHeight: "1rem",
    color: colors.BLACKISH,
    "@media(max-width:900px)": {
      fontSize: "0.5rem",
      lineHeight: "0.8rem",
    },
  },
  emailUs: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.75rem",
    lineHeight: "1rem",
    color: colors.BLACKISH,
    "@media(max-width:900px)": {
      fontSize: "0.5rem",
      lineHeight: "0.8rem",
    },
  },
  callUs: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.75rem",
    lineHeight: "1rem",
    color: colors.BLACKISH,
    "@media(max-width:900px)": {
      fontSize: "0.5rem",
      lineHeight: "0.8rem",
    },
  },
  listItem: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    borderBottom: "0.063rem solid #CECECE",
    padding: 0,
    "&:hover": { backgroundColor: colors.WHITISH_BLUE },
    "@media(max-width:900px)": {
      borderBottom: "0rem solid white",
    },
  },
  listButton: {
    paddingY: "0.7rem",
    paddingLeft: "0.438rem",
    display: "flex",
    alignItems: "center",
  },
  listItemIcon: {
    minWidth: "2.5rem",
    "@media(max-width:900px)": {
      minWidth: "1.5rem",
      "& .svgIcon": {
        height: "1rem",
        width: "1rem",
      },
    },
  },
  listItemInternalText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "0.875rem",
    lineHeight: "1rem",
    color: colors.BLACKISH,
    opacity: 0.7,
    "@media(max-width:900px)": {
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  blueText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "0.875rem",
    lineHeight: "1rem",
    color: colors.DARK_BLUE,
    opacity: 0.7,
    "@media(max-width:900px)": {
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  iconUserProfile: {
    minWidth: "3rem",
    minHeight: "3rem",
    "@media(max-width:900px)": { height: "1.3rem", width: "1.3rem" },
  },
};
