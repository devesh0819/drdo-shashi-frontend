import React, { useEffect, useState } from "react";
import { ReactComponent as IconApplication } from "../../Assets/Images/iconApplication.svg";
import { ReactComponent as IconUserProfile } from "../../Assets/Images/iconUserProfile.svg";
import { ReactComponent as IconPayment } from "../../Assets/Images/iconPayment.svg";
import { ReactComponent as IconLock } from "../../Assets/Images/iconLock.svg";
import { ReactComponent as IconSignOut } from "../../Assets/Images/iconSignOut.svg";
import { ReactComponent as IconNewApplication } from "../../Assets/Images/iconNewApplication.svg";
import { ReactComponent as IconBlueDashboard } from "../../Assets/Images/iconBlueDashboard.svg";
import { ReactComponent as IconBlackApplications } from "../../Assets/Images/iconBlackApplications.svg";
import { ReactComponent as IconBlackAgencyAssessor } from "../../Assets/Images/iconBlackAgencyAssessor.svg";
import { ReactComponent as IconBlackCommittee } from "../../Assets/Images/iconBlackCommittee.svg";
import { ReactComponent as IconBlackManageAssessments } from "../../Assets/Images/iconBlackManageAssessments.svg";
import { ReactComponent as IconBlackQuestionairre } from "../../Assets/Images/iconBlackQuestionairre.svg";
import * as routes from "../../Routes/Routes";
import {
  Typography,
  Box,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Button,
} from "@mui/material";
import { styles } from "./SideNavStyles";
import { useLocation, useNavigate } from "react-router-dom";
import getPrivateRoutes from "../../Routes/PrivateRoutes";
import { getUserData, logout } from "../../Services/localStorageService";
import { logoutAction } from "../../Redux/Login/loginAction";
import { useDispatch, useSelector } from "react-redux";
import { setAdminTabAction } from "../../Redux/Admin/AdminAllApplications/adminAllApplicationAction";
import { setAssessorTabAction } from "../../Redux/LeadAssessor/assessorAllApplicationsActions";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";
import ArrowCircleLeftIcon from "@mui/icons-material/ArrowCircleLeft";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ForwardRoundedIcon from "@mui/icons-material/ForwardRounded";

const applicantArray = [
  {
    title: "All Applications",
    icon: <IconApplication className="svgIcon" />,
  },
  {
    title: "New Application",
    icon: <IconNewApplication className="svgIcon" />,
    route: routes.GENERAL_INFO,
  },
  {
    title: "Payment History",
    icon: <IconPayment className="svgIcon" />,
  },
  {
    title: "Change Password",
    icon: <IconLock className="svgIcon" />,
  },
];

const adminArray = [
  {
    title: "Dashboard",
    icon: <IconBlueDashboard className="svgIcon" />,
  },
  {
    title: "All Applications",
    icon: <IconBlackApplications className="svgIcon" />,
  },
  {
    title: "Manage Assessments",
    icon: <IconBlackManageAssessments className="svgIcon" />,
    route: routes.MANAGE_ASSESSMENTS,
  },
  {
    title: "Agency & Assessors",
    icon: <IconBlackAgencyAssessor className="svgIcon" />,
    route: routes.AGENCY_ASSESSOR,
  },
  {
    title: "Committee",
    icon: <IconBlackCommittee className="svgIcon" />,
    route: routes.COMMITTEE,
  },
  {
    title: "Manage Questionairre",
    icon: <IconBlackQuestionairre className="svgIcon" />,
  },
];

const Content = (props) => {
  const { route, activeIcon, icon, title, setSelectedRoute, index, children } =
    props;
  const navigate = useNavigate();
  const location = useLocation();

  // const handleButtonClick = () => {
  //   setSelectedRoute(route);
  //   navigate(route);
  // };

  const dispatch = useDispatch();
  const handleButtonClick = () => {
    dispatch(setAdminTabAction("1"));
    dispatch(setAssessorTabAction("1"));
    setSelectedRoute(route);
    navigate(route);
  };

  return (
    <List sx={customCommonStyles.padding0}>
      <ListItem sx={styles.listItem} key={index}>
        <ListItemButton
          sx={styles.listButton}
          onClick={handleButtonClick}
          selected={
            location.pathname === route || location.pathname.includes(route)
          }
        >
          <ListItemIcon sx={styles.listItemIcon}>
            {location.pathname === route || location.pathname.includes(route)
              ? activeIcon
              : icon}
          </ListItemIcon>
          <ListItemText
            disableTypography
            primary={
              <Typography
                sx={
                  // checkRoute()
                  location.pathname === route ||
                  location.pathname.includes(route)
                    ? styles.blueText
                    : styles.listItemInternalText
                }
              >
                {title}
              </Typography>
            }
          />
        </ListItemButton>
      </ListItem>
    </List>
  );
};

export default function SideNav(props) {
  const { openSidebar, setOpenSidebar } = props;
  const navigate = useNavigate();
  const userData = getUserData();
  const dispatch = useDispatch();
  const privateRoutes = getPrivateRoutes();
  const [selectedRoute, setSelectedRoute] = useState();
  const userProfile = useSelector((state) => state.profile.userProfile);

  useEffect(() => {
    if (privateRoutes?.length > 0) {
      setSelectedRoute(privateRoutes[0]?.path);
    }
  }, [privateRoutes]);

  const handleLogOut = () => {
    dispatch(setAdminTabAction("1"));
    dispatch(setAssessorTabAction("1"));
    logout(navigate);
  };

  const renderSideBarData = () => {
    // let array = [];
    // switch (role) {
    //   case "Applicant":
    //     array = applicantArray;
    //     break;
    //   case "Admin":
    //     array = adminArray;
    //     break;
    //   default:
    //     array = [];
    // }
    return privateRoutes?.map((item, index) => (
      <Content
        icon={item.icon}
        activeIcon={item.activeIcon}
        title={item.title}
        index={index}
        key={index}
        route={item.path}
        children={item.children}
        selectedRoute={selectedRoute}
        setSelectedRoute={setSelectedRoute}
      />
    ));
  };

  return (
    <>
      <Box sx={styles.outerContainer}>
        <Box sx={styles.upperContainer}>
          <Box sx={styles.headingContainer}>
            <Box sx={commonStyles.displayCenterStyle}>
              <Box>
                <IconUserProfile sx={styles.iconUserProfile} />
              </Box>
              <Box sx={styles.headingTitleContainer}>
                <Typography sx={styles.nameText}>{`${
                  userProfile?.username || userData?.username
                }`}</Typography>
                <Typography sx={styles.emailText}>{`${
                  userProfile?.first_name || userData?.first_name
                } ${
                  userProfile?.last_name || userData?.last_name
                }`}</Typography>
              </Box>
            </Box>
            <Box
              onClick={() => setOpenSidebar(!openSidebar)}
              sx={styles.leftArrowBox}
            >
              {/* <ArrowBackIcon style={styles.rightArrowStyle} /> */}
              <ForwardRoundedIcon style={styles.leftArrowIcon} />
            </Box>
          </Box>
          <Box sx={styles.sideBarDataContainer}>{renderSideBarData()}</Box>
          <Button
            style={styles.signOutButton}
            className="signOutButton"
            // onClick={() => logout(navigate)}
            onClick={handleLogOut}
          >
            <Box sx={styles.iconSignOut}>
              <IconSignOut className="svgIcon" />
            </Box>
            <Typography sx={styles.signOutText}>Sign Out</Typography>
          </Button>
        </Box>
        <Box sx={styles.footerContent}>
          <Typography sx={styles.contactUs}>Contact Us</Typography>
          <Typography sx={styles.emailUs}>Email us: samar@qcin.org</Typography>
          {/* <Typography sx={styles.callUs}>Call us: 9623-763-873 </Typography> */}
        </Box>
      </Box>
    </>
  );
}
