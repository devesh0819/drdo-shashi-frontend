import {
  Autocomplete,
  Box,
  Grid,
  InputBase,
  MenuItem,
  Select,
  TextareaAutosize,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as _ from "lodash";
import { commonStyles } from "../../Styles/CommonStyles";
import QpButton from "../QpButton/QpButton";
import { styles } from "../QpImportQuestionModal/QpImportQuestionStyles";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import QpTypography from "../QpTypography/QpTypography";
import moment from "moment";
import { useParams } from "react-router-dom";
import InputLabel from "@mui/material/InputLabel";
import CustomImageUploader from "../CustomImageUploader/CustomImageUploader";

const AssignAssessorModal = (props) => {
  const initialState = {
    questionairre_doc: "",
  };

  const dispatch = useDispatch();

  const [selectedAssessor, setSelectedAssessor] = useState("");
  const [selectedAssessorID, setSelectdAssessorID] = useState(null);
  const [nameError, setNameError] = useState(false);

  useEffect(() => {}, [selectedAssessor]);

  const handleInputChange = (event) => {};

  const handleSelectChange = (event) => {
    let value = event?.target?.value;
    let res = props?.applicationDetail?.assessors?.find(
      (item) => value === item?.assessor_id
    );
    if (!res) {
      setNameError(true);
    } else {
      let name = `${res?.profile?.first_name}${" "}${res?.profile?.last_name}`;
      setSelectedAssessor(name);
      setSelectdAssessorID(value || res);
      setNameError(false);
    }
  };

  const handleCancel = () => {
    props.closeModal(false);
  };

  const onSubmit = () => {
    if (
      selectedAssessor == "" ||
      selectedAssessor == undefined ||
      selectedAssessor == " "
    )
      setNameError(true);
    else {
      setNameError(false);
      props.handleAssign(selectedAssessorID);
    }
  };

  return (
    <Box sx={styles.formContainer}>
      <Grid container spacing={2}>
        <Grid xs={12} item>
          {/* <InputLabel id="demo-simple-select-label">Assessors<span style={{color:"red"}}>*</span></InputLabel> */}
          <Select
            id="assessor"
            name="assessor"
            MenuProps={{
              sx: {
                ...commonStyles.iconButtonStyle,
                ...commonStyles.fullWidth,
              },
            }}
            sx={styles.select}
            value={selectedAssessorID}
            onChange={handleSelectChange}
            native={false}
            renderValue={(val) => {
              if (!val)
                return (
                  <Typography sx={commonStyles.placeHolderColor}>
                    Select Assessor
                  </Typography>
                );
              else
                return (
                  <Typography sx={styles.dropdownText}>
                    {selectedAssessor}
                  </Typography>
                );
            }}
            displayEmpty={true}
          >
            {/* <MenuItem value={"Large"}  sx={styles.dropdownText}>Large</MenuItem>
              <MenuItem value={"Small/Medium"}  sx={styles.dropdownText}>Small/Medium</MenuItem> */}
            {props?.applicationDetail?.assessors?.map((item) => {
              return (
                <MenuItem
                  key={item}
                  value={item?.assessor_id}
                  sx={styles.dropdownText}
                >{`${item?.profile?.first_name}${" "}${
                  item?.profile?.last_name
                }`}</MenuItem>
              );
            })}
          </Select>
          {nameError && (
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={"Please select any one."}
            />
          )}
        </Grid>

        <Box
          sx={{
            ...styles.buttonContainer,
            ...styles.buttonContainerImportModal,
          }}
        >
          <QpButton
            styleData={styles.saveButton}
            displayText="Save"
            onClick={onSubmit}
            textStyle={commonStyles.blueButtonText}
          />
          <QpButton
            displayText="Cancel"
            styleData={styles.cancelButton}
            onClick={handleCancel}
            textStyle={commonStyles.greyButtonText}
          />
        </Box>
      </Grid>
    </Box>
  );
};

export default AssignAssessorModal;
