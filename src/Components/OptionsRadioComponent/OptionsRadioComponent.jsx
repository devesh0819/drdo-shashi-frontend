import { Box, Typography, Grid } from "@mui/material";
import { styles } from "../../Containers/Admin/ManageQuestionairre/QuestionListCard/QuestionListCardStyles";
import { commonStyles } from "../../Styles/CommonStyles.js";
import { useEffect, useState, useRef } from "react";
// import SaveNextButtons from "../../../../Components/SaveNextButtons/SaveNextButtons";
import Radio from "@mui/material/Radio";

export const OptionsRadioComponent = ({
  parameterData,
  question,
  section,
  category,
  closeContainer,
  inputDisabled,
  handleOptionChange,
  isAssessor,
}) => {
  const [sectionData, setSectionData] = useState({});
  const [selectedResponse, setSelectedResponse] = useState("");

  useEffect(() => {
    setSectionData(section);
    setSelectedResponse(section?.response);
  }, []);

  useEffect(() => {
    if (section?.response) {
      setSectionData(section);
      setSelectedResponse(section?.response);
    }
  }, [section?.response]);

  const handleChange = (event) => {
    let type = event?.target?.id;
    let response = event?.target?.value;
    // setSelectedResponse(type)
    // setSelectedResponseData(response)
    handleOptionChange(type, response, section?.section_uuid);
  };

  return (
    <>
      <Box>
        <Grid
          container
          rowSpacing={1}
          columnSpacing={"4rem"}
          sx={commonStyles.signUpContainer}
        >
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Gold</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio
                  checked={selectedResponse === "bronze"}
                  name="radio-buttons"
                  disbaled={isAssessor}
                  onChange={isAssessor ? () => {} : handleChange}
                  value={
                    sectionData?.options &&
                    JSON.parse(sectionData?.options)?.bronze
                  }
                  id="bronze"
                />
                {sectionData?.options &&
                  JSON.parse(sectionData?.options)?.bronze}
              </Box>
            </Box>

            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Bronze</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio
                  checked={selectedResponse === "silver"}
                  name="radio-buttons"
                  disbaled={isAssessor}
                  onChange={isAssessor ? () => {} : handleChange}
                  value={
                    sectionData?.options &&
                    JSON.parse(sectionData?.options)?.silver
                  }
                  id="silver"
                />
                {sectionData?.options &&
                  JSON.parse(sectionData?.options)?.silver}
              </Box>
            </Box>

            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Silver</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio
                  checked={selectedResponse === "gold"}
                  name="radio-buttons"
                  disbaled={isAssessor}
                  onChange={isAssessor ? () => {} : handleChange}
                  value={
                    sectionData?.options &&
                    JSON.parse(sectionData?.options)?.gold
                  }
                  id="gold"
                />
                {sectionData?.options && JSON.parse(sectionData?.options)?.gold}
              </Box>
            </Box>

            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Diamond</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio
                  checked={selectedResponse === "diamond"}
                  name="radio-buttons"
                  disbaled={isAssessor}
                  onChange={isAssessor ? () => {} : handleChange}
                  value={
                    sectionData?.options &&
                    JSON.parse(sectionData?.options)?.diamond
                  }
                  id="diamond"
                />
                {sectionData?.options &&
                  JSON.parse(sectionData?.options)?.diamond}
              </Box>
            </Box>

            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Platinum</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio
                  checked={selectedResponse === "platinum"}
                  name="radio-buttons"
                  disbaled={isAssessor}
                  onChange={isAssessor ? () => {} : handleChange}
                  value={
                    sectionData?.options &&
                    JSON.parse(sectionData?.options)?.platinum
                  }
                  id="platinum"
                />
                {sectionData?.options &&
                  JSON.parse(sectionData?.options)?.platinum}
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
