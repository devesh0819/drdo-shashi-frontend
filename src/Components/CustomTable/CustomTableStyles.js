import * as colors from "../../Constant/ColorConstant";
import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  titleText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1.125rem",
    lineHeight: "1.532rem",
    color: colors.BLACKISH,
  },
  tableHeadingText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    color: colors.BLACK,
  },
  hasFilterStyle: {
    marginLeft: "0.5rem",
  },
  cellOnClickStyle: {
    "&:hover": { cursor: "pointer" },
  },
  completeColor: {
    color: "#0f84c1",
  },
  borderStyle: {
    borderBottom: "0.063rem solid #EAEAEA",
  },
  outerContainer: {
    // minHeight: "calc(100vh - 15rem)",
    // maxHeight: "calc(100vh - 15rem)",
    // backgroundColor: colors.WHITE,
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: "100%",
    // overflowY: "auto",
    // ...commonStyles.customScrollBar,
    // "@media(max-width:900px)": {
    //   maxHeight: "calc(100vh - 25rem)",
    //   minHeight: "unset"
    // },
  },
  tableContentText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    color: colors.BLACKISH,
  },
  navigateArrows: {
    display: "flex",
    alignItems: "center",
    backgroundColor: colors.LIGHT_GREY,
    height: "1.8rem",
    // width: "3.5rem",
    //width: "1.438rem",
    // borderRadius: "624.938rem",
    borderRadius: "1rem",
  },
  marginLeftTwo: {
    marginLeft: "2rem",
  },
  marginRightTwo: {
    marginRight: "2rem",
  },
  nextPrevText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.7rem",
    margin: "0.8rem",
    // lineHeight: "1.188rem",
    color: colors.BLACKISH,
  },
  topHeader: {
    // borderBottom: "0.063rem solid #EAEAEA",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: "2.063rem",
    paddingRight: "2.063rem",
    paddingTop: "1.25rem",
    paddingBottom: "1.25rem",
    backgroundColor: colors.WHITE,
  },
  filterHeader: {
    alignItems: "center",
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  },
  backButton: {
    minWidth: "unset",
    padding: 0,
    paddingRight: "0.563rem",
    "&:hover": { backgroundColor: "transparent" },
  },
  tableContainer: {
    marginTop: "1.063rem",
    paddingLeft: "2.063rem",
    paddingRight: "2.063rem",
    // backgroundColor: colors.WHITE,
    // minHeight: "calc(100vh - 20rem)",
    overflowY: "auto",
    ...commonStyles.customScrollBar,
  },
  headerCell: {
    backgroundColor: colors.LIGHT_GREY,
    paddingTop: "0.813rem",
    paddingBottom: "0.813rem",
    pagginfLeft: "1.063rem",
  },
  cellMinWidth: {
    minWidth: "8.5rem",
  },
  cellBoldStyle: {
    fontWeight: 600,
    color: "#323A41",
  },
  headBoldStyle: {
    fontWeight: 600,
    color: "#323A41",
  },
  tableBodyRow: {
    "&:hover": { backgroundColor: colors.WHITISH_BLUE },
    "&:hover .icons": commonStyles.displayFlex,
  },
  tableBodyCell: {
    paddingTop: "1rem",
    paddingBottom: "1rem",
    pagginfLeft: "1.063rem",
    // width: "20%",
    "& .MuiTableCell": {
      padding: "0.75rem",
    },
  },
  paginationContainer: {
    display: "flex",
    justifyContent: "center",
    position: "sticky",
    bottom: 0,
    paddingBottom: "1rem",
    paddingTop: "1rem",
    backgroundColor: "white",
  },
  paginationItem: {
    border: "0.063rem solid #F6F6F6",
    "&.Mui-selected": {
      color: colors.WHITE,
    },
    height: "1.438rem",
    width: "1.438rem",
    minWidth: "unset",
    borderRadius: "624.938rem",
    fontSize: "0.75rem",
  },
  maxWidth: {
    // maxWidth: "0px",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden",
    maxWidth: "10rem",
  },
};
