import {
  Box,
  Button,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Pagination,
  PaginationItem,
  Badge,
} from "@mui/material";
import React from "react";
import { ReactComponent as IconCircularBack } from "../../Assets/Images/iconCircularBack.svg";
import { styles } from "./CustomTableStyles";
import { NavigateBefore, NavigateNext } from "@mui/icons-material";
import QpButton from "../../Components/QpButton/QpButton";
import { commonStyles } from "../../Styles/CommonStyles";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import QpTypography from "../QpTypography/QpTypography";
import { DARKER_BLUE } from "../../Constant/ColorConstant";
import FilterComponent from "../FilterComponent/FilterComponent";

const CustomTable = (props) => {
  const {
    saveHandler,
    filterOptions,
    alreadySelectedFilters,
    setOpenFilterDialog,
    setNoOfFiltersApplied,
    setAlreadySelectedFilters,
    stateValue,
    setState,
    districtArray,
  } = props;
  const navigateArrows = (next) => {
    return (
      <Box
        sx={{
          ...styles.navigateArrows,
          ...(next ? styles.marginLeftTwo : styles.marginRightTwo),
        }}
      >
        {/* {next ? <NavigateNext /> : <NavigateBefore />} */}
        <Typography sx={styles.nextPrevText}>
          {next ? "Next" : "Prev"}
        </Typography>
      </Box>
    );
  };
  return (
    <Box sx={{ ...styles.outerContainer, ...props.boxStyle }}>
      <Box>
        <Box sx={{ ...styles.borderStyle }}>
          {props.title && (
            <Box
              sx={
                props?.isQuestionairreTable
                  ? { ...props?.questionairreHeaderStyle }
                  : { ...styles.topHeader }
              }
            >
              {props.hasBack && (
                <Button disableRipple sx={styles.backButton}>
                  <IconCircularBack />
                </Button>
              )}
              <Box sx={styles.filterHeader}>
                <Typography style={styles.titleText}>{`${props.title} (${
                  props.totalValues || 0
                })`}</Typography>
              </Box>

              <Box sx={{ ...props?.buttonsDivStyle }}>
                {props.isTopHeadButton && (
                  <QpButton
                    styleData={{
                      ...commonStyles.blueButtonStyle,
                      ...props.topHeadButtonStyle,
                      marginRight: "3%",
                      // width: "10rem",
                    }}
                    displayText={props.TopHeaderButtonText}
                    onClick={props.topheaderButtonClick}
                    textStyle={
                      props?.isQuestionairreTable
                        ? {
                            ...commonStyles.blueButtonText,
                            ...props?.questionairreBtnStyle,
                          }
                        : props.topHeadButtonTextStyle ||
                          commonStyles.blueButtonText
                    }
                  />
                )}
                {props.isSecondaryButton && props.isTopHeadButton && (
                  <QpButton
                    styleData={
                      props?.isSecondaryButton
                        ? {
                            ...props?.BtnStylingInSecondaryBtncase,
                            ...commonStyles.blueButtonStyle,
                          }
                        : ""
                    }
                    displayText={props.secondaryButtonText}
                    onClick={props.secondaryButtonClick}
                    textStyle={
                      props?.isQuestionairreTable
                        ? {
                            ...commonStyles.blueButtonText,
                            ...props?.questionairreBtnStyle,
                          }
                        : { ...commonStyles.blueButtonText }
                    }
                  />
                )}
                {props.hasExport && (
                  <QpButton
                    styleData={commonStyles.blueButtonStyle}
                    displayText="Export"
                    onClick={props.exportButtonClick}
                    textStyle={commonStyles.tableHeadButtonText}
                  />
                )}
              </Box>
            </Box>
          )}
          {props.hasFilter && (
            <FilterComponent
              saveHandler={saveHandler}
              filterOptions={filterOptions}
              alreadySelectedFilters={alreadySelectedFilters}
              setOpenFilterDialog={setOpenFilterDialog}
              setNoOfFiltersApplied={setNoOfFiltersApplied}
              setAlreadySelectedFilters={setAlreadySelectedFilters}
              stateValue={stateValue}
              setState={setState}
              districtArray={districtArray}
            />
          )}
        </Box>
        <Box sx={{ ...styles.tableContainer, ...props.styleData }}>
          <Table stickyHeader>
            <TableHead>
              <TableRow key={1}>
                {props.columnDefs.map((item, index) => (
                  <TableCell
                    align="left"
                    sx={{
                      ...styles.headerCell,
                      ...props.headerCellStyle,
                      ...props.verticalBorderStyle,
                      minWidth: item.minWidth || styles.cellMinWidth,
                    }}
                    key={index}
                  >
                    <Typography
                      sx={{
                        ...styles.tableHeadingText,
                        ...(item.headBold && styles.headBoldStyle),
                      }}
                    >
                      {item.field}
                    </Typography>
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {props?.rowData?.map((row, index) => (
                <TableRow key={index} sx={styles.tableBodyRow}>
                  {props.columnDefs.map((head, columnDefsIndex) =>
                    head.renderColumn ? (
                      <TableCell
                        key={columnDefsIndex}
                        sx={{
                          ...styles.tableBodyCell,
                          ...props.tableBodyCellStyle,
                        }}
                      >
                        {head.renderColumn(row)}
                      </TableCell>
                    ) : (
                      <TableCell
                        key={columnDefsIndex}
                        sx={{
                          ...styles.tableBodyCell,
                          ...props.verticalBorderStyle,
                          ...(head.onClick && styles.cellOnClickStyle),
                          // width: head.width && head.width,
                        }}
                      >
                        {!props.hasSplit ? (
                          <Typography
                            style={{
                              ...styles.tableContentText,
                              ...(head["ellipsisClass"] === true &&
                                styles.maxWidth),
                              ...(head.onClick &&
                                props.rowData[index][head.field] ===
                                  "Complete" &&
                                styles.completeColor),
                            }}
                            onClick={() => head.onClick(row)}
                          >
                            {props.rowData[index][head.field]}
                          </Typography>
                        ) : (
                          <>
                            {props.rowData[index][head.field]
                              .toString()
                              ?.split("\n")
                              ?.map((item, index) => (
                                <Typography
                                  sx={{
                                    ...(head.bold && styles.cellBoldStyle),
                                  }}
                                  key={index}
                                >
                                  {item}
                                </Typography>
                              ))}

                            {/* <Typography>
                              {
                                props.rowData[index][head.field]
                                  .toString()
                                  ?.split("\n")[1]
                              }
                            </Typography> */}
                          </>
                        )}
                      </TableCell>
                    )
                  )}
                </TableRow>
              ))}
            </TableBody>
          </Table>
          {props.totalValues === 0 && (
            <Box>
              <QpTypography
                displayText="No data available"
                styleData={commonStyles.noData}
              />
            </Box>
          )}
        </Box>
      </Box>
      {props.hasPagination && (
        <Box
          sx={{
            ...styles.paginationContainer,
            ...props.customPaginationContainer,
          }}
        >
          <Pagination
            count={
              props.totalValues && props.totalValues % 10 !== 0
                ? Math.floor(props.totalValues / 10 + 1)
                : Math.floor(props.totalValues / 10)
            }
            color="primary"
            // page={props.currentPage}
            page={props.currentPage}
            onChange={(e, val) => props.handlePagination(val)}
            renderItem={(item) => (
              <PaginationItem
                sx={styles.paginationItem}
                components={{
                  previous: () => navigateArrows(false),
                  next: () => navigateArrows(true),
                }}
                {...item}
              />
            )}
          />
        </Box>
      )}
    </Box>
  );
};

export default CustomTable;
