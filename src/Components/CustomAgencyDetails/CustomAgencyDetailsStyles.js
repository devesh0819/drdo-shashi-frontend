export const styles = {
  agencyInput: {
    height: "2rem",
  },
  agencyLabel: {
    fontSize: "0.75rem",
    marginBottom: "0",
  },
  agencyFormContainer: {
    // margin: "1rem",
    // border: "1px solid #60C5F9",
    padding: "2rem",
    paddingTop: "0.5rem",
  },
  addAgencyHeading: {
    fontWeight: 700,
    textAlign: "center",
  },
};
