import React, { useState, useRef, useEffect } from "react";
import { Box, Grid } from "@mui/material";
import CustomInput from "../CustomInput/CustomInput";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import SaveNextButtons from "../SaveNextButtons/SaveNextButtons";
import { styles } from "./CustomAgencyDetailsStyles.js";
import { useDispatch } from "react-redux";
import {
  addAgencyAction,
  updateAgencyAction,
} from "../../Redux/Admin/AgencyAssessor/agencyAssessorActions";
import QpTypography from "../QpTypography/QpTypography";

export default function CustomAgencyDetails({
  addAgency,
  setAddAgency,
  defaultFirstName,
  defaultLastName,
  defaultTitle,
  defaultAddress,
  defaultPhoneNo,
  defaultEmail,
  setEditAgencyId,
  editAgencyId,
  setEditAssessorId,
}) {
  const [isSubmit, setIsSubmit] = useState(false);
  const [agencyData, setAgencyData] = useState({});
  const agencyDataRef = useRef(agencyData);
  const dispatch = useDispatch();
  const updateState = (newState) => {
    agencyDataRef.current = newState;
    setAgencyData(newState);
    setIsSubmit(false);
  };

  useEffect(() => {
    const { title, first_name, last_name, email, phone_number, address } =
      agencyData;
    if (title && first_name && last_name && email && phone_number && address) {
      onSubmit();
    }
  }, [agencyData]);

  const getData = (key, value) => {
    updateState({
      ...agencyDataRef.current,
      [key]: value,
    });
  };
  const saveHandler = () => {
    setIsSubmit(true);
  };
  const cancelHandler = () => {
    setAddAgency(false);
    setEditAgencyId("");
    setEditAssessorId("");
  };

  const onSubmit = () => {
    const { title, first_name, last_name, email, phone_number, address } =
      agencyData;
    if (
      !title ||
      !first_name ||
      !last_name ||
      !email ||
      !phone_number ||
      !address
    ) {
      return;
    }
    const completeData = {
      title: title,
      first_name: first_name,
      last_name: last_name,
      email: email,
      phone_number: phone_number,
      address: address,
    };

    if (editAgencyId) {
      dispatch(
        updateAgencyAction(
          editAgencyId,
          completeData,
          setAddAgency,
          setEditAgencyId
        )
      );
    } else {
      dispatch(addAgencyAction(completeData, setAddAgency, setEditAgencyId));
    }
  };

  return (
    <Box sx={styles.agencyFormContainer}>
      <QpTypography
        displayText={editAgencyId ? "Edit Agency" : "Add Agency"}
        styleData={styles.addAgencyHeading}
      />
      <Grid
        container
        rowSpacing={1}
        columnSpacing={"4rem"}
        sx={commonStyles.signUpContainer}
      >
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <CustomInput
            label="Title"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="title"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("title", data?.title)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultTitle}
          />
        </Grid>
        <Grid item xs={6} sm={12} md={6} lg={6}>
          <CustomInput
            label="First Name"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="first_name"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("first_name", data?.first_name)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultFirstName}
          />
        </Grid>
        <Grid item xs={6} sm={12} md={6} lg={6}>
          <CustomInput
            label="Last Name"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="last_name"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("last_name", data?.last_name)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultLastName}
          />
        </Grid>
        <Grid item xs={6} sm={12} md={6} lg={6}>
          <CustomInput
            label="Email"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="email"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("email", data?.email)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultEmail}
            disabled={editAgencyId ? true : false}
          />
        </Grid>
        <Grid item xs={6} sm={12} md={6} lg={6}>
          <CustomInput
            label="Mobile Number"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="phone_number"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("phone_number", data?.phone_number)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultPhoneNo}
            maxLength={10}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <CustomInput
            label="Address"
            inputStyleData={{
              ...commonStyles.inputStyle,
              ...styles.agencyInput,
            }}
            labelStyleData={{
              ...commonStyles.inputLabel,
              ...styles.agencyLabel,
            }}
            registerName="address"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("address", data?.address)}
            boxStyle={customCommonStyles.marginBottomO}
            defaultValue={defaultAddress}
          />
        </Grid>
      </Grid>
      <SaveNextButtons
        blueButtonText="Save"
        greyButtonText="Cancel"
        onSaveClick={cancelHandler}
        onNextClick={saveHandler}
      />
    </Box>
  );
}
