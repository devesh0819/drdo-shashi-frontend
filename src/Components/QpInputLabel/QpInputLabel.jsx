import React from "react";
import InputLabel from "@mui/material/InputLabel";

const QpInputLabel = (props) => {
  return (
    <InputLabel required={props.required} sx={props.styleData} focused={false}>
      {" "}
      {props.displayText}
    </InputLabel>
  );
};

export default QpInputLabel;
