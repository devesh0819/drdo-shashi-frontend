import React, { useState } from "react";

import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";

export default function CustomDatePicker(props) {
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DatePicker
        label={props.label}
        value={props.value}
        onChange={props.handleDateChange}
        renderInput={(params) => (
          <TextField size="small" {...params} sx={props.textFieldStyle} />
        )}
        minDate={props.minDate}
        maxDate={props.maxDate}
        disabled={props.disabled}
        disableHighlightToday={props.disableHighlightToday}
        inputFormat="DD/MM/YYYY"
      />
    </LocalizationProvider>
  );
}
