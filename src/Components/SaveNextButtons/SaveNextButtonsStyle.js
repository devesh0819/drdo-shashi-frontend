export const styles = {
  outerContainer: {
    "@media(max-width:600px)": {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    marginTop: "1.25rem",
  },
  saveButton: {
    background: "#D8DCE0",
    borderRadius: "5px",
    textTransform: "none",
    width: "11rem",
    height: "3rem",
    "&:hover": {
      background: "#D8DCE0",
    },
    marginRight: "1rem",
  },
  saveText: { fontWeight: 700, fontSize: "1rem" },
  nextButton: {
    background: "#2C6A9F",
    borderRadius: "5px",
    textTransform: "none",
    width: "11rem",
    height: "3rem",
    "&:hover": {
      background: "#2C6A9F",
    },
  },
  nextText: { fontWeight: 700, fontSize: "1rem", color: "white" },
};
