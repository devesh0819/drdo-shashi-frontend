import React from "react";
import QpButton from "../QpButton/QpButton";
import { Box } from "@mui/material";
import { styles } from "./SaveNextButtonsStyle";

const SaveNextButtons = ({
  final = false,
  onSaveClick,
  onNextClick,
  onPreviewClick,
  greyButtonText,
  blueButtonText,
  greyButtonStyle,
  blueButtonStyle,
  greyButtonTextStyle,
  blueButtonTextStyle,
  blueButtonRequired = true,
  greyButtonRequired = true,
  buttonContainerStyle,
}) => {
  return (
    <Box sx={{ ...styles.outerContainer, ...buttonContainerStyle }}>
      {greyButtonRequired && (
        <QpButton
          displayText={greyButtonText || "Save"}
          styleData={{ ...styles.saveButton, ...greyButtonStyle }}
          textStyle={{ ...styles.saveText, ...greyButtonTextStyle }}
          onClick={onSaveClick}
        />
      )}
      {final && (
        <QpButton
          displayText="Preview"
          styleData={styles.saveButton}
          textStyle={styles.saveText}
          onClick={onPreviewClick}
        />
      )}
      {blueButtonRequired && (
        <QpButton
          displayText={blueButtonText || "Next"}
          styleData={{ ...styles.nextButton, ...blueButtonStyle }}
          textStyle={{ ...styles.nextText, ...blueButtonTextStyle }}
          onClick={onNextClick}
        />
      )}
    </Box>
  );
};

export default SaveNextButtons;
