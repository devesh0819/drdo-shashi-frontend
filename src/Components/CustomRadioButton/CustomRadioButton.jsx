import React, { useEffect } from "react";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { styles } from "./CustomRadioButtonStyles.js";
import { Box, InputBase, Link, Typography } from "@mui/material";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import QpInputLabel from "../QpInputLabel/QpInputLabel";
import QpTypography from "../QpTypography/QpTypography";
import * as _ from "lodash";

const customRadioButtonSchema = (registerName) =>
  Yup.object().shape({
    [registerName]: Yup.string().required(`This field is required`).nullable(),
  });

export default function CustomRadioButton(props) {
  const {
    displayText,
    options,
    setValueData,
    isFinalStep = false,
    disabled,
    readOnly,
  } = props;
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
  } = useForm({
    resolver: yupResolver(customRadioButtonSchema(props.registerName)),
    mode: "onChange",
  });
  const handleChange = (event) => {
    setValueData((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
    setValue(`${props.registerName}`, event.target.value);
  };
  useEffect(() => {
    if (props.submit) {
      handleSubmit(onSubmit, onError)();
    }
  }, [props.submit]);

  const onError = (err) => {
    props.setData(null);
  };

  const onSubmit = (data) => {
    props.setData(data);
  };
  useEffect(() => {
    if (props.defaultValue) {
      setValue(`${props.registerName}`, props.defaultValue, {
        shouldValidate: true,
      });
    }
  }, [props.defaultValue]);

  return (
    <Box sx={customCommonStyles.marginBottomOne}>
      {/* // <FormControl sx={styles.formControlContainer}> */}
      <FormLabel
        id="demo-row-radio-buttons-group-label"
        sx={{ ...commonStyles.label, ...styles.customRadioLabel }}
        focused={false}
      >
        <Box sx={commonStyles.labelContainer}>
          <QpInputLabel
            styleData={props.labelStyleData || commonStyles.label}
            displayText={displayText}
            required={props.isRequired || false}
          />
          {props.hasLink && (
            <Box sx={commonStyles.linkContainer}>
              <Link href={props.href} sx={commonStyles.link} underline="none">
                {props.linkText}
              </Link>
            </Box>
          )}
        </Box>
      </FormLabel>
      {isFinalStep && (
        <Typography sx={styles.subText}>
          (Government subsidy is only available for UAM Registered MSMEs)
        </Typography>
      )}

      <RadioGroup
        row
        aria-labelledby="demo-row-radio-buttons-group-label"
        onChange={handleChange}
        value={props.value || props.defaultValue}
        // defaultValue={props.defaultValue}
        name={props.name}
      >
        {options.map((opt, index) => {
          return (
            <FormControlLabel
              disabled={disabled}
              key={index}
              value={opt.value}
              control={<Radio size="small" />}
              label={opt.label}
              {...register(`${props.registerName}`, {
                onChange: handleChange,
              })}
              // error={errors[props.registerName] ? true : false}
            />
          );
        })}
      </RadioGroup>
      {isFinalStep && (
        <InputBase
          id={props.inputId}
          sx={styles.registerNoInput}
          value={props.value}
          readOnly={props.readOnly}
          type={props.type}
          name={props.name}
        />
      )}
      <QpTypography
        styleData={{
          ...commonStyles.errorText,
          ...customCommonStyles.marginTopO,
        }}
        displayText={errors[props.registerName]?.message}
      />
      {/* // </FormControl> */}
    </Box>
  );
}
