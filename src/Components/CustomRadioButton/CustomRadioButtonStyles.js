import * as colors from "../../Constant/ColorConstant";

export const styles = {
  customRadioLabel: {
    "& .Mui-focused": {
      color: colors.BLACKISH,
    },
  },
  subText: {
    fontWeight: 400,
    fontSize: "0.75rem",
    lineHeight: "1rem",
    color: "#9F9F9F",
    marginLeft: "0.75rem",
  },
  registerNoInput: {
    width: "100%",
    height: "2.813rem",
    padding: 0,
    border: "0.063rem solid #A9A9A9",
    marginTop: "0.438rem",
    paddingLeft: "0.938rem",
    paddingRight: "0.938rem",
    marginBottom: "1.25rem",
  },
  formControlContainer: {
    marginBottom: "1rem",
    width: "100%",
  },
};
