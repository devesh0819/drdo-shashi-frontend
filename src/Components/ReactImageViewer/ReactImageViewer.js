import moment from "moment";
import React from "react";
import ImgsViewer from "react-images-viewer";

const ReactImageViewer = (props) => {
  function convertEpochToSpecificTimezone(timeEpoch) {
    const newDate = new Date(timeEpoch * 1000);
    return moment(newDate)?.format("DD/MM/YYYY HH:mm");
  }
  return (
    <>
      <ImgsViewer
        imgs={[
          {
            src: props.imgs,
            caption:
              props?.lat && props?.long && props?.timestamp
                ? `LAT:${props?.lat}${" "} LONG:${props?.long} TIMESTAMP:${
                    props?.fileDateTime === "N/A"
                      ? props?.timestamp
                      : convertEpochToSpecificTimezone(props?.timestamp)
                  }`
                : "",
          },
        ]}
        isOpen={props.isOpen}
        onClose={props.onClose}
        showImgCount={false}
      />
    </>
  );
};

export default ReactImageViewer;
