import { Box, Button, Typography } from "@mui/material";
import { ReactComponent as IconBackArrow } from "../../Assets/Images/iconBackArrow.svg";
import React from "react";
import QpStepper from "../QpStepper/QpStepper";
import { registrationOptions } from "../../Constant/AppConstant";
import { styles } from "./QpFormContainerStyles.js";
import { commonStyles } from "../../Styles/CommonStyles";
import { useNavigate } from "react-router-dom";

export default function QpFormContainer(props) {
  const { isStepperRequired = true } = props;
  const navigate = useNavigate();
  return (
    <Box sx={styles.outerContainer}>
      <Button
        disableRipple
        sx={styles.backButton}
        onClick={() => navigate(props.changeNavigate)}
      >
        <IconBackArrow />
        <Typography sx={styles.backText}>Back</Typography>
      </Button>
      <Box
        sx={{
          // height: "calc(100vh - 16.5rem)",
          // overflow: "scroll",
          ...commonStyles.customScrollBar,
          ...styles.mainContainer,
        }}
      >
        {isStepperRequired && (
          <Box>
            <Box sx={styles.stepperContainer}>
              <Box sx={styles.stepperUpper}>
                <Typography sx={styles.stepperText}>New Application</Typography>
              </Box>
              <Box sx={styles.stepperLower}>
                <QpStepper
                  stepOptions={registrationOptions}
                  activeStep={props.activeStep}
                />
              </Box>
            </Box>
          </Box>
        )}
        <Box sx={styles.formContainer}>{props.children}</Box>
      </Box>
    </Box>
  );
}
