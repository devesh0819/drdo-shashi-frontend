import * as colors from "../../Constant/ColorConstant";
import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  outerContainer: {
    paddingLeft: "3.75rem",
    paddingRight: "3.75rem",
    paddingTop: "1rem",
    paddingBottom: "1rem",
    "@media(max-width:600px)": {
      padding: "1rem 0",
    },
  },
  mainContainer: {
    marginTop: "1rem",
    "@media(max-width: 600px)": {
      height: "calc(100vh - 13.5rem)",
      overflow: "scroll",
      ...commonStyles.customScrollBar,
      padding: "0 1rem",
    },
  },
  backButton: {
    display: "flex",
    justifyContent: "flex-start",
    minWidth: "unset",
    padding: 0,
    "&:hover": { backgroundColor: "transparent" },
    "@media(max-width:600px)": {
      marginLeft: "1rem",
    },
  },
  backText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1rem",
    lineHeight: "1.703rem",
    color: colors.BLACK,
    paddingLeft: "0.375rem",
    textTransform: "none",
    "@media(max-width:600px)": {
      fontSize: "0.75rem",
      fontWeight: 600,
    },
  },
  stepperContainer: {
    border: "0.063rem solid #255491",
    marginBottom: "1.4rem",
    paddingLeft: "2.635rem",
    paddingRight: "2.635rem",
    backgroundColor: colors.WHITE,
    borderRadius: "0.25rem",
    "@media(max-width:600px)": {
      paddingX: "0.75rem",
    },
  },
  stepperUpper: {
    borderBottom: "0.063rem solid #EAEAEA",
    paddingY: "0.8rem",
    "@media(max-width:600px)": {
      paddingY: "0.5rem",
    },
  },
  stepperText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1.5rem",
    lineHeight: "2.043rem",
    color: colors.BLACK,
    "@media(max-width:600px)": {
      fontSize: "1rem",
      fontWeight: 600,
    },
  },
  stepperLower: {
    paddingY: "1.5rem",
    "@media(max-width:600px)": {
      paddingY: "0.75rem",
    },
  },
  formContainer: {
    minHeight: "calc(100vh - 11.5rem)",
    backgroundColor: colors.WHITE,
    paddingTop: "1.5rem",
    paddingBottom: "1.5rem",
    paddingLeft: "2.635rem",
    paddingRight: "2.635rem",
    borderRadius: "0.25rem",
    // height: "calc(100vh - 14.75rem)",
    // overflow: "scroll",
    // ...commonStyles.customScrollBar,
    "@media(max-width:600px)": {
      padding: "0.75rem",
    },
  },
};
