import React, { useState } from "react";
import dayjs from "dayjs";
import TextField from "@mui/material/TextField";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";

export default function CustomDateTime(props) {
  const { textAreaStyle } = props;
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DateTimePicker
        renderInput={(props) => <TextField {...props} sx={textAreaStyle} />}
        label={props.label}
        value={props.selectedDateTime}
        onChange={(newValue) => {
          props.setSelectedDateTime(newValue);
        }}
        minDate={dayjs(props?.minDateTime)?.$d}
        disablePast={true}
        disableIgnoringDatePartForTimeValidation={true}
        minTime={dayjs(new Date(new Date(props.selectedDateTime).setHours(9)))}
        maxTime={dayjs(new Date(new Date(props.selectedDateTime).setHours(17)))}
        disableHighlightToday
        inputFormat="DD/MM/YYYY"
      />
    </LocalizationProvider>
  );
}
