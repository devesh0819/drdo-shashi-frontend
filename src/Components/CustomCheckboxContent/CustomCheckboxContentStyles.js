import * as colors from "../../Constant/ColorConstant";

export const styles = {
  checkBoxContainer: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  declarationText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "1rem",
    lineHeight: "1.362rem",
    color: colors.BLACKISH,
  },
};
