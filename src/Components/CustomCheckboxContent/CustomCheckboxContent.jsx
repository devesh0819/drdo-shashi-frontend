import React from "react";
import { Box, Typography } from "@mui/material";
import QpCheckbox from "../QpCheckbox/QpCheckbox";
import { styles } from "./CustomCheckboxContentStyles.js";

export default function CustomCheckboxContent(props) {
  const { checked, setChecked, displayText } = props;
  const content =
    "I hereby declare that information given above is true to the best of my knowledge. Any information, that may be required to be verified, shall be provided immediately before the concerned authority.";

  const handleCheckboxChange = (event) => {
    setChecked(event.target.checked);
  };
  return (
    <Box sx={styles.checkBoxContainer}>
      <QpCheckbox checked={checked} onChange={handleCheckboxChange} />
      <Typography sx={styles.declarationText}>
        {displayText || content}
      </Typography>
    </Box>
  );
}
