import React, { useState, useEffect, useRef } from "react";
import { Grid, Typography, Box } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { styles } from "../../Containers/Applicant/Preview/PreviewStyles.js";
import * as moment from "moment";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import Row from "../../Components/Row/Row";
import CustomTable from "../../Components/CustomTable/CustomTable";
import { useParams } from "react-router-dom";
import { maskEmailField, maskFields } from "../../Services/commonService";
import samarLogo from "../../Assets/Images/samar_logo.jpg";

import {
  drdoCategoryOptions,
  drdoMultiOptions,
  enterpriseType,
  natureOfBusinessArray,
  natureOfEnterpriseArray,
  socialCategoryOptions,
  specialTypeOptions,
  typeOfEnterpriseArray,
  unitCategories,
} from "../../Constant/AppConstant";
import * as _ from "lodash";
import dayjs from "dayjs";

const CustomApplicatonView = (props) => {
  const { applicationDetail } = props;
  const [generalAddressTableRow, setGeneralAddressTableRow] = useState([]);

  const [customerTableRow, setCustomerTableRow] = useState([]);

  const [productTableRow, setProductTableRow] = useState([]);

  const [rawMaterialTableRow, setRawMaterialTableRow] = useState([]);

  const [pocTableRow, setPocTableRow] = useState([]);

  const [drdoStorage, setDrdoStorage] = useState([]);

  const productTableColumn = [
    { field: "Name of the product", headBold: true },
    { field: "Is the product being exported?", headBold: true },
    {
      field: "Destination Countries",
      headBold: true,
      renderColumn: (row) => {
        return (
          <>
            {row.destinationCountries === "-"
              ? "-"
              : row.destinationCountries &&
                row.destinationCountries?.map((country) => {
                  return (
                    <Typography sx={commonStyles.countryText} key={country}>
                      {country.title}
                    </Typography>
                  );
                })}
          </>
        );
      },
    },
    { field: "Percentage(%) of export of the product", headBold: true },
    { field: "Total Production Capacity Per Year", headBold: true },
    { field: "Present Production Capacity Per Year", headBold: true },
    { field: "Date Of Commencement", headBold: true },
  ];

  const rawMaterialTableColumn = [
    { field: "Key Raw Material / Bought-out part", headBold: true },
    { field: "Name of raw material", headBold: true },
    { field: "Whether  imported?", headBold: true },
    { field: "Percentage(%) imported", headBold: true },
    {
      field: "Name of country from which the material / part is imported",
      headBold: true,
    },
  ];

  const customerTableColumn = [
    { field: "Name of the customer", headBold: true },
    { field: "Is the customer international or domestic?", headBold: true },
    { field: "Country", headBold: true },
  ];

  const generalAddressTableColumn = [
    { field: "", bold: true },
    { field: "Registered address", headBold: true },
    { field: "Unit address", headBold: true },
  ];

  const pocTableColumn = [
    { field: "Name of the Point of Contact (PoC)", headBold: true },
    { field: "Designation of PoC in the Enterprise", headBold: true },
    { field: "Mobile Number of PoC", headBold: true },
  ];

  useEffect(() => {
    if (
      applicationDetail?.poc_name &&
      applicationDetail?.poc_designation &&
      applicationDetail?.poc_mobile_number
    ) {
      const pocRow = [
        {
          "Name of the Point of Contact (PoC)": applicationDetail?.poc_name,
          "Designation of PoC in the Enterprise":
            applicationDetail?.poc_designation,
          "Mobile Number of PoC": applicationDetail?.poc_mobile_number,
        },
      ];
      setPocTableRow(pocRow);
    }
  }, [
    applicationDetail?.poc_name,
    applicationDetail?.poc_designation,
    applicationDetail?.poc_mobile_number,
  ]);

  useEffect(() => {
    if (
      applicationDetail?.registered_address &&
      applicationDetail?.registered_state?.title &&
      applicationDetail?.registered_district?.title &&
      applicationDetail?.registered_pin &&
      applicationDetail?.registered_mobile_number &&
      applicationDetail?.registered_email &&
      applicationDetail?.unit_address &&
      applicationDetail?.unit_state?.title &&
      applicationDetail?.unit_district?.title &&
      applicationDetail?.unit_pin &&
      applicationDetail?.unit_mobile_number &&
      applicationDetail?.unit_email
    ) {
      let reg_address = `${applicationDetail?.registered_address}\n
        ${applicationDetail?.registered_district?.title}\n
          ${applicationDetail?.registered_state?.title}\n
                  ${applicationDetail?.registered_pin}\n
                      ${applicationDetail?.registered_mobile_number}\n`;
      if (applicationDetail?.registered_landline_number) {
        reg_address = `${reg_address}${applicationDetail?.registered_landline_number}\n`;
      } else {
        reg_address = `${reg_address}-\n`;
      }
      if (applicationDetail?.registered_email) {
        reg_address = `${reg_address}${applicationDetail?.registered_email}`;
      }
      if (applicationDetail?.registered_alternate_email) {
        reg_address = `${reg_address}\n${applicationDetail?.registered_alternate_email}`;
      } else {
        reg_address = `${reg_address}\n-`;
      }

      let unitAddress = `${applicationDetail?.unit_address}\n
      ${applicationDetail?.unit_district?.title}\n
        ${applicationDetail?.unit_state?.title}\n
                ${applicationDetail?.unit_pin}\n
                    ${applicationDetail?.unit_mobile_number}\n`;
      if (applicationDetail?.unit_landline_number) {
        unitAddress = `${unitAddress}${applicationDetail?.unit_landline_number}\n`;
      } else {
        unitAddress = `${unitAddress}-\n`;
      }
      if (applicationDetail?.unit_email) {
        unitAddress = `${unitAddress}${applicationDetail?.unit_email}`;
      }
      if (applicationDetail?.unit_alternate_email) {
        unitAddress = `${unitAddress}\n${applicationDetail?.unit_alternate_email}`;
      } else {
        unitAddress = `${unitAddress}\n-`;
      }

      let addressColumn = `Address\nDistrict\nState\nPincode\nMobile Number\nLandline Number\nEmail\nAlternate email`;

      const rowData = [
        {
          "": addressColumn,
          "Registered address": reg_address,
          "Unit address": unitAddress,
        },
      ];
      setGeneralAddressTableRow(rowData);
    } else {
      setGeneralAddressTableRow([]);
    }
  }, [
    applicationDetail?.registered_address,
    applicationDetail?.registered_state?.title,
    applicationDetail?.registered_district?.title,
    applicationDetail?.registered_pin,
    applicationDetail?.registered_mobile_number,
    applicationDetail?.registered_landline_number,
    applicationDetail?.registered_email,
    applicationDetail?.registered_alternate_email,
    applicationDetail?.unit_address,
    applicationDetail?.unit_state?.title,
    applicationDetail?.unit_district?.title,
    applicationDetail?.unit_pin,
    applicationDetail?.unit_mobile_number,
    applicationDetail?.unit_landline_number,
    applicationDetail?.unit_email,
    applicationDetail?.unit_alternate_email,
  ]);

  useEffect(() => {
    if (applicationDetail?.drdo_storage) {
      setDrdoStorage(JSON.parse(applicationDetail?.drdo_storage));
    } else {
      setDrdoStorage([]);
    }
  }, [applicationDetail?.drdo_storage]);

  useEffect(() => {
    if (applicationDetail?.ownership?.key_domestic_customers) {
      const rowData = JSON.parse(
        applicationDetail?.ownership?.key_domestic_customers
      )?.map((customer, index) => {
        return {
          "Name of the customer": customer.customerName,
          "Is the customer international or domestic?":
            customer.customerType === "1" ? "Domestic" : "International",
          Country: customer.customerCountry,
        };
      });
      setCustomerTableRow(rowData);
    } else {
      setCustomerTableRow([]);
    }
  }, [applicationDetail?.ownership?.key_domestic_customers]);

  useEffect(() => {
    if (applicationDetail?.ownership?.key_raw_materials) {
      const rowData = JSON.parse(
        applicationDetail?.ownership?.key_raw_materials
      )?.map((material, index) => {
        return {
          "Key Raw Material / Bought-out part":
            material.whetherRawMaterial === "1"
              ? "Key Raw Material"
              : "Bought-out part",
          "Name of raw material": material.rawMaterialName,
          "Whether  imported?":
            material.rawMaterialImported === "1" ? "Yes" : "No",
          "Percentage(%) imported":
            material.rawMaterialImported === "1"
              ? material.rawMaterialPercentageImported
              : "-",
          "Name of country from which the material / part is imported":
            material.rawMaterialImported === "1"
              ? material.rawMaterialCountryName
              : "-",
        };
      });
      setRawMaterialTableRow(rowData);
    } else {
      setRawMaterialTableRow([]);
    }
  }, [applicationDetail?.ownership?.key_raw_materials]);

  useEffect(() => {
    if (applicationDetail?.ownership?.product_manufactured) {
      const rowdata = JSON.parse(
        applicationDetail?.ownership?.product_manufactured
      )?.map((product, index) => {
        return {
          "Name of the product": product.productName,
          "Is the product being exported?":
            product.isProductExported === "1" ? "Yes" : "No",
          "Destination Countries": product.destinationCountries,
          "Percentage(%) of export of the product":
            product.percentageExportProduct || "-",
          "Total Production Capacity Per Year": `${product.totalProductionCapacityPerYear} ${product.totalCapacityUnit}`,
          "Present Production Capacity Per Year": `${product.presentProductionCapacityPerYear} ${product.presentCapacityUnit}`,
          "Date Of Commencement": moment(
            new Date(product.dateOfCommencement)
          ).format("D MMM,YYYY"),
          destinationCountries: product.destinationCountries,
          id: index,
          targetName: "product_manufactured",
        };
      });
      setProductTableRow(rowdata);
    } else {
      setProductTableRow([]);
    }
  }, [applicationDetail?.ownership?.product_manufactured]);

  //   const editHandler = () => {
  //     navigate(`/general-info/${id}`);
  //   };

  //   const submitHandler = () => {
  //     dispatch(
  //       submitApplicationAction(applicationDetail?.application_uuid, navigate)
  //     );
  //     // navigate(PAYMENT);
  //   };

  return (
    <>
      {/* <img src={samarLogo} alt="samar" style={commonStyles.watermarked} /> */}
      {/* <Box
        style={{
          ...commonStyles.watermarked,
          // "&:before": {
          //   content: "",
          // },
          backgroundImage: `url(${samarLogo})`,
        }}
      ></Box> */}
      <Grid
        container
        columnSpacing={"2.5rem"}
        sx={commonStyles.viewContainerGrid}
      >
        <Grid item md={6} sm={6} xs={6}>
          <Row
            label="Name of Enterprise"
            value={applicationDetail?.enterprise_name}
          />
          <Row
            label="Date of Enterprise Commencement"
            value={dayjs(
              applicationDetail?.enterprise_commencement_date
            ).format("DD/MM/YYYY")}
          />
          <Row
            label="Site Exterior"
            {...{
              [applicationDetail?.site_exterior_picture?.file_type ===
              "application/pdf"
                ? "pdfUrl"
                : "url"]: applicationDetail?.site_exterior_picture_url,
            }}
          />
          {applicationDetail?.is_existing_vendor == 1 && (
            <Row
              label="DRDO Registration Number"
              value={applicationDetail?.drdo_registration_number}
            />
          )}
          <Row
            label="GST Number of Unit"
            value={applicationDetail?.entrepreneur_gst_number}
          />
          <Row
            label="Enterprise Classification"
            value={
              _.find(typeOfEnterpriseArray, {
                value: applicationDetail?.enterprice_type,
              })?.label
            }
          />
          {applicationDetail?.enterprice_type !== "L" ? (
            <Row
              label="UDYAM Registration Certificate"
              // url={applicationDetail?.udyam_certificate_url}
              {...{
                [applicationDetail?.udyam_certificate?.file_type ===
                "application/pdf"
                  ? "pdfUrl"
                  : "url"]: applicationDetail?.udyam_certificate_url,
              }}
            />
          ) : (
            ""
          )}
        </Grid>
        <Grid item md={6} sm={6} xs={6}>
          <Row
            label="Type of ownership"
            value={
              _.find(natureOfEnterpriseArray, {
                value: applicationDetail?.enterprice_nature,
              })?.label
            }
          />
          <Row
            label="Date of Commencement of Unit"
            value={dayjs(applicationDetail?.unit_commencement_date).format(
              "DD/MM/YYYY"
            )}
          />
          <Row
            label="Are you an existing DRDO vendor?"
            value={`${
              applicationDetail?.is_existing_vendor == 1 ? "Yes" : "No"
            }`}
          />
          {applicationDetail?.is_existing_vendor == 1 && (
            <Row
              label="DRDO Vendor Certificate"
              {...{
                [applicationDetail?.vendor_certificate?.file_type ===
                "application/pdf"
                  ? "pdfUrl"
                  : "url"]: applicationDetail?.vendor_certificate_url,
              }}
            />
          )}
          {applicationDetail?.is_existing_vendor == 1 &&
            applicationDetail?.drdo_category && (
              <Row
                label="Category under DRDO vendor"
                value={
                  _.find(drdoCategoryOptions, {
                    value: applicationDetail?.drdo_category,
                  })?.label
                }
              />
            )}
          <Row
            label="GST Certificate"
            // url={applicationDetail?.entrepreneur_gst_certi_url}
            {...{
              [applicationDetail?.entrepreneur_g_s_t_certificate?.file_type ===
              "application/pdf"
                ? "pdfUrl"
                : "url"]: applicationDetail?.entrepreneur_gst_certi_url,
            }}
          />

          {applicationDetail?.enterprice_type !== "L" ? (
            <Row
              label="UDYAM Registration Number"
              value={applicationDetail?.uam_number}
            />
          ) : (
            ""
          )}
        </Grid>
        <Grid item xs={12} md={12} sm={12}>
          {applicationDetail?.is_existing_vendor == 1 && drdoStorage && (
            <Row
              label="The stores under DRDO vendor registration"
              multiOptions={drdoStorage?.map((item) => item.title)}
            />
          )}
        </Grid>
      </Grid>
      <CustomTable
        columnDefs={pocTableColumn}
        rowData={pocTableRow}
        styleData={commonStyles.tableStyleData}
        boxStyle={{
          ...commonStyles.unsetTableHeight,
          ...customCommonStyles.marginBottomOne,
        }}
      />
      <CustomTable
        columnDefs={generalAddressTableColumn}
        rowData={generalAddressTableRow}
        styleData={commonStyles.tableStyleData}
        boxStyle={{
          ...commonStyles.unsetTableHeight,
          ...customCommonStyles.marginBottomOne,
        }}
        hasSplit
      />

      {applicationDetail?.ownership && (
        <>
          {applicationDetail?.ownership?.third_party_cert && (
            <Row
              label="Third Party Certifications"
              chipValue={applicationDetail?.ownership?.third_party_cert?.split(
                ","
              )}
            />
          )}
          {applicationDetail?.ownership?.key_processes && (
            <Row
              label="Key Processes"
              chipValue={applicationDetail?.ownership?.key_processes?.split(
                ","
              )}
            />
          )}
          {applicationDetail?.ownership?.processes_outsourced && (
            <Row
              label="Outsourced Processes"
              chipValue={applicationDetail?.ownership?.processes_outsourced?.split(
                ","
              )}
            />
          )}
          {applicationDetail?.ownership?.admin_manpower_count && (
            <Row
              label="Number of people employed in the enterprise"
              value={applicationDetail?.ownership?.admin_manpower_count}
            />
          )}
          <Row
            label="Whether blacklisted/banned ever by any competent authority?"
            value={
              applicationDetail?.ownership?.is_competent_banned === 1
                ? "Yes"
                : "No"
            }
          />

          <Row
            label="Any ongoing enquiry/investigation against firm?"
            value={
              applicationDetail?.ownership?.is_enquiry_against_firm === 1
                ? "Yes"
                : "No"
            }
          />
          {productTableRow?.length !== 0 && (
            <>
              <Row label="Product/Articles" />
              {productTableRow?.length !== 0 && (
                <CustomTable
                  columnDefs={productTableColumn}
                  rowData={productTableRow}
                  styleData={commonStyles.tableStyleData}
                  boxStyle={{
                    ...commonStyles.unsetTableHeight,
                    ...customCommonStyles.marginBottomOne,
                  }}
                />
              )}
            </>
          )}

          {customerTableRow?.length !== 0 && (
            <>
              <Row label="Customer Details" />

              {customerTableRow?.length !== 0 && (
                <CustomTable
                  columnDefs={customerTableColumn}
                  rowData={customerTableRow}
                  styleData={commonStyles.tableStyleData}
                  boxStyle={{
                    ...commonStyles.unsetTableHeight,
                    ...customCommonStyles.marginBottomOne,
                  }}
                />
              )}
            </>
          )}

          {rawMaterialTableRow?.length !== 0 && (
            <>
              <Row label="Key Raw Material / Bought-out part" />
              {rawMaterialTableRow?.length !== 0 && (
                <CustomTable
                  columnDefs={rawMaterialTableColumn}
                  rowData={rawMaterialTableRow}
                  styleData={commonStyles.tableStyleData}
                  boxStyle={{
                    ...commonStyles.unsetTableHeight,
                    ...customCommonStyles.marginBottomOne,
                  }}
                />
              )}
            </>
          )}
        </>
      )}
      {/* </Box> */}
    </>
  );
};
export default CustomApplicatonView;
