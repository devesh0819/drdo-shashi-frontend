import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import React, { useState } from "react";

export default function QpRadioButton(props) {
  const { options, styleData } = props;

  return (
    <>
      <RadioGroup
        row
        aria-labelledby="demo-row-radio-buttons-group-label"
        name={props.name}
        onChange={props.onChange}
        value={props.value}
        sx={styleData}
      >
        {options.map((opt, index) => {
          return (
            <FormControlLabel
              key={index}
              value={opt.value}
              control={<Radio size="small" />}
              label={opt.label}
            />
          );
        })}
      </RadioGroup>
    </>
  );
}
