import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  addNoteContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "1rem",
  },
  textAreaContainer: {
    height: "20rem",
    display: "flex",
    alignItems: "start",
    paddingRight: "0",
    // overflow: "auto",
    "& .MuiInputBase-input": {
      height: "19.3rem !important",
      overflow: "auto !important",
      ...commonStyles.customScrollBar,
    },
  },
  noteBox: {
    width: "91%",
  },
};
