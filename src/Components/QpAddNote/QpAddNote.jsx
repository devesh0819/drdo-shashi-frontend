import React, { useState } from "react";
import { Box, InputBase } from "@mui/material";
import QpTypography from "../QpTypography/QpTypography";
import { commonStyles } from "../../Styles/CommonStyles";
import SaveNextButtons from "../SaveNextButtons/SaveNextButtons";
import { styles } from "./QpAddNoteStyles.js";
import { useDispatch } from "react-redux";
import { addNoteAction } from "../../Redux/Admin/Member/memberActions";
import { useNavigate, useParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import * as Yup from "yup";

const meetModalSchema = Yup.object().shape({
  addNote: Yup.string()
    .notRequired()
    .nullable()
    .test(
      "length",
      "This field should be 5 to 3000 characters long",
      function (value) {
        if (value) {
          return value.length >= 5 && value.length <= 3000 ? true : false;
        }
        return true;
      }
    ),
});

export default function QpAddNote(props) {
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
    getValues,
  } = useForm({
    resolver: yupResolver(meetModalSchema),
    mode: "onChange",
  });
  const { closeModal, meetingId } = props;
  const [noteText, setNoteText] = useState("");
  const [noteError, setNoteError] = useState();
  const dispatch = useDispatch();
  const { id } = useParams();
  const navigate = useNavigate();

  const handleCancel = () => {
    closeModal(false);
  };
  const handleSave = (data) => {
    // if (noteText?.length === 0) {
    //   setNoteError("This field is required");
    // } else if (noteText?.length > 255 || noteText?.length < 5) {
    //   setNoteError("This field should be 5 to 255 characters long");
    // } else {
    dispatch(
      addNoteAction(
        meetingId,
        { meeting_notes: data.addNote },
        closeModal,
        id,
        navigate
      )
    );
    // }
  };
  const handleNoteChange = (event) => {
    let newNoteText = event.target.value;
    if (newNoteText?.length === 0) {
      setNoteError("This field is required");
    } else if (newNoteText?.length > 255 || newNoteText?.length < 5) {
      setNoteError("This field should be 5 to 255 characters long");
    } else {
      setNoteText(newNoteText);
    }
  };
  return (
    <>
      <Box sx={styles.addNoteContainer}>
        <Box sx={styles.noteBox}>
          <InputBase
            multiline
            id="add_note"
            name="add_note"
            required
            placeholder="Add Note"
            sx={{
              ...commonStyles.textInputStyle,
              ...styles.textAreaContainer,
            }}
            // {...register("add_note")}
            // error={errors.add_note ? true : false}
            // value={noteText}
            // onChange={(event) => handleNoteChange(event)}
            autoFocus="off"
            {...register("addNote")}
            error={errors.addNote ? true : false}
          />

          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.addNote?.message}
          />
        </Box>
        <SaveNextButtons
          blueButtonText="Save"
          greyButtonText="Cancel"
          onSaveClick={handleCancel}
          onNextClick={handleSubmit(handleSave)}
        />
      </Box>
    </>
  );
}
