import { toast } from "react-toastify";

export const showToast = (message, type, onClick) => {
  toast(message, {
    position: "top-right",
    autoClose: 3000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: false,
    progress: undefined,
    type: type,
    theme: "colored",
    onClick: onClick,
  });
};
