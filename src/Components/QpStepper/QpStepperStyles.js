import StepConnector, {
  stepConnectorClasses,
} from "@mui/material/StepConnector";
import { styled } from "@mui/material/styles";
import * as colors from "../../Constant/ColorConstant";

export const QontoConnector = styled(StepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: "0.5rem",
    left: "calc(-50% + 0.5rem)",
    right: "calc(50% + 0.5rem)",
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: colors.SKY_BLUE,
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: colors.SKY_BLUE,
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    borderColor: colors.GREY,
    borderWidth: "0.625rem",
  },
}));

export const ColorlibStepIconRoot = styled("div")(({ theme, ownerState }) => ({
  backgroundColor: colors.GREY,
  zIndex: 1,
  color: "#ffffff",
  fontWeight: 400,
  fontSize: "1rem",
  lineHeight: "1.375rem",
  display: "flex",
  borderRadius: "50%",
  justifyContent: "center",
  alignItems: "center",
  width: "1.75rem",
  height: "1.75rem",
  ...((ownerState.active || ownerState.completed) && {
    background: colors.SKY_BLUE,
  }),
}));
