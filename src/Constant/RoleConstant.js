export const ROLES = {
  ADMIN: "Admin",
  APPLICANT: "Applicant",
  VENDOR: "Vendor",
  DRDO: "Drdo",
  AGENCY: "Agency",
  ASSESSOR: "Assessor",
  LAB_ADMIN: "LabAdmin",
};

export const APP_STAGE = {
  BASIC: "app_basic",
  FINANCIAL: "app_financial",
  OWNERSHIP: "app_ownership",
  PENDING_APPROVAL: "app_pending_approval",
  APPROVED: "app_approved",
  PAYMENT_DONE: "app_payment_done",
  SCHEDULED: "app_scheduled",
  REJECTED: "app_rejected",
  ASSESSOR_ASSIGNED: "assess_assign",
  PARAMETER_ASSIGNED: "param_assign",
  ASSESSORS_ASSIGNED: "assess_started",
  PAREMETER_ALLOCATED: "asses_init",
  ASSESSMENT_SUBMITTED: "asses_submit",
  ASSESSMENT_REOPEN: "assess_reopen",
  ASSESSMENT_COMPLETED: "asses_complete",
  ASSESSMENT_FEEDBACK: "asses_feed_sub",
  APPLICATION_ASSESSMENT_COMPLETED: "app_assess_complete",
  FEEDBACK_SUBMITTED: "app_feedback_complete",
  PAYMENT_REVIEW: "app_payment_review",
  PAYMENT_APPROVED: "app_payment_approved",
};

export const PARAM_TYPE = {
  PLANNING: "P",
  DEPLOYMENT: "D",
  MONITORING: "M",
};
