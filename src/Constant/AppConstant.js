export const PASSWORD_REGEX_WITH_SPECIAL_CHARACTERS =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,20}$)/;
export const NO_SPACE_AT_START = /^[^-\s][a-zA-Z0-9_\s-]+$/;
export const WITHOUT_SPACE =
  /^\w[a-zA-Z!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~0-9.]*$/;
export const ONLY_NUMERIC = /^[0-9]{10}/;
export const ATLEAST_ONE_NUMERIC = /\d/;
export const ATLEAST_ONE_UPPERCASE = /[A-Z]/;
export const ATLEAST_ONE_LOWERCASE = /[a-z]/;
export const ATLEAST_ONE_SPECIAL = /\W/;
export const NO_SPACE_AT_START_END = /^[^\s]+(\s+[^\s]+)*$/;
export const PINCODE_REGEX = /(^[1-9][0-9]{5}$)/;
export const EMAIL_REGEX =
  /^\s*[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)\s*$/;
export const MOBILE_REGEX = /(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/;
export const LANDLINE_REGEX = /(^\d{5}([- ]*)\d{6}$)/;
export const PAN_REGEX = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
export const AADHAR_REGEX = /(^[2-9]{1}\d{3}\s\d{4}\s\d{4}$)/;
export const GST_REGEX =
  /(^\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}$)/;
export const LENGTH_REGEX = /^.{10}$/;
export const IFSC_REGEX = /^[A-Z]{4}0[A-Z0-9]{6}$/;
export const ONLY_VALID_NUMERIC = /^[1-9]/;
export const ONLY_VALID_NUMERIC_WITH_ZERO = /^[0-9]+$/;
export const WEB_URL_REGEX =
  /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&=]*)/;
export const ONLY_CHARACTERS = /^[a-zA-Z ]+$/;
export const WITHOUT_SPECIAL_CHARACTERS = /^[a-zA-Z0-9 ]+$/;
export const DECIMAL_VALUES = /^\d+(\.\d{1,2})?$/;
export const TITLE_LENGTH = 28;
export const UDYAM_REGEX = /^UDYAM-[A-Z]{2}-\d{2}-\d{7}$/;

// export const PASSWORD_ENCRYPTION_SECRET =
//   "xhma2ivLxVbq66aItXaaYTcUn2PFRlknWeseqvrgSj0=";

export const registrationOptions = [
  { label: "Step 1", index: 1 },
  { label: "Step 2", index: 2 },
  // { label: "Step 3", index: 3 },
];

export const socialCategoryOptions = [
  { label: "General", value: "GN" },
  { label: "SC", value: "SC" },
  { label: "ST", value: "ST" },
  { label: "OBC", value: "OBC" },
];

export const specialTypeOptions = [
  { label: "Ex Servicemen", value: "EXS" },
  { label: "Person with disability", value: "PWD" },
  { label: "Woman", value: "WOM" },
  { label: "Minority", value: "MIN" },
  { label: "None", value: "NONE" },
];

export const natureOfEnterpriseArray = [
  { label: "Proprietary", value: "Prop" },
  { label: "Private Limited", value: "PL" },
  { label: "Public Limited", value: "Pub" },
  // { label: "Limited", value: "Lim" },
  { label: "Partnership", value: "Part" },
  { label: "Joint Venture", value: "Join" },
  { label: "Limited Liability Partnership", value: "LimPart" },
];

export const enterpriseType = [
  { label: "Indian", value: "Indian" },
  { label: "Foreigner", value: "Foreigner" },
  { label: "Joint Venture", value: "Joint_Venture" },
];

export const natureOfBusinessArray = [
  { label: "Manufacturer", value: "manufacturer" },
  { label: "Fabricator", value: "fabricator" },
  { label: "Assembler", value: "assembler" },
];

export const typeOfEnterpriseArray = [
  { label: "Micro", value: "MIC" },
  { label: "Small", value: "SM" },
  { label: "Medium", value: "MED" },
  { label: "Large", value: "L" },
];

export const drdoApplicationsStatus = [
  { label: "Approval Pending", value: "approval_pending" },
  { label: "Approved", value: "approved" },
  { label: "Rejected", value: "rejected" },
];

export const states = [
  { label: "Punjab", value: "Punjab" },
  { label: "Himachal Pradesh", value: "Himachal Pradesh" },
  { label: "Haryana", value: "Haryana" },
  { label: "Delhi", value: "Delhi" },
];

export const assessorStatus = [
  { label: "Active", value: "active" },
  { label: "On-Hold", value: "onhold" },
  { label: "Blocked", value: "blocked" },
];

export const unitCategories = [
  { label: "Red", value: "R" },
  { label: "Orange", value: "O" },
  { label: "Green", value: "G" },
  { label: "White", value: "W" },
  { label: "Not applicable", value: "NA" },
];

export const yesNo = [
  { value: 1, label: "Yes" },
  { value: 0, label: "No" },
];

export const tdsValues = [
  { label: "0", value: "0" },
  { label: "2", value: "2" },
  { label: "10", value: "10" },
];

export const leasedOwned = [
  { value: 1, label: "Leased" },
  { value: 0, label: "Owned" },
];

export const customerType = [
  { value: 1, label: "Domestic" },
  { value: 0, label: "International" },
];

export const materialPartsOption = [
  { value: 1, label: "Key Raw Material" },
  { value: 0, label: "Bought Out Parts" },
];

export const testingMethod = [
  { value: 1, label: "In-house" },
  { value: 0, label: "External" },
];

export const thirdPartyFinanceAuditOption = [
  { label: "Yes", value: "Yes" },
  { label: "No", value: "No" },
];

export const energySourceUsedOption = [
  { label: "Electricity", value: "Electricity" },
  { label: "Petrol", value: "Petrol" },
  { label: "Diesel", value: "Diesel" },
  { label: "Coal", value: "Coal" },
  { label: "Solar", value: "Solar" },
  { label: "Wood", value: "Wood" },
  { label: "Natural gas", value: "Natural gas" },
  { label: "Other", value: "Other" },
];

export const finalRegistrationNumber = [
  { label: "UAM", value: "UAM" },
  { label: "EM2", value: "EM2" },
  { label: "SSI", value: "SSI" },
  { label: "Other", value: "Other" },
];

export const meetApplication = [
  { label: "Online", value: "online" },
  { label: "Offline", value: "offline" },
];

export const units = ["Numbers", "Kilogram", "Liters"];

export const certificationLevel = [{ label: "Beginer", value: "Beginer" }];

export const appStatus = [
  { label: "Yet to submit", value: "app_basic" },
  { label: "Yet to submit", value: "app_financial" },
  { label: "Yet to submit", value: "app_ownership" },
  { label: "DRDO Approval pending", value: "app_pending_approval" },
  { label: "DRDO Approved", value: "app_approved" },
  { label: "Assessment Scheduled", value: "app_scheduled" },
  { label: "Yet to be Scheduled", value: "app_payment_done" },
  { label: "In process", value: "in_process" },
  { label: "Rejected", value: "app_rejected" },
  { label: "Assessment In progress", value: "started" },
  { label: "Assessor Assigned", value: "assess_assign" },
  { label: "Parameter Assigned", value: "param_assign" },
  { label: "Parameters Allocated", value: "asses_init" },
  { label: "Assessment Submitted", value: "asses_submit" },
  { label: "Assessment Reopened", value: "asses_reopen" },
  { label: "Assessment Completed", value: "asses_complete" },
  { label: "Assessment Feedback Submitted", value: "asses_feed_sub" },
  { label: "Assessors Assigned", value: "assess_started" },
  { label: "Assessment completed ", value: "app_assess_complete" },
  { label: "Feedback Submitted", value: "app_feedback_complete" },
  { label: "Application Payment Reviewed", value: "app_payment_review" },
  { label: "Payment Pending", value: "app_payment_approved" },
];

export const appStatusAfterSubmit = [
  { label: "DRDO Approval Pending", value: "app_pending_approval" },
  { label: "DRDO Approved", value: "app_approved" },
  { label: "Assessment Scheduled", value: "app_scheduled" },
  { label: "Yet to be Scheduled", value: "app_payment_done" },
  { label: "In progess", value: "in_process" },
  { label: "Rejected", value: "app_rejected" },
  { label: "Assessment completed ", value: "app_assess_complete" },
  { label: "Feedback Submitted", value: "app_feedback_complete" },
  { label: "Application Payment Reviewed", value: "app_payment_review" },
  { label: "Payment Pending", value: "app_payment_approved" },
];
export const vendorStatus = [
  { label: "Yet to Submit", value: "in_progress" },
  { label: "DRDO Approval pending", value: "app_pending_approval" },
  { label: "DRDO Approved", value: "app_approved" },
  { label: "Assessment Scheduled", value: "app_scheduled" },
  { label: "Yet to be Scheduled", value: "app_payment_done" },
  { label: "Rejected", value: "app_rejected" },
  { label: "Feedback Submitted", value: "app_feedback_complete" },
  { label: "Payment Pending", value: "app_payment_approved" },
  { label: "Assessment In Progress", value: "app_assess_in_progress" },
  { label: "Assessment Completed", value: "app_assess_complete" },
];
export const assessmentStatus = [
  { label: "Parameters Allocated", value: "asses_init" },
  { label: "Assessment Submitted", value: "asses_submit" },
  { label: "Assessment Completed", value: "asses_complete" },
  { label: "Assessors Assigned", value: "assess_started" },
  { label: "Assessment Feedback Submitted ", value: "asses_feed_sub" },
  // { label: "Application assessment completed ", value: "app_assess_complete" },
  // { label: "Feedback Submitted", value: "app_feedback_complete" },
];
export const paymentStatus = [
  { label: "Paid", value: "paid" },
  { label: "Yet to Pay", value: "yet_to_pay" },
];
export const ratingArray = [
  { label: "Level 1", value: "bronze" },
  { label: "Level 2", value: "silver" },
  { label: "Level 3", value: "gold" },
  { label: "Level 4", value: "diamond" },
  { label: "Level 5", value: "platinum" },
  { label: "Yet to be Assessed", value: 0 },
];
export const feedbackQuestions = [
  { label: "Professionalism of assessors", value: "assessor_professinalism" },
  { label: "Knowledge of assessors", value: "assessor_knowledge" },
  { label: "Overall assessment process", value: "overall_process" },
  { label: "Usefulness of the assessment ", value: "assessment_usefulness" },
];
export const UserData = [
  {
    id: 1,
    year: 2016,
    userGain: 80000,
    userLost: 823,
  },
  {
    id: 2,
    year: 2017,
    userGain: 45677,
    userLost: 345,
  },
  {
    id: 3,
    year: 2018,
    userGain: 78888,
    userLost: 555,
  },
  {
    id: 4,
    year: 2019,
    userGain: 90000,
    userLost: 4555,
  },
  {
    id: 5,
    year: 2020,
    userGain: 4300,
    userLost: 234,
  },
];

export const StackedBarChartData = [
  {
    id: 1,
    year: 2016,
    userGain: 8000,
    userLost: 5000,
  },
  {
    id: 2,
    year: 2017,
    userGain: 4567,
    userLost: 3453,
  },
  {
    id: 3,
    year: 2018,
    userGain: 7888,
    userLost: 5551,
  },
  {
    id: 4,
    year: 2019,
    userGain: 9000,
    userLost: 4555,
  },
  {
    id: 5,
    year: 2020,
    userGain: 4300,
    userLost: 2341,
  },
  {
    id: 6,
    year: 2021,
    userGain: 4300,
    userLost: 2341,
  },
];

export const DRAFT = "draft";
export const SUBMITTED = "sumitted";
export const REJECTED = "rejected";
export const PENDING_APPROVAL = "approval_pending";
export const APPROVED = "approved";

export const questionairreStatus = [
  { label: "Pending", value: DRAFT },
  { label: "Submitted", value: SUBMITTED },
  { label: "Sent for Approval", value: PENDING_APPROVAL },
  { label: "Approved", value: APPROVED },
  { label: "Rejected", value: REJECTED },
];

export const maturityTableRow = [
  { label: "Level 1", data: "Largely ad hoc processes" },
  { label: "Level 2", data: "Process of standardisation has just begun" },
  { label: "Level 3", data: "Processes are standardised" },
  {
    label: "Level 4",
    data: "Processes are standardised, measured and controlled",
  },
  {
    label: "Level 5",
    data: "Processes are standardised, measured and continually improved for optimization",
  },
];

export const certificationTableRow = [
  { enterprise_type: "Micro & Small", man_days: 6, fees: "1,27,840/-" },
  { enterprise_type: "Small", man_days: 6, fees: "1,27,840/-" },
  { enterprise_type: "Medium", man_days: 8, fees: "1,56,240/-" },
  { enterprise_type: "Large", man_days: 10, fees: "1,84,640/-" },
];

export const drdoCategoryOptions = [
  { label: "Fabricator / Development Partner", value: "FDP" },
  { label: "Manufacturers / Distributors (MFD)", value: "MFD" },
  { label: "Fabrication / Production Agency (FPA)", value: "FPA" },
  { label: "Development and Production Agency (DPA)", value: "DPA" },
  { label: "Design, Development and Production Agency (DDP)", value: "DDP" },
  { label: "Others", value: "others" },
];
export const drdoMultiOptions = [
  // { id: 0, title: "Select All" },
  {
    id: 1,
    title: "Access Control Systems",
  },
  {
    id: 2,
    title: "Accessories",
  },
  {
    id: 3,
    title: "Acoustic Systems",
  },
  {
    id: 4,
    title: "Acoustics",
  },
  {
    id: 5,
    title: "Adhesives",
  },
  {
    id: 6,
    title: "Aeronautics",
  },
  {
    id: 7,
    title: "Air Conditioning, Cooling Systems",
  },
  {
    id: 8,
    title: "Aircraft Jet Engines",
  },
  {
    id: 9,
    title: "Aircrafts & Aircraft Components",
  },
  {
    id: 10,
    title: "Aluminium Structure",
  },
  {
    id: 11,
    title: "Ammunition & Explosive",
  },
  {
    id: 12,
    title: "Amplifiers",
  },
  {
    id: 13,
    title: "Analytical Balance",
  },
  {
    id: 14,
    title: "Automobiles And Auto Spares",
  },
  {
    id: 15,
    title: "Automotive Spare Parts",
  },
  {
    id: 16,
    title: "Batteries",
  },
  {
    id: 17,
    title: "Batteries and Load Cells",
  },
  {
    id: 18,
    title: "Bearing",
  },
  {
    id: 19,
    title: "Bomb Detection & Disposal Equipment",
  },
  {
    id: 20,
    title: "Brushes",
  },
  {
    id: 21,
    title: "Cables",
  },
  {
    id: 22,
    title: "Calipers",
  },
  {
    id: 23,
    title: "Capacitors",
  },
  {
    id: 24,
    title: "Carbon Graphite Products, Graphite",
  },
  {
    id: 25,
    title: "Castings",
  },
  {
    id: 26,
    title: "Chemical Analyzers, Lab Equipment / Instruments",
  },
  {
    id: 27,
    title: "Chemicals & Polymers",
  },
  {
    id: 28,
    title: "Civil Works / Construction Materials",
  },
  {
    id: 29,
    title: "Clothings",
  },
  {
    id: 30,
    title: "Combat Aircraft",
  },
  {
    id: 31,
    title: "Communication Systems",
  },
  {
    id: 32,
    title: "Components",
  },
  {
    id: 33,
    title: "Composites",
  },
  {
    id: 34,
    title: "Compressor",
  },
  {
    id: 35,
    title: "Computer Accessories & LAN Related EQPTS / Furniture",
  },
  {
    id: 36,
    title: "Computer Hardware",
  },
  {
    id: 37,
    title: "Computer Related Consumable",
  },
  {
    id: 38,
    title: "Computer Tables",
  },
  {
    id: 39,
    title: "Computers",
  },
  {
    id: 40,
    title: "Computers Accessories And LAN Related EQPTS / Furniture",
  },
  {
    id: 41,
    title: "Consumable",
  },
  {
    id: 42,
    title: "Control Systems",
  },
  {
    id: 43,
    title: "Controlled Environment Test Chambers",
  },
  {
    id: 44,
    title: "Controlled Environment Testing",
  },
  {
    id: 45,
    title: "Copper",
  },
  {
    id: 46,
    title: "Cutter",
  },
  {
    id: 47,
    title: "Data Acquisition System",
  },
  {
    id: 48,
    title: "Diesel Generators Sets",
  },
  {
    id: 49,
    title: "Digital Oscilloscope",
  },
  {
    id: 50,
    title: "Digital Signal Processing System",
  },
  {
    id: 51,
    title: "Diodes",
  },
  {
    id: 52,
    title: "Displays & Readouts",
  },
  {
    id: 53,
    title: "Drafting And Drawing Equipment",
  },
  {
    id: 54,
    title: "Drill",
  },
  {
    id: 55,
    title: "Dynamometers",
  },
  {
    id: 56,
    title: "Electrical Equipment & Accessories",
  },
  {
    id: 57,
    title: "Electrical Equipment And Instruments",
  },
  {
    id: 58,
    title: "Electrical Fittings And Goods",
  },
  {
    id: 59,
    title: "Electrical Machines And Accessories",
  },
  {
    id: 60,
    title: "Electro optic connectors",
  },
  {
    id: 61,
    title: "Electronic & Coomunication Systems",
  },
  {
    id: 62,
    title: "Electronics & Communication System",
  },
  {
    id: 63,
    title: "Encoders",
  },
  {
    id: 64,
    title: "Environ Test Equipment",
  },
  {
    id: 65,
    title: "Environmental Cold Chambers",
  },
  {
    id: 66,
    title: "Fasterners",
  },
  {
    id: 67,
    title: "Fiber Optics",
  },
  {
    id: 68,
    title: "Filters",
  },
  {
    id: 69,
    title: "Fire Extinguishers",
  },
  {
    id: 70,
    title: "Fire Fighting / Security Equipment",
  },
  {
    id: 71,
    title: "Fittings",
  },
  {
    id: 72,
    title: "Forgings",
  },
  {
    id: 73,
    title: "FRP",
  },
  {
    id: 74,
    title: "Fuel",
  },
  {
    id: 75,
    title: "Fuel, Oil And Lubricants",
  },
  {
    id: 76,
    title: "Furnace",
  },
  {
    id: 77,
    title: "Gauges",
  },
  {
    id: 78,
    title: "Gear Box",
  },
  {
    id: 79,
    title: "General Engineering",
  },
  {
    id: 80,
    title: "Glass",
  },
  {
    id: 81,
    title: "Glassware, Apparatus And Filter Papers",
  },
  {
    id: 82,
    title: "Grinding Wheels",
  },
  {
    id: 83,
    title: "Guns",
  },
  {
    id: 84,
    title: "Hardness Testers",
  },
  {
    id: 85,
    title: "High Speed Camera",
  },
  {
    id: 86,
    title: "Ho Hand / Portable Oscilloscope",
  },
  {
    id: 87,
    title: "Hoses",
  },
  {
    id: 88,
    title: "Hydraulic Sub Systems",
  },
  {
    id: 89,
    title: "Human Safety Equipment",
  },
  {
    id: 90,
    title: "Hydraulic & Pneumatic Pumps / Jacks",
  },
  {
    id: 91,
    title: "Hydraulic Components",
  },
  {
    id: 92,
    title: "Igniters",
  },
  {
    id: 93,
    title: "Impact Testing Machine",
  },
  {
    id: 94,
    title: "Indicators",
  },
  {
    id: 95,
    title: "Industrial Chemicals And Lab Chemicals",
  },
  {
    id: 96,
    title: "Industrial Shoes",
  },
  {
    id: 97,
    title: "Infrared Equipment",
  },
  {
    id: 98,
    title: "Instrumentation Systems",
  },
  {
    id: 99,
    title: "Integrated Circuits",
  },
  {
    id: 100,
    title: "Isolation Instrumentation",
  },
  {
    id: 101,
    title: "IT Hardware",
  },
  {
    id: 102,
    title: "Job Work",
  },
  {
    id: 103,
    title: "Laser",
  },
  {
    id: 104,
    title: "LED / LCD Display Systems",
  },
  {
    id: 105,
    title: "Liveries",
  },
  {
    id: 106,
    title: "Machining",
  },
  {
    id: 107,
    title: "Material Handling Equipment",
  },
  {
    id: 108,
    title: "Mechanical Items & Equipment",
  },
  {
    id: 109,
    title: "Mechanical Reprographics",
  },
  {
    id: 110,
    title: "Mechanical Seals",
  },
  {
    id: 111,
    title: "Mechatronic Equipment",
  },
  {
    id: 112,
    title: "Mechatronics",
  },
  {
    id: 113,
    title: "Medical Equipment",
  },
  {
    id: 114,
    title: "Medical Equipment & Medicines",
  },
  {
    id: 115,
    title: "Metal Matrix Composites",
  },
  {
    id: 116,
    title: "Metallic Hoses Tubes & Pipes",
  },
  {
    id: 117,
    title: "Metallic Materials",
  },
  {
    id: 118,
    title: "Missiles & Related Systems",
  },
  {
    id: 119,
    title: "Mobile Communication Devices",
  },
  {
    id: 120,
    title: "Mufflers",
  },
  {
    id: 121,
    title: "Multimedia Products",
  },
  {
    id: 122,
    title: "Naval Systems",
  },
  {
    id: 123,
    title: "NBC Equipment",
  },
  {
    id: 124,
    title: "Network Analyzer",
  },
  {
    id: 125,
    title: "Networking Accessories",
  },
  {
    id: 126,
    title: "Non-Metallic Materials",
  },
  {
    id: 127,
    title: "Office Chair / Table",
  },
  {
    id: 128,
    title: "Office Furniture & Furnishing",
  },
  {
    id: 129,
    title: "Office Stationery, Printing, Binding & Drawing Stationery",
  },
  {
    id: 130,
    title: "Optics & Optical Equipment",
  },
  {
    id: 131,
    title: "Oscillators",
  },
  {
    id: 132,
    title: "Oscilloscopes & Signal Generators",
  },
  {
    id: 133,
    title: "Other",
  },
  {
    id: 134,
    title: "Packing Materials",
  },
  {
    id: 135,
    title: "Paints",
  },
  {
    id: 136,
    title: "Paper",
  },
  {
    id: 137,
    title: "PCB Sensors",
  },
  {
    id: 138,
    title: "Pen / Pencil",
  },
  {
    id: 139,
    title: "Photographic Equipment",
  },
  {
    id: 140,
    title: "Plants & Machineries",
  },
  {
    id: 141,
    title: "Plastic, Polymers, Polycarbonate",
  },
  {
    id: 142,
    title: "Plastic / Rubber Products & Manufacturing Of Rubber / Plastics",
  },
  {
    id: 143,
    title: "Pneumatic Components",
  },
  {
    id: 144,
    title: "Pneumatic Sub Systems",
  },
  {
    id: 145,
    title: "Power Supplies",
  },
  {
    id: 146,
    title: "Printer, Plotter, Scanners",
  },
  {
    id: 147,
    title: "Propellants",
  },
  {
    id: 148,
    title: "Reprographics",
  },
  {
    id: 149,
    title: "Robotic Systems",
  },
  {
    id: 150,
    title: "Roofing",
  },
  {
    id: 151,
    title: "Rubber Products",
  },
  {
    id: 152,
    title: "Safety Clothings",
  },
  {
    id: 153,
    title: "Satellite Receiving Stations",
  },
  {
    id: 154,
    title: "Security Cameras & Systems",
  },
  {
    id: 155,
    title: "Sensors",
  },
  {
    id: 156,
    title: "Servers",
  },
  {
    id: 157,
    title: "Spares",
  },
  {
    id: 158,
    title: "Spares & Sub-systems",
  },
  {
    id: 159,
    title: "Spring Testers",
  },
  {
    id: 160,
    title: "Stabilizers",
  },
  {
    id: 161,
    title: "Stainless Steel Structure",
  },
  {
    id: 162,
    title: "Standard Spares",
  },
  {
    id: 163,
    title: "Stapler",
  },
  {
    id: 164,
    title: "Stationery",
  },
  {
    id: 165,
    title: "Steel",
  },
  {
    id: 166,
    title: "Steel & Wooden Office Furniture (For Computer Furniture)",
  },
  {
    id: 167,
    title: "Strategic Items",
  },
  {
    id: 168,
    title: "Structural Steel",
  },
  {
    id: 169,
    title: "Sub Assembly",
  },
  {
    id: 170,
    title: "Switches",
  },
  {
    id: 171,
    title: "Telephone Exchange And Equipments",
  },
  {
    id: 172,
    title: "Temperature Chamber",
  },
  {
    id: 173,
    title: "Temperature Control System",
  },
  {
    id: 174,
    title: "Temperature Indicator",
  },
  {
    id: 175,
    title: "Test Kit",
  },
  {
    id: 176,
    title: "Testing & Callibration Equipments",
  },
  {
    id: 177,
    title: "Testing & Measuring Equipments",
  },
  {
    id: 178,
    title: "Testing Equipments",
  },
  {
    id: 179,
    title: "Timber & Plywood",
  },
  {
    id: 180,
    title: "Timers",
  },
  {
    id: 181,
    title: "Tools And Hardware",
  },
  {
    id: 182,
    title: "Torpedoes (Thermal Torpedoes (Close Cycles))",
  },
  {
    id: 183,
    title: "Transducers",
  },
  {
    id: 184,
    title: "Transmitters",
  },
  {
    id: 185,
    title: "Transport",
  },
  {
    id: 186,
    title: "Tubes And Tubing",
  },
  {
    id: 187,
    title: "UAVs (Tactical)",
  },
  {
    id: 188,
    title: "UPS",
  },
  {
    id: 189,
    title: "Vacuums Furnace",
  },
  {
    id: 190,
    title: "Valves",
  },
  {
    id: 191,
    title: "Vehicles",
  },
  {
    id: 192,
    title: "VLSO, Embedded Systems",
  },
  {
    id: 193,
    title: "VSAR, Satellite Phones",
  },
  {
    id: 194,
    title: "Warm Shafts / Gears / Gear Boxes",
  },
  {
    id: 195,
    title: "Weapons & Weapon Platforms",
  },
  {
    id: 196,
    title: "Welding Machines & Accessories",
  },
  {
    id: 197,
    title: "Workshop Machine Tools & Tool Kits",
  },
  {
    id: 198,
    title: "Workshop Raw Materials",
  },
];
export const roleOptionsArray = [
  { label: "Chairperson", value: "Chairperson" },
  { label: "Member", value: "Member" },
  { label: "Convener", value: "Convener" },
];

export const firstDeclaration =
  "The above information is true to the best of my knowledge and I agree that in case, any information provided above is found to be false, my application, rating and certification, as applicable, may be cancelled or withdrawn and action, as deemed fit may be taken by DRDO or QCI.";

export const secondDeclaration =
  " I agree that acceptance of my application shall be at the sole discretion of DRDO or QCI.";
