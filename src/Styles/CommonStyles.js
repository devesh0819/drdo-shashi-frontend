import { relativeTimeRounding } from "moment";
import {
  BACKGROUND_LIGHT_BLUE,
  BLACKISH,
  DARK_BLUE,
  WHITE,
  METALLIC_SILVER,
  BLUE,
  BLACK,
  DEFAULT_GREY,
  ICON_BLUE,
  ICON_GREY,
} from "../Constant/ColorConstant";

export const commonStyles = {
  customScrollBar: {
    "&::-webkit-scrollbar": {
      width: "0.3rem",
      height: "0.3rem",
    },
    "&::-webkit-scrollbar-track": {
      boxShadow: "inset 0 0 0.375rem rgba(0,0,0,0.00)",
      webkitBoxShadow: "inset 0 0 0.375rem rgba(0,0,0,0.00)",
      width: "0.438rem",
      borderRadius: "0.75rem",
      // background: SILVER_GREY,
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "rgba(0,0,0,.1)",
      borderRadius: "0.75rem",
      background: METALLIC_SILVER,
    },
  },
  textStyle: {
    fontFamily: "Open Sans, sans-serif",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1rem",
    lineHeight: "1.375rem",
    color: WHITE,
    "@media(max-width: 600px)": {
      fontSize: "0.625rem",
    },
  },
  iconColor: {
    color: ICON_BLUE,
    cursor: "pointer",
    width: "1.25rem",
    height: "1.25rem",
  },
  greyIconColor: {
    color: ICON_GREY,
    cursor: "not-allowed",
    width: "1.25rem",
    height: "1.25rem",
  },
  iconButtonStyle: {
    minWidth: "unset",
    padding: 0,
    marginRight: "1.75rem",
    "&:hover": { backgroundColor: "transparent" },
  },
  committeeContainer: {
    paddingLeft: "1.063rem",
    paddingRight: "1.063rem",
  },
  displayNone: {
    display: "none",
  },
  adminApplicationIcon: {
    width: "160px",
  },
  vendorApplicationIcon: {
    width: "160px",
  },
  footerStyle: {
    background: DARK_BLUE,
    position: "fixed",
    bottom: "0",
    width: "100%",
    // width: "calc(100vw - 2.188rem)",
    paddingY: "1.188rem",
    paddingX: "1.188rem",
    textAlign: "center",
    marginTop: "1.375rem",
    "@media(max-width: 600px)": {
      padding: "0.75rem",
    },
  },
  bodyContainer: {
    backgroundColor: BACKGROUND_LIGHT_BLUE,
    height: "calc(100vh - 3.75rem)",
    "@media(max-width: 600px)": {
      height: "calc(100vh - 2.875rem)",
    },
  },
  dividerStyle: {
    marginLeft: "2.125rem",
    marginRight: "5rem",
    // height: "36rem",
    height: "99%",
    border: "0.063rem solid #6EA0EC",
    background: "#6EA0EC",
    marginTop: "0",
    marginBottom: "0",
  },
  signUpText: {
    fontWeight: "700",
    fontSize: "2.125rem",
    lineHeight: "2.875rem",
    color: BLACKISH,
    "@media(max-width:600px)": {
      fontSize: "1.5rem",
    },
  },
  alreadySignUpText: {
    fontWeight: "700",
    fontSize: "1.125rem",
    lineHeight: "1.563rem",
    color: "#454545",
    margin: "0.375rem",
    "@media(max-width:600px)": {
      fontSize: "0.875rem",
    },
  },
  displayStyle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  loginText: {
    color: BLUE,
    cursor: "pointer",
  },
  inputLabel: {
    fontWeight: 600,
    fontSize: "0.938rem",
    lineHeight: "1.25rem",
    color: BLACKISH,
    marginBottom: "0.438rem",
    "& .MuiFormLabel-asterisk": {
      color: "red",
    },
  },
  inputStyle: {
    background: "#F8FBFF",
    border: "0.063rem solid #D0E8FF",
    borderRadius: "0.313rem",
    width: "100%",
    height: "2.25rem",
  },
  signUpFields: {
    // marginTop: "1rem",
    width: "100%",
  },
  createAccountButton: {
    width: "100%",
    marginTop: "1.5rem",
    height: "3.125rem",
    color: WHITE,
    backgroundColor: "#3278B9",
    marginLeft: "4rem",
    textTransform: "none",
    "&:hover": {
      backgroundColor: "#3278B9",
    },
  },
  fullWidth: {
    width: "100%",
    // alignSelf: "center",
    // display: "flex",
    "@media(min-width:1100px)": {
      // alignSelf: "center",
      // display: "flex",
      // alignItems: "center",
    },
    "@media(max-width:1099px) and (min-width:900px)": {
      // alignSelf: "center",
      // display: "flex",
      // alignItems: "center",
    },
  },
  signUpTextContainer: {
    marginBottom: "1rem",
  },
  errorText: {
    color: "red",
    width: "100%",
    marginTop: "0.2rem",
    lineHeight: "0.7rem",
    height: "1rem",
    fontSize: "0.75rem",
  },
  signupEnterOtpContainer: {
    background: WHITE,
    boxShadow: "0px 2px 16px 2px rgba(126, 126, 126, 0.1)",
    borderRadius: "0.625rem",
    width: "34.75rem",
    "@media(max-width:600px)": {
      height: "80%",
      width: "calc(100vw - 3rem)",
    },

    // height: "inherit",
  },
  otpContainer: {
    height: "calc(100vh - 11.5rem)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    overflow: "scroll",
  },
  enterOtpText: {
    color: BLACKISH,
    fontSize: "2rem",
    lineHeight: "2.75rem",
    fontWeight: 700,
    textAlign: "center",
    fontFamily: "Open Sans",
    marginBottom: "1.25rem",
  },
  displayCenterStyle: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  verifyOtpText: {
    fontWeight: 600,
    fontSize: "1rem",
    lineHeight: "1.375rem",
    textAlign: "center",
    fontFamily: "Open Sans",
    marginBottom: "0.313rem",
  },
  digitCodeText: {
    fontWeight: 600,
    fontSize: "1rem",
    lineHeight: "1.375rem",
    textAlign: "center",
    color: DEFAULT_GREY,
    fontFamily: "Open Sans",
    marginBottom: "1rem",
  },
  validateButton: {
    backgroundColor: BLUE,
    marginTop: "2.938rem",
    marginBottom: "2.125rem",
    color: WHITE,
    fontWeight: 700,
    height: "3.125rem",
    width: "100%",
    textTransform: "none",
    "&:hover": {
      backgroundColor: BLUE,
    },
  },
  resendPasswordText: {
    color: "#6A6A6A",
    fontWeight: 700,
    fontSize: "1rem",
    lineHeight: "1.375rem",
    textAlign: "center",
  },
  enableResendPasswordText: {
    color: "rgb(25, 68, 120)",
    textDecoration: "underline",
    fontWeight: 700,
    fontSize: "1rem",
    lineHeight: "1.375rem",
    textAlign: "center",
    cursor: "pointer",
  },
  timer: {
    color: "#6A6A6A",
    fontWeight: 500,
    fontSize: "1rem",
    lineHeight: "1.375rem",
    textAlign: "center",
  },
  forgotPasswordText: {
    fontWeight: 700,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    color: BLUE,
    paddingLeft: "4rem",
  },
  forgotEmailStyle: {
    width: "27.5rem",
    height: "3.125rem",
    marginTop: "0.813rem",
    border: "0.063rem solid #A9A9A9",
    borderRadius: "0.313rem",
    "@media(max-width:600px)": {
      width: "80%",
    },
  },
  enterEmailText: {
    fontWeight: 600,
    fontSize: "1.125rem",
    lineHeight: "1.563rem",
    color: DEFAULT_GREY,
  },
  validateButtontext: {
    color: WHITE,
    fontWeight: 700,
    fontSize: "1.125rem",
    lineHeight: "1.563rem",
  },
  goToPageText: {
    fontWeight: 400,
    fontSize: "1.125rem",
    lineHeight: "1.563rem",
  },
  goToPageTextColor: {
    color: "#454545",
  },
  goToLoginText: {
    color: BLUE,
    textDecoration: "underline",
    cursor: "pointer",
  },
  securityCheckText: {
    fontWeight: 600,
    fontSize: "0.938rem",
    lineHeight: "1.25rem",
    color: "#000000",
    marginBottom: "0.3rem",
  },
  securityCheckTextCenter: {
    textAlign: "center",
  },
  enterCaptchaText: {
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    color: "#323A41",
    margin: "0.75rem 0",
  },
  enterCaptchaContainer: {
    padding: "0.3rem",
    width: "50%",
    background: "#F8FBFF",
    border: "1px solid #D5EAFF",
    borderRadius: "2px",
    "@media(max-width:600px)": {
      width: "88%",
    },
  },
  captchaContainerFullWidth: {
    width: "100%",
  },
  captchaContainer: {
    // width: "20.813rem",
    background: WHITE,
    // padding: "1rem",
    // width: "19.5rem",
    // width: "94%",
  },
  enterCaptchaInput: {
    background: WHITE,
    border: "1px solid #DAECFF",
    borderRadius: "0.25rem",
    // width: "21.5rem",
    marginRight: "0.5rem",
  },
  paramText: {
    fontWeight: 400,
    fontSize: "12px",
    lineHeight: "16px",
    marginLeft: "0.313rem",
    color: "#9D9E9E",
  },
  passwordChangdText: {
    fontWeight: 600,
    fontSize: "1.125rem",
    lineHeight: "1.563rem",
    textAlign: "center",
    color: DEFAULT_GREY,
  },
  headerContainer: {
    background:
      "linear-gradient(90deg, #0E3063 21.7%, #2C6A9F 83.26%, #3278B9 102.67%)",
    padding: "1.25rem",
  },
  drdoHeaderImage: {
    objectFit: "contain",
    "@media(max-width: 600px)": {
      objectFit: "contain",
      width: "20rem",
      height: "4rem",
    },
  },
  samarHeaderImage: {
    objectFit: "contain",
    // background: "white",
    height: "4rem",
    padding: "0 1rem",
    "@media(max-width: 600px)": {
      display: "none",
    },
  },
  zedHeaderImage: {
    objectFit: "contain",
    // background: "white",
    padding: "0 1rem",
    "@media(max-width: 600px)": {
      display: "none",
    },
  },
  fullHeight: {
    height: "100%",
  },
  loginContainer: {
    flexDirection: "column",
    alignItems: "start",
  },
  forgotContainer: {
    // marginTop: "0.5rem",
  },
  cursorPointer: {
    cursor: "pointer",
  },
  signUpContainer: {
    display: "flex",
    justifyContent: "space-between",
  },
  tableHeight: {
    // minHeight: "calc(100vh - 20rem)",
    // maxHeight: "calc(100vh - 20rem)",
  },
  termsText: {
    color: "#3278B9",
    textDecoration: "underline",
    cursor: "pointer",
  },
  agreeToContainer: {
    display: "flex",
    alignItems: "center",
    paddingLeft: "3.5rem",
  },
  agreeToTextContainer: {
    display: "flex",
    columnGap: "0.313rem",
    flexWrap: "wrap",
  },
  enterOtpContainer: {
    margin: "1rem 1rem",
    "@media(max-width:600px)": {
      margin: "1rem 2rem",
    },
  },
  registrationText: {
    width: "25.25rem",
    "@media(max-width:600px)": {
      width: "100%",
    },
  },
  goToHomepageText: {
    width: "19.813rem",
  },
  smallWhiteContainer: {
    textAlign: "start",
    marginTop: "2rem",
    marginLeft: "3rem",
    marginRight: "3rem",

    background: "white",
    "@media(max-width:600px)": {
      marginLeft: "0",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
    },
  },
  forgotAlign: {
    textAlign: "start",
  },
  requestLinkButton: {
    width: "27.5rem",
    "@media(max-width:600px)": {
      width: "80%",
    },
  },
  sentEmailTextContainer: {
    fontWeight: 600,
    fontSize: "1.125rem",
    lineHeight: "1.563rem",
    color: "#000000",
    display: "flex",
    flexWrap: "wrap",
    "@media(max-width:600px)": {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      textAlign: "center",
    },
  },
  paramWrapper: {
    display: "flex",
    flexWrap: "wrap",
    marginRight: "1rem",
    marginTop: "1rem",
  },
  parametersContainer: { display: "flex", flexWrap: "wrap", width: "27.5rem" },
  emailText: { color: "#3278B9", fontWeight: 600 },
  verifiedContainer: { textAlign: "center", margin: "1rem 0", padding: "4rem" },
  label: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1rem",
    lineHeight: "1.363rem",
    color: BLACKISH,
    "@media(max-width:600px)": {
      fontSize: "0.875rem",
      fontWeight: 600,
      marginBottom: "0.5rem",
    },
    "& .MuiFormLabel-asterisk": {
      color: "red",
    },
  },
  inputBaseStyle: {
    // width: "50%",
    width: "12.5rem",
    // marginBottom: "0.5rem",
    height: "2.35rem",
    border: "0.063rem solid #A9A9A9",
    paddingX: "0.5rem",
    fontSize: "0.875rem",
    borderRadius: "4px",
    // paddingY: "0",
    "& .MuiInputBase-input": {
      padding: "0px",
    },
  },
  link: {
    marginLeft: "0.5rem",
    fontWeight: 400,
    color: BLACK,
    "@media(max-width:600px)": {
      fontSize: "0.875rem",
      marginLeft: "0.75rem",
      fontWeight: 400,
    },
  },
  linkContainer: {
    paddingLeft: "0.4rem",
    paddingRight: "0.4rem",
    backgroundColor: "#EEEEEE",
    paddingTop: "0.2rem",
    paddingBottom: "0.2rem",
    fontSize: "0.67rem",
    marginLeft: "0.8rem",
  },
  labelContainer: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    "@media(max-width:600px)": {
      flexDirection: "column",
      alignItems: "flex-start",
    },
  },
  multipleImageUpload: {
    marginBottom: "1rem",
    alignItems: "start",
    "@media(max-width: 600px)": {
      flexDirection: "column",
      alignItems: "start",
    },
  },
  customImageDisplay: {
    display: "flex",
    justifyContent: "end",
    marginBottom: "0.25rem",
  },
  addressLabel: {
    marginBottom: "1.25rem",
  },

  topHeader: {
    borderBottom: "0.063rem solid #EAEAEA",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: "2.063rem",
    paddingRight: "2.063rem",
    paddingTop: "1.25rem",
    paddingBottom: "1.25rem",
    backgroundColor: WHITE,
  },
  noDataStyle: {
    // display: "flex",
    // alignItems: "center",
    // justifyContent: "center",
    // padding: "8rem 10rem",
    marginTop: "1rem",
    textAlign: "center",
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "1rem",
  },
  titleText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1.125rem",
    lineHeight: "1.532rem",
    color: BLACKISH,
  },
  greyButton: {
    textTransform: "none",
    background: "#D8DCE0",
    borderRadius: "5px",
    height: "3rem",
    width: "11rem",
    marginRight: "1rem",
    "&:hover": {
      background: "#D8DCE0",
    },
  },
  blueButton: {
    background: "#2C6A9F",
    borderRadius: "5px",
    textTransform: "none",
    width: "11rem",
    height: "3rem",
    "&:hover": {
      background: "#2C6A9F",
    },
    "@media print": {
      visibility: "hidden",
    },
  },
  dialogContainer: {
    // width: "",
    // height: "",
    // width: "100%",
    // height: "100%",
    "& .MuiDialog-container": {
      marginTop: "7rem",
      height: "50%",
    },
    "& .MuiPaper-root": {
      width: "70%",
    },
  },
  greyButtonText: { fontWeight: 700, fontSize: "1rem" },
  blueButtonText: {
    fontWeight: 700,
    fontSize: "1rem",
    color: "white",
  },
  errorStyle: {
    "@media(max-width: 600px)": {
      textAlign: "center",
    },
  },
  addButton: {
    background: "#1FBAED",
    boxShadow: "0px 4px 4px rgba(102, 104, 105, 0.18)",
    borderRadius: "100px",
    color: "white",
    "&:hover": {
      background: "#1FBAED",
    },
  },
  tableHeadButtonText: {
    fontSize: "0.875rem",
    fontWeight: 600,
    textTransform: "capitalize",
    color: "white",
  },
  committeeInputStyle: {
    background: "#FFFFFF",
    border: "0.063rem solid #D0E8FF",
    borderRadius: "0.313rem",
    width: "100%",
    height: "2.25rem",
  },
  tableStyleData: {
    padding: "0",
    marginTop: "1rem",
    marginBottom: "1rem",
  },
  tableBoxStyleData: {
    minHeight: "100%",
  },
  unsetTableHeight: {
    height: "unset",
  },
  passwordContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    flexDirection: "column",
    marginTop: "0",
    marginLeft: "0",
  },
  headerOuterContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: "2.063rem",
    paddingRight: "2.063rem",
    paddingTop: "1.125rem",
    paddingBottom: "1.125rem",
    backgroundColor: WHITE,
    borderBottom: "0.063rem solid #EAEAEA",
  },
  paddingContainer: {
    paddingLeft: "2.063rem",
    paddingRight: "2.063rem",
    paddingTop: "1.125rem",
    paddingBottom: "1.125rem",
    // height: "calc(100vh - 20.5rem)",
    overflow: "auto",
  },
  topBackButtonHeader: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  backButton: {
    minWidth: "unset",
    padding: 0,
    paddingRight: "0.563rem",
    "&:hover": { backgroundColor: "transparent" },
  },
  titleBackText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1.125rem",
    lineHeight: "1.532rem",
    color: BLACKISH,
  },
  labelText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1rem",
    lineHeight: "1.362rem",
    color: BLACKISH,
    "@media(max-width:600px)": {
      fontSize: "0.8rem",
      fontWeight: 600,
    },
  },
  applicantLabel: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.8rem",
    lineHeight: "1rem",
    color: "black",
    "@media(max-width:600px)": {
      fontSize: "0.6rem",
      fontWeight: 600,
    },
  },
  valueText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "1rem",
    lineHeight: "1.362rem",
    color: "#454545",
    "@media(max-width:600px)": {
      fontSize: "0.8rem",
      fontWeight: 300,
    },
  },
  applicantValue: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.8rem",
    lineHeight: "1rem",
    color: "black",
    wordBreak: "break-word",
    "@media(max-width:600px)": {
      fontSize: "0.6rem",
      fontWeight: 300,
    },
  },
  rowMarginBottom: {
    marginBottom: "0.675rem",
  },
  imagePreview: {
    height: "3.75rem",
    width: "4.063rem",
    marginRight: "1.5rem",
    cursor: "pointer",
  },
  noData: {
    fontWeight: 600,
    fontSize: "1.25rem",
    marginTop: "2rem",
    textAlign: "center",
  },
  linkText: {
    textDecoration: "underline",
    color: "blue",
  },
  horizontalLine: {
    borderBottom: "0.063rem solid #EAEAEA",
    paddingTop: "1.538rem",
  },
  dialogContainerWithSeventy: {
    "& .MuiDialog-container": {
      height: "80%",
    },
  },
  greyContainer: {
    backgroundColor: "#FAFAFA",
    border: "0.063rem solid #F0F1F2",
    borderRadius: "0.125rem",
    paddingX: "1.563rem",
    paddingTop: "0.875rem",
    paddingBottom: "2.188rem",
    marginTop: "1.063rem",
  },
  cardMainContainer: {
    border: "1px solid #60C5F9",
    background: "#F8FCFF",
    padding: "1rem",
    // margin: "1rem",
    marginBottom: "1rem",
    marginRight: "1rem",
  },
  textInputStyle: {
    border: "1px solid rgba(0,0,0,0.2)",
    background: "white",
    borderRadius: "4px",
    padding: "0.2rem 1rem",
    width: "100%",
  },
  paginationStyle: {
    margin: "1rem 0rem",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  topHeaderHeight: {
    height: "2.675rem",
  },
  withNumberContainer: {
    display: "flex",
    paddingRight: "0.5rem",
    marginBottom: "0.75rem",
  },
  numbers: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  },
  declarationContainer: {
    marginLeft: "4rem",
  },
  verifiedBoxContainer: {
    textAlign: "center",
    margin: "1rem 0",
    "@media(max-width:600px)": {
      width: "100%",
    },
  },
  otpErrortext: {
    textAlign: "center",
    marginBottom: "1rem",
  },
  aboutText: {
    cursor: "pointer",
  },
  filterBox: {
    flexDirection: "column",
    width: "38%",
    position: "absolute",
    right: "3rem",
    top: "12rem",
    backgroundColor: "rgb(244,250,254)",
    padding: "0.5rem",
    zIndex: 9,
    maxHeight: "calc(100vh - 23rem)",
    overflow: "auto",
    boxShadow:
      "4px -4px 20px rgba(224, 220, 220, 0.25), -4px 4px 20px rgba(173, 173, 173, 0.25)",
    borderRadius: "5px",
    "@media(max-width:900px)": {
      top: "15.5rem",
    },
  },
  dateFilter: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "2rem",
    "@media(max-width:1200px)": {
      flexDirection: "column",
    },
  },
  middleBox: {
    marginBottom: "1rem",
  },
  filterHeader: {
    display: "flex",
    justifyContent: "space-between",
    paddingBottom: "1rem",
    marginBottom: "2rem",
    borderBottom: "0.063rem solid rgb(240,241,242)",
    paddingLeft: "1rem",
    paddingRight: "1rem",
  },
  filterText: { fontSize: "1rem", fontWeight: 700 },
  select: {
    width: "100%",
    height: "2.3rem",
    // border: "0.063rem solid #A9A9A9",
    borderRadius: 0,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.192rem",
    color: BLACKISH,
    marginTop: "0.738rem",
    "@media(max-width:600px)": {
      height: "1.8rem",
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  dropdownText: {
    "@media(max-width:600px)": {
      fontSize: "0.7rem",
      minHeight: "unset",
    },
  },
  filterByStatus: {
    width: "100%",
    marginBottom: "2rem",
  },
  bottomFilterContainer: {
    display: "flex",
    flexDirection: "column",
    paddingLeft: "1rem",
    paddingRight: "1rem",
  },
  buttonsContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  customButtonConatinerStyle: {
    "@media(max-width:1100px)": {
      // width: "50%",
      // flexDirection: "row",
    },
    "@media(max-width:600px)": {
      // width: "50%",
    },
    flexDirection: "row",
    display: "flex",
    marginTop: "0",
  },
  greyButtonStyle: {
    width: "7rem",
    height: "2rem",
    "@media(max-width:1100px)": {
      marginBottom: "1rem",
      width: "100%",
    },
  },
  blueButtonStyle: {
    width: "7rem",
    height: "2rem",
    backgroundColor: "rgb(44,106,159)",
    "@media(max-width:1100px)": {
      width: "100%",
    },
    "&:hover": { backgroundColor: "rgb(44,106,159)" },
  },
  buttonTextStyle: {
    fontWeight: 500,
    fontSize: "0.8rem",
  },
  buttonFilter: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  selectStyle: {
    height: "2rem",
    marginTop: "0",
    borderRadius: "4px",
  },
  gridItem: {
    "& .MuiGrid-item": {
      paddingTop: "0rem",
    },
  },
  chipContainer: {
    marginBottom: "0.5rem",
  },
  feedbackDetails: {
    paddingLeft: "2rem",
    paddingTop: "0.75rem",
  },
  tableButton: {
    width: "9rem",
    height: "2rem",
  },
  tableButtonText: {
    fontSize: "0.75rem",
    fontWeight: 500,
  },
  applicationViewContainer: {
    height: "calc(100vh - 20.55rem)",
    padding: "0",
  },
  placeHolderColor: {
    color: "#A9A9AC",
  },
  placeHolderText: {
    fontSize: "0.875rem",
  },
  displayFlex: {
    display: "flex",
  },
  menuProps: {
    maxHeight: "12rem",
    "@media(max-width:600px)": {
      maxHeight: "9rem",
    },
  },
  iconsBtnContainer: {
    width: "120px",
  },
  parameterContainerHeight: {
    height: "calc(100vh - 16.5rem)",
  },
  parameterContainer: {
    width: "70vw",
    marginTop: "1%",
    padding: "0 0 0 3%",
  },
  parameterText: {
    wordWrap: "break-word",
    fontSize: "0.9rem",
    minHeight: "3rem",
  },
  tabDiv: {
    borderBottom: 1,
    borderColor: "divider",
  },
  committeeIcons: {
    width: "120px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  dropdownTextField: {
    backgroundColor: "#fff",
    borderBottomLeftRadius: "0.5rem",
    borderTopLeftRadius: "0.5rem",
    borderTopRightRadius: 0,
    minWidth: "520px",
  },
  feedbackSurveyDiv: {
    width: "26.75rem",
    paddingRight: "0rem",
  },
  feedbackBlankDiv: {
    minWidth: "21.5rem",
  },
  commentText: {
    fontSize: "0.875rem",
  },
  levelInputBox: {
    wordWrap: "break-word",
    fontSize: "0.9rem",
    minHeight: "5rem",
    display: "flex",
    alignItems: "center",
  },
  scheduleAppointmentBtn: {
    width: "14rem",
  },
  rescheduleAppointmentBtn: {
    width: "15rem",
  },
  localAssessorDiv: {
    display: "flex",
    alignItems: "center",
  },
  localAssessorText: {
    color: BLACKISH,
    fontSize: "1rem",
    fontWeight: 600,
  },
  headerCellStyle: {
    width: "16.66%",
  },
  tableBodyCellStyle: {
    width: "16.66%",
  },
  rejectApplication: {
    color: "red",
    "&.Mui-checked": {
      color: "red",
    },
  },
  marginRight2Percent: {
    marginRight: "2%",
  },
  displayInBlock: {
    display: "inline-block",
  },
  rejectReasonDiv: {
    width: 400,
    minHeight: 100,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.8rem",
    padding: "0 1%",
  },
  submitDrdo: {
    float: "right",
    minHeight: "20px",
  },
  vendorAppView: {
    height: "calc(100vh - 18.5rem)",
  },
  forgotPwd: { marginTop: "1.938rem" },
  newPwd: {
    paddingLeft: "0.938rem",
    paddingRight: "0.938rem",
  },
  assesorResume: {
    width: "21.25rem",
  },
  assessorFileButtonStyle: {
    height: "1.5rem",
  },
  boxContainer: {
    marginTop: "1rem",
    width: "11.5rem",
  },
  rowContainer: {
    display: "flex",
    alignItems: "center",
  },
  rowDropdownStyle: {
    width: "5rem",
    height: "2rem",
    marginLeft: "0.5rem",
  },
  divFlexStyle: {
    display: "flex",
    justifyContent: "end",
    alignItems: "center",
  },
  paymentHistoryIcon: {
    width: "40px",
  },
  watermarked: {
    // position: "relative",
    // "&::before": {
    //   content: '""',
    //   display: "block",
    width: "100%",
    height: "100%",
    position: "absolute",
    // top: "0",
    // left: "0",
    backgroundSize: "contain",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    opacity: 0.1,
    // },
  },
  viewContainerGrid: {
    marginBottom: "0.675rem",
  },
  positionRealtive: {
    position: "relative",
  },
  rowChipContainer: {
    marginRight: "0.5rem",
  },
  multiRow: {
    marginTop: "0.75rem",
  },
  countryText: {
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    color: "rgb(50, 58, 65)",
  },
  multiSelect: {
    marginX: "3.5rem",
  },
  borderLine: {
    borderBottom: "0.063rem solid #EAEAEA",
    marginBottom: "2rem",
    paddingBottom: "1rem",
  },
  customMessageContainer: {
    marginBottom: "1rem",
  },
};

export const customCommonStyles = {
  mainHeightContainer: {
    height: "calc(100vh - 16rem)",
    // overflow: "scroll",
    overflow: "hidden",
    ...commonStyles.customScrollBar,
  },
  fieldContainer: {
    padding: "1rem 2rem",
    height: "calc(100vh - 16.5rem)",
    overflow: "auto",
    ...commonStyles.customScrollBar,
  },
  listCardContainer: {
    height: "calc(100vh - 18rem)",
    overflow: "auto",
    ...commonStyles.customScrollBar,
  },
  marginBottomOne: {
    marginBottom: "1rem",
  },
  marginTopO: {
    marginTop: "0rem",
  },
  marginBottomO: {
    marginBottom: "0rem",
  },
  outerContainer: {
    // minHeight: "calc(100vh - 15rem)",
    // maxHeight: "calc(100vh - 15rem)",
    backgroundColor: WHITE,
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    overflowY: "auto",
    ...commonStyles.customScrollBar,
  },
  marginLeft0: {
    marginLeft: "0",
  },
  marginLeftOne: {
    marginLeft: "1rem",
  },
  marginRight0: {
    marginRight: "0",
  },
  marginRightOne: {
    marginRight: "1rem",
  },
  marginRightTwo: {
    marginRight: "2rem",
  },
  chartsContainer: {
    height: "calc(100vh - 14.5rem)",
    overflowY: "auto",
    ...commonStyles.customScrollBar,
  },
  paddingTopOne: {
    paddingTop: "1rem",
  },
  marginBottomTwo: {
    marginBottomTwo: "2rem",
  },
  marginTopOne: {
    marginTop: "1rem",
  },
  margin0: {
    margin: "0",
  },
  marginOne: {
    margin: "1rem",
  },
  marginTopTwo: { marginTop: "2rem" },
  padding0: { padding: "0" },
};
