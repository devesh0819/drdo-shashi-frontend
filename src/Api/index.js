import superagent from "superagent";
import { showLoader } from "../Redux/Loader/loaderActions";
import { showToast } from "../Components/Toast/Toast";
import {
  logoutAction,
  requestLinkAction,
  resetPasswordAction,
} from "../Redux/Login/loginAction";
import { logout } from "../Services/localStorageService";

/*
 * @function "call" common method that makes api requests
 * @param {object} "request" stores the request 'method','endpoint', 'payload', 'query',
 * 'token' as keys...'
 */

export default function call({
  method = "get",
  url,
  endpoint,
  payload,
  query,
  token,
  type = "application/json",
  formData,
  dispatch,
}) {
  const _url = url || `${process.env.REACT_APP_BASE_URL}/${endpoint}`;
  const _apiRequest = superagent(method, _url);
  return new Promise((resolve, reject) => {
    if (type && type.length) {
      _apiRequest.set("Content-Type", type);
      _apiRequest.set("Accept", type);
    }
    dispatch(showLoader(true));
    _apiRequest
      .set("Authorization", `Bearer ${token || localStorage.getItem("token")}`)
      .send(payload)
      .query(query)
      .then((res) => {
        resolve(res);
      })
      .then(() => {
        dispatch(showLoader(false));
      })
      .catch((error) => {
        dispatch(showLoader(false));
        if (
          error?.response?.statusCode === 401 &&
          (error?.response?.statusText === "Unauthorized" ||
            error?.response?.statusText === "Not Found" ||
            error?.response?.statusText === "Unauthenticated.")
        ) {
          logout();
        }

        let errorBody;
        if (error?.response?.body?.error === "Your email is not verified yet") {
          window.location.href = "/enter-otp?login=true";
          errorBody = `${error.response.body.error} 
           Please click here to verify email`;
          showToast(
            errorBody,
            "error",
            () => (window.location.href = "/enter-otp")
          );
        }
        if (error?.response?.statusCode === 404) {
          errorBody = "Something went wrong";
        } else if (error?.response?.statusCode === 429) {
          errorBody = error?.response?.body?.error;
          showToast(errorBody, "error");
          return;
        } else errorBody = error;

        reject(errorBody);
      });
  });
}
