import React, { Suspense } from "react";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import "./App.css";
import Loader from "./Components/Loader/Loader";
import Layout from "./Containers/Layout/Layout";
import { publicRoutes } from "./Routes/PublicRoutes";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { privateRoutes } from "./Routes/PrivateRoutes";
import { ThemeProvider } from "@mui/material/styles";
import theme from "./Assets/Themes/Themes";
import { getToken } from "./Auth/getToken";
import PublicRoute from "./Routes/PublicRoute";
import PrivateRoute from "./Routes/PrivateRoute";
import DefaultRoute from "./Routes/DefaultRoute";

toast.configure();

function App() {
  return (
    <>
      <ThemeProvider theme={theme}>
        <Loader />
        <BrowserRouter>
          <Suspense fallback={<Loader />}>
            <Routes>
              {publicRoutes.map((publicRouteDetail, index) => {
                return (
                  <Route path="/" element={<PublicRoute />} key={index}>
                    <Route
                      key={index}
                      exact
                      path={publicRouteDetail.path}
                      element={
                        <Suspense fallback={<Loader />}>
                          <Layout
                            isSidebarNeeded={publicRouteDetail.isSidebarNeeded}
                            isFooterNeeded={publicRouteDetail.isFooterNeeded}
                            isHeaderNeeded={publicRouteDetail.isHeaderNeeded}
                          >
                            {publicRouteDetail.component}
                          </Layout>
                        </Suspense>
                      }
                    />
                  </Route>
                );
              })}
              {privateRoutes.map((privateRouteDetail, index) => {
                return (
                  <Route exact path="/" element={<PrivateRoute />} key={index}>
                    <Route
                      // exact
                      path={privateRouteDetail.path}
                      element={
                        <Suspense fallback={<Loader />}>
                          <Layout
                            isSidebarNeeded={privateRouteDetail.isSidebarNeeded}
                            role={privateRouteDetail.role}
                            isFooterNeeded={privateRouteDetail.isFooterNeeded}
                            isHeaderNeeded={privateRouteDetail.isHeaderNeeded}
                          >
                            {privateRouteDetail.children ? (
                              <Outlet />
                            ) : (
                              privateRouteDetail.component
                            )}
                          </Layout>
                        </Suspense>
                      }
                    >
                      {privateRouteDetail.children &&
                        privateRouteDetail.children.map(
                          (child, privateIndex) => {
                            return (
                              <Route
                                key={privateIndex}
                                exact
                                path={child.path}
                                element={
                                  child.children ? (
                                    <Outlet key={privateIndex} />
                                  ) : (
                                    child.component
                                  )
                                }
                              >
                                {child.children?.map(
                                  (nestedChild, nestedIndex) => (
                                    <Route
                                      key={nestedIndex}
                                      path={nestedChild.path}
                                      element={nestedChild.component}
                                    />
                                  )
                                )}
                              </Route>
                            );
                          }
                        )}
                    </Route>
                  </Route>
                );
              })}
              <Route path="*" element={<DefaultRoute />} />
            </Routes>
          </Suspense>
        </BrowserRouter>
      </ThemeProvider>
    </>
  );
}

export default App;
