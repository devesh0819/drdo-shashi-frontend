import { getItem } from "../Services/localStorageService.js";

export const getToken = () => {
  const token = getItem("token") || "";
  return token;
};
