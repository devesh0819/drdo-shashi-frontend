import { APP_STAGE, ROLES } from "../Constant/RoleConstant";
import {
  ASSESSOR_ALL_APPLICATIONS,
  DASHBOARD,
  DRDO_DASHBOARD,
  HOME,
  MANAGE_ASSESSMENTS,
} from "../Routes/Routes";
import * as _ from "lodash";
import {
  APPROVED,
  appStatus,
  assessorStatus,
  DRAFT,
  PENDING_APPROVAL,
  REJECTED,
  typeOfEnterpriseArray,
} from "../Constant/AppConstant";

export const getDefaultPath = (role) => {
  if (role === ROLES.ADMIN) {
    return DASHBOARD;
  }
  if (role === ROLES.AGENCY) {
    return MANAGE_ASSESSMENTS;
  }
  if (role === ROLES.DRDO) {
    return DRDO_DASHBOARD;
  }
  if (role === ROLES.ASSESSOR) {
    return ASSESSOR_ALL_APPLICATIONS;
  }
  if (role === ROLES.VENDOR) {
    return HOME;
  }
};

export const maskFields = (fieldData) => {
  return fieldData?.replace(/.(?=.{3})/g, "* ");
};

export const maskEmailField = (email) => {
  return email?.replace(/(?<=.{2}).(?=[^@]*?@)/gm, " *");
};

export const getAppStatus = (status) => {
  return _.find(appStatus, { value: status })?.label;
};

export const getAssessorStatus = (status) => {
  return _.find(assessorStatus, { value: status })?.label;
};

export const getEnterpriceType = (status) => {
  return _.find(typeOfEnterpriseArray, { value: status })?.label;
};

export const afterDiscountValue = (amount, discount) => {
  return amount - amount * (discount / 100);
};
export const percentValue = (value, percent) => {
  return value * (percent / 100);
};

export const textTransformCapital = (text) => {
  if (text?.length > 0) {
    let tempString = text.toLowerCase();
    return tempString[0]?.toUpperCase() + tempString.slice(1);
  }
  return "";
};
export const checkQuestionairreStatus = (state) => {
  if (state === DRAFT) return "Pending";
  if (state === PENDING_APPROVAL) return "Sent for Approval";
  if (state === APPROVED) return "Approved";
  if (state === REJECTED) return "Rejected";
};

export const checkDrdoQuestionairreStatus = (state) => {
  if (state === PENDING_APPROVAL) return "Pending";
  if (state === APPROVED) return "Approved";
  if (state === REJECTED) return "Rejected";
};

export const nFormatter = (num) => {
  if (num >= 1000000000) {
    return (num / 1000000000).toFixed(1).replace(/\.0$/, "") + "G";
  }
  if (num >= 1000000) {
    return (num / 1000000).toFixed(1).replace(/\.0$/, "") + "M";
  }
  if (num >= 1000) {
    return (num / 1000).toFixed(1).replace(/\.0$/, "") + "K";
  }
  return num;
};

export const formatData = (text) => {
  let newText = "";
  if ((text.length + 1) % 5 === 0 && text.length !== 14) {
    newText = text + " ";
  }
  return newText;
};

export const formatUdyamNumber = (text) => {
  let newText = "";
  if (text?.length === 5 || text?.length === 8 || text?.length === 11) {
    newText = text + "-";
  }
  return newText;
};

export const getAssessmentStatus = (item) => {
  if (
    item?.progress === APP_STAGE.PAREMETER_ALLOCATED &&
    item?.is_ongoing_assessment === true
  ) {
    return "Assessment In Progress";
  } else if (
    item?.progress === APP_STAGE.ASSESSORS_ASSIGNED &&
    item?.assessors?.length === 0
  ) {
    return "Assessment Scheduled";
  } else {
    return getAppStatus(item?.progress);
  }
};
