import { PASSWORD_ENCRYPTION_SECRET } from "../Constant/AppConstant";
import * as crypto from "crypto-js";
import { LOGIN } from "../Routes/Routes";
import { showToast } from "../Components/Toast/Toast";

export const removeItem = (key) => {
  localStorage.removeItem(key);
};

export const encrypt = (data, secretKey) => {
  const cipher = crypto.AES.encrypt(data, secretKey).toString();
  return cipher;
};

export const decrypt = (data, secretKey) => {
  const bytes = crypto.AES.decrypt(data, secretKey);
  const originalText = bytes.toString(crypto.enc.Utf8);
  return originalText;
};

export const getItem = (key) => {
  let retrievedVal = localStorage.getItem(key);

  if (!retrievedVal) {
    return null;
  }
  return retrievedVal;
};

export const getUserData = () => {
  const encryptedUserData = localStorage.getItem("user");
  if (encryptedUserData) {
    return JSON.parse(
      decrypt(encryptedUserData, process.env.REACT_APP_PASSWORD_SECRET_KEY)
    );
  } else {
    return null;
  }
};

export const logout = (navigate) => {
  localStorage.clear();
  if (navigate) {
    navigate(LOGIN);
  } else {
    window.location.href = LOGIN;
  }
  showToast("Logged out successfully", "success");
};

export const lazyRetry = function (componentImport) {
  return new Promise((resolve, reject) => {
    // check if the window has already been refreshed
    const hasRefreshed = JSON.parse(
      window.sessionStorage.getItem("retry-lazy-refreshed") || "false"
    );
    // try to import the component
    componentImport()
      .then((component) => {
        window.sessionStorage.setItem("retry-lazy-refreshed", "false"); // success so reset the refresh
        resolve(component);
      })
      .catch((error) => {
        if (!hasRefreshed) {
          // not been refreshed yet
          window.sessionStorage.setItem("retry-lazy-refreshed", "true"); // we are now going to refresh
          return window.location.reload(); // refresh the page
        }
        reject(error); // Default error behaviour as already tried refresh
      });
  });
};
