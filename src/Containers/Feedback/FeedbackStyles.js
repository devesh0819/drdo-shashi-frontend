import { DEFAULT_GREY } from "../../Constant/ColorConstant";
import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  questionRating: {
    display: "flex",
    alignItems: "center",
    marginBottom: "1rem",
    justifyContent: "space-between",
  },
  questionContainer: {
    // marginRight: "1rem",
    width: "20rem",
  },
  feedbackBlankDiv: {
    minWidth: "22rem",
  },
  commentBox: {
    height: "7rem",
    display: "flex",
    alignItems: "start",
    paddingRight: "0",
    "& .MuiInputBase-input": {
      height: "6.3rem !important",
      overflow: "auto !important",
      ...commonStyles.customScrollBar,
    },
  },
  noteText: {
    fontWeight: 600,
    fontSize: "0.75rem",
    lineHeight: "1.375rem",
    textAlign: "left",
    color: DEFAULT_GREY,
    fontFamily: "Open Sans",
    marginBottom: "1rem",
    fontStyle: "italic",
  },
};
