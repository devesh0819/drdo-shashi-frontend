import React from "react";
import { Box } from "@mui/material";
import QpBoxContainer from "../../Components/QpBoxContainer/QpBoxContainer";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import QpTypography from "../../Components/QpTypography/QpTypography";
import verifiedTick from "../../Assets/Images/greenCircleTick.png";
import QpButton from "../../Components/QpButton/QpButton";
import { useNavigate } from "react-router-dom";
import { getDefaultPath } from "../../Services/commonService";
import { getUserData } from "../../Services/localStorageService";

export default function FeedbackSubmitted() {
  const navigate = useNavigate();
  const userData = getUserData();
  return (
    <QpBoxContainer loginRequired={false}>
      {" "}
      <Box sx={commonStyles.verifiedContainer}>
        <Box>
          <img
            src={verifiedTick}
            alt="verifiedTick"
            width="105px"
            height="105px"
          />
        </Box>
        <QpTypography
          displayText="Feedback Submitted Successfully!"
          styleData={commonStyles.enterOtpText}
        />
        <QpButton
          displayText="Go to Homepage"
          styleData={{
            ...commonStyles.validateButton,
            ...commonStyles.goToHomepageText,
            ...customCommonStyles.margin0,
          }}
          onClick={() => navigate(getDefaultPath(userData?.roles[0]))}
          textStyle={commonStyles.validateButtontext}
        />
      </Box>
    </QpBoxContainer>
  );
}
