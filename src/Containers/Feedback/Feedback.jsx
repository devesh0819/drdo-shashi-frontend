import { Box, InputBase, Tooltip, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import QpBoxContainer from "../../Components/QpBoxContainer/QpBoxContainer";
import QpTypography from "../../Components/QpTypography/QpTypography";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";

import { styles } from "./FeedbackStyles.js";
import QpButton from "../../Components/QpButton/QpButton";
import { useDispatch, useSelector } from "react-redux";
import { submitFeedbackAction } from "../../Redux/Login/loginAction";
import { useNavigate, useSearchParams } from "react-router-dom";
import { FEEDBACK_SUBMITTED } from "../../Routes/Routes";
import { feedbackQuestions } from "../../Constant/AppConstant";
import CustomRating from "../../Components/CustomRating/CustomRating";
import { showToast } from "../../Components/Toast/Toast";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../Redux/AllApplications/allApplicationsActions";
import {
  getApplicationFeedbackConfigAction,
  getFeedbackConfigurationAction,
} from "../../Redux/Admin/FeedbackConfiguration/feedbackConfigurationActions";

export default function Feedback() {
  const [commentText, setCommentText] = useState();
  // const [feedbackArray, setFeedbackArray] = useState([-1, -1, -1, -1]);
  const [feedbackArray, setFeedbackArray] = useState();
  const [feedbackConfigurationData, setFeedbackConfigurationData] = useState(
    []
  );
  const [feedbackValues, setFeedbackValues] = useState({});
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [searchParams] = useSearchParams();
  const token = searchParams.get("token");
  const email = searchParams.get("email");
  const application_id = searchParams.get("application_id");
  const applicationFeedbackConfig = useSelector(
    (state) => state.feedbackConfiguration.applicationFeedbackConfig
  );

  useEffect(() => {
    if (application_id) {
      dispatch(getApplicationDetailAction(application_id));
      dispatch(getApplicationFeedbackConfigAction(application_id));
    }
    return () => {
      dispatch(getApplicationDetail({}));
    };
  }, [application_id]);

  // useEffect(() => {
  //   dispatch(getApplicationFeedbackConfigAction());
  // }, []);

  useEffect(() => {
    if (
      applicationFeedbackConfig &&
      Object.keys(applicationFeedbackConfig)?.length > 0
    ) {
      let feedbackKeys = Object.keys(applicationFeedbackConfig);
      let tempFeedback = feedbackKeys.map((ques) => {
        return { uuid: ques, title: applicationFeedbackConfig[ques] };
      });
      setFeedbackConfigurationData(tempFeedback);
    }
  }, [applicationFeedbackConfig]);

  useEffect(() => {
    if (feedbackConfigurationData?.length > 0) {
      let ques;
      for (ques = 0; ques < feedbackConfigurationData?.length; ++ques) {
        if (feedbackConfigurationData[ques]?.uuid) {
          setFeedbackValues({
            ...feedbackValues,
            [feedbackConfigurationData[ques]?.uuid]: -1,
          });
        }
      }
    }
  }, [feedbackConfigurationData]);

  const handleFeedbackChange = (event, index, uuid) => {
    let allFeedback = feedbackValues;
    if (allFeedback[uuid] === -1 || event.target.value !== allFeedback[uuid]) {
      allFeedback[uuid] = event.target.value;
    } else if (allFeedback[uuid] !== -1) {
      allFeedback[uuid] = -1;
    }
    setFeedbackValues(allFeedback);
  };
  const onSubmit = () => {
    if (Object.values(feedbackValues)?.includes(-1)) {
      showToast("Please fill all fields", "error");
      return;
    }

    let completeData = feedbackValues;
    if (commentText?.length > 0) {
      completeData["feedback_comment"] = commentText;
    }

    dispatch(
      submitFeedbackAction(token, completeData, navigate, application_id)
    );
  };
  return (
    <QpBoxContainer loginRequired={false}>
      {" "}
      <Box sx={{ ...commonStyles.smallWhiteContainer }}>
        <QpTypography
          displayText="Feedback Survey"
          styleData={{
            ...commonStyles.enterOtpText,
            ...commonStyles.forgotAlign,
            ...customCommonStyles.marginBottomOne,
          }}
        />
        <Box>
          <Box sx={commonStyles.withNumberContainer}>
            <Box sx={styles.feedbackBlankDiv}></Box>
            <Box sx={commonStyles.numbers}>
              <QpTypography displayText={1} />
              <QpTypography displayText={2} />
              <QpTypography displayText={3} />
              <QpTypography displayText={4} />
            </Box>
          </Box>
          {feedbackConfigurationData &&
            feedbackConfigurationData?.map((ques, index) => {
              return (
                <Box sx={styles.questionRating} key={index}>
                  <QpTypography
                    displayText={`${index + 1}. ${ques?.title}`}
                    styleData={styles.questionContainer}
                  />
                  <CustomRating
                    onChange={(event) =>
                      handleFeedbackChange(event, index, ques?.uuid)
                    }
                  />
                </Box>
              );
            })}
        </Box>
        <InputBase
          multiline
          id="Comment"
          name="Comment"
          required
          placeholder="Comment, if any"
          sx={{
            ...commonStyles.textInputStyle,
            ...styles.commentBox,
          }}
          autoFocus={false}
          value={commentText}
          onChange={(event) => setCommentText(event.target.value)}
        />
        <QpTypography
          displayText="Note: 1 is the lowest and 4 is the highest"
          styleData={styles.noteText}
        />
        <QpButton
          displayText="Submit"
          styleData={{
            ...commonStyles.validateButton,
            ...commonStyles.requestLinkButton,
            ...customCommonStyles.marginTopOne,
          }}
          textStyle={commonStyles.validateButtontext}
          onClick={onSubmit}
        />
      </Box>
    </QpBoxContainer>
  );
}
