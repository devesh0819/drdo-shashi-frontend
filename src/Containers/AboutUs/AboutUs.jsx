import React, { useEffect, useState } from "react";
import { Box, Typography } from "@mui/material";
import QpTypography from "../../Components/QpTypography/QpTypography";
import { styles } from "./AboutUsStyles.js";
import CustomTable from "../../Components/CustomTable/CustomTable";
import { maturityTableRow } from "../../Constant/AppConstant";
import QpButton from "../../Components/QpButton/QpButton";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { LOGIN, SIGN_UP } from "../../Routes/Routes";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getFeeConfigurationData } from "../../Redux/Admin/FeeConfiguration/feeConfigurationActions";
import drdoLandingPage from "../../Assets/Images/drdoLandingPage.png";
import drdoPortal from "../../Assets/Images/drdoPortal.jpg";
import drdoWebPortal from "../../Assets/Images/drdoWebPortal.jpg";
import landingPage from "../../Assets/Images/landingPage.jpg";
import samarLogo from "../../Assets/Images/samar_logo.jpg";

export default function AboutUs() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [certificationData, setCertificationData] = useState();
  const feeConfigurationData = useSelector(
    (state) => state.feeConfiguration.feeConfigurationData
  );

  useEffect(() => {
    dispatch(getFeeConfigurationData());
  }, [dispatch]);

  useEffect(() => {
    if (feeConfigurationData) {
      setCertificationData([
        {
          enterprise_type: "Micro",
          man_days: 6,
          fees:
            feeConfigurationData[0]?.payment_breakup
              ?.micro_certification_fee_value +
            feeConfigurationData[0]?.payment_breakup?.micro_assessor_fee_value,
        },
        {
          enterprise_type: "Small",
          man_days: 6,
          fees:
            feeConfigurationData[1]?.payment_breakup
              ?.small_certification_fee_value +
            feeConfigurationData[1]?.payment_breakup?.small_assessor_fee_value,
        },
        {
          enterprise_type: "Medium",
          man_days: 8,
          fees:
            feeConfigurationData[2]?.payment_breakup
              ?.medium_certification_fee_value +
            feeConfigurationData[2]?.payment_breakup?.medium_assessor_fee_value,
        },
        {
          enterprise_type: "Large",
          man_days: 10,
          fees:
            feeConfigurationData[3]?.payment_breakup
              ?.large_certification_fee_value +
            feeConfigurationData[3]?.payment_breakup?.large_assessor_fee_value,
        },
      ]);
    }
  }, [feeConfigurationData]);

  const maturityTableColumn = [
    { field: "Maturity Level" },
    { field: "Organisational Attribute" },
  ];

  const certificationTableColumn = [
    { field: "Type of Enterprise" },
    { field: "Man-days" },
    { field: "Certification Fee (INR)" },
  ];

  const dataSet =
    maturityTableRow?.length > 0
      ? maturityTableRow?.map((labelData) => {
          return {
            "Maturity Level": labelData?.label,
            "Organisational Attribute": labelData?.data,
          };
        })
      : [];

  const certDataSet =
    certificationData?.length > 0
      ? certificationData?.map((labelData) => {
          return {
            "Type of Enterprise": labelData?.enterprise_type,
            "Man-days": labelData?.man_days,
            "Certification Fee (INR)": labelData?.fees,
          };
        })
      : [];

  return (
    <>
      {/* <Box
        sx={{
          backgroundImage: `url(${drdoLandingPage})`,
          backgroundPosition: "center",
          // width: "100%",
          backgroundSize: "contain",
          backgroundRepeat: "no-repeat",
          height: "calc(100vh - 3rem)",
          // height: "100%",
          border: "1px solid red",
        }}
      ></Box> */}
      <Box>
        <Box sx={styles.displayStyle}>
          <QpTypography
            displayText="Login"
            onClick={() => navigate(LOGIN)}
            styleData={styles.loginText}
          />
          <QpTypography displayText=" | " styleData={styles.loginText} />

          <QpTypography
            displayText=" Register"
            onClick={() => navigate(SIGN_UP)}
            styleData={styles.loginText}
          />
        </Box>
        <img src={landingPage} style={styles.landingPageImg} alt="drdo" />

        {/* <QpButton
          displayText="REGISTER"
          styleData={{
            ...commonStyles.blueButton,
            ...styles.registerButton,
          }}
          textStyle={{
            ...commonStyles.blueButtonText,
            ...styles.loginButtonText,
          }}
          onClick={() => navigate(SIGN_UP)}
        />
        <QpButton
          displayText="LOGIN"
          styleData={{
            ...commonStyles.blueButton,
            ...styles.loginButton,
          }}
          textStyle={{
            ...commonStyles.blueButtonText,
            ...styles.loginButtonText,
          }}
          onClick={() => navigate(LOGIN)}
        /> */}
      </Box>
      {/* <Box sx={styles.certificateContainer}>
        <Box>
          <QpTypography
            displayText="Certification Fees*"
            styleData={styles.certificationFeeStyle}
          />
        </Box>
        <Box sx={{ ...commonStyles.displayCenterStyle }}>
          <Box
            sx={{
              ...commonStyles.displayStyle,
              ...styles.certificateFeeContainer,
            }}
          >
            {certificationData?.map((feeData, index) => {
              return (
                <>
                  <Box>
                    <QpTypography
                      displayText={`${feeData?.enterprise_type} Enterprise`}
                      styleData={styles.enterpriseText}
                    />
                    <QpTypography
                      displayText={`₹ ${feeData?.fees?.toLocaleString(
                        "en-IN"
                      )}`}
                      styleData={styles.feeAmount}
                    />
                  </Box>
                </>
              );
            })}
          </Box>
        </Box>
        <QpTypography
          displayText="*Taxes extra, as applicable"
          styleData={styles.taxesStyle}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="Copyright © 2023, DRDO, Ministry of Defence, Government of India"
          styleData={styles.copyrightStyle}
        />
      </Box> */}

      {/* <Box sx={styles.aboutContainer}>
        <Box sx={commonStyles.displayStyle}>
          <QpTypography displayText="About" styleData={styles.aboutUsText} />
          <QpButton
            displayText="Go to Login"
            styleData={{
              ...commonStyles.blueButton,
              ...styles.loginButton,
            }}
            textStyle={{
              ...commonStyles.blueButtonText,
              ...styles.loginButtonText,
            }}
            onClick={() => navigate(LOGIN)}
          />
        </Box>
        <Typography>
          <i style={styles.italicStyle}>
            SAMAR- System for Advance Manufacturing Assessment and Rating
          </i>{" "}
          is a benchmark to measure the maturity of defence manufacturing
          enterprises. The certification is based on a maturity assessment model
          developed by Quality Council of India and is applicable to all defence
          manufacturing enterprises i.e., micro, small, medium and large
          enterprises.
        </Typography>
        <br />
        <Typography>
          ARMS-DM is an outcome of the collaboration between DRDO and QCI to
          strengthen the{" "}
          <i style={{ fontWeight: "bold" }}>defence manufacturing </i>ecosystem
          in the country with an objective to further the vision of making India
          self- reliant in defence manufacturing.  
          <br />
          ARMS-DM Certification framework recognizes 5 levels of enterprise
          maturity in terms of the defined organisational attributes:
        </Typography>
        <CustomTable
          columnDefs={maturityTableColumn}
          rowData={dataSet}
          styleData={styles.tableStyle}
        />
        <CustomTable
          columnDefs={certificationTableColumn}
          rowData={certDataSet}
          title="Certification fees"
          styleData={styles.certificationTable}
        />
        <QpTypography
          displayText="* Taxes extra, as applicable."
          styleData={styles.taxStyle}
        />
        <Typography>
          <b>Validity of Certification: </b>2 years
        </Typography>
        <Typography>
          <b> Subsidy on Certification Fee: </b>
          <br />
          To encourage the Micro and Small Enterprises, DRDO has subsidized the
          cost of Certification for its vendors (micro &amp; small enterprise,
          as per the new definition under the MSED Act, 2006), as under:
        </Typography>
        <Typography>Micro enterprises: 80%</Typography>
        <Typography>{`Micro enterprise: ${feeConfigurationData[0]?.payment_breakup?.micro_subsidy_value}%`}</Typography>
        <Typography sx={{ marginBottom: "1rem" }}>
          Small enterprises: 50%
        </Typography>
        <Typography sx={customCommonStyles.marginBottomOne}>
          {`Small enterprise: ${feeConfigurationData[1]?.payment_breakup?.small_subsidy_value}%`}
        </Typography>
      </Box> */}
    </>
  );
}
