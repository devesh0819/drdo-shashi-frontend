import { DARKER_BLUE } from "../../Constant/ColorConstant";

export const styles = {
  aboutUsText: {
    fontSize: "1.5rem",
    fontWeight: 700,
    marginBottom: "1rem",
  },
  tableStyle: {
    padding: "0",
    marginBottom: "1rem",
  },
  certificationTable: {
    padding: "0",
  },
  taxStyle: {
    fontStyle: "italic",
    marginBottom: "1rem",
  },
  certificateFeeContainer: {
    width: "100%",
    paddingX: "5rem",
    paddingTop: "5rem",
  },
  loginButton: {
    width: "8.25rem",
    height: "3rem",
    position: "absolute",
    top: "52%",
    left: "10%",
    // "@media(min-width:2050px)": {
    //   top: "50vh",
    //   // left: "50vh",
    // },
    // "@media(min-width:1900px) and (max-width:2049px)": {
    //   top: "50vh",
    //   // left: "40vh",
    // },
    // "@media(min-width:1700px) and (max-width:1900px)": {
    //   top: "45vh",
    // },
    // "@media(min-width:1650px) and (max-width:1700px)": {
    //   top: "40vh",
    // },
    // // "@media(min-width:1650px) and (max-width:1900px)": {
    // //   top: "40vh",
    // // },
    // "@media(min-width:1550px) and (max-width:1650px)": {
    //   top: "40vh",
    // },
    // "@media(min-width:1450px) and (max-width:1550px)": {
    //   top: "38vh",
    // },
    // "@media(min-width:1350px) and (max-width:1450px)": {
    //   top: "35vh",
    // },
    // "@media(min-width:1250px) and (max-width:1350px)": {
    //   top: "32vh",
    // },
    // "@media(min-width:1200px) and (max-width:1250px)": {
    //   top: "29vh",
    // },
    // "@media(min-width:1100px) and (max-width:1200px)": {
    //   top: "28vh",
    // },
    marginRight: "1rem",
  },
  registerButton: {
    width: "11.25rem",
    height: "3rem",
    position: "absolute",
    top: "52%",
    left: "40%",
    // "@media(min-width:2050px)": {
    //   top: "50%",
    // },
    // "@media(min-width:1900px) and (max-width:2050px)": {
    //   top: "50%",
    // },
    // "@media(min-width:1700px) and (max-width:1899px)": {
    //   top: "45%",
    // },
    // "@media(min-width:1650px) and (max-width:1700px)": {
    //   top: "40%",
    // },
    // // "@media(min-width:1650px) and (max-width:1900px)": {
    // //   top: "40%",
    // // },
    // "@media(min-width:1550px) and (max-width:1650px)": {
    //   top: "40%",
    // },
    // "@media(min-width:1450px) and (max-width:1550px)": {
    //   top: "38%",
    // },
    // "@media(min-width:1350px) and (max-width:1450px)": {
    //   top: "35%",
    // },
    // "@media(min-width:1250px) and (max-width:1350px)": {
    //   top: "32%",
    // },
    // "@media(min-width:1200px) and (max-width:1250px)": {
    //   top: "29%",
    // },
    // "@media(min-width:1100px) and (max-width:1200px)": {
    //   top: "28%",
    //   left: "28%",
    // },
  },
  certificationFeeStyle: {
    fontSize: "4rem",
    color: DARKER_BLUE,
    fontWeight: 700,
    textAlign: "center",
  },
  certificateContainer: {
    paddingTop: "1rem",
    backgroundColor: "white",
    marginTop: "-0.5rem",
  },
  enterpriseText: {
    fontSize: "2rem",
    fontWeight: 600,
    color: "#524e4e",
    textAlign: "center",
  },
  feeAmount: {
    fontSize: "3.5rem",
    fontWeight: 600,
    textAlign: "center",
    color: DARKER_BLUE,
  },
  taxesStyle: {
    textAlign: "center",
    fontSize: "1.5rem",
    paddingBottom: "5rem",
    color: "#585151",
  },
  copyrightStyle: {
    fontSize: "1.25rem",
    // background:
    //   "linear-gradient(90deg, #0E3063 21.7%, #2C6A9F 83.26%, #3278B9 102.67%)",
    background:
      "linear-gradient(270deg, #1c51a1 21.7%, #3083c9 47.26%, #3c96ea 102.67%)",
    color: "white",
    fontWeight: 600,
    textAlign: "center",
    paddingY: "1rem",
  },
  landingPageImg: {
    backgroundPosition: "center",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
    maxWidth: "100%",
    width: "100%",
    position: "relative",
  },
  loginButtonText: {
    // fontSize: "0.75rem",
    fontSize: "1rem",
    fontWeight: 500,
  },
  italicStyle: {
    fontWeight: "bold",
  },
  aboutContainer: {
    padding: "2rem 5rem",
    backgroundColor: "white",
  },
  loginText: {
    color: DARKER_BLUE,
    // textDecoration: "underline",
    fontWeight: "bold",
    fontSize: "1rem",
    // marginLeft: "1rem",
    cursor: "pointer",
    marginRight: "1rem",
  },
  displayStyle: {
    display: "flex",
    justifyContent: "flex-end",
    backgroundColor: "white",
  },
};
