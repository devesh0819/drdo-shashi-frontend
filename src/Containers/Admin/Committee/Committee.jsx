import { Box, Typography } from "@mui/material";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import QpButton from "../../../Components/QpButton/QpButton";
import CommiteeListCard from "./CommitteeListCard/CommitteeListCard";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getCommitteeListingAction } from "../../../Redux/Admin/Member/memberActions";
import AddCommittee from "./AddCommittee/AddCommittee";
import CustomPagination from "../../../Components/CustomPagination/CustomPagination";
import { COMMITTEE } from "../../../Routes/Routes";
import { useNavigate } from "react-router-dom";
import { getUserData } from "../../../Services/localStorageService";
import { ROLES } from "../../../Constant/RoleConstant";

const Committee = () => {
  const [addCommittee, setAddCommittee] = useState(false);
  const [editCommittee, setEditCommittee] = useState();
  const [pageNumber, setPageNumber] = useState(1);
  const userData = getUserData();
  const role = userData?.roles[0];

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const result = useSelector((state) => state.members.committeeListingData);

  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(COMMITTEE);
    });
  }, []);

  useEffect(() => {
    if (pageNumber) {
      dispatch(getCommitteeListingAction({ start: pageNumber }));
    }
  }, [pageNumber]);
  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  return (
    <Box sx={customCommonStyles.outerContainer}>
      <Box>
        <Box
          style={{
            ...commonStyles.topHeader,
            ...commonStyles.displayStyle,
            ...commonStyles.committeeContainer,
          }}
        >
          <Typography style={commonStyles.titleText}>
            {`Listed Committees (${result.data?.total})`}
          </Typography>
          {role === ROLES.ADMIN && (
            <QpButton
              displayText="+ ADD COMMITEE"
              styleData={commonStyles.addButton}
              onClick={() => {
                setAddCommittee(true);
                setEditCommittee();
              }}
              isDisable={editCommittee || addCommittee ? true : false}
            />
          )}
        </Box>
        <Box sx={customCommonStyles.listCardContainer}>
          {addCommittee && <AddCommittee setAddCommittee={setAddCommittee} />}
          {result.data?.total === 0 && !addCommittee ? (
            <Box sx={commonStyles.noDataStyle}>No data available</Box>
          ) : (
            <>
              {result.data?.committees?.map((committee, index) => {
                return (
                  <CommiteeListCard
                    committee={committee}
                    key={index}
                    setAddCommittee={setAddCommittee}
                    addCommittee={addCommittee}
                    editCommittee={editCommittee}
                    setEditCommittee={setEditCommittee}
                    role={role}
                  />
                );
              })}
            </>
          )}
        </Box>
        {result?.data?.total > 0 && (
          <CustomPagination
            totalValues={result?.data?.total}
            handlePagination={handlePagination}
            hasPagination={result?.data?.total > 0 ? true : false}
            styleData={commonStyles.paginationStyle}
          />
        )}
      </Box>
    </Box>
  );
};

export default Committee;
