import { BLACKISH, DEFAULT_GREY } from "../../../../Constant/ColorConstant";

export const styles = {
  titleContainer: {
    display: "flex",
    cursor: "pointer",
  },
  cardMainContainer: {
    border: "1px solid #60C5F9",
    background: "#F8FCFF",
    padding: "1rem",
    margin: "1rem",
  },
  titleText: {
    // marginLeft: "0.35rem",
    fontWeight: 600,
    fontSize: "1rem",
    color: BLACKISH,
  },
  iconsContainer: {
    width: "8rem",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  smallText: {
    fontWeight: 400,
    fontSize: "0.875rem",
    color: BLACKISH,
  },
  smallTextStyle: {
    // marginLeft: "1.1rem",
  },
  dividerStyle: {
    fontSize: "0.625rem",
    color: DEFAULT_GREY,
    marginX: "0.4rem",
  },
  addressContainer: {
    display: "flex",
    alignItems: "center",
  },
};
