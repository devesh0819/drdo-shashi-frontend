import { Box, Tooltip } from "@mui/material";
import { ReactComponent as IconPencil } from "../../../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconDelete } from "../../../../Assets/Images/deleteIcon.svg";
import { ReactComponent as MeetIcon } from "../../../../Assets/Images/meetIcon.svg";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import { commonStyles } from "../../../../Styles/CommonStyles";
import { styles } from "./CommitteeListCardStyles.js";
import { Link, useNavigate } from "react-router-dom";
import { COMMITTEE } from "../../../../Routes/Routes";
import QpDialog from "../../../../Components/QpDialog/QpDialog";
import { useState, useEffect } from "react";
import QpConfirmModal from "../../../../Components/QpConfirmModal/QpConfirmModal";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteCommitteeAction,
  getCommitteeDetailAction,
} from "../../../../Redux/Admin/Member/memberActions";
import { ReactComponent as IconMenuSearch } from "../../../../Assets/Images/iconMenuSearch.svg";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import ContentPasteSearchIcon from "@mui/icons-material/ContentPasteSearch";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";

import AddCommittee from "../AddCommittee/AddCommittee";
import { ROLES } from "../../../../Constant/RoleConstant";

export default function CommitteeListCard(props) {
  const {
    setAddCommittee,
    setEditCommittee,
    editCommittee,
    addCommittee,
    role,
  } = props;
  const [committeeDialog, setCommitteeDialog] = useState(false);
  const [selectedMembers, setSelectedMembers] = useState([]);
  const { committee } = props;
  const committeeData = useSelector(
    (state) => state.members.committeeDetailData
  );
  useEffect(() => {
    if (committeeData?.data?.members) {
      const newMembers = committeeData?.data?.members?.map((member, index) => {
        return {
          id: member?.id,
          title: `${member?.first_name} ${member?.last_name}`,
        };
      });
      setSelectedMembers(newMembers);
    }
  }, [committeeData?.data?.members]);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleCommitteeConfirm = () => {
    dispatch(deleteCommitteeAction(committee?.uuid));
  };

  const handleEditCommittee = () => {
    dispatch(getCommitteeDetailAction(committee?.uuid, navigate));
  };

  return (
    <>
      {editCommittee?.uuid === committee?.uuid && !addCommittee ? (
        <AddCommittee
          setAddCommittee={setAddCommittee}
          editCommittee={editCommittee}
          setEditCommittee={setEditCommittee}
          defaultMembers={selectedMembers}
        />
      ) : (
        <Box sx={styles.cardMainContainer}>
          <Box>
            <Box sx={commonStyles.displayStyle}>
              <Box sx={styles.titleContainer}>
                <QpTypography
                  displayText={committee?.title}
                  styleData={styles.titleText}
                />
              </Box>
              <Box sx={styles.iconsContainer}>
                <MeetIcon />
                {role === ROLES.ADMIN && (
                  <Box
                    onClick={() => {
                      dispatch(getCommitteeDetailAction(committee?.uuid));
                      setEditCommittee(committee);
                      // setAddCommittee(true);
                    }}
                    sx={commonStyles.cursorPointer}
                  >
                    <Tooltip title="Edit" arrow>
                      <BorderColorOutlinedIcon sx={commonStyles.iconColor} />
                    </Tooltip>
                  </Box>
                )}
                <Box
                  onClick={handleEditCommittee}
                  sx={commonStyles.cursorPointer}
                >
                  <Tooltip title="View" arrow>
                    <ContentPasteSearchIcon sx={commonStyles.iconColor} />
                  </Tooltip>
                </Box>
                {role === ROLES.ADMIN && (
                  <Box
                    onClick={() => setCommitteeDialog(true)}
                    sx={commonStyles.cursorPointer}
                  >
                    <Tooltip title="Delete" arrow>
                      <DeleteOutlineIcon sx={commonStyles.iconColor} />
                    </Tooltip>
                  </Box>
                )}
              </Box>
            </Box>
            <Box sx={styles.addressContainer}>
              <QpTypography
                displayText={`No. of Members: ${
                  committee.members_count < 10
                    ? `0${committee.members_count}`
                    : committee.members_count
                }`}
                styleData={{ ...styles.smallText, ...styles.smallTextStyle }}
              />
              <QpTypography displayText="|" styleData={styles.dividerStyle} />
              <QpTypography
                displayText={`Meetings: ${
                  committee.meeting_counts < 10
                    ? `0${committee.meeting_counts}`
                    : committee.meeting_counts
                }`}
                styleData={styles.smallText}
              />
            </Box>
          </Box>
          <QpDialog
            open={committeeDialog}
            closeModal={setCommitteeDialog}
            styleData={commonStyles.dialogContainer}
          >
            <QpConfirmModal
              displayText="Are you sure you want to delete this committee?"
              closeModal={setCommitteeDialog}
              onConfirm={() => handleCommitteeConfirm()}
            />
          </QpDialog>
        </Box>
      )}
    </>
  );
}
