import * as colors from "../../../../Constant/ColorConstant";

export const styles = {
  headerOuterContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: "2.063rem",
    paddingRight: "2.063rem",
    paddingTop: "0.625rem",
    paddingBottom: "0.625rem",
    backgroundColor: colors.WHITE,
    borderBottom: "0.063rem solid #EAEAEA",
    height: "2.25rem",
  },
  topHeader: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  topAddButton: {
    background: "#1FBAED",
    boxShadow: "0px 4px 4px rgba(102, 104, 105, 0.18)",
    borderRadius: "100px",
    color: "white",
    "&:hover": {
      background: "#1FBAED",
    },
  },
  titleTextContainer: {
    display: "flex",
    alignItems: "center",
  },
  outerContainer: {
    // minHeight: "calc(100vh - 15rem)",
    // maxHeight: "calc(100vh - 15rem)",
    backgroundColor: "white",
    overflow: "auto",
  },
  backButton: {
    minWidth: "unset",
    padding: 0,
    paddingRight: "0.563rem",
    "&:hover": { backgroundColor: "transparent" },
  },
  titleText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1.125rem",
    lineHeight: "1.532rem",
    color: colors.BLACKISH,
  },
  lowerBlock: {
    backgroundColor: colors.WHITE,
    paddingX: "0.5rem",
    paddingY: "0.5rem",
    "@media(max-width:1200px)": {
      paddingY: "1rem",
    },
  },
  lowerContainer: {
    // border: "0.093rem solid #EAEAEA",
    // padding: "1rem",
    // margin: "1rem",
    //maxHeight: "calc(100vh - 21rem)",
    backgroundColor: colors.WHITE,
    paddingX: "1rem",
    paddingY: "1rem",
    height: "calc(100vh - 15.5rem)",
    "@media(max-width:1200px)": {
      paddingY: "1rem",
    },
  },
  tabContainer: {
    // borderBottom: 1,
    // borderColor: "divider",
    // display: "flex",
    // justifyContent: "space-between",
    backgroundColor: "#FAFAFA",
    border: "0.063rem solid #F0F1F2",
    borderRadius: "0.125rem",
  },
  tabsText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    textTransform: "none",
  },
  tabPanels: {
    marginY: "0.15rem",
    padding: 0,
  },
  containerButton: {
    marginBottom: "0.85rem",
    boxShadow: "0px 4px 4px rgba(102, 104, 105, 0.18)",
    borderRadius: "100px",
    color: "#1FBAED",
    "&:hover": {
      color: "#1FBAED",
    },
  },
  meetingModalContainer: {
    "& .MuiDialog-container": {
      marginLeft: "6rem",
      marginTop: "2.5rem",
      height: "100%",
      width: "85%",
    },
  },
  memberModalContainer: {
    "& .MuiDialog-container": {
      marginLeft: "6rem",
      marginTop: "2.5rem",
      height: "92%",
      width: "75%",
    },
  },
  noData: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "8rem 10rem",
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "1.125rem",
  },
  tableStyle: {
    padding: "0",
    maxHeight: "calc(100vh - 21rem)",
    minHeight: "calc(100vh - 21rem)",
  },
  tableBoxStyle: {
    minHeight: "100%",
  },
  headerCellStyle: {
    width: "25%",
  },
  tableBodyCellStyle: {
    width: "25%",
  },
  customPaginationContainer: {
    marginY: "1rem",
  },
  secondHeaderCellStyle: {
    width: "16.66%",
  },
  secondTableBodyCellStyle: {
    width: "12.66%",
  },
  marginRightFour: {
    marginRight: "4rem",
  },
};
