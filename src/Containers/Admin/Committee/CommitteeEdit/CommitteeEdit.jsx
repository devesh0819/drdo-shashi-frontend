import React, { useEffect, useState } from "react";
import { ReactComponent as IconPencil } from "../../../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconDelete } from "../../../../Assets/Images/deleteIcon.svg";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import {
  Button,
  Box,
  Typography,
  Tab,
  Tooltip,
  Grid,
  TextareaAutosize,
  InputBase,
} from "@mui/material";
import CustomTable from "../../../../Components/CustomTable/CustomTable";
import { ReactComponent as IconCircularBack } from "../../../../Assets/Images/iconCircularBack.svg";
import { useNavigate, useParams } from "react-router-dom";
import { COMMITTEE } from "../../../../Routes/Routes";
import { styles } from "./CommitteeEditStyles";
import { ReactComponent as MeetIcon } from "../../../../Assets/Images/meetIcon.svg";
import { ReactComponent as IconMenuSearch } from "../../../../Assets/Images/iconMenuSearch.svg";

import QpButton from "../../../../Components/QpButton/QpButton";
import QpDialog from "../../../../Components/QpDialog/QpDialog";
import QpMeetModal from "../../../../Components/QpMeetModal/QpMeetModal";
import QpMemberModal from "../../../../Components/QpMemberModal/QpMemberModal";
import * as _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteMemberAction,
  getCommitteeDetailAction,
} from "../../../../Redux/Admin/Member/memberActions";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import * as moment from "moment";
import {
  commonStyles,
  customCommonStyles,
} from "../../../../Styles/CommonStyles";
import { BLACKISH } from "../../../../Constant/ColorConstant";
import PostAddRoundedIcon from "@mui/icons-material/PostAddRounded";
import QpConfirmModal from "../../../../Components/QpConfirmModal/QpConfirmModal";
import SaveNextButtons from "../../../../Components/SaveNextButtons/SaveNextButtons";
import QpAddNote from "../../../../Components/QpAddNote/QpAddNote";
import { getMasterDataAction } from "../../../../Redux/Admin/GetMasterData/getMasterDataActions";
import ContentPasteSearchIcon from "@mui/icons-material/ContentPasteSearch";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";
import { getUserData } from "../../../../Services/localStorageService";
import { ROLES } from "../../../../Constant/RoleConstant";

const CommitteeEdit = () => {
  const [addMember, setAddMember] = useState(false);
  const [editMember, setEditMember] = useState();
  const [addMeet, setAddMeet] = useState(false);
  const [editMeet, setEditMeet] = useState(true);
  const [memberDialog, setMemberDialog] = useState(false);
  const [deleteMember, setDeleteMember] = useState();
  const [addNoteDialog, setAddNoteDialog] = useState(false);
  const [meetingId, setMeetingId] = useState();
  const [defaultAssessments, setDefaultAssessments] = useState([]);
  const [meetingTable, setMeetingTable] = useState([]);
  const [memberTable, setMemberTable] = useState([]);
  const userData = getUserData();
  const role = userData?.roles[0];

  const [addNoteData, setAddNoteData] = useState();
  const { id, meeting_id } = useParams();
  const [value, setValue] = useState("1");
  const dispatch = useDispatch();
  // #1065B5
  const result = useSelector((state) => state.members.committeeDetailData);

  const assessmentsList = useSelector(
    (state) => state.getMasterData.masterData
  );

  useEffect(() => {
    dispatch(getMasterDataAction({ type: "assessments" }));
  }, [dispatch]);

  useEffect(() => {
    if (id) {
      dispatch(getCommitteeDetailAction(id, navigate));
    }
  }, [id, dispatch]);

  const handleEditMember = (memberId) => {
    const data = result?.data?.members.filter(
      (item) => item?.uuid === memberId
    );
    setAddMember(true);
    setEditMember(data);
  };

  const handleEditMeeting = (meetId) => {
    const data = result?.data?.meetings.filter((item) => item?.uuid === meetId);
    setEditMeet(data[0]);
    setAddMeet(true);
  };

  const handleMemberDelete = (memberId) => {
    setMemberDialog(true);
    setDeleteMember(memberId);
  };

  const handleMemberDeleteConfirm = () => {
    dispatch(deleteMemberAction(deleteMember));
  };

  const membersColumn = [
    {
      field: "Member name",
    },
    { field: "Role" },
    { field: "Email" },
    { field: "Organisation" },
    { field: "Designation" },
    { field: "Mobile" },

    // { field: "" },
    role === ROLES.ADMIN && {
      field: "",
      renderColumn: (row) => {
        return (
          <Box sx={commonStyles.committeeIcons}>
            <Box className="icons" sx={commonStyles.displayNone}>
              <Button
                sx={{
                  ...commonStyles.iconButtonStyle,
                  ...styles.marginRightFour,
                }}
                onClick={() => {
                  handleEditMember(row.id);
                }}
              >
                <BorderColorOutlinedIcon sx={commonStyles.iconColor} />
              </Button>
              {/* <Button
                sx={{
                  minWidth: "unset",
                  padding: 0,
                  "&:hover": { backgroundColor: "transparent" },
                }}
                onClick={
                  () => handleMemberDelete(row.id)

                  // dispatch(deleteMemberAction(row.id))
                }
              >
                <IconDelete />
              </Button> */}
            </Box>
          </Box>
        );
      },
    },
  ];

  const meetingColumn = [
    {
      field: "Meeting Title",
    },
    { field: "Date & Duration" },
    { field: "Meeting Mode" },
    // { field: "Meeting Link" },
    // { field: "Invited Members" },
    // { field: "" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Box sx={commonStyles.iconsBtnContainer}>
            <Box className="icons" sx={commonStyles.displayNone}>
              <Button
                sx={commonStyles.iconButtonStyle}
                onClick={() =>
                  navigate(`/committee/edit/${id}/meeting/${row.id}`)
                }
              >
                <Tooltip title="View" arrow>
                  <ContentPasteSearchIcon sx={commonStyles.iconColor} />
                </Tooltip>
              </Button>
              {role === ROLES.ADMIN && (
                <Button
                  sx={commonStyles.iconButtonStyle}
                  onClick={() => {
                    handleEditMeeting(row.id);
                  }}
                >
                  <Tooltip title="Edit" arrow>
                    <BorderColorOutlinedIcon sx={commonStyles.iconColor} />
                  </Tooltip>
                </Button>
              )}
              {role === ROLES.ADMIN && (
                <Button
                  sx={{
                    ...commonStyles.iconButtonStyle,
                    ...customCommonStyles.marginRightOne,
                  }}
                  onClick={() => {
                    setAddNoteDialog(true);
                    setAddNoteData(row);
                    setMeetingId(row.id);
                  }}
                >
                  <Tooltip title="Add note" arrow>
                    <PostAddRoundedIcon sx={commonStyles.iconColor} />
                  </Tooltip>
                </Button>
              )}
            </Box>
          </Box>
        );
      },
    },
    // {
    //   field: "",
    //   renderColumn: (row) => {
    //     return (
    //       <Box sx={{ width: "60px", display: "flex", justifyContent: "end" }}>
    //         <Box className="icons" sx={commonStyles.displayNone}>
    //           <Button
    //             sx={{
    //               minWidth: "unset",
    //               padding: 0,
    //               marginRight: "1rem",
    //               "&:hover": { backgroundColor: "transparent" },
    //             }}
    //             onClick={() => {
    //               setAddNoteDialog(true);
    //               setAddNoteData(row);
    //             }}
    //           >
    //             <Tooltip title="Add note" arrow>
    //               <PostAddRoundedIcon
    //                 sx={{
    //                   color: "#1065B5",
    //                   fontSize: "large",
    //                   fontWeight: 500,
    //                 }}
    //               />
    //             </Tooltip>
    //           </Button>
    //         </Box>
    //       </Box>
    //     );
    //   },
    // },
  ];

  const navigate = useNavigate();
  const handleChange = (e, newValue) => {
    setValue(newValue);
  };
  const handleConfirm = () => {};

  useEffect(() => {
    if (result?.data?.members?.length > 0) {
      const membersTableRow = result?.data?.members
        ? result?.data?.members.map((member, index) => {
            return {
              "Member name": `${member.first_name} ${member.last_name}`,
              Role: member.member_role || "Member",
              Email: member.email,
              id: member.uuid,
              Organisation: member.organisation,
              Designation: member.designation,
              Mobile: member.phone_number,
            };
          })
        : [];
      setMemberTable(membersTableRow);
    }
  }, [result?.data?.members]);
  useEffect(() => {
    if (result?.data?.meetings?.length > 0) {
      const meetingTableRow = result?.data?.meetings
        ? result?.data?.meetings.map((meeting, index) => {
            return {
              "Meeting Title": meeting?.meeting_title || "",
              "Date & Duration":
                moment(meeting?.meeting_date_time).format(
                  "D MMM,YYYY hh:mm A"
                ) || "",
              "Meeting Mode":
                meeting?.meeting_type === "online" ? "Online" : "Offline",
              // "Meeting Link ": meeting?.meeting_link || "",
              id: meeting.uuid,
            };
          })
        : [];
      setMeetingTable(meetingTableRow);
    }
  }, [result?.data?.meetings]);

  useEffect(() => {
    if (editMeet?.meeting_assessments && assessmentsList?.data) {
      setDefaultAssessments(
        editMeet?.meeting_assessments?.map((assessment) =>
          _.find(assessmentsList?.data, { id: assessment })
        )
      );
    }
  }, [editMeet?.meeting_assessments, assessmentsList?.data]);

  return (
    <Box sx={styles.outerContainer}>
      <Box sx={styles.headerOuterContainer}>
        <Box sx={styles.topHeader}>
          <Box sx={styles.titleTextContainer}>
            <Button
              disableRipple
              sx={styles.backButton}
              onClick={() => navigate(COMMITTEE)}
            >
              <IconCircularBack />
            </Button>
            <Typography style={styles.titleText}>
              {result?.data?.title}
            </Typography>
          </Box>
          {value === "1" && role === ROLES.ADMIN && (
            <QpButton
              variant="outlined"
              displayText="+ Add Member"
              onClick={() => {
                setAddMember(true);
                setEditMember(null);
              }}
              // styleData={styles.containerButton}
              styleData={styles.topAddButton}
            />
          )}
          {value === "2" && role === ROLES.ADMIN && (
            <Button
              variant="outlined"
              startIcon={<MeetIcon />}
              onClick={() => {
                setAddMeet(true);
                setEditMeet(null);
                setDefaultAssessments([]);
              }}
              // sx={styles.containerButton}
              sx={styles.topAddButton}
            >
              New Meeting
            </Button>
          )}
        </Box>
      </Box>
      {/* <Box sx={styles.lowerBlock}> */}
      <Box sx={styles.lowerContainer}>
        {/* <QpTypography displayText={result?.data?.title} /> */}
        <TabContext value={value}>
          <Box sx={styles.tabContainer}>
            <TabList onChange={handleChange}>
              <Tab label="Member" value="1" sx={styles.tabsText} />
              <Tab label="Meetings" value="2" sx={styles.tabsText} />
            </TabList>
          </Box>
          <TabPanel value="1" sx={styles.tabPanels}>
            <>
              {memberTable?.length === 0 ? (
                <Box sx={styles.noData}>No data available</Box>
              ) : (
                <CustomTable
                  columnDefs={membersColumn}
                  rowData={memberTable}
                  hasPagination={false}
                  styleData={styles.tableStyle}
                  boxStyle={styles.tableBoxStyle}
                  headerCellStyle={styles.headerCellStyle}
                  tableBodyCellStyle={styles.tableBodyCellStyle}
                  totalValues={memberTable?.length}
                  customPaginationContainer={styles.customPaginationContainer}
                />
              )}
            </>
          </TabPanel>
          <TabPanel value="2" sx={styles.tabPanels}>
            {meetingTable?.length > 0 ? (
              <CustomTable
                columnDefs={meetingColumn}
                rowData={meetingTable}
                styleData={styles.tableStyle}
                boxStyle={styles.tableBoxStyle}
                headerCellStyle={styles.secondHeaderCellStyle}
                tableBodyCellStyle={styles.secondTableBodyCellStyle}
                totalValues={meetingTable?.length}
                hasPagination={false}
                customPaginationContainer={styles.customPaginationContainer}
              />
            ) : (
              <Box sx={styles.noData}>No data available</Box>
            )}
          </TabPanel>
        </TabContext>
        {/* </Box> */}
      </Box>
      {/* {addMeet && ( */}
      <QpDialog
        open={addMeet}
        title="Schedule Meeting"
        closeModal={setAddMeet}
        dialogSize="xl"
        styleData={styles.meetingModalContainer}
      >
        <QpMeetModal
          closeModal={setAddMeet}
          onConfirm={() => handleConfirm()}
          editMeet={editMeet}
          defaultAssessments={defaultAssessments}
        />
      </QpDialog>
      {/* )} */}
      {addMember && (
        <QpDialog
          open={addMember}
          title={
            editMember && editMember[0]?.uuid ? "Edit Member" : "Add Member"
          }
          closeModal={setAddMember}
          styleData={styles.memberModalContainer}
        >
          <QpMemberModal
            closeModal={setAddMember}
            committeeTitle={result?.data?.title}
            memberDetail={editMember}
            onConfirm={() => handleConfirm()}
          />
        </QpDialog>
      )}
      <QpDialog
        open={memberDialog}
        closeModal={setMemberDialog}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to delete this member?"
          closeModal={setMemberDialog}
          onConfirm={() => handleMemberDeleteConfirm()}
        />
      </QpDialog>
      <QpDialog
        open={addNoteDialog}
        title="Add Note"
        closeModal={setAddNoteDialog}
        // styleData={styles.memberModalContainer}
        // styleData={commonStyles.dialogContainer}
      >
        <QpAddNote closeModal={setAddNoteDialog} meetingId={meetingId} />
      </QpDialog>
    </Box>
  );
};

export default CommitteeEdit;
