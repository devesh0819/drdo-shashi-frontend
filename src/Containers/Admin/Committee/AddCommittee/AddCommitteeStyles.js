export const styles = {
    committeeInput: {
      height: "3rem",
    },
    committeeLabel: {
      fontSize: "0.95rem",
    },
    memberLabel: {
      fontSize: "0.95rem",
      fontWeight: 600
    },
    committeeFormContainer: {
      margin: "1rem",
      border: "1px solid #60C5F9",
      padding: "1rem",
    },
    addCommitteeHeading: {
      fontWeight: 700,
      textAlign: "center",
    },
  };
  