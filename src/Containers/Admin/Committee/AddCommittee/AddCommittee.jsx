import React, { useState, useEffect } from "react";
import { Autocomplete, Box, Grid, InputBase, TextField } from "@mui/material";
import { styles } from "./AddCommitteeStyles.js";
import { useDispatch, useSelector } from "react-redux";
import { commonStyles } from "../../../../Styles/CommonStyles";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import SaveNextButtons from "../../../../Components/SaveNextButtons/SaveNextButtons.jsx";
import {
  addCommitteeAction,
  updateCommitteeAction,
} from "../../../../Redux/Admin/Member/memberActions.js";
import * as _ from "lodash";
import { getMasterDataAction } from "../../../../Redux/Admin/GetMasterData/getMasterDataActions.js";
import QpInputLabel from "../../../../Components/QpInputLabel/QpInputLabel.jsx";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { committeeSchema } from "../../../../validationSchema/committeeSchema";
import { useNavigate } from "react-router-dom";

export default function AddCommittee({
  setAddCommittee,
  editCommittee,
  setEditCommittee,
  defaultMembers,
}) {
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(committeeSchema),
    mode: "onChange",
  });

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [selectedMembers, setSelectedMembers] = useState([]);
  const [defaultSelectedMembers, setDefaultSelectedMembers] = useState([]);

  const membersList = useSelector((state) => state.getMasterData.masterData);

  useEffect(() => {
    dispatch(getMasterDataAction({ type: "member" }));
  }, [dispatch]);

  useEffect(() => {
    if (selectedMembers.length > 0) {
      setValue("members", selectedMembers, { shouldValidate: true });
    }
  }, [selectedMembers]);

  const cancelHandler = () => {
    setAddCommittee(false);
    setEditCommittee();
  };

  const onSubmit = (data) => {
    if (editCommittee?.uuid) {
      dispatch(updateCommitteeAction(editCommittee?.uuid, data, navigate));
    } else {
      dispatch(addCommitteeAction(data, navigate));
    }
    setAddCommittee(false);
  };

  useEffect(() => {
    if (defaultMembers?.length > 0) {
      setDefaultSelectedMembers(defaultMembers);
    }
  }, [defaultMembers]);

  return (
    <Box sx={styles.committeeFormContainer}>
      <QpTypography
        displayText={editCommittee?.uuid ? "Edit Committee" : "Add Committee"}
        styleData={styles.addCommitteeHeading}
      />
      <Grid
        container
        rowSpacing={1}
        columnSpacing={"3rem"}
        sx={commonStyles.signUpContainer}
      >
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <QpInputLabel
            displayText="Title"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <InputBase
            id="title"
            name="title"
            required
            sx={commonStyles.textInputStyle}
            {...register("title")}
            error={errors.title ? true : false}
            defaultValue={editCommittee?.title}
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.title?.message}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <QpInputLabel
            displayText={"Select Members"}
            required={true}
            id="members"
            name="members"
            styleData={commonStyles.inputLabel}
          />
          <Autocomplete
            multiple
            id="tags-outlined"
            options={membersList?.data || []}
            onChange={(event, newValue) => {
              setSelectedMembers(newValue.map((option) => option.id || option));
              setDefaultSelectedMembers(newValue);
            }}
            getOptionLabel={(option) => option.title}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                style={{
                  backgroundColor: "#fff",
                  borderBottomLeftRadius: "0.5rem",
                  borderTopLeftRadius: "0.5rem",
                  borderTopRightRadius: 0,
                  minWidth: "520px",
                }}
              />
            )}
            value={defaultSelectedMembers}
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.members?.message}
          />
        </Grid>
      </Grid>
      <SaveNextButtons
        blueButtonText="Save"
        greyButtonText="Cancel"
        onSaveClick={cancelHandler}
        onNextClick={handleSubmit(onSubmit)}
      />
    </Box>
  );
}
