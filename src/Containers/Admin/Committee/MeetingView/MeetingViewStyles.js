import { BLACKISH } from "../../../../Constant/ColorConstant";

export const meetingStyles = {
  containerHeight: {
    maxHeight: "calc(100vh - 13.8rem)",
    minHeight: "calc(100vh - 13.8rem)",
  },
  meetingContainer: {
    paddingTop: "0.625rem",
    paddingBottom: "0.625rem",
    paddingLeft: "1.063rem",
    paddingRight: "1.063rem",
    marginBottom: "1rem",
  },
  meetingDisplay: {
    display: "flex",
    alignItems: "center",
  },
  dateText: {
    fontWeight: 600,
    fontSize: "0.875rem",
    color: BLACKISH,
  },
  contentText: {
    fontWeight: 400,
    fontSize: "0.875rem",
    color: BLACKISH,
  },
};
