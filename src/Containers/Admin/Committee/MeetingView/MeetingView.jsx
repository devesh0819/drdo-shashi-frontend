import { Box, Button, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  commonStyles,
  customCommonStyles,
} from "../../../../Styles/CommonStyles";
import { styles } from "../../ManageAssessments/ManageAssessmentsEdit/ManageAssessmentsEditStyles.js";
import { meetingStyles } from "./MeetingViewStyles";
import { ReactComponent as IconCircularBack } from "../../../../Assets/Images/iconCircularBack.svg";
import { useDispatch, useSelector } from "react-redux";
import { getCommitteeDetailAction } from "../../../../Redux/Admin/Member/memberActions";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import { BLACKISH } from "../../../../Constant/ColorConstant";
import * as moment from "moment";
import * as _ from "lodash";
export default function MeetingView() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id, meeting_id } = useParams();
  const [notes, setNotes] = useState([]);
  const result = useSelector((state) => state.members.committeeDetailData);

  useEffect(() => {
    if (id) {
      dispatch(getCommitteeDetailAction(id, null));
    }
  }, [id]);

  useEffect(() => {
    if (result?.data?.meetings?.length > 0) {
      let i;
      let all_meeting_notes = [];
      all_meeting_notes = _.find(result?.data?.meetings, { uuid: meeting_id });
      //   for (i = 0; i < result?.data?.meetings?.length; i++) {
      //     all_meeting_notes = all_meeting_notes.concat(
      //       result?.data?.meetings[i]?.meeting_notes
      //     );
      // }
      setNotes(all_meeting_notes?.meeting_notes);
    }
  }, [result?.data?.meetings]);

  return (
    <>
      <Box sx={customCommonStyles.outerContainer}>
        <Box>
          <Box
            style={{
              ...commonStyles.topHeader,
              ...commonStyles.displayStyle,
              ...meetingStyles.meetingContainer,
            }}
          >
            <Box
              sx={{
                ...styles.titleTextContainer,
                ...meetingStyles.meetingDisplay,
              }}
            >
              <Button
                disableRipple
                sx={styles.backButton}
                onClick={() => navigate(`/committee/edit/${id}`)}
              >
                <IconCircularBack />
              </Button>
              <Typography style={commonStyles.titleText}>
                Meeting Notes
              </Typography>
            </Box>
          </Box>
          <Box sx={meetingStyles.containerHeight}>
            {notes?.length === 0 ? (
              <Box sx={commonStyles.noDataStyle}>No data available</Box>
            ) : (
              notes?.map((note, index) => {
                let tempText = note?.meeting_notes.split("\n");

                return (
                  <>
                    <Box
                      sx={{
                        ...commonStyles.cardMainContainer,
                        ...customCommonStyles.marginLeftOne,
                      }}
                      key={index}
                    >
                      <QpTypography
                        displayText={moment(note?.created_at).format(
                          "D MMM,YYYY"
                        )}
                        styleData={meetingStyles.dateText}
                      />
                      {tempText?.map((text, index) => {
                        return (
                          <>
                            <QpTypography
                              displayText={text}
                              styleData={meetingStyles.contentText}
                            />
                            {index !== tempText?.length - 1 && (
                              <QpTypography
                                displayText={"\n"}
                                // styleData={{ fontSize: "0.75rem" }}
                              />
                            )}
                          </>
                        );
                      })}
                    </Box>
                  </>
                );
              })
            )}
          </Box>
        </Box>
      </Box>
    </>
  );
}
