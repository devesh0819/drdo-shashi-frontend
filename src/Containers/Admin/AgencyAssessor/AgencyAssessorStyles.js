import { commonStyles } from "../../../Styles/CommonStyles";

export const styles = {
  addAgency: {
    background: "#1FBAED",
    boxShadow: "0px 4px 4px rgba(102, 104, 105, 0.18)",
    borderRadius: "100px",
    color: "white",
    "&:hover": {
      background: "#1FBAED",
    },
  },
  agencyListCard: {
    height: "calc(100vh - 16.5rem)",
    overflow: "auto",
    ...commonStyles.customScrollBar,
  },
  addAssessorHeader: {
    padding: "0rem 2.063rem",
    paddingTop: "1.25rem",
  },
  addAssessorBtn: {
    width: "9rem",
    // "&:hover": {
    //   background: "#60C5F9",
    // },
    // width: "10rem",
    // backgroundColor: "rgb(44,106,159)",
    // "@media(max-width:1100px)": {
    //   width: "100%",
    // },
    // "&:hover": { backgroundColor: "rgb(44,106,159)" },
  },
  statusButton: {
    fontSize: "0.75rem",
    textTransform: "none",
    padding: "6px 10x",
    cursor: "default",
    width: "5rem",
  },
  dialogContainer: {
    height: "82%",
  },
};
