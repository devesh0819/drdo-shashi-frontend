import React, { useEffect, useState } from "react";
import { Box, Button, Typography } from "@mui/material";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import AgencyListCard from "./AgencyListCard/AgencyListCard";
import QpButton from "../../../Components/QpButton/QpButton";
import { styles } from "./AgencyAssessorStyles.js";
import CustomAgencyDetails from "../../../Components/CustomAgencyDetails/CustomAgencyDetails";
import { useDispatch, useSelector } from "react-redux";
import {
  getAgencyListAction,
  getAssessorDetail,
  getAssessorListAction,
  viewAssessorAction,
} from "../../../Redux/Admin/AgencyAssessor/agencyAssessorActions";
import * as _ from "lodash";
import { AGENCY_ASSESSOR } from "../../../Routes/Routes";
import { useNavigate } from "react-router-dom";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";
import { ReactComponent as IconPencil } from "../../../Assets/Images/iconPencil.svg";
import QpDialog from "../../../Components/QpDialog/QpDialog";
import CustomAssessorDetails from "../../../Components/CustomAssessorDetails/CustomAssessorDetails";
import FilterComponent from "../../../Components/FilterComponent/FilterComponent";
import {
  assessmentStatus,
  typeOfEnterpriseArray,
} from "../../../Constant/AppConstant";
import { getAssessorStatus } from "../../../Services/commonService";
import { getUserData } from "../../../Services/localStorageService";
import {
  getDistrictAction,
  getStatesAction,
} from "../../../Redux/Admin/GetMasterData/getMasterDataActions";
import { ROLES } from "../../../Constant/RoleConstant";
import { downloadAction } from "../../../Redux/AllApplications/allApplicationsActions";

const AgencyAssessor = () => {
  const [addAgency, setAddAgency] = useState(false);
  const [editAgencyId, setEditAgencyId] = useState();
  const [editAgency, setEditAgency] = useState();
  const [editAssessorId, setEditAssessorId] = useState();
  const [editAssessorData, setEditAssessorData] = useState();
  const [addAssessor, setAddAssessor] = useState(false);
  const [dataRowSet, setDataRowSet] = useState();
  const [openFilterDialog, setOpenFilterDialog] = useState(false);
  const [noOfFiltersApplied, setNoOfFiltersApplied] = useState(0);
  const [alreadySelectedFilters, setAlreadySelectedFilters] = useState({});
  const [pageNumber, setPageNumber] = useState(1);
  const userData = getUserData();

  const assessors = useSelector((state) => state.agencyAssessor.assessorList);
  const [state, setState] = useState();
  const [payloadState, setPayloadState] = useState();

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const stateOptions = useSelector((state) => state.getMasterData.statesData);
  const districtOptions = useSelector(
    (state) => state.getMasterData.districtData
  );

  const assessorDetail = useSelector(
    (state) => state.agencyAssessor.assessorDetail
  );

  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(AGENCY_ASSESSOR);
    });
  }, []);

  useEffect(() => {
    if (assessors?.assessors) {
      let rowData = assessors?.assessors?.map((assessor, index) => {
        return {
          "Assessor name": `${assessor.first_name} ${assessor.last_name}`,
          "Base location": `${assessor?.assessorMeta?.assessor_district?.title},${assessor?.assessorMeta?.assessor_state?.title}`,
          "Highest qualification with specialization":
            assessor?.assessorMeta?.expertise,
          "Domain Experience": assessor.assessorMeta?.work_domain,
          "Years of experience": assessor.assessorMeta?.years_of_experience,
          Status: getAssessorStatus(assessor?.assessorMeta?.assessor_status),
          Designation: assessor.designation,
          Email: assessor.email,
          id: assessor.uuid,
          first_name: assessor.first_name,
          last_name: assessor.last_name,
          phone_number: assessor.phone_number,
          address: assessor.address,
          expertise: assessor.expertise,
          // agency_id: agencies[0]?.agencyMeta?.user_id,
        };
      });
      setDataRowSet(rowData);
    }
  }, [assessors?.assessors, editAssessorId]);

  useEffect(() => {
    dispatch(getStatesAction());
  }, [dispatch]);

  useEffect(() => {
    if (state?.id) {
      dispatch(getDistrictAction(state?.id));
    }
  }, [dispatch, state?.id]);

  // useEffect(() => {
  //   if (editAgencyId) {
  //     setEditAgency(_.find(agencies, { uuid: editAgencyId }));
  //   }
  // }, [editAgencyId]);

  useEffect(() => {
    if (pageNumber) {
      dispatch(getAssessorListAction({ start: pageNumber, ...payloadState }));
    }
  }, [pageNumber, payloadState, dispatch]);
  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };
  useEffect(() => {
    if (assessorDetail) {
      setEditAssessorData(assessorDetail);
    }
  }, [assessorDetail]);
  const getStatusColor = (status) => {
    if (status === "Active") return "success";
    else if (status === "On-Hold") return "warning";
    else return "error";
  };
  const columnDefs = [
    { field: "Assessor name" },
    { field: "Base location", minWidth: "12rem" },
    { field: "Highest qualification with specialization", minWidth: "11rem" },
    { field: "Domain Experience" },
    { field: "Years of experience" },
    {
      field: "Status",
      renderColumn: (row) => {
        return (
          <>
            <Button
              color={getStatusColor(row["Status"])}
              variant="contained"
              disableRipple
              sx={styles.statusButton}
              disableTouchRipple={true}
              disableFocusRipple
              disableElevation
            >
              {row["Status"] || "NA"}
            </Button>
          </>
        );
      },
    },
    userData?.roles[0] === ROLES.ADMIN && {
      field: "",
      width: "3%",
      renderColumn: (row) => {
        return (
          <>
            {/* {addAgency || addAssessor || editAgencyId || addAgency ? ( */}
            {/* <Box>
              {" "}
              <BorderColorOutlinedIcon
                color="disabled"
                style={{ width: "1rem", height: "1rem" }}
              />
            </Box>
            ) : ( */}
            {userData?.roles[0] === ROLES.ADMIN && (
              <Button
                onClick={() => {
                  dispatch(viewAssessorAction(row.id));
                  setAddAssessor(true);
                  setEditAssessorId(row.id);
                  // setParticularAssessor(true);
                }}
                sx={commonStyles.cursorPointer}
              >
                <BorderColorOutlinedIcon style={commonStyles.iconColor} />
              </Button>
            )}
          </>
        );
      },
    },
  ];
  const addAssessorHandler = () => {
    setAddAssessor(true);
    dispatch(getAssessorDetail({}));
  };

  const filterOptions = [
    {
      filterId: 1,
      displayName: "Work Domain",
      filterType: "Search",
      key: "work_domain",
      labelId: "Work Domain",
      label: "Work domain",
      filterName: "workDomain",
    },
    {
      filterId: 2,
      displayName: "Location",
      filterType: "Location",
      key: "location",
      stateArray: stateOptions,
      // districtArray: districtOptions,
      labelId: "State",
      label: "State",
      labelIdDistrict: "District",
      labelDistrict: "District",
      filterName: "stateDistrict",
      // setState: setState,
      // stateValue: state,
    },
  ];

  const saveHandler = (payload) => {
    dispatch(getAssessorListAction({ start: pageNumber, ...payload }));
    setPageNumber(1);
    setPayloadState(payload);
  };

  const exportButtonClick = () => {
    const url = `${process.env.REACT_APP_BASE_URL}/admin/assessor?is_excel=1&excel_type=admin_assessor`;
    dispatch(downloadAction("AdminAssessors", dispatch, url, "doc", "xlsx"));
  };

  return (
    <>
      {/* {assessors?.length === 0 ? (
        <QpTypography
          displayText="No assessors available"
          styleData={commonStyles.noData}
        />
      ) : ( */}
      <>
        {openFilterDialog && (
          <FilterComponent
            saveHandler={saveHandler}
            filterOptions={filterOptions}
            alreadySelectedFilters={alreadySelectedFilters}
            setOpenFilterDialog={setOpenFilterDialog}
            setNoOfFiltersApplied={setNoOfFiltersApplied}
            setAlreadySelectedFilters={setAlreadySelectedFilters}
            stateValue={state}
            setState={setState}
            districtArray={districtOptions}
          />
        )}
        <CustomTable
          columnDefs={columnDefs}
          rowData={dataRowSet}
          // styleData={commonStyles.tableStyleData}
          // boxStyle={commonStyles.tableBoxStyleData}
          title="Implementation Partner and Assessors"
          hasPagination
          handlePagination={handlePagination}
          currentPage={pageNumber}
          totalValues={assessors?.total}
          styleData={commonStyles.tableHeight}
          topHeadButtonStyle={styles.addAssessorBtn}
          TopHeaderButtonText="Add Assessor"
          isTopHeadButton={userData?.roles[0] === ROLES.ADMIN ? true : false}
          // isQuestionairreTable
          topHeadButtonTextStyle={commonStyles.tableHeadButtonText}
          questionairreHeaderStyle={{
            ...commonStyles.displayStyle,
            ...styles.addAssessorHeader,
          }}
          topheaderButtonClick={addAssessorHandler}
          hasFilter
          onClickFilter={() => setOpenFilterDialog(!openFilterDialog)}
          noOfFiltersApplied={noOfFiltersApplied}
          buttonsDivStyle={commonStyles.divFlexStyle}
          exportButtonClick={exportButtonClick}
          hasExport={true}
          saveHandler={saveHandler}
          filterOptions={filterOptions}
          alreadySelectedFilters={alreadySelectedFilters}
          setOpenFilterDialog={setOpenFilterDialog}
          setNoOfFiltersApplied={setNoOfFiltersApplied}
          setAlreadySelectedFilters={setAlreadySelectedFilters}
          stateValue={state}
          setState={setState}
          districtArray={districtOptions}
        />
      </>
      {/* )} */}
      <QpDialog
        open={addAssessor}
        closeModal={setAddAssessor}
        getAssessorDetail={getAssessorDetail}
        setEditAssessorId={setEditAssessorId}
        dispatch={dispatch}
        styleData={styles.dialogContainer}
        title={editAssessorId ? "Edit Assessor" : "Add Assessor"}
      >
        <CustomAssessorDetails
          setAddAssessor={setAddAssessor}
          editAssessorId={editAssessorId}
          setEditAssessorId={setEditAssessorId}
          setEditAssessorData={setEditAssessorData}
          // agencyId={agencies[0]?.agencyMeta?.user_id}
          agencyId={4}
          assessorDetail={assessorDetail}
          defaultFirstName={editAssessorData?.first_name}
          defaultLastName={editAssessorData?.last_name}
          defaultEmail={editAssessorData?.email}
          defaultPhoneNo={editAssessorData?.phone_number}
          defaultAddress={editAssessorData?.assessorMeta?.address}
          defaultExpertise={editAssessorData?.assessorMeta?.expertise}
          defaultAlternatePhoneNo={
            editAssessorData?.assessorMeta?.alternate_phone_number
          }
          defaultAlternateEmail={
            editAssessorData?.assessorMeta?.alternate_email
          }
          defaultWorkDomain={editAssessorData?.assessorMeta?.work_domain}
          defaultExperienceYear={
            editAssessorData?.assessorMeta?.years_of_experience
          }
          defaultAssessorStatus={assessorDetail?.assessorMeta?.assesstor_status}
          defaultAssessorState={assessorDetail?.assessorMeta?.assesstor_state}
          defaultAssessorDistrict={
            assessorDetail?.assessorMeta?.assesstor_district
          }
        />
      </QpDialog>
    </>
  );
};

export default AgencyAssessor;
