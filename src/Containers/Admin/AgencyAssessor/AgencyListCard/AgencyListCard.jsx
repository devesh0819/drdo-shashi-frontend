import React, { useState, useEffect } from "react";
import { Box, Button } from "@mui/material";
import { ReactComponent as PlusAgency } from "../../../../Assets/Images/plusAgency.svg";
import { ReactComponent as MinusAgency } from "../../../../Assets/Images/minusAgency.svg";
import { ReactComponent as IconPencil } from "../../../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconDelete } from "../../../../Assets/Images/deleteIcon.svg";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import { commonStyles } from "../../../../Styles/CommonStyles";
import { styles } from "./AgencyListCardStyles.js";
import CustomTable from "../../../../Components/CustomTable/CustomTable";
import { useNavigate } from "react-router-dom";
import QpButton from "../../../../Components/QpButton/QpButton";
import CustomAgencyDetails from "../../../../Components/CustomAgencyDetails/CustomAgencyDetails";
import CustomAssessorDetails from "../../../../Components/CustomAssessorDetails/CustomAssessorDetails";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteAgencyAction,
  deleteAssessorAction,
  viewAssessorAction,
} from "../../../../Redux/Admin/AgencyAssessor/agencyAssessorActions";
import QpDialog from "../../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../../Components/QpConfirmModal/QpConfirmModal";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";

export default function AgencyListCard({
  agency,
  addAgency,
  agencyUuid,
  setAddAgency,
  setEditAgencyId,
  editAgency,
  editAgencyId,
  editAssessorId,
  setEditAssessorId,
  editAssessorData,
  setEditAssessorData,
  setEditAgency,
  addAssessor,
  setAddAssessor,
}) {
  const [showContainer, setShowContainer] = useState(false);
  const [openContainer, setOpenContainer] = useState(-1);
  const [particularAssessor, setParticularAssessor] = useState(false);

  const [dataRowSet, setDataRowSet] = useState();
  const [openAgencyDialog, setOpenAgencyDialog] = useState(false);
  const [openAssessorDialog, setOpenAssessorDialog] = useState(false);
  const [assessorId, setAssessorId] = useState();

  const assessorDetail = useSelector(
    (state) => state.agencyAssessor.assessorDetail
  );

  useEffect(() => {
    if (addAgency) {
      setShowContainer(false);
    }
  }, [addAgency]);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const columnDefs = [
    { field: "Employee name", width: "20%" },
    { field: "Designation", width: "20%" },
    { field: "Email", width: "50%" },

    {
      field: "",
      width: "3%",
      renderColumn: (row) => {
        return (
          <>
            {addAgency || addAssessor || editAgencyId || addAgency ? (
              <Box>
                {" "}
                <BorderColorOutlinedIcon
                  color="disabled"
                  style={styles.iconButton}
                />
              </Box>
            ) : (
              <Button
                onClick={() => {
                  dispatch(viewAssessorAction(row.id));
                  setAddAssessor(true);
                  setEditAssessorId(row.id);
                  setParticularAssessor(true);
                }}
                sx={commonStyles.cursorPointer}
              >
                <IconPencil />
              </Button>
            )}
          </>
        );
      },
    },
    // {
    //   field: "",
    //   width: "7%",
    //   renderColumn: (row) => {
    //     return (
    //       <Button
    //         // onClick={() => dispatch(deleteAssessorAction(row.id))}
    //         onClick={() => {
    //           setAssessorId(row.id);
    //           setOpenAssessorDialog(true);
    //         }}
    //         sx={commonStyles.cursorPointer}
    //       >
    //         <IconDelete />
    //       </Button>
    //     );
    //   },
    // },
    { field: "" },
  ];
  useEffect(() => {
    if (agency?.agencyMeta?.assessors) {
      let rowData = agency?.agencyMeta?.assessors
        ?.filter((assessor, index) => editAssessorId !== assessor.id)
        .map((assessor, index) => {
          return {
            "Employee name": assessor.name,
            Designation: assessor.designation,
            Email: assessor.email,
            id: assessor.id,
            first_name: assessor.first_name,
            last_name: assessor.last_name,
            phone_number: assessor.phone_number,
            address: assessor.address,
            expertise: assessor.expertise,
            agency_id: agency?.agencyMeta?.user_id,
          };
        });

      setDataRowSet(rowData);
    }
  }, [agency?.agencyMeta?.assessors, editAssessorId]);

  useEffect(() => {
    if (assessorDetail) {
      setEditAssessorData(assessorDetail);
    }
  }, [assessorDetail]);

  // useEffect(() => {
  //   if (!showContainer) {
  //     setOpenContainer(agencyUuid);
  //   } else {
  //     setOpenContainer();
  //   }
  // }, [showContainer]);

  useEffect(() => {
    setOpenContainer(-1);
  }, []);

  const handleAgencyConfirm = () => {
    setEditAgencyId();
    setEditAgency({});
    setAddAgency(false);
    dispatch(deleteAgencyAction(agency?.uuid));
  };
  const handleAssessorConfirm = () => {
    dispatch(deleteAssessorAction(assessorId));
  };

  return (
    <>
      {editAgencyId === agency?.uuid && !addAgency ? (
        <CustomAgencyDetails
          setAddAgency={setAddAgency}
          setEditAgencyId={setEditAgencyId}
          editAgencyId={editAgencyId}
          defaultFirstName={editAgency?.first_name}
          defaultLastName={editAgency?.last_name}
          defaultTitle={editAgency?.agencyMeta?.title}
          defaultAddress={editAgency?.agencyMeta?.address}
          defaultEmail={editAgency?.email}
          defaultPhoneNo={editAgency?.phone_number}
        />
      ) : (
        <Box
          sx={{
            ...styles.cardMainContainer,
            // ...(editAgency && commonStyles.displayNone),
          }}
        >
          <Box>
            <Box sx={commonStyles.displayStyle}>
              <Box
                sx={styles.imageTitleContainer}
                // onClick={() => setShowContainer(!showContainer)}
                onClick={() => {
                  if (openContainer === agencyUuid) {
                    setOpenContainer(-1);
                    return;
                  }
                  setOpenContainer(agencyUuid);
                }}
              >
                {openContainer !== agencyUuid ? (
                  <PlusAgency style={styles.imageSize} />
                ) : (
                  <MinusAgency style={styles.imageSize} />
                )}
                <QpTypography
                  displayText={agency?.agencyMeta?.title}
                  styleData={styles.titleText}
                />
              </Box>
              <Box sx={styles.iconsContainer}>
                {addAssessor || editAssessorId || addAgency || editAgencyId ? (
                  <Box>
                    {" "}
                    <BorderColorOutlinedIcon
                      color="disabled"
                      style={styles.iconButton}
                    />
                  </Box>
                ) : (
                  <Box
                    sx={{
                      ...commonStyles.cursorPointer,
                      // ...(addAgency && commonStyles.displayNone),
                    }}
                    onClick={() => {
                      setEditAgencyId(agency?.uuid);
                      // setAddAgency(true);
                    }}
                  >
                    <IconPencil />
                  </Box>
                )}

                {/* <Box
                  onClick={() => setOpenAgencyDialog(true)}
                  sx={commonStyles.cursorPointer}
                >
                  <IconDelete />
                </Box> */}
              </Box>
            </Box>
            <Box sx={styles.addressContainer}>
              <QpTypography
                displayText={agency?.agencyMeta?.address}
                styleData={{ ...styles.smallText, ...styles.smallTextStyle }}
              />
              <QpTypography displayText="|" styleData={styles.dividerStyle} />
              <QpTypography
                displayText={`No. of Assessors: ${
                  agency?.agencyMeta?.assessors.length < 10
                    ? `0${agency?.agencyMeta?.assessors.length}`
                    : agency?.agencyMeta?.assessors.length
                }`}
                styleData={styles.smallText}
              />
            </Box>
            {openContainer === agencyUuid && (
              <>
                {dataRowSet.length !== 0 && (
                  <CustomTable
                    columnDefs={columnDefs}
                    rowData={dataRowSet}
                    styleData={commonStyles.tableStyleData}
                    boxStyle={commonStyles.tableBoxStyleData}
                  />
                )}
                {addAssessor && particularAssessor && (
                  <CustomAssessorDetails
                    setAddAssessor={setAddAssessor}
                    editAssessorId={editAssessorId}
                    setEditAssessorId={setEditAssessorId}
                    setEditAssessorData={setEditAssessorData}
                    agencyId={agency?.agencyMeta?.user_id}
                    defaultFirstName={editAssessorData?.first_name}
                    defaultLastName={editAssessorData?.last_name}
                    defaultEmail={editAssessorData?.email}
                    defaultPhoneNo={editAssessorData?.phone_number}
                    defaultAddress={editAssessorData?.assessorMeta?.address}
                    defaultExpertise={
                      editAssessorData?.assessorMeta?.designation
                    }
                  />
                )}
                <QpButton
                  displayText="+  Add Assessor"
                  styleData={{
                    ...styles.addAssessor,
                    ...((editAssessorId || addAgency || addAssessor) && {
                      border: "none",
                    }),
                  }}
                  onClick={() => {
                    setParticularAssessor(true);
                    setAddAssessor(true);
                  }}
                  isDisable={
                    editAssessorId || addAgency || addAssessor ? true : false
                  }
                  // isDisable={true}
                />
              </>
            )}
          </Box>
          <QpDialog
            open={openAgencyDialog}
            closeModal={setOpenAgencyDialog}
            styleData={commonStyles.dialogContainer}
          >
            <QpConfirmModal
              displayText="Are you sure you want to delete this agency?"
              closeModal={setOpenAgencyDialog}
              onConfirm={() => handleAgencyConfirm()}
            />
          </QpDialog>
          <QpDialog
            open={openAssessorDialog}
            closeModal={setOpenAssessorDialog}
            styleData={commonStyles.dialogContainer}
          >
            <QpConfirmModal
              displayText="Are you sure you want to delete this assessor?"
              closeModal={setOpenAssessorDialog}
              onConfirm={() => handleAssessorConfirm()}
            />
          </QpDialog>
        </Box>
      )}
    </>
  );
}
