import {
  BACKGROUND_LIGHT_BLUE,
  BLACKISH,
  DEFAULT_GREY,
  LIGHT_BLUE,
} from "../../../../Constant/ColorConstant";

export const styles = {
  imageSize: {
    width: "23px",
    height: "23px",
  },
  iconButton: {
    width: "1rem",
    height: "1rem",
  },
  imageTitleContainer: {
    display: "flex",
    cursor: "pointer",
    alignItems: "center",
  },
  cardMainContainer: {
    border: "1px solid #60C5F9",
    background: "#F8FCFF",
    padding: "1rem",
    margin: "1rem",
  },
  titleText: {
    marginLeft: "0.75rem",
    fontWeight: 600,
    fontSize: "1rem",
    color: BLACKISH,
  },
  iconsContainer: {
    width: "5rem",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  smallText: {
    fontWeight: 400,
    fontSize: "0.875rem",
    color: BLACKISH,
  },
  smallTextStyle: {
    marginLeft: "2.188rem",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden",
    width: "5rem",
  },
  dividerStyle: {
    fontSize: "0.625rem",
    color: DEFAULT_GREY,
    marginX: "0.4rem",
  },
  addressContainer: {
    display: "flex",
    alignItems: "center",
  },
  addAssessor: {
    border: "1px solid #1FBAED",
    borderRadius: "100px",
    background: "white",
    color: "#1FBAED",
    fontWeight: 600,
    marginTop: "0.85rem",
    "&:hover": {
      background: "white",
    },
  },
};
