import React, { useEffect, useRef, useState } from "react";
import { Box, Button, Tab, Select, MenuItem, Typography } from "@mui/material";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import { styles } from "../../ManageAssessments/ManageAssessmentsEdit/ManageAssessmentsEditStyles.js";
import QpButton from "../../../../Components/QpButton/QpButton";
import { useNavigate, useParams } from "react-router-dom";
import { ReactComponent as IconCircularBack } from "../../../../Assets/Images/iconCircularBack.svg";
import { ALL_APPLICATIONS } from "../../../../Routes/Routes";
// import RenderQuestionairre from "./RenderQuestionairre/RenderQuestionairre.jsx";
import { useDispatch, useSelector } from "react-redux";
import AdminApplicationView from "../AdminApplicationView/AdminApplicationView";
import {
  getAgencyListingAction,
  getAssessorsListAction,
  updateAssignedAssessors,
} from "../../../../Redux/Admin/ManageAssessments/manageAssessmentsActions.js";
import {
  assignAgencyAction,
  getAdminApplicationDetail,
  getAdminApplicationDetailAction,
} from "../../../../Redux/Admin/AdminAllApplications/adminAllApplicationAction.js";
import * as _ from "lodash";
import {
  commonStyles,
  customCommonStyles,
} from "../../../../Styles/CommonStyles.js";
import PaymentDetails from "../../../Applicant/PaymentHistory/PaymentDetails.jsx";
import { APP_STAGE, ROLES } from "../../../../Constant/RoleConstant.js";
import PaymentBreakupDetails from "../PaymentBreakupDetails/PaymentBreakupDetails.jsx";
import { getPaymentBreakupDetailAction } from "../../../../Redux/Payment/paymentActions";
import QpDialog from "../../../../Components/QpDialog/QpDialog.jsx";
import QpConfirmModal from "../../../../Components/QpConfirmModal/QpConfirmModal.jsx";
import { getUserData } from "../../../../Services/localStorageService.js";
import AssignAgency from "./AssignAgency/AssignAgency.jsx";
import AssignAssessor from "../../ManageAssessments/AssignAssessor/AssignAssessor.jsx";
import { showToast } from "../../../../Components/Toast/Toast.js";
import { TITLE_LENGTH } from "../../../../Constant/AppConstant.js";
import CustomApplicatonView from "../../../../Components/CustomApplicationView/CustomApplicationView.jsx";

export default function AdminApplicationTabView() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();
  const userData = getUserData();
  const [value, setValue] = useState("1");
  const [selectedAgency, setSelectedAgency] = useState("");
  const [taxValue, setTaxValue] = useState(0);
  const [paymentModal, setPaymentModal] = useState(false);
  const [selectedLeadAssessor, setSelectedLeadAssessor] = useState({});
  const [selectedAssessors, setSelectedAssessors] = useState([]);
  const [selectedAssessorsIds, setSelectedAssessorsIds] = useState([]);
  const [defaultAssessorsIds, setDefaultAssessorsIds] = useState([]);
  const [defaultLeadAssessor, setDefaultLeadAssessor] = useState({});

  const [data, setData] = useState({ whether_local_assessor: "0" });
  const runOnce = useRef(false);
  const assessorListData = useSelector(
    (state) => state.manageAssessments.assessorListData
  );

  const agencyListingData = useSelector(
    (state) => state.manageAssessments.agencyListingData
  );
  const selectedAgencyData = useSelector(
    (state) => state.adminAllApplications.assignAgencyData
  );

  useEffect(() => {
    if (id) {
      dispatch(getAgencyListingAction());
    }
  }, [id]);

  const applicationDetail = useSelector(
    (state) => state.adminAllApplications.adminApplicationDetail
  );

  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getAdminApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getAdminApplicationDetail({}));
    };
  }, [id]);

  // useEffect(() => {
  //   if (selectedAgencyData?.agency_id) {
  //     setSelectedAgency(
  //       _.find(agencyListingData, { id: selectedAgencyData?.agency_id })
  //     );
  //   }
  // }, [selectedAgencyData?.agency_id]);

  useEffect(() => {
    if (applicationDetail?.assessment?.agency_id) {
      dispatch(
        getAssessorsListAction(applicationDetail?.assessment?.agency_id)
      );
    }
  }, [dispatch, applicationDetail?.assessment?.agency_id]);
  useEffect(() => {
    if (applicationDetail) {
      setDefaultAssessorsIds(
        applicationDetail?.assessors?.map((assessor) => assessor.assessor_id)
      );
      setDefaultLeadAssessor(applicationDetail?.lead_assessor?.id);
    }
  }, [applicationDetail]);

  useEffect(() => {
    if (assessorListData && defaultLeadAssessor) {
      setSelectedLeadAssessor(
        _.find(assessorListData, { id: defaultLeadAssessor })
      );

      setSelectedAssessors(
        defaultAssessorsIds?.map((id) => ({
          id: id,
          title: _.find(assessorListData, { id: id })?.title,
        }))
      );
      setSelectedAssessorsIds(defaultAssessorsIds);
    }
  }, [assessorListData, defaultLeadAssessor]);
  const handleChange = (e, newValue) => {
    setValue(newValue);
  };

  const updateHandler = () => {
    // dispatch(
    //   assignAgencyAction(id, { agency_id: selectedAgency?.id }, navigate)
    // );
    if (selectedAssessors?.length < 1) {
      showToast("Please select atleast one assessor", "error");
      return;
    } else if (selectedAssessors?.length > 0 && !selectedLeadAssessor) {
      showToast("Please select Lead Assessor", "error");
      return;
    }
    dispatch(
      updateAssignedAssessors(
        {
          uuid: applicationDetail?.assessment?.assessment_uuid,
          body: {
            assessor_ids: selectedAssessorsIds,
            lead_assessor_id: selectedLeadAssessor?.id,
          },
        },
        navigate
      )
    );
  };
  const saveHandler = () => {
    dispatch(
      getPaymentBreakupDetailAction(
        id,
        {
          local_assessor: Number(data.whether_local_assessor),
        },
        navigate
      )
    );
  };
  return (
    <>
      <Box sx={styles.outerContainer}>
        <Box
          sx={{
            ...styles.headerOuterContainer,
            ...commonStyles.topHeaderHeight,
          }}
        >
          <Box sx={styles.topHeader}>
            <Button
              disableRipple
              sx={styles.backButton}
              onClick={() => navigate(ALL_APPLICATIONS)}
              className="backButton"
            >
              <IconCircularBack />
            </Button>
            <Typography
              sx={{
                ...styles.titleText,
                ...(applicationDetail?.enterprise_name?.length > TITLE_LENGTH &&
                  styles.titleTextEllipsis),
              }}
            >
              {applicationDetail?.enterprise_name}
            </Typography>
          </Box>
          {value === "2" && (
            <QpButton
              styleData={styles.updateButton}
              displayText="Update"
              textStyle={styles.updateButtonText}
              onClick={updateHandler}
            />
          )}
          {value === "3" && userData?.roles[0] === ROLES.ADMIN && (
            <QpButton
              styleData={styles.updateButton}
              displayText="Save"
              textStyle={styles.updateButtonText}
              onClick={() => setPaymentModal(true)}
            />
          )}
        </Box>
        <Box sx={styles.lowerContainer}>
          <TabContext value={value}>
            <Box sx={styles.tabContainer}>
              <TabList
                onChange={handleChange}
                sx={styles.tabList}
                className="tabList"
              >
                <Tab label="Applicant Detail" value="1" sx={styles.tabsText} />
                {/* {applicationDetail?.application_stage !==
                  APP_STAGE.APPROVED && (
                  <Tab label="Assigned Agency" value="2" sx={styles.tabsText} />
                )} */}
                {applicationDetail?.application_stage ===
                  APP_STAGE.SCHEDULED && (
                  <Tab
                    label="Assign Assessors"
                    value="2"
                    sx={styles.tabsText}
                  />
                )}
                {applicationDetail?.application_stage ===
                  APP_STAGE.APPROVED && (
                  <Tab label="Payment Details" value="3" sx={styles.tabsText} />
                )}
              </TabList>
            </Box>
            <TabPanel value="1" sx={styles.tabPanels}>
              <AdminApplicationView applicationDetail={applicationDetail} />
            </TabPanel>
            {/* <TabPanel value="2" sx={styles.tabPanels}>
              <AssignAgency
                value={selectedAgency}
                setValue={setSelectedAgency}
                selectArray={agencyListingData?.data || []}
              />
            </TabPanel> */}
            <TabPanel value="2" sx={styles.tabPanels}>
              <AssignAssessor
                data={assessorListData}
                selectArray={selectedAssessors}
                setSelectedAssessors={setSelectedAssessors}
                value={selectedLeadAssessor}
                setValue={setSelectedLeadAssessor}
                setSelectedAssessorsIds={setSelectedAssessorsIds}
                defaultAssessorsArray={selectedAssessors}
                // readOnly={
                //   applicationDetail?.progress !== APP_STAGE.ASSESSORS_ASSIGNED
                //     ? true
                //     : false
                // }
              />
            </TabPanel>
            <TabPanel value="3" sx={styles.tabPanels}>
              <PaymentBreakupDetails
                applicationDetail={applicationDetail}
                data={data}
                setData={setData}
                tdsFeeTextRequired={false}
                setTaxValue={setTaxValue}
                taxValue={taxValue}
              />
            </TabPanel>
          </TabContext>
        </Box>
      </Box>
      <QpDialog
        open={paymentModal}
        closeModal={() => setPaymentModal(false)}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to approve the payment?"
          closeModal={() => setPaymentModal(false)}
          onConfirm={saveHandler}
        />
      </QpDialog>
    </>
  );
}
