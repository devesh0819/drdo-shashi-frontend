import React from "react";
import { Box, MenuItem, Select, Typography } from "@mui/material";
import { styles } from "../../../ManageAssessments/ManageAssessmentsEdit/ManageAssessmentsEditStyles.js";
import { useLocation } from "react-router-dom";
import { commonStyles } from "../../../../../Styles/CommonStyles.js";

export default function AssignAgency(props) {
  const handleChange = (event) => {
    props.setValue(event.target.value);
  };

  return (
    <Box>
      <Box sx={styles.greyContainer}>
        <Typography sx={styles.italicText}>
          {`${
            !props.canChangeAnytimeText
              ? "You have not assigned any agency yet"
              : "You can change assigned agency at any time"
          }`}
        </Typography>
        <Typography sx={styles.heading}>Agency Name</Typography>
        <Select
          labelId="Agency Name"
          id="Agency Name"
          disabled={props.disabled}
          value={props.value}
          MenuProps={{
            sx: commonStyles.menuProps,
          }}
          displayEmpty
          onChange={handleChange}
          sx={styles.select}
          renderValue={(val) => {
            if (!val) {
              return (
                <Typography sx={commonStyles.placeHolderColor}>
                  Select{" "}
                </Typography>
              );
            }
            return <Typography>{val?.title}</Typography>;
          }}
        >
          {props.selectArray.map((item, index) => (
            <MenuItem value={item} key={index} sx={styles.dropdownText}>
              {item.title}
            </MenuItem>
          ))}
        </Select>
      </Box>
    </Box>
  );
}
