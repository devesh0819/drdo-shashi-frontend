import React, { useEffect, useState } from "react";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import { ReactComponent as IconCircularDownload } from "../../../Assets/Images/iconCircularDownload.svg";
import { ReactComponent as IconPencil } from "../../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconMenuSearch } from "../../../Assets/Images/iconMenuSearch.svg";
import { Button, Box, Tooltip, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import * as moment from "moment";
import { useNavigate } from "react-router-dom";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { commonStyles } from "../../../Styles/CommonStyles";
import { getAdminAllApplicationsAction } from "../../../Redux/Admin/AdminAllApplications/adminAllApplicationAction";
import QpButton from "../../../Components/QpButton/QpButton";
import { ALL_APPLICATIONS, MANAGE_ASSESSMENTS } from "../../../Routes/Routes";
import {
  getAppStatus,
  getEnterpriceType,
} from "../../../Services/commonService";
import { APP_STAGE, ROLES } from "../../../Constant/RoleConstant";
import {
  downloadAction,
  downloadReportAction,
} from "../../../Redux/AllApplications/allApplicationsActions";
import { ReactComponent as IconCircularDownloadDisable } from "../../../Assets/Images/iconCircularDisable.svg";
import FilterComponent from "../../../Components/FilterComponent/FilterComponent";
import {
  assessmentStatus,
  paymentStatus,
  typeOfEnterpriseArray,
  vendorStatus,
  yesNo,
} from "../../../Constant/AppConstant";
import { getUserData } from "../../../Services/localStorageService";
import DownloadForOfflineOutlinedIcon from "@mui/icons-material/DownloadForOfflineOutlined";
import { getAssessorsListAction } from "../../../Redux/Admin/ManageAssessments/manageAssessmentsActions";
import WorkspacePremiumIcon from "@mui/icons-material/WorkspacePremium";
import ContentPasteSearchIcon from "@mui/icons-material/ContentPasteSearch";

const AllApplications = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [rowData, setRowData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [openFilterDialog, setOpenFilterDialog] = useState(false);
  const [noOfFiltersApplied, setNoOfFiltersApplied] = useState(0);
  const [alreadySelectedFilters, setAlreadySelectedFilters] = useState({});
  const [payloadState, setPayloadState] = useState();

  const userData = getUserData();
  const role = userData?.roles[0];

  const allApplications = useSelector(
    (state) => state.adminAllApplications.allAdminApplications
  );
  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(ALL_APPLICATIONS);
    });
  }, []);

  const columnDefs = [
    { field: "Application number" },
    { field: "Name of Enterprise", ellipsisClass: true },
    { field: "Type of Enterprise" },
    // { field: "Payment Status" },
    {
      field: "Payment Status",
      // width: "5%",
      onClick: (row) => {
        row["Payment Status"] === "Complete" &&
          navigate(
            `/all-applications/payment-complete-details/${row.id}?readOnly=true`
          );
      },
    },
    { field: "Application Status", minWidth: "11rem" },
    { field: "Application Date" },
    { field: "Assessment Date" },
    { field: "Subsidy" },
    { field: "Feedback" },
    // { field: "Assessment no." },
    // { field: "Enterprise Name" },
    // // { field: "Application Date" },
    // { field: "Last Updated" },
    // { field: "Status" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <>
            {row.status === APP_STAGE.APPROVED && (
              <QpButton
                displayText="Payment Review"
                styleData={{
                  ...commonStyles.blueButton,
                  ...commonStyles.tableButton,
                }}
                textStyle={{
                  ...commonStyles.blueButtonText,
                  ...commonStyles.tableButtonText,
                }}
                onClick={() => navigate(`/all-applications/${row.id}`)}
              />
            )}
            <Box sx={commonStyles.adminApplicationIcon}>
              {row.status !== APP_STAGE.APPROVED && (
                <Button
                  sx={commonStyles.iconButtonStyle}
                  onClick={() => navigate(`/all-applications/${row.id}`)}
                >
                  <Tooltip title="View" arrow>
                    <ContentPasteSearchIcon style={commonStyles.iconColor} />
                  </Tooltip>
                </Button>
              )}
              {row.status !== APP_STAGE.APPROVED && (
                <Button sx={commonStyles.iconButtonStyle}>
                  <Tooltip title="Report" arrow>
                    <DownloadForOfflineOutlinedIcon
                      style={
                        row.status ===
                          APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                        row.status === APP_STAGE.FEEDBACK_SUBMITTED ||
                        row.status === APP_STAGE.ASSESSMENT_COMPLETED
                          ? commonStyles.iconColor
                          : commonStyles.greyIconColor
                      }
                      onClick={() =>
                        (row.status ===
                          APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                          row.status === APP_STAGE.FEEDBACK_SUBMITTED ||
                          row.status === APP_STAGE.ASSESSMENT_COMPLETED) &&
                        dispatch(
                          downloadReportAction(
                            row.id,
                            row.application_number,
                            dispatch,
                            "report"
                          )
                        )
                      }
                    />
                  </Tooltip>
                </Button>
              )}
              {row.status !== APP_STAGE.APPROVED && (
                <Button sx={commonStyles.iconButtonStyle}>
                  <Tooltip title="Certificate" arrow>
                    <WorkspacePremiumIcon
                      style={
                        row.status ===
                          APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                        row.status === APP_STAGE.FEEDBACK_SUBMITTED ||
                        row.status === APP_STAGE.ASSESSMENT_COMPLETED
                          ? commonStyles.iconColor
                          : commonStyles.greyIconColor
                      }
                      onClick={() =>
                        (row.status ===
                          APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                          row.status === APP_STAGE.FEEDBACK_SUBMITTED ||
                          row.status === APP_STAGE.ASSESSMENT_COMPLETED) &&
                        dispatch(
                          downloadReportAction(
                            row.id,
                            row.application_number,
                            dispatch,
                            "certificate"
                          )
                        )
                      }
                    />
                  </Tooltip>
                </Button>
              )}
            </Box>
          </>
        );
      },
    },
    // {
    //   field: "",
    //   renderColumn: (row) => {
    //     return (
    //       <Box sx={{ width: "120px" }}>
    //         <Box className="icons" sx={commonStyles.displayNone}>
    //           <Button
    //             sx={{
    //               minWidth: "unset",
    //               padding: 0,
    //               marginRight: "1.75rem",
    //               "&:hover": { backgroundColor: "transparent" },
    //             }}
    //             onClick={() => navigate(`/all-applications/${row.id}`)}
    //           >
    //             <Tooltip title="View" arrow>
    //               <IconMenuSearch />
    //             </Tooltip>
    //           </Button>
    //         </Box>
    //       </Box>
    //     );
    //   },
    // },
  ];

  useEffect(() => {
    if (allApplications?.applications) {
      const dataSet = allApplications?.applications?.map((application) => {
        return {
          "Application number": application.application_number,
          "Name of Enterprise": application.enterprise_name,
          "Type of Enterprise": getEnterpriceType(
            application.assessment?.enterprice_type
          ),
          "Payment Status":
            application?.payment_status === "Pending" ||
            application?.payment_status === ""
              ? "Yet to pay"
              : application?.payment_status,
          "Application Status": getAppStatus(application.status),
          "Application Date": application?.application_date
            ? moment(application?.application_date).format("D MMM,YYYY")
            : "NA",
          "Assessment Date": application?.assessment?.assessment_date
            ? moment(application?.assessment?.assessment_date).format(
                "D MMM,YYYY"
              )
            : "Yet to schedule",
          Subsidy:
            application?.assessment?.subsidy_approved == 1 ? "Yes" : "No",
          Feedback:
            application?.assessment?.application_stage ===
            APP_STAGE.FEEDBACK_SUBMITTED
              ? "Received"
              : "Not Received",
          // "Assessment no.":
          //   application?.assessment?.assessment?.assessment_number || "N/A",
          // "Application Date": moment(application.last_update_time).format(
          //   "D MMM,YYYY"
          // ),
          id: application.application_uuid,
          assessment: application.assessment,
          status: application.status,
          application_number: application.application_number,
          assessment_stage:
            application.assessment?.assessment?.assessment_stage,
        };
      });
      setRowData(dataSet);
    }
  }, [allApplications]);

  useEffect(() => {
    if (pageNumber) {
      dispatch(
        getAdminAllApplicationsAction({ start: pageNumber, ...payloadState })
      );
    }
  }, [pageNumber, dispatch, payloadState]);

  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const getFilterOptions = () => {
    if (role === ROLES.ADMIN) {
      return [
        {
          filterId: 1,
          displayName: "Type of Enterprise",
          filterType: "Dropdown",
          key: "type_of_enterprise",
          dataArray: typeOfEnterpriseArray,
          labelId: "Type of Enterprise",
          label: "Enterprise Type",
          filterName: "enterpriseType",
        },
        {
          filterId: 2,
          displayName: "Application Status",
          filterType: "Dropdown",
          key: "application_status",
          dataArray: vendorStatus,
          labelId: "Application Status",
          label: "Application Status",
          filterName: "applicationStatus",
        },
        {
          filterId: 3,
          displayName: "Subsidy",
          filterType: "Dropdown",
          key: "subsidy",
          dataArray: yesNo,
          labelId: "Subsidy",
          label: "Subsidy",
          filterName: "subsidy",
        },
        {
          filterId: 4,
          displayName: "Feedback",
          filterType: "Dropdown",
          key: "feedback_received",
          dataArray: yesNo,
          labelId: "Feedback",
          label: "Feedback",
          filterName: "feedback",
        },
        {
          filterId: 5,
          displayStartName: "Application Start Date",
          displayLastName: "Application End Date",
          filterType: "Date",
          keysArray: ["application_start_date", "application_end_date"],
          filterName: "applicationDate",
        },
        {
          filterId: 6,
          displayStartName: "Assessment Start Date",
          displayLastName: "Assessment End Date",
          filterType: "AssessmentDate",
          keysArray: ["assessment_start_date", "assessment_end_date"],
          filterName: "assessmentDate",
        },
      ];
    } else if (role === ROLES.DRDO) {
      return [
        {
          filterId: 1,
          displayName: "Application No.",
          filterType: "Search",
          key: "application_no",
          labelId: "Application No.",
          label: "Application No.",
          filterName: "applicationNumber",
        },
        {
          filterId: 2,
          displayName: "Enterprise Name",
          filterType: "Search",
          key: "enterprise_name",
          labelId: "Enterprise Name",
          label: "Enterprise Name",
          filterName: "enterpriseName",
        },
        {
          filterId: 3,
          displayName: "Type of Enterprise",
          filterType: "Dropdown",
          key: "type_of_enterprise",
          dataArray: typeOfEnterpriseArray,
          labelId: "Type of Enterprise",
          label: "Enterprise Type",
          filterName: "enterpriseType",
        },
        {
          filterId: 4,
          displayName: "Payment Status",
          filterType: "Dropdown",
          key: "payment_status",
          dataArray: paymentStatus,
          labelId: "Payment Status",
          label: "Payment Status",
          filterName: "paymentStatus",
        },
        {
          filterId: 5,
          displayName: "Application Status",
          filterType: "Dropdown",
          key: "application_status",
          dataArray: vendorStatus,
          labelId: "Application Status",
          label: "Application Status",
          filterName: "applicationStatus",
        },
        {
          filterId: 6,
          displayStartName: "Assessment Start Date",
          displayLastName: "Assessment End Date",
          filterType: "AssessmentDate",
          keysArray: ["assessment_start_date", "assessment_end_date"],
          filterName: "assessmentDate",
        },
        {
          filterId: 7,
          displayStartName: "Application Start Date",
          displayLastName: "Application End Date",
          filterType: "Date",
          keysArray: ["application_start_date", "application_end_date"],
          filterName: "applicationDate",
        },
        {
          filterId: 8,
          displayName: "Subsidy",
          filterType: "Dropdown",
          key: "subsidy",
          dataArray: yesNo,
          labelId: "Subsidy",
          label: "Subsidy",
          filterName: "subsidy",
        },
        {
          filterId: 9,
          displayName: "Feedback Received",
          filterType: "Dropdown",
          key: "feedback_received",
          dataArray: yesNo,
          labelId: "Feedback Received",
          label: "Feedback Received",
          filterName: "feedback",
        },
      ];
    }
  };

  const saveHandler = (payload) => {
    dispatch(getAdminAllApplicationsAction({ start: pageNumber, ...payload }));
    setPageNumber(1);
    setPayloadState(payload);
  };

  const exportButtonClick = () => {
    const url = `${process.env.REACT_APP_BASE_URL}/admin/applications?is_excel=1&excel_type=admin_applications`;
    dispatch(downloadAction("AdminApplications", dispatch, url, "doc", "xlsx"));
  };

  return (
    <>
      {/* {allApplications?.total === 0 ? (
        <QpTypography
          displayText="No data available"
          styleData={commonStyles.noData}
        />
      ) : ( */}
      <>
        {/* {openFilterDialog && (
          <FilterComponent
            saveHandler={saveHandler}
            filterOptions={getFilterOptions()}
            alreadySelectedFilters={alreadySelectedFilters}
            setOpenFilterDialog={setOpenFilterDialog}
            setNoOfFiltersApplied={setNoOfFiltersApplied}
            setAlreadySelectedFilters={setAlreadySelectedFilters}
          />
        )} */}
        <CustomTable
          columnDefs={columnDefs}
          rowData={rowData}
          title="All Applications"
          hasPagination
          handlePagination={handlePagination}
          currentPage={pageNumber}
          totalValues={allApplications.total}
          styleData={commonStyles.tableHeight}
          hasFilter
          onClickFilter={() => setOpenFilterDialog(!openFilterDialog)}
          noOfFiltersApplied={noOfFiltersApplied}
          buttonsDivStyle={commonStyles.divFlexStyle}
          exportButtonClick={exportButtonClick}
          hasExport={true}
          saveHandler={saveHandler}
          filterOptions={getFilterOptions()}
          alreadySelectedFilters={alreadySelectedFilters}
          setOpenFilterDialog={setOpenFilterDialog}
          setNoOfFiltersApplied={setNoOfFiltersApplied}
          setAlreadySelectedFilters={setAlreadySelectedFilters}
        />
      </>
      {/* )} */}
    </>
  );
};

export default AllApplications;
