import React from "react";
import { Box } from "@mui/material";
import {
  commonStyles,
  customCommonStyles,
} from "../../../../Styles/CommonStyles";
import * as _ from "lodash";
import CustomApplicatonView from "../../../../Components/CustomApplicationView/CustomApplicationView";

export default function AdminApplicationView({ applicationDetail }) {
  return (
    <>
      <Box
        sx={{
          ...customCommonStyles.fieldContainer,
          ...commonStyles.applicationViewContainer,
        }}
      >
        <CustomApplicatonView applicationDetail={applicationDetail} />
      </Box>
    </>
  );
}
