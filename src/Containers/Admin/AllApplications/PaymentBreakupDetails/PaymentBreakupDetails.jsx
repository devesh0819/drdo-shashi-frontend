import React, { useEffect, useState } from "react";
import PaymentDetails from "../../../Applicant/PaymentHistory/PaymentDetails";

export default function PaymentBreakupDetails({
  applicationDetail,
  data,
  setData,
  tdsFeeTextRequired,
  taxValue,
  setTaxValue,
  tdsApplicable,
}) {
  const [paymentData, setPaymentData] = useState();

  useEffect(() => {
    if (applicationDetail?.payment_order?.payment_breakup) {
      setPaymentData(
        JSON.parse(applicationDetail?.payment_order?.payment_breakup)
      );
    }
  }, [applicationDetail?.payment_order?.payment_breakup]);

  return (
    <>
      {applicationDetail && applicationDetail?.enterprice_type === "SM" && (
        <PaymentDetails
          paymentData={paymentData}
          applicationDetail={applicationDetail}
          orderNumber={applicationDetail?.order?.order_number}
          certificationFeeText={paymentData?.small_certification_fee_text}
          certificationFeeValue={Number(
            paymentData?.small_certification_fee_value
          )}
          assessorFeeText={paymentData?.small_assessor_fee_text}
          assessorFeeValue={
            applicationDetail?.payment_order?.assessor_fee
              ? Number(applicationDetail?.payment_order?.assessor_fee)
              : 0
          }
          gstFeeText={paymentData?.small_gst_text}
          gstFeeValue={paymentData?.small_gst_value}
          tdsFeeText={paymentData?.small_tds_text}
          tdsFeeValue={paymentData?.small_tds_value}
          isSubsidized={applicationDetail?.subsidy_approved}
          // subsidiyPercent={paymentData?.small_subsidy_value}
          subsidiyPercent={applicationDetail?.payment_order?.discount}
          subsidiyFeeText={paymentData?.small_subsidy_text}
          data={data}
          setData={setData}
          tdsFeeTextRequired={tdsFeeTextRequired}
          taxValue={taxValue}
          setTaxValue={setTaxValue}
          taxPercent={
            tdsApplicable
              ? applicationDetail?.payment_order?.tax_percent || 10
              : 0
          }
        />
      )}
      {applicationDetail && applicationDetail?.enterprice_type === "MIC" && (
        <PaymentDetails
          paymentData={paymentData}
          applicationDetail={applicationDetail}
          orderNumber={applicationDetail?.order?.order_number}
          certificationFeeText={paymentData?.micro_certification_fee_text}
          certificationFeeValue={Number(
            paymentData?.micro_certification_fee_value
          )}
          assessorFeeText={paymentData?.micro_assessor_fee_text}
          assessorFeeValue={
            applicationDetail?.payment_order?.assessor_fee
              ? Number(applicationDetail?.payment_order?.assessor_fee)
              : 0
          }
          gstFeeText={paymentData?.micro_gst_text}
          gstFeeValue={paymentData?.micro_gst_value}
          tdsFeeText={paymentData?.micro_tds_text}
          tdsFeeValue={paymentData?.micro_tds_value}
          isSubsidized={applicationDetail?.subsidy_approved}
          // subsidiyPercent={paymentData?.micro_subsidy_value}
          subsidiyPercent={applicationDetail?.payment_order?.discount}
          subsidiyFeeText={paymentData?.micro_subsidy_text}
          data={data}
          setData={setData}
          tdsFeeTextRequired={tdsFeeTextRequired}
          taxValue={taxValue}
          setTaxValue={setTaxValue}
          taxPercent={
            tdsApplicable
              ? applicationDetail?.payment_order?.tax_percent || 10
              : 0
          }
        />
      )}
      {applicationDetail && applicationDetail?.enterprice_type === "MED" && (
        <PaymentDetails
          paymentData={paymentData}
          applicationDetail={applicationDetail}
          orderNumber={applicationDetail?.order?.order_number}
          certificationFeeText={paymentData?.medium_certification_fee_text}
          certificationFeeValue={Number(
            paymentData?.medium_certification_fee_value
          )}
          assessorFeeText={paymentData?.medium_assessor_fee_text}
          assessorFeeValue={
            applicationDetail?.payment_order?.assessor_fee
              ? Number(applicationDetail?.payment_order?.assessor_fee)
              : 0
          }
          gstFeeText={paymentData?.medium_gst_text}
          gstFeeValue={paymentData?.medium_gst_value}
          tdsFeeText={paymentData?.medium_tds_text}
          tdsFeeValue={paymentData?.medium_tds_value}
          // isSubsidized={applicationDetail?.subsidy_approved}
          // isSubsidized={0}
          // subsidiyPercent={paymentData?.medium_subsidy_value}
          subsidiyPercent={applicationDetail?.payment_order?.discount}
          subsidiyFeeText={paymentData?.medium_subsidy_text}
          data={data}
          setData={setData}
          tdsFeeTextRequired={tdsFeeTextRequired}
          taxValue={taxValue}
          setTaxValue={setTaxValue}
          taxPercent={
            tdsApplicable
              ? applicationDetail?.payment_order?.tax_percent || 10
              : 0
          }
        />
      )}
      {applicationDetail && applicationDetail?.enterprice_type === "L" && (
        <PaymentDetails
          paymentData={paymentData}
          applicationDetail={applicationDetail}
          orderNumber={applicationDetail?.order?.order_number}
          certificationFeeText={paymentData?.large_certification_fee_text}
          certificationFeeValue={Number(
            paymentData?.large_certification_fee_value
          )}
          assessorFeeText={paymentData?.large_assessor_fee_text}
          assessorFeeValue={
            applicationDetail?.payment_order?.assessor_fee
              ? Number(applicationDetail?.payment_order?.assessor_fee)
              : 0
          }
          gstFeeText={paymentData?.large_gst_text}
          gstFeeValue={paymentData?.large_gst_value}
          tdsFeeText={paymentData?.large_tds_text}
          tdsFeeValue={paymentData?.large_tds_value}
          // isSubsidized={applicationDetail?.subsidy_approved}
          // isSubsidized={0}
          // subsidiyPercent={paymentData?.large_subsidy_value}
          subsidiyPercent={applicationDetail?.payment_order?.discount}
          subsidiyFeeText={paymentData?.large_subsidy_text}
          data={data}
          setData={setData}
          tdsFeeTextRequired={tdsFeeTextRequired}
          taxValue={taxValue}
          setTaxValue={setTaxValue}
          taxPercent={
            tdsApplicable
              ? applicationDetail?.payment_order?.tax_percent || 10
              : 0
          }
        />
      )}
    </>
  );
}
