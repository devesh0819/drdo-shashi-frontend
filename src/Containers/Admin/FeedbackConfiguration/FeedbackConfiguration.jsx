import React, { useEffect, useState } from "react";
import { Box, Grid, InputBase, Typography } from "@mui/material";
import { commonStyles } from "../../../Styles/CommonStyles";
import { getUserData } from "../../../Services/localStorageService";
import { ROLES } from "../../../Constant/RoleConstant";
import QpButton from "../../../Components/QpButton/QpButton";
import { styles } from "./FeedbackConfigurationStyles.js";
import { useDispatch, useSelector } from "react-redux";
import {
  addFeedbackConfigurationAction,
  deleteFeedbackConfigurationAction,
  getFeedbackConfigurationAction,
  getFeedbackConfigurationData,
} from "../../../Redux/Admin/FeedbackConfiguration/feedbackConfigurationActions";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import QpInputBase from "../../../Components/QpInputBase/QpInputBase";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { WITHOUT_SPECIAL_CHARACTERS } from "../../../Constant/AppConstant";
import DeleteIcon from "@mui/icons-material/Delete";
import CustomRating from "../../../Components/CustomRating/CustomRating";
import QpDialog from "../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../Components/QpConfirmModal/QpConfirmModal";

export const feedbackConfigSchema = Yup.object().shape({
  questionText: Yup.string()
    .required(`This field is required`)
    .matches(WITHOUT_SPECIAL_CHARACTERS, "This field is invalid")
    .min(5, "This field should be 5 to 55 characters long")
    .max(55, "This field should be 5 to 55 characters long"),
});

export default function FeedbackConfiguration() {
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(feedbackConfigSchema),
    mode: "onChange",
  });
  const userData = getUserData();
  const role = userData?.roles[0];
  const [addQuestionDialog, setAddQuestionDialog] = useState(false);
  const [deleteQuestionDialog, setDeleteQuestionDialog] = useState(false);
  const [quesId, setQuesId] = useState();

  const dispatch = useDispatch();
  const feedbackConfigurationData = useSelector(
    (state) => state.feedbackConfiguration.feedbackConfigurationData
  );
  useEffect(() => {
    dispatch(getFeedbackConfigurationAction());
  }, []);

  const onSubmit = (data) => {
    dispatch(addFeedbackConfigurationAction({ title: data?.questionText }));
    setValue("questionText", "", { shouldValidate: false });
  };
  return (
    <>
      <Box sx={commonStyles.headerOuterContainer}>
        <Box sx={commonStyles.topBackButtonHeader}>
          {/* <Button disableRipple sx={commonStyles.backButton}>
            <IconCircularBack />
          </Button> */}
          <Typography style={commonStyles.titleBackText}>
            Feedback Configuration
          </Typography>
        </Box>
      </Box>
      <Box sx={{ paddingX: "2.063rem", paddingTop: "1rem" }}>
        <Box
          sx={{
            ...commonStyles.withNumberContainer,
            ...commonStyles.feedbackSurveyDiv,
          }}
        >
          <Box sx={commonStyles.feedbackBlankDiv}></Box>
          <Box sx={commonStyles.numbers}>
            <QpTypography displayText={1} />
            <QpTypography displayText={2} />
            <QpTypography displayText={3} />
            <QpTypography displayText={4} />
          </Box>
        </Box>
        {feedbackConfigurationData?.map((ques, index) => {
          return (
            <Box sx={styles.questionRating} key={index}>
              <QpTypography
                displayText={`${index + 1}. ${ques?.title}`}
                styleData={styles.questionContainer}
              />
              <CustomRating
                // defaultValue=""
                readOnly={true}
                // onChange={(event) => handleFeedbackChange(event, index)}
              />
              {role === ROLES.ADMIN && (
                <Box sx={styles.deleteBox}>
                  <DeleteIcon
                    onClick={() => {
                      setDeleteQuestionDialog(true);
                      setQuesId(ques?.question_uuid);
                    }}
                  />
                </Box>
              )}
            </Box>
          );
        })}
      </Box>
      <Box sx={{ paddingX: "2.063rem", paddingTop: "1rem" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "self-start",
            marginTop: "0.875rem",
          }}
        >
          <QpTypography
            displayText={`Feedback Question-${
              feedbackConfigurationData?.length + 1
            }`}
            styleData={{
              fontWeight: 600,
              minWidth: "21rem",
              // maxWidth: "20rem",
            }}
          />
          <Box sx={{ minWidth: "18rem", maxWidth: "18rem" }}>
            <InputBase
              sx={{
                ...styles.inputBaseStyle,
                // ...commonStyles.fullWidth,
              }}
              placeholder="Enter feedback question"
              size="small"
              disabled={role === ROLES.DRDO ? true : false}
              {...register("questionText")}
              // value={selectedFilters?.[activeFilter?.key] || ""}
              // onChange={handleInputChange}
            />
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={errors.questionText?.message}
            />
          </Box>
          {role === ROLES.ADMIN && (
            <QpButton
              displayText="Add"
              styleData={styles.updateButton}
              textStyle={styles.updateButtonText}
              onClick={() => setAddQuestionDialog(true)}
            />
          )}
        </Box>
      </Box>
      <QpDialog
        open={deleteQuestionDialog}
        closeModal={setDeleteQuestionDialog}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to delete this question?"
          closeModal={setDeleteQuestionDialog}
          onConfirm={() => dispatch(deleteFeedbackConfigurationAction(quesId))}
        />
      </QpDialog>
      <QpDialog
        open={addQuestionDialog}
        closeModal={setAddQuestionDialog}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to add this question?"
          closeModal={setAddQuestionDialog}
          onConfirm={handleSubmit(onSubmit)}
        />
      </QpDialog>
    </>
  );
}
