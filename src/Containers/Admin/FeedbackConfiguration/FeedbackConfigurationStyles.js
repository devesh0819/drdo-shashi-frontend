import { DARKER_BLUE, WHITE } from "../../../Constant/ColorConstant";

export const styles = {
  updateButton: {
    backgroundColor: DARKER_BLUE,
    // width: "5rem",
    height: "2rem",
    marginLeft: "0.5rem",
    "&:hover": {
      backgroundColor: DARKER_BLUE,
    },
    "@media(max-width:600px)": {
      width: "5.1rem",
      height: "1.613rem",
    },
  },
  questionRating: {
    display: "flex",
    alignItems: "center",
    marginBottom: "1rem",
  },
  questionContainer: {
    width: "20rem",
    fontSize: "0.875rem",
  },
  deleteButtonStyle: {
    background: "#D8DCE0",
    borderRadius: "5px",
    textTransform: "none",
    // width: "11rem",
    height: "2rem",
    "&:hover": {
      background: "#D8DCE0",
    },
    marginRight: "1rem",
  },
  deleteButtonText: {
    fontWeight: 500,
    fontSize: "0.875rem",
  },
  deleteBox: {
    cursor: "pointer",
    marginLeft: "2rem",
  },
  updateButtonText: {
    color: WHITE,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1rem",
    textTransform: "none",
    "@media(max-width:600px)": {
      fontWeight: 600,
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  inputBaseStyle: {
    // minWidth: "10rem",
    // maxWidth: "10rem",
    // width: "50%",
    width: "18rem",
    border: "0.063rem solid #A9A9A9",
    paddingX: "0.5rem",
  },
};
