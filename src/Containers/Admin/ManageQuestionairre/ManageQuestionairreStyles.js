import { WHITE } from "../../../Constant/ColorConstant";

export const styles = {
  iconBtnWidth: {
    width: "120px",
  },
  questionairreHeaderStyle: {
    display: "flex",
    justifyContent: "space-between",
    borderBottom: "0.063rem solid #EAEAEA",
    alignItems: "center",
    paddingLeft: "2.063rem",
    paddingTop: "1.25rem",
    paddingBottom: "1.25rem",
    paddingRight: "2.063rem",
    backgroundColor: WHITE,
  },
  questionairreBtnStyle: {
    // width: "12rem",
    fontSize: "0.9rem",
    textTransform: "capitalize",
  },
  buttonsDivStyle: {
    display: "flex",
    width: "55%",
    justifyContent: "end",
  },
  BtnStylingInSecondaryBtncase: {
    margin: "0 0 0 1%",
  },
};
