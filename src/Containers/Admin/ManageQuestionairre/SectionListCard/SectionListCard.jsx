import React, { useEffect, useState } from "react";
import { Box } from "@mui/material";
import { styles } from "../QuestionListCard/QuestionListCardStyles";
import { commonStyles } from "../../../../Styles/CommonStyles.js";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import { ReactComponent as PlusAgency } from "../../../../Assets/Images/plusAgency.svg";
import { ReactComponent as MinusAgency } from "../../../../Assets/Images/minusAgency.svg";
import { ReactComponent as IconPencil } from "../../../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconDelete } from "../../../../Assets/Images/deleteIcon.svg";
import { useNavigate } from "react-router-dom";
import { LevelInputComponent } from "../LevelInputComponent/LevelInputComponent";
import { getUserData } from "../../../../Services/localStorageService";
import { ROLES } from "../../../../Constant/RoleConstant";
import { OptionsRadioComponent } from "../../../../Components/OptionsRadioComponent/OptionsRadioComponent";

export default function SectionListCard({
  parameterData,
  keyIndex,
  questionUuid,
  questionairre_id,
  section,
  category,
  sectionIndex,
  currentActive,
  setCurrentActive,
  section_id,
  showContainerFromProp,
  changeParameterOption,
  isQuestionairre,
}) {
  const [showContainer, setShowContainer] = useState(false);
  const [sectionData, setSectionData] = useState({});
  const navigate = useNavigate();

  const userData = getUserData();
  const role = userData?.roles[0];
  useEffect(() => {
    // dispatch(getAgencyListAction());
    setSectionData(section);
  }, []);

  useEffect(() => {
    setSectionData(section);
  }, [parameterData]);

  const handleCancelButtonClick = () => {
    setShowContainer(false);
  };

  const handleRadioChange = (type, data, section_id) => {
    changeParameterOption(type, data, section_id);
  };

  return (
    <>
      <Box
        sx={{
          ...styles.cardMainContainer,
        }}
      >
        <Box>
          <Box sx={commonStyles.displayStyle}>
            <Box
              sx={styles.imageTitleContainer}
              // onClick={() => setShowContainer(!showContainer)}
              onClick={() => {
                if (currentActive == section_id) setCurrentActive(0);
                else setCurrentActive(section_id);
              }}
            >
              {/* {!showContainer ? ( */}
              {!showContainerFromProp ? (
                <PlusAgency style={styles.imageSize} />
              ) : (
                <MinusAgency style={styles.imageSize} />
              )}

              <QpTypography
                displayText={`Section- ${sectionIndex + 1}`}
                styleData={styles.titleText}
              />
            </Box>
          </Box>
          {/* {showContainer && ( */}
          {showContainerFromProp && (
            <>
              {(role === ROLES.ADMIN ||
                role === ROLES.DRDO ||
                role === ROLES.ASSESSOR) &&
              !isQuestionairre ? (
                <OptionsRadioComponent
                  parameterData={parameterData}
                  section={sectionData}
                  category={category}
                  closeContainer={handleCancelButtonClick}
                  inputDisabled={true}
                  handleOptionChange={(type, data, section_id) =>
                    handleRadioChange(type, data, section_id)
                  }
                  isAssessor={
                    role === ROLES.ASSESSOR || role === ROLES.DRDO
                      ? true
                      : false
                  }
                />
              ) : (
                <LevelInputComponent
                  parameterData={parameterData}
                  section={sectionData}
                  category={category}
                  closeContainer={handleCancelButtonClick}
                  inputDisabled={true}
                />
              )}
            </>
          )}
        </Box>
      </Box>
    </>
  );
}
