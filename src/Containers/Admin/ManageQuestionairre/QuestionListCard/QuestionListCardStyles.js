import {
    BACKGROUND_LIGHT_BLUE,
    BLACKISH,
    DEFAULT_GREY,
    LIGHT_BLUE,
  } from "../../../../Constant/ColorConstant";
  
  export const styles = {
    imageSize: {
      width: "23px",
      height: "23px",
    },
    imageTitleContainer: {
      display: "flex",
      cursor: "pointer",
      alignItems: "center",
    },
    cardMainContainer: {
      border: "1px solid #60C5F9",
      background: "#F8FCFF",
      padding: "1rem",
      margin: "1rem",
    },
    titleText: {
      marginLeft: "0.75rem",
      fontWeight: 600,
      fontSize: "1rem",
      color: BLACKISH,
    },
    iconsContainer: {
      width: "5rem",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
    },
    smallText: {
      fontWeight: 400,
      fontSize: "0.875rem",
      color: BLACKISH,
    },
    smallTextStyle: {
      marginLeft: "2.188rem",
      textOverflow: "ellipsis",
      whiteSpace: "nowrap",
      overflow: "hidden",
      width: "5rem",
    },
    dividerStyle: {
      fontSize: "0.625rem",
      color: DEFAULT_GREY,
      marginX: "0.4rem",
    },
    addressContainer: {
      display: "flex",
      alignItems: "center",
    },
    addAssessor: {
      border: "1px solid #1FBAED",
      borderRadius: "100px",
      background: "white",
      color: "#1FBAED",
      fontWeight: 600,
      marginTop: "0.85rem",
      "&:hover": {
        background: "white",
      },
    },
    smallTextMargin:{
        marginLeft: "0.80rem",
    },
    agencyInput: {
      height: "2rem",
    },
    agencyLabel: {
      fontSize: "0.75rem",
      marginTop:"0.7rem"
    },
    addAgency: {
      background: "#1FBAED",
      boxShadow: "0px 4px 4px rgba(102, 104, 105, 0.18)",
      borderRadius: "100px",
      color: "white",
      "&:hover": {
        background: "#1FBAED",
      },
    },
    wordBreak:{
      wordWrap: "break-word",
        wordBreak: "break-all",
        fontSize: "0.9rem",
        minHeight: "5rem",
    },
    marginTop:{
      marginTop:"0.7rem"
    },
    parameterListCardStyle:{
      button:{
        display:"inline-block",
        marginLeft:'1%',
        fontSize:"0.7rem",
        padding:"2px 4px "
      },
      flexContainer:{
        display:"flex",
        justifyContent:"space-between",
        alignItems: "center",
      },
      inlineText:{
        display:"inline-block"
      }
    }
  };
  