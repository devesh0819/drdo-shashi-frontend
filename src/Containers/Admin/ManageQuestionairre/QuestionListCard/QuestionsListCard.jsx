import React, { useEffect, useState } from "react";
import { Box, Tooltip } from "@mui/material";
import { styles } from "./QuestionListCardStyles.js";
import { commonStyles } from "../../../../Styles/CommonStyles.js";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import { ReactComponent as PlusAgency } from "../../../../Assets/Images/plusAgency.svg";
import { ReactComponent as MinusAgency } from "../../../../Assets/Images/minusAgency.svg";
import { ReactComponent as IconPencil } from "../../../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconDelete } from "../../../../Assets/Images/deleteIcon.svg";
import { ReactComponent as IconView } from "../../../../Assets/Images/iconApplication.svg";
import { useNavigate } from "react-router-dom";

export default function QuestionListCard({
  parameter,
  keyIndex,
  questionUuid,
  questionairre_id,
  discipline,
}) {
  const [showContainer, setShowContainer] = useState(false);
  const navigate = useNavigate();
  useEffect(() => {}, []);

  return (
    <>
      <Box
        sx={{
          ...styles.cardMainContainer,
        }}
      >
        <Box>
          <Box sx={commonStyles.displayStyle}>
            <Box
              sx={styles.imageTitleContainer}
              // onClick={() => setShowContainer(!showContainer)}
              onClick={() => {}}
            >
              {/* {!showContainer ? (
                      <PlusAgency style={styles.imageSize} />
                    
                      ) : (
                      <MinusAgency style={styles.imageSize} />
                    )} */}
              {/* <PlusAgency style={styles.imageSize} /> */}
              <QpTypography
                displayText={parameter?.title}
                styleData={styles.titleText}
              />
            </Box>
            <Box sx={styles.iconsContainer}>
              <Box
                sx={{
                  ...commonStyles.cursorPointer,
                }}
                onClick={() => {
                  navigate(`question-edit/${parameter.id}`);
                }}
              >
                <Tooltip title="View" arrow>
                  {/* <IconPencil /> */}
                  <IconView />
                </Tooltip>
              </Box>
            </Box>
          </Box>
          <Box sx={styles.addressContainer}>
            <QpTypography
              displayText={`Discipline: ${discipline.title}`}
              styleData={{ ...styles.smallText, ...styles.smallTextMargin }}
            />
            <QpTypography displayText="|" styleData={styles.dividerStyle} />
            <QpTypography
              displayText={`Planning: ${parameter?.section_p_count || "0"}`}
              styleData={{ ...styles.smallText, ...styles.smallTextMargin }}
            />
            <QpTypography displayText="|" styleData={styles.dividerStyle} />
            <QpTypography
              displayText={`Deployment: ${parameter?.section_d_count || "0"}`}
              styleData={{ ...styles.smallText, ...styles.smallTextMargin }}
            />
            <QpTypography displayText="|" styleData={styles.dividerStyle} />
            <QpTypography
              displayText={`Monitoring: ${parameter?.section_m_count || "0"}`}
              styleData={{ ...styles.smallText, ...styles.smallTextMargin }}
            />
          </Box>
        </Box>
      </Box>
    </>
  );
}
