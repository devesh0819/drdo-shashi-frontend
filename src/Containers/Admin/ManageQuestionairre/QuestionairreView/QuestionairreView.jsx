import React, { useEffect, useState } from "react";
import { Box, Typography, Button } from "@mui/material";
import {
  commonStyles,
  customCommonStyles,
} from "../../../../Styles/CommonStyles";
import QpButton from "../../../../Components/QpButton/QpButton";
import { useDispatch, useSelector } from "react-redux";
import {
  getQuestionairreDetailAction,
  getQuestionairreChangesAction,
  sendForApprovalAction,
} from "../../../../Redux/Admin/ManageQuestionairre/manageQuestionairreActions";
import * as _ from "lodash";
import QuestionListCard from "../QuestionListCard/QuestionsListCard";
import { useParams, useNavigate } from "react-router-dom";
import { ReactComponent as IconCircularBack } from "../../../../Assets/Images/iconCircularBack.svg";
import questionairreViewStyles from "./QuestionairreViewStyles";
import { ROLES } from "../../../../Constant/RoleConstant";
import { getUserData } from "../../../../Services/localStorageService";
import QpDialog from "../../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../../Components/QpConfirmModal/QpConfirmModal";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import {
  getDrdoQuestionairreDetailAction,
  getDrdoQuestionairreChangesAction,
  drdoApproveQuestionairreAction,
} from "../../../../Redux/DrdoAdmin/DrdoQuestionairre/drdoQuestionairreActions";
import {
  DRAFT,
  PENDING_APPROVAL,
  APPROVED,
  REJECTED,
} from "../../../../Constant/AppConstant";

const QuestionairreView = () => {
  const [showModal, setShowModal] = useState(false);
  const [sendApproval, selectSendApproval] = useState(false);
  const [approve, selectApprove] = useState(false);
  const [reject, selectReject] = useState(false);

  const userData = getUserData();
  const role = userData?.roles[0];
  const dispatch = useDispatch();
  const questionairre = useSelector((state) => {
    if (role == ROLES.ADMIN) return state.questionairres.questionairreDetail;
    if (role == ROLES.DRDO)
      return state.drdoQuestionairres.drdoQuestionairreDetail;
  });
  const questinairreChanges = useSelector((state) => {
    if (role == ROLES.ADMIN)
      return state.questionairres.questionairreChangesDetail;
    if (role == ROLES.DRDO)
      return state.drdoQuestionairres.drdoQuestionairreChangesDetail;
  });

  const { id } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    if (id) {
      if (role == ROLES.ADMIN) {
        dispatch(getQuestionairreDetailAction(id));
        dispatch(getQuestionairreChangesAction(id));
      }
      if (role == ROLES.DRDO) {
        dispatch(getDrdoQuestionairreDetailAction(id));
        dispatch(getDrdoQuestionairreChangesAction(id));
      }
    }
  }, [id]);

  const clickSendForApproval = () => {
    selectSendApproval(true);
    setShowModal(true);
  };

  const clickDrdoApprove = () => {
    selectApprove(true);
    setShowModal(true);
  };

  const clickDrdoReject = () => {
    selectReject(true);
    setShowModal(true);
  };

  const handleSendForApproval = () => {
    // api-pending
    dispatch(sendForApprovalAction(id));
  };

  const handleDrdoApprove = () => {
    let payload = {
      is_approved: 1,
    };
    dispatch(drdoApproveQuestionairreAction(id, payload));
  };

  const handleDrdoReject = () => {
    let payload = {
      is_approved: 0,
    };

    dispatch(drdoApproveQuestionairreAction(id, payload));
  };
  const handleCloseModal = () => {
    selectSendApproval(false);
    selectApprove(false);
    selectReject(false);
    setShowModal(false);
  };

  const handleConfirm = () => {
    if (role == ROLES.DRDO) {
      if (approve) handleDrdoApprove();
      if (reject) handleDrdoReject();
    }

    if (role == ROLES.ADMIN) {
      if (sendApproval) handleSendForApproval();
    }
  };

  return (
    <>
      <Box sx={customCommonStyles.outerContainer}>
        <Box>
          <Box
            style={{
              ...commonStyles.topHeader,
              ...questionairreViewStyles.headerMainStyle,
            }}
            // style={{ ...commonStyles.topHeader, ...commonStyles.displayStyle }}
          >
            <Box sx={questionairreViewStyles.headerBackTitleBox}>
              <Button
                disableRipple
                sx={commonStyles.backButton}
                // sx={{
                //   minWidth: "unset",
                //   padding: 0,
                //   paddingRight: "0.563rem",
                //   "&:hover": { backgroundColor: "transparent" },
                //   "@media(max-width:600px)": {
                //     "&.backButton svg": {
                //       height: "1rem",
                //       width: "1rem",
                //     },
                //   },
                // }}
                onClick={() => navigate("/manage-questionairre")}
                className="backButton"
              >
                <IconCircularBack />
              </Button>
              <Typography style={commonStyles.titleText}>
                Paramters List
              </Typography>
            </Box>
            <Box sx={questionairreViewStyles.headerButtonsBox}>
              {role == ROLES.ADMIN && questionairre?.state == DRAFT ? (
                <QpButton
                  displayText="Send For Approval"
                  onClick={clickSendForApproval}
                  styleData={{
                    ...questionairreViewStyles.headerButtons,
                    ...questionairreViewStyles.approvalButton,
                    ...questionairreViewStyles.sendforapprovalButton,
                  }}
                  textStyle={{ ...questionairreViewStyles.textStyle }}
                />
              ) : (
                ""
              )}
              {role == ROLES.DRDO &&
              questionairre?.state == PENDING_APPROVAL ? (
                <QpButton
                  displayText="Approve"
                  styleData={{
                    ...questionairreViewStyles.headerButtons,
                    ...questionairreViewStyles.approvalButton,
                  }}
                  onClick={clickDrdoApprove}
                  textStyle={{ ...questionairreViewStyles.textStyle }}
                />
              ) : (
                ""
              )}
              {role === ROLES.DRDO &&
              questionairre?.state == PENDING_APPROVAL ? (
                <QpButton
                  displayText="Reject"
                  styleData={{
                    ...questionairreViewStyles.headerButtons,
                    ...questionairreViewStyles.rejectButton,
                  }}
                  onClick={clickDrdoReject}
                  textStyle={{ ...questionairreViewStyles.textStyle }}
                />
              ) : (
                ""
              )}
            </Box>
          </Box>
          <Box sx={commonStyles.parameterContainerHeight}>
            <Typography
              style={{
                ...commonStyles.titleText,
                ...questionairreViewStyles.changesLabel,
              }}
            >
              Recent Changes
            </Typography>
            {questinairreChanges?.length ? (
              <Box
                sx={{
                  ...questionairreViewStyles.changesMainContainer,
                  ...questionairreViewStyles.customScrollBarHidden,
                }}
              >
                {questinairreChanges?.map((changes) => {
                  return (
                    <Box
                      sx={questionairreViewStyles.changesCards}
                      key={changes}
                    >
                      <QpTypography
                        displayText={`Description:`}
                        styleData={{
                          ...questionairreViewStyles.smallText,
                          ...questionairreViewStyles.smallTextMargin,
                          ...questionairreViewStyles.generalLabel,
                        }}
                      />
                      <QpTypography
                        displayText={`${changes?.description}`}
                        styleData={{
                          ...questionairreViewStyles.smallText,
                          ...questionairreViewStyles.smallTextMargin,
                        }}
                      />
                      {changes?.old_value && (
                        <QpTypography
                          displayText={`Old Value:`}
                          styleData={{
                            ...questionairreViewStyles.smallText,
                            ...questionairreViewStyles.smallTextMargin,
                            ...questionairreViewStyles.generalLabel,
                            ...questionairreViewStyles.oldLabel,
                          }}
                        />
                      )}
                      {changes?.old_value && (
                        <QpTypography
                          displayText={`${changes?.old_value}`}
                          styleData={{
                            ...questionairreViewStyles.smallText,
                            ...questionairreViewStyles.smallTextMargin,
                          }}
                        />
                      )}
                      {changes?.new_value && (
                        <QpTypography
                          displayText={`New Value:`}
                          styleData={{
                            ...questionairreViewStyles.smallText,
                            ...questionairreViewStyles.smallTextMargin,
                            ...questionairreViewStyles.generalLabel,
                            ...questionairreViewStyles.newLabel,
                          }}
                        />
                      )}
                      {changes?.new_value && (
                        <QpTypography
                          displayText={`${changes?.new_value}`}
                          styleData={{
                            ...questionairreViewStyles.smallText,
                            ...questionairreViewStyles.smallTextMargin,
                          }}
                        />
                      )}
                    </Box>
                  );
                })}
              </Box>
            ) : (
              <QpTypography
                displayText={`No recent changes`}
                styleData={{
                  ...questionairreViewStyles.smallText,
                  ...questionairreViewStyles.smallTextMargin,
                  marginLeft: "1rem",
                }}
              />
            )}
            {questionairre?.disciplines?.length ? (
              questionairre?.disciplines?.map((d) => {
                return d.parameters.map((p, p_index) => {
                  return (
                    <QuestionListCard
                      parameter={p}
                      keyIndex={p_index}
                      //questoinUuid={question?.uuid}
                      questionairre_id={id}
                      discipline={d}
                    />
                  );
                });
              })
            ) : (
              <p>"No Data for disciplines and parameters"</p>
            )}
          </Box>
        </Box>
      </Box>
      <QpDialog
        open={showModal}
        closeModal={() => handleCloseModal()}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to perform this Action?"
          closeModal={() => handleCloseModal()}
          onConfirm={() => handleConfirm()}
        />
      </QpDialog>
    </>
  );
};

export default QuestionairreView;
