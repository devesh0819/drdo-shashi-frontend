const questionairreViewStyles = {
  headerMainStyle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  headerBackTitleBox: {
    display: "flex",
    justifyContent: "start",
    alignItems: "center",
  },
  headerButtonsBox: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  sendForApprovalButton: {},
  headerButtons: {
    background: "#1FBAED",
    boxShadow: "0px 4px 4px rgba(102, 104, 105, 0.18)",
    borderRadius: "4px",
    color: "white",
    "&:hover": {
      background: "#1FBAED",
    },
  },
  sendforapprovalButton: {
    minWidth: "210px",
  },
  approvalButton: {
    margin: "0 2%",
  },

  rejectButton: {
    background: "#AF0000",
    "&:hover": {
      background: "#C31414",
    },
  },

  textStyle: {
    fontWeight: 700,
    fontSize: "1rem",
  },

  changesMainContainer: {
    height: "400px",
    overflow: "auto",
    // position:"relative",
    marginLeft: "1%",
    margin: "1rem",
  },
  changesLabel: {
    height: "30px",
    margin: "1rem",
    // border: "1px solid #60C5F9",
    //   background: "#F8FCFF",
    //   padding: "1rem",
  },
  changesContainer: {
    // overflow:"scroll",
  },
  changesCards: {
    minHeight: "100px",
    // display:"flex",
    alignItems: "center",
    // justifyContent:"start",
    wordWrap: "break-word",
    //...
    padding: "1%",
    // display:"flex",
    // flexDirection:'column',
    border: "0.5px solid #D3D3D3 ",
    borderRadius: "4px",
    margin: "1% 0",
    // boxShadow:
    boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
    width: "97.5%",
  },
  smallText: {
    fontWeight: 400,
    fontSize: "0.875rem",
  },
  smallTextMargin: {
    marginLeft: "0.80rem",
  },

  generalLabel: {
    fontWeight: "600",
  },
  descriptionLabel: {
    color: "black",
  },
  oldLabel: {
    color: "red",
  },
  newLabel: {
    color: "green",
  },
  customScrollBarHidden: {
    "&::-webkit-scrollbar": {
      width: "0.3rem",
      height: "0.3rem",
    },
    "&::-webkit-scrollbar-track": {
      //   boxShadow: "inset 0 0 0.375rem rgba(0,0,0,0.00)",
      //   webkitBoxShadow: "inset 0 0 0.375rem rgba(0,0,0,0.00)",
      width: "0.438rem",
      borderRadius: "0.75rem",
      // background: SILVER_GREY,
    },
    "&::-webkit-scrollbar-thumb": {
      //   backgroundColor: "rgba(0,0,0,.1)",
      backgroundColor: "transparent",
      borderRadius: "0.75rem",
      //   background: METALLIC_SILVER,
    },
  },
};

export default questionairreViewStyles;
