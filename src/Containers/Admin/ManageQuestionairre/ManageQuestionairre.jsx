import React, { useEffect, useState } from "react";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import { ReactComponent as IconCircularDownload } from "../../../Assets/Images/iconCircularDownload.svg";
import { ReactComponent as IconMenuSearch } from "../../../Assets/Images/iconMenuSearch.svg";
import { ReactComponent as IconApplicationView } from "../../../Assets/Images/iconApplication.svg";
import { Button, Box, Tooltip, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import * as moment from "moment";
import { useNavigate } from "react-router-dom";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { commonStyles } from "../../../Styles/CommonStyles";

import QpDialog from "../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../Components/QpConfirmModal/QpConfirmModal";
import { getQuestionairreListAction } from "../../../Redux/Admin/ManageQuestionairre/manageQuestionairreActions";
import QpImportQuestionModal from "../../../Components/QpImportQuestionModal/QpImportQuestionModal";
import * as colors from "../../../Constant/ColorConstant";
import { getUserData } from "../../../Services/localStorageService";
import { ROLES } from "../../../Constant/RoleConstant";
import { getDrdoQuestionairreListAction } from "../../../Redux/DrdoAdmin/DrdoQuestionairre/drdoQuestionairreActions";

import {
  checkDrdoQuestionairreStatus,
  checkQuestionairreStatus,
  getEnterpriceType,
} from "../../../Services/commonService";
import { MANAGE_QUESTIONAIRRE } from "../../../Routes/Routes";
import { styles } from "./ManageQuestionairreStyles";
import { downloadAction } from "../../../Redux/AllApplications/allApplicationsActions";

const ManageQuestionairre = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [rowData, setRowData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  // const [allQuestionairres, setAllQuestionairres]=useState([])
  const [showConfirm, setShowConfirm] = useState(false);
  const [showImportDialog, setShowInportDialog] = useState(false);
  const [selectedId, setSelectedId] = useState(null);
  const userData = getUserData();
  const role = userData?.roles[0];
  const allQuestionairres = useSelector((state) => {
    if (role === ROLES.ADMIN) return state.questionairres.questionairreList;
    if (role === ROLES.DRDO) {
      return state.drdoQuestionairres.drdoQuestionairreList;
    }
  });

  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(MANAGE_QUESTIONAIRRE);
    });
  }, []);
  const columnDefs = [
    {
      field: "Upload Date",
    },
    {
      field: "Criteria Name",
      minWidth: "15rem",
    },
    {
      field: "Version",
    },
    {
      field: "Applicability",
    },
    {
      field: role === ROLES.ADMIN ? "DRDO Approved" : "Status",
    },

    {
      field: "",
      renderColumn: (row) => {
        return (
          <Box sx={styles.iconBtnWidth}>
            <Box className="icons" sx={commonStyles.displayNone}>
              <Button
                sx={commonStyles.iconButtonStyle}
                onClick={
                  () => {
                    setSelectedId(row?.id);
                    if (row?.id) {
                      navigate(`questionairre-view/${row?.id}`);
                    }
                  }
                  // setShowConfirm(true)
                }
              >
                <Tooltip title="View" arrow>
                  <IconApplicationView />
                </Tooltip>
              </Button>
              {/* {role!==ROLES.DRDO && row.status=="approved" && <Button
                sx={commonStyles.iconButtonStyle}
                onClick={() => {
                }}
              >
                <Tooltip title="Download" arrow>
                  <IconCircularDownload />
                </Tooltip>
              </Button> } */}
            </Box>
          </Box>
        );
      },
    },
  ];

  useEffect(() => {
    if (pageNumber) {
      if (role === ROLES.ADMIN)
        dispatch(getQuestionairreListAction({ start: pageNumber }));
      if (role === ROLES.DRDO) {
        // get drdo questinairre list
        dispatch(getDrdoQuestionairreListAction({ start: pageNumber }));
      }
    }
  }, [pageNumber, role]);

  useEffect(() => {
    if (allQuestionairres?.questionnaires?.length) {
      const dataSet = allQuestionairres?.questionnaires?.map(
        (questionairre) => {
          return {
            "Upload Date":
              moment(questionairre?.created_at).format("D MMM,YYYY") || "-",
            "Criteria Name": questionairre?.title || "-",
            Version: questionairre?.version || "-",
            Applicability: questionairre?.applicability || "-",
            // "DRDO Approved":(questionairre.status==0 || questionairre.status==null) ? "Pending" : "Approved",
            // id:questionairre?.id || "-",
            "DRDO Approved":
              checkQuestionairreStatus(questionairre?.state) || "-",
            // "Status":(questionairre.status==0 || questionairre.status==null) ? "Pending" : "Approved",
            Status: checkDrdoQuestionairreStatus(questionairre?.state) || "-",
            id: questionairre?.id || "-",
            status: questionairre?.state,
          };
        }
      );
      setRowData(dataSet);
    }
  }, [allQuestionairres?.questionnaires]);

  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const handleTopHeaderButtonClick = () => {
    setShowInportDialog(true);
  };
  const handleConfirm = () => {};

  const handleDownloadButton = () => {
    window.open(
      `${process.env.REACT_APP_DOWNLOAD_QUESTIONAIRE}/questionnaires/RNSDefenceVendorAssessmentModelforLargeEnterprises.xlsx`
    );
  };

  const exportButtonClick = () => {
    if (role === ROLES.ADMIN) {
      const url = `${process.env.REACT_APP_BASE_URL}/admin/questionnaires?is_excel=1&excel_type=admin_questionnaires`;
      dispatch(
        downloadAction("ManageQuestionairre", dispatch, url, "doc", "xlsx")
      );
    }

    if (role === ROLES.DRDO) {
      // get drdo questinairre list
      const url = `${process.env.REACT_APP_BASE_URL}/drdo/questionnaires?is_excel=1&excel_type=drdo_questionnaires`;
      dispatch(
        downloadAction("ManageQuestionairre", dispatch, url, "doc", "xlsx")
      );
    }
  };

  return (
    <>
      {allQuestionairres?.questionnaires?.total === 0 ? (
        <QpTypography
          displayText="No data available"
          styleData={commonStyles.noData}
        />
      ) : (
        <CustomTable
          columnDefs={columnDefs}
          rowData={rowData}
          title="Manage Assessment Criteria"
          hasPagination
          handlePagination={handlePagination}
          currentPage={pageNumber}
          totalValues={allQuestionairres?.total}
          isTopHeadButton={role === ROLES.DRDO ? false : true}
          TopHeaderButtonText={"Import"}
          topheaderButtonClick={handleTopHeaderButtonClick}
          styleData={commonStyles.tableHeight}
          isQuestionairreTable={true}
          questionairreBtnStyle={styles.questionairreBtnStyle}
          questionairreHeaderStyle={styles.questionairreHeaderStyle}
          isSecondaryButton={true}
          // buttonsDivStyle={styles.buttonsDivStyle}
          secondaryButtonClick={handleDownloadButton}
          secondaryButtonText={"Download"}
          BtnStylingInSecondaryBtncase={styles.BtnStylingInSecondaryBtncase}
          buttonsDivStyle={commonStyles.divFlexStyle}
          exportButtonClick={exportButtonClick}
          hasExport={false}
        />
      )}

      <QpDialog
        open={showConfirm}
        closeModal={() => {
          setSelectedId(null);
          setShowConfirm(false);
        }}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to View this Questionairre?"
          closeModal={() => {
            setSelectedId(null);
            setShowConfirm(false);
          }}
          onConfirm={() => {
            if (selectedId) {
              navigate(`questionairre-view/${selectedId}`);
            }
          }}
          // onConfirm={() => { if(selectedId) {
          //   cloneQuestionairreAction(navigate,selectedId)
          // }}}
        />
      </QpDialog>
      <QpDialog
        title={"Upload Questionairre"}
        open={showImportDialog}
        closeModal={() => {
          setShowInportDialog(false);
        }}
        styleData={{
          ...commonStyles.dialogContainer,
          ...commonStyles.dialogContainerWithSeventy,
        }}
      >
        <QpImportQuestionModal
          closeModal={() => setShowInportDialog(false)}
          onConfirm={() => handleConfirm()}
          setPageNumber={setPageNumber}
        />
      </QpDialog>
    </>
  );
};

export default ManageQuestionairre;
