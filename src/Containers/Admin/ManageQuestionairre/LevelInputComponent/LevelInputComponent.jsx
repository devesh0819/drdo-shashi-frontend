import { Box, Grid } from "@mui/material";
import { styles } from "../QuestionListCard/QuestionListCardStyles.js";
import { commonStyles } from "../../../../Styles/CommonStyles.js";
import { useEffect, useState } from "react";
import Radio from "@mui/material/Radio";

export const LevelInputComponent = ({
  question,
  section,
  category,
  closeContainer,
  inputDisabled,
}) => {
  const [sectionData, setSectionData] = useState({});

  useEffect(() => {
    setSectionData(section);
  }, []);

  return (
    <>
      <Box>
        <Grid
          container
          rowSpacing={1}
          columnSpacing={"4rem"}
          sx={commonStyles.signUpContainer}
        >
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Gold</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio checked={false} name="radio-buttons" disbaled />
                {sectionData?.options &&
                  JSON.parse(sectionData?.options)?.bronze}
              </Box>
            </Box>

            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Bronze</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio checked={false} name="radio-buttons" disbaled />
                {sectionData?.options &&
                  JSON.parse(sectionData?.options)?.silver}
              </Box>
            </Box>

            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Silver</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio checked={false} name="radio-buttons" disbaled />
                {sectionData?.options && JSON.parse(sectionData?.options)?.gold}
              </Box>
            </Box>

            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Diamond</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio checked={false} name="radio-buttons" disbaled />
                {sectionData?.options &&
                  JSON.parse(sectionData?.options)?.diamond}
              </Box>
            </Box>

            <Box>
              {/* <p 
                      style={{...commonStyles.inputLabel,
                      ...styles.agencyLabel,}}
                      >Platinum</p> */}
              <Box sx={commonStyles.levelInputBox}>
                <Radio checked={false} name="radio-buttons" disbaled />
                {sectionData?.options &&
                  JSON.parse(sectionData?.options)?.platinum}
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
