import { React, useEffect, useState } from "react";
import { Box, Typography, InputBase, Button } from "@mui/material";
import {
  commonStyles,
  customCommonStyles,
} from "../../../../Styles/CommonStyles";
import SectionListCard from "../SectionListCard/SectionListCard.jsx";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import { useParams, useNavigate } from "react-router-dom";
import QpInputLabel from "../../../../Components/QpInputLabel/QpInputLabel";
import { ReactComponent as IconCircularBack } from "../../../../Assets/Images/iconCircularBack.svg";
import { useDispatch, useSelector } from "react-redux";
import { getQuestionairreParameterAction } from "../../../../Redux/Admin/ManageQuestionairre/manageQuestionairreActions";
import { PARAM_TYPE, ROLES } from "../../../../Constant/RoleConstant";
import { getUserData } from "../../../../Services/localStorageService";
import { getDrdoQuestionairreParameterAction } from "../../../../Redux/DrdoAdmin/DrdoQuestionairre/drdoQuestionairreActions";

const QuestionsEdit = () => {
  const dispatch = useDispatch();
  const { id, q_id } = useParams();
  const [tabValue, setTabValue] = useState("p");
  // const [parameterData, setParameter]=useState({})
  const [currentActive, setCurrentActive] = useState(0);
  const userData = getUserData();
  const role = userData?.roles[0];
  const navigate = useNavigate();
  const parameterData = useSelector((state) => {
    if (role == ROLES.ADMIN) return state?.questionairres?.parameterDetail;
    if (role == ROLES.DRDO)
      return state?.drdoQuestionairres?.drdoParameterDetail;
  });

  useEffect(() => {
    if (role == ROLES.ADMIN) dispatch(getQuestionairreParameterAction(q_id));
    if (role == ROLES.DRDO) {
      dispatch(getDrdoQuestionairreParameterAction(q_id));
    }
  }, []);

  const handleTabChange = (e, val) => {
    setCurrentActive(0);
    setTabValue(val);
  };

  return (
    <>
      <Box sx={customCommonStyles.outerContainer}>
        <Box>
          <Box
            // style={{ ...commonStyles.topHeader, ...commonStyles.displayStyle }}
            style={{ ...commonStyles.topHeader }}
          >
            <Button
              disableRipple
              sx={commonStyles.backButton}
              // sx={{
              //   minWidth: "unset",
              //   padding: 0,
              //   paddingRight: "0.563rem",
              //   "&:hover": { backgroundColor: "transparent" },
              //   "@media(max-width:600px)": {
              //     "&.backButton svg": {
              //       height: "1rem",
              //       width: "1rem",
              //     },
              //   },
              // }}
              onClick={() => {
                navigate(`/manage-questionairre/questionairre-view/${id}`);
              }}
              className="backButton"
            >
              <IconCircularBack />
            </Button>
            <Typography style={commonStyles.titleText}>
              View Parameter
            </Typography>
          </Box>
          <Box sx={commonStyles.parameterContainerHeight}>
            <Box style={commonStyles.parameterContainer}>
              <QpInputLabel
                displayText="Parameter Title"
                // required={true}
                styleData={commonStyles.inputLabel}
              />
              <Box sx={commonStyles.parameterText}>
                {parameterData?.title || "-"}
              </Box>
            </Box>
            {/* ...........................tabs.............. */}

            <Box sx={commonStyles.fullWidth}>
              <TabContext value={tabValue}>
                <Box sx={commonStyles.tabDiv}>
                  <TabList
                    onChange={handleTabChange}
                    aria-label="lab API tabs example"
                  >
                    <Tab label="Planning" value="p" />
                    <Tab label="Deployment" value="d" />
                    <Tab label="Monitoring" value="m" />
                  </TabList>
                </Box>
                <TabPanel value="p">
                  <>
                    {parameterData?.sections?.filter(
                      (item) => item.type == PARAM_TYPE.PLANNING
                    )?.length
                      ? parameterData?.sections
                          ?.filter((item) => item.type == PARAM_TYPE.PLANNING)
                          ?.map((section, sectionIndex) => {
                            return (
                              <SectionListCard
                                key={section}
                                parameterData={parameterData}
                                section={section}
                                category={tabValue}
                                sectionIndex={sectionIndex}
                                currentActive={currentActive}
                                setCurrentActive={(data) =>
                                  setCurrentActive(data)
                                }
                                section_id={sectionIndex + 1}
                                showContainerFromProp={
                                  sectionIndex + 1 == currentActive
                                    ? true
                                    : false
                                }
                                isQuestionairre={true}
                              />
                            );
                          })
                      : "No Sections for this type"}
                  </>
                </TabPanel>

                <TabPanel value="d">
                  {parameterData?.sections?.filter(
                    (item) => item.type == PARAM_TYPE.DEPLOYMENT
                  )?.length
                    ? parameterData?.sections
                        ?.filter((item) => item.type == PARAM_TYPE.DEPLOYMENT)
                        ?.map((section, sectionIndex) => {
                          return (
                            <SectionListCard
                              key={section}
                              parameterData={parameterData}
                              section={section}
                              category={tabValue}
                              sectionIndex={sectionIndex}
                              currentActive={currentActive}
                              setCurrentActive={(data) =>
                                setCurrentActive(data)
                              }
                              section_id={sectionIndex + 1}
                              showContainerFromProp={
                                sectionIndex + 1 == currentActive ? true : false
                              }
                              isQuestionairre={true}
                            />
                          );
                        })
                    : "No Sections for this type"}
                </TabPanel>
                <TabPanel value="m">
                  {parameterData?.sections?.filter(
                    (item) => item.type == PARAM_TYPE.MONITORING
                  )?.length
                    ? parameterData?.sections
                        ?.filter((item) => item.type == PARAM_TYPE.MONITORING)
                        ?.map((section, sectionIndex) => {
                          return (
                            <SectionListCard
                              key={section}
                              parameterData={parameterData}
                              section={section}
                              category={tabValue}
                              sectionIndex={sectionIndex}
                              currentActive={currentActive}
                              setCurrentActive={setCurrentActive}
                              section_id={sectionIndex + 1}
                              showContainerFromProp={
                                sectionIndex + 1 == currentActive ? true : false
                              }
                              isQuestionairre={true}
                            />
                          );
                        })
                    : "No Sections for this type"}
                </TabPanel>
              </TabContext>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default QuestionsEdit;
