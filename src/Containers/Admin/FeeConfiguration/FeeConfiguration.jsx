import { Autocomplete, Box, Grid, TextField, Typography } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import CustomInput from "../../../Components/CustomInput/CustomInput";
import QpButton from "../../../Components/QpButton/QpButton";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import { styles } from "./FeeConfigirationStyles";
import {
  getFeeConfigurationData,
  updateFeeConfigurationAction,
} from "../../../Redux/Admin/FeeConfiguration/feeConfigurationActions";
import { useDispatch, useSelector } from "react-redux";
import CustomRadioButton from "../../../Components/CustomRadioButton/CustomRadioButton";
import { tdsValues, yesNo } from "../../../Constant/AppConstant";
import * as _ from "lodash";
import { showToast } from "../../../Components/Toast/Toast";
import QpDialog from "../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../Components/QpConfirmModal/QpConfirmModal";
import { ROLES } from "../../../Constant/RoleConstant";
import { getUserData } from "../../../Services/localStorageService";

const FeeConfiguration = () => {
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const userData = getUserData();
  const role = userData?.roles[0];
  const feeConfigurationData = useSelector(
    (state) => state.feeConfiguration.feeConfigurationData
  );

  const initialTdsState = {
    small_tds_radio_value: "0",
    small_tds_value: [],
    micro_tds_radio_value: "0",
    micro_tds_value: [],
    large_tds_radio_value: "0",
    large_tds_value: [],
    medium_tds_radio_value: "0",
    medium_tds_value: [],
  };
  const [isSubmit, setIsSubmit] = useState(false);
  const [feeData, setFeeData] = useState({});
  const feeDataRef = useRef(feeData);
  const [tdsData, setTdsData] = useState(initialTdsState);

  const setEnterpriseTdsValues = (radioKey, key, value) => {
    if (value === 0 || value === "0") {
      setTdsData((data) => ({
        ...data,
        [radioKey]: "0",
        [key]: [],
      }));
    } else {
      let valueArray = value?.split(",");
      valueArray = valueArray?.map((value) => ({ label: value, value: value }));
      setTdsData((data) => ({
        ...data,
        [radioKey]: "1",
        [key]: valueArray,
      }));
    }
  };

  useEffect(() => {
    if (feeConfigurationData?.length > 0) {
      feeConfigurationData?.forEach((enterprise) => {
        if (enterprise?.enterprice_type === "MIC") {
          setEnterpriseTdsValues(
            "micro_tds_radio_value",
            "micro_tds_value",
            enterprise?.payment_breakup?.micro_tds_value
          );
        } else if (enterprise?.enterprice_type === "SM") {
          setEnterpriseTdsValues(
            "small_tds_radio_value",
            "small_tds_value",
            enterprise?.payment_breakup?.small_tds_value
          );
        } else if (enterprise?.enterprice_type === "MED") {
          setEnterpriseTdsValues(
            "medium_tds_radio_value",
            "medium_tds_value",
            enterprise?.payment_breakup?.medium_tds_value
          );
        } else if (enterprise?.enterprice_type === "L") {
          setEnterpriseTdsValues(
            "large_tds_radio_value",
            "large_tds_value",
            enterprise?.payment_breakup?.large_tds_value
          );
        }
      });
    }
  }, [feeConfigurationData]);

  useEffect(() => {
    dispatch(getFeeConfigurationData());
  }, [dispatch]);

  const onSubmit = () => {
    if (
      tdsData?.small_tds_radio_value === "1" &&
      tdsData?.small_tds_value.length === 0
    ) {
      showToast(`Please select TDS Value for small enterprise`, "error");
      return;
    }
    if (
      tdsData?.micro_tds_radio_value === "1" &&
      tdsData?.micro_tds_value.length === 0
    ) {
      showToast(`Please select TDS Value for micro enterprise`, "error");
      return;
    }
    if (
      tdsData?.medium_tds_radio_value === "1" &&
      tdsData?.medium_tds_value.length === 0
    ) {
      showToast(`Please select TDS Value for medium enterprise`, "error");
      return;
    }
    if (
      tdsData?.large_tds_radio_value === "1" &&
      tdsData?.large_tds_value.length === 0
    ) {
      showToast(`Please select TDS Value for large enterprise`, "error");
      return;
    }

    const {
      small_certification_fee_text,
      small_certification_fee_value,
      small_assessor_fee_text,
      small_assessor_fee_value,
      small_gst_text,
      small_gst_value,
      small_tds_text,
      small_tds_value,
      //   small_miscell_fee_text,
      //   small_miscell_fee_value,
      //   small_miscell_discount_text,
      //   small_miscell_discount_value,
      small_subsidy_text,
      small_subsidy_value,
      micro_certification_fee_text,
      micro_certification_fee_value,
      micro_assessor_fee_text,
      micro_assessor_fee_value,
      micro_gst_text,
      micro_gst_value,
      micro_tds_text,
      micro_tds_value,
      //   micro_miscell_fee_text,
      //   micro_miscell_fee_value,
      //   micro_miscell_discount_text,
      //   micro_miscell_discount_value,
      micro_subsidy_text,
      micro_subsidy_value,
      medium_certification_fee_text,
      medium_certification_fee_value,
      medium_assessor_fee_text,
      medium_assessor_fee_value,
      medium_gst_text,
      medium_gst_value,
      medium_tds_text,
      medium_tds_value,
      //   medium_miscell_fee_text,
      //   medium_miscell_fee_value,
      //   medium_miscell_discount_text,
      //   medium_miscell_discount_value,
      medium_subsidy_text,
      medium_subsidy_value,
      large_certification_fee_text,
      large_certification_fee_value,
      large_assessor_fee_text,
      large_assessor_fee_value,
      large_gst_text,
      large_gst_value,
      large_tds_text,
      large_tds_value,
      //   large_miscell_fee_text,
      //   large_miscell_fee_value,
      //   large_miscell_discount_text,
      //   large_miscell_discount_value,
      large_subsidy_text,
      large_subsidy_value,
      small_tds_radio_value,
      micro_tds_radio_value,
      large_tds_radio_value,
      medium_tds_radio_value,
    } = feeData;
    if (
      !small_certification_fee_text ||
      !small_certification_fee_value ||
      !small_assessor_fee_text ||
      !small_assessor_fee_value ||
      !small_gst_text ||
      !small_gst_value ||
      !small_tds_text ||
      //   !small_tds_value ||
      //   !small_miscell_fee_text ||
      //   !small_miscell_fee_value ||
      //   !small_miscell_discount_text ||
      //   !small_miscell_discount_value ||
      !small_subsidy_text ||
      !small_subsidy_value ||
      !micro_certification_fee_text ||
      !micro_certification_fee_value ||
      !micro_assessor_fee_text ||
      !micro_assessor_fee_value ||
      !micro_gst_text ||
      !micro_gst_value ||
      !micro_tds_text ||
      //   !micro_tds_value ||
      //   !micro_miscell_fee_text ||
      //   !micro_miscell_fee_value ||
      //   !micro_miscell_discount_text ||
      //   !micro_miscell_discount_value ||
      !micro_subsidy_text ||
      !micro_subsidy_value ||
      !medium_certification_fee_text ||
      !medium_certification_fee_value ||
      !medium_assessor_fee_text ||
      !medium_assessor_fee_value ||
      !medium_gst_text ||
      !medium_gst_value ||
      !medium_tds_text ||
      //   !medium_tds_value ||
      //   !medium_miscell_fee_text ||
      //   !medium_miscell_fee_value ||
      //   !medium_miscell_discount_text ||
      //   !medium_miscell_discount_value ||
      !medium_subsidy_text ||
      !medium_subsidy_value ||
      !large_certification_fee_text ||
      !large_certification_fee_value ||
      !large_assessor_fee_text ||
      !large_assessor_fee_value ||
      !large_gst_text ||
      !large_gst_value ||
      !large_tds_text ||
      //   !large_tds_value ||
      //   !large_miscell_fee_text ||
      //   !large_miscell_fee_value ||
      //   !large_miscell_discount_text ||
      //   !large_miscell_discount_value ||
      !large_subsidy_text ||
      !large_subsidy_value ||
      !small_tds_radio_value ||
      !micro_tds_radio_value ||
      !large_tds_radio_value ||
      !medium_tds_radio_value
    ) {
      return;
    }
    const payload = {
      small_certification_fee_text,
      small_certification_fee_value: Number(small_certification_fee_value),
      small_assessor_fee_text,
      small_assessor_fee_value: Number(small_assessor_fee_value),
      small_gst_text,
      small_gst_value: Number(small_gst_value),
      small_tds_text,
      small_tds_value:
        tdsData?.small_tds_radio_value === "0"
          ? "0"
          : tdsData?.small_tds_value?.map((val) => val.label).toString(),
      //   small_miscell_fee_text,
      //   small_miscell_fee_value,
      //   small_miscell_discount_text,
      //   small_miscell_discount_value,
      small_subsidy_text,
      small_subsidy_value: Number(small_subsidy_value),
      micro_certification_fee_text,
      micro_certification_fee_value: Number(micro_certification_fee_value),
      micro_assessor_fee_text,
      micro_assessor_fee_value: Number(micro_assessor_fee_value),
      micro_gst_text,
      micro_gst_value: Number(micro_gst_value),
      micro_tds_text,
      micro_tds_value:
        tdsData?.micro_tds_radio_value === "0"
          ? "0"
          : tdsData?.micro_tds_value?.map((val) => val.label).toString(),
      //   micro_miscell_fee_text,
      //   micro_miscell_fee_value,
      //   micro_miscell_discount_text,
      //   micro_miscell_discount_value,
      micro_subsidy_text,
      micro_subsidy_value: Number(micro_subsidy_value),
      medium_certification_fee_text,
      medium_certification_fee_value: Number(medium_certification_fee_value),
      medium_assessor_fee_text,
      medium_assessor_fee_value: Number(medium_assessor_fee_value),
      medium_gst_text,
      medium_gst_value: Number(medium_gst_value),
      medium_tds_text,
      medium_tds_value:
        tdsData?.medium_tds_radio_value === "0"
          ? "0"
          : tdsData?.medium_tds_value?.map((val) => val.label).toString(),
      //   medium_miscell_fee_text,
      //   medium_miscell_fee_value,
      //   medium_miscell_discount_text,
      //   medium_miscell_discount_value,
      medium_subsidy_text,
      medium_subsidy_value: Number(medium_subsidy_value),
      large_certification_fee_text,
      large_certification_fee_value: Number(large_certification_fee_value),
      large_assessor_fee_text,
      large_assessor_fee_value: Number(large_assessor_fee_value),
      large_gst_text,
      large_gst_value: Number(large_gst_value),
      large_tds_text,
      large_tds_value:
        tdsData?.large_tds_radio_value === "0"
          ? "0"
          : tdsData?.large_tds_value?.map((val) => val.label).toString(),
      //   large_miscell_fee_text,
      //   large_miscell_fee_value,
      //   large_miscell_discount_text,
      //   large_miscell_discount_value,
      large_subsidy_text,
      large_subsidy_value: Number(large_subsidy_value),
    };
    dispatch(updateFeeConfigurationAction(payload));
  };

  useEffect(() => {
    const {
      small_certification_fee_text,
      small_certification_fee_value,
      small_assessor_fee_text,
      small_assessor_fee_value,
      small_gst_text,
      small_gst_value,
      small_tds_text,
      small_tds_value,
      //   small_miscell_fee_text,
      //   small_miscell_fee_value,
      //   small_miscell_discount_text,
      //   small_miscell_discount_value,
      small_subsidy_text,
      small_subsidy_value,
      micro_certification_fee_text,
      micro_certification_fee_value,
      micro_assessor_fee_text,
      micro_assessor_fee_value,
      micro_gst_text,
      micro_gst_value,
      micro_tds_text,
      micro_tds_value,
      //   micro_miscell_fee_text,
      //   micro_miscell_fee_value,
      //   micro_miscell_discount_text,
      //   micro_miscell_discount_value,
      micro_subsidy_text,
      micro_subsidy_value,
      medium_certification_fee_text,
      medium_certification_fee_value,
      medium_assessor_fee_text,
      medium_assessor_fee_value,
      medium_gst_text,
      medium_gst_value,
      medium_tds_text,
      medium_tds_value,
      //   medium_miscell_fee_text,
      //   medium_miscell_fee_value,
      //   medium_miscell_discount_text,
      //   medium_miscell_discount_value,
      medium_subsidy_text,
      medium_subsidy_value,
      large_certification_fee_text,
      large_certification_fee_value,
      large_assessor_fee_text,
      large_assessor_fee_value,
      large_gst_text,
      large_gst_value,
      large_tds_text,
      large_tds_value,
      //   large_miscell_fee_text,
      //   large_miscell_fee_value,
      //   large_miscell_discount_text,
      //   large_miscell_discount_value,
      large_subsidy_text,
      large_subsidy_value,
    } = feeData;
    if (
      small_certification_fee_text &&
      small_certification_fee_value &&
      small_assessor_fee_text &&
      small_assessor_fee_value &&
      small_gst_text &&
      small_gst_value &&
      small_tds_text &&
      //   small_tds_value &&
      //   small_miscell_fee_text &&
      //   small_miscell_fee_value &&
      //   small_miscell_discount_text &&
      //   small_miscell_discount_value &&
      small_subsidy_text &&
      small_subsidy_value &&
      micro_certification_fee_text &&
      micro_certification_fee_value &&
      micro_assessor_fee_text &&
      micro_assessor_fee_value &&
      micro_gst_text &&
      micro_gst_value &&
      micro_tds_text &&
      //   micro_tds_value &&
      //   micro_miscell_fee_text &&
      //   micro_miscell_fee_value &&
      //   micro_miscell_discount_text &&
      //   micro_miscell_discount_value &&
      micro_subsidy_text &&
      micro_subsidy_value &&
      medium_certification_fee_text &&
      medium_certification_fee_value &&
      medium_assessor_fee_text &&
      medium_assessor_fee_value &&
      medium_gst_text &&
      medium_gst_value &&
      medium_tds_text &&
      //   medium_tds_value &&
      //   medium_miscell_fee_text &&
      //   medium_miscell_fee_value &&
      //   medium_miscell_discount_text &&
      //   medium_miscell_discount_value &&
      medium_subsidy_text &&
      medium_subsidy_value &&
      large_certification_fee_text &&
      large_certification_fee_value &&
      large_assessor_fee_text &&
      large_assessor_fee_value &&
      large_gst_text &&
      large_gst_value &&
      large_tds_text &&
      //   large_tds_value &&
      //   large_miscell_fee_text &&
      //   large_miscell_fee_value &&
      //   large_miscell_discount_text &&
      //   large_miscell_discount_value &&
      large_subsidy_text &&
      large_subsidy_value
    ) {
      onSubmit();
    }
  }, [feeData]);
  const updateState = (newState) => {
    feeDataRef.current = newState;
    setFeeData(newState);
    setIsSubmit(false);
  };
  const getData = (key, value) => {
    updateState({
      ...feeDataRef.current,
      [key]: value,
    });
  };
  const saveHandler = () => {
    setIsSubmit(true);
  };

  return (
    <>
      <Box sx={commonStyles.headerOuterContainer}>
        <Box sx={commonStyles.topBackButtonHeader}>
          {/* <Button disableRipple sx={commonStyles.backButton}>
            <IconCircularBack />
          </Button> */}
          <Typography style={commonStyles.titleBackText}>
            Fee Configuration
          </Typography>
        </Box>
        {role === ROLES.ADMIN && (
          <QpButton
            displayText="Save"
            styleData={styles.updateButton}
            textStyle={styles.updateButtonText}
            onClick={() => setShowModal(true)}
          />
        )}
      </Box>
      <Box
        sx={{
          ...customCommonStyles.fieldContainer,
        }}
      >
        <Box sx={customCommonStyles.marginBottomTwo}>
          <QpTypography
            displayText="1. Small Enterprise"
            styleData={styles.headingText}
          />
        </Box>
        <Grid
          container
          // rowSpacing={}
          columnSpacing={"5rem"}
          sx={{
            ...commonStyles.signUpContainer,
            ...customCommonStyles.marginBottomTwo,
          }}
        >
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Certification Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_certification_fee_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "small_certification_fee_text",
                  data?.small_certification_fee_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[1]?.payment_breakup
                  ?.small_certification_fee_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Certification Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_certification_fee_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "small_certification_fee_value",
                  data?.small_certification_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[1]?.payment_breakup?.small_certification_fee_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Subsidy Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_subsidy_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("small_subsidy_text", data?.small_subsidy_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[1]?.payment_breakup?.small_subsidy_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Subsidy Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_subsidy_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("small_subsidy_value", data?.small_subsidy_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[1]?.payment_breakup?.small_subsidy_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Outstation Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_assessor_fee_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "small_assessor_fee_text",
                  data?.small_assessor_fee_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[1]?.payment_breakup
                  ?.small_assessor_fee_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Outstation Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_assessor_fee_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "small_assessor_fee_value",
                  data?.small_assessor_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[1]?.payment_breakup?.small_assessor_fee_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="GST Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_gst_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("small_gst_text", data?.small_gst_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[1]?.payment_breakup?.small_gst_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="GST Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_gst_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("small_gst_value", data?.small_gst_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[1]?.payment_breakup?.small_gst_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="TDS Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_tds_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("small_tds_text", data?.small_tds_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[1]?.payment_breakup?.small_tds_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
              //   border: "1px solid red",
            }}
          >
            {/* <CustomInput
              label="TDS Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_tds_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("small_tds_value", data?.small_tds_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.small_tds_value}
            /> */}
            <Box sx={styles.tdsBoxStyle}>
              <CustomRadioButton
                displayText="TDS Value"
                options={yesNo}
                labelStyleData={{
                  ...commonStyles.inputLabel,
                  ...styles.labelStyleData,
                }}
                setValueData={setTdsData}
                defaultValue={tdsData?.small_tds_radio_value}
                name="small_tds_radio_value"
                registerName="small_tds_radio_value"
                submit={isSubmit}
                setData={(data) =>
                  getData("small_tds_radio_value", data?.small_tds_radio_value)
                }
                readOnly={role === ROLES.DRDO ? true : false}
                disabled={role === ROLES.DRDO ? true : false}
              />
              {tdsData?.small_tds_radio_value === "0" ? (
                <Typography sx={styles.noTdsText}>
                  TDS Value for small enterprise is 0
                </Typography>
              ) : (
                <Autocomplete
                  multiple
                  filterSelectedOptions
                  id="tags-outlined"
                  options={tdsValues}
                  disabled={role === ROLES.DRDO ? true : false}
                  onChange={(event, newValue) => {
                    if (newValue.length > 0) {
                      setTdsData((data) => ({
                        ...data,
                        small_tds_value: newValue,
                        small_tds_radio_value: "1",
                      }));
                    } else {
                      setTdsData((data) => ({
                        ...data,
                        small_tds_value: newValue,
                        small_tds_radio_value: "0",
                      }));
                    }
                  }}
                  defaultValue={tdsData?.small_tds_value}
                  getOptionLabel={(option) => option.label}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      style={{
                        backgroundColor: "#fff",
                        borderBottomLeftRadius: "0.5rem",
                        borderTopLeftRadius: "0.5rem",
                        borderTopRightRadius: 0,
                        minWidth: "240px",
                        // "& .MuiInputBase-root": { paddingRight: 0 },
                      }}
                    />
                  )}
                />
              )}
            </Box>
          </Grid>
          {/* <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_miscell_fee_text"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData("small_miscell_fee_text", data?.small_miscell_fee_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.small_miscell_fee_text}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_miscell_fee_value"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "small_miscell_fee_value",
                  data?.small_miscell_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.small_miscell_fee_value}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Discount Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_miscell_discount_text"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "small_miscell_discount_text",
                  data?.small_miscell_discount_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.small_miscell_discount_text}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Discount Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="small_miscell_discount_value"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "small_miscell_discount_value",
                  data?.small_miscell_discount_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.small_miscell_discount_value}
            />
          </Grid> */}
        </Grid>
        <Box sx={customCommonStyles.marginBottomTwo}>
          <QpTypography
            displayText="2. Micro Enterprise"
            styleData={styles.headingText}
          />
        </Box>
        <Grid
          container
          // rowSpacing={}
          columnSpacing={"5rem"}
          sx={{
            ...commonStyles.signUpContainer,
            ...customCommonStyles.marginBottomTwo,
          }}
        >
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Certification Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_certification_fee_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "micro_certification_fee_text",
                  data?.micro_certification_fee_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[0]?.payment_breakup
                  ?.micro_certification_fee_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Certification Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_certification_fee_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "micro_certification_fee_value",
                  data?.micro_certification_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[0]?.payment_breakup?.micro_certification_fee_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Subsidy Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_subsidy_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("micro_subsidy_text", data?.micro_subsidy_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[0]?.payment_breakup?.micro_subsidy_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Subsidy Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_subsidy_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("micro_subsidy_value", data?.micro_subsidy_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[0]?.payment_breakup?.micro_subsidy_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Outstation Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_assessor_fee_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "micro_assessor_fee_text",
                  data?.micro_assessor_fee_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[0]?.payment_breakup
                  ?.micro_assessor_fee_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Outstation Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_assessor_fee_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "micro_assessor_fee_value",
                  data?.micro_assessor_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[0]?.payment_breakup?.micro_assessor_fee_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="GST Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_gst_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("micro_gst_text", data?.micro_gst_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[0]?.payment_breakup?.micro_gst_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="GST Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_gst_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("micro_gst_value", data?.micro_gst_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[0]?.payment_breakup?.micro_gst_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="TDS Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_tds_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("micro_tds_text", data?.micro_tds_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[0]?.payment_breakup?.micro_tds_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          {/* <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="TDS Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_tds_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("micro_tds_value", data?.micro_tds_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.micro_tds_value}
            />
          </Grid> */}
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
              //   border: "1px solid red",
            }}
          >
            <Box sx={styles.tdsBoxStyle}>
              <CustomRadioButton
                displayText="TDS Value"
                options={yesNo}
                labelStyleData={{
                  ...commonStyles.inputLabel,
                  ...styles.labelStyleData,
                }}
                setValueData={setTdsData}
                defaultValue={tdsData?.micro_tds_radio_value}
                name="micro_tds_radio_value"
                registerName="micro_tds_radio_value"
                submit={isSubmit}
                setData={(data) =>
                  getData("micro_tds_radio_value", data?.micro_tds_radio_value)
                }
                readOnly={role === ROLES.DRDO ? true : false}
                disabled={role === ROLES.DRDO ? true : false}
              />
              {tdsData?.micro_tds_radio_value === "0" ? (
                <Typography sx={styles.noTdsText}>
                  TDS Value for micro enterprise is 0
                </Typography>
              ) : (
                <Autocomplete
                  multiple
                  filterSelectedOptions
                  id="tags-outlined"
                  options={tdsValues}
                  disabled={role === ROLES.DRDO ? true : false}
                  onChange={(event, newValue) => {
                    if (newValue.length > 0) {
                      setTdsData((data) => ({
                        ...data,
                        micro_tds_value: newValue,
                        micro_tds_radio_value: "1",
                      }));
                    } else {
                      setTdsData((data) => ({
                        ...data,
                        micro_tds_value: newValue,
                        micro_tds_radio_value: "0",
                      }));
                    }
                  }}
                  defaultValue={tdsData?.micro_tds_value}
                  getOptionLabel={(option) => option.label}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      style={{
                        backgroundColor: "#fff",
                        borderBottomLeftRadius: "0.5rem",
                        borderTopLeftRadius: "0.5rem",
                        borderTopRightRadius: 0,
                        minWidth: "240px",
                        // "& .MuiInputBase-root": { paddingRight: 0 },
                      }}
                    />
                  )}
                />
              )}
            </Box>
          </Grid>
          {/* <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_miscell_fee_text"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData("micro_miscell_fee_text", data?.micro_miscell_fee_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.micro_miscell_fee_text}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_miscell_fee_value"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "micro_miscell_fee_value",
                  data?.micro_miscell_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.micro_miscell_fee_value}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Discount Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_miscell_discount_text"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "micro_miscell_discount_text",
                  data?.micro_miscell_discount_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.micro_miscell_discount_text}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Discount Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="micro_miscell_discount_value"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "micro_miscell_discount_value",
                  data?.micro_miscell_discount_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.micro_miscell_discount_value}
            />
          </Grid> */}
        </Grid>
        <Box sx={customCommonStyles.marginBottomTwo}>
          <QpTypography
            displayText="3. Medium Enterprise"
            styleData={styles.headingText}
          />
        </Box>
        <Grid
          container
          // rowSpacing={}
          columnSpacing={"5rem"}
          sx={{
            ...commonStyles.signUpContainer,
            ...customCommonStyles.marginBottomTwo,
          }}
        >
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Certification Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_certification_fee_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "medium_certification_fee_text",
                  data?.medium_certification_fee_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[2]?.payment_breakup
                  ?.medium_certification_fee_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Certification Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_certification_fee_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "medium_certification_fee_value",
                  data?.medium_certification_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[2]?.payment_breakup?.medium_certification_fee_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Subsidy Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_subsidy_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("medium_subsidy_text", data?.medium_subsidy_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[2]?.payment_breakup?.medium_subsidy_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Subsidy Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_subsidy_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("medium_subsidy_value", data?.medium_subsidy_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[2]?.payment_breakup?.medium_subsidy_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Outstation Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_assessor_fee_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "medium_assessor_fee_text",
                  data?.medium_assessor_fee_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[2]?.payment_breakup
                  ?.medium_assessor_fee_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Outstation Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_assessor_fee_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "medium_assessor_fee_value",
                  data?.medium_assessor_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[2]?.payment_breakup?.medium_assessor_fee_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="GST Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_gst_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("medium_gst_text", data?.medium_gst_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[2]?.payment_breakup?.medium_gst_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="GST Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_gst_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("medium_gst_value", data?.medium_gst_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[2]?.payment_breakup?.medium_gst_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="TDS Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_tds_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("medium_tds_text", data?.medium_tds_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[2]?.payment_breakup?.medium_tds_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          {/* <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="TDS Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_tds_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("medium_tds_value", data?.medium_tds_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.medium_tds_value}
            />
          </Grid> */}
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
              //   border: "1px solid red",
            }}
          >
            <Box sx={styles.tdsBoxStyle}>
              <CustomRadioButton
                displayText="TDS Value"
                options={yesNo}
                labelStyleData={{
                  ...commonStyles.inputLabel,
                  ...styles.labelStyleData,
                }}
                setValueData={setTdsData}
                defaultValue={tdsData?.medium_tds_radio_value}
                name="medium_tds_radio_value"
                registerName="medium_tds_radio_value"
                submit={isSubmit}
                setData={(data) =>
                  getData(
                    "medium_tds_radio_value",
                    data?.medium_tds_radio_value
                  )
                }
                readOnly={role === ROLES.DRDO ? true : false}
                disabled={role === ROLES.DRDO ? true : false}
              />
              {tdsData?.medium_tds_radio_value === "0" ? (
                <Typography sx={styles.noTdsText}>
                  TDS Value for medium enterprise is 0
                </Typography>
              ) : (
                <Autocomplete
                  multiple
                  filterSelectedOptions
                  id="tags-outlined"
                  options={tdsValues}
                  disabled={role === ROLES.DRDO ? true : false}
                  onChange={(event, newValue) => {
                    if (newValue.length > 0) {
                      setTdsData((data) => ({
                        ...data,
                        medium_tds_value: newValue,
                        medium_tds_radio_value: "1",
                      }));
                    } else {
                      setTdsData((data) => ({
                        ...data,
                        medium_tds_value: newValue,
                        medium_tds_radio_value: "0",
                      }));
                    }
                  }}
                  defaultValue={tdsData?.medium_tds_value}
                  getOptionLabel={(option) => option.label}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      style={{
                        backgroundColor: "#fff",
                        borderBottomLeftRadius: "0.5rem",
                        borderTopLeftRadius: "0.5rem",
                        borderTopRightRadius: 0,
                        minWidth: "240px",
                        // "& .MuiInputBase-root": { paddingRight: 0 },
                      }}
                    />
                  )}
                />
              )}
            </Box>
          </Grid>
          {/* <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_miscell_fee_text"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "medium_miscell_fee_text",
                  data?.medium_miscell_fee_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.medium_miscell_fee_text}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_miscell_fee_value"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "medium_miscell_fee_value",
                  data?.medium_miscell_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.medium_miscell_fee_value}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Discount Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_miscell_discount_text"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "medium_miscell_discount_text",
                  data?.medium_miscell_discount_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.medium_miscell_discount_text}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Discount Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="medium_miscell_discount_value"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "medium_miscell_discount_value",
                  data?.medium_miscell_discount_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.medium_miscell_discount_value}
            />
          </Grid> */}
        </Grid>
        <Box sx={customCommonStyles.marginBottomTwo}>
          <QpTypography
            displayText="4. Large Enterprise"
            styleData={styles.headingText}
          />
        </Box>
        <Grid
          container
          // rowSpacing={}
          columnSpacing={"5rem"}
          sx={{
            ...commonStyles.signUpContainer,
            ...customCommonStyles.marginBottomTwo,
          }}
        >
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Certification Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_certification_fee_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "large_certification_fee_text",
                  data?.large_certification_fee_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[3]?.payment_breakup
                  ?.large_certification_fee_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Certification Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_certification_fee_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "large_certification_fee_value",
                  data?.large_certification_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[3]?.payment_breakup?.large_certification_fee_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Subsidy Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_subsidy_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("large_subsidy_text", data?.large_subsidy_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[3]?.payment_breakup?.large_subsidy_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Subsidy Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_subsidy_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("large_subsidy_value", data?.large_subsidy_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[3]?.payment_breakup?.large_subsidy_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Outstation Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_assessor_fee_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "large_assessor_fee_text",
                  data?.large_assessor_fee_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[3]?.payment_breakup
                  ?.large_assessor_fee_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Outstation Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_assessor_fee_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "large_assessor_fee_value",
                  data?.large_assessor_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[3]?.payment_breakup?.large_assessor_fee_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="GST Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_gst_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("large_gst_text", data?.large_gst_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[3]?.payment_breakup?.large_gst_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="GST Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_gst_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("large_gst_value", data?.large_gst_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[3]?.payment_breakup?.large_gst_value?.toString()
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="TDS Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_tds_text"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("large_tds_text", data?.large_tds_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={
                feeConfigurationData &&
                feeConfigurationData[3]?.payment_breakup?.large_tds_text
              }
              readOnly={role === ROLES.DRDO ? true : false}
              disabled={role === ROLES.DRDO ? true : false}
            />
          </Grid>
          {/* <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="TDS Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_tds_value"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("large_tds_value", data?.large_tds_value)
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.large_tds_value}
            />
          </Grid> */}
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
              //   border: "1px solid red",
            }}
          >
            <Box sx={styles.tdsBoxStyle}>
              <CustomRadioButton
                displayText="TDS Value"
                options={yesNo}
                labelStyleData={{
                  ...commonStyles.inputLabel,
                  ...styles.labelStyleData,
                }}
                setValueData={setTdsData}
                defaultValue={tdsData?.large_tds_radio_value}
                name="large_tds_radio_value"
                registerName="large_tds_radio_value"
                submit={isSubmit}
                setData={(data) =>
                  getData("large_tds_radio_value", data?.large_tds_radio_value)
                }
                readOnly={role === ROLES.DRDO ? true : false}
                disabled={role === ROLES.DRDO ? true : false}
              />
              {tdsData?.large_tds_radio_value === "0" ? (
                <Typography sx={styles.noTdsText}>
                  TDS Value for large enterprise is 0
                </Typography>
              ) : (
                <Autocomplete
                  multiple
                  filterSelectedOptions
                  id="tags-outlined"
                  options={tdsValues}
                  onChange={(event, newValue) => {
                    if (newValue.length > 0) {
                      setTdsData((data) => ({
                        ...data,
                        large_tds_value: newValue,
                        large_tds_radio_value: "1",
                      }));
                    } else {
                      setTdsData((data) => ({
                        ...data,
                        large_tds_value: newValue,
                        large_tds_radio_value: "0",
                      }));
                    }
                  }}
                  defaultValue={tdsData?.large_tds_value}
                  disabled={role === ROLES.DRDO ? true : false}
                  getOptionLabel={(option) => option.label}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      style={{
                        backgroundColor: "#fff",
                        borderBottomLeftRadius: "0.5rem",
                        borderTopLeftRadius: "0.5rem",
                        borderTopRightRadius: 0,
                        minWidth: "240px",
                        // "& .MuiInputBase-root": { paddingRight: 0 },
                      }}
                    />
                  )}
                />
              )}
            </Box>
          </Grid>
          {/* <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Fee Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_miscell_fee_text"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData("large_miscell_fee_text", data?.large_miscell_fee_text)
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.large_miscell_fee_text}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Fee Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_miscell_fee_value"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "large_miscell_fee_value",
                  data?.large_miscell_fee_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.large_miscell_fee_value}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Discount Text"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_miscell_discount_text"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "large_miscell_discount_text",
                  data?.large_miscell_discount_text
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.large_miscell_discount_text}
            />
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={6}
            lg={6}
            sx={{
              ...commonStyles.gridItem,
              ...customCommonStyles.marginBottomOne,
            }}
          >
            <CustomInput
              label="Miscellaneous Discount Value"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.labelStyleData,
              }}
              errorTextStyle={styles.errorTextStyle}
              registerName="large_miscell_discount_value"
              isRequired={false}
              submit={isSubmit}
              setData={(data) =>
                getData(
                  "large_miscell_discount_value",
                  data?.large_miscell_discount_value
                )
              }
              boxStyle={styles.inputStyle}
              defaultValue={feeConfigurationData?.large_miscell_discount_value}
            />
          </Grid> */}
        </Grid>
        <QpDialog
          open={showModal}
          closeModal={() => setShowModal(false)}
          styleData={commonStyles.dialogContainer}
        >
          <QpConfirmModal
            displayText="Are you sure you want to update fee configurations?"
            closeModal={() => setShowModal(false)}
            onConfirm={saveHandler}
          />
        </QpDialog>
      </Box>
    </>
  );
};

export default FeeConfiguration;
