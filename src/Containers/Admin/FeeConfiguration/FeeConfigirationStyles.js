import { BLACKISH, DARKER_BLUE, WHITE } from "../../../Constant/ColorConstant";

export const styles = {
  agencyInput: {
    height: "2rem",
  },
  updateButton: {
    backgroundColor: DARKER_BLUE,
    width: "8.563rem",
    height: "2.613rem",
    "&:hover": {
      backgroundColor: DARKER_BLUE,
    },
    "@media(max-width:600px)": {
      width: "5.1rem",
      height: "1.613rem",
    },
  },
  updateButtonText: {
    color: WHITE,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    textTransform: "none",
    "@media(max-width:600px)": {
      fontWeight: 600,
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  inputStyle: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    "@media(max-width:1024px)": {
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
  },
  labelStyleData: {
    marginBottom: "1rem",
    fontSize: "0.8rem",
    width: "13rem",
  },
  headingText: {
    fontWeight: 700,
    fontSize: "1rem",
    textAlign: "center",
    marginBottom: "0.5rem",
  },
  errorTextStyle: {
    fontSize: "0.6rem",
  },
  tdsBoxStyle: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: "0.2rem",
    // border: "1px solid blue",
  },
  noTdsText: { fontWeight: 600, fontSize: "0.8rem", color: BLACKISH },
  customRadioBoxStyle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 0,
  },
};
