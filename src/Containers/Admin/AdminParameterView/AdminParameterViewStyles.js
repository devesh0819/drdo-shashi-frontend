import { HeightTwoTone } from "@mui/icons-material";
import { padding } from "@mui/system";
import { BLACKISH } from "../../../Constant/ColorConstant";

const parameterViewStyles = {
  evidenceContainer: {
    // borderTop:"1px solid #808080",
    marginTop: "2%",
    padding: "1%",
  },

  evidenceImgContainer: {
    overflow: "",
    width: "60px",
    height: "60px",
    objectFit: "cover",
    border: "0.5px solid #D3D3D3",
    overflow: "hidden",
  },
  evidenceImage: {
    width: "100%",
    height: "auto",
  },
  commentBoxContainer: {
    // overflow:"scroll",
    height: "200px",
    marginTop: "1%",
    padding: "0rem 2rem 0 1rem",
    // paddingLeft:"0",
  },
  commentBoxesStyles: {
    padding: "1%",
    display: "flex",
    flexDirection: "column",
    border: "0.5px solid #D3D3D3 ",
    borderRadius: "4px",
    margin: "1% 0",
    width: "60vw",
    wordWrap: "break-word",
    // boxShadow:
    boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
  },
  commentBoxInnerBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "start",
    // padding:"0 1% "
    position: "relative",
  },
  customScrollBarHidden: {
    "&::-webkit-scrollbar": {
      width: "0.3rem",
      height: "0.3rem",
    },
    "&::-webkit-scrollbar-track": {
      //   boxShadow: "inset 0 0 0.375rem rgba(0,0,0,0.00)",
      //   webkitBoxShadow: "inset 0 0 0.375rem rgba(0,0,0,0.00)",
      width: "0.438rem",
      borderRadius: "0.75rem",
      // background: SILVER_GREY,
    },
    "&::-webkit-scrollbar-thumb": {
      //   backgroundColor: "rgba(0,0,0,.1)",
      backgroundColor: "transparent",
      borderRadius: "0.75rem",
      //   background: METALLIC_SILVER,
    },
  },
  commentTypeSelect: {
    width: "50%",
    height: "2.4rem",
    border: "0.8px solid #BDBDBD",
    borderRadius: "4px",
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.192rem",
    color: BLACKISH,
    marginTop: "0.738rem",
    "@media(max-width:600px)": {
      height: "1.8rem",
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  marginOnePercent: {
    // marginLeft:"1%"
  },
  commentCategoryLabel: {
    fontSize: "0.938rem",
    color: "#797b7d",
    fontWeight: "600",
    padding: 0,
  },
  commentBoxDate: {
    fontWeight: 500,
    fontSize: "0.938rem",
    lineHeight: "1.25rem",
    color: BLACKISH,
    marginBottom: "0.438rem",
    "& .MuiFormLabel-asterisk": {
      color: "red",
    },
  },
  commentTypeColorBox: {
    display: "flex",
    justifyContent: "right",
    // width:"60%"
    position: "absolute",
    right: "0",
  },
};

export default parameterViewStyles;
