import React, { useState, useEffect, useRef } from "react";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import { ReactComponent as IconCircularDownload } from "../../../Assets/Images/iconCircularDownload.svg";
import { ReactComponent as IconPencil } from "../../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconMenuSearch } from "../../../Assets/Images/iconMenuSearch.svg";
import { Button, Box, Tooltip, Typography, Chip } from "@mui/material";
import {
  getAgencyTableListingAction,
  getAssessmentsListingAction,
} from "../../../Redux/Admin/ManageAssessments/manageAssessmentsActions";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import moment from "moment";
import { styles } from "../ManageAssessments/ManageAssessmentsStyles.js";
import {
  getAppStatus,
  getEnterpriceType,
} from "../../../Services/commonService";
import { getUserData, logout } from "../../../Services/localStorageService";
import { APP_STAGE, ROLES } from "../../../Constant/RoleConstant";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import {
  downloadAction,
  downloadReportAction,
} from "../../../Redux/AllApplications/allApplicationsActions";
import { ReactComponent as IconCircularDownloadDisable } from "../../../Assets/Images/iconCircularDisable.svg";
import ContentPasteSearchIcon from "@mui/icons-material/ContentPasteSearch";
import DownloadForOfflineOutlinedIcon from "@mui/icons-material/DownloadForOfflineOutlined";
import WorkspacePremiumIcon from "@mui/icons-material/WorkspacePremium";

import { ASSESSMENTS, MANAGE_ASSESSMENTS } from "../../../Routes/Routes";
import {
  assessmentStatus,
  CERTIFIED,
  typeOfEnterpriseArray,
  yesNo,
} from "../../../Constant/AppConstant";
import FilterComponent from "../../../Components/FilterComponent/FilterComponent";

const CompleteAssessments = () => {
  const navigate = useNavigate();
  const [applicantsListing, setApplicantsListing] = useState([]);
  const [dataRow, setDataRow] = useState([]);
  const [openFilterDialog, setOpenFilterDialog] = useState(false);
  const [noOfFiltersApplied, setNoOfFiltersApplied] = useState(0);
  const [alreadySelectedFilters, setAlreadySelectedFilters] = useState({});
  const [payloadState, setPayloadState] = useState();
  const userData = getUserData();
  const role = userData?.roles[0];
  const runOnce = useRef(false);
  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(ASSESSMENTS);
    });
  }, []);
  useEffect(() => {
    if (
      runOnce.current === false &&
      role !== ROLES.ADMIN &&
      role !== ROLES.DRDO &&
      role !== ROLES.LAB_ADMIN
    ) {
      logout(navigate);
    }
    return () => {
      runOnce.current = true;
    };
  }, [role]);
  const columnDefs = [
    { field: "Application Number" },
    { field: "Assessment Number" },
    { field: "Name of enterprise", ellipsisClass: true },
    { field: "Type of Enterprise" },
    { field: "Application Date" },
    { field: "Assessment Date" },
    // { field: "No. of days", minWidth: "5rem" },
    { field: "Assigned Agency", minWidth: "11rem" },
    // { field: "Assessment Type" },
    // { field: "Location" },
    { field: "Status", minWidth: "11rem" },
    { field: "Certification Level" },
    { field: "Feedback" },
    {
      field: "Assessors",
      renderColumn: (row) => {
        return (
          <>
            {row["Assessors"]?.map((assessor, index) => {
              return (
                <Chip
                  color={
                    row.lead_assessor?.id === assessor?.profile?.id
                      ? "success"
                      : "info"
                  }
                  size="small"
                  key={index}
                  index={index}
                  label={`${assessor?.profile?.first_name} ${assessor?.profile?.last_name}`}
                  sx={commonStyles.chipContainer}
                />
              );
            })}
          </>
        );
      },
    },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Box
            sx={{ ...styles.iconContainer, ...styles.completeIconConatienr }}
          >
            <Box className="icons" sx={commonStyles.displayNone}>
              <Button
                sx={commonStyles.iconButtonStyle}
                onClick={() =>
                  navigate(`/assessments/edit/${row.assessment_id}`)
                }
              >
                <Tooltip title="View" arrow>
                  <ContentPasteSearchIcon style={commonStyles.iconColor} />
                </Tooltip>
              </Button>
              <Button sx={commonStyles.iconButtonStyle}>
                <Tooltip title="Report" arrow>
                  <DownloadForOfflineOutlinedIcon
                    style={
                      row.status ===
                        APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                      row.status === APP_STAGE.FEEDBACK_SUBMITTED ||
                      row.status === APP_STAGE.ASSESSMENT_COMPLETED
                        ? commonStyles.iconColor
                        : commonStyles.greyIconColor
                    }
                    onClick={() =>
                      (row.status ===
                        APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                        row.status === APP_STAGE.FEEDBACK_SUBMITTED ||
                        row.status === APP_STAGE.ASSESSMENT_COMPLETED) &&
                      dispatch(
                        downloadReportAction(
                          row.application_id,
                          row.application_number,
                          dispatch,
                          "report"
                        )
                      )
                    }
                  />
                </Tooltip>
              </Button>
              <Button
                sx={{
                  ...commonStyles.iconButtonStyle,
                  ...customCommonStyles.marginRightOne,
                }}
              >
                <Tooltip title="Certificate" arrow>
                  <WorkspacePremiumIcon
                    style={
                      row.status ===
                        APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                      row.status === APP_STAGE.FEEDBACK_SUBMITTED ||
                      row.status === APP_STAGE.ASSESSMENT_COMPLETED
                        ? commonStyles.iconColor
                        : commonStyles.greyIconColor
                    }
                    onClick={() =>
                      (row.status ===
                        APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                        row.status === APP_STAGE.FEEDBACK_SUBMITTED ||
                        row.status === APP_STAGE.ASSESSMENT_COMPLETED) &&
                      dispatch(
                        downloadReportAction(
                          row.application_id,
                          row.application_number,
                          dispatch,
                          "certificate"
                        )
                      )
                    }
                  />
                </Tooltip>
              </Button>
            </Box>
          </Box>
        );
      },
    },
  ];

  const [pageNumber, setPageNumber] = useState(1);
  const dispatch = useDispatch();
  const applicantsListingData = useSelector(
    (state) => state.manageAssessments.assessmentsListingData
  );
  const agencyTableListingData = useSelector(
    (state) => state.manageAssessments.agencyTableListingData
  );

  useEffect(() => {
    if (
      role === ROLES.ADMIN ||
      role === ROLES.DRDO ||
      role === ROLES.LAB_ADMIN
    ) {
      setApplicantsListing(applicantsListingData);
    } else if (role === ROLES.AGENCY) {
      setApplicantsListing(agencyTableListingData);
    }
  }, [role, applicantsListingData, agencyTableListingData]);

  useEffect(() => {
    if (
      role === ROLES.ADMIN ||
      role === ROLES.DRDO ||
      role === ROLES.LAB_ADMIN
    ) {
      dispatch(
        getAssessmentsListingAction({
          start: pageNumber,
          certified: true,
          ...payloadState,
        })
      );
    } else if (role === ROLES.AGENCY) {
      dispatch(
        getAgencyTableListingAction({ start: pageNumber, certified: true })
      );
    }
  }, [pageNumber, dispatch, role, payloadState]);

  useEffect(() => {
    if (applicantsListing?.data?.assessments) {
      const rowData1 = applicantsListing?.data?.assessments
        ? applicantsListing?.data?.assessments?.map((item, index) => ({
            "Application Number": item?.application?.application_number,
            "Assessment Number": item?.assessment_number,
            "Name of enterprise": item?.application?.enterprise_name,
            "Type of Enterprise": getEnterpriceType(
              item?.application?.enterprice_type
            ),
            "Application Date":
              item?.application?.application_date &&
              moment(item?.application?.application_date).format("D MMM,YYYY"),
            "Assigned Agency": item?.agency?.title,
            "Assessment Date":
              item?.assessment_date &&
              moment(item?.assessment_date).format("D MMM,YYYY"),
            "Assessment Type": item?.assessment_type,
            "No. of days": item?.number_of_days,
            "Certification Level": item?.assessment_level,
            Assessors: item?.assessors,
            Status: getAppStatus(item?.progress),
            Feedback:
              item?.application?.application_stage ===
              APP_STAGE.FEEDBACK_SUBMITTED
                ? "Received"
                : "Not Received",
            Location: `${item?.application?.registered_district?.title},${item?.application?.registered_state?.title}`,
            status: item?.progress,
            application_id: item?.application?.application_uuid,
            lead_assessor: item?.lead_assessor,
            assessment_id: item?.uuid,
            application_number: item?.application?.application_number,
          }))
        : [];
      setDataRow(rowData1);
    }
  }, [applicantsListing?.data?.assessments]);

  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const filterOptions = [
    {
      filterId: 1,
      displayName: "Type of Enterprise",
      filterType: "Dropdown",
      key: "type_of_enterprise",
      dataArray: typeOfEnterpriseArray,
      labelId: "Type of Enterprise",
      label: "Enterprise Type",
      filterName: "enterpriseType",
    },
    {
      filterId: 2,
      displayName: "Assessment Status",
      filterType: "Dropdown",
      key: "assessment_status",
      dataArray: assessmentStatus,
      labelId: "Assessment Status",
      label: "Assessment Status",
      filterName: "assessmentStatus",
    },
    {
      filterId: 3,
      displayName: "Assessor Name",
      filterType: "Search",
      key: "assessor_name",
      labelId: "Assessor Name",
      label: "Assessor Name",
      filterName: "assessorName",
    },
    {
      filterId: 4,
      displayName: "Feedback",
      filterType: "Dropdown",
      key: "feedback_received",
      dataArray: yesNo,
      labelId: "Feedback",
      label: "Feedback",
      filterName: "feedback",
    },
    {
      filterId: 5,
      displayStartName: "Assessment Start Date",
      displayLastName: "Assessment End Date",
      filterType: "AssessmentDate",
      keysArray: ["assessment_start_date", "assessment_end_date"],
      filterName: "assessmentDate",
    },
    {
      filterId: 6,
      displayStartName: "Application Start Date",
      displayLastName: "Application End Date",
      filterType: "Date",
      keysArray: ["application_start_date", "application_end_date"],
      filterName: "applicationDate",
    },
  ];

  const saveHandler = (payload) => {
    dispatch(
      getAssessmentsListingAction({
        start: pageNumber,
        certified: true,
        ...payload,
      })
    );
    setPageNumber(1);
    setPayloadState(payload);
  };

  const exportButtonClick = () => {
    if (
      role === ROLES.ADMIN ||
      role === ROLES.DRDO ||
      role === ROLES.LAB_ADMIN
    ) {
      const url = `${process.env.REACT_APP_BASE_URL}/admin/assessments?certified=true&is_excel=1&excel_type=admin_assessments`;
      dispatch(
        downloadAction("CertifiedAssessments", dispatch, url, "doc", "xlsx")
      );
    } else if (role === ROLES.AGENCY) {
      const url = `${process.env.REACT_APP_BASE_URL}/agency/assessments?certified=true&is_excel=1&excel_type=agency_assessments`;
      dispatch(
        downloadAction("CertifiedAssessments", dispatch, url, "doc", "xlsx")
      );
    }
  };

  return (
    <>
      {/* {applicantsListing?.data?.total === 0 ? (
        <QpTypography
          displayText="No data available"
          styleData={commonStyles.noData}
        />
      ) : ( */}
      {/* {openFilterDialog && (
        <FilterComponent
          saveHandler={saveHandler}
          filterOptions={filterOptions}
          alreadySelectedFilters={alreadySelectedFilters}
          setOpenFilterDialog={setOpenFilterDialog}
          setNoOfFiltersApplied={setNoOfFiltersApplied}
          setAlreadySelectedFilters={setAlreadySelectedFilters}
        />
      )} */}
      <CustomTable
        columnDefs={columnDefs}
        rowData={dataRow}
        title="SAMAR Certified"
        hasPagination
        headerCellStyle={styles.headerCellStyle}
        tableBodyCellStyle={styles.tableBodyCellStyle}
        handlePagination={handlePagination}
        currentPage={pageNumber}
        totalValues={applicantsListing?.data?.total}
        styleData={commonStyles.tableHeight}
        hasFilter={
          role === ROLES.ADMIN ||
          role === ROLES.DRDO ||
          role === ROLES.LAB_ADMIN
            ? true
            : false
        }
        onClickFilter={() => setOpenFilterDialog(!openFilterDialog)}
        noOfFiltersApplied={noOfFiltersApplied}
        buttonsDivStyle={commonStyles.divFlexStyle}
        exportButtonClick={exportButtonClick}
        hasExport={true}
        saveHandler={saveHandler}
        filterOptions={filterOptions}
        alreadySelectedFilters={alreadySelectedFilters}
        setOpenFilterDialog={setOpenFilterDialog}
        setNoOfFiltersApplied={setNoOfFiltersApplied}
        setAlreadySelectedFilters={setAlreadySelectedFilters}
      />
      {/* )} */}
    </>
  );
};

export default CompleteAssessments;
