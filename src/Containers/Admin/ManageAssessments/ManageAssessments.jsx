import React, { useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Button, Box, Tooltip, Chip } from "@mui/material";
import moment from "moment";
import ContentPasteSearchIcon from "@mui/icons-material/ContentPasteSearch";
import CustomTable from "../../../Components/CustomTable/CustomTable";

import {
  getAgencyTableListingAction,
  getAssessmentsListingAction,
} from "../../../Redux/Admin/ManageAssessments/manageAssessmentsActions";
import {
  getAssessmentStatus,
  getEnterpriceType,
} from "../../../Services/commonService";
import { getUserData, logout } from "../../../Services/localStorageService";
import { APP_STAGE, ROLES } from "../../../Constant/RoleConstant";
import { commonStyles } from "../../../Styles/CommonStyles";
import { downloadAction } from "../../../Redux/AllApplications/allApplicationsActions";
import { MANAGE_ASSESSMENTS } from "../../../Routes/Routes";
import {
  assessmentStatus,
  typeOfEnterpriseArray,
  yesNo,
} from "../../../Constant/AppConstant";
import { styles } from "./ManageAssessmentsStyles.js";

const ManageAssessments = () => {
  const navigate = useNavigate();
  const [applicantsListing, setApplicantsListing] = useState([]);
  const [openFilterDialog, setOpenFilterDialog] = useState(false);
  const [noOfFiltersApplied, setNoOfFiltersApplied] = useState(0);
  const [alreadySelectedFilters, setAlreadySelectedFilters] = useState({});
  const [dataRow, setDataRow] = useState([]);
  const [payloadState, setPayloadState] = useState();
  const userData = getUserData();
  const role = userData?.roles[0];
  const runOnce = useRef(false);
  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(MANAGE_ASSESSMENTS);
    });
  }, []);
  useEffect(() => {
    if (
      runOnce.current === false &&
      role !== ROLES.ADMIN &&
      role !== ROLES.DRDO
    ) {
      logout(navigate);
    }
    return () => {
      runOnce.current = true;
    };
  }, [role]);
  const columnDefs = [
    {
      field: "Application Number",
    },
    { field: "Assessment Number" },
    { field: "Name of enterprise", ellipsisClass: true },
    { field: "Type of Enterprise" },
    { field: "Application Date" },
    { field: "Assessment Date" },
    // { field: "No. of days", minWidth: "5rem" },
    { field: "Assigned Agency", minWidth: "11rem" },
    {
      field: "Assessors",
      renderColumn: (row) => {
        return (
          <>
            {row["Assessors"]?.length === 0
              ? "NA"
              : row["Assessors"]?.map((assessor, index) => {
                  return (
                    <Chip
                      color={
                        row.lead_assessor?.id === assessor?.profile?.id
                          ? "success"
                          : "info"
                      }
                      size="small"
                      key={index}
                      index={index}
                      label={`${assessor?.profile?.first_name} ${assessor?.profile?.last_name}`}
                      sx={commonStyles.chipContainer}
                    />
                  );
                })}
          </>
        );
      },
    },
    // { field: "Assessment Type" },
    // { field: "Location" },
    { field: "Status", minWidth: "11rem" },
    { field: "Feedback" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Box sx={styles.iconContainer}>
            <Box className="icons" sx={commonStyles.displayNone}>
              <Button
                sx={commonStyles.iconButtonStyle}
                onClick={() => navigate(`/manage-assessments/edit/${row.id}`)}
              >
                <Tooltip title="View" arrow>
                  <ContentPasteSearchIcon style={commonStyles.iconColor} />
                </Tooltip>
              </Button>
            </Box>
          </Box>
        );
      },
    },
  ];

  const [pageNumber, setPageNumber] = useState(1);
  const dispatch = useDispatch();
  const applicantsListingData = useSelector(
    (state) => state.manageAssessments.assessmentsListingData
  );
  const agencyTableListingData = useSelector(
    (state) => state.manageAssessments.agencyTableListingData
  );

  useEffect(() => {
    if (role === ROLES.ADMIN || role === ROLES.DRDO) {
      setApplicantsListing(applicantsListingData);
    } else if (role === ROLES.AGENCY) {
      setApplicantsListing(agencyTableListingData);
    }
  }, [role, applicantsListingData, agencyTableListingData]);

  useEffect(() => {
    if (role === ROLES.ADMIN || role === ROLES.DRDO) {
      dispatch(
        getAssessmentsListingAction({ start: pageNumber, ...payloadState })
      );
    } else if (role === ROLES.AGENCY) {
      dispatch(getAgencyTableListingAction({ start: pageNumber }));
    }
  }, [pageNumber, dispatch, role, payloadState]);

  useEffect(() => {
    if (applicantsListing?.data?.assessments) {
      const rowData1 = applicantsListing?.data?.assessments
        ? applicantsListing?.data?.assessments?.map((item, index) => ({
            "Application Number": item?.application?.application_number,
            "Assessment Number": item?.assessment_number,
            "Name of enterprise": item?.application?.enterprise_name,
            "Type of Enterprise": getEnterpriceType(
              item?.application?.enterprice_type
            ),
            "Application Date":
              item?.application?.application_date &&
              moment(item?.application?.application_date).format("D MMM,YYYY"),
            "Assigned Agency": item?.agency?.title,
            "Assessment Date":
              item?.assessment_date &&
              moment(item?.assessment_date).format("D MMM,YYYY"),
            Assessors: item?.assessors,
            lead_assessor: item?.lead_assessor,
            "Assessment Type": item?.assessment_type,
            "No. of days": item?.number_of_days,
            Status: getAssessmentStatus(item),

            Feedback:
              item?.application?.application_stage ===
              APP_STAGE.FEEDBACK_SUBMITTED
                ? "Received"
                : "Not Received",
            status: item?.progress,
            id: item?.uuid,
            Location: `${item?.application?.registered_district?.title},${item?.application?.registered_state?.title}`,
          }))
        : [];
      setDataRow(rowData1);
    }
  }, [applicantsListing?.data?.assessments]);

  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const filterOptions = [
    {
      filterId: 1,
      displayName: "Type of Enterprise",
      filterType: "Dropdown",
      key: "type_of_enterprise",
      dataArray: typeOfEnterpriseArray,
      labelId: "Type of Enterprise",
      label: "Enterprise Type",
    },
    {
      filterId: 2,
      displayName: "Assessment Status",
      filterType: "Dropdown",
      key: "assessment_status",
      dataArray: assessmentStatus,
      labelId: "Assessment Status",
      label: "Status",
    },
  ];
  const getFilterOptions = () => {
    if (role === ROLES.ADMIN) {
      return [
        {
          filterId: 1,
          displayName: "Type of Enterprise",
          filterType: "Dropdown",
          key: "type_of_enterprise",
          dataArray: typeOfEnterpriseArray,
          labelId: "Type of Enterprise",
          label: "Enterprise Type",
          filterName: "enterpriseType",
        },
        {
          filterId: 2,
          displayName: "Assessment Status",
          filterType: "Dropdown",
          key: "assessment_status",
          dataArray: assessmentStatus,
          labelId: "Assessment Status",
          label: "Assessment Status",
          filterName: "assessmentStatus",
        },
        {
          filterId: 3,
          displayName: "Assessor Name",
          filterType: "Search",
          key: "assessor_name",
          labelId: "Assessor Name",
          label: "Assessor Name",
          filterName: "assessorName",
        },
        {
          filterId: 4,
          displayStartName: "Assessment Start Date",
          displayLastName: "Assessment End Date",
          filterType: "AssessmentDate",
          keysArray: ["assessment_start_date", "assessment_end_date"],
          filterName: "assessmentDate",
        },
        {
          filterId: 5,
          displayStartName: "Application Start Date",
          displayLastName: "Application End Date",
          filterType: "Date",
          keysArray: ["application_start_date", "application_end_date"],
          filterName: "applicationDate",
        },
      ];
    } else if (role === ROLES.DRDO) {
      return [
        {
          filterId: 1,
          displayName: "Type of Enterprise",
          filterType: "Dropdown",
          key: "type_of_enterprise",
          dataArray: typeOfEnterpriseArray,
          labelId: "Type of Enterprise",
          label: "Enterprise Type",
          filterName: "enterpriseType",
        },
        {
          filterId: 2,
          displayName: "Assessment Status",
          filterType: "Dropdown",
          key: "assessment_status",
          dataArray: assessmentStatus,
          labelId: "Assessment Status",
          label: "Status",
          filterName: "assessmentStatus",
        },
        {
          filterId: 3,
          displayName: "Assessor Name",
          filterType: "Search",
          key: "assessor_name",
          labelId: "Assessor Name",
          label: "Assessor Name",
          filterName: "assessorName",
        },
        {
          filterId: 4,
          displayName: "Feedback",
          filterType: "Dropdown",
          key: "feedback_received",
          dataArray: yesNo,
          labelId: "Feedback",
          label: "Feedback",
          filterName: "feedback",
        },
        {
          filterId: 5,
          displayStartName: "Assessment Start Date",
          displayLastName: "Assessment End Date",
          filterType: "AssessmentDate",
          keysArray: ["assessment_start_date", "assessment_end_date"],
          filterName: "assessmentDate",
        },
        {
          filterId: 6,
          displayStartName: "Application Start Date",
          displayLastName: "Application End Date",
          filterType: "Date",
          keysArray: ["application_start_date", "application_end_date"],
          filterName: "applicationDate",
        },
      ];
    }
  };

  const saveHandler = (payload) => {
    if (role === ROLES.ADMIN || role === ROLES.DRDO) {
      dispatch(getAssessmentsListingAction({ start: pageNumber, ...payload }));
    }
    setPageNumber(1);
    setPayloadState(payload);
  };

  const exportButtonClick = () => {
    if (role === ROLES.ADMIN || role === ROLES.DRDO) {
      const url = `${process.env.REACT_APP_BASE_URL}/admin/assessments?is_excel=1&excel_type=admin_assessments`;
      dispatch(
        downloadAction("ManageAssessments", dispatch, url, "doc", "xlsx")
      );
    } else if (role === ROLES.AGENCY) {
      const url = `${process.env.REACT_APP_BASE_URL}/agency/assessments?is_excel=1&excel_type=agency_assessments`;
      dispatch(
        downloadAction("CertifiedAssessments", dispatch, url, "doc", "xlsx")
      );
    }
  };

  return (
    <>
      {/* {applicantsListing?.data?.total === 0 ? (
        <QpTypography
          displayText="No data available"
          styleData={commonStyles.noData}
        />
      ) : ( */}
      <>
        {/* {openFilterDialog && (
          <FilterComponent
            saveHandler={saveHandler}
            filterOptions={getFilterOptions()}
            alreadySelectedFilters={alreadySelectedFilters}
            setOpenFilterDialog={setOpenFilterDialog}
            setNoOfFiltersApplied={setNoOfFiltersApplied}
            setAlreadySelectedFilters={setAlreadySelectedFilters}
          />
        )} */}
        <CustomTable
          columnDefs={columnDefs}
          rowData={dataRow}
          title="Manage Assessments"
          hasPagination
          headerCellStyle={styles.headerCellStyle}
          tableBodyCellStyle={styles.tableBodyCellStyle}
          handlePagination={handlePagination}
          currentPage={pageNumber}
          totalValues={applicantsListing?.data?.total}
          styleData={commonStyles.tableHeight}
          hasFilter={role === ROLES.ADMIN || role === ROLES.DRDO ? true : false}
          onClickFilter={() => setOpenFilterDialog(!openFilterDialog)}
          noOfFiltersApplied={noOfFiltersApplied}
          buttonsDivStyle={commonStyles.divFlexStyle}
          exportButtonClick={exportButtonClick}
          hasExport={true}
          saveHandler={saveHandler}
          filterOptions={getFilterOptions()}
          alreadySelectedFilters={alreadySelectedFilters}
          setOpenFilterDialog={setOpenFilterDialog}
          setNoOfFiltersApplied={setNoOfFiltersApplied}
          setAlreadySelectedFilters={setAlreadySelectedFilters}
        />
      </>
      {/* )} */}
    </>
  );
};

export default ManageAssessments;
