import React from "react";
import { Box } from "@mui/material";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import CustomRating from "../../../../Components/CustomRating/CustomRating";
import { commonStyles } from "../../../../Styles/CommonStyles";
import { styles } from "../ManageAssessmentsEdit/ManageAssessmentsEditStyles.js";

export default function FeedbackSurvey(props) {
  const { feedbackData, feedbackComment } = props;
  const getDefaultValue = (index, rating) => {
    return parseInt(rating);
    // if (index === 0) return parseInt(feedbackData?.assessor_professinalism);
    // else if (index === 1) return parseInt(feedbackData?.assessor_knowledge);
    // else if (index === 2) return parseInt(feedbackData?.overall_process);
    // else return parseInt(feedbackData?.assessment_usefulness);
  };
  return (
    <>
      <Box>
        <Box
          sx={{
            ...commonStyles.withNumberContainer,
            ...commonStyles.feedbackSurveyDiv,
          }}
        >
          <Box sx={commonStyles.feedbackBlankDiv}></Box>
          <Box sx={commonStyles.numbers}>
            <QpTypography displayText={1} />
            <QpTypography displayText={2} />
            <QpTypography displayText={3} />
            <QpTypography displayText={4} />
          </Box>
        </Box>

        {feedbackData &&
          feedbackData?.map((ques, index) => {
            return (
              <>
                <Box sx={styles.questionRating}>
                  <QpTypography
                    displayText={`${index + 1}. ${ques?.title}`}
                    styleData={styles.questionContainer}
                  />
                  <CustomRating
                    defaultValue={parseInt(ques?.rating)}
                    readOnly={true}
                  />
                </Box>
              </>
            );
          })}
      </Box>
      {feedbackComment?.length > 0 && (
        <>
          <QpTypography
            displayText="Comment"
            styleData={commonStyles.inputLabel}
          />
          <QpTypography
            displayText={feedbackComment}
            styleData={commonStyles.commentText}
            // styleData={commonStyles.inputLabel}
          />
        </>
      )}
    </>
  );
}
