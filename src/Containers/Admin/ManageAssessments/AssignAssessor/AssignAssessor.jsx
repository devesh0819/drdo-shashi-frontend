import React, { useState } from "react";
import {
  Autocomplete,
  Box,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { styles } from "../ManageAssessmentsEdit/ManageAssessmentsEditStyles.js";
import { useLocation } from "react-router-dom";
import QpInputLabel from "../../../../Components/QpInputLabel/QpInputLabel.jsx";
import { commonStyles } from "../../../../Styles/CommonStyles.js";

export default function AssignAssessor(props) {
  const handleChange = (event) => {
    props.setValue(event.target.value);
  };

  return (
    <Box>
      <Box sx={styles.greyContainer}>
        <QpInputLabel
          displayText={"Select Assessors"}
          id="members"
          name="members"
          styleData={commonStyles.inputLabel}
        />
        <Autocomplete
          multiple
          filterSelectedOptions
          id="tags-outlined"
          options={props.data || []}
          onChange={(event, newValue) => {
            props.setSelectedAssessors(newValue.map((option) => option));
            props.setSelectedAssessorsIds(
              newValue.map((option) => option.id || option)
            );
          }}
          defaultValue={props.defaultAssessorsArray}
          getOptionLabel={(option) => option.title}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="outlined"
              style={commonStyles.dropdownTextField}
            />
          )}
          readOnly={props.readOnly}
          disabled={props.readOnly}
        />
        <Typography sx={styles.heading}>Select Lead Assessor</Typography>
        <Select
          labelId="Select Lead Assessor"
          id="Select Lead Assessor"
          // disabled={props.readOnly}
          disabled={props.readOnly}
          value={props.value}
          MenuProps={{
            sx: commonStyles.menuProps,
          }}
          displayEmpty
          onChange={handleChange}
          sx={styles.select}
          renderValue={(val) => {
            if (!val) {
              return (
                <Typography sx={commonStyles.placeHolderColor}>
                  Select{" "}
                </Typography>
              );
            }
            return <Typography>{val?.title}</Typography>;
          }}
          readOnly={props?.readOnly}
        >
          {props?.selectArray?.map((item, index) => (
            <MenuItem value={item} key={index} sx={styles.dropdownText}>
              {item.title}
            </MenuItem>
          ))}
        </Select>
      </Box>
    </Box>
  );
}
