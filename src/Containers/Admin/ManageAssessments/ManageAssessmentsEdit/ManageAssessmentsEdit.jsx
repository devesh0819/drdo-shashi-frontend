import React, { useState, useEffect } from "react";
import {
  Box,
  Button,
  Typography,
  Tab,
  Select,
  MenuItem,
  InputBase,
} from "@mui/material";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import { ReactComponent as IconCircularBack } from "../../../../Assets/Images/iconCircularBack.svg";
import { styles } from "./ManageAssessmentsEditStyles";
import QpButton from "../../../../Components/QpButton/QpButton";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { ASSESSMENTS, MANAGE_ASSESSMENTS } from "../../../../Routes/Routes";
import { useDispatch, useSelector } from "react-redux";
import {
  getAgencyListingAction,
  getApplicantDetailData,
  getAssessorsListAction,
  getFeedbackDataAction,
  updateAssignedAgency,
  updateAssignedAssessors,
} from "../../../../Redux/Admin/ManageAssessments/manageAssessmentsActions";
import { showToast } from "../../../../Components/Toast/Toast";
import Row from "../../../../Components/Row/Row";
import AssessmentApplicationView from "../AssessmentApplicationView";
import {
  getAgencyApplicationDetail,
  getAgencyApplicationDetailAction,
  getAssessmentApplicationDetail,
  getAssessmentApplicationDetailAction,
  getAdminAssessmentParameterAction,
  completeAdminAssessmentAction,
  setAdminTabAction,
} from "../../../../Redux/Admin/AdminAllApplications/adminAllApplicationAction";
import { useRef } from "react";
// import RenderQuestionairre from "../../../Admin/AllApplications/AdminApplicationTabView/RenderQuestionairre/RenderQuestionairre";
import { getUserData } from "../../../../Services/localStorageService";
import { APP_STAGE, ROLES } from "../../../../Constant/RoleConstant";
import AssignAssessor from "../AssignAssessor/AssignAssessor";
import * as _ from "lodash";
import ParameterListCard from "../../../../Containers/LeadAssessor/ParameterListCard/ParameterListCard";
import tabContainerStyles from "../../../LeadAssessor/ApplicationDetail/ApplicationDetalStyles";
import {
  feedbackQuestions,
  TITLE_LENGTH,
} from "../../../../Constant/AppConstant";
import QpTypography from "../../../../Components/QpTypography/QpTypography";
import CustomRating from "../../../../Components/CustomRating/CustomRating";
import {
  commonStyles,
  customCommonStyles,
} from "../../../../Styles/CommonStyles";
import FeedbackSurvey from "../FeedbackSurvey/FeedbackSurvey";
import QpDialog from "../../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../../Components/QpConfirmModal/QpConfirmModal";
import AssignAgency from "../../AllApplications/AdminApplicationTabView/AssignAgency/AssignAgency";
import CustomApplicatonView from "../../../../Components/CustomApplicationView/CustomApplicationView";
import AssessmentDetailAccordian from "./AssessmentDetailAccordian/AssessmentDetailAccordian";
import qciLogo from "../../../../Assets/Images/quci-logo.png";
import samarLogo from "../../../../Assets/Images/samar_logo.jpg";
import { getFeedbackConfigurationAction } from "../../../../Redux/Admin/FeedbackConfiguration/feedbackConfigurationActions";

// const RenderQuestionairre = (props) => {
//   const handleChange = (event) => {
//     props.setValue(event.target.value);
//   };
//   return (
//     <Box>
//       <Box sx={styles.greyContainer}>
//         <Typography sx={styles.italicText}>
//           You can change assigned agency at any time
//         </Typography>
//         <Typography sx={styles.heading}>Agency Name</Typography>
//         <Select
//           labelId="Agency Name"
//           id="Agency Name"
//           value={props.value}
//           MenuProps={{
//             sx: {
//               maxHeight: "12rem",
//               "@media(max-width:600px)": {
//                 maxHeight: "9rem",
//               },
//             },
//           }}
//           onChange={handleChange}
//           sx={styles.select}
//         >
//           {props.selectArray.map((item, index) => (
//             <MenuItem value={item.id} key={index} sx={styles.dropdownText}>
//               {item.title}
//             </MenuItem>
//           ))}
//         </Select>
//       </Box>
//     </Box>
//   );
// };

// const ApplicantDetail = (props) => {
//   return (
//     <>
//       <Row
//         label="Name of Entrepreneur"
//         value={props.data?.application?.entrepreneur_name}
//         applicantDetail
//       />
//       <Row
//         label="Name of Enterprise"
//         value={props.data?.application?.enterprise_name}
//         applicantDetail
//       />
//     </>
//   );
// };

const ManageAssessmentsEdit = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const runOnce = useRef(false);
  const { id } = useParams();
  const [value, setValue] = useState("1");
  const [applicationDetailObj, setApplicationDetailObj] = useState([]);
  const navigate = useNavigate();
  const [selectedAgency, setSelectedAgency] = useState("");
  const [selectedLeadAssessor, setSelectedLeadAssessor] = useState({});
  const [selectedAssessors, setSelectedAssessors] = useState([]);
  const [selectedAssessorsIds, setSelectedAssessorsIds] = useState([]);
  const [defaultAssessorsIds, setDefaultAssessorsIds] = useState([]);
  const [defaultLeadAssessor, setDefaultLeadAssessor] = useState({});
  const [isCompleteAssessmentShow, setShowCompleteAssessment] = useState(false);
  const [feedbackConfigurationData, setFeedbackConfigurationData] = useState(
    []
  );
  // const [defaultAssessorsArray, setDefaultAssessorsArray] = useState([]);

  const participantsArrayDummy = [
    {
      name: "Ritika Madaan",
      designation: "Manager",
    },
    {
      name: "Ritika Madaan Madaan  ",
      designation: "Software developer ",
    },
    {
      name: "Vibha Mishra",
      designation: "Manager",
    },
    {
      name: "Priya Singh",
      designation: "Developer",
    },
    {
      name: "Muskan Gupta",
      designation: "HROPS",
    },
    {
      name: "Vishesh Thakral",
      designation: "Junior Associate Software",
    },
    {
      name: "Ritika Madaan",
      designation: "Manager",
    },
    {
      name: "Ritika Madaan",
      designation: "Manager",
    },
  ];

  const commentsArrayDummy = [
    {
      comment: "Milk",
      last_updated_at: 1670490106120,
      comment_author: "Ritika Gupta",
    },
    {
      comment: "Milk",
      last_updated_at: 1670490106120,
      comment_author: "Ritika Gupta",
    },
    {
      comment: "Milk",
      last_updated_at: 1670490106120,
      comment_author: "Ritika Gupta",
    },
    {
      comment: "Milk",
      last_updated_at: 1670490106120,
      comment_author: "Ritika Gupta",
    },
  ];

  const siteTourArrayDummy = [
    {
      image: qciLogo,
      comment:
        "Writing coment here Writing coment here Writing coment here Writing coment here Writing coment here Writing coment here",
    },
    {
      image: samarLogo,
      comment: "Writing coment here Writing coment here ",
    },
    {
      image: qciLogo,
      comment: "Writing coment here Writing coment here Writing coment here",
    },
    {
      image: samarLogo,
      comment:
        "Writing coment here Writing coment here Writing coment here Writing coment here",
    },
  ];

  const userData = getUserData();
  const role = userData?.roles[0];
  const agencyListingData = useSelector(
    (state) => state.manageAssessments.agencyListingData
  );
  const assessorListData = useSelector(
    (state) => state.manageAssessments.assessorListData
  );

  const applicantDetailData = useSelector(
    (state) => state.manageAssessments.applicantDetailData
  );
  const assessmentApplicationDetail = useSelector(
    (state) => state.adminAllApplications.assessmentApplicationDetail
  );
  const agencyApplicationDetail = useSelector(
    (state) => state.adminAllApplications.agencyApplicationDetail
  );
  const adminAssessmentParameters = useSelector(
    (state) => state.adminAllApplications.adminAssessmentParameter
  );

  const currentTabAdmin = useSelector(
    (state) => state.adminAllApplications.currentTabAdmin
  );

  useEffect(() => {
    dispatch(setAdminTabAction(currentTabAdmin));
    // setValue(currentTabAdmin)
  }, []);

  useEffect(() => {
    if (
      applicationDetailObj?.assessors &&
      applicationDetailObj?.lead_assessor?.id
    ) {
      setDefaultAssessorsIds(
        applicationDetailObj?.assessors?.map((assessor) => assessor.assessor_id)
      );
      setDefaultLeadAssessor(applicationDetailObj?.lead_assessor?.id);
    }
  }, [
    applicationDetailObj?.assessors,
    applicationDetailObj?.lead_assessor?.id,
  ]);

  useEffect(() => {
    if ((assessorListData, defaultLeadAssessor)) {
      setSelectedLeadAssessor(
        _.find(assessorListData, { id: defaultLeadAssessor })
      );

      setSelectedAssessors(
        defaultAssessorsIds?.map((id) => ({
          id: id,
          title: _.find(assessorListData, { id: id })?.title,
        }))
      );
      setSelectedAssessorsIds(defaultAssessorsIds);
    }
  }, [assessorListData, defaultLeadAssessor]);

  useEffect(() => {
    if (assessmentApplicationDetail) {
      setApplicationDetailObj(assessmentApplicationDetail);
    }
    // else if (role === ROLES.AGENCY) {
    //   setApplicationDetailObj(agencyApplicationDetail);
    // }
  }, [assessmentApplicationDetail]);

  useEffect(() => {
    if (applicationDetailObj?.agency?.title) {
      setSelectedAgency(applicationDetailObj?.agency);
    }
  }, [applicationDetailObj]);

  useEffect(() => {
    if (id && runOnce.current === false) {
      if (
        role === ROLES.ADMIN ||
        role === ROLES.DRDO ||
        role === ROLES.LAB_ADMIN
      ) {
        dispatch(getAssessmentApplicationDetailAction(id));
        // dispatch(getAdminAssessmentParameterAction(id));
      } else if (role === ROLES.AGENCY) {
        dispatch(getAgencyApplicationDetailAction(id));
      }
    }
    return () => {
      runOnce.current = true;
      dispatch(getAssessmentApplicationDetail({}));
      dispatch(getAgencyApplicationDetail({}));
    };
  }, [id]);

  useEffect(() => {
    if (
      applicationDetailObj?.agency_id &&
      applicationDetailObj?.application?.registered_district?.id
    ) {
      dispatch(
        getAssessorsListAction(
          applicationDetailObj?.agency_id,
          applicationDetailObj?.application?.registered_district?.id
        )
      );
    }
    // dispatch(getApplicantDetailData(id));
  }, [
    dispatch,
    applicationDetailObj?.agency_id,
    applicationDetailObj?.application?.registered_district?.id,
  ]);

  useEffect(() => {
    if (applicationDetailObj?.application?.feedback?.feedback_payload) {
      let feedbackJson = JSON.parse(
        applicationDetailObj?.application?.feedback?.feedback_payload
      );
      if (Object.keys(feedbackJson)?.length > 0) {
        setFeedbackConfigurationData(Object.values(feedbackJson));
      }
    }
  }, [applicationDetailObj?.application?.feedback?.feedback_payload]);

  const handleChange = (e, newValue) => {
    // setValue(newValue);
    dispatch(setAdminTabAction(newValue));
  };

  useEffect(() => {
    // if (role === ROLES.AGENCY) {
    if (selectedAssessorsIds?.includes(selectedLeadAssessor?.id) === false) {
      setSelectedLeadAssessor("");
    }
    // }
  }, [selectedLeadAssessor, selectedAssessorsIds, role]);

  const updateHandler = () => {
    // if (role === ROLES.ADMIN || role === ROLES.DRDO) {
    //   if (!selectedAgency) {
    //     showToast("Please select an agency", "error");
    //     return;
    //   }
    //   dispatch(
    //     updateAssignedAgency(
    //       {
    //         uuid: id,
    //         body: {
    //           agency_id: selectedAgency?.user_id || selectedAgency?.id,
    //         },
    //       },
    //       navigate
    //     )
    //   );
    //   // dispatch(setAdminTabAction("1"));
    // } else if (role === ROLES.AGENCY) {
    if (selectedAssessors?.length < 1) {
      showToast("Please select atleast one assessor", "error");
      return;
    } else if (selectedAssessors?.length > 0 && !selectedLeadAssessor) {
      showToast("Please select Lead Assessor", "error");
      return;
    }
    dispatch(
      updateAssignedAssessors(
        {
          uuid: id,
          body: {
            assessor_ids: selectedAssessorsIds,
            lead_assessor_id: selectedLeadAssessor?.id,
          },
        },
        navigate
      )
    );
    // }
  };

  const handleCompleteAssessment = () => {
    // do completet assessment api work
    let assessment_id = id;
    dispatch(completeAdminAssessmentAction(assessment_id));
    // setValue("1");
    // dispatch(setAdminTabAction("1"));
  };

  const handleBackButtonClick = () => {
    if (location.pathname.includes("manage-assessments")) {
      navigate(MANAGE_ASSESSMENTS);
    } else {
      navigate(ASSESSMENTS);
    }
    dispatch(setAdminTabAction("1"));
    // navigate(MANAGE_ASSESSMENTS);
  };

  return (
    <>
      <Box sx={styles.outerContainer}>
        <Box
          sx={{
            ...styles.headerOuterContainer,
            ...commonStyles.topHeaderHeight,
          }}
        >
          <Box sx={styles.topHeader}>
            <Button
              disableRipple
              sx={styles.backButton}
              onClick={handleBackButtonClick}
              className="backButton"
            >
              <IconCircularBack />
            </Button>
            <Typography
              sx={{
                ...styles.titleText,
                ...(applicationDetailObj?.application?.enterprise_name?.length >
                  TITLE_LENGTH && styles.titleTextEllipsis),
              }}
            >
              {applicationDetailObj?.application?.enterprise_name}
            </Typography>
            {(role === ROLES?.ADMIN || role === ROLES?.DRDO) &&
            assessmentApplicationDetail?.progress !==
              APP_STAGE.PAREMETER_ALLOCATED &&
            assessmentApplicationDetail?.assessment_score &&
            assessmentApplicationDetail?.assessment_score > 0 ? (
              <Typography
                sx={{
                  ...styles.titleText,
                  ...customCommonStyles.marginLeftOne,
                }}
              >
                {`(Final Score - ${assessmentApplicationDetail?.assessment_score?.toFixed(
                  2
                )})`}
              </Typography>
            ) : (
              ""
            )}
          </Box>
          {currentTabAdmin === "2" &&
          (assessmentApplicationDetail?.progress ===
            APP_STAGE.ASSESSORS_ASSIGNED ||
            applicationDetailObj?.progress === APP_STAGE.ASSESSORS_ASSIGNED) &&
          role === ROLES.ADMIN ? (
            <QpButton
              styleData={styles.updateButton}
              displayText="Update"
              textStyle={styles.updateButtonText}
              onClick={updateHandler}
              // isDisable={value === "1"}
            />
          ) : (
            role === ROLES.ADMIN &&
            assessmentApplicationDetail?.progress ===
              APP_STAGE.ASSESSMENT_SUBMITTED &&
            currentTabAdmin === "3" && (
              <QpButton
                styleData={styles.updateButton}
                displayText="Complete"
                textStyle={styles.updateButtonText}
                // onClick={handleCompleteAssessment}
                onClick={() => setShowCompleteAssessment(true)}
                // isDisable={true}
              />
            )
          )}
        </Box>
        <Box sx={styles.lowerContainer}>
          <TabContext value={currentTabAdmin}>
            <Box sx={styles.tabContainer}>
              <TabList
                onChange={handleChange}
                sx={styles.tabList}
                className="tabList"
              >
                <Tab label="Applicant Detail" value="1" sx={styles.tabsText} />
                {/* {(role === ROLES.ADMIN || role === ROLES.DRDO) &&
                  assessmentApplicationDetail?.progress ===
                    APP_STAGE.ASSESSORS_ASSIGNED && (
                    <Tab
                      label="Assigned Agency"
                      value="2"
                      sx={styles.tabsText}
                    />
                  )} */}
                {/* {role === ROLES.AGENCY && ( */}
                {/* {applicationDetailObj?.progress ===
                  APP_STAGE.ASSESSORS_ASSIGNED && ( */}
                <Tab
                  label="Assigned Assessors"
                  value="2"
                  sx={styles.tabsText}
                />
                {/* )} */}
                {/* )} */}
                {(role === ROLES.ADMIN ||
                  role === ROLES.DRDO ||
                  role === ROLES.LAB_ADMIN) &&
                  (assessmentApplicationDetail?.progress ===
                    APP_STAGE.ASSESSMENT_SUBMITTED ||
                    assessmentApplicationDetail?.progress ===
                      APP_STAGE.PAREMETER_ALLOCATED) && (
                    <Tab
                      label="Assement Parameters"
                      value="3"
                      sx={styles.tabsText}
                    />
                  )}

                {assessmentApplicationDetail?.application?.application_stage ===
                  APP_STAGE.FEEDBACK_SUBMITTED && (
                  <Tab label="Feedback" value="4" sx={styles.tabsText} />
                )}
              </TabList>
            </Box>
            <TabPanel value="1" sx={styles.tabPanels}>
              <AssessmentApplicationView
                applicationDetail={applicationDetailObj?.application}
              />
            </TabPanel>
            <TabPanel value="2" sx={styles.tabPanels}>
              {/* {(role === ROLES.ADMIN || role === ROLES.DRDO) &&
                assessmentApplicationDetail?.progress ===
                  APP_STAGE.ASSESSORS_ASSIGNED && (
                  <AssignAgency
                    value={selectedAgency}
                    setValue={setSelectedAgency}
                    selectArray={agencyListingData?.data || []}
                    // disabled={true}
                    canChangeAnytimeText={true}
                    showMultiSelect={true}
                  />
                )} */}
              {/* {role === ROLES.AGENCY && ( */}
              <AssignAssessor
                data={assessorListData}
                selectArray={selectedAssessors}
                setSelectedAssessors={setSelectedAssessors}
                value={selectedLeadAssessor}
                setValue={setSelectedLeadAssessor}
                setSelectedAssessorsIds={setSelectedAssessorsIds}
                defaultAssessorsArray={selectedAssessors}
                readOnly={
                  applicationDetailObj?.progress !==
                    APP_STAGE.ASSESSORS_ASSIGNED ||
                  role === ROLES.DRDO ||
                  role === ROLES.LAB_ADMIN
                    ? true
                    : false
                }
              />
              {/* )} */}
            </TabPanel>
            {(applicationDetailObj?.progress ===
              APP_STAGE.ASSESSMENT_SUBMITTED ||
              applicationDetailObj?.progress ===
                APP_STAGE.PAREMETER_ALLOCATED) && (
              <TabPanel value="3" sx={styles.tabPanels}>
                <Box
                  sx={{
                    ...tabContainerStyles.fieldContainer,
                    ...tabContainerStyles.customScrollBar,
                    ...commonStyles.applicationViewContainer,
                  }}
                >
                  {/* {adminAssessmentParameters?.parameters?.disciplines?.map(
                    (d) => {
                      return d?.parameters?.map((p, p_index) => {
                        return (
                          <ParameterListCard
                            applicationDetail={assessmentApplicationDetail}
                            applicationParameterDetail={
                              adminAssessmentParameters
                            }
                            discipline={d}
                            parameter={p}
                            parameterID={p?.id}
                            isAdmin={true}
                            role={role}
                            // showAssignModal={(id)=>{handleShowAssignModal(id)}}
                          />
                        );
                      });
                    }
                  )} */}
                  <AssessmentDetailAccordian
                    adminAssessmentParameters={adminAssessmentParameters}
                    role={role}
                    assessmentApplicationDetail={assessmentApplicationDetail}
                  />
                </Box>
              </TabPanel>
            )}
            <TabPanel value="4" sx={styles.tabPanels}>
              <Box sx={styles.greyContainer}>
                <FeedbackSurvey
                  // feedbackData={
                  //   assessmentApplicationDetail?.application?.feedback
                  // }
                  feedbackData={feedbackConfigurationData}
                  feedbackComment={
                    applicationDetailObj?.application?.feedback
                      ?.feedback_comment
                  }
                />
              </Box>
            </TabPanel>
          </TabContext>
        </Box>
      </Box>
      <QpDialog
        open={isCompleteAssessmentShow}
        closeModal={() => setShowCompleteAssessment(false)}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to complete this assessment?"
          closeModal={() => setShowCompleteAssessment(false)}
          onConfirm={() => handleCompleteAssessment()}
        />
      </QpDialog>
    </>
  );
};

export default ManageAssessmentsEdit;
