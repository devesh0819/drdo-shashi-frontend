import React, { useEffect, useState, useRef } from "react";
import { Box, Typography } from "@mui/material";
import axios from "axios";
import CustomAccordian from "../../../../../Components/CustomAccordian/CustomAccordian";
import MeetingsDetailView from "../../../../../Components/CustomAccordian/MeetingsDetailView";
import ParameterListCard from "../../../../LeadAssessor/ParameterListCard/ParameterListCard";
import ReactImageViewer from "../../../../../Components/ReactImageViewer/ReactImageViewer";
import SiteTourCard from "../../../../../Components/CustomAccordian/SiteTourCard";
import { getItem } from "../../../../../Services/localStorageService";
import { showToast } from "../../../../../Components/Toast/Toast";
import { styles } from "../ManageAssessmentsEditStyles";

export const AssessmentDetailAccordian = (props) => {
  const {
    adminAssessmentParameters,
    assessmentApplicationDetail,
    role,
    handleShowAssignModal,
  } = props;
  const [activeAccordian, setActiveAccordian] = useState(-1);
  const [selectedImage, setSelectedImage] = useState(null);
  const [showLoader, setShowloader] = useState(false);
  const [openingImg, setOpeningImg] = useState("");
  const [closingImg, setClosingImg] = useState("");
  const [siteTourArray, setSiteTourArray] = useState([]);
  const [selectedImageLat, setImageLat] = useState(null);
  const [selectedImageLong, setImageLong] = useState(null);
  const [selectedImageTimestamp, setImageTimestamp] = useState(null);
  const [selectedImageDateTime, setImageDateTime] = useState(null);

  const [isOpen, setIsOpen] = useState(false);

  const runOnceOpening = useRef(false);
  const runOnceClosing = useRef(false);
  const runOnceSiteTour = useRef(false);

  const getImage = (
    path,
    isArray,
    lat,
    long,
    timestamp,
    isOpening,
    comment,
    fileDateTime
  ) => {
    let latTemp = lat || null;
    let longTemp = long || null;
    let timestampTemp = timestamp || null;
    // return () => {
    setShowloader(true);
    axios
      .get(path, {
        headers: {
          "Content-type": "image/jpeg",
          Authorization: `Bearer ${getItem("token")}`,
        },
        responseType: "blob",
      })
      .then((res) => {
        setTimeout(() => {
          setShowloader(false);
        }, 1000);
        if (res.status === 200) {
          const imageObject = {
            url: URL.createObjectURL(res.data),
            lat: latTemp,
            long: longTemp,
            timestamp: timestampTemp,
            fileDateTime: fileDateTime,
          };
          if (!isArray) {
            if (isOpening) {
              setOpeningImg(imageObject);
            } else {
              setClosingImg(imageObject);
            }
          } else {
            setSiteTourArray((prev) => [
              ...prev,
              {
                ...imageObject,
                caption: comment,
              },
            ]);
          }
        }
      })
      .catch((err) => {
        setShowloader(false);
        showToast("Something went wrong", "error");
      });
    // };
  };

  useEffect(() => {
    if (
      assessmentApplicationDetail?.site_details?.opening_meeting
        ?.opening_meeting_evidense?.length > 0 &&
      runOnceOpening.current === false
    ) {
      const img =
        assessmentApplicationDetail?.site_details?.opening_meeting
          ?.opening_meeting_evidense[0];
      getImage(
        img?.file,
        false,
        img?.latittude,
        img?.longitude,
        img?.last_updated_at,
        true,
        "",
        img?.file_date_time
      );
      return () => {
        runOnceOpening.current = true;
      };
    }
  }, [
    assessmentApplicationDetail?.site_details?.opening_meeting
      ?.opening_meeting_evidense,
  ]);

  useEffect(() => {
    if (
      assessmentApplicationDetail?.site_details?.closing_meeting
        ?.closing_meeting_evidense?.length > 0 &&
      runOnceClosing.current === false
    ) {
      const img =
        assessmentApplicationDetail?.site_details?.closing_meeting
          ?.closing_meeting_evidense[0];
      getImage(
        img?.file,
        false,
        img?.latittude,
        img?.longitude,
        img?.last_updated_at,
        false,
        "",
        img?.file_date_time
      );
      return () => {
        runOnceClosing.current = true;
      };
    }
  }, [
    assessmentApplicationDetail?.site_details?.closing_meeting
      ?.closing_meeting_evidense,
  ]);

  useEffect(() => {
    if (
      assessmentApplicationDetail?.site_details?.site_tour?.site_tour_evidenses
        ?.length > 0 &&
      runOnceSiteTour.current === false
    ) {
      assessmentApplicationDetail?.site_details?.site_tour?.site_tour_evidenses?.map(
        (evidense) =>
          getImage(
            evidense?.file,
            true,
            evidense?.latittude,
            evidense?.longitude,
            evidense?.last_updated_at,
            false,
            evidense?.caption,
            evidense?.file_date_time
          )
      );
      return () => {
        runOnceSiteTour.current = true;
      };
    }
  }, [
    assessmentApplicationDetail?.site_details?.site_tour?.site_tour_evidenses,
  ]);

  const handleImageClick = (img, lat, long, timestamp, fileDateTime) => {
    setSelectedImage(img);
    setIsOpen(true);
    setImageLat(lat);
    setImageLong(long);
    setImageTimestamp(timestamp); //this can be changed when we get time in unix timestamp directly from backend
    setImageDateTime(fileDateTime);
  };

  const handleImageClose = () => {
    setSelectedImage(null);
    setImageLat(null);
    setImageLong(null);
    setIsOpen(false);
    setImageDateTime(null);
  };

  return (
    <>
      <CustomAccordian
        title="Opening Meeting"
        index={0}
        setActiveAccordian={setActiveAccordian}
        activeAccordian={activeAccordian}
      />
      {activeAccordian === 0 && (
        <Box sx={styles.accordianContentConatiner}>
          <MeetingsDetailView
            participantsArray={
              assessmentApplicationDetail?.site_details?.opening_meeting
                ?.opening_meeting_participants
            }
            imgObj={openingImg}
            commentsArray={
              assessmentApplicationDetail?.site_details?.opening_meeting
                ?.opening_meeting_comments
            }
            handleImageClick={handleImageClick}
            showLoader={showLoader}
            type="Opening"
          />
        </Box>
      )}
      <CustomAccordian
        title="Site Tour"
        index={1}
        setActiveAccordian={setActiveAccordian}
        activeAccordian={activeAccordian}
      />
      {activeAccordian === 1 &&
        (assessmentApplicationDetail?.site_details?.site_tour
          ?.site_tour_evidenses?.length > 0 ? (
          <Box sx={styles.paddingOne}>
            {siteTourArray.length > 0
              ? siteTourArray?.map((item, index) => (
                  <SiteTourCard
                    key={item}
                    imgObj={item}
                    handleImageClick={handleImageClick}
                    showLoader={showLoader}
                  />
                ))
              : assessmentApplicationDetail?.site_details?.site_tour?.site_tour_evidenses?.map(
                  (evidense) => (
                    <SiteTourCard
                      key={evidense}
                      imgObj={evidense}
                      handleImageClick={handleImageClick}
                      showLoader={showLoader}
                    />
                  )
                )}
          </Box>
        ) : (
          <Box sx={styles.paddingOne}>
            <Box sx={styles.siteTourContentContainer}>
              <Typography sx={styles.noSiteEvidenseText}>
                No Site Tour Evidences
              </Typography>
            </Box>
          </Box>
        ))}
      <CustomAccordian
        title="Assessment Parameter"
        index={2}
        setActiveAccordian={setActiveAccordian}
        activeAccordian={activeAccordian}
      />
      {activeAccordian === 2 && (
        <Box sx={styles.accordianContentConatiner}>
          {adminAssessmentParameters?.parameters?.disciplines?.map((d) => {
            return d?.parameters?.map((p) => {
              return (
                <ParameterListCard
                  key={p}
                  applicationDetail={assessmentApplicationDetail}
                  applicationParameterDetail={adminAssessmentParameters}
                  discipline={d}
                  parameter={p}
                  parameterID={p?.id}
                  isAdmin={true}
                  role={role}
                  showAssignModal={(id) => {
                    handleShowAssignModal(id);
                  }}
                />
              );
            });
          })}
        </Box>
      )}
      <CustomAccordian
        title="Closing Meeting"
        index={3}
        setActiveAccordian={setActiveAccordian}
        activeAccordian={activeAccordian}
      />
      {activeAccordian === 3 && (
        <Box sx={styles.accordianContentConatiner}>
          <MeetingsDetailView
            participantsArray={
              assessmentApplicationDetail?.site_details?.closing_meeting
                ?.closing_meeting_participants
            }
            imgObj={closingImg}
            commentsArray={
              assessmentApplicationDetail?.site_details?.closing_meeting
                ?.closing_meeting_comments
            }
            handleImageClick={handleImageClick}
            showLoader={showLoader}
            type="Closing"
          />
        </Box>
      )}
      <ReactImageViewer
        imgs={selectedImage}
        isOpen={isOpen}
        onClose={handleImageClose}
        lat={selectedImageLat}
        long={selectedImageLong}
        timestamp={selectedImageTimestamp}
        fileDateTime={selectedImageDateTime}
      />
    </>
  );
};

export default AssessmentDetailAccordian;
