import * as colors from "../../../../Constant/ColorConstant";
import { commonStyles } from "../../../../Styles/CommonStyles";

export const styles = {
  outerContainer: {
    // minHeight: "calc(100vh - 15rem)",
    // maxHeight: "calc(100vh - 15rem)",
    backgroundColor: "white",
    overflow: "auto",
  },
  headerOuterContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: "2.063rem",
    paddingRight: "2.063rem",
    paddingTop: "0.625rem",
    paddingBottom: "0.625rem",
    backgroundColor: colors.WHITE,
    borderBottom: "0.063rem solid #EAEAEA",
    "@media(max-width:600px)": {
      paddingLeft: "1rem",
      paddingRight: "1rem",
    },
  },
  iconButton: {
    minWidth: "unset",
    padding: 0,
    paddingRight: "0.563rem",
    "&:hover": { backgroundColor: "transparent" },
    "@media(max-width:600px)": {
      "&.backButton svg": {
        height: "1rem",
        width: "1rem",
      },
    },
  },
  commentSelectType: {
    display: "flex",
    justifyContent: "start",
    alignItems: "center",
    margin: "1% 0",
  },
  addCommentStyle: {
    width: "50vw",
    display: "inline-block",
    border: "1px solid #BDBDBD",
    padding: "0 5px",
    borderRadius: "4px",
  },
  marginLeft: {
    marginLeft: "2%",
  },
  commentLabel: {
    marginTop: "1%",
    marginBottom: "0.2rem",
  },
  dateStyle: {
    padding: "0 2%",
  },
  topHeader: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  backButton: {
    minWidth: "unset",
    padding: 0,
    paddingRight: "0.563rem",
    "&:hover": { backgroundColor: "transparent" },
    "@media(max-width:600px)": {
      "&.backButton svg": {
        height: "1rem",
        width: "1rem",
      },
    },
  },
  titleText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1.125rem",
    lineHeight: "1.532rem",
    color: colors.BLACKISH,

    "@media(max-width:600px)": {
      fontWeight: 600,
      fontSize: "0.7rem",
      lineHeight: "0.8rem",
    },
  },
  titleTextEllipsis: {
    width: "20rem",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
  },
  updateButton: {
    backgroundColor: colors.DARKER_BLUE,
    width: "8.563rem",
    height: "2.613rem",
    "&:hover": {
      backgroundColor: colors.DARKER_BLUE,
    },
    "@media(max-width:600px)": {
      width: "5.1rem",
      height: "1.613rem",
    },
  },
  updateButtonText: {
    color: colors.WHITE,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    textTransform: "none",
    "@media(max-width:600px)": {
      fontWeight: 600,
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  lowerContainer: {
    backgroundColor: colors.WHITE,
    paddingX: "1rem",
    paddingY: "1rem",
    height: "calc(100vh - 15.5rem)",
    "@media(max-width:1200px)": {
      paddingY: "1rem",
    },
  },
  tabContainer: {
    backgroundColor: "#FAFAFA",
    border: "0.063rem solid #F0F1F2",
    borderRadius: "0.125rem",
    // paddingLeft: "1.125rem",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: "1rem",
  },
  tabsText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    textTransform: "none",
    "@media(max-width:600px)": {
      fontWeight: 400,
      fontSize: "0.68rem",
      lineHeight: "0.8rem",
    },
  },
  tabPanels: {
    marginY: "1.25rem",
    padding: 0,
  },
  greyContainer: {
    backgroundColor: "#FAFAFA",
    border: "0.063rem solid #F0F1F2",
    borderRadius: "0.125rem",
    paddingX: "1.563rem",
    paddingTop: "0.875rem",
    paddingBottom: "2.188rem",
  },
  italicText: {
    fontFamily: "Open Sans",
    fontStyle: "italic",
    fontWeight: 500,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    textTransform: "none",
    opacity: 0.3,
    "@media(max-width:600px)": {
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  heading: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.192rem",
    textTransform: "none",
    marginTop: "1.563rem",
    "@media(max-width:600px)": {
      fontSize: "0.68rem",
      lineHeight: "0.8rem",
    },
  },
  select: {
    width: "100%",
    height: "2.813rem",
    border: "0.063rem solid #A9A9A9",
    borderRadius: 0,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.192rem",
    color: colors.BLACKISH,
    marginTop: "0.738rem",
    "@media(max-width:600px)": {
      height: "1.8rem",
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
  tabList: {
    "@media(max-width:600px)": {
      "&.tabList div": {
        overflowX: "scroll",
        ...commonStyles.customScrollBar,
      },
    },
  },
  dropdownText: {
    "@media(max-width:600px)": {
      fontSize: "0.7rem",
      minHeight: "unset",
    },
  },
  questionRating: {
    display: "flex",
    alignItems: "center",
    marginBottom: "1rem",
  },
  questionContainer: {
    width: "20rem",
    fontSize: "0.875rem",
  },
  parameterListCardStyle: {
    button: {
      display: "inline-block",
      marginLeft: "1%",
      fontSize: "0.7rem",
      padding: "3px 4px ",
    },
    flexContainer: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
    inlineText: {
      display: "inline-block",
    },
  },
  dialogContainerWithSeventy: {
    "& .MuiDialog-container": {
      height: "80%",
      marginTop: "5rem",
    },
  },
  accordianContentConatiner: {
    padding: "1rem 0",
    margin: "0 1rem",
    border: "0.5px solid #D3D3D3 ",
    borderRadius: "4px",
    boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
  },
  paddingOne: {
    padding: "0 1rem",
  },
  siteTourContentContainer: {
    padding: "2rem 0",
    margin: "1rem 0",
    border: "0.5px solid #D3D3D3 ",
    borderRadius: "4px",
    boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
    // width: "100%",
    textAlign: "center",
  },
  noSiteEvidenseText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.875rem",
  },
};
