import React, { useEffect, useState, useRef } from "react";
import { Box, Typography, Button, Grid } from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import * as moment from "moment";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import Row from "../../../Components/Row/Row";
import { useDispatch, useSelector } from "react-redux";

import { styles } from "../../Applicant/Preview/PreviewStyles.js";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import { maskEmailField, maskFields } from "../../../Services/commonService";
import {
  getAdminApplicationDetail,
  getAdminApplicationDetailAction,
  getAssessmentApplicationDetailAction,
  getAssessmentApplicationDetail,
} from "../../../Redux/Admin/AdminAllApplications/adminAllApplicationAction";
import {
  enterpriseType,
  natureOfBusinessArray,
  natureOfEnterpriseArray,
  socialCategoryOptions,
  specialTypeOptions,
  typeOfEnterpriseArray,
  unitCategories,
} from "../../../Constant/AppConstant";
import * as _ from "lodash";
import dayjs from "dayjs";
import { getUserData } from "../../../Services/localStorageService";
import { ROLES } from "../../../Constant/RoleConstant";
import CustomApplicatonView from "../../../Components/CustomApplicationView/CustomApplicationView";

export default function AssessmentApplicationView(props) {
  const { applicationDetail } = props;
  const [customerTableRow, setCustomerTableRow] = useState([]);
  const [vendorTableRow, setVendorTableRow] = useState([]);
  const [productTableRow, setProductTableRow] = useState([]);
  const [applicationDetailObj, setApplicationDetailObj] = useState([]);

  const [rawMaterialTableRow, setRawMaterialTableRow] = useState([]);

  const [partsTableRow, setPartsTableRow] = useState([]);
  const [testingTableRow, setTestingTableRow] = useState([]);
  const [turnoverData, setTurnoverData] = useState([]);
  const [profitData, setProfitData] = useState([]);
  const [lossData, setLossData] = useState([]);
  const [outlayData, setOutlayData] = useState([]);
  const runOnce = useRef(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();
  const userData = getUserData();
  const role = userData?.roles[0];

  const assessmentApplicationDetail = useSelector(
    (state) => state.adminAllApplications.assessmentApplicationDetail
  );
  const agencyApplicationDetail = useSelector(
    (state) => state.adminAllApplications.agencyApplicationDetail
  );

  useEffect(() => {
    if (role === ROLES.ADMIN || role === ROLES.DRDO) {
      setApplicationDetailObj(assessmentApplicationDetail);
    } else if (role === ROLES.AGENCY) {
      setApplicationDetailObj(agencyApplicationDetail);
    }
  }, [role, assessmentApplicationDetail, agencyApplicationDetail]);
  // const applicationDetail = applicationDetailObj?.application;

  //   useEffect(() => {
  //     if (id && runOnce.current === false) {
  //       dispatch(getAssessmentApplicationDetailAction(id));
  //     }
  //     return () => {
  //       runOnce.current = true;
  //       dispatch(getAssessmentApplicationDetail({}));
  //     };
  //   }, [id]);

  const productTableColumn = [
    { field: "Name of the product" },
    { field: "Total Production Capacity Per Year" },
    { field: "Present Production Capacity Per Year" },
    { field: "Total Production Capacity Per Month" },
    { field: "Present Production Capacity Per Month" },
    { field: "Spare Capacity" },
    { field: "Date Of Commencement" },
    { field: "Cost Audit" },
    { field: "Cost Audit Details" },
    { field: "Fire Safety" },
    { field: "License Detail" },
    { field: "Government Regulatory" },
    { field: "Government Regulatory Details" },
    { field: "Defect Level/Acceptance Quality Level of the product" },
  ];

  const rawMaterialTableColumn = [
    { field: "Name of raw material" },
    { field: "Whether  imported?" },
    { field: "Percentage(%) of raw material imported" },
    { field: "Name of country" },
    { field: "Source of Procurement" },
  ];

  const partsTableColumn = [
    { field: "Name of part/components/sub-assembly/assembly" },
    { field: "Whether imported?" },
    { field: "Percentage(%) imported" },
    { field: "Name of country" },
    { field: "Source of Procurement" },
  ];

  const testingTableColumn = [
    { field: "Name of the test" },
    { field: "Whether external?" },
    { field: "Testing agency name" },
    { field: "Testing agency address" },
  ];

  const vendorTableColumn = [
    { field: "Name of the vendor" },
    { field: "Address" },
  ];

  const customerTableColumn = [
    { field: "Name of the customer" },
    { field: "Whether domestic or international?" },
    { field: "Address" },
  ];

  useEffect(() => {
    if (applicationDetail?.ownership?.key_domestic_customers) {
      const rowData = JSON.parse(
        applicationDetail?.ownership?.key_domestic_customers
      )?.map((customer) => {
        return {
          "Name of the customer": customer.customerName,
          "Whether domestic or international?":
            customer.customerType === "1" ? "Domestic" : "International",
          Address: customer.customerAddress,
        };
      });
      setCustomerTableRow(rowData);
    } else {
      setCustomerTableRow([]);
    }
  }, [applicationDetail?.ownership?.key_domestic_customers]);

  useEffect(() => {
    if (applicationDetail?.ownership?.key_vendors) {
      const rowData = JSON.parse(
        applicationDetail?.ownership?.key_vendors
      )?.map((vendor, index) => {
        return {
          "Name of the vendor": vendor.name,
          Address: vendor.address,
        };
      });
      setVendorTableRow(rowData);
    } else {
      setVendorTableRow([]);
    }
  }, [applicationDetail?.ownership?.key_vendors]);

  useEffect(() => {
    if (applicationDetail?.ownership?.testing_method) {
      const rowData = JSON.parse(
        applicationDetail?.ownership?.testing_method
      )?.map((test, index) => {
        return {
          "Name of the test": test.testName,
          "Whether external?":
            test.externalTesting === "1" ? "In-house" : "External",
          "Testing agency name": test.agencyTestName || "-",
          "Testing agency address": test.agencyTestAddress || "-",
        };
      });
      setTestingTableRow(rowData);
    } else {
      setTestingTableRow([]);
    }
  }, [applicationDetail?.ownership?.testing_method]);

  useEffect(() => {
    if (applicationDetail?.ownership?.boughtout_parts) {
      const rowData = JSON.parse(
        applicationDetail?.ownership?.boughtout_parts
      )?.map((part, index) => {
        return {
          "Name of part/components/sub-assembly/assembly": part.partsName,
          "Whether imported?": part.partsImported === "1" ? "Yes" : "No",
          "Percentage(%) imported": part.partsPercentageImported,
          "Name of country": part.partsCountryName,
          "Source of Procurement": part.partsSourceOfProcurement,
        };
      });
      setPartsTableRow(rowData);
    } else {
      setPartsTableRow([]);
    }
  }, [applicationDetail?.ownership?.boughtout_parts]);

  useEffect(() => {
    if (applicationDetail?.ownership?.key_raw_materials) {
      const rowData = JSON.parse(
        applicationDetail?.ownership?.key_raw_materials
      )?.map((material, index) => {
        return {
          "Name of raw material": material.rawMaterialName,
          "Whether  imported?":
            material.rawMaterialImported === "1" ? "Yes" : "No",
          "Percentage(%) of raw material imported":
            material.rawMaterialPercentageImported,
          "Name of country": material.rawMaterialCountryName,
          "Source of Procurement": material.rawMaterialSourceOfProcurement,
        };
      });
      setRawMaterialTableRow(rowData);
    } else {
      setRawMaterialTableRow([]);
    }
  }, [applicationDetail?.ownership?.key_raw_materials]);

  useEffect(() => {
    if (applicationDetail?.ownership?.product_manufactured) {
      const rowdata = JSON.parse(
        applicationDetail?.ownership?.product_manufactured
      )?.map((product, index) => {
        return {
          "Name of the product": product.productName,
          "Total Production Capacity Per Year":
            product.totalProductionCapacityPerYear,
          "Present Production Capacity Per Year":
            product.presentProductionCapacityPerYear,
          "Total Production Capacity Per Month":
            product.totalProductionCapacityPerMonth,
          "Present Production Capacity Per Month":
            product.presentProductionCapacityPerMonth,
          "Spare Capacity": product.spareCapacity,
          "Date Of Commencement": moment(
            new Date(product.dateOfCommencement)
          ).format("D MMM,YYYY"),
          "Cost Audit": product.isCostAudit === "1" ? "Yes" : "No",
          "Cost Audit Details": product.costAudit,
          "Fire Safety": product.fireSafety === "1" ? "Yes" : "No",
          "License Detail": product.licnenseDetail,
          "Government Regulatory":
            product.isGovernmentRegulatory === "1" ? "Yes" : "No",
          "Government Regulatory Details": product.governmentRegulatory,
          "Defect Level/Acceptance Quality Level of the product":
            product.defectLevel,
          id: index,
          targetName: "product_manufactured",
        };
      });
      setProductTableRow(rowdata);
    } else {
      setProductTableRow([]);
    }
  }, [applicationDetail?.ownership?.product_manufactured]);

  useEffect(() => {
    if (applicationDetail?.ownership?.financial_capital_outlay) {
      const outlay = JSON.parse(
        applicationDetail?.ownership?.financial_capital_outlay
      );
      setOutlayData(outlay);
    }
  }, [applicationDetail?.ownership?.financial_capital_outlay]);

  useEffect(() => {
    if (applicationDetail?.ownership?.turnover_3_year) {
      const turnover = JSON.parse(
        applicationDetail?.ownership?.turnover_3_year
      );
      setTurnoverData(turnover);
    }
  }, [applicationDetail?.ownership?.turnover_3_year]);

  useEffect(() => {
    if (applicationDetail?.ownership?.profit_3_year) {
      const profit = JSON.parse(applicationDetail?.ownership?.profit_3_year);
      setProfitData(profit);
    }
  }, [applicationDetail?.ownership?.profit_3_year]);

  useEffect(() => {
    if (applicationDetail?.ownership?.acc_loss_3_year) {
      const loss = JSON.parse(applicationDetail?.ownership?.acc_loss_3_year);
      setLossData(loss);
    }
  }, [applicationDetail?.ownership?.acc_loss_3_year]);

  return (
    <>
      <Box
        sx={{
          ...customCommonStyles.fieldContainer,
          ...commonStyles.applicationViewContainer,
        }}
      >
        <CustomApplicatonView
          applicationDetail={applicationDetailObj?.application}
        />
      </Box>
    </>
  );
}
