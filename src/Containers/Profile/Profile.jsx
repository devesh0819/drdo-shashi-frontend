import { Box, Button, Grid, Typography } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import CustomInput from "../../Components/CustomInput/CustomInput";
import QpButton from "../../Components/QpButton/QpButton";
import SaveNextButtons from "../../Components/SaveNextButtons/SaveNextButtons";
import { getUserData, logout } from "../../Services/localStorageService";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { styles } from "./ProfileStyles.js";
import { editProfileAction } from "../../Redux/Profile/profileActions";
import { ROLES } from "../../Constant/RoleConstant";

export default function Profile() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const userData = getUserData();
  const initialState = {
    first_name: userData.first_name,
    last_name: userData.last_name,
    email: userData.email,
    phone_number: userData.phone_number,
    username: userData.username,
  };
  const [data, setData] = useState(initialState);
  const [isSubmit, setIsSubmit] = useState(false);
  const [updatedProfileData, setUpdatedProfileData] = useState({});
  const profileDataRef = useRef(updatedProfileData);
  const profileData = useSelector((state) => state.profile.profileData);
  useEffect(() => {
    if (userData?.roles[0] !== ROLES.VENDOR) {
      logout(navigate);
    }
  }, [userData?.roles[0]]);
  useEffect(() => {
    const { first_name, last_name, email, phone_number } = updatedProfileData;

    if (first_name && last_name && phone_number && email) {
      onSubmit();
    }
  }, [updatedProfileData]);
  const updateState = (newState) => {
    profileDataRef.current = newState;
    setUpdatedProfileData(newState);
    setIsSubmit(false);
  };
  const getData = (key, value) => {
    updateState({
      ...profileDataRef.current,
      [key]: value,
    });
  };
  const saveHandler = () => {
    setIsSubmit(true);
  };

  useEffect(() => {
    if (profileData) {
      setData(profileData);
    }
  }, [profileData]);

  const onSubmit = () => {
    const { first_name, last_name, email, phone_number } = updatedProfileData;
    if (!first_name || !last_name || !phone_number || !email) {
      return;
    }
    let completeData = {
      first_name,
      last_name,
      email,
      phone_number,
    };
    dispatch(editProfileAction(completeData));
  };

  return (
    <>
      <Box sx={commonStyles.headerOuterContainer}>
        <Box sx={commonStyles.topBackButtonHeader}>
          {/* <Button disableRipple sx={commonStyles.backButton}>
            <IconCircularBack />
          </Button> */}
          <Typography style={commonStyles.titleBackText}>Profile</Typography>
        </Box>
        <QpButton
          displayText="Save"
          styleData={styles.updateButton}
          textStyle={styles.updateButtonText}
          onClick={saveHandler}
        />
      </Box>
      <Box
        sx={{
          ...customCommonStyles.fieldContainer,
          ...styles.containerDiv,
        }}
      >
        <Grid
          container
          // rowSpacing={}
          columnSpacing={"2rem"}
          sx={commonStyles.signUpContainer}
        >
          <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
            <CustomInput
              label="First Name"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.agencyLabel,
              }}
              registerName="first_name"
              isRequired={true}
              submit={isSubmit}
              setData={(data) => getData("first_name", data?.first_name)}
              boxStyle={customCommonStyles.marginBottomO}
              defaultValue={data.first_name}
            />
          </Grid>
          <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
            <CustomInput
              label="Last Name"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.agencyLabel,
              }}
              registerName="last_name"
              isRequired={true}
              submit={isSubmit}
              setData={(data) => getData("last_name", data?.last_name)}
              boxStyle={customCommonStyles.marginBottomO}
              defaultValue={data.last_name}
            />
          </Grid>
          <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
            <CustomInput
              label="Email"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.agencyLabel,
              }}
              registerName="email"
              isRequired={true}
              submit={isSubmit}
              setData={(data) => getData("email", data?.email)}
              boxStyle={customCommonStyles.marginBottomO}
              defaultValue={data.email}
            />
          </Grid>
          <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
            <CustomInput
              label="Mobile Number"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.agencyLabel,
              }}
              registerName="phone_number"
              isRequired={true}
              submit={isSubmit}
              setData={(data) => getData("phone_number", data?.phone_number)}
              boxStyle={customCommonStyles.marginBottomO}
              defaultValue={data.phone_number}
              maxLength={10}
            />
          </Grid>
          <Grid item xs={6} sm={6} md={6} lg={6} sx={commonStyles.gridItem}>
            <CustomInput
              label="Unique Id"
              inputStyleData={{
                ...commonStyles.inputStyle,
                ...styles.agencyInput,
                ...commonStyles.textInputStyle,
              }}
              labelStyleData={{
                ...commonStyles.inputLabel,
                ...styles.agencyLabel,
              }}
              // registerName="unique_id"
              isRequired={true}
              // submit={isSubmit}
              // setData={(data) => getData("unique_id", data?.unique_id)}
              boxStyle={customCommonStyles.marginBottomO}
              defaultValue={data.username}
              disabled={true}
            />
          </Grid>
        </Grid>
      </Box>
    </>
  );
}
