import { DARKER_BLUE, WHITE } from "../../Constant/ColorConstant";

export const styles = {
  agencyInput: {
    height: "2rem",
  },
  containerDiv: {
    paddingX: "7rem",
    paddingTop: "1.5rem",
    paddingBottom: "0rem",
    // paddingTop: "4rem",
  },
  updateButton: {
    backgroundColor: DARKER_BLUE,
    width: "8.563rem",
    height: "2.613rem",
    "&:hover": {
      backgroundColor: DARKER_BLUE,
    },
    "@media(max-width:600px)": {
      width: "5.1rem",
      height: "1.613rem",
    },
  },
  updateButtonText: {
    color: WHITE,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    textTransform: "none",
    "@media(max-width:600px)": {
      fontWeight: 600,
      fontSize: "0.6rem",
      lineHeight: "0.8rem",
    },
  },
};
