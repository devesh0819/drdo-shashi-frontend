import React, { useState } from "react";
import SideNav from "../../Components/SideNav/SideNav";
import { Button, Grid } from "@mui/material";
import { styles } from "./ContainerLayoutStyle";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";
import ArrowCircleLeftIcon from "@mui/icons-material/ArrowCircleLeft";

export default function ContainerLayout(props) {
  const { children, openSidebar, setOpenSidebar } = props;
  return (
    <Grid
      container
      columnSpacing={"0.563rem"}
      sx={{
        ...styles.gridOuterContainer,
      }}
    >
      {openSidebar && (
        <Grid
          item
          xs={12}
          md={openSidebar ? 3 : 0}
          sx={{
            ...styles.gridSidenavContainer,
          }}
        >
          <SideNav
            role={props.role}
            openSidebar={openSidebar}
            setOpenSidebar={setOpenSidebar}
          />
        </Grid>
      )}

      <Grid
        item
        xs={12}
        md={openSidebar ? 9 : 12}
        sx={styles.childrenContainer}
      >
        {children}
      </Grid>
    </Grid>
  );
}
