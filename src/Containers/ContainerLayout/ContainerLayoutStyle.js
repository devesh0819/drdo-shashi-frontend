import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  gridOuterContainer: {
    paddingX: "1.125rem",
    paddingY: "1.563rem",
    // minHeight: "calc(100vh - 14.188rem)",
    height: "calc(100vh - 7.125rem)",
  },
  childrenContainer: {
    maxHeight: "calc(100vh - 9.5rem)",
    minHeight: "calc(100vh - 9.5rem)",
    overflow: "auto",
    background: "white",
    ...commonStyles.customScrollBar,
  },
  gridSidenavContainer: {
    // maxHeight: "calc(100vh - 15rem)",
    // minHeight: "calc(100vh - 15rem)",
    "@media(max-width:900px)": {
      minHeight: "unset",
      overflow: "auto",
      ...commonStyles.customScrollBar,
    },
  },
};
