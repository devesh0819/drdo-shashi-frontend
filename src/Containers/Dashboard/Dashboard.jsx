import { Box, Button, Typography } from "@mui/material";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Barchart from "../../Components/Charts/Barchart";
import PieChart from "../../Components/Charts/PieChart";
import { UserData, StackedBarChartData } from "../../Constant/AppConstant";
import { getChartsData } from "../../Redux/Admin/Dashboard/dashboardActions";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { styles } from "./DashboardStyles";
import QpTypography from "../../Components/QpTypography/QpTypography";
import { DASHBOARD } from "../../Routes/Routes";
import { useNavigate } from "react-router-dom";
import { nFormatter } from "../../Services/commonService";

export default function Dashboard() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const chartsData = useSelector((state) => state.adminDashboard.chartsData);
  const [pieChartTitle, setPieChartTitle] = useState("Large");
  const labelsArray = ["Micro", "Small", "Medium", "Large"];
  const data1 = [12, 19, 3, 5, 2];
  const data2 = [10, 18, 3, 5, 2];
  const data3 = [18, 19, 3, 7, 2];
  const data4 = [5, 8, 3, 5, 4];

  useEffect(() => {
    dispatch(getChartsData());
  }, [dispatch]);
  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(DASHBOARD);
    });
  }, []);

  const [barChartData, setBarChartdata] = useState({
    labels: labelsArray,
    datasets: [
      {
        label: "No. of applications",
        data: [
          chartsData?.application_count?.micro,
          chartsData?.application_count?.small,
          chartsData?.application_count?.medium,
          chartsData?.application_count?.large,
        ],
        backgroundColor: ["#255491"],
        borderColor: "rgb(36,84,145)",
        borderWidth: 2,
      },
    ],
  });

  useEffect(() => {
    if (chartsData?.application_count) {
      setBarChartdata({
        labels: labelsArray,
        datasets: [
          {
            label: "No. of applications",
            data: [
              chartsData?.application_count?.micro,
              chartsData?.application_count?.small,
              chartsData?.application_count?.medium,
              chartsData?.application_count?.large,
            ],
            backgroundColor: ["#255491"],
            borderColor: "rgb(36,84,145)",
            borderWidth: 2,
          },
        ],
      });
    }
  }, [chartsData?.application_count]);
  const barChartOptions = {
    plugins: {
      title: {
        display: true,
        text: "No of Applications",
      },
      legend: {
        display: true,
        position: "bottom",
      },
    },
  };

  const [subsidisedBarChart, setSubsidisedBarChart] = useState({
    labels: labelsArray,
    datasets: [
      {
        label: "Subsidised",
        data: [
          chartsData?.subsidised_count?.micro?.subsidised,
          chartsData?.subsidised_count?.small?.subsidised,
          chartsData?.subsidised_count?.medium?.subsidised,
          chartsData?.subsidised_count?.large?.subsidised,
        ],
        backgroundColor: ["#255491"],
        borderColor: "rgb(36,84,145)",
        borderWidth: 2,
      },
      {
        label: "Non-Subsidised",
        data: [
          chartsData?.subsidised_count?.micro?.nonsubsidised,
          chartsData?.subsidised_count?.small?.nonsubsidised,
          chartsData?.subsidised_count?.medium?.nonsubsidised,
          chartsData?.subsidised_count?.large?.nonsubsidised,
        ],
        backgroundColor: ["rgb(236,125,49)"],
        borderColor: "rgb(236,125,49)",
        borderWidth: 2,
      },
    ],
  });

  useEffect(() => {
    if (chartsData?.subsidised_count) {
      setSubsidisedBarChart({
        labels: labelsArray,
        datasets: [
          {
            label: "Subsidised",
            data: [
              chartsData?.subsidised_count?.micro?.subsidised,
              chartsData?.subsidised_count?.small?.subsidised,
              chartsData?.subsidised_count?.medium?.subsidised,
              chartsData?.subsidised_count?.large?.subsidised,
            ],
            backgroundColor: ["#255491"],
            borderColor: "rgb(36,84,145)",
            borderWidth: 2,
          },
          {
            label: "Non-Subsidised",
            data: [
              chartsData?.subsidised_count?.micro?.nonsubsidised,
              chartsData?.subsidised_count?.small?.nonsubsidised,
              chartsData?.subsidised_count?.medium?.nonsubsidised,
              chartsData?.subsidised_count?.large?.nonsubsidised,
            ],
            backgroundColor: ["rgb(236,125,49)"],
            borderColor: "rgb(236,125,49)",
            borderWidth: 2,
          },
        ],
      });
    }
  }, [chartsData?.subsidised_count]);

  const subsidisedBarChartOptions = {
    plugins: {
      title: {
        display: true,
        text: "Category wise Subsidised & Non- Subsidised",
      },
      legend: {
        display: true,
        position: "bottom",
      },
    },
    // responsive: true,
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: true,
      },
    },
  };

  const [fieldAssessmentBarChart, setFieldAssessmentBarChart] = useState({
    labels: labelsArray,
    datasets: [
      {
        label: "Completed",
        data: [
          chartsData?.assessment_status?.micro?.completed,
          chartsData?.assessment_status?.small?.completed,
          chartsData?.assessment_status?.medium?.completed,
          chartsData?.assessment_status?.large?.completed,
        ],
        backgroundColor: ["#255491"],
        borderColor: "rgb(36,84,145)",
        borderWidth: 2,
      },
      {
        label: "Pending",
        data: [
          chartsData?.assessment_status?.micro?.pending,
          chartsData?.assessment_status?.small?.pending,
          chartsData?.assessment_status?.medium?.pending,
          chartsData?.assessment_status?.large?.pending,
        ],
        backgroundColor: ["rgb(236,125,49)"],
        borderColor: "rgb(236,125,49)",
        borderWidth: 2,
      },
    ],
  });

  useEffect(() => {
    if (chartsData?.assessment_status) {
      setFieldAssessmentBarChart({
        labels: labelsArray,
        datasets: [
          {
            label: "Completed",
            data: [
              chartsData?.assessment_status?.micro?.completed,
              chartsData?.assessment_status?.small?.completed,
              chartsData?.assessment_status?.medium?.completed,
              chartsData?.assessment_status?.large?.completed,
            ],
            backgroundColor: ["#255491"],
            borderColor: "rgb(36,84,145)",
            borderWidth: 2,
          },
          {
            label: "Pending",
            data: [
              chartsData?.assessment_status?.micro?.pending,
              chartsData?.assessment_status?.small?.pending,
              chartsData?.assessment_status?.medium?.pending,
              chartsData?.assessment_status?.large?.pending,
            ],
            backgroundColor: ["rgb(236,125,49)"],
            borderColor: "rgb(236,125,49)",
            borderWidth: 2,
          },
        ],
      });
    }
  }, [chartsData?.assessment_status]);

  const fieldAssessmentBarChartOptions = {
    plugins: {
      title: {
        display: true,
        text: "Assessment status",
      },
      legend: {
        display: true,
        position: "bottom",
      },
    },
    // responsive: true,
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: true,
      },
    },
  };

  const [pieChart, setPieChart] = useState({
    // labels: ["Bronze", "Diamond", "Gold", "Platinum", "Silver"],
    labels: ["Level-1", "Level-2", "Level-3", "Level-4", "Level-5"],

    datasets: [
      {
        label: "# of Votes",
        data: [
          chartsData?.certification_count?.large?.bronze,
          chartsData?.certification_count?.large?.silver,
          chartsData?.certification_count?.large?.gold,
          chartsData?.certification_count?.large?.platinum,
          chartsData?.certification_count?.large?.diamond,
        ],
        backgroundColor: [
          "rgb(68,114,196)",
          "rgb(236,125,49)",
          "rgb(165,165,165)",
          "rgb(254,191,0)",
          "rgb(91,154,213)",
        ],
        borderColor: [
          "rgb(255, 255, 255)",
          "rgb(255, 255, 255)",
          "rgb(255, 255, 255)",
          "rgb(255, 255, 255)",
          "rgb(255, 255, 255)",
        ],
        borderWidth: 1,
        radius: "70%",
      },
    ],
  });

  useEffect(() => {
    if (chartsData?.certification_count) {
      setPieChart({
        // labels: ["Bronze", "Diamond", "Gold", "Platinum", "Silver"],
        labels: ["Level-1", "Level-2", "Level-3", "Level-4", "Level-5"],
        datasets: [
          {
            label: "# of Votes",
            data: [
              chartsData?.certification_count?.large?.bronze,
              chartsData?.certification_count?.large?.silver,
              chartsData?.certification_count?.large?.gold,
              chartsData?.certification_count?.large?.platinum,
              chartsData?.certification_count?.large?.diamond,
            ],
            backgroundColor: [
              "rgb(68,114,196)",
              "rgb(236,125,49)",
              "rgb(165,165,165)",
              "rgb(254,191,0)",
              "rgb(91,154,213)",
            ],
            borderColor: [
              "rgb(255, 255, 255)",
              "rgb(255, 255, 255)",
              "rgb(255, 255, 255)",
              "rgb(255, 255, 255)",
              "rgb(255, 255, 255)",
            ],
            borderWidth: 1,
            radius: "70%",
          },
        ],
      });
    }
  }, [chartsData?.certification_count]);
  const pieChartOptions = {
    plugins: {
      title: {
        display: true,
        text: `Selected Category - ${pieChartTitle}`,
      },
      legend: {
        display: true,
        position: "bottom",
      },
    },
  };

  const InfoBlueBox = (props) => {
    return (
      <Box sx={styles.blueBox}>
        <Typography sx={styles.infoHeadingText}>{props.label}</Typography>
        <Typography sx={styles.infoValueText}>{props.value}</Typography>
      </Box>
    );
  };
  const CategoryContainer = (props) => {
    return (
      <Button sx={styles.categoryButton} onClick={props.onCategoryClick}>
        <Typography sx={styles.categorySubText}>{props.category}</Typography>
      </Button>
    );
  };

  const handleCategoryClick = (array, title) => {
    setPieChartTitle(title);
    setPieChart({
      // labels: ["Bronze", "Diamond", "Gold", "Platinum", "Silver"],
      labels: ["Level-1", "Level-2", "Level-3", "Level-4", "Level-5"],

      datasets: [
        {
          // label: "# of Votes",
          data: array,
          backgroundColor: [
            "rgb(68,114,196)",
            "rgb(236,125,49)",
            "rgb(165,165,165)",
            "rgb(254,191,0)",
            "rgb(91,154,213)",
          ],
          borderColor: [
            "rgb(255, 255, 255)",
            "rgb(255, 255, 255)",
            "rgb(255, 255, 255)",
            "rgb(255, 255, 255)",
            "rgb(255, 255, 255)",
          ],
          borderWidth: 1,
          radius: "70%",
        },
      ],
    });
  };

  return (
    <Box sx={styles.outerContainer}>
      <Box style={{ ...commonStyles.topHeader, ...commonStyles.displayStyle }}>
        <Typography style={commonStyles.titleText}>Dashboard</Typography>
      </Box>
      <Box sx={customCommonStyles.chartsContainer}>
        <Box sx={styles.topInfoContainer}>
          <Box sx={styles.topContainers}>
            <InfoBlueBox
              label="Amount Alloted by DRDO"
              value={nFormatter(
                chartsData?.amount_spent?.amount_Alloted
              )?.toLocaleString("en-IN")}
            />
            <InfoBlueBox
              label="Total Amount Spent"
              value={nFormatter(
                chartsData?.amount_spent?.total_amount_spent
              )?.toLocaleString("en-IN")}
            />
          </Box>
          <Box sx={styles.topContainers}>
            <InfoBlueBox
              label="Micro"
              value={nFormatter(
                chartsData?.amount_spent?.micro_amount_spent
              )?.toLocaleString("en-IN")}
            />
            <InfoBlueBox
              label="Small"
              value={nFormatter(
                chartsData?.amount_spent?.small_amount_spent
              )?.toLocaleString("en-IN")}
            />
          </Box>
        </Box>
        <Box sx={styles.upperContainer}>
          <Box sx={styles.upperBarchart}>
            <Barchart chartData={barChartData} options={barChartOptions} />
          </Box>
          <Box sx={styles.upperBarchart}>
            <Barchart
              chartData={subsidisedBarChart}
              options={subsidisedBarChartOptions}
            />
          </Box>
        </Box>
        <Box sx={styles.lowerContainer}>
          {/* <Box sx={styles.pieChartContainer}> */}
          <Box sx={styles.categoryOuterContainer}>
            <Typography sx={styles.categoryText}>Category</Typography>
            <Box sx={styles.categoryFlex}>
              <CategoryContainer
                category="Large"
                onCategoryClick={() =>
                  handleCategoryClick(
                    [
                      chartsData?.certification_count?.large?.bronze,
                      chartsData?.certification_count?.large?.silver,
                      chartsData?.certification_count?.large?.gold,
                      chartsData?.certification_count?.large?.platinum,
                      chartsData?.certification_count?.large?.diamond,
                    ],
                    "Large"
                  )
                }
              />
              <CategoryContainer
                category="Medium"
                onCategoryClick={() =>
                  handleCategoryClick(
                    [
                      chartsData?.certification_count?.medium?.bronze,
                      chartsData?.certification_count?.medium?.silver,
                      chartsData?.certification_count?.medium?.gold,
                      chartsData?.certification_count?.medium?.platinum,
                      chartsData?.certification_count?.medium?.diamond,
                    ],
                    "Medium"
                  )
                }
              />
              <CategoryContainer
                category="Micro"
                onCategoryClick={() =>
                  handleCategoryClick(
                    [
                      chartsData?.certification_count?.micro?.bronze,
                      chartsData?.certification_count?.micro?.silver,
                      chartsData?.certification_count?.micro?.gold,
                      chartsData?.certification_count?.micro?.platinum,
                      chartsData?.certification_count?.micro?.diamond,
                    ],
                    "Micro"
                  )
                }
              />
              <CategoryContainer
                category="Small"
                onCategoryClick={() =>
                  handleCategoryClick(
                    [
                      chartsData?.certification_count?.small?.bronze,
                      chartsData?.certification_count?.small?.silver,
                      chartsData?.certification_count?.small?.gold,
                      chartsData?.certification_count?.small?.platinum,
                      chartsData?.certification_count?.small?.diamond,
                    ],
                    "Small"
                  )
                }
              />
            </Box>
          </Box>
          <Box sx={styles.upperBarchart}>
            {pieChart?.datasets[0]?.data?.every((value) => value === 0) ? (
              <QpTypography
                displayText={`No data available for ${pieChartTitle}`}
                styleData={{
                  ...customCommonStyles.marginBottomOne,
                  ...styles.noChartData,
                }}
              />
            ) : (
              <PieChart chartData={pieChart} options={pieChartOptions} />
            )}
          </Box>
          {/* </Box> */}
          <Box sx={styles.lastChart}>
            <Barchart
              chartData={fieldAssessmentBarChart}
              options={fieldAssessmentBarChartOptions}
            />
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
