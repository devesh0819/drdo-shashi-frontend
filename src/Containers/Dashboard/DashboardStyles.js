import * as colors from "../../Constant/ColorConstant";
import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  outerContainer: {
    minHeight: "calc(100vh - 11rem)",
    maxHeight: "calc(100vh - 11rem)",
    backgroundColor: colors.WHITE,
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    overflowY: "auto",
    ...commonStyles.customScrollBar,
  },
  upperContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: "3rem",
    borderBottom: "0.063rem solid #EAEAEA",
    marginTop: "3rem",
    alignItems: "center",
    "@media(max-width:1100px)": {
      display: "flex",
      flexDirection: "column",
      alignItem: "flex-start",
      borderBottom: "none",
      paddingBottom: "0rem",
    },
  },
  upperBarchart: {
    width: "45%",
    "@media(max-width:1100px)": {
      width: "70%",
      marginBottom: "5rem",
      borderBottom: "0.063rem solid #EAEAEA",
    },
  },
  lastChart: {
    width: "45%",
    "@media(max-width:1100px)": {
      width: "70%",
      marginBottom: "5rem",
    },
  },
  lowerContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "3rem",
    paddingBottom: "3rem",
    borderBottom: "0.063rem solid #EAEAEA",
    // border: "1px solid red",
    alignItems: "center",
    "@media(max-width:1100px)": {
      display: "flex",
      flexDirection: "column",
      alignItem: "flex-start",
      marginTop: "0rem",
    },
  },
  topInfoContainer: {
    display: "flex",
    flexDirection: "row",
    padding: "2rem",
    borderBottom: "0.063rem solid #EAEAEA",
    "@media(max-width:700px)": {
      flexDirection: "column",
      alignItems: "center",
    },
  },
  topContainers: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "50%",
    "@media(max-width:600px)": {
      flexDirection: "column",
      alignItems: "center",
    },
  },
  blueBox: {
    backgroundColor: colors.DARKER_BLUE,
    padding: "1rem",
    paddingBottom: "2rem",
    margin: "0.2rem",
    width: "45%",
    textAlign: "center",
    borderRadius: "0.85rem",
    border: "0.063rem solid rgb(36,84,145)",
    "@media(max-width:1100px)": {
      //   marginBottom: "1rem",
      width: "45%",
      padding: "0.2rem",
    },
  },
  infoHeadingText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    color: colors.WHITE,
    "@media(max-width:1100px)": {
      fontSize: "0.575rem",
    },
  },
  infoValueText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "1.5rem",
    lineHeight: "1.188rem",
    color: colors.WHITE,
    marginTop: "0.6rem",
    "@media(max-width:1200px)": {
      fontSize: "0.9rem",
      marginTop: "0rem",
    },
  },
  pieChartContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    // marginTop: "3rem",
    paddingBottom: "3rem",
    alignItems: "center",
    // border: "1px solid red",
    flex: 1,
    "@media(max-width:800px)": {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
    },
  },
  categoryText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "1.2rem",
    lineHeight: "1.188rem",
    borderBottom: "0.0633rem solid #194478",
    paddingBottom: "0.2rem",
    color: colors.BLACK,
    "@media(max-width:1100px)": {
      fontSize: "0.675rem",
    },
  },
  categoryButton: {
    minWidth: "unset",
    padding: 0,
    "&:hover": { backgroundColor: "transparent" },
    border: "0.063rem solid rgb(165,165,165)",
    margin: "0.2rem",
    width: "100%",
  },
  categoryFlex: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "95%",
    marginTop: "0.2rem",
  },
  categoryOuterContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    border: "0.0633rem solid #194478",
    padding: "0.2rem",
    paddingBottom: "2rem",
    width: "13%",
  },
  categorySubText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.875rem",
    lineHeight: "1.188rem",
    color: colors.BLACK,
    textTransform: "capitalize",
    "@media(max-width:1100px)": {
      fontSize: "0.675rem",
    },
  },
  noChartData: {
    marginTop: "1rem",
    textAlign: "center",
  },
};
