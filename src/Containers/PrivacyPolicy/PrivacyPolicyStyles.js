import { DARK_BLUE } from "../../Constant/ColorConstant";

export const styles = {
  headingButtonsContainer: {
    display: "flex",
    justifyContent: "end",
    alignItems: "center",
  },

  loginButton: { width: "9rem", height: "2rem", marginLeft: "1%" },
  loginTextStyle: { fontSize: "0.75rem", fontWeight: 500 },
  linkText: {
    fontWeight: 600,
  },
  standardText: {
    fontSize: "1.5rem",
    fontWeight: 700,
    marginBottom: "1rem",
    fontStyle: "italic",
  },

  outerContainer: {
    padding: "2rem 5rem",
  },
  displayStyle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },

  contentContainer: {
    paddingTop: "2rem",
  },
  headingText: {
    fontWeight: "bold",
    background: DARK_BLUE,
    color: "white",
    paddingLeft: "5px",
    margin: "1rem 0",
  },
  subheadingText: {
    fontWeight: "bold",
    margin: "1rem 0",
  },
  contentText: {
    lineHeight: "2rem",
  },
};
