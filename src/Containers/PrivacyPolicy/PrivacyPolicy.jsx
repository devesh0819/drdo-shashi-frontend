import { Box } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import QpButton from "../../Components/QpButton/QpButton";
import QpTypography from "../../Components/QpTypography/QpTypography";
import { LOGIN } from "../../Routes/Routes";
import { commonStyles } from "../../Styles/CommonStyles";
import { styles } from "./PrivacyPolicyStyles.js";

export default function PrivacyPolicy() {
  const navigate = useNavigate();
  return (
    <>
      <Box sx={styles.outerContainer} id="standardMainContainer">
        <Box sx={styles.displayStyle}>
          <QpTypography
            displayText="Terms & Conditions and Website Policy"
            styleData={styles.standardText}
          />
          <Box sx={{ ...styles.headingButtonsContainer }}>
            <QpButton
              displayText="Go to Login"
              styleData={{
                ...commonStyles.blueButton,
                ...styles.loginButton,
                ...styles.headingButton,
              }}
              textStyle={{
                ...commonStyles.blueButtonText,
                ...styles.loginTextStyle,
              }}
              onClick={() => navigate(LOGIN)}
            />
          </Box>
        </Box>
        <Box sx={styles.contentContainer}>
          <QpTypography
            displayText="Disclaimer"
            styleData={styles.headingText}
          />
          <QpTypography
            displayText="The terms & conditions and website policies of SAMAR are same as that
          of DRDO. May also refer to DRDO website for terms & conditions and
          policies. For the benefit and convenience of the viewer, the terms and
          policies available on the DRDO website are replicated below."
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Terms & Conditions"
            styleData={styles.headingText}
          />
          <QpTypography
            displayText={`This website is designed, developed and maintained by Defence R&D
          Organisation (DRDO), Ministry of Defence, Government of India.
          DESIDOC, a constituent of DRDO, is the nodal agency for this activity.`}
            styleData={styles.contentText}
          />
          <br />
          <QpTypography
            displayText={` These terms and conditions shall be governed by and construed in
          accordance with the Indian Laws. Any dispute arising under these terms
          and conditions shall be subject to the jurisdiction of the courts of
          India, New Delhi.`}
            styleData={styles.contentText}
          />
          <br />
          <QpTypography
            displayText={`The information posted on this website could include hypertext links
          or pointers to information created and maintained by
          non-Government/private organisations. DRDO is providing these links
          and pointers solely for your information and convenience. When you
          select a link to an outside website, you are leaving the 'Guidelines
          for Indian Government Websites' site and are subject to the privacy
          and security policies of the owners/sponsors of the outside website.`}
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Privacy Policy"
            styleData={styles.headingText}
          />
          <QpTypography
            displayText="This website does not automatically capture any specific personal
          information from you, (like name, phone number or e-mail address),
          that allows us to identify you individually."
            styleData={styles.contentText}
          />
          <br />
          <QpTypography
            displayText="Wherever the Website requests you to provide personal information, you
          will be informed for the particular purposes for which the information
          is gathered and adequate security measures will be taken to protect
          your personal information."
            styleData={styles.contentText}
          />
          <br />
          <QpTypography
            displayText="We do not sell or share any personally identifiable information
          volunteered on the website site to any third party (public/private).
          Any information provided to this Website will be protected from loss,
          misuse, unauthorized access or disclosure, alteration, or destruction."
            styleData={styles.contentText}
          />
          <br />
          <QpTypography
            displayText="We gather certain information about the User, such as Internet
          protocol (IP) addresses, domain name, browser type, operating system,
          the date and time of the visit and the pages visited. We make no
          attempt to link these addresses with the identity of individuals visiting our
          site unless an attempt to damage the site has been detected."
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Copyright Policy"
            styleData={styles.headingText}
          />
          <QpTypography
            displayText=" Material featured on this Portal may be reproduced free of charge
          after taking proper permission by sending a mail to us. However, the
          material has to be reproduced accurately and not to be used in a
          derogatory manner or in a misleading context."
            styleData={styles.contentText}
          />
          <br />
          <QpTypography
            displayText="Wherever the material is being published or issued to others, the source must be prominently
          acknowledged. However, the permission to reproduce this material shall
          not extend to any material which is identified as being copyright of a
          third party. Authorisation to reproduce such material must be obtained
          from the departments/copyright holders concerned."
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Hyperlinking Policy"
            styleData={styles.headingText}
          />
          <QpTypography
            displayText="Links to external websites / portals:"
            styleData={styles.subheadingText}
          />
          <QpTypography
            displayText="At many places in this website, you shall find links to other
          websites/portals. The links have been placed for your convenience.
          DRDO is not responsible for the contents and reliability of the linked
          websites and does not necessarily endorse the views expressed in them."
            styleData={styles.contentText}
          />
          <br />
          <QpTypography
            displayText="Mere presence of the link or its listing on this Portal should not be
          assumed as endorsement of any kind. We cannot guarantee that these
          links will work all the time and we have no control over availability
          of linked pages."
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Links to SAMAR website by other websites:"
            styleData={styles.subheadingText}
          />
          <QpTypography
            displayText="We do not object to you linking directly to the information that is
          hosted on this site and no prior permission is required for the same.
          However, we would like you to inform us about any links provided to
          this Portal so that you can be informed of any changes or updations
          therein."
            styleData={styles.contentText}
          />
          <br />
          <QpTypography
            displayText=" Also, we do not permit our pages to be loaded into frames on your
          site. The pages belonging to this site must load into a newly opened
          browser window of the User."
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Website Monitoring Policy"
            styleData={styles.headingText}
          />
          <QpTypography
            displayText="DRDO has a Website Monitoring Policy in place and the website is
          monitored periodically to address and fix the quality and
          compatibility issues around the following parameters:"
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Performance:"
            styleData={styles.subheadingText}
          />
          <QpTypography
            displayText="Site download time is optimized for a variety of network
          connections as well as devices. All- important pages of the website
          are tested for this."
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Functionality:"
            styleData={styles.subheadingText}
          />
          <QpTypography
            displayText=" All modules of the website are tested for their functionality. The
          interactive components of the site such as, feedback forms are working
          smoothly."
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Broken Links:"
            styleData={styles.subheadingText}
          />
          <QpTypography
            displayText=" The website is thoroughly reviewed to rule out the presence of any
          broken links or errors."
            styleData={styles.contentText}
          />
          <QpTypography
            displayText="Traffic Analysis:"
            styleData={styles.subheadingText}
          />
          <QpTypography
            displayText=" The site traffic is regularly monitored to analyse
          the usage patterns."
            styleData={styles.contentText}
          />
          <QpTypography displayText="Feedback" styleData={styles.headingText} />
          <QpTypography
            displayText="Feedback from the visitors is the best way to judge a website’s
          performance and make necessary improvements. For any suggestions /
          queries, please mail to samar@qcin.org"
            styleData={styles.contentText}
          />
        </Box>
      </Box>
    </>
  );
}
