import React, { useState } from "react";
import { Box, InputBase, InputAdornment, IconButton } from "@mui/material";
import { commonStyles } from "../../Styles/CommonStyles.js";
import {
  useLocation,
  useNavigate,
  useParams,
  useSearchParams,
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  changePasswordSchema,
  newPasswordSchema,
} from "../../validationSchema/newPasswordSchema";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import getCaptcha from "../../Assets/Images/getCaptcha.png";
import {
  changePasswordAction,
  resetPasswordAction,
} from "../../Redux/Login/loginAction";
import QpButton from "../../Components/QpButton/QpButton";
import QpTypography from "../../Components/QpTypography/QpTypography.jsx";
import { encrypt } from "../../Services/localStorageService.js";
import { PASSWORD_ENCRYPTION_SECRET } from "../../Constant/AppConstant.js";
import { showToast } from "../../Components/Toast/Toast.js";
import { useEffect } from "react";
import { getCaptchaAction } from "../../Redux/GetCaptcha/getCaptchaActions.js";
import { useRef } from "react";
import { CHANGE_PASSWORD } from "../../Routes/Routes.js";

export default function NewPassword(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();
  const [passwordType, setPasswordType] = useState(true);
  const [oldPasswordType, setOldPasswordType] = useState(true);
  const [newPassword, setNewPassword] = useState(true);
  const [searchParams] = useSearchParams();
  const token = searchParams.get("token");
  const email = searchParams.get("email");
  const captchaData = useSelector((state) => state.getCaptcha.captchaData);
  const runOnce = useRef(false);

  const {
    register,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(
      token && email ? newPasswordSchema : changePasswordSchema
    ),
    mode: "onChange",
  });

  useEffect(() => {
    if (runOnce.current === false) {
      dispatch(getCaptchaAction());
      return () => {
        runOnce.current = true;
      };
    }
  }, []);

  const changeToggle = () => {
    setPasswordType(!passwordType);
  };
  const changeOldPasswordToggle = () => {
    setOldPasswordType(!oldPasswordType);
  };

  const changeNewPasswordToggle = () => {
    setNewPassword(!newPassword);
  };
  const parameters = [
    "Minimum 6 Characters",
    "1 Number",
    "1 Upper and Lowercase",
    "1 Special Character",
  ];
  const onSubmit = (data) => {
    if (data && data.password === data.password_confirmation) {
      if (token && email) {
        dispatch(
          resetPasswordAction(
            {
              password: encrypt(
                data.password,
                process.env.REACT_APP_PASSWORD_SECRET_KEY
              ),
              password_confirmation: encrypt(
                data.password_confirmation,
                process.env.REACT_APP_PASSWORD_SECRET_KEY
              ),
              token: token,
              email: atob(email.toString()),
            },
            navigate
          )
        );
      } else {
        if (!data.old_password) {
          showToast("Old password is required", "error");
          return;
        }
        let completeData = {
          old_password: encrypt(
            data.old_password,
            process.env.REACT_APP_PASSWORD_SECRET_KEY
          ),
          password: encrypt(
            data.password,
            process.env.REACT_APP_PASSWORD_SECRET_KEY
          ),
          password_confirmation: encrypt(
            data.password_confirmation,
            process.env.REACT_APP_PASSWORD_SECRET_KEY
          ),
          key: captchaData?.key,
          captcha: data.captcha,
        };
        dispatch(changePasswordAction(completeData));
        setTimeout(() => {
          setValue("old_password", "", { shouldValidate: false });
          setValue("password", "", { shouldValidate: false });
          setValue("password_confirmation", "", { shouldValidate: false });
          setValue("captcha", "", { shouldValidate: false });
        }, 1000);
      }
    }
  };
  return (
    <>
      <Box sx={{ ...commonStyles.smallWhiteContainer, ...props.styleData }}>
        <QpTypography
          displayText="Set New Password"
          styleData={{
            ...commonStyles.enterOtpText,
            ...commonStyles.forgotAlign,
          }}
        />
        {!token && !email && (
          <Box>
            <InputBase
              id="old_password"
              name="old_password"
              placeholder="Enter Old Password"
              required
              type={oldPasswordType ? "password" : "text"}
              sx={{
                ...commonStyles.forgotEmailStyle,
                ...commonStyles.newPwd,
              }}
              endAdornment={
                <InputAdornment position="start">
                  <IconButton
                    aria-label="toggle old_password visibility"
                    onClick={changeOldPasswordToggle}
                    edge="end"
                  >
                    {oldPasswordType ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              {...register("old_password")}
              error={errors.old_password ? true : false}
              autoComplete="off"
            />
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={errors.old_password?.message}
            />
          </Box>
        )}
        <Box>
          <InputBase
            id="password"
            name="password"
            placeholder="Enter Password"
            required
            type={passwordType ? "password" : "text"}
            sx={{
              ...commonStyles.forgotEmailStyle,
              ...commonStyles.newPwd,
            }}
            endAdornment={
              <InputAdornment position="start">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={changeToggle}
                  edge="end"
                >
                  {passwordType ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            {...register("password")}
            error={errors.password ? true : false}
            autoComplete="off"
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.password?.message}
          />
        </Box>

        <Box>
          <InputBase
            id="password_confirmation"
            name="password_confirmation"
            type={newPassword ? "password" : "text"}
            placeholder="Re-enter new password"
            required
            sx={{
              ...commonStyles.forgotEmailStyle,
              ...commonStyles.newPwd,
            }}
            {...register("password_confirmation")}
            error={errors.password_confirmation ? true : false}
            endAdornment={
              <InputAdornment position="start">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={changeNewPasswordToggle}
                  edge="end"
                >
                  {newPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            autoComplete="off"
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.password_confirmation?.message}
          />
        </Box>
        {location.pathname.includes(CHANGE_PASSWORD) && (
          <Box>
            <QpTypography
              displayText="CAPTCHA Security Check "
              styleData={{
                ...commonStyles.securityCheckText,
                ...commonStyles.securityCheckTextCenter,
              }}
            />
            <Box
              sx={{
                ...commonStyles.enterCaptchaContainer,
                ...commonStyles.captchaContainerFullWidth,
              }}
            >
              <Box>
                <Box
                  sx={{
                    ...commonStyles.captchaContainer,
                    ...commonStyles.displayCenterStyle,
                  }}
                >
                  <img src={captchaData?.img} alt="captcha" />
                </Box>
                <QpTypography
                  displayText="Enter the text below you see on the image"
                  styleData={commonStyles.enterCaptchaText}
                />
                <Box sx={commonStyles.displayStyle}>
                  <InputBase
                    sx={commonStyles.enterCaptchaInput}
                    id="captcha"
                    name="captcha"
                    {...register("captcha")}
                    error={errors.captcha ? true : false}
                  />
                  <Box
                    onClick={() => dispatch(getCaptchaAction())}
                    sx={commonStyles.cursorPointer}
                  >
                    <img src={getCaptcha} alt="getCaptcha" />
                  </Box>
                </Box>
                <QpTypography
                  styleData={commonStyles.errorText}
                  displayText={errors.captcha?.message}
                />
              </Box>
            </Box>
          </Box>
        )}
        <QpButton
          displayText="Reset Password"
          styleData={{
            ...commonStyles.validateButton,
            ...commonStyles.requestLinkButton,
          }}
          textStyle={commonStyles.validateButtontext}
          onClick={handleSubmit(onSubmit)}
        />
      </Box>
    </>
  );
}
