import { commonStyles } from "../../../Styles/CommonStyles";

export const styles = {
  container: {
    marginX: "1rem",
    marginY: "1.5rem",
  },
  dashboardContainer: {
    display: "flex",
    justifyContent: "space-between",
    height: "calc(100vh - 14rem)",
    overflow: "scroll",
    ...commonStyles.customScrollBar,
  },
  boxStyle: {
    minHeight: "100%",
    maxHeight: "100%",
  },
};
