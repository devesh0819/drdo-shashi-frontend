import React, { useEffect, useState } from "react";
import { Box, Typography, Button } from "@mui/material";
import Radio from "@mui/material/Radio";
import TextareaAutosize from "@mui/material/TextareaAutosize";
import { useNavigate, useParams } from "react-router-dom";
import { DRDO_ALL_APPLICATIONS } from "../../../Routes/Routes";
import { ReactComponent as IconCircularBack } from "../../../Assets/Images/iconCircularBack.svg";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";

import { useDispatch, useSelector } from "react-redux";
import {
  getDrdoApplicationDetailAction,
  setDrdoApplicationViewFormAction,
} from "../../../Redux/AllApplications/allApplicationsActions";
import { styles } from "../../Applicant/Preview/PreviewStyles.js";

import QpButton from "../../../Components/QpButton/QpButton";
import * as _ from "lodash";
import QpDialog from "../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../Components/QpConfirmModal/QpConfirmModal";

import CustomApplicatonView from "../../../Components/CustomApplicationView/CustomApplicationView";

export default function DrdoApplicationView() {
  const [selectedValue, setSelectedValue] = useState("APPROVE");
  const [isSubsidicedChecked, setSubsidiced] = useState(false);
  const [rejectReasonText, setrejectReasonText] = useState("");
  const [showConfirm, setShowConfirm] = useState(false);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();

  const applicationDetail = useSelector(
    (state) => state.allApplications.drdoApplicationDetail
  );

  useEffect(() => {
    dispatch(getDrdoApplicationDetailAction(id));
  }, [id]);

  const handleRadioChange = (e) => {
    setSelectedValue(e?.target?.value);
    if (e.target.value === "APPROVE") setrejectReasonText("");
    else setSubsidiced(false);
  };
  // const handleSudsidyChange=()=>{
  //   setSubsidiced(!isSubsidicedChecked)
  // }
  const handleTextAreaChange = (e) => {
    if (e?.target?.value) setrejectReasonText(e.target.value);
  };
  const handleConfirmRadioChange = (e) => {
    if (e.target.value === "true") setSubsidiced(true);
    else setSubsidiced(false);
  };
  const handleSubmit = () => {
    const payload = {};

    if (selectedValue === "APPROVE") {
      payload.application_stage = "app_approved";
      payload.subsidy_approved = isSubsidicedChecked;
    } else {
      payload.application_stage = "app_rejected";
      if (rejectReasonText?.length > 0)
        payload.reject_reason = rejectReasonText;
    }
    dispatch(setDrdoApplicationViewFormAction(payload, navigate, id));
  };

  return (
    <>
      <Box sx={commonStyles.headerOuterContainer}>
        <Box sx={commonStyles.topBackButtonHeader}>
          <Button
            disableRipple
            sx={commonStyles.backButton}
            onClick={() => navigate(DRDO_ALL_APPLICATIONS)}
          >
            <IconCircularBack />
          </Button>
          <Typography style={commonStyles.titleBackText}>
            Application Detail
          </Typography>
        </Box>
      </Box>
      <Box sx={customCommonStyles.fieldContainer}>
        <CustomApplicatonView applicationDetail={applicationDetail} />

        <div className="view-application-action-continer">
          <div
            className="view-application-action-radio-div"
            style={{ padding: "2% 0" }}
          >
            {/* <span>Approve</span> */}
            <Typography
              sx={commonStyles.labelText}
              style={commonStyles.displayInBlock}
            >
              Approve
            </Typography>
            <Radio
              checked={selectedValue === "APPROVE"}
              onChange={handleRadioChange}
              value="APPROVE"
              name="radio-buttons"
              label="Approve"
            />
            {/* <span>Reject</span> */}
            <Typography
              sx={commonStyles.labelText}
              style={commonStyles.displayInBlock}
            >
              Reject
            </Typography>
            <Radio
              checked={selectedValue === "REJECT"}
              onChange={handleRadioChange}
              value="REJECT"
              name="radio-buttons"
              sx={commonStyles.rejectApplication}
            />
          </div>
          <div className="view-application-satus-div">
            {selectedValue === "APPROVE" ? (
              <>
                {/* <Checkbox
            checked={isSubsidicedChecked}
            onChange={handleSudsidyChange}
          /> */}
                {/* <p> Eligible for Subsidy ?</p> */}
                <Typography
                  sx={{
                    ...commonStyles.labelText,
                    ...commonStyles.marginRight2Percent,
                  }}
                  style={commonStyles.displayInBlock}
                >
                  Eligible for Subsidy ?
                </Typography>
                <Typography
                  sx={commonStyles.labelText}
                  style={commonStyles.displayInBlock}
                >
                  Yes
                </Typography>
                <Radio
                  checked={isSubsidicedChecked}
                  onChange={handleConfirmRadioChange}
                  value={true}
                  name="radio-buttons"
                  label="Approve"
                />
                {/* <span>Reject</span> */}
                <Typography
                  sx={commonStyles.labelText}
                  style={commonStyles.displayInBlock}
                >
                  No
                </Typography>
                <Radio
                  checked={!isSubsidicedChecked}
                  onChange={handleConfirmRadioChange}
                  value={false}
                  name="radio-buttons"
                  sx={commonStyles.rejectApplication}
                />
              </>
            ) : (
              <TextareaAutosize
                maxRows={4}
                aria-label="maximum height"
                placeholder="Reason for Rejection"
                //  defaultValue=""
                style={commonStyles.rejectReasonDiv}
                onChange={handleTextAreaChange}
              />
            )}
          </div>
        </div>
        <QpButton
          displayText="Submit"
          styleData={{
            ...commonStyles.blueButton,
            ...styles.approveButton,
            ...commonStyles.submitDrdo,
          }}
          textStyle={{
            ...commonStyles.blueButtonText,
            ...styles.approveButtonText,
          }}
          onClick={() => setShowConfirm(true)}
          className="drdo-application-view-submit"
        />
      </Box>
      <QpDialog
        open={showConfirm}
        closeModal={setShowConfirm}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to perform this action?"
          closeModal={setShowConfirm}
          onConfirm={() => handleSubmit()}
        />
      </QpDialog>
    </>
  );
}
