import React, { useState, useEffect } from "react";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import { ReactComponent as IconCircularDownload } from "../../../Assets/Images/iconCircularDownload.svg";
import { ReactComponent as IconPencil } from "../../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconMenuSearch } from "../../../Assets/Images/iconMenuSearch.svg";
import { Button, Box, Tooltip } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import moment from "moment";
import {
  approveApplicationAction,
  getAllApplicationsAction,
} from "../../../Redux/DrdoAdmin/DrdoAllApplications/drdoAllApplicationActions";
import QpButton from "../../../Components/QpButton/QpButton";
import { commonStyles } from "../../../Styles/CommonStyles";
import QpConfirmModal from "../../../Components/QpConfirmModal/QpConfirmModal";
import QpDialog from "../../../Components/QpDialog/QpDialog";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { styles } from "./AllApplicationsStyle.js";
import {
  getAppStatus,
  getEnterpriceType,
} from "../../../Services/commonService";
import FilterComponent from "../../../Components/FilterComponent/FilterComponent";
import {
  drdoApplicationsStatus,
  typeOfEnterpriseArray,
} from "../../../Constant/AppConstant";
import { downloadAction } from "../../../Redux/AllApplications/allApplicationsActions";

const AllApplications = () => {
  const navigate = useNavigate();
  const [openModal, setOpenModal] = useState(false);
  const [approveId, setApproveId] = useState();
  const [appStatus, setAppStatus] = useState();
  const [pageNumber, setPageNumber] = useState(1);
  const [rowData, setRowData] = useState([]);
  const [openFilterDialog, setOpenFilterDialog] = useState(false);
  const [noOfFiltersApplied, setNoOfFiltersApplied] = useState(0);
  const [alreadySelectedFilters, setAlreadySelectedFilters] = useState({});
  const [payloadState, setPayloadState] = useState();

  const dispatch = useDispatch();
  const applicantsListing = useSelector(
    (state) => state.drdoAllApplications.allApplications
  );

  useEffect(() => {
    if (pageNumber) {
      dispatch(
        getAllApplicationsAction({ start: pageNumber, ...payloadState })
      );
    }
  }, [pageNumber, dispatch, payloadState]);

  const handleApproveApplication = () => {
    dispatch(
      approveApplicationAction(approveId, {
        subsidy_approved: 1,
        application_stage: appStatus,
      })
    );
  };
  const columnDefs = [
    { field: "Application number" },
    {
      field: "Name of enterprise",
      ellipsisClass: true,
    },
    { field: "Type of enterprise" },
    { field: "Application Date" },
    { field: "DRDO Registered Vendor" },

    { field: "Status" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <QpButton
            displayText="View"
            styleData={{ ...commonStyles.blueButton, ...styles.approveButton }}
            textStyle={{
              ...commonStyles.blueButtonText,
              ...styles.approveButtonText,
            }}
            onClick={() => {
              // setApproveId(row.id);
              // setAppStatus(row.appStatus);
              // setOpenModal(true);
              navigate(`details/${row.id}`);
            }}
          />
        );
      },
    },
    // {
    //   field: "",
    //   renderColumn: (row) => {
    //     return (
    //       <Box sx={{ width: "120px" }}>
    //         <Box className="icons" sx={commonStyles.displayNone}>
    //           <Button
    //             sx={{
    //               minWidth: "unset",
    //               padding: 0,
    //               marginRight: "1.75rem",
    //               "&:hover": { backgroundColor: "transparent" },
    //             }}
    //           >
    //             <Tooltip title="Menu" arrow>
    //               <IconMenuSearch />
    //             </Tooltip>
    //           </Button>
    //           <Button
    //             sx={{
    //               minWidth: "unset",
    //               padding: 0,
    //               marginRight: "1.75rem",
    //               "&:hover": { backgroundColor: "transparent" },
    //             }}
    //             onClick={() => navigate(`/drdo-allApplications/edit/${row.id}`)}
    //           >
    //             <Tooltip title="Edit" arrow>
    //               <IconPencil />
    //             </Tooltip>
    //           </Button>
    //           <Button
    //             sx={{
    //               minWidth: "unset",
    //               padding: 0,
    //               "&:hover": { backgroundColor: "transparent" },
    //             }}
    //           >
    //             <Tooltip title="Download" arrow>
    //               <IconCircularDownload />
    //             </Tooltip>
    //           </Button>
    //         </Box>
    //       </Box>
    //     );
    //   },
    // },
  ];

  useEffect(() => {
    if (applicantsListing?.applications) {
      const dataSet = applicantsListing?.applications?.map((item, index) => {
        return {
          "Application number": item.application_number,
          "Name of enterprise": item.enterprise_name,
          "Type of enterprise": getEnterpriceType(item?.enterprice_type),
          "Application Date":
            item?.application_date &&
            moment(item?.application_date).format("D MMM,YYYY"),
          "DRDO Registered Vendor":
            item?.is_existing_vendor && item?.vendor_certificate_url
              ? "Yes"
              : "No",
          Status: getAppStatus(item.application_stage),
          id: item.application_uuid,
          appStatus: item.status,
          vendor_certificate_url: item?.vendor_certificate_url,
          application_number: item?.application_number,
          is_existing_vendor: item?.is_existing_vendor,
        };
      });
      setRowData(dataSet);
    }
  }, [applicantsListing]);

  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const filterOptions = [
    {
      filterId: 1,
      displayName: "Type of Enterprise",
      filterType: "Dropdown",
      key: "type_of_enterprise",
      dataArray: typeOfEnterpriseArray,
      labelId: "Type of Enterprise",
      label: "Enterprise Type",
      filterName: "enterpriseType",
    },
    // {
    //   filterId: 2,
    //   displayName: "Application Status",
    //   filterType: "Dropdown",
    //   key: "application_status",
    //   dataArray: drdoApplicationsStatus,
    //   labelId: "Application Status",
    //   label: "Status",
    // },
  ];

  const saveHandler = (payload) => {
    dispatch(getAllApplicationsAction({ start: pageNumber, ...payload }));
    setPageNumber(1);
    setPayloadState(payload);
  };

  const exportButtonClick = () => {
    const url = `${process.env.REACT_APP_BASE_URL}/drdo/application?is_excel=1&excel_type=drdo_application`;
    dispatch(
      downloadAction("PendingApplications", dispatch, url, "doc", "xlsx")
    );
  };

  return (
    <>
      {/* {applicantsListing?.total === 0 ? (
        <QpTypography
          displayText="No data available"
          styleData={commonStyles.noData}
        />
      ) : ( */}
      <>
        {/* {openFilterDialog && (
          <FilterComponent
            saveHandler={saveHandler}
            filterOptions={filterOptions}
            alreadySelectedFilters={alreadySelectedFilters}
            setOpenFilterDialog={setOpenFilterDialog}
            setNoOfFiltersApplied={setNoOfFiltersApplied}
            setAlreadySelectedFilters={setAlreadySelectedFilters}
          />
        )} */}
        <CustomTable
          columnDefs={columnDefs}
          rowData={rowData}
          title="Pending Applications"
          hasPagination
          headerCellStyle={commonStyles.headerCellStyle}
          tableBodyCellStyle={commonStyles.tableBodyCellStyle}
          handlePagination={handlePagination}
          currentPage={pageNumber}
          totalValues={applicantsListing?.total}
          styleData={commonStyles.tableHeight}
          hasFilter
          onClickFilter={() => setOpenFilterDialog(!openFilterDialog)}
          noOfFiltersApplied={noOfFiltersApplied}
          buttonsDivStyle={commonStyles.divFlexStyle}
          exportButtonClick={exportButtonClick}
          hasExport={true}
          saveHandler={saveHandler}
          filterOptions={filterOptions}
          alreadySelectedFilters={alreadySelectedFilters}
          setOpenFilterDialog={setOpenFilterDialog}
          setNoOfFiltersApplied={setNoOfFiltersApplied}
          setAlreadySelectedFilters={setAlreadySelectedFilters}
        />
      </>
      {/* )} */}
      <QpDialog
        open={openModal}
        closeModal={setOpenModal}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to approve the application?"
          closeModal={setOpenModal}
          onConfirm={() => handleApproveApplication()}
        />
      </QpDialog>
    </>
  );
};

export default AllApplications;
