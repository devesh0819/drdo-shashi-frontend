export const styles = {
  approveButton: {
    width: "5rem",
    height: "2rem",
  },
  approveButtonText: {
    fontSize: "0.75rem",
    fontWeight: 500,
  },
};
