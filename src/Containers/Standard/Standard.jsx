import { Box, Link, Typography } from "@mui/material";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import QpTypography from "../../Components/QpTypography/QpTypography";
import { styles } from "./StandardStyles.js";
import QpButton from "../../Components/QpButton/QpButton";
import { useNavigate } from "react-router-dom";
import { LOGIN } from "../../Routes/Routes";
import { dat } from "./standardData";
import { useState, useEffect } from "react";
import { getStandardQuestionairreAction } from "../../Redux/FooterLinks/footerLinkActions";
import { useDispatch, useSelector } from "react-redux";

export default function Standard() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [data, setData] = useState();
  const standardQuestionairreData = useSelector(
    (state) => state.footerLinksData.standardQuestionairre
  );

  useEffect(() => {
    dispatch(getStandardQuestionairreAction());
  }, [dispatch]);

  useEffect(() => {
    if (standardQuestionairreData) {
      setData(standardQuestionairreData);
    }
  }, [standardQuestionairreData]);

  const handlePrint = () => {
    window?.print();
  };

  const renderStandard = (data) => {
    return (
      <>
        <Box sx={styles.mainBox}>
          {data?.disciplines?.length
            ? data?.disciplines?.map((d) => {
                return (
                  <Box style={styles.disciplineBox}>
                    <Box style={{ ...styles.DisciplineText }}>
                      <b>Discipline:</b> {`${d?.title}`}
                    </Box>
                    {d?.parameters.map((p, index) => {
                      return (
                        <Box>
                          <Box key={index} sx={{ ...styles.parameterBox }}>
                            <b>Parameter:</b> {`${p?.title}`}
                          </Box>
                          {p?.sections?.filter((s) => s.type === "P")
                            ?.length ? (
                            <>
                              <Box>
                                <Box sx={styles.PdmMargin}>Planning</Box>
                                <Box sx={{ ...styles.tablesContainer }}>
                                  <table style={styles.tableStyle}>
                                    <tbody>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 1
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "P")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.bronze}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 2
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "P")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.silver}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 3
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "P")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                  // ...styles.levelWidth,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)?.gold}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 4
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "P")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.diamond}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            // ...styles.levelWidth,
                                          }}
                                        >
                                          Level 5
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "P")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.platinum}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                    </tbody>
                                  </table>
                                </Box>
                              </Box>
                            </>
                          ) : (
                            <>
                              <Box sx={styles.noDataMargin}>
                                No Data for Planning
                              </Box>
                            </>
                          )}
                          {p?.sections?.filter((s) => s.type === "D")
                            ?.length ? (
                            <>
                              <Box>
                                <Box sx={styles.PdmMargin}>Deployment</Box>
                                <Box sx={{ ...styles.tablesContainer }}>
                                  <table style={styles.tableStyle}>
                                    <tbody>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 1
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "D")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.bronze}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 2
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "D")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.silver}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 3
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "D")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)?.gold}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 4
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "D")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.diamond}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 5
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "D")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.platinum}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                    </tbody>
                                  </table>
                                </Box>
                              </Box>
                            </>
                          ) : (
                            <>
                              <Box sx={styles.noDataMargin}>
                                No Data for Deployment
                              </Box>
                            </>
                          )}
                          {p?.sections?.filter((s) => s.type === "M")
                            ?.length ? (
                            <>
                              <Box>
                                <Box sx={styles.PdmMargin}>Monitoring</Box>
                                <Box sx={{ ...styles.tablesContainer }}>
                                  <table style={styles.tableStyle}>
                                    <tbody>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 1
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "M")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.bronze}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 2
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "M")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.silver}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 3
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "M")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)?.gold}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 4
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "M")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.diamond}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                      <tr>
                                        <th
                                          style={{
                                            ...styles.borderStyle,
                                            ...styles.paddingZeroOne,
                                            ...styles.levelWidth,
                                          }}
                                        >
                                          Level 5
                                        </th>
                                        {p?.sections
                                          ?.filter((s) => s.type === "M")
                                          ?.map((s, index) => {
                                            return (
                                              <td
                                                style={{
                                                  ...styles.borderStyle,
                                                  ...styles.paddingZeroOne,
                                                }}
                                              >
                                                {s?.options &&
                                                  JSON.parse(s?.options)
                                                    ?.platinum}
                                              </td>
                                            );
                                          })}
                                      </tr>
                                    </tbody>
                                  </table>
                                </Box>
                              </Box>
                            </>
                          ) : (
                            <Box sx={styles.noDataMargin}>
                              No Data for Monitoring
                            </Box>
                          )}
                        </Box>
                      );
                    })}
                  </Box>
                );
              })
            : "No Data"}
        </Box>
      </>
    );
  };

  const documents = [
    {
      label:
        "The SAMAR certification is based on a maturity assessment model and is applicable to all defence manufacturing enterprises i.e., micro, small, medium and large enterprises. SAMAR certification framework recognizes 5 levels of enterprise maturity in terms of the defined organisational attributes. Click on the hyperlink to know more about the certification process, fees, subsidy and validity of SAMAR certification.",
      link: "https://drive.google.com/drive/folders/1bSFDFzB90rIw3stSVqYa_Q4csq1GjOpf?usp=sharing",
      linkText: "Certification Process",
    },
    {
      label:
        "The SAMAR certification standard consists of separate models for Large and MSME enterprises. Each model consists of 10 broad disciplines and multiple parameters under each of these disciplines. Click on the  hyperlink to know more about the assessment models.",
      link: "https://drive.google.com/drive/folders/1qMCDH3dq6ZWI3Oe4pZpxYrHBnDYp0R2H?usp=sharing",
      linkText: "Certification Standard",
    },
    {
      label:
        "Usage of SAMAR website and its content is guided by specific terms & conditions and website policies. To know more about these, click on the hyperlink.",
      link: "https://drive.google.com/drive/folders/15tNDKkyFKPPGVpapJ9NRhdsrpP-hhEnf?usp=sharing",
      linkText: "Terms & Conditions and Website Policies",
    },
    {
      label:
        "Click on the above hyperlink to access more information about the SAMAR.",
      link: "https://drive.google.com/drive/folders/1daGotK0X4DB1bNLeVbh5VmX8oa13oyjE?usp=sharing",
      linkText: "Other documents",
    },
  ];

  return (
    <>
      <Box sx={styles.outerContainer} id="standardMainContainer">
        <Box sx={styles.displayStyle}>
          <QpTypography
            displayText="Documents"
            styleData={styles.standardText}
          />
          <Box sx={{ ...styles.headingButtonsContainer }}>
            {/* <QpButton
              displayText="Print"
              styleData={{
                ...commonStyles.blueButton,
                ...{ width: "9rem", height: "2rem", marginLeft: "1%" },
                ...styles.headingButton,
              }}
              textStyle={{
                ...commonStyles.blueButtonText,
                ...{ fontSize: "0.75rem", fontWeight: 500 },
              }}
              onClick={handlePrint}
            /> */}
            <QpButton
              displayText="Go to Login"
              styleData={{
                ...commonStyles.blueButton,
                ...styles.loginButton,
                ...styles.headingButton,
              }}
              textStyle={{
                ...commonStyles.blueButtonText,
                ...styles.loginTextStyle,
              }}
              onClick={() => navigate(LOGIN)}
            />
          </Box>
        </Box>
        <Box id="printableDiv">
          {documents?.map((doc, index) => {
            return (
              <Box key={doc} sx={{ ...customCommonStyles.marginBottomOne }}>
                <Typography>
                  {index + 1}.{" "}
                  <Link
                    href={doc.link}
                    target="_blank"
                    sx={{ ...styles.linkText }}
                  >
                    {doc.linkText}
                  </Link>
                </Typography>
                <QpTypography
                  displayText={doc.label}
                  styleData={{ ...styles.documentText }}
                />
                {/* <Typography>
                  Click here-{" "}
                  <Link href={doc.link} target="_blank">
                    {doc.link}
                  </Link>
                </Typography> */}
              </Box>
            );
          })}
          {/* ...........LARGE QUESTIONAIRRE STANDARD........ */}
          {/* <Typography sx={{ marginBottom: "0.5rem" }}>
            <i style={{ fontWeight: "bold", fontSize: "1.5rem" }}>Documents</i>
          </Typography> */}
          {/* {renderStandard(data?.large_questionnaire)} */}
          {/* ........SMALL QUESTIONAIRRE STANDARD........ */}
          {/* <Typography sx={{ margin: "2.5rem 0 0.5rem 0" }}>
            <i style={{ fontWeight: "bold", fontSize: "1.5rem" }}>
              Standards for Small/Medium/Micro Enterprise Assessment
            </i>
          </Typography>
          {renderStandard(data?.small_questionnaire)} */}
        </Box>
      </Box>
    </>
  );
}
