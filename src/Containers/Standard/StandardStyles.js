export const styles = {
  mainContainer: {},
  tableStyle: {
    width: "100%",
    marginBottom: "0.5rem",
  },
  headingButtonsContainer: {
    display: "flex",
    justifyContent: "end",
    alignItems: "center",
  },
  headingButton: {
    "@media print": {
      visibility: "hidden",
    },
  },
  documentText: {
    marginBottom: "0.5rem",
  },
  loginButton: { width: "9rem", height: "2rem", marginLeft: "1%" },
  loginTextStyle: { fontSize: "0.75rem", fontWeight: 500 },
  linkText: {
    fontWeight: 600,
  },
  standardText: {
    fontSize: "1.5rem",
    fontWeight: 700,
    marginBottom: "1rem",
    fontStyle: "italic",
  },
  disciplineBox: {
    border: "1px solid black",
    marginBottom: "1rem",
    padding: "1rem 0.3rem",
    // textAlign:'center',
    fontSize: "1.1rem",
  },
  parameterBox: {
    fontSize: "1.1rem",
  },
  mainBox: {
    // border:"1px solid black",
  },
  outerContainer: {
    padding: "2rem 5rem",
    backgroundColor: "white",
  },
  displayStyle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  borderStyle: {
    border: "1px solid black",
  },
  boldborderStyle: {
    border: "2px solid black",
  },
  paddingZeroOne: {
    padding: "6px 6px",
  },
  DisciplineText: {
    fontSize: "1.3rem",
    marginBottom: "1rem",
  },
  ParameterLabel: {},
  PDMLabel: {},
  tablesContainer: {
    fontSize: "0.8rem",
  },
  PdmMargin: {
    marginTop: "1rem",
    marginBottom: "1rem",
  },
  noDataMargin: {
    marginTop: "1rem",
  },
  levelWidth: {
    maxWidth: "10rem",
    minWidth: "10rem",
    width: "10rem",
  },
};
