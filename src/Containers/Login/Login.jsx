import React, { useState, useEffect, useRef } from "react";
import {
  Grid,
  InputBase,
  Box,
  InputAdornment,
  IconButton,
} from "@mui/material";
import { commonStyles } from "../../Styles/CommonStyles";
import QpInputLabel from "../../Components/QpInputLabel/QpInputLabel";
import QpTypography from "../../Components/QpTypography/QpTypography";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import getCaptcha from "../../Assets/Images/getCaptcha.png";
import QpButton from "../../Components/QpButton/QpButton";
import { loginSchema } from "../../validationSchema/loginSchema";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useDispatch, useSelector } from "react-redux";
import { getCaptchaAction } from "../../Redux/GetCaptcha/getCaptchaActions";
import { useNavigate } from "react-router-dom";
import { FORGOT_PASSWORD } from "../../Routes/Routes";
import { loginAction } from "../../Redux/Login/loginAction";
import { decrypt, encrypt } from "../../Services/localStorageService";
import { PASSWORD_ENCRYPTION_SECRET } from "../../Constant/AppConstant";

export default function Login({ url }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(loginSchema),
    mode: "onChange",
  });

  const runOnce = useRef(false);
  const [passwordType, setPasswordType] = useState(true);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const captchaData = useSelector((state) => state.getCaptcha.captchaData);

  const onSubmit = (data) => {
    if (data) {
      data["password"] = encrypt(
        data?.password,
        process.env.REACT_APP_PASSWORD_SECRET_KEY
      );
      data["key"] = captchaData?.key;
    }
    dispatch(loginAction(data, navigate, url));
  };
  const changeToggle = () => {
    setPasswordType(!passwordType);
  };

  useEffect(() => {
    if (runOnce.current === false) {
      dispatch(getCaptchaAction());
      return () => {
        runOnce.current = true;
      };
    }
  }, []);

  return (
    <>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={"4rem"}
        sx={commonStyles.signUpContainer}
      >
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={12}
          sx={commonStyles.signUpFields}
        >
          <QpInputLabel
            displayText="Email or Unique Id"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <InputBase
            id="email"
            name="email"
            required
            sx={{ ...commonStyles.inputStyle }}
            {...register("email")}
            error={errors.email ? true : false}
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.email?.message}
          />
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={12}
          sx={commonStyles.signUpFields}
        >
          <QpInputLabel
            displayText="Password"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <InputBase
            id="password"
            name="password"
            required
            sx={{ ...commonStyles.inputStyle }}
            type={passwordType ? "password" : "text"}
            {...register("password")}
            error={errors.password ? true : false}
            endAdornment={
              <InputAdornment position="start">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={changeToggle}
                  edge="end"
                >
                  {passwordType ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.password?.message}
          />
        </Grid>
        <Grid sx={commonStyles.forgotContainer}>
          <QpTypography
            displayText="Forgot password?"
            styleData={{
              ...commonStyles.forgotPasswordText,
              ...commonStyles.cursorPointer,
            }}
            onClick={() => navigate(FORGOT_PASSWORD)}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12} sx={{ paddingTop: "0" }}>
          <Box>
            <QpTypography
              displayText="CAPTCHA Security Check "
              styleData={commonStyles.securityCheckText}
            />
            <Box sx={commonStyles.enterCaptchaContainer}>
              <Box>
                <Box
                  sx={{
                    ...commonStyles.captchaContainer,
                    ...commonStyles.displayCenterStyle,
                  }}
                >
                  <img src={captchaData?.img} alt="captcha" />
                </Box>
                <QpTypography
                  displayText="Enter the text below you see on the image"
                  styleData={commonStyles.enterCaptchaText}
                />
                <Box sx={commonStyles.displayStyle}>
                  <InputBase
                    sx={commonStyles.enterCaptchaInput}
                    id="captcha"
                    name="captcha"
                    {...register("captcha")}
                    error={errors.captcha ? true : false}
                  />
                  <Box
                    onClick={() => dispatch(getCaptchaAction())}
                    sx={commonStyles.cursorPointer}
                  >
                    <img src={getCaptcha} alt="getCaptcha" />
                  </Box>
                </Box>
                <QpTypography
                  styleData={commonStyles.errorText}
                  displayText={errors.captcha?.message}
                />
              </Box>
            </Box>
          </Box>
        </Grid>
        <QpButton
          displayText="Log In"
          styleData={{
            ...commonStyles.createAccountButton,
            ...{ marginTop: "1.25rem" },
          }}
          onClick={handleSubmit(onSubmit)}
        />
      </Grid>
    </>
  );
}
