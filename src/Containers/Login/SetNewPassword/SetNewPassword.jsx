import React from "react";
import QpBoxContainer from "../../../Components/QpBoxContainer/QpBoxContainer";

import NewPassword from "../../NewPassword/NewPassword";

export default function SetNewPassword() {
  return (
    <QpBoxContainer>
      <NewPassword />
    </QpBoxContainer>
  );
}
