import { Box } from "@mui/material";
import React from "react";
import QpBoxContainer from "../../../Components/QpBoxContainer/QpBoxContainer";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import verifiedTick from "../../../Assets/Images/greenCircleTick.png";
import { commonStyles } from "../../../Styles/CommonStyles";

export default function PasswordChanged() {
  return (
    <QpBoxContainer>
      {" "}
      <Box sx={commonStyles.verifiedContainer}>
        <Box>
          <img
            src={verifiedTick}
            alt="verifiedTick"
            width="105px"
            height="105px"
          />
        </Box>
        <QpTypography
          displayText="Password Changed Successfully!"
          styleData={commonStyles.enterOtpText}
        />
        <QpTypography
          displayText="Your password has been changed successfully. 
Use your new password to log in."
          styleData={commonStyles.passwordChangdText}
        />
      </Box>
    </QpBoxContainer>
  );
}
