import React from "react";
import QpBoxContainer from "../../../Components/QpBoxContainer/QpBoxContainer";
import { Box, InputBase } from "@mui/material";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import userIcon from "../../../Assets/Images/userIon.png";
import QpButton from "../../../Components/QpButton/QpButton";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { forgotPasswordAction } from "../../../Redux/Login/loginAction";
import { WITHOUT_SPACE } from "../../../Constant/AppConstant";

const forgotPasswordSchema = Yup.object().shape({
  email: Yup.string().required("Email is required").email("Email is invalid"),
});

export default function LoginForgotPassword() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(forgotPasswordSchema),
    mode: "onChange",
  });
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const onSubmit = (data) => {
    if (!data) return;
    dispatch(forgotPasswordAction(data, navigate));
  };
  return (
    <QpBoxContainer>
      <Box
        sx={{
          ...commonStyles.smallWhiteContainer,
          ...customCommonStyles.marginLeft0,
          ...customCommonStyles.marginRight0,
        }}
      >
        <QpTypography
          displayText="Forgot Password"
          styleData={{
            ...commonStyles.enterOtpText,
            ...commonStyles.forgotAlign,
          }}
        />
        <QpTypography
          displayText="Enter email address"
          styleData={commonStyles.enterEmailText}
        />
        <InputBase
          id="email"
          name="email"
          placeholder="Email Id"
          startAdornment={
            <img
              src={userIcon}
              alt="userIcon"
              style={customCommonStyles.marginOne}
            />
          }
          required
          sx={{ ...commonStyles.forgotEmailStyle }}
          {...register("email")}
          error={errors.email ? true : false}
        />
        <QpTypography
          styleData={{ ...commonStyles.errorText, ...commonStyles.errorStyle }}
          displayText={errors.email?.message}
        />
        <QpButton
          displayText="Request reset link"
          styleData={{
            ...commonStyles.validateButton,
            ...commonStyles.requestLinkButton,
            ...commonStyles.forgotPwd,
          }}
          textStyle={commonStyles.validateButtontext}
          onClick={handleSubmit(onSubmit)}
        />
      </Box>
    </QpBoxContainer>
  );
}
