import React from "react";
import { Box, Typography } from "@mui/material";
import QpBoxContainer from "../../../Components/QpBoxContainer/QpBoxContainer";
import QpButton from "../../../Components/QpButton/QpButton";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { commonStyles } from "../../../Styles/CommonStyles";
import { useDispatch } from "react-redux";
import { forgotPasswordAction } from "../../../Redux/Login/loginAction";
import { useNavigate, useParams } from "react-router-dom";
import { LOGIN } from "../../../Routes/Routes";

export default function ResendLink() {
  const { email } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  return (
    <>
      <QpBoxContainer>
        <Box sx={commonStyles.smallWhiteContainer}>
          <QpTypography
            displayText="Request Link Sent"
            styleData={{
              ...commonStyles.enterOtpText,
              ...commonStyles.forgotAlign,
            }}
          />
          <Typography sx={commonStyles.sentEmailTextContainer}>
            We have sent an email to&nbsp;
            <Typography sx={commonStyles.emailText}>{atob(email)}</Typography>
            containing your reset password link. Please check your inbox to set
            a new password.
          </Typography>
          <QpButton
            displayText="Resend link"
            styleData={{
              ...commonStyles.validateButton,
              ...commonStyles.requestLinkButton,
            }}
            textStyle={commonStyles.validateButtontext}
            onClick={() =>
              dispatch(forgotPasswordAction({ email: atob(email) }, navigate))
            }
          />
        </Box>
      </QpBoxContainer>
    </>
  );
}
