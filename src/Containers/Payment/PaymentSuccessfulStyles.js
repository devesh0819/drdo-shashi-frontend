import * as colors from "../../Constant/ColorConstant";

export const styles = {
  upperContainer: {
    border: "0.063rem solid #E7E7E7",
    paddingX: "1.875rem",
    paddingY: "2.213rem",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    "@media(max-width:1200px)": {
      flexDirection: "column",
      alignItems: "flex-start",
    },
  },
  detailDiv: {
    width: "45%",
  },
  scheduleText: {
    background: "#F7FAFD",
    border: "1px solid #DBDBDB",
    width: "50%",
    padding: "1rem",
    marginTop: "1rem",
  },
  scheduleStyle: {
    fontWeight: 600,
  },
  paymentSuccessText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "2rem",
    lineHeight: "2.724rem",
    color: colors.GREEN,
    paddingLeft: "0.938rem",
    "@media(max-width:1200px)": {
      fontSize: "1.4rem",
      fontWeight: 600,
      lineHeight: "1.945rem",
    },
    "@media(max-width:800px)": {
      fontSize: "1rem",
      fontWeight: 600,
      lineHeight: "1.2rem",
    },
  },
  pickAssessmentText: {
    height: "2.9rem",
    margin: "0.875rem 0",
    background: colors.SKY_BLUE,
    "&:hover": {
      background: colors.SKY_BLUE,
    },
  },
  thankyouText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1.05rem",
    lineHeight: "1.362rem",
    color: colors.BLACKISH,
    marginTop: "0.438rem",
    "@media(max-width:1200px)": {
      fontSize: "0.9rem",
      fontWeight: 600,
      lineHeight: "1.162rem",
    },
    "@media(max-width:800px)": {
      fontSize: "0.7rem",
      fontWeight: 600,
      lineHeight: "0.9rem",
    },
  },
  horizontalLine: {
    borderTop: "0.063rem solid #DDDDDD",
    marginTop: "1.25rem",
    paddingTop: "1.25rem",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    "@media(max-width:1200px)": {
      justifyContent: "flex-start",
    },
  },
  emailText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "1rem",
    lineHeight: "1.362rem",
    color: "#989898",
    "@media(max-width:1200px)": {
      fontSize: "0.8rem",
      fontWeight: 300,
      lineHeight: "1.162rem",
    },
    "@media(max-width:800px)": {
      fontSize: "0.6rem",
      fontWeight: 300,
      lineHeight: "0.9rem",
    },
  },
  innerContainers: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    "@media(max-width:1200px)": {
      justifyContent: "flex-start",
    },
  },
  rightContainer: {
    width: "50%",
    "@media(max-width:1200px)": {
      marginTop: "1rem",
    },
  },
  paymentFailedText: {
    color: "rgb(226,64,64)",
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "2rem",
    lineHeight: "2.724rem",
    paddingLeft: "0.938rem",
    "@media(max-width:1200px)": {
      fontSize: "1.4rem",
      fontWeight: 600,
      lineHeight: "1.945rem",
    },
    "@media(max-width:800px)": {
      fontSize: "1rem",
      fontWeight: 600,
      lineHeight: "1.2rem",
    },
  },
  errorMessage: {
    color: "rgb(226,64,64)",
    fontWeight: 600,
    marginTop: "30px",
  },
  homepageButton: {
    width: "11rem",
    marginRight: "20px",
  },
  homepageBtnText: {
    fontSize: "1rem",
  },
  displayStyle: {
    display: "flex",
    flexDirection: "column",
  },
};
