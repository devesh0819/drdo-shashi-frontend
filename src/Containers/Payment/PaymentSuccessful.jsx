import React, { useEffect, useRef, useState } from "react";
import QpFormContainer from "../../Components/QpFormContainer/QpFormContainer";
import { HOME, PAYMENT, PAYMENT_SUCCESSFUL } from "../../Routes/Routes";
import { PaymentRow } from "./Payment";
import { Box, Typography } from "@mui/material";
import { styles } from "./PaymentSuccessfulStyles";
import { ReactComponent as IconGreenTickCircle } from "../../Assets/Images/iconGreenTickCircle.svg";
import QpButton from "../../Components/QpButton/QpButton";
import { styles as paymentStyles } from "./PaymentStyles.js";
import QpTypography from "../../Components/QpTypography/QpTypography";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import DateTimePicker from "react-datetime-picker";
import * as moment from "moment";
import dayjs from "dayjs";
import CustomDateTime from "../../Components/CustomDateTime/CustomDateTime";
import { useDispatch, useSelector } from "react-redux";
import SaveNextButtons from "../../Components/SaveNextButtons/SaveNextButtons";
import { showToast } from "../../Components/Toast/Toast";
import { useNavigate, useParams } from "react-router-dom";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../Redux/AllApplications/allApplicationsActions";
import { scheduleApoointmentAction } from "../../Redux/ApplicantForm/applicantFormActions";
import { afterDiscountValue } from "../../Services/commonService";
import { makePaymentAction } from "../../Redux/Payment/paymentActions";
import { APP_STAGE } from "../../Constant/RoleConstant";
import CustomDatePicker from "../../Components/CustomDatePicker/CustomDatePicker";
export const Column = (props) => {
  return (
    <>
      <Box>
        <QpTypography
          displayText={props.label}
          styleData={commonStyles.labelText}
        />
        <QpTypography
          displayText={props.value}
          styleData={commonStyles.valueText}
        />
      </Box>
    </>
  );
};

const PaymentSuccessful = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const currentDate = new Date();
  // const dateTime = currentDate.setDate(currentDate.getDate());
  const dateTime =
    process.env.REACT_APP_TEST_FLAG === "TRUE"
      ? currentDate.setDate(currentDate.getDate())
      : currentDate.setDate(currentDate.getDate() + 10);
  const tempMinDateTime = new Date(dateTime).setHours(9);
  const minDateTime = new Date(tempMinDateTime).setMinutes(0);
  const [pickAssessment, setPickAssessment] = useState(false);
  const runOnce = useRef(false);
  const { id } = useParams();

  const [selectedDateTime, setSelectedDateTime] = useState(minDateTime);

  const generalInfoData = useSelector(
    (state) => state.applicantForm.generalInfoData
  );
  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );

  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(`/payment-successful/${id}`);
    });
  }, []);

  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
    };
  }, [id]);

  // useEffect(() => {
  //   if (
  //     applicationDetail &&
  //     applicationDetail?.application_stage &&
  //     // Object.keys(applicationDetail)?.length > 0 &&
  //     (applicationDetail?.application_stage === APP_STAGE.BASIC ||
  //       applicationDetail?.application_stage === APP_STAGE.OWNERSHIP ||
  //       applicationDetail?.application_stage === APP_STAGE.SCHEDULED ||
  //       applicationDetail?.application_stage === APP_STAGE.PENDING_APPROVAL ||
  //       applicationDetail?.application_stage === APP_STAGE.APPROVED ||
  //       applicationDetail?.application_stage === APP_STAGE.REJECTED ||
  //       applicationDetail?.application_stage === APP_STAGE.PAYMENT_APPROVED ||
  //       applicationDetail?.application_stage ===
  //         APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
  //       applicationDetail?.application_stage === APP_STAGE.FEEDBACK_SUBMITTED)
  //   ) {
  //     navigate(HOME);
  //   }
  // }, [applicationDetail?.application_stage]);

  useEffect(() => {
    if (selectedDateTime) {
      // setSelectedDateTime(minDateTime);
    }
  }, [selectedDateTime]);

  const cancelHandler = () => {
    setSelectedDateTime(minDateTime);
  };

  const saveHandler = () => {
    const formData = new FormData();
    formData.append(
      "assessment_date",
      dayjs(new Date(selectedDateTime)).format("YYYY-MM-DD hh:mm:00")
    );

    dispatch(scheduleApoointmentAction(id, formData, navigate, dispatch));
    // navigate(HOME);
  };

  return (
    <QpFormContainer isStepperRequired={false} changeNavigate={HOME}>
      <Box sx={styles.upperContainer}>
        <Box sx={styles.detailDiv}>
          <PaymentRow
            label="Application Reference No."
            value={`: ${applicationDetail?.application_number}`}
            isPaymentSuccess
          />
          <PaymentRow
            label="Name of Enterprise"
            value={`: ${applicationDetail?.enterprise_name}`}
            isPaymentSuccess
          />
          <PaymentRow
            label="Transaction Amount"
            value={`: Rs. ${Number(
              applicationDetail?.payment_order?.final_amount
            )?.toLocaleString("en-IN", {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}`}
            isPaymentSuccess
          />
          <PaymentRow
            label="Order No."
            value={`: ${applicationDetail?.payment_order?.order_number}`}
            isPaymentSuccess
          />
          <PaymentRow
            label="Date of Payment"
            value={`: ${dayjs(
              applicationDetail?.payment_order?.created_at
            ).format("DD/MM/YYYY")}`}
            isPaymentSuccess
          />
          <PaymentRow
            label="Time of Payment"
            value={`: ${dayjs(
              applicationDetail?.payment_order?.updated_at
            ).format("LT")}`}
            isPaymentSuccess
          />
        </Box>
        <Box sx={styles.rightContainer}>
          <Box sx={styles.innerContainers}>
            <IconGreenTickCircle />
            <Typography sx={styles.paymentSuccessText}>
              Payment Successful!
            </Typography>
          </Box>
          <Box sx={styles.innerContainers}>
            <Typography sx={styles.thankyouText}>
              {`Thank you! Your payment of Rs.${Number(
                applicationDetail?.payment_order?.final_amount
              )?.toLocaleString("en-IN", {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })} has been received`}
            </Typography>
          </Box>
          <Box sx={styles.horizontalLine}>
            <Typography sx={styles.emailText}>
              An email of the reciept will be sent to the registered ID.
            </Typography>
          </Box>
        </Box>
      </Box>
      <Box
        sx={{
          ...paymentStyles.headingContainer,
          ...paymentStyles.makePaymentContainer,
        }}
      >
        <Typography sx={paymentStyles.heading}>Schedule Assessment</Typography>
      </Box>
      <Box>
        <Box sx={commonStyles.displayStyle}>
          <Column
            label="Application Reference No."
            value={`${applicationDetail?.application_number}`}
          />
          {/* <Column
            label="Name of Enterpise"
            value={`${applicationDetail?.enterprise_name}`}
          /> */}
          <Column
            label="Enterprise Name"
            value={`${applicationDetail?.enterprise_name}`}
          />
          <Column label="Assessment Type" value="Online Assessment" />
        </Box>
        {!pickAssessment ? (
          <QpButton
            displayText="Pick Assessment Date"
            styleData={styles.pickAssessmentText}
            textStyle={paymentStyles.makePaymentText}
            onClick={() => setPickAssessment(true)}
          />
        ) : (
          <Box sx={customCommonStyles.marginTopOne}>
            <Typography sx={commonStyles.labelText}>Select date</Typography>
            {/* <CustomDateTime
              minDateTime={minDateTime}
              selectedDateTime={selectedDateTime}
              setSelectedDateTime={setSelectedDateTime}
            /> */}
            <CustomDatePicker
              value={selectedDateTime}
              handleDateChange={setSelectedDateTime}
              minDate={minDateTime}
              disableHighlightToday
            />
            {selectedDateTime && (
              <Box sx={styles.scheduleText}>
                <QpTypography
                  displayText={`Schedule Assessment for : ${dayjs(
                    selectedDateTime
                  ).format("DD/MMM/YYYY")} ? `}
                  styleData={styles.scheduleStyle}
                />
              </Box>
            )}
            <SaveNextButtons
              blueButtonText="Confirm"
              greyButtonText="Cancel"
              onSaveClick={cancelHandler}
              onNextClick={saveHandler}
            />
          </Box>
        )}
      </Box>
    </QpFormContainer>
  );
};

export default PaymentSuccessful;
