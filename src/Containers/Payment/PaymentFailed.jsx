import { Box, Typography } from "@mui/material";
import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import QpFormContainer from "../../Components/QpFormContainer/QpFormContainer";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../Redux/AllApplications/allApplicationsActions";
import { PaymentRow } from "./Payment";
import { styles } from "./PaymentSuccessfulStyles.js";
import WrongTickCircle from "../../Assets/Images/cross.png";
import dayjs from "dayjs";
import { HOME, PAYMENT } from "../../Routes/Routes";
import QpButton from "../../Components/QpButton/QpButton";
import { commonStyles } from "../../Styles/CommonStyles";
import QpTypography from "../../Components/QpTypography/QpTypography";

export default function PaymentFailed() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const runOnce = useRef(false);
  const { id } = useParams();
  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );
  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(`/payment-successful/${id}`);
    });
  }, []);

  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
    };
  }, [id]);
  return (
    <>
      <QpFormContainer isStepperRequired={false} changeNavigate={HOME}>
        <Box sx={styles.upperContainer}>
          <Box sx={styles.detailDiv}>
            <PaymentRow
              label="Application Reference No."
              value={`: ${applicationDetail?.application_number}`}
              isPaymentSuccess
            />
            <PaymentRow
              label="Name of Enterprise"
              value={`: ${applicationDetail?.enterprise_name}`}
              isPaymentSuccess
            />
            <PaymentRow
              label="Transaction Amount"
              value={`: Rs. ${Number(
                applicationDetail?.payment_order?.final_amount
              )?.toLocaleString("en-IN", {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}`}
              isPaymentSuccess
            />
            <PaymentRow
              label="Order No."
              value={`: ${applicationDetail?.payment_order?.order_number}`}
              isPaymentSuccess
            />
            <PaymentRow
              label="Date of Payment"
              value={`: ${dayjs(
                applicationDetail?.payment_order?.created_at
              ).format("DD/MM/YYYY")}`}
              isPaymentSuccess
            />
            <PaymentRow
              label="Time of Payment"
              value={`: ${dayjs(
                applicationDetail?.payment_order?.updated_at
              ).format("LT")}`}
              isPaymentSuccess
            />
          </Box>
          <Box sx={styles.rightContainer}>
            <Box sx={styles.innerContainers}>
              <img
                src={WrongTickCircle}
                alt="failed"
                width="33px"
                height="33px"
              />
              <Typography sx={styles.paymentFailedText}>
                Payment Failed!
              </Typography>
            </Box>
            <Box sx={styles.innerContainers}>
              <Typography sx={styles.thankyouText}>
                {`Sorry! Your payment of Rs.${Number(
                  applicationDetail?.payment_order?.final_amount
                )?.toLocaleString("en-IN", {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                })} was not completed`}
              </Typography>
            </Box>
            <Box sx={styles.horizontalLine}>
              <Typography sx={styles.emailText}>
                You can go back to home page or retry again.
              </Typography>
            </Box>
          </Box>
        </Box>
        <Box sx={styles.displayStyle}>
          <QpTypography
            displayText="We regret to inform you that your payment was not completed. Please review your payment details and retry again, or return to the homepage to continue browsing our website."
            styleData={styles.errorMessage}
          />
          <Box>
            <QpButton
              displayText="Go to Homepage"
              styleData={{
                ...commonStyles.validateButton,
                ...commonStyles.goToHomepageText,
                ...styles.homepageButton,
              }}
              onClick={() => navigate(HOME)}
              textStyle={{
                ...commonStyles.validateButtontext,
                ...styles.homepageBtnText,
              }}
            />
            <QpButton
              displayText="Make Payment"
              styleData={{
                ...commonStyles.validateButton,
                ...commonStyles.goToHomepageText,
                ...styles.homepageButton,
              }}
              onClick={() => navigate(`/payment/${id}`)}
              textStyle={{
                ...commonStyles.validateButtontext,
                ...styles.homepageBtnText,
              }}
            />
          </Box>
        </Box>
        {/* <Box
          sx={{
            ...paymentStyles.headingContainer,
            ...paymentStyles.makePaymentContainer,
          }}
        >
          <Typography sx={paymentStyles.heading}>
            Schedule Assessment
          </Typography>
        </Box>
        <Box>
          <Box sx={commonStyles.displayStyle}>
            <Column
              label="Application Reference No."
              value={`${applicationDetail?.application_number}`}
            />
            <Column
              label="Enterprise Name"
              value={`${applicationDetail?.enterprise_name}`}
            />
            <Column label="Assessment Type" value="Online Assessment" />
          </Box>
        </Box> */}
      </QpFormContainer>
    </>
  );
}
