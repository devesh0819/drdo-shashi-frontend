import * as colors from "../../Constant/ColorConstant";

export const styles = {
  heading: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1.5rem",
    lineHeight: "2.043rem",
    color: colors.BLACKISH,
    "@media(max-width:700px)": {
      fontSize: "0.85rem",
      fontWeight: 600,
      lineHeight: "1.043rem",
    },
  },
  headingContainer: {
    borderBottom: "0.063rem solid #EAEAEA",
    paddingBottom: "1.063rem",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: "1.5rem",
  },
  downloadText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.8rem",
    lineHeight: "1.362rem",
    color: colors.BLACKISH,
    paddingLeft: "0.3rem",
    "@media(max-width:700px)": {
      fontSize: "0.6rem",
      fontWeight: 600,
      lineHeight: "0.9rem",
    },
  },
  downloadButton: {
    background: "#D4D2D2",
    border: "1px solid #A9A9A9",
    borderRadius: "4px",
    textTransform: "none",
    height: "2rem",
    "&:hover": {
      background: "#D4D2D2",
    },
    color: "black",
  },
  label: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.9rem",
    lineHeight: "1.362rem",
    color: colors.BLACKISH,
    "@media(max-width:600px)": {
      fontSize: "0.8rem",
      fontWeight: 600,
    },
  },
  value: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.9rem",
    lineHeight: "1.362rem",
    color: "#454545",
    "@media(max-width:600px)": {
      fontSize: "0.8rem",
      fontWeight: 300,
    },
  },
  buttonStyle: {
    height: "2.813rem",
    width: "12.6rem",
    background: "#D8DCE0",
    "&:hover": {
      background: "#D8DCE0",
    },
    "@media(max-width:700px)": {
      width: "9.6rem",
    },
  },
  blueButtonTextStyle: {
    color: colors.BLACK,
    fontSize: "0.9rem",
    "@media(max-width:700px)": {
      fontSize: "0.6rem",
      fontWeight: 600,
      lineHeight: "0.9rem",
    },
  },
  makePaymentContainer: {
    marginTop: "2.063rem",
  },
  outerBookingContainer: {
    border: "0.063rem solid #DBDBDB",
    maxWidth: "33.44rem",
  },
  headingBookingContainer: {
    paddingLeft: "1.313rem",
    paddingY: "1rem",
    borderBottom: "0.063rem solid #DBDBDB",
    background: "#F2FAFF",
  },
  bookingTitleText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1rem",
    lineHeight: "1.532rem",
    color: "#454545",
    "@media(max-width:600px)": {
      fontSize: "0.8rem",
      lineHeight: "1rem",
    },
  },
  lowerBookingContainer: {
    paddingLeft: "1.313rem",
    paddingRight: "1.313rem",
    paddingTop: "1rem",
    paddingBottom: "1.5rem",
  },
  amountContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginY: "1rem",
    "@media(max-width:600px)": {
      flexDirection: "column",
      alignItems: "flex-start",
    },
  },
  makePaymentButton: {
    width: "100%",
    height: "2.9rem",
    background: colors.SKY_BLUE,
    "&:hover": {
      background: colors.SKY_BLUE,
    },
  },
  makePaymentText: {
    color: colors.WHITE,
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.8rem",
    lineHeight: "1.362rem",
    paddingLeft: "0.3rem",
    "@media(max-width:700px)": {
      fontSize: "0.6rem",
      fontWeight: 600,
      lineHeight: "0.9rem",
    },
  },
  waitingText: {
    color: "green",
    fontWeight: 600,
    fontSize: "1.125rem",
    lineHeight: "1.5",
  },
  waitingMargin: {
    marginTop: "1.538rem",
  },
  waitingRedColor: {
    color: "red",
  },
  reason: {
    color: "red",
    fontWeight: 500,
  },
  reasonText: {
    color: "red",
    fontWeight: 400,
  },
};
