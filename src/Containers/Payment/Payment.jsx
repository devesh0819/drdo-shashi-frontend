import React, { useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { Box, Typography, Grid, Button, InputBase } from "@mui/material";

import { ReactComponent as IconCircularBack } from "../../Assets/Images/iconCircularBack.svg";
import SaveNextButtons from "../../Components/SaveNextButtons/SaveNextButtons";
import QpButton from "../../Components/QpButton/QpButton";
import QpTypography from "../../Components/QpTypography/QpTypography";
import PaymentBreakupDetails from "../Admin/AllApplications/PaymentBreakupDetails/PaymentBreakupDetails";
import Row from "../../Components/Row/Row";
import CustomCheckboxContent from "../../Components/CustomCheckboxContent/CustomCheckboxContent";
import QpRadioButton from "../../Components/QpRadioButton/QpRadioButton";
import QpInputLabel from "../../Components/QpInputLabel/QpInputLabel";
import { HOME } from "../../Routes/Routes";
import { APP_STAGE } from "../../Constant/RoleConstant";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../Redux/AllApplications/allApplicationsActions";
import {
  getPaymentEncRequest,
  getPaymentEncRequestAction,
  makePaymentAction,
} from "../../Redux/Payment/paymentActions";

import { showToast } from "../../Components/Toast/Toast";
import { yesNo } from "../../Constant/AppConstant";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { styles } from "./PaymentStyles";
import TaxInformation from "../../Components/TaxInformation/TaxInformation";

export const PaymentRow = (props) => {
  return (
    <Grid
      container
      // columnSpacing={props.isInBox ? "4.5rem" : "7.5rem"}
      sx={{ marginBottom: props.isInBox ? "0.2rem" : "0.575rem" }}
    >
      <Grid item sm={props.isPaymentSuccess ? 7 : 5} xs={12}>
        <Typography sx={styles.label}>{props.label}</Typography>
      </Grid>
      <Grid item sm={props.isPaymentSuccess ? 5 : 7} xs={12}>
        <Typography sx={styles.value}>{props.value}</Typography>
      </Grid>
    </Grid>
  );
};
const Payment = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id, status } = useParams();
  const [disableButton, setDisableButton] = useState(false);
  const [isAcceptProceed, setIsAcceptProceed] = useState(false);
  const [taxValue, setTaxValue] = useState();
  const [isSubmit, setIsSubmit] = useState(false);
  const [taxInfo, setTaxInfo] = useState({});
  const taxInfoRef = useRef(taxInfo);
  const isDone = useRef(false);

  const [data, setData] = useState({
    gst_number: "",
    tan_number: "",
    gstApplicable: "0",
    tdsApplicable: "0",
  });

  const runOnce = useRef(false);
  const encRequestOnce = useRef(false);
  // const applicationStatus = useSelector(
  //   (state) => state.applicantForm.submitApplicationStatus
  // );
  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );

  const encRequest = useSelector((state) => state.payment.paymentEncRequest);
  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
      // dispatch(getPaymentEncRequestAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
      // dispatch(getPaymentEncRequest(null));
    };
  }, [id]);

  useEffect(() => {
    if (
      id &&
      applicationDetail &&
      applicationDetail?.application_stage &&
      // Object.keys(applicationDetail)?.length > 0 &&
      (applicationDetail?.application_stage === APP_STAGE.BASIC ||
        applicationDetail?.application_stage === APP_STAGE.OWNERSHIP ||
        applicationDetail?.application_stage === APP_STAGE.SCHEDULED ||
        applicationDetail?.application_stage === APP_STAGE.PAYMENT_DONE ||
        applicationDetail?.application_stage ===
          APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
        applicationDetail?.application_stage === APP_STAGE.FEEDBACK_SUBMITTED)
    ) {
      // navigate(HOME);
    }
  }, [id, applicationDetail?.application_stage]);

  // useEffect(() => {
  //   if (
  //     status &&
  //     status === APP_STAGE.PAYMENT_APPROVED &&
  //     encRequestOnce.current === false
  //   ) {
  //     dispatch(getPaymentEncRequestAction(id));
  //   }
  //   return () => {
  //     encRequestOnce.current = true;
  //     dispatch(getPaymentEncRequest(null));
  //   };
  // }, [status]);

  useEffect(() => {
    makePaymentHandler();
  }, [taxInfo]);

  const nextHandler = () => {
    setIsSubmit(true);
    isDone.current = true;
  };
  const updateState = (newState) => {
    taxInfoRef.current = newState;
    setTaxInfo(newState);
    setIsSubmit(false);
  };

  const getData = (data) => {
    updateState({
      ...taxInfoRef.current,
      ...data,
    });
  };

  const editHandler = () => {
    navigate(`/general-info/${id}`);
  };

  const makePaymentHandler = (paymentData) => {
    const { gst_number, tan_number } = taxInfo;

    if (!isDone.current) {
      return;
    }
    isDone.current = false;
    if (data?.gstApplicable == 1 && !gst_number) {
      return;
    }
    if (data?.tdsApplicable == 1 && !tan_number) {
      return;
    }

    if (!taxValue) {
      showToast(`Please select TDS value`, "error");
      return;
    }
    // if (data?.gstApplicable && !gst_number) {
    //   showToast(`GST Number is required`, "error");
    //   return;
    // }
    // if (data?.tdsApplicable && !tan_number) {
    //   showToast(`TAN Number is required`, "error");
    //   return;
    // }
    let query = { tds: data?.tdsApplicable == 1 ? taxValue : 0 };
    if (data?.gstApplicable == 1 && gst_number) {
      query = { ...query, gst_number: gst_number };
    }
    if (data?.tdsApplicable == 1 && tan_number) {
      query = { ...query, tan_number: tan_number };
    }

    dispatch(
      getPaymentEncRequestAction(
        id,
        applicationDetail?.order?.order_number,
        query
      )
    );
    setDisableButton(true);
  };

  return (
    // <QpFormContainer isStepperRequired={false} changeNavigate={HOME}>
    //   <Box>
    //     <Box sx={styles.headingContainer}>
    //       <Typography sx={styles.heading}>Review application</Typography>
    //     </Box>
    <Box>
      <Box sx={commonStyles.headerOuterContainer}>
        <Box sx={commonStyles.topBackButtonHeader}>
          <Button
            disableRipple
            sx={commonStyles.backButton}
            onClick={() => navigate(HOME)}
          >
            <IconCircularBack />
          </Button>
          <Typography style={commonStyles.titleBackText}>
            Review application
          </Typography>
        </Box>
      </Box>
      {/* <Box> */}
      <Box sx={customCommonStyles.fieldContainer}>
        <PaymentRow
          label="Application Reference No."
          value={`: ${applicationDetail?.application_number}`}
        />
        {/* <PaymentRow
          label="Name of Enterprise"
          value={`: ${applicationDetail?.enterprise_name}`}
        /> */}
        <PaymentRow
          label="Enterprise Name"
          value={`: ${applicationDetail?.enterprise_name}`}
        />
        <PaymentRow
          label="Address"
          value={`: ${applicationDetail?.registered_address}`}
        />
        <PaymentRow
          label="Phone Number"
          value={`: +91 ${applicationDetail?.registered_mobile_number}`}
        />
        {/* </Box> */}
        <SaveNextButtons
          blueButtonText={"Edit Application"}
          greyButtonText="View Application"
          greyButtonStyle={styles.buttonStyle}
          blueButtonStyle={styles.buttonStyle}
          blueButtonTextStyle={styles.blueButtonTextStyle}
          greyButtonTextStyle={styles.blueButtonTextStyle}
          blueButtonRequired={
            applicationDetail &&
            (applicationDetail?.application_stage === APP_STAGE.BASIC ||
              applicationDetail?.application_stage === APP_STAGE.FINANCIAL ||
              applicationDetail?.application_stage === APP_STAGE.OWNERSHIP)
              ? true
              : false
          }
          onNextClick={editHandler}
          onSaveClick={() => navigate(`/home/${id}`)}
        />
        {applicationDetail &&
          (applicationDetail?.application_stage ===
            APP_STAGE.PENDING_APPROVAL ||
            applicationDetail?.application_stage === APP_STAGE.APPROVED) && (
            <Box
            // sx={{ minHeight: "calc(100vh - 32.75rem)" }}
            >
              <Box sx={commonStyles.horizontalLine} />
              <QpTypography
                displayText="Your application has been submitted successfully. Details of payment and further process will  be shared within 7 working days."
                styleData={{
                  ...styles.waitingText,
                  ...styles.waitingMargin,
                }}
                textStyle={styles.waitingText}
              />
            </Box>
          )}{" "}
        {applicationDetail &&
          applicationDetail?.application_stage === APP_STAGE.REJECTED && (
            <Box
            // sx={{ minHeight: "calc(100vh - 32.75rem)" }}
            >
              <Box sx={commonStyles.horizontalLine} />
              <QpTypography
                displayText="Your application has been rejected by DRDO."
                styleData={{
                  ...styles.waitingText,
                  ...styles.waitingMargin,
                  ...styles.waitingRedColor,
                }}
                textStyle={styles.waitingText}
              />
              <Box sx={commonStyles.displayFlex}>
                {/* <QpTypography
                  displayText="Reason: "
                  styleData={styles.reason}
                /> */}
                <QpTypography
                  displayText={`Reason: ${applicationDetail?.reject_reason}`}
                  styleData={styles.reasonText}
                />
              </Box>
            </Box>
          )}{" "}
        {applicationDetail &&
          applicationDetail?.application_stage ===
            APP_STAGE.PAYMENT_APPROVED && (
            <>
              <Box
                sx={{
                  ...styles.headingContainer,
                  ...styles.makePaymentContainer,
                }}
              >
                <Typography sx={styles.heading}>Undertaking</Typography>
              </Box>
              <Box>
                <Typography>
                  I/We (applicant enterprise), undertake that:
                </Typography>
                <br />
                <Typography sx={styles.undertakingList}>
                  1. By agreeing to proceed and make relevant payment, I/We
                  undertake that I/we have read and understood the requirements
                  of the Certification for which the payment is being made and
                  I/we takes full responsibility of the same.
                  <br />
                  2. I/we undertake that our enterprise/unit fulfils all the
                  relevant statutory and regulatory requirements and
                  non-fulfilment of the same may lead to the withdrawal of
                  certification.
                  <br />
                  3. I/we understand that in case our enterprise/unit gets
                  blacklisted by any government authority, it may also lead to
                  the withdrawal of certification.
                  <br />
                  4. I/we understand that the amount once remitted is
                  non-refundable, non-adjustable & non-transferable.
                  <br />
                  5. I/we understand that mere submission of relevant payment
                  does not guarantee Certification. The Certification will only
                  be accorded after completion of due process.
                </Typography>
                <CustomCheckboxContent
                  checked={isAcceptProceed}
                  setChecked={setIsAcceptProceed}
                  displayText="Accept & Proceed"
                />
              </Box>
              <TaxInformation
                data={data}
                setData={setData}
                submit={isSubmit}
                setPaymentData={getData}
              />
              <>
                <Box
                  sx={{
                    ...styles.headingContainer,
                    ...styles.makePaymentContainer,
                  }}
                >
                  <Typography sx={styles.heading}>Make Payment</Typography>
                </Box>
                <Box sx={styles.outerBookingContainer}>
                  <Box sx={styles.headingBookingContainer}>
                    <Typography sx={styles.bookingTitleText}>
                      Booking Information
                    </Typography>
                  </Box>
                  <Box sx={styles.lowerBookingContainer}>
                    <Row label="Pay To" value="Quality Council of India" />
                    <PaymentBreakupDetails
                      applicationDetail={applicationDetail}
                      tdsFeeTextRequired={true}
                      setTaxValue={setTaxValue}
                      taxValue={taxValue}
                      tdsApplicable={Number(data?.tdsApplicable)}
                    />
                    {/* <Row
                    label="Order No."
                    value={`: ${applicationDetail?.order?.order_number}`}
                    isInBox
                  />
                  <Row
                    label="Amount"
                    value={`: Rs. ${applicationDetail?.order?.amount}`}
                    isInBox
                  />
                  {applicationDetail?.order?.discount > 0 && (
                    <Row
                      label="Subsidy"
                      value={`: ${applicationDetail?.order?.discount}%`}
                      isInBox
                    />
                  )}
                  <Box sx={styles.amountContainer}>
                    <Typography sx={styles.bookingTitleText}>
                      Total Amount to be paid
                    </Typography>
                    <Typography sx={styles.bookingTitleText}>
                      {`Rs ${afterDiscountValue(
                        applicationDetail?.order?.amount,
                        applicationDetail?.order?.discount
                      )}`}
                    </Typography>
                  </Box> */}
                    <QpButton
                      displayText="Make Payment"
                      styleData={styles.makePaymentButton}
                      textStyle={styles.makePaymentText}
                      // onClick={handleSubmit(makePaymentHandler)}
                      onClick={nextHandler}
                      isDisable={isAcceptProceed ? false : true}
                    />
                  </Box>
                </Box>
              </>
            </>
          )}
      </Box>
    </Box>
  );
};

export default Payment;
