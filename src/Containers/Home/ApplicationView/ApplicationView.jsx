import React, { useEffect, useRef } from "react";
import {
  Box,
  Typography,
  Button,
  Grid,
  Alert,
  AlertTitle,
} from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import { HOME } from "../../../Routes/Routes";
import { ReactComponent as IconCircularBack } from "../../../Assets/Images/iconCircularBack.svg";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import { useDispatch, useSelector } from "react-redux";
import {
  getApplicationDetailAction,
  getApplicationDetail,
} from "../../../Redux/AllApplications/allApplicationsActions";
import QpButton from "../../../Components/QpButton/QpButton";
import { APP_STAGE } from "../../../Constant/RoleConstant";
import * as _ from "lodash";
import CustomApplicatonView from "../../../Components/CustomApplicationView/CustomApplicationView";
import CustomMessage from "../../../Components/CustomMessage/CustomMessage";

export default function ApplicationView() {
  const runOnce = useRef(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();

  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );

  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
    };
  }, [id]);

  return (
    <>
      <Box
        sx={{
          ...commonStyles.headerOuterContainer,
        }}
      >
        <Box sx={commonStyles.topBackButtonHeader}>
          <Button
            disableRipple
            sx={commonStyles.backButton}
            onClick={() => navigate(HOME)}
          >
            <IconCircularBack />
          </Button>
          <Typography style={commonStyles.titleBackText}>
            Application Detail
          </Typography>
        </Box>
        {applicationDetail?.application_stage ===
          APP_STAGE.PAYMENT_APPROVED && (
          <QpButton
            displayText="Make Payment"
            styleData={commonStyles.blueButton}
            textStyle={commonStyles.blueButtonText}
            onClick={() =>
              navigate(`/payment/${applicationDetail?.application_uuid}`)
            }
          />
        )}
        {applicationDetail?.application_stage === APP_STAGE.PAYMENT_DONE && (
          <QpButton
            displayText="Schedule appointment"
            styleData={{
              ...commonStyles.blueButton,
              ...commonStyles.scheduleAppointmentBtn,
            }}
            textStyle={commonStyles.blueButtonText}
            onClick={() =>
              navigate(
                `/payment-successful/${applicationDetail?.application_uuid}`
              )
            }
          />
        )}
        {applicationDetail?.application_stage === APP_STAGE.SCHEDULED &&
          applicationDetail?.assess_reschedule_count === null && (
            <QpButton
              displayText="Reschedule appointment"
              styleData={{
                ...commonStyles.blueButton,
                ...commonStyles.rescheduleAppointmentBtn,
              }}
              textStyle={commonStyles.blueButtonText}
              onClick={() =>
                navigate(
                  `/payment-successful/${applicationDetail?.application_uuid}`
                )
              }
            />
          )}
      </Box>
      <Box
        sx={{
          ...customCommonStyles.fieldContainer,
          ...((applicationDetail?.application_stage ===
            APP_STAGE.PAYMENT_APPROVED ||
            applicationDetail?.application_stage === APP_STAGE.PAYMENT_DONE) &&
            commonStyles.vendorAppView),
        }}
      >
        {applicationDetail?.application_stage === APP_STAGE.REJECTED && (
          <Box sx={commonStyles.customMessageContainer}>
            <CustomMessage
              type="error"
              alertTitle="Your application has been reject by DRDO"
              subHeading="Reason:"
              text={applicationDetail?.reject_reason}
            />
          </Box>
        )}
        <CustomApplicatonView applicationDetail={applicationDetail} />
      </Box>
    </>
  );
}
