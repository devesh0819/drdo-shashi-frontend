import React, { useEffect, useState } from "react";
import CustomTable from "../../Components/CustomTable/CustomTable";
import { ReactComponent as IconCircularDownload } from "../../Assets/Images/iconCircularDownload.svg";
import { ReactComponent as IconPencil } from "../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconMenuSearch } from "../../Assets/Images/iconMenuSearch.svg";
import { Button, Box, Tooltip, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import {
  downloadAction,
  downloadReportAction,
  getAllApplicationsAction,
} from "../../Redux/AllApplications/allApplicationsActions";
import * as moment from "moment";
import { useNavigate } from "react-router-dom";
import QpTypography from "../../Components/QpTypography/QpTypography";
import QpDialog from "../../Components/QpDialog/QpDialog";
import { commonStyles } from "../../Styles/CommonStyles";
import { APP_STAGE } from "../../Constant/RoleConstant";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";
import { getAppStatus } from "../../Services/commonService";
import { FEEDBACK, HOME } from "../../Routes/Routes";
import { ReactComponent as IconCircularDownloadDisable } from "../../Assets/Images/iconCircularDisable.svg";
import { ReactComponent as IconPencilDisable } from "../../Assets/Images/iconPencilDisable.svg";
import CommentIcon from "@mui/icons-material/Comment";
import {
  appStatus,
  paymentStatus,
  ratingArray,
  vendorStatus,
} from "../../Constant/AppConstant";
import * as dayjs from "dayjs";
import FilterComponent from "../../Components/FilterComponent/FilterComponent";
import ContentPasteSearchIcon from "@mui/icons-material/ContentPasteSearch";
import DownloadForOfflineOutlinedIcon from "@mui/icons-material/DownloadForOfflineOutlined";
import WorkspacePremiumIcon from "@mui/icons-material/WorkspacePremium";
import { ICON_BLUE } from "../../Constant/ColorConstant";
import FeedbackOutlinedIcon from "@mui/icons-material/FeedbackOutlined";

const Home = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [rowData, setRowData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [openFilterDialog, setOpenFilterDialog] = useState(false);
  const [noOfFiltersApplied, setNoOfFiltersApplied] = useState(0);
  const [alreadySelectedFilters, setAlreadySelectedFilters] = useState({});
  const [payloadState, setPayloadState] = useState();

  const allApplications = useSelector(
    (state) => state.allApplications.allApplications
  );

  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(HOME);
    });
  }, []);

  const handleEdit = (appStatus, appId) => {
    if (appStatus === APP_STAGE.BASIC) {
      navigate(`/step2-details/${appId}`);
    }
    if (appStatus === APP_STAGE.FINANCIAL) {
      navigate(`/step3-details/${appId}`);
    }
    if (appStatus === APP_STAGE.OWNERSHIP) {
      navigate(`/preview/${appId}`);
    }
    // if (appStatus === APP_STAGE.OWNERSHIP) {
    //   navigate(`/preview/${appId}`);
    // }
    // if (appStatus === APP_STAGE.BASIC) {
    //   navigate(`/step2-details/${appId}`);
    // }
  };

  const findFiltersApplied = (payload) => {
    let count = Object.keys(payload).length;
    filterOptions?.forEach((option) => {
      if (option.filterType === "Date" && payload[option?.keysArray[0]]) {
        count--;
      }
    });
    return count;
  };

  const saveHandler = (payload) => {
    // setOpenFilterDialog(false);
    // setAlreadySelectedFilters(payload);
    // let payloadCopy = { ...payload };
    // filterOptions.forEach((option) => {
    //   if (option?.filterType === "Date") {
    //     payloadCopy[option?.keysArray[0]] =
    //       payload[option?.keysArray[0]] &&
    //       dayjs(payload[option?.keysArray[0]]).format("DD/MM/YYYY");
    //     payloadCopy[option?.keysArray[1]] =
    //       payload[option?.keysArray[1]] &&
    //       dayjs(payload[option?.keysArray[1]]).format("DD/MM/YYYY");
    //   }
    //   if (option?.filterType === "Dropdown") {
    //     payloadCopy[option.key] = payload[option.key]?.value;
    //   }
    //   if (option?.filterType === "Search") {
    //     if (payloadCopy[option.key]?.length === 0) {
    //       delete payloadCopy[option.key];
    //       delete payload[option.key];
    //     }
    //   }
    // });
    // setNoOfFiltersApplied(findFiltersApplied(payload));

    dispatch(getAllApplicationsAction({ start: pageNumber, ...payload }));
    setPageNumber(1);
    setPayloadState(payload);
  };

  const columnDefs = [
    { field: "Application Number" },
    { field: "Unit/Enterprise Name", ellipsisClass: true, minWidth: "10rem" },
    { field: "Application Date" },
    {
      field: "Payment Status",
      onClick: (row) => {
        row["Payment Status"] === "Complete" &&
          navigate(`/payment-history/payment-complete-details/${row.id}`);
      },
    },
    { field: "Application Status" },
    { field: "Assessment Date" },
    { field: "Rating", width: "5%" },
    // { field: "Status" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Box sx={commonStyles.vendorApplicationIcon}>
            <Box className="icons" sx={commonStyles.displayNone}>
              <Button
                sx={commonStyles.iconButtonStyle}
                onClick={() => navigate(`/home/${row.id}`)}
              >
                <Tooltip title="View" arrow>
                  <ContentPasteSearchIcon style={commonStyles.iconColor} />
                </Tooltip>
              </Button>
              {row.status !== APP_STAGE.BASIC &&
              row.status !== APP_STAGE.FINANCIAL &&
              row.status !== APP_STAGE.OWNERSHIP ? (
                <Button
                  sx={commonStyles.iconButtonStyle}
                  onClick={() =>
                    row.status === APP_STAGE.FEEDBACK_SUBMITTED &&
                    navigate(`/home/feedback-details/${row.id}`)
                  }
                >
                  <Tooltip title="Feedback" arrow>
                    {/* <IconPencil /> */}
                    <FeedbackOutlinedIcon
                      style={
                        row.status === APP_STAGE.FEEDBACK_SUBMITTED
                          ? commonStyles.iconColor
                          : commonStyles.greyIconColor
                      }
                    />
                  </Tooltip>
                </Button>
              ) : (
                <Button
                  sx={commonStyles.iconButtonStyle}
                  onClick={() => handleEdit(row.status, row.id)}
                >
                  <Tooltip title="Edit" arrow>
                    {/* <IconPencil /> */}
                    <BorderColorOutlinedIcon
                      style={
                        row.status !== APP_STAGE.BASIC &&
                        row.status !== APP_STAGE.FINANCIAL &&
                        row.status !== APP_STAGE.OWNERSHIP
                          ? commonStyles.greyIconColor
                          : commonStyles.iconColor
                      }
                    />
                  </Tooltip>
                </Button>
              )}

              <Button sx={commonStyles.iconButtonStyle}>
                <Tooltip title="Report" arrow>
                  <DownloadForOfflineOutlinedIcon
                    style={
                      row.status ===
                        APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                      row.status === APP_STAGE.FEEDBACK_SUBMITTED
                        ? commonStyles.iconColor
                        : commonStyles.greyIconColor
                    }
                    onClick={() =>
                      (row.status ===
                        APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                        row.status === APP_STAGE.FEEDBACK_SUBMITTED) &&
                      dispatch(
                        downloadReportAction(
                          row.id,
                          row.application_number,
                          dispatch,
                          "report"
                        )
                      )
                    }
                  />
                </Tooltip>
              </Button>
              <Button
                sx={{ ...commonStyles.iconButtonStyle, marginRight: "0" }}
              >
                <Tooltip title="Certificate" arrow>
                  <WorkspacePremiumIcon
                    style={
                      row.status ===
                        APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                      row.status === APP_STAGE.FEEDBACK_SUBMITTED
                        ? commonStyles.iconColor
                        : commonStyles.greyIconColor
                    }
                    onClick={() =>
                      (row.status ===
                        APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
                        row.status === APP_STAGE.FEEDBACK_SUBMITTED) &&
                      dispatch(
                        downloadReportAction(
                          row.id,
                          row.application_number,
                          dispatch,
                          "certificate"
                        )
                      )
                    }
                  />
                </Tooltip>
              </Button>
            </Box>
          </Box>
        );
      },
    },
  ];

  useEffect(() => {
    if (allApplications) {
      const dataSet = allApplications?.applications?.map((application) => {
        return {
          "Application Number": application.application_number,
          "Unit/Enterprise Name": application.enterprise_name,
          "Application Date": moment(application.application_date).format(
            "D MMM,YYYY"
          ),
          "Payment Status":
            application?.payment_status === "Pending" ||
            application?.payment_status === ""
              ? "Yet to pay"
              : application?.payment_status,
          "Application Status": getAppStatus(application.status),
          "Assessment Date": application.assessment_date
            ? moment(application.assessment_date).format("D MMM,YYYY")
            : "Yet to be  scheduled",
          Rating: application?.rating || "Yet to be assessed",
          id: application.application_uuid,
          status: application.status,
          application_number: application.application_number,
        };
      });
      setRowData(dataSet);
    }
  }, [allApplications]);

  useEffect(() => {
    if (pageNumber) {
      dispatch(
        getAllApplicationsAction({ start: pageNumber, ...payloadState })
      );
    }
  }, [pageNumber]);
  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const filterOptions = [
    {
      filterId: 1,
      displayName: "Application No.",
      filterType: "Search",
      key: "application_no",
      labelId: "Application No.",
      label: "Application No.",
      filterName: "applicationNumber",
    },
    {
      filterId: 2,
      displayName: "Enterprise Name",
      filterType: "Search",
      key: "enterprise_name",
      labelId: "Enterprise Name",
      label: "Enterprise Name",
      filterName: "enterpriseName",
    },
    {
      filterId: 3,
      displayStartName: "Application Start Date",
      displayLastName: "Application Last Date",
      filterType: "Date",
      keysArray: ["application_start_date", "application_end_date"],
      filterName: "applicationDate",
    },
    {
      filterId: 4,
      displayName: "Payment Status",
      filterType: "Dropdown",
      key: "payment_status",
      dataArray: paymentStatus,
      labelId: "Payment Status",
      label: "Payment Status",
      filterName: "paymentStatus",
    },
    {
      filterId: 5,
      displayName: "Application Status",
      filterType: "Dropdown",
      key: "application_status",
      dataArray: vendorStatus,
      labelId: "Application Status",
      label: "Application Status",
      filterName: "applicationStatus",
    },
    {
      filterId: 6,
      displayName: "Assessment Date",
      displayStartName: "Assessment Start Date",
      displayLastName: "Assessment Last Date",
      filterType: "AssessmentDate",
      keysArray: ["assessment_start_date", "assessment_end_date"],
      filterName: "assessmentDate",
    },
    {
      filterId: 7,
      displayName: "Rating",
      filterType: "Dropdown",
      key: "rating",
      dataArray: ratingArray,
      labelId: "Rating",
      label: "Rating",
      filterName: "rating",
    },
  ];

  const exportButtonClick = () => {
    const url = `${process.env.REACT_APP_BASE_URL}/applicant/applications?is_excel=1&excel_type=applicant_applications`;
    dispatch(
      downloadAction("VendorApplications", dispatch, url, "doc", "xlsx")
    );
  };

  return (
    <>
      {allApplications?.total === 0 && !payloadState ? (
        <QpTypography
          displayText="Welcome to your dashboard"
          styleData={commonStyles.noData}
        />
      ) : (
        <>
          {openFilterDialog && (
            <FilterComponent
              saveHandler={saveHandler}
              filterOptions={filterOptions}
              alreadySelectedFilters={alreadySelectedFilters}
              setOpenFilterDialog={setOpenFilterDialog}
              setNoOfFiltersApplied={setNoOfFiltersApplied}
              setAlreadySelectedFilters={setAlreadySelectedFilters}
            />
          )}
          <CustomTable
            columnDefs={columnDefs}
            rowData={rowData}
            title="All Applications"
            hasPagination
            handlePagination={handlePagination}
            currentPage={pageNumber}
            totalValues={allApplications?.total}
            styleData={commonStyles.tableHeight}
            hasFilter
            onClickFilter={() => setOpenFilterDialog(!openFilterDialog)}
            noOfFiltersApplied={noOfFiltersApplied}
            buttonsDivStyle={commonStyles.divFlexStyle}
            exportButtonClick={exportButtonClick}
            hasExport={true}
            saveHandler={saveHandler}
            filterOptions={filterOptions}
            alreadySelectedFilters={alreadySelectedFilters}
            setOpenFilterDialog={setOpenFilterDialog}
            setNoOfFiltersApplied={setNoOfFiltersApplied}
            setAlreadySelectedFilters={setAlreadySelectedFilters}
          />
        </>
      )}
    </>
  );
};

export default Home;
