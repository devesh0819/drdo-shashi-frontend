import { Box, Button, Typography } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { commonStyles } from "../../Styles/CommonStyles";
import { ReactComponent as IconCircularBack } from "../../Assets/Images/iconCircularBack.svg";
import FeedbackSurvey from "../Admin/ManageAssessments/FeedbackSurvey/FeedbackSurvey";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../Redux/AllApplications/allApplicationsActions";
import { HOME } from "../../Routes/Routes";
import { getFeedbackConfigurationAction } from "../../Redux/Admin/FeedbackConfiguration/feedbackConfigurationActions";

export default function FeedbackDetails() {
  const runOnce = useRef(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();
  const [feedbackConfigurationData, setFeedbackConfigurationData] = useState(
    []
  );

  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );
  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
    };
  }, [id]);

  useEffect(() => {
    if (applicationDetail?.feedback?.feedback_payload) {
      let feedbackJson = JSON.parse(
        applicationDetail?.feedback?.feedback_payload
      );
      if (Object.keys(feedbackJson)?.length > 0) {
        setFeedbackConfigurationData(Object.values(feedbackJson));
      }
    }
  }, [applicationDetail?.feedback?.feedback_payload]);

  return (
    <>
      <Box sx={commonStyles.headerOuterContainer}>
        <Box sx={commonStyles.topBackButtonHeader}>
          <Button
            disableRipple
            sx={commonStyles.backButton}
            onClick={() => navigate(HOME)}
          >
            <IconCircularBack />
          </Button>
          <Typography style={commonStyles.titleBackText}>Feedback</Typography>
        </Box>
      </Box>
      <Box sx={commonStyles.feedbackDetails}>
        {/* <FeedbackSurvey feedbackData={applicationDetail?.feedback} /> */}
        <FeedbackSurvey
          feedbackData={feedbackConfigurationData}
          feedbackComment={applicationDetail?.feedback?.feedback_comment}
        />
      </Box>
    </>
  );
}
