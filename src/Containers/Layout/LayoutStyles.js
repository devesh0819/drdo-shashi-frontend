export const styles = {
  drdoLogo: {
    objectFit: "contain",
    width: "16rem",
    height: "4rem",
  },
  samarLogo: {
    // height: "4rem",
    height: "6.5rem",
    position: "relative",
    top: "-22px",
    left: "0",
  },
  qciLogo: {
    height: "4rem",
  },
  containerHeight: {
    height: "calc(100vh - 7.1rem)",
  },
  openSidePosition: {
    position: "relative",
  },
  rightArrowBox: {
    color: "white",
    backgroundColor: "#255491",
    position: "absolute",
    top: "9.55rem",
    width: "24px",
    height: "24px",
    // padding: "0.2rem",
    borderRadius: "0 4px 4px 0",
    cursor: "pointer",
    // left: "-22px",
    "&:hover": {
      color: "white",
      backgroundColor: "#255491",
    },
  },
};
