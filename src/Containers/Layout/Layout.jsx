import React, { useState } from "react";
import { Box, Button, Typography } from "@mui/material";
import drdoLogo from "../../Assets/Images/drdoLogo.png";
import drdoLogoText from "../../Assets/Images/drdoLogoText.png";
import drdoLogoTransparent from "../../Assets/Images/drdoLogoTransparent.png";
import zedLogo from "../../Assets/Images/zedLogo.png";
import samarLogo from "../../Assets/Images/samar_logo.jpg";
import samarTransparent from "../../Assets/Images/samarTransparent.png";
import transparentSamar from "../../Assets/Images/transparentSamar.png";
import samarLogoWithText from "../../Assets/Images/samarLogoWithText.png";
import samarLogoOne from "../../Assets/Images/samarLogoOne.png";

import qciLogo from "../../Assets/Images/quci-logo.png";
import qciTransparent from "../../Assets/Images/qciTransparent.png";

import { commonStyles } from "../../Styles/CommonStyles";
import ContainerLayout from "../ContainerLayout/ContainerLayout";
import { useLocation, useNavigate } from "react-router-dom";
import { styles } from "./LayoutStyles.js";
import { ABOUT_US, CERTIFIED_ENTERPRISES, STANDARD } from "../../Routes/Routes";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";
import ForwardRoundedIcon from "@mui/icons-material/ForwardRounded";

export default function Layout(props) {
  const { children } = props;
  const isAuthenticated = localStorage.getItem("token");
  const location = useLocation();
  const navigate = useNavigate();
  const [openSidebar, setOpenSidebar] = useState(true);

  return (
    <Box
      sx={{
        ...commonStyles.bodyContainer,
        ...(!props.isFooterNeeded && commonStyles.fullHeight),
        ...(!openSidebar && styles.openSidePosition),
      }}
    >
      {props.isHeaderNeeded && (
        <Box sx={commonStyles.headerContainer}>
          <Box sx={commonStyles.displayStyle}>
            <Box sx={commonStyles.drdoHeaderImage}>
              <img
                src={drdoLogoTransparent}
                alt="drdoLogo"
                style={styles.drdoLogo}
                // width={{
                //   "@media(maxWidth: 600px)": {
                //     width: "20rem",
                //   },
                // }}
              />
            </Box>
            <Box sx={commonStyles.samarHeaderImage}>
              <img
                src={samarLogoOne}
                alt="samarLogo"
                style={styles.samarLogo}
              />
            </Box>
            <Box sx={commonStyles.zedHeaderImage}>
              <img src={qciTransparent} alt="qciLogo" style={styles.qciLogo} />
              {/* <QCILogo
              width={5}
              height={5}
              style={{ width: "5rem", height: "5rem" }}
            /> */}
            </Box>
          </Box>
        </Box>
      )}
      {!openSidebar && (
        <Box
          onClick={() => setOpenSidebar(!openSidebar)}
          sx={styles.rightArrowBox}
        >
          <ForwardRoundedIcon />
        </Box>
      )}
      {isAuthenticated && props.isSidebarNeeded ? (
        <ContainerLayout
          role={props.role}
          openSidebar={openSidebar}
          setOpenSidebar={setOpenSidebar}
          isFooterNeeded={props.isFooterNeeded}
        >
          {" "}
          {children}
        </ContainerLayout>
      ) : (
        <Box
          sx={location.pathname.includes("feedback") && styles.containerHeight}
        >
          {children}
        </Box>
      )}
      {props.isFooterNeeded && (
        <Box sx={{ ...commonStyles.footerStyle, ...commonStyles.displayStyle }}>
          <Typography sx={[commonStyles.textStyle]}>
            Copyright © 2023, DRDO, Ministry of Defence, Government of India
          </Typography>
          <Box sx={{ ...commonStyles.displayStyle }}>
            <Typography
              sx={{ ...commonStyles.textStyle, ...commonStyles.aboutText }}
              onClick={() => navigate(ABOUT_US)}
            >
              Home |
            </Typography>
            <Typography
              sx={{ ...commonStyles.textStyle, ...commonStyles.aboutText }}
              onClick={() => navigate(STANDARD)}
            >
              Documents |
            </Typography>
            <Typography
              sx={{ ...commonStyles.textStyle, ...commonStyles.aboutText }}
              onClick={() => navigate(CERTIFIED_ENTERPRISES)}
            >
              Certified Enterprises
            </Typography>
          </Box>
        </Box>
      )}
    </Box>
  );
}
