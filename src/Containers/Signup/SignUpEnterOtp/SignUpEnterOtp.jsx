import React, { useEffect, useRef } from "react";
import { Box } from "@mui/material";
import { useDispatch } from "react-redux";
import QpOtpVerification from "../../../Components/QpOtpVerification/QpOtpVerification";
import { verifyEmailOtpAction } from "../../../Redux/SignUp/signUpActions";
import { commonStyles } from "../../../Styles/CommonStyles";
import { useNavigate, useSearchParams } from "react-router-dom";
import { getUserData } from "../../../Services/localStorageService";
import { SIGN_UP } from "../../../Routes/Routes";
import { requestLinkAction } from "../../../Redux/Login/loginAction";

export default function SignUpEnterOtp() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [serachParams] = useSearchParams();
  const isLogin = serachParams.get("login");
  const runOnce = useRef(false);

  const userData = getUserData();
  const validateOtp = (data) => {
    if (data) {
      dispatch(verifyEmailOtpAction(data, navigate));
    }
  };

  useEffect(() => {
    window.addEventListener("popstate", () => {
      localStorage.clear();
      navigate(SIGN_UP);
    });
  }, []);

  useEffect(() => {
    if (isLogin && runOnce.current === false) {
      dispatch(requestLinkAction());
    }
    return () => {
      runOnce.current = true;
    };
  }, [isLogin]);

  return (
    <Box sx={{ ...commonStyles.otpContainer, ...commonStyles.customScrollBar }}>
      <Box
        sx={{
          ...commonStyles.signupEnterOtpContainer,
          ...commonStyles.displayCenterStyle,
        }}
      >
        <QpOtpVerification
          otpData={(data) => validateOtp(data)}
          phoneNumber={userData?.email}
        />
      </Box>
    </Box>
  );
}
