import React, { useEffect } from "react";
import { Box } from "@mui/material";

import { commonStyles } from "../../../Styles/CommonStyles";
import QpOtpVerified from "../../../Components/QpOtpVerified/QpOtpVerified";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { SIGN_UP } from "../../../Routes/Routes";

export default function SignUpOtpVerified() {
  const otpValue = useSelector((state) => state.signUp.otpValue);
  const navigate = useNavigate();
  useEffect(() => {
    if (!otpValue) {
      navigate(SIGN_UP);
    }
  }, []);

  return (
    <>
      <Box
        sx={{ ...commonStyles.otpContainer, ...commonStyles.customScrollBar }}
      >
        <Box
          sx={{
            ...commonStyles.signupEnterOtpContainer,
            ...commonStyles.displayCenterStyle,
          }}
        >
          <QpOtpVerified />
        </Box>
      </Box>
    </>
  );
}
