import { WHITE } from "../../Constant/ColorConstant";
import { commonStyles } from "../../Styles/CommonStyles";

export const styles = {
  signUpMainContainer: {
    border: "0.063rem solid #74B8F6",
    borderRadius: "0.625em",
    margin: "1.375rem 3.625rem 0 3.625rem",
    backgroundColor: WHITE,
  },
  scrollStyle: {
    height: "calc(100vh - 14.313rem)",
    "@media(max-width:600px)": {
      height: "calc(100vh - 12.313rem)",
      marginX: "1.5rem",
    },
  },
  signUpInsideMainContainer: {
    padding: "0px 2.625rem",
    display: "flex",
    "@media(max-width:600px)": {
      padding: "0 1rem",
    },
  },
  rectangleContainer: {
    backgroundColor: "white",
    // minWidth: "25.567rem",
    minWidth: "20.188rem",
    height: "100%",
  },
  signUpLeftContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "",
    // width: "50%",
    "@media(max-width:600px)": {
      display: "none",
    },
  },
  signUpContainer: {
    overflowY: "auto",
    overflowX: "auto",
    height: "calc(100vh - 16rem)",
    ...commonStyles.customScrollBar,
    "@media(min-width:1100px) and (min-height:850px)": {
      display: "flex",
      alignItems: "center",
    },
  },
  rightContainer: {
    minWidth: "37rem",
    maxWidth: "37rem",
  },
  imageContainer: {
    backgroundPosition: "center",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
  },
  flexDirection: {
    flexDirection: "column",
  },
};
