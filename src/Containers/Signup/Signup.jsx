import React, { useEffect, useState } from "react";

import { Box } from "@mui/material";

import QpTypography from "../../Components/QpTypography/QpTypography";
import SignUpForm from "./SignUpForm/SignUpForm";

import { styles } from "../Signup/SignupStyles";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { ABOUT_US, LOGIN, SIGN_UP } from "../../Routes/Routes";
import { useLocation, useNavigate, useSearchParams } from "react-router-dom";
import Login from "../Login/Login";
import samarLogo from "../../Assets/Images/samar_logo.jpg";
import samarLogoWithText from "../../Assets/Images/samarLogoWithText.png";
import line from "../../Assets/Images/Line.png";
import QpButton from "../../Components/QpButton/QpButton";

const SignUp = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [currentpage, setCurrentPage] = useState(false);

  const [searchParams] = useSearchParams();
  const url = searchParams.get("url");
  useEffect(() => {
    if (location.pathname.includes(LOGIN)) {
      setCurrentPage(true);
    } else {
      setCurrentPage(false);
    }
  }, [location.pathname]);
  return (
    <Box
      sx={{
        ...styles.signUpMainContainer,
        ...styles.scrollStyle,
        ...commonStyles.displayCenterStyle,
      }}
    >
      <Box
        sx={{
          ...commonStyles.displayCenterStyle,
          ...customCommonStyles.mainHeightContainer,
          ...commonStyles.fullWidth,
        }}
      >
        <Box
          sx={{
            ...styles.signUpInsideMainContainer,
            ...commonStyles.fullHeight,
            ...commonStyles.fullWidth,
          }}
        >
          <Box
            sx={{
              // ...commonStyles.displayCenterStyle,
              ...styles.signUpLeftContainer,
              // flexDirection: "column",
            }}
          >
            <Box
              sx={{
                ...commonStyles.displayCenterStyle,
                ...styles.flexDirection,
              }}
            >
              {/* <QpButton
                displayText="Back to About Us"
                styleData={commonStyles.blueButton}
                textStyle={commonStyles.blueButtonText}
                onClick={() => navigate(ABOUT_US)}
              /> */}
              <Box
                sx={{
                  ...styles.rectangleContainer,
                  ...styles.imageContainer,
                  backgroundImage: `url(${samarLogoWithText})`,
                }}
              ></Box>
            </Box>
            <Box sx={[commonStyles.dividerStyle]}>
              <img src={line} alt="zedLogo" />
            </Box>
          </Box>
          <Box
            sx={{
              ...commonStyles.fullWidth,
              ...styles.signUpContainer,
            }}
          >
            <Box sx={styles.rightContainer}>
              <Box
                sx={{
                  ...commonStyles.displayStyle,
                  ...commonStyles.signUpTextContainer,
                }}
              >
                <QpTypography
                  displayText={currentpage ? "Log In" : "Sign Up"}
                  styleData={commonStyles.signUpText}
                />
                <Box
                  sx={{
                    ...commonStyles.displayStyle,
                    ...(currentpage && { flexDirection: "row" }),
                  }}
                >
                  <QpTypography
                    displayText={
                      currentpage ? "New here?" : "Already signed up? "
                    }
                    styleData={commonStyles.alreadySignUpText}
                  />
                  <QpTypography
                    displayText={
                      currentpage ? "Sign Up for Applicant" : "Login"
                    }
                    styleData={{
                      ...commonStyles.alreadySignUpText,
                      ...commonStyles.loginText,
                    }}
                    onClick={() =>
                      currentpage ? navigate(SIGN_UP) : navigate(LOGIN)
                    }
                  />
                </Box>
              </Box>
              {currentpage ? (
                <Login navigateState={location.state} url={url} />
              ) : (
                <SignUpForm />
              )}
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default SignUp;
