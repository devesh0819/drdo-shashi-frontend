import React, { useState, useEffect } from "react";
import {
  Grid,
  InputBase,
  Box,
  InputAdornment,
  IconButton,
} from "@mui/material";
import QpInputLabel from "../../../Components/QpInputLabel/QpInputLabel";
import QpButton from "../../../Components/QpButton/QpButton";
import QpCheckbox from "../../../Components/QpCheckbox/QpCheckbox";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { signUpSchema } from "../../../validationSchema/signUpSchema";
import { useNavigate } from "react-router-dom";
import { commonStyles } from "../../../Styles/CommonStyles";
import { signUpAction } from "../../../Redux/SignUp/signUpActions";
import { useDispatch } from "react-redux";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { encrypt } from "../../../Services/localStorageService";
import { PASSWORD_ENCRYPTION_SECRET } from "../../../Constant/AppConstant";
import { textTransformCapital } from "../../../Services/commonService";

export default function SignUpForm() {
  const [checked, setChecked] = useState(false);
  const [passwordType, setPasswordType] = useState(true);
  const [confirmPasswordType, setConfirmPasswordType] = useState(true);

  const handleChange = (event) => {
    setChecked(event.target.checked);
    setValue("termsValue", event.target.checked ? 1 : 0, {
      shouldValidate: true,
    });
  };
  const {
    register,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(signUpSchema),
    mode: "onChange",
  });
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const passwordChangeToggle = () => {
    setPasswordType(!passwordType);
  };
  const confirmPasswordChangeToggle = () => {
    setConfirmPasswordType(!confirmPasswordType);
  };

  const onSubmit = (data) => {
    if (!data) return;
    let first_name = textTransformCapital(data.first_name.trim());
    let last_name = textTransformCapital(data.last_name.trim());

    const completeData = {
      first_name: first_name,
      last_name: last_name,
      email: data.email.trim(),
      phone_number: data.phone_number,
      password: encrypt(
        data.password,
        process.env.REACT_APP_PASSWORD_SECRET_KEY
      ),
      password_confirmation: encrypt(
        data.password_confirmation,
        process.env.REACT_APP_PASSWORD_SECRET_KEY
      ),
    };
    if (checked) {
      data["terms"] = 1;
      dispatch(signUpAction(completeData, navigate));
    }
  };
  useEffect(() => {
    setValue("termsValue", checked ? 1 : 0, { shouldValidate: true });
  }, [checked]);
  return (
    <>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={"4rem"}
        sx={commonStyles.signUpContainer}
      >
        <Grid item xs={12} sm={12} md={6} lg={6} sx={commonStyles.signUpFields}>
          <QpInputLabel
            displayText="First Name"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <InputBase
            id="first_name"
            name="first_name"
            required
            sx={{ ...commonStyles.inputStyle }}
            {...register("first_name")}
            error={errors.first_name ? true : false}
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.first_name?.message}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} sx={commonStyles.signUpFields}>
          <QpInputLabel
            displayText="Last Name"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <InputBase
            id="last_name"
            name="last_name"
            required
            sx={{ ...commonStyles.inputStyle }}
            {...register("last_name")}
            error={errors.last_name ? true : false}
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.last_name?.message}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} sx={commonStyles.signUpFields}>
          <QpInputLabel
            displayText="Email"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <InputBase
            id="email"
            name="email"
            required
            sx={{ ...commonStyles.inputStyle }}
            {...register("email")}
            error={errors.email ? true : false}
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.email?.message}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} sx={commonStyles.signUpFields}>
          <QpInputLabel
            displayText="Phone Number"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <InputBase
            id="phone_number"
            name="phone_number"
            required
            sx={{ ...commonStyles.inputStyle }}
            {...register("phone_number")}
            error={errors.phone_number ? true : false}
            inputProps={{ maxLength: 10 }}
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.phone_number?.message}
          />
        </Grid>

        <Grid item xs={12} sm={12} md={6} lg={6} sx={commonStyles.signUpFields}>
          <QpInputLabel
            displayText="Password"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <InputBase
            id="password"
            name="password"
            required
            sx={{ ...commonStyles.inputStyle }}
            type={passwordType ? "password" : "text"}
            {...register("password")}
            error={errors.password ? true : false}
            endAdornment={
              <InputAdornment position="start">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={passwordChangeToggle}
                  edge="end"
                >
                  {passwordType ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            autoComplete="off"
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.password?.message}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} sx={commonStyles.signUpFields}>
          <QpInputLabel
            displayText="Re-enter Password"
            required={true}
            styleData={commonStyles.inputLabel}
          />
          <InputBase
            id="password_confirmation"
            name="password_confirmation"
            required
            sx={{ ...commonStyles.inputStyle }}
            type={confirmPasswordType ? "password" : "text"}
            {...register("password_confirmation")}
            error={errors.password_confirmation ? true : false}
            endAdornment={
              <InputAdornment position="start">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={confirmPasswordChangeToggle}
                  edge="end"
                >
                  {confirmPasswordType ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            autoComplete="off"
          />
          <QpTypography
            styleData={commonStyles.errorText}
            displayText={errors.password_confirmation?.message}
          />
        </Grid>
        <Grid
          sx={{
            ...commonStyles.signUpFields,
            ...commonStyles.agreeToContainer,
          }}
        >
          <QpCheckbox checked={checked} onChange={handleChange} />
          <Box sx={commonStyles.agreeToTextContainer}>
            <QpTypography displayText="I agree to the " />
            <QpTypography
              displayText=" Terms of Service"
              // styleData={commonStyles.termsText}
            />
            <QpTypography displayText="&" />
            <QpTypography
              displayText="Privacy Policy"
              onClick={() => navigate("/privacy-policy")}
              styleData={commonStyles.termsText}
            />
          </Box>
        </Grid>
        <QpTypography
          styleData={{
            ...commonStyles.errorText,
            ...commonStyles.declarationContainer,
          }}
          displayText={checked ? "" : errors.terms?.message}
        />
        <QpButton
          displayText="Create Account"
          styleData={commonStyles.createAccountButton}
          onClick={handleSubmit(onSubmit)}
        />
      </Grid>
    </>
  );
}
