import { useParams } from "react-router-dom";
import { React, useEffect, useState, useRef } from "react";
import {
  Box,
  Typography,
  InputBase,
  Button,
  CircularProgress,
  Select,
  MenuItem,
} from "@mui/material";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import SectionListCard from "../../Admin/ManageQuestionairre/SectionListCard/SectionListCard.jsx";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
// import { styles } from "../../QuestionListCard/QuestionListCardStyles.js";
import QpButton from "../../../Components/QpButton/QpButton";
import QpInputLabel from "../../../Components/QpInputLabel/QpInputLabel";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { useNavigate } from "react-router-dom";
import { ReactComponent as IconCircularBack } from "../../../Assets/Images/iconCircularBack.svg";
import TextareaAutosize from "@mui/material/TextareaAutosize";
import Grid from "@mui/material/Grid";
import parameterViewStyles from "./AssessorParameterViewStyles";
import { useDispatch, useSelector } from "react-redux";
import {
  getAssessorParameterDetailAction,
  addCommentToParameterAction,
  getAssessorParameterDetail,
} from "../../../Redux/LeadAssessor/assessorAllApplicationsActions";
import { styles } from "../../Admin/ManageAssessments/ManageAssessmentsEdit/ManageAssessmentsEditStyles";
import ReactImageViewer from "../../../Components/ReactImageViewer/ReactImageViewer";
import axios from "axios";
import { getItem } from "../../../Services/localStorageService";
import { showToast } from "../../../Components/Toast/Toast";
import * as moment from "moment";
import InputLabel from "@mui/material/InputLabel";
import { PARAM_TYPE } from "../../../Constant/RoleConstant";

const AssessorParameterView = () => {
  const { p_id, id } = useParams();
  const dispatch = useDispatch();
  const assessment_id = id;
  const [tabValue, setTabValue] = useState("p");
  const [question, setQuestion] = useState({});
  const [questionTitle, setQuestionTitle] = useState("");
  const [question_text, setQuestionText] = useState("");
  const [titleError, setTitleError] = useState(false);
  const [parameterData, setParameter] = useState({});
  const [currentActive, setCurrentActive] = useState(0);
  const [commentText, setCommentText] = useState("");
  const [commentError, setCommentError] = useState(true);
  const [selectedImage, setSelectedImage] = useState(null);
  const [isOpen, setIsOpen] = useState(false);
  const [imgUrl, setImgUrl] = useState("");
  const [imgArray, setImgArray] = useState([]);
  const [showLoader, setShowloader] = useState(false);
  const [commentType, setCommentType] = useState("Comment");
  const [commentlengtherror, setCommentLengthError] = useState(false);
  const [selectedImageLat, setImageLat] = useState(null);
  const [selectedImageLong, setImageLong] = useState(null);
  const [selectedImageTimestamp, setImageTimestamp] = useState(null);

  const runOnce = useRef(false);
  const navigate = useNavigate();
  const assessorParameterDetail = useSelector(
    (state) => state.assessorAllAssessments.parameterDetail
  );
  const viewImage = useSelector((state) => state.allApplications.image);

  const commentTypeColor = {
    default: "grey",
    observation: "#E1C16E",
    ofi: "pink",
    strength: "green",
  };

  const getImage = (path, isArray, lat, long, timestamp) => {
    let latTemp = lat || null;
    let longTemp = long || null;
    let timestampTemp = timestamp || null;
    // return () => {
    setShowloader(true);
    axios
      .get(path, {
        headers: {
          "Content-type": "image/jpeg",
          Authorization: `Bearer ${getItem("token")}`,
        },
        responseType: "blob",
      })
      .then((res) => {
        setTimeout(() => {
          setShowloader(false);
        }, 1000);
        if (res.status === 200) {
          if (!isArray) {
            setImgUrl(URL.createObjectURL(res.data));
          } else {
            setImgArray((prev) => [
              ...prev,
              {
                url: URL.createObjectURL(res.data),
                lat: latTemp,
                long: longTemp,
                timestamp: timestampTemp,
              },
            ]);
          }
        }
      })
      .catch((err) => {
        setShowloader(false);
        showToast("Something went wrong", "error");
      });
    // };
  };

  useEffect(() => {
    if (p_id && runOnce.current === false) {
      dispatch(getAssessorParameterDetailAction(p_id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getAssessorParameterDetail({}));
    };
  }, [p_id]);

  useEffect(() => {
    if (assessorParameterDetail?.evidenses?.length) {
      // getImagesArray(imageArray);
      assessorParameterDetail?.evidenses.map((img) =>
        getImage(
          img?.file,
          true,
          img?.latitude,
          img?.longitude,
          img?.file_date_time
        )
      );
    }
  }, [assessorParameterDetail?.evidenses?.length]);

  const handleTabChange = (e, val) => {
    setCurrentActive(0);
    setTabValue(val);
  };

  // const handleCommentBlur=(e)=>{
  //   let value=e?.target?.value
  //   setCommentText(value)
  //   if(value=="" || value==" " || value==null || value==undefined)
  //   setCommentError(true)
  //   else
  //   {

  //     if(value.length<=2){
  //         setCommentLengthError(true)
  //         setCommentError(true)
  //     }
  //     else{
  //   setCommentLengthError(false)
  //   setCommentError(false)
  //     }
  //   }
  // }

  const handleTextAreaChange = (e) => {
    let value = e?.target?.value;
    setCommentText(value);
    if (value == "" || value == " " || value == null || value == undefined)
      setCommentError(true);
    else {
      if (value.length <= 2) {
        setCommentLengthError(true);
        setCommentError(true);
      } else {
        setCommentLengthError(false);
        setCommentError(false);
      }
    }
  };
  const handleAddComment = () => {
    let comment = commentText;
    let comment_type;
    if (
      comment == "" ||
      comment == " " ||
      comment == null ||
      comment == undefined
    ) {
      setCommentError(true);
    } else {
      if (comment.length <= 2) {
        setCommentLengthError(true);
      } else {
        setCommentError(false);
        setCommentLengthError(false);
        if (
          commentType == "" ||
          commentType == " " ||
          commentType == null ||
          commentType == undefined
        )
          comment_type = "Default";
        else if (commentType == "Comment") comment_type = "Default";
        else comment_type = commentType;

        let payload = {};
        payload.comment = comment.replace("\n", " ");
        comment_type = comment_type?.replace(/'s/g, "");
        payload.comment_type = comment_type?.toLowerCase();

        dispatch(addCommentToParameterAction(payload, p_id, id));
        setCommentText("");
        setCommentType("");
        setCommentError(true);
      }
    }
  };
  const handleImageClick = (event, lat, long, timestamp) => {
    setSelectedImage(event?.target?.src);
    setImageLat(lat);
    setImageLong(long);
    setImageTimestamp(timestamp);
    setIsOpen(true);
  };

  const handleImageClose = () => {
    setSelectedImage(null);
    setImageLat(null);
    setImageLong(null);
    setIsOpen(false);
  };

  const handleCommentTypeRadioChange = (event) => {
    let value = event?.target?.value;
    setCommentType(value);
  };

  const checkColorObj = (type) => {
    switch (type) {
      case "strength":
        return commentTypeColor.strength;
        break;
      case "ofi":
        return commentTypeColor.ofi;
        break;
      case "observations":
        return commentTypeColor.observation;
        break;
      default:
        return commentTypeColor.default;
    }
  };

  const checkCommentType = (text) => {
    switch (text) {
      case "strength":
        return "Strength";
        break;
      case "ofi":
        return "OFI's";
        break;
      case "observations":
        return "Observations";
        break;
      default:
        return "Comment";
    }
  };

  return (
    <>
      {/* Implementation In Progress */}
      <Box sx={customCommonStyles.outerContainer}>
        <Box>
          <Box
            // style={{ ...commonStyles.topHeader, ...commonStyles.displayStyle }}
            style={{ ...commonStyles.topHeader }}
          >
            <Button
              disableRipple
              sx={parameterViewStyles.iconBtnStyle}
              onClick={() => {
                runOnce.current = false;
                navigate(
                  `/assessor-allApplications/assessment-details/${assessment_id}`
                );
              }}
              className="backButton"
            >
              <IconCircularBack />
            </Button>
            <Typography style={commonStyles.titleText}>
              Parameter Details
            </Typography>
          </Box>
          <Box sx={commonStyles.parameterContainerHeight}>
            <Box style={commonStyles.parameterContainer}>
              <QpInputLabel
                displayText="Parameter Title"
                // required={true}
                styleData={commonStyles.inputLabel}
              />

              <Box sx={commonStyles.parameterText}>
                {assessorParameterDetail?.parameter}
              </Box>

              {titleError && (
                <QpTypography
                  styleData={commonStyles.errorText}
                  displayText={"This field cannot be empty."}
                />
              )}
            </Box>
            {/* ...........................tabs.............. */}

            <Box sx={commonStyles.fullWidth}>
              <TabContext value={tabValue}>
                <Box sx={commonStyles.tabDiv}>
                  <TabList
                    onChange={handleTabChange}
                    aria-label="lab API tabs example"
                  >
                    <Tab label="Planning" value="p" />
                    <Tab label="Deployment" value="d" />
                    <Tab label="Monitoring" value="m" />
                  </TabList>
                </Box>
                <TabPanel value="p">
                  <>
                    {assessorParameterDetail?.sections?.filter(
                      (item) => item.type == PARAM_TYPE.PLANNING
                    )?.length
                      ? assessorParameterDetail?.sections
                          ?.filter((item) => item.type == PARAM_TYPE.PLANNING)
                          ?.map((section, sectionIndex) => {
                            return (
                              <SectionListCard
                                key={section}
                                parameterData={assessorParameterDetail}
                                section={section}
                                category={tabValue}
                                sectionIndex={sectionIndex}
                                currentActive={currentActive}
                                setCurrentActive={(data) =>
                                  setCurrentActive(data)
                                }
                                section_id={sectionIndex + 1}
                                showContainerFromProp={
                                  sectionIndex + 1 == currentActive
                                    ? true
                                    : false
                                }
                              />
                            );
                          })
                      : "No Sections for this type"}
                  </>
                </TabPanel>

                <TabPanel value="d">
                  {assessorParameterDetail?.sections?.filter(
                    (item) => item.type == PARAM_TYPE.DEPLOYMENT
                  )?.length
                    ? assessorParameterDetail?.sections
                        ?.filter((item) => item.type == PARAM_TYPE.DEPLOYMENT)
                        ?.map((section, sectionIndex) => {
                          return (
                            <SectionListCard
                              key={section}
                              parameterData={assessorParameterDetail}
                              section={section}
                              category={tabValue}
                              sectionIndex={sectionIndex}
                              currentActive={currentActive}
                              setCurrentActive={(data) =>
                                setCurrentActive(data)
                              }
                              section_id={sectionIndex + 1}
                              showContainerFromProp={
                                sectionIndex + 1 == currentActive ? true : false
                              }
                            />
                          );
                        })
                    : "No Sections for this type"}
                </TabPanel>
                <TabPanel value="m">
                  {assessorParameterDetail?.sections?.filter(
                    (item) => item.type == PARAM_TYPE.MONITORING
                  )?.length
                    ? assessorParameterDetail?.sections
                        ?.filter((item) => item.type == PARAM_TYPE.MONITORING)
                        ?.map((section, sectionIndex) => {
                          return (
                            <SectionListCard
                              key={section}
                              parameterData={assessorParameterDetail}
                              section={section}
                              category={tabValue}
                              sectionIndex={sectionIndex}
                              currentActive={currentActive}
                              setCurrentActive={setCurrentActive}
                              section_id={sectionIndex + 1}
                              showContainerFromProp={
                                sectionIndex + 1 == currentActive ? true : false
                              }
                            />
                          );
                        })
                    : "No Sections for this type"}
                </TabPanel>
              </TabContext>
            </Box>
            {/* _________________Evidence section________________________ */}
            <Box style={commonStyles.parameterContainer}>
              <QpInputLabel
                displayText="Evidences"
                styleData={commonStyles.inputLabel}
              />
              <Box
                sx={{ ...parameterViewStyles.evidenceContainer, flexGrow: 1 }}
              >
                <Grid container spacing={1}>
                  {imgArray?.length &&
                  imgArray?.length ===
                    assessorParameterDetail?.evidenses?.length &&
                  !showLoader
                    ? imgArray?.map((image) => {
                        return (
                          <Grid item xs={3} md={1}>
                            <Box sx={parameterViewStyles.evidenceContainer}>
                              <img
                                // src={`data:image/*;base64,${imgUrl}`}
                                // src={`${url}`}
                                src={image?.url}
                                alt="url"
                                style={commonStyles.imagePreview}
                                onClick={(event) =>
                                  handleImageClick(
                                    event,
                                    image?.lat,
                                    image?.long,
                                    image?.timestamp
                                  )
                                }
                              />
                            </Box>
                          </Grid>
                        );
                      })
                    : !showLoader && "No Evidence"}
                  {showLoader
                    ? assessorParameterDetail?.evidenses?.map((img) => {
                        return <CircularProgress />;
                      })
                    : null}
                </Grid>
              </Box>
            </Box>

            {/* __________________comment section________________________ */}

            <Box style={commonStyles.parameterContainer}>
              <QpInputLabel
                displayText="Add Comment"
                styleData={{ ...commonStyles.inputLabel }}
              />
              <Box>
                {/* <TextareaAutosize
                minRows={3}
                placeholder="Add Comment Here"
                style={{ width: "50vw" ,display:"inline-block"}}
                onChange={handleTextAreaChange}
                value={commentText}
                /> */}
                <Box style={styles.commentSelectType}>
                  {/* <InputLabel style={{...parameterViewStyles.commentCategoryLabel}}>Comment Category</InputLabel> */}
                  <Select
                    id="type"
                    name="type"
                    MenuProps={{
                      sx: {
                        ...commonStyles.menuProps,
                        ...commonStyles.fullHeight,
                      },
                    }}
                    sx={{
                      ...parameterViewStyles.commentTypeSelect,
                      ...parameterViewStyles.marginOnePercent,
                    }}
                    value={commentType}
                    onChange={handleCommentTypeRadioChange}
                    native={false}
                    renderValue={(val) => {
                      if (!val)
                        return (
                          <Typography sx={commonStyles.placeHolderColor}>
                            {"Comment"}
                          </Typography>
                        );
                      else
                        return (
                          <Typography sx={styles.dropdownText}>
                            {commentType}
                          </Typography>
                        );
                    }}
                    displayEmpty={true}
                  >
                    <MenuItem id="1" value={"Comment"} sx={styles.dropdownText}>
                      Comment
                    </MenuItem>
                    <MenuItem
                      id="2"
                      value={"Observations"}
                      sx={styles.dropdownText}
                    >
                      Observations
                    </MenuItem>
                    <MenuItem
                      id="3"
                      value={"Strength"}
                      sx={styles.dropdownText}
                    >
                      Strength
                    </MenuItem>
                    <MenuItem id="4" value={"OFI's"} sx={styles.dropdownText}>
                      OFI's
                    </MenuItem>
                  </Select>
                </Box>
                <Box style={styles.commentSelectType}>
                  <InputBase
                    multiline={true}
                    minRows={3}
                    style={styles.addCommentStyle}
                    onChange={handleTextAreaChange}
                    value={commentText}
                    placeholder="Add Comment Here"
                  />
                  <QpButton
                    styleData={{ ...styles.updateButton, ...styles.marginLeft }}
                    displayText="Add Comment"
                    textStyle={styles.updateButtonText}
                    isDisable={commentError}
                    onClick={handleAddComment}
                  />
                </Box>
                {commentlengtherror && (
                  <QpTypography
                    styleData={commonStyles.errorText}
                    displayText={"Comment should have minimum 3 alphabets"}
                  />
                )}
              </Box>
              <QpInputLabel
                displayText="Recent Comments"
                styleData={{
                  ...commonStyles.inputLabel,
                  ...styles.commentLabel,
                }}
              />
              {assessorParameterDetail?.comments?.length ? (
                <Box
                  sx={{
                    ...customCommonStyles.fieldContainer,
                    ...parameterViewStyles.commentBoxContainer,
                    ...parameterViewStyles.customScrollBarHidden,
                  }}
                >
                  {assessorParameterDetail?.comments?.map((comment) => {
                    return (
                      <Box sx={parameterViewStyles.commentBoxesStyles}>
                        <Box sx={parameterViewStyles.commentBoxInnerBox}>
                          <QpInputLabel
                            displayText={comment?.comment_author || "-"}
                            styleData={{ ...commonStyles.inputLabel }}
                          />
                          <QpTypography
                            displayText={
                              moment(comment?.comment?.created_at).format(
                                "D MMM,YYYY"
                              ) || "-"
                            }
                            styleData={{
                              ...parameterViewStyles.commentBoxDate,
                              ...styles.dateStyle,
                            }}
                          />
                          <Box sx={parameterViewStyles.commentTypeColorBox}>
                            <QpInputLabel
                              displayText={checkCommentType(
                                comment?.comment_type
                              )}
                              styleData={{
                                ...commonStyles.inputLabel,
                                color: checkColorObj(comment?.comment_type),
                              }}
                            />
                          </Box>
                        </Box>
                        <QpTypography displayText={comment?.comment} />
                      </Box>
                    );
                  })}{" "}
                </Box>
              ) : (
                "No comments yet"
              )}
              <Box></Box>
            </Box>
            {/* {commentError &&  <QpTypography
            styleData={{...commonStyles.errorText,padding:"0 0 0 3%"}}
            displayText={"This field cannot be empty."}
          /> } */}
          </Box>
        </Box>
      </Box>

      <ReactImageViewer
        imgs={selectedImage}
        isOpen={isOpen}
        onClose={handleImageClose}
        lat={selectedImageLat}
        long={selectedImageLong}
        timestamp={selectedImageTimestamp}
      />
    </>
  );
};

export default AssessorParameterView;
