import {
  BACKGROUND_LIGHT_BLUE,
  BLACKISH,
  DARK_BLUE,
  WHITE,
  METALLIC_SILVER,
  BLUE,
  BLACK,
  DEFAULT_GREY,
} from "../../../Constant/ColorConstant";
import { HeightTwoTone } from "@mui/icons-material";
import { padding } from "@mui/system";

const tabContainerStyles = {
  fieldContainer: {
    padding: "1rem 2rem",
    height: "calc(100vh - 16.5rem)",
    overflow: "scroll",
  },
  customScrollBar: {
    "&::-webkit-scrollbar": {
      width: "0.3rem",
      height: "0.3rem",
    },
    "&::-webkit-scrollbar-track": {
      boxShadow: "inset 0 0 0.375rem rgba(0,0,0,0.00)",
      webkitBoxShadow: "inset 0 0 0.375rem rgba(0,0,0,0.00)",
      width: "0.438rem",
      borderRadius: "0.75rem",
      // background: SILVER_GREY,
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "rgba(0,0,0,.1)",
      borderRadius: "0.75rem",
      background: METALLIC_SILVER,
    },
  },
  dialogHeightSingle: {
    height: "400px",
  },
  dialogHeightMultiple: {
    height: "900px",
  },
};

export default tabContainerStyles;
