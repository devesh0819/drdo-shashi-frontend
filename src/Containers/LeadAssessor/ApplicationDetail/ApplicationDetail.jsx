import React, { useEffect, useRef, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Box, Button, Tab, Select, MenuItem, Typography } from "@mui/material";
import { styles } from "../../Admin/ManageAssessments/ManageAssessmentsEdit/ManageAssessmentsEditStyles";
import QpButton from "../../../Components/QpButton/QpButton";
import { ReactComponent as IconCircularBack } from "../../../Assets/Images/iconCircularBack.svg";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import AdminApplicationView from "../../Admin/AllApplications/AdminApplicationView/AdminApplicationView";
import {
  getApplicationDetailAction,
  getApplicationParameterAction,
  assignAssessorToParameterAction,
  startAssessmentLeadAction,
  submitAssessmentLeadAction,
  setAssessorTabAction,
  getApplicationParameter,
} from "../../../Redux//LeadAssessor/assessorAllApplicationsActions";
import ParameterListCard from "../ParameterListCard/ParameterListCard";
import QpDialog from "../../../Components/QpDialog/QpDialog";
import { commonStyles } from "../../../Styles/CommonStyles";
import AssignAssessorModal from "../../../Components/AssignAssessorModal/AssignAssessorModal";
import { ASSESSOR_ALL_APPLICATIONS } from "../../../Routes/Routes";
import QpConfirmModal from "../../../Components/QpConfirmModal/QpConfirmModal";
import tabContainerStyles from "./ApplicationDetalStyles";
import { APP_STAGE } from "../../../Constant/RoleConstant";
import { getUserData } from "../../../Services/localStorageService";
import { showToast } from "../../../Components/Toast/Toast";
import AssignMultipleParameters from "../../../Components/AssignMultipleParameters/AssignMultipleParameters";
import CustomApplicatonView from "../../../Components/CustomApplicationView/CustomApplicationView";
import AssessmentDetailAccordian from "../../Admin/ManageAssessments/ManageAssessmentsEdit/AssessmentDetailAccordian/AssessmentDetailAccordian";

export default function ApplicationDetail() {
  const { id } = useParams();
  const userData = getUserData();
  const role = userData?.roles[0];

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [value, setValue] = useState("1");
  const [selectedAgency, setSelectedAgency] = useState("");
  const [unassignedParameters, setUnassignedParameters] = useState([]);
  const [showAssignDialog, setShowAssignDialog] = useState(false);
  const [checkedParamIds, setCheckedParamIds] = useState([]);
  const [showMainAssignDialog, setShowMainAssignDialog] = useState(false);
  const [selectedAssessorId, setSelectedAssessorId] = useState("");
  const [currentParameterID, setCurrentParameterID] = useState(null);
  const [showStartSubmitAssessment, setshowStartSubmitAssessment] =
    useState(false);
  const [disableSubmitBtn, setDisableSubmitBtn] = useState(false);
  const runOnce = useRef(false);

  let disableSubmitButton = false;
  let disableCount = 0;

  const agencyListingData = useSelector(
    (state) => state.manageAssessments.agencyListingData
  );
  const selectedAgencyData = useSelector(
    (state) => state.adminAllApplications.assignAgencyData
  );
  const applicationDetail = useSelector(
    (state) => state.assessorAllAssessments.assessmentDetail
  );
  const applicationParameterDetail = useSelector(
    (state) => state.assessorAllAssessments.assessmentParameter
  );

  const currentTabAssessor = useSelector(
    (state) => state.assessorAllAssessments.currentTabAssessor
  );

  // const handleSubmit = (paramId) => {
  //   if (!selectedAssessorId) {
  //     showToast("Please Select Assessor", "error");
  //   }
  //   if (selectedAssessorId && paramId) {
  //     let payload = {
  //       assessor_id: selectedAssessorId,
  //     };
  //     dispatch(assignAssessorToParameterAction(payload, paramId, id));
  //   }
  // };

  useEffect(() => {
    dispatch(setAssessorTabAction(currentTabAssessor));
    // setValue(currentTabAdmin)
  }, []);

  useEffect(() => {
    if (id) {
      dispatch(getApplicationDetailAction(id));
      // dispatch(getApplicationParameterAction(id));
    }
    return () => {
      dispatch(getApplicationParameter([]));
    };
  }, [id, dispatch]);

  useEffect(() => {
    applicationParameterDetail?.parameters?.disciplines?.forEach((d) => {
      d?.parameters?.forEach((p, p_index) => {
        if (
          p.assessor_id === null ||
          p.assessor_id === undefined ||
          p.assessor_id === "" ||
          p.assessor_id === " "
        ) {
          disableCount = disableCount + 1;
        }
      });
    });
    if (disableCount == 0) setDisableSubmitBtn(false);
    else setDisableSubmitBtn(true);
  }, [applicationParameterDetail]);

  useEffect(() => {
    setUnassignedParameters((prev) => {
      let temp = [...prev];
      applicationParameterDetail?.parameters?.disciplines?.forEach(
        (discipline) => {
          discipline.parameters.forEach((param) => {
            if (
              !param.assessor_id &&
              !temp.find((obj) => obj.id === param.id)
            ) {
              temp.push({ ...param, disciplineTitle: discipline?.title });
              // temp.push(param);
            } else if (
              param.assessor_id &&
              temp.find((obj) => obj.id === param.id)
            ) {
              temp = temp.filter((obj) => obj.id !== param.id);
            }
          });
        }
      );
      return temp;
    });
  }, [applicationParameterDetail?.parameters?.disciplines]);

  useEffect(() => {
    // if(applicationDetail && applicationDetail.progress=="asses_complete")
    if (
      applicationDetail &&
      applicationDetail.progress == APP_STAGE.ASSESSMENT_COMPLETED
    )
      dispatch(setAssessorTabAction("1"));
  }, [applicationDetail]);

  const handleChange = (e, newValue) => {
    // setValue(newValue);
    dispatch(setAssessorTabAction(newValue));
  };

  const handleShowAssignModal = (parameter) => {
    setCurrentParameterID(parameter);
    showAssignModal(true);
  };

  const showAssignModal = (val) => {
    setShowAssignDialog(val);
  };

  const handleConfirm = () => {};

  const handleMultipleParamAssign = () => {
    if (checkedParamIds.length === 0) {
      showToast("Please select some parameters", "error");
      return;
    }
    if (selectedAssessorId && checkedParamIds) {
      let payload = {
        assessor_id: selectedAssessorId,
        parameterIDs: checkedParamIds,
      };
      dispatch(
        assignAssessorToParameterAction(
          payload,
          null,
          id,
          true,
          setCheckedParamIds
        )
      );
      setShowMainAssignDialog(false);
    }
  };

  const onSaveHandler = (assessor_id) => {
    if (assessor_id && currentParameterID) {
      let payload = {
        assessor_id: assessor_id,
      };
      dispatch(
        assignAssessorToParameterAction(payload, currentParameterID, id)
      );
      showAssignModal(false);
    }
  };
  const handleStartOrSubmit = () => {
    // if (applicationDetail?.progress === "assess_started") {
    if (applicationDetail?.progress === APP_STAGE.ASSESSORS_ASSIGNED) {
      dispatch(startAssessmentLeadAction(id));
    }
    // else if (applicationDetail?.progress === "asses_init") {
    else if (applicationDetail?.progress === APP_STAGE.PAREMETER_ALLOCATED) {
      dispatch(submitAssessmentLeadAction(id));
    } else {
      // do nothing
      showToast("Something went wrong, Kindly try later sometime.", "error");
    }
  };

  const handleBackButtonClick = () => {
    dispatch(setAssessorTabAction("1"));
    navigate(ASSESSOR_ALL_APPLICATIONS);
  };

  return (
    <>
      <Box sx={styles.outerContainer}>
        <Box
          sx={{
            ...styles.headerOuterContainer,
            ...commonStyles.topHeaderHeight,
          }}
        >
          <Box sx={styles.topHeader}>
            <Button
              disableRipple
              sx={styles.backButton}
              onClick={handleBackButtonClick}
              className="backButton"
            >
              <IconCircularBack />
            </Button>
            <Typography sx={styles.titleText}>
              {applicationDetail?.application?.enterprise_name}
            </Typography>
          </Box>
          {/* {applicationDetail?.progress === "assess_started" && currentTabAssessor === "2" ? ( */}
          {applicationDetail?.progress === APP_STAGE.ASSESSORS_ASSIGNED &&
          currentTabAssessor === "2" ? (
            <QpButton
              styleData={styles.updateButton}
              displayText="Sync"
              textStyle={styles.updateButtonText}
              isDisable={disableSubmitBtn}
              // isDisable={disableCount==0?false:true}
              onClick={() => {
                setshowStartSubmitAssessment(true);
              }}
            />
          ) : (
            // ) : applicationDetail?.progress === "asses_init" && currentTabAssessor === "2" ? (
            applicationDetail?.progress === APP_STAGE.PAREMETER_ALLOCATED &&
            currentTabAssessor === "2" && (
              <QpButton
                styleData={styles.updateButton}
                displayText="Submit"
                textStyle={styles.updateButtonText}
                // isDisable={true}
                onClick={() => {
                  setshowStartSubmitAssessment(true);
                }}
              />
            )
          )}
        </Box>
        <Box sx={styles.lowerContainer}>
          {/* <TabContext value={value}> */}
          <TabContext value={currentTabAssessor}>
            <Box sx={styles.tabContainer}>
              <TabList
                onChange={handleChange}
                sx={styles.tabList}
                className="tabList"
              >
                <Tab label="Applicant Detail" value="1" sx={styles.tabsText} />
                {(applicationDetail?.progress ===
                  APP_STAGE.ASSESSORS_ASSIGNED ||
                  applicationDetail?.progress ===
                    APP_STAGE.PAREMETER_ALLOCATED ||
                  applicationDetail?.progress ===
                    APP_STAGE.ASSESSMENT_SUBMITTED) && (
                  <Tab label="Assessment" value="2" sx={styles.tabsText} />
                )}
              </TabList>
              {applicationDetail?.progress === APP_STAGE.ASSESSORS_ASSIGNED &&
              currentTabAssessor === "2" &&
              unassignedParameters.length > 0 ? (
                <Button
                  variant="outlined"
                  sx={styles.parameterListCardStyle.button}
                  onClick={() => {
                    // showAssignModal(parameterID);
                    setShowMainAssignDialog(true);
                  }}
                >
                  Assign Assessor
                </Button>
              ) : null}
            </Box>
            <TabPanel value="1" sx={styles.tabPanels}>
              <AdminApplicationView
                applicationDetail={applicationDetail?.application}
              />
            </TabPanel>
            <TabPanel value="2" sx={styles.tabPanels}>
              <Box
                sx={{
                  ...tabContainerStyles.fieldContainer,
                  ...tabContainerStyles.customScrollBar,
                  ...commonStyles.applicationViewContainer,
                }}
              >
                {/* {applicationParameterDetail?.parameters?.disciplines?.map(
                  (d) => {
                    return d?.parameters?.map((p, p_index) => {
                      return (
                        <ParameterListCard
                          applicationDetail={applicationDetail}
                          applicationParameterDetail={
                            applicationParameterDetail
                          }
                          discipline={d}
                          parameter={p}
                          parameterID={p?.id}
                          showAssignModal={(id) => {
                            handleShowAssignModal(id);
                          }}
                          role={role}
                        />
                      );
                    });
                  }
                )} */}
                <AssessmentDetailAccordian
                  adminAssessmentParameters={applicationParameterDetail}
                  role={role}
                  assessmentApplicationDetail={applicationDetail}
                  handleShowAssignModal={handleShowAssignModal}
                />
              </Box>
            </TabPanel>
          </TabContext>
        </Box>
      </Box>
      <QpDialog
        title={"Assign Assessors"}
        open={showAssignDialog}
        closeModal={() => {
          showAssignModal(false);
        }}
        styleData={{
          ...commonStyles.dialogContainer,
          ...commonStyles.dialogContainerWithSeventy,
          ...tabContainerStyles.dialogHeightSingle,
        }}
      >
        <AssignAssessorModal
          closeModal={() => {
            showAssignModal(false);
          }}
          onConfirm={() => handleConfirm()}
          applicationDetail={applicationDetail}
          handleAssign={(assr_id) => onSaveHandler(assr_id)}
        />
      </QpDialog>
      <QpDialog
        title={"Assign Assessor"}
        open={showMainAssignDialog}
        closeModal={() => {
          setCheckedParamIds([]);
          setShowMainAssignDialog(false);
        }}
        styleData={{
          ...commonStyles.dialogContainer,
          ...styles.dialogContainerWithSeventy,
          ...tabContainerStyles.dialogHeightMultiple,
        }}
      >
        <AssignMultipleParameters
          closeModal={(val) => {
            setCheckedParamIds([]);
            setShowMainAssignDialog(val);
          }}
          onConfirm={() => handleConfirm()}
          applicationDetail={applicationDetail}
          applicationParameterDetail={applicationParameterDetail}
          handleAssign={(assr_id) => {
            setSelectedAssessorId(assr_id);
          }}
          unassignedParameters={unassignedParameters}
          setCheckedParamIds={setCheckedParamIds}
          checkedParamIds={checkedParamIds}
          handleSubmit={handleMultipleParamAssign}
        />
      </QpDialog>

      <QpDialog
        open={showStartSubmitAssessment}
        closeModal={() => setshowStartSubmitAssessment(false)}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to perform this action?"
          closeModal={() => setshowStartSubmitAssessment(false)}
          onConfirm={() => handleStartOrSubmit()}
        />
      </QpDialog>
    </>
  );
}
