import React, { useEffect, useState } from "react";
import { Box, Tooltip, Button } from "@mui/material";
import { styles } from "../../Admin/ManageQuestionairre/QuestionListCard/QuestionListCardStyles";
import { commonStyles } from "../../../Styles/CommonStyles.js";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { ReactComponent as IconPencil } from "../../../Assets/Images/iconPencil.svg";
import { useNavigate } from "react-router-dom";
import { ROLES } from "../../../Constant/RoleConstant";
import parameterListCardStyle from "./ParameterListCardStyles";
import { APP_STAGE } from "../../../Constant/RoleConstant";

export default function ParameterListCard(props) {
  const [assginedAssessor, setAssginedAssessor] = useState(null);
  const navigate = useNavigate();
  const {
    applicationDetail,
    applicationParameterDetail,
    discipline,
    parameter,
    parameterID,
    showAssignModal,
  } = props;

  useEffect(() => {
    let assginedAssessor = applicationDetail?.assessors?.find(
      (a) => a.assessor_id === parameter?.assessor_id
    );
    setAssginedAssessor(assginedAssessor);
  }, [parameter?.assessor_id]);

  const handleBoxClick = () => {
    if (props.role === ROLES.ADMIN || props.role === ROLES.DRDO)
      navigate(`admin-parameter-details/${parameterID}`);
    else navigate(`assessor-parameter-details/${parameterID}`);
  };

  return (
    <>
      <Box
        sx={{
          ...styles.cardMainContainer,
          // ...(editAgency && commonStyles.displayNone),
        }}
      >
        <Box>
          <Box sx={commonStyles.displayStyle}>
            <Box sx={styles.imageTitleContainer} onClick={handleBoxClick}>
              <QpTypography
                displayText={parameter?.parameter}
                styleData={styles.titleText}
              />
            </Box>
          </Box>
          <Box
            sx={{
              ...styles.addressContainer,
              ...styles.parameterListCardStyle.flexContainer,
            }}
          >
            <Box sx={styles.parameterListCardStyle.flexContainer}>
              <QpTypography
                displayText={`Discipline: ${discipline.title}`}
                styleData={{ ...styles.smallText, ...styles.smallTextMargin }}
              />
              {(props.role === ROLES.ADMIN || props.role === ROLES.DRDO) &&
              parameter?.parameter_score ? (
                <>
                  <QpTypography
                    displayText="|"
                    styleData={styles.dividerStyle}
                  />
                  <QpTypography
                    displayText={`Score : `}
                    styleData={{
                      ...styles.smallText,
                      ...styles.smallTextMargin,
                      ...styles.inlineText,
                    }}
                  />
                  <QpTypography
                    displayText={parameter?.parameter_score}
                    styleData={{
                      ...styles.smallText,
                      ...styles.smallTextMargin,
                      ...styles.inlineText,
                    }}
                  />
                </>
              ) : (
                ""
              )}
              {assginedAssessor ? (
                <>
                  <QpTypography
                    displayText="|"
                    styleData={styles.dividerStyle}
                  />
                  <QpTypography
                    displayText={`Assessor : `}
                    styleData={{
                      ...styles.smallText,
                      ...styles.smallTextMargin,
                      ...styles.inlineText,
                    }}
                  />
                </>
              ) : (
                ""
              )}
              {assginedAssessor ? (
                <>
                  {/* <QpTypography
                    displayText="|"
                    styleData={styles.dividerStyle}
                  /> */}

                  <QpTypography
                    displayText={`${
                      assginedAssessor?.profile?.first_name
                    } ${""} ${assginedAssessor?.profile?.last_name}`}
                    styleData={{
                      ...styles.smallText,
                      ...styles.smallTextMargin,
                      ...styles.inlineText,
                    }}
                  />
                </>
              ) : (
                ""
              )}

              {assginedAssessor &&
              // (applicationDetail.progress == "asses_init" ||
              //   applicationDetail.progress == "asses_submit") ? (
              (applicationDetail.progress == APP_STAGE.PAREMETER_ALLOCATED ||
                applicationDetail.progress ==
                  APP_STAGE.ASSESSMENT_SUBMITTED) ? (
                <>
                  <QpTypography
                    displayText="|"
                    styleData={styles.dividerStyle}
                  />

                  <QpTypography
                    displayText={`Responses :`}
                    styleData={{
                      ...styles.smallText,
                      ...styles.smallTextMargin,
                      ...styles.inlineText,
                    }}
                  />
                </>
              ) : (
                ""
              )}
              {assginedAssessor &&
                // (applicationDetail.progress == "asses_init" ||
                //   applicationDetail.progress == "asses_submit") ? (
                (applicationDetail.progress == APP_STAGE.PAREMETER_ALLOCATED ||
                  applicationDetail.progress ==
                    APP_STAGE.ASSESSMENT_SUBMITTED) &&
                (props?.parameter?.parameter_completed ? (
                  <QpTypography
                    displayText={"Completed"}
                    styleData={{
                      ...styles.smallText,
                      ...styles.smallTextMargin,
                      ...styles.inlineText,
                      ...parameterListCardStyle.allresponse,
                    }}
                  />
                ) : (
                  <QpTypography
                    displayText={"Pending"}
                    styleData={{
                      ...styles.smallText,
                      ...styles.smallTextMargin,
                      ...styles.inlineText,
                      ...parameterListCardStyle.notAllResponse,
                    }}
                  />
                ))}
            </Box>
            <Box>
              {props.role == ROLES.ASSESSOR &&
                // applicationDetail.progress == "assess_started" ? (
                applicationDetail.progress == APP_STAGE.ASSESSORS_ASSIGNED &&
                (assginedAssessor ? (
                  <Tooltip title="Edit Assessor" arrow>
                    <IconPencil
                      style={commonStyles.cursorPointer}
                      onClick={() => {
                        showAssignModal(parameterID);
                      }}
                    />
                  </Tooltip>
                ) : (
                  <Button
                    variant="outlined"
                    sx={styles.parameterListCardStyle.button}
                    onClick={() => {
                      showAssignModal(parameterID);
                    }}
                  >
                    Assign
                  </Button>
                ))}
            </Box>
          </Box>
        </Box>
      </Box>
      {/* ) */}
      {/* } */}
    </>
  );
}
