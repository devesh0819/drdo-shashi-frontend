const parameterListCardStyle = {
  notAllResponse: {
    color: "red",
    fontWeight: "500",
  },
  allresponse: {
    color: "green",
    fontWeight: "500",
  },
};

export default parameterListCardStyle;
