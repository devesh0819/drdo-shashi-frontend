import React, { useEffect, useState } from "react";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import { ReactComponent as IconCircularDownload } from "../../../Assets/Images/iconCircularDownload.svg";
import { ReactComponent as IconPencil } from "../../../Assets/Images/iconPencil.svg";
import { ReactComponent as IconMenuSearch } from "../../../Assets/Images/iconMenuSearch.svg";
import { Button, Box, Tooltip, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import * as moment from "moment";
import { useNavigate } from "react-router-dom";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { commonStyles } from "../../../Styles/CommonStyles";
import { getAllApplicationsAction } from "../../../Redux/LeadAssessor/assessorAllApplicationsActions.jsx";
import QpButton from "../../../Components/QpButton/QpButton";
import { MANAGE_ASSESSMENTS } from "../../../Routes/Routes";
import {
  getAppStatus,
  getAssessmentStatus,
} from "../../../Services/commonService";
import { APP_STAGE } from "../../../Constant/RoleConstant";
import ContentPasteSearchIcon from "@mui/icons-material/ContentPasteSearch";
import { styles } from "./ApplicationListStyles.js";
import { downloadAction } from "../../../Redux/AllApplications/allApplicationsActions";
import {
  typeOfEnterpriseArray,
  assessmentStatus,
} from "../../../Constant/AppConstant";

const AllApplications = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [rowData, setRowData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [noOfFiltersApplied, setNoOfFiltersApplied] = useState(0);
  const [alreadySelectedFilters, setAlreadySelectedFilters] = useState({});
  const [openFilterDialog, setOpenFilterDialog] = useState(false);

  const [dataRow, setDataRow] = useState([]);
  const [payloadState, setPayloadState] = useState();

  const allApplications = useSelector(
    (state) => state.assessorAllAssessments.allAssessments
  );

  const filterOptions = [
    {
      filterId: 1,
      displayName: "Assessment Status",
      filterType: "Dropdown",
      key: "assessment_status",
      dataArray: assessmentStatus,
      labelId: "Assessment Status",
      label: "Status",
      filterName: "assessmentStatus",
    },

    {
      filterId: 2,
      displayStartName: "Assessment Start Date",
      displayLastName: "Assessment End Date",
      filterType: "AssessmentDate",
      keysArray: ["assessment_start_date", "assessment_end_date"],
      filterName: "assessmentDate",
    },
    {
      filterId: 3,
      displayStartName: "Application Start Date",
      displayLastName: "Application End Date",
      filterType: "Date",
      keysArray: ["application_start_date", "application_end_date"],
      filterName: "applicationDate",
    },
  ];
  const columnDefs = [
    { field: "Application Number" },
    { field: "Assessment Number" },
    { field: "Assessment Date" },
    { field: "Application Date" },
    { field: "Enterprise Name" },
    { field: "Assessment Type" },
    { field: "Assigned Agency", minWidth: "11rem" },
    { field: "Status", minWidth: "11rem" },
    {
      field: "",
      width: "2%",
      renderColumn: (row) => {
        return (
          <Box sx={styles.iconButtonWidth}>
            <Box className="icons" sx={commonStyles.displayNone}>
              <Button
                sx={styles.iconBtnDiv}
                // onClick={() => navigate(`/all-applications/${row.id}`)}
                onClick={() =>
                  navigate(
                    `/assessor-allApplications/assessment-details/${row.id}`
                  )
                }
              >
                <Tooltip title="View" arrow>
                  <ContentPasteSearchIcon style={commonStyles.iconColor} />
                </Tooltip>
              </Button>
            </Box>
          </Box>
        );
      },
    },
  ];

  useEffect(() => {
    if (allApplications?.assessments) {
      const dataSet = allApplications?.assessments?.map((application) => {
        return {
          "Application Number": application?.application?.application_number,
          "Assessment Number": application?.assessment_number,
          "Assessment Date": moment(application?.assessment_date).format(
            "D MMM,YYYY"
          ),
          "Application Date":
            application?.application?.application_date &&
            moment(application?.application?.application_date).format(
              "D MMM,YYYY"
            ),
          "Enterprise Name": application?.application?.enterprise_name,
          "Assessment Type": application?.assessment_type,
          "Assigned Agency": application?.agency?.title,
          Status: getAssessmentStatus(application),
          id: application?.uuid,
        };
      });
      setRowData(dataSet);
    }
  }, [allApplications]);

  useEffect(() => {
    if (pageNumber) {
      dispatch(
        getAllApplicationsAction({ start: pageNumber, ...payloadState })
      );
    }
  }, [pageNumber]);
  const saveHandler = (payload) => {
    dispatch(
      getAllApplicationsAction({
        start: pageNumber,
        ...payload,
      })
    );
    setPageNumber(1);
    setPayloadState(payload);
  };
  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const exportButtonClick = () => {
    const url = `${process.env.REACT_APP_BASE_URL}/assessor/assessments?is_excel=1&excel_type=assessor_assessments`;
    dispatch(
      downloadAction("AssessorApplications", dispatch, url, "doc", "xlsx")
    );
  };

  return (
    <>
      <CustomTable
        columnDefs={columnDefs}
        rowData={rowData}
        title="All Assessments"
        hasPagination
        handlePagination={handlePagination}
        currentPage={pageNumber}
        totalValues={allApplications?.total}
        styleData={commonStyles.tableHeight}
        buttonsDivStyle={commonStyles.divFlexStyle}
        exportButtonClick={exportButtonClick}
        hasExport={true}
        hasFilter={true}
        saveHandler={saveHandler}
        filterOptions={filterOptions}
        alreadySelectedFilters={alreadySelectedFilters}
        setOpenFilterDialog={setOpenFilterDialog}
        setNoOfFiltersApplied={setNoOfFiltersApplied}
        setAlreadySelectedFilters={setAlreadySelectedFilters}
      />
    </>
  );
};

export default AllApplications;
