export const styles = {
  iconButtonWidth: {
    width: "20px",
  },
  iconBtnDiv: {
    minWidth: "unset",
    padding: 0,
    // marginRight: "1.75rem",
    "&:hover": { backgroundColor: "transparent" },
  },
};
