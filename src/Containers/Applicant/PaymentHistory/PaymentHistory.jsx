import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import * as moment from "moment";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import { commonStyles } from "../../../Styles/CommonStyles";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { getPaymentHistoryListAction } from "../../../Redux/Payment/paymentActions";
import { afterDiscountValue } from "../../../Services/commonService";
import { PAYMENT_HISTORY } from "../../../Routes/Routes";
import { Button, Box, Tooltip } from "@mui/material";
import ContentPasteSearchIcon from "@mui/icons-material/ContentPasteSearch";
import { APP_STAGE } from "../../../Constant/RoleConstant";
import { downloadAction } from "../../../Redux/AllApplications/allApplicationsActions";

export default function PaymentHistory() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [rowData, setRowData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);

  const paymentHistoryList = useSelector(
    (state) => state.payment.paymentHistoryList
  );

  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(PAYMENT_HISTORY);
    });
  }, []);

  const columnDefs = [
    { field: "Order No." },
    { field: "Enterprise Name", ellipsisClass: true },
    { field: "Payment Date" },
    { field: "Amount(INR)" },
    { field: "Status" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Box sx={commonStyles.paymentHistoryIcon}>
            <Box className="icons" sx={commonStyles.displayNone}>
              <Button
                sx={commonStyles.iconButtonStyle}
                onClick={() =>
                  row.Status === "Complete" &&
                  navigate(
                    `/payment-history/payment-complete-details/${row.id}`
                  )
                }
              >
                <Tooltip title="View" arrow>
                  <ContentPasteSearchIcon
                    style={
                      row.Status === "Complete"
                        ? commonStyles.iconColor
                        : commonStyles.greyIconColor
                    }
                  />
                </Tooltip>
              </Button>
            </Box>
          </Box>
        );
      },
    },
  ];
  useEffect(() => {
    if (paymentHistoryList) {
      const dataSet = paymentHistoryList?.transactions?.map((payment) => {
        return {
          "Order No.": payment.order_number,
          "Enterprise Name": payment.application?.enterprise_name,
          "Payment Date": moment(payment.created_at).format("D MMM,YYYY"),
          "Amount(INR)": payment?.final_amount
            ? Number(payment?.final_amount)?.toLocaleString("en-IN", {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })
            : "NA",
          Status: payment.status,
          id: payment.application.application_uuid,
        };
      });
      setRowData(dataSet);
    }
  }, [paymentHistoryList]);

  useEffect(() => {
    if (pageNumber) {
      dispatch(getPaymentHistoryListAction({ start: pageNumber }));
    }
  }, [pageNumber]);
  const handlePagination = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const exportButtonClick = () => {
    const url = `${process.env.REACT_APP_BASE_URL}/applicant/payment-history?is_excel=1&excel_type=payment_history`;
    dispatch(downloadAction("PaymentHistory", dispatch, url, "doc", "xlsx"));
  };

  return (
    <>
      {/* {paymentHistoryList?.total === 0 ? (
        <QpTypography
          displayText="No data available"
          styleData={commonStyles.noData}
        />
      ) : ( */}
      <CustomTable
        columnDefs={columnDefs}
        rowData={rowData}
        title="Payment History"
        hasPagination
        handlePagination={handlePagination}
        currentPage={pageNumber}
        totalValues={paymentHistoryList?.total}
        styleData={commonStyles.tableHeight}
        buttonsDivStyle={commonStyles.divFlexStyle}
        exportButtonClick={exportButtonClick}
        hasExport={true}
      />
      {/* )} */}
    </>
  );
}
