import { Box, Button, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { getApplicationDetailAction } from "../../../Redux/AllApplications/allApplicationsActions";
import { ReactComponent as IconCircularBack } from "../../../Assets/Images/iconCircularBack.svg";
import { commonStyles } from "../../../Styles/CommonStyles";
import PaymentDetails from "./PaymentDetails";
import { ALL_APPLICATIONS, PAYMENT_HISTORY } from "../../../Routes/Routes";
import PaymentBreakupDetails from "../../Admin/AllApplications/PaymentBreakupDetails/PaymentBreakupDetails";
import { getUserData } from "../../../Services/localStorageService";
import { ROLES } from "../../../Constant/RoleConstant";
import { getAdminApplicationDetailAction } from "../../../Redux/Admin/AdminAllApplications/adminAllApplicationAction";
import Row from "../../../Components/Row/Row";

const PaymentCompleteDetails = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const userData = getUserData();

  const [applicationDetail, setApplicationDetail] = useState();
  const applicantApplicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );
  const drdoApplicationDetail = useSelector(
    (state) => state.adminAllApplications.adminApplicationDetail
  );
  const { id } = useParams();

  useEffect(() => {
    if (
      userData?.roles[0] === ROLES.DRDO ||
      userData?.roles[0] === ROLES.ADMIN
    ) {
      dispatch(getAdminApplicationDetailAction(id));
    } else if (userData?.roles[0] === ROLES.VENDOR) {
      dispatch(getApplicationDetailAction(id));
    }
  }, [dispatch, id, userData?.roles[0]]);

  useEffect(() => {
    if (
      (userData?.roles[0] === ROLES.DRDO ||
        userData?.roles[0] === ROLES.ADMIN) &&
      drdoApplicationDetail
    ) {
      setApplicationDetail(drdoApplicationDetail);
    } else if (
      userData?.roles[0] === ROLES.VENDOR &&
      applicantApplicationDetail
    ) {
      setApplicationDetail(applicantApplicationDetail);
    }
  }, [userData?.roles[0], drdoApplicationDetail, applicantApplicationDetail]);

  return (
    <>
      <Box sx={commonStyles.headerOuterContainer}>
        <Box sx={commonStyles.topBackButtonHeader}>
          <Button
            disableRipple
            sx={commonStyles.backButton}
            onClick={() => {
              if (userData?.roles[0] === ROLES.VENDOR) {
                navigate(PAYMENT_HISTORY);
              } else {
                navigate(ALL_APPLICATIONS);
              }
            }}
          >
            <IconCircularBack />
          </Button>
          <Typography style={commonStyles.titleBackText}>
            Payment Detail
          </Typography>
        </Box>
      </Box>
      <Box sx={commonStyles.paddingContainer}>
        {/* <PaymentDetails applicationDetail={applicationDetail} /> */}
        <PaymentBreakupDetails
          applicationDetail={applicationDetail}
          tdsFeeTextRequired={true}
          tdsApplicable={
            applicationDetail?.payment_order?.tan_number ? true : false
          }
        />
      </Box>
      <Box sx={commonStyles.paddingContainer}>
        <Typography
          style={{ ...commonStyles.titleBackText, ...commonStyles.borderLine }}
        >
          Tax Information
        </Typography>
        <Row
          label="GSTIN Applicable?"
          value={applicationDetail?.payment_order?.gst_number ? "Yes" : "No"}
        />
        {applicationDetail?.payment_order?.gst_number && (
          <Row
            label="GST Number"
            value={applicationDetail?.payment_order?.gst_number}
          />
        )}
        <Row
          label="TDS Deductible?"
          value={applicationDetail?.payment_order?.tan_number ? "Yes" : "No"}
        />
        {applicationDetail?.payment_order?.tan_number && (
          <Row
            label="TAN Number"
            value={applicationDetail?.payment_order?.tan_number}
          />
        )}
      </Box>
    </>
  );
};

export default PaymentCompleteDetails;
