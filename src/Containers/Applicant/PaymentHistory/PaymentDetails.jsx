import React, { useEffect, useState } from "react";
import Row from "../../../Components/Row/Row";
import {
  afterDiscountValue,
  percentValue,
} from "../../../Services/commonService";
import QpRadioButton from "../../../Components/QpRadioButton/QpRadioButton";
import { yesNo } from "../../../Constant/AppConstant";
import { Box, Grid } from "@mui/material";
import QpInputLabel from "../../../Components/QpInputLabel/QpInputLabel";
import { useLocation, useSearchParams } from "react-router-dom";
import { getUserData } from "../../../Services/localStorageService";
import { ROLES } from "../../../Constant/RoleConstant";
import { commonStyles } from "../../../Styles/CommonStyles";

export default function PaymentDetails(props) {
  const {
    applicationDetail,
    paymentData,
    orderNumber,
    certificationFeeText,
    certificationFeeValue,
    assessorFeeText,
    assessorFeeValue,
    gstFeeText,
    gstFeeValue,
    tdsFeeText,
    tdsFeeValue,
    isSubsidized,
    subsidiyPercent,
    subsidiyFeeText,
    data,
    setData,
    tdsFeeTextRequired,
    taxValue,
    setTaxValue,
    taxPercent,
  } = props;
  // const [paymentData, setPaymentData] = useState();
  const location = useLocation();
  const [newAmount, setNewAmount] = useState();
  const [gstAmount, setGstAmount] = useState(0);
  const [taxAmount, setTaxAmount] = useState(0);
  // const [taxValue, setTaxValue] = useState();
  const [taxOptions, setTaxOptions] = useState([]);
  const userData = getUserData();
  const [searchParams] = useSearchParams();
  const readOnly = searchParams.get("readOnly");

  const [assessorFee, setAssessorFee] = useState(0);
  const handleRadioButton = (event) => {
    setData((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };
  // useEffect(() => {
  //   if (applicationDetail?.payment_order?.payment_breakup) {
  //     setPaymentData(
  //       JSON.parse(applicationDetail?.payment_order?.payment_breakup)
  //     );
  //   }
  // }, [applicationDetail?.payment_order?.payment_breakup]);

  useEffect(() => {
    if (assessorFeeValue) {
      setAssessorFee(assessorFeeValue);
    }
  }, [assessorFeeValue]);

  useEffect(() => {
    if (tdsFeeValue) {
      setTaxOptions(tdsFeeValue?.split(","));
    }
  }, [tdsFeeValue]);

  useEffect(() => {
    if (tdsFeeValue == 0 && setTaxValue) {
      setTaxValue("0");
    } else if (taxOptions?.length === 1 && setTaxValue) {
      setTaxValue(taxOptions[0]);
    }
  }, [tdsFeeValue, taxOptions]);

  useEffect(() => {
    if (data?.whether_local_assessor == 0 && assessorFeeValue) {
      setAssessorFee(assessorFeeValue);
    } else if (
      data?.whether_local_assessor == 1 &&
      location.pathname.includes("all-applications")
    ) {
      setAssessorFee(0);
    } else if (assessorFeeValue) {
      setAssessorFee(assessorFeeValue);
    }
  }, [data?.whether_local_assessor, assessorFeeValue, location.pathname]);

  useEffect(() => {
    if (
      certificationFeeValue &&
      // assessorFeeValue &&
      assessorFee?.toString()
      // &&
      // isSubsidized === 1
    ) {
      const afterDiscountAmount = afterDiscountValue(
        Number(certificationFeeValue) + Number(assessorFee),
        subsidiyPercent
      );
      setNewAmount(afterDiscountAmount);
    }
    // else if (
    //   certificationFeeValue &&
    //   // assessorFeeValue &&
    //   assessorFee?.toString()
    //   //  &&
    //   // isSubsidized === 0
    // ) {
    //   const afterDiscountAmount = afterDiscountValue(
    //     Number(certificationFeeValue) + Number(assessorFee),
    //     0
    //   );
    //   setNewAmount(afterDiscountAmount);
    // }
  }, [subsidiyPercent, assessorFee, paymentData]);

  useEffect(() => {
    if (taxPercent && newAmount) {
      setTaxAmount(percentValue(newAmount, Number(taxPercent)));
    } else if (taxPercent === 0 && newAmount) {
      setTaxAmount(percentValue(newAmount, 0));
    }
  }, [taxPercent, newAmount]);

  useEffect(() => {
    if (newAmount && gstFeeValue?.toString()) {
      setGstAmount(
        Number(percentValue(newAmount, Number(gstFeeValue)).toFixed(2))
      );
    }
    // if (newAmount && taxValue?.toString() && tdsFeeTextRequired) {
    //   setTaxAmount(percentValue(newAmount, Number(taxValue)));
    // }
    //  else if (newAmount && gstFeeValue && isSubsidized === 1) {
    //   setGstAmount(percentValue(newAmount, gstFeeValue));
    //   setTaxAmount(percentValue(newAmount, 0));
    // }
  }, [newAmount, taxValue, gstFeeValue, isSubsidized, tdsFeeTextRequired]);

  const handleDropdownChange = (event) => {
    setTaxValue(event.target.value);
  };

  return (
    <>
      {location.pathname.includes("all-applications") &&
        (userData?.roles[0] === ROLES.ADMIN ||
          userData?.roles[0] === ROLES.DRDO) &&
        !readOnly && (
          <Grid
            container
            columnSpacing={"3.5rem"}
            sx={commonStyles.localAssessorDiv}
          >
            <Grid item md={6}>
              <QpInputLabel
                displayText="Whether local assessors?"
                styleData={commonStyles.localAssessorText}
              />
            </Grid>
            <Grid item md={6}>
              <QpRadioButton
                options={yesNo}
                name="whether_local_assessor"
                onChange={handleRadioButton}
                value={data?.whether_local_assessor}
              />
            </Grid>
          </Grid>
        )}
      <Row label="Order No." value={`${orderNumber}`} />
      <Row
        label={`${certificationFeeText}`}
        value={`Rs. ${(certificationFeeValue + assessorFee)?.toLocaleString(
          "en-IN",
          { minimumFractionDigits: 2, maximumFractionDigits: 2 }
        )}`}
      />
      {/* <Row
        label={`${assessorFeeText}`}
        value={`Rs. ${assessorFee?.toLocaleString("en-IN")}`}
        
      /> */}
      {/* {isSubsidized ? ( */}
      <Row
        label={`${subsidiyFeeText} (${subsidiyPercent}%)`}
        // value={`: Rs. ${applicationDetail?.payment_order?.amount}`}
        value={`Rs. ${percentValue(
          Number(certificationFeeValue) + Number(assessorFee),
          subsidiyPercent
        )?.toLocaleString("en-IN", {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })}`}
      />
      {/* ) : (
        <Row
          label={`${subsidiyFeeText} (${0}%)`}
          // value={`: Rs. ${applicationDetail?.payment_order?.amount}`}
          value={`Rs. ${percentValue(
            Number(certificationFeeValue) + Number(assessorFee),
            0
          )?.toLocaleString("en-IN")}`}
          
        />
      )} */}
      {/* <Row
        label={`${subsidiyFeeText} (${subsidiyPercent}%)`}
        // value={`: Rs. ${applicationDetail?.payment_order?.amount}`}
        value={`Rs. ${percentValue(
          Number(certificationFeeValue) + Number(assessorFee),
          subsidiyPercent
        )?.toLocaleString("en-IN")}`}
        
      /> */}
      <Row
        label={`Amount Payable`}
        // value={`: Rs. ${applicationDetail?.payment_order?.amount}`}
        value={`Rs. ${newAmount?.toLocaleString("en-IN", {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })}`}
      />
      <Row
        label={`${gstFeeText} (${gstFeeValue}%)`}
        value={`Rs. ${(
          percentValue(newAmount, gstFeeValue) || gstAmount
        )?.toLocaleString("en-IN", {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })}`}
      />
      {(taxPercent === 0 || taxPercent) &&
      location.pathname.includes("payment") ? (
        <Row
          label={`${tdsFeeText} ${`(${taxPercent}%)`}`}
          value={`Rs. ${taxAmount?.toLocaleString("en-IN", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}`}
          isDropdown={false}
          // isDropdown={false}
          // dropdownOptions={taxOptions}
          // dropdownValue={2}
          // handleDropdownChange={handleDropdownChange}
        />
      ) : (
        tdsFeeTextRequired &&
        location.pathname.includes("payment") && (
          <Row
            label={`${tdsFeeText} ${
              taxOptions?.length === 1 ? `(${tdsFeeValue}%)` : ""
            }`}
            value={`Rs. ${taxAmount?.toLocaleString("en-IN", {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}`}
            isDropdown={taxOptions?.length > 1 ? true : false}
            // isDropdown={false}
            dropdownOptions={taxOptions}
            // dropdownValue={2}
            handleDropdownChange={handleDropdownChange}
          />
        )
      )}

      <Row
        label="Total Amount to be paid"
        value={`Rs ${(
          Number(newAmount) +
          gstAmount -
          taxAmount
        )?.toLocaleString("en-IN", {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })}`}
      />
    </>
  );
}
