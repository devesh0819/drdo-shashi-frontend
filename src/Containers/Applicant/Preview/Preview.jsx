import React, { useState, useEffect, useRef } from "react";
import QpFormContainer from "../../../Components/QpFormContainer/QpFormContainer";
import { Typography, Box } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { styles } from "./PreviewStyles";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import SaveNextButtons from "../../../Components/SaveNextButtons/SaveNextButtons";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { submitApplicationAction } from "../../../Redux/ApplicantForm/applicantFormActions";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../../Redux/AllApplications/allApplicationsActions";
import QpDialog from "../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../Components/QpConfirmModal/QpConfirmModal";
import CustomApplicatonView from "../../../Components/CustomApplicationView/CustomApplicationView";
import { APP_STAGE } from "../../../Constant/RoleConstant";
import { HOME } from "../../../Routes/Routes";
import { appStatusAfterSubmit } from "../../../Constant/AppConstant";
import * as _ from "lodash";
const Preview = () => {
  const [submitDialog, setSubmitDialog] = useState(false);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();
  const runOnce = useRef(false);

  const { id } = useParams();

  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
    };
  }, [id]);

  useEffect(() => {
    if (
      applicationDetail &&
      applicationDetail?.application_stage &&
      // Object.keys(applicationDetail)?.length > 0 &&
      (applicationDetail?.application_stage === APP_STAGE.PENDING_APPROVAL ||
        applicationDetail?.application_stage === APP_STAGE.APPROVED ||
        applicationDetail?.application_stage === APP_STAGE.REJECTED ||
        applicationDetail?.application_stage === APP_STAGE.SCHEDULED ||
        applicationDetail?.application_stage === APP_STAGE.PAYMENT_APPROVED ||
        applicationDetail?.application_stage === APP_STAGE.PAYMENT_DONE ||
        applicationDetail?.application_stage ===
          APP_STAGE.APPLICATION_ASSESSMENT_COMPLETED ||
        applicationDetail?.application_stage === APP_STAGE.FEEDBACK_SUBMITTED)
    ) {
      navigate(HOME);
    }
  }, [applicationDetail?.application_stage]);

  const editHandler = () => {
    navigate(`/general-info/${id}`);
  };

  const submitHandler = () => {
    dispatch(
      submitApplicationAction(applicationDetail?.application_uuid, navigate)
    );
    // navigate(PAYMENT);
  };

  return (
    <>
      <QpFormContainer
        isStepperRequired={false}
        changeNavigate={`/step2-details/${id}`}
      >
        <Box sx={styles.headingContainer}>
          <Typography sx={styles.heading}>
            Review your answers before sending your application
          </Typography>
        </Box>

        <Box
          sx={{
            ...customCommonStyles.marginTopOne,
            ...commonStyles.positionRealtive,
          }}
        >
          <CustomApplicatonView applicationDetail={applicationDetail} />
        </Box>
        <SaveNextButtons
          blueButtonText="Submit"
          greyButtonText="Edit"
          onSaveClick={editHandler}
          onNextClick={() => setSubmitDialog(true)}
        />
      </QpFormContainer>
      <QpDialog
        open={submitDialog}
        closeModal={setSubmitDialog}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to submit application?"
          closeModal={setSubmitDialog}
          onConfirm={submitHandler}
        />
      </QpDialog>
    </>
  );
};
export default Preview;
