import * as colors from "../../../Constant/ColorConstant";

export const styles = {
  label: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1rem",
    lineHeight: "1.362rem",
    color: colors.BLACKISH,
    "@media(max-width:600px)": {
      fontSize: "0.8rem",
      fontWeight: 600,
    },
  },
  applicantLabel: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "0.8rem",
    lineHeight: "1rem",
    color: colors.BLACKISH,
    "@media(max-width:600px)": {
      fontSize: "0.6rem",
      fontWeight: 600,
    },
  },
  value: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "1rem",
    lineHeight: "1.362rem",
    color: "#454545",
    "@media(max-width:600px)": {
      fontSize: "0.8rem",
      fontWeight: 300,
    },
  },
  applicantValue: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.8rem",
    lineHeight: "1rem",
    color: "#454545",
    "@media(max-width:600px)": {
      fontSize: "0.6rem",
      fontWeight: 300,
    },
  },
  heading: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1.5rem",
    lineHeight: "2.043rem",
    color: colors.BLACKISH,
    "@media(max-width:600px)": {
      fontSize: "1rem",
      fontWeight: 600,
    },
  },
  subHeadings: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "1.25rem",
    lineHeight: "1.703rem",
    color: colors.BLACKISH,
    marginBottom: "1.938rem",
    marginTop: "1.938rem",
    "@media(max-width:600px)": {
      fontSize: "0.95rem",
      fontWeight: 400,
      marginBottom: "1.138rem",
      marginTop: "1.138rem",
    },
  },
  headingContainer: {
    borderBottom: "0.063rem solid #EAEAEA",
    paddingBottom: "1.063rem",
  },
  horizontalLine: {
    borderBottom: "0.063rem solid #EAEAEA",
    paddingTop: "1.538rem",
  },
  bankerText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.85rem",
    lineHeight: "1.403rem",
    color: colors.BLACKISH,
    marginBottom: "1.138rem",
    marginTop: "1.138rem",
    "@media(max-width:600px)": {
      fontSize: "0.75rem",
      fontWeight: 400,
      marginBottom: "1rem",
      marginTop: "1rem",
    },
  },
  image: {
    height: "3.75rem",
    width: "4.063rem",
  },
};
