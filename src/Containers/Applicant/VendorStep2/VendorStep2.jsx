import React, { useState, useEffect, useRef } from "react";
import {
  Box,
  Button,
  InputAdornment,
  Chip,
  InputBase,
  Autocomplete,
  TextField,
  Typography,
} from "@mui/material";
import CustomImageUploader from "../../../Components/CustomImageUploader/CustomImageUploader";
import CustomInput from "../../../Components/CustomInput/CustomInput";
import QpFormContainer from "../../../Components/QpFormContainer/QpFormContainer";
import QpInputBase from "../../../Components/QpInputBase/QpInputBase";
import QpRadioButton from "../../../Components/QpRadioButton/QpRadioButton";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import { styles } from "../Step3Form/Step3FormStyles.js";
import {
  customerType,
  firstDeclaration,
  materialPartsOption,
  ONLY_NUMERIC,
  secondDeclaration,
  states,
  testingMethod,
  units,
  yesNo,
} from "../../../Constant/AppConstant";
import QpButton from "../../../Components/QpButton/QpButton";
import { showToast } from "../../../Components/Toast/Toast";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import { ReactComponent as IconDelete } from "../../../Assets/Images/deleteIcon.svg";
import QpDialog from "../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../Components/QpConfirmModal/QpConfirmModal";
import SaveNextButtons from "../../../Components/SaveNextButtons/SaveNextButtons";
import CustomMultipleImageUploader from "../../../Components/CustomMultipleImageUploader/CustomMultipleImageUploader";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { saveStep3FormAction } from "../../../Redux/ApplicantForm/applicantFormActions";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../../Redux/AllApplications/allApplicationsActions";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as moment from "moment";
import CustomRadioButton from "../../../Components/CustomRadioButton/CustomRadioButton";
import {
  customerSchema,
  partsSchema,
  productSchema,
  rawMaterialSchema,
  step3Schema,
  testingSchema,
  vendorSchema,
} from "../../../validationSchema/step3Schema";
import {
  getCountriesAction,
  getStatesAction,
} from "../../../Redux/Admin/GetMasterData/getMasterDataActions";
import DropdownFilter from "../../../Components/AllFilters/DropdownFilter";
import CustomCheckboxContent from "../../../Components/CustomCheckboxContent/CustomCheckboxContent";
import { APP_STAGE } from "../../../Constant/RoleConstant";
import { HOME } from "../../../Routes/Routes";

export default function VendorStep2() {
  const initialState = {
    admin_manpower_count: "",

    third_party_cert: [],

    productName: "",
    isProductExported: "0",
    percentageExportProduct: "",
    totalProductionCapacityPerYear: "",
    totalCapacityUnit: "Kilogram",
    presentProductionCapacityPerYear: "",
    presentCapacityUnit: "Kilogram",
    dateOfCommencement: "",
    destinationCountries: "",
    product_manufactured: [],

    customerName: "",
    customerType: "1",
    customerCountry: "",
    customerCountryObject: "",
    key_domestic_customers: [],

    key_processes: [],

    processes_outsourced: [],

    rawMaterialName: "",
    whetherRawMaterial: "1",
    rawMaterialPercentageImported: "",
    rawMaterialCountryName: "",
    rawMaterialCountryObject: "",
    rawMaterialImported: "0",
    key_raw_materials: [],

    is_competent_banned: "0",

    is_enquiry_against_firm: "0",
    app_data_confirmation: "",
    app_acceptance_confirmation: "",
  };

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [data, setData] = useState(initialState);
  const {
    register: customerRegister,
    handleSubmit: handleCustomerSubmit,
    setValue: customerSetValue,
    reset: customerReset,
    formState: { errors: customerErrors },
  } = useForm({
    resolver: yupResolver(customerSchema),
    mode: "onChange",
  });

  const {
    register: rawMaterialRegister,
    handleSubmit: handleRawMaterialSubmit,
    setValue: rawMaterialSetValue,
    reset: rawMaterialReset,
    formState: { errors: rawMaterialErrors },
  } = useForm({
    resolver: yupResolver(rawMaterialSchema),
    mode: "onChange",
  });

  const {
    register: productRegister,
    handleSubmit: handleProductSubmit,
    setValue: productSetValue,
    reset: productReset,
    formState: { errors: productErrors },
  } = useForm({
    resolver: yupResolver(productSchema),
    mode: "onChange",
  });

  const [thirdPartyCertChip, setThirdPartyCertChip] = useState([]);
  const [keyProcesses, setKeyProcesses] = useState([]);
  const [outsourcedProcess, setOutsourcedProcess] = useState([]);

  const [openDialog, setOpenDialog] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [stepThreeData, setStepThreeData] = useState({});
  const stepThreeDataRef = useRef(stepThreeData);

  const [customerTableRow, setCustomerTableRow] = useState([]);
  const [productTableRow, setProductTableRow] = useState([]);
  const [rawMaterialTableRow, setRawMaterialTableRow] = useState([]);

  const [productsContainer, setProductsContainer] = useState(false);
  const [customerContainer, setCustomerContainer] = useState(false);
  const [materialContainer, setMaterialContainer] = useState(false);

  const [deleteItem, setDeleteItem] = useState();
  const [dataConfirmation, setDataConfirmation] = useState(false);
  const [acceptanceConfirmation, setAcceptanceConfirmation] = useState(false);

  const runOnce = useRef(false);
  const isDoneRef = useRef(false);
  const location = useLocation();
  const { id } = useParams();

  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );

  const countryOptions = useSelector(
    (state) => state.getMasterData.countriesData
  );

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
    };
  }, [id]);

  useEffect(() => {
    dispatch(getCountriesAction());
  }, []);

  useEffect(() => {
    if (
      applicationDetail &&
      Object.keys(applicationDetail)?.length > 0 &&
      applicationDetail?.ownership &&
      Object.keys(applicationDetail?.ownership)?.length > 0
    ) {
      setData(applicationDetail?.ownership);
      setData((values) => ({
        ...values,
        product_manufactured:
          applicationDetail?.ownership?.product_manufactured || [],
        key_domestic_customers:
          applicationDetail?.ownership?.key_domestic_customers || [],
        key_vendors: applicationDetail?.ownership?.key_vendors || [],
        key_raw_materials:
          applicationDetail?.ownership?.key_raw_materials || [],
        testing_method: applicationDetail?.ownership?.testing_method || [],
        boughtout_parts: applicationDetail?.ownership?.boughtout_parts || [],

        key_processes: applicationDetail?.ownership?.key_processes || [],
        processes_outsourced:
          applicationDetail?.ownership?.processes_outsourced || [],
        energy_resources_used:
          applicationDetail?.ownership?.energy_resources_used || [],
        natural_resources_used:
          applicationDetail?.ownership?.natural_resources_used || [],
        environment_consents_name:
          applicationDetail?.ownership?.environment_consents_name || [],
      }));
    }
  }, [applicationDetail?.ownership]);

  useEffect(() => {
    if (
      applicationDetail &&
      applicationDetail?.application_stage &&
      // Object.keys(applicationDetail)?.length > 0 &&
      applicationDetail?.application_stage !== APP_STAGE.BASIC &&
      applicationDetail?.application_stage !== APP_STAGE.OWNERSHIP
    ) {
      navigate(HOME);
    }
  }, [applicationDetail?.application_stage]);

  useEffect(() => {
    if (
      applicationDetail &&
      applicationDetail?.application_stage &&
      applicationDetail?.application_stage === APP_STAGE.OWNERSHIP
    ) {
      setAcceptanceConfirmation(true);
      setDataConfirmation(true);
    }
  }, [applicationDetail?.application_stage]);

  useEffect(() => {
    productSetValue("isProductExported", "0", { shouldValidate: true });
    productSetValue("totalCapacityUnit", "Kilogram", { shouldValidate: true });

    productSetValue("presentCapacityUnit", "Kilogram", {
      shouldValidate: true,
    });
    // setData((values) => ({
    //   ...values,
    //   isProductExported: "0",
    //   totalCapacityUnit: "Kilogram",
    //   presentCapacityUnit: "Kilogram",
    // }));
  }, []);

  useEffect(() => {
    customerSetValue("customerType", "1", { shouldValidate: true });
  }, []);

  useEffect(() => {
    rawMaterialSetValue("whetherRawMaterial", "1", { shouldValidate: true });
    rawMaterialSetValue("rawMaterialImported", "0", { shouldValidate: true });
  }, []);

  useEffect(() => {
    if (applicationDetail?.ownership?.key_domestic_customers) {
      setData((values) => ({
        ...values,
        key_domestic_customers: JSON.parse(
          applicationDetail?.ownership?.key_domestic_customers
        ),
      }));
    }
  }, [applicationDetail?.ownership?.key_domestic_customers]);

  useEffect(() => {
    if (applicationDetail?.ownership?.key_raw_materials) {
      setData((values) => ({
        ...values,
        key_raw_materials: JSON.parse(
          applicationDetail?.ownership?.key_raw_materials
        ),
      }));
    }
  }, [applicationDetail?.ownership?.key_raw_materials]);

  useEffect(() => {
    if (applicationDetail?.ownership?.product_manufactured) {
      setData((values) => ({
        ...values,
        product_manufactured: JSON.parse(
          applicationDetail?.ownership?.product_manufactured
        ),
      }));
    }
  }, [applicationDetail?.ownership?.product_manufactured]);

  useEffect(() => {
    if (data?.product_manufactured) {
      setProductTableRow(
        data?.product_manufactured?.map((product, index) => {
          return {
            "Name of the product": product.productName,
            "Is the product being exported?":
              product.isProductExported === "1" ? "Yes" : "No",
            "Destination Countries": product.destinationCountries,
            "Percentage(%) of export of the product":
              product.percentageExportProduct || "-",
            "Total Production Capacity Per Year": `${product.totalProductionCapacityPerYear} ${product.totalCapacityUnit}`,
            "Present Production Capacity Per Year": `${product.presentProductionCapacityPerYear} ${product.presentCapacityUnit}`,
            "Date Of Commencement": moment(
              new Date(product.dateOfCommencement)
            ).format("D MMM,YYYY"),
            destinationCountries: product.destinationCountries,
            id: index,
            targetName: "product_manufactured",
          };
        })
      );
    }
  }, [data?.product_manufactured]);

  useEffect(() => {
    if (data?.key_domestic_customers) {
      setCustomerTableRow(
        data?.key_domestic_customers?.map((customer, index) => {
          return {
            "Name of the customer": customer.customerName,
            "Is the customer international or domestic?":
              customer.customerType === "1" ? "Domestic" : "International",
            Country: customer.customerCountry,
            id: index,
            targetName: "key_domestic_customers",
          };
        })
      );
    }
  }, [data?.key_domestic_customers]);

  useEffect(() => {
    if (data?.key_raw_materials) {
      setRawMaterialTableRow(
        data?.key_raw_materials?.map((material, index) => {
          return {
            "Key Raw Material / Bought-out part":
              material.whetherRawMaterial === "1"
                ? "Key Raw Material"
                : "Bought-out part",
            "Name of raw material": material.rawMaterialName,
            "Whether  imported?":
              material.rawMaterialImported === "1" ? "Yes" : "No",
            "Percentage(%) imported":
              material.rawMaterialImported === "1"
                ? material.rawMaterialPercentageImported
                : "-",
            "Name of country from which the material / part is imported":
              material.rawMaterialImported === "1"
                ? material.rawMaterialCountryName
                : "-",
            id: index,
            targetName: "key_raw_materials",
          };
        })
      );
    }
  }, [data?.key_raw_materials]);

  useEffect(() => {
    if (data?.key_processes?.length > 0) {
      setKeyProcesses(data?.key_processes.split(","));
    }
  }, [data?.key_processes]);
  useEffect(() => {
    if (data?.processes_outsourced?.length > 0) {
      setOutsourcedProcess(data?.processes_outsourced.split(","));
    }
  }, [data?.processes_outsourced]);

  useEffect(() => {
    if (data?.third_party_cert?.length > 0) {
      setThirdPartyCertChip(data?.third_party_cert.split(","));
    }
  }, [data?.third_party_cert]);

  const updateState = (newState) => {
    stepThreeDataRef.current = newState;
    setStepThreeData(newState);
    setIsSubmit(false);
  };

  const getData = (key, value) => {
    updateState({
      ...stepThreeDataRef.current,
      [key]: value,
    });
  };

  useEffect(() => {
    if (stepThreeData && Object.keys(stepThreeData)?.length > 0) onSubmit();
  }, [stepThreeData]);

  const handleDropdownChange = (event, name, type) => {
    setData((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
    if (type === "product") {
      productSetValue(`${name}`, event.target.value, { shouldValidate: true });
      setData((values) => ({
        ...values,
        [name]: event.target.value,
      }));
    }
    if (type === "customer") {
      customerSetValue(`${name}`, event.target.value?.title, {
        shouldValidate: true,
      });
      setData((values) => ({
        ...values,
        customerCountryObject: event.target.value,
      }));
    }
    if (type === "rawMaterial") {
      rawMaterialSetValue(`${name}`, event.target.value?.title, {
        shouldValidate: true,
      });
      setData((values) => ({
        ...values,
        rawMaterialCountryObject: event.target.value,
      }));
    }
  };

  const handleRecordConfirm = (item) => {
    setData((values) => ({
      ...values,
      [item.targetName]: values[item.targetName]?.filter(
        (target, ind) => ind !== deleteItem.index
      ),
    }));
  };

  const onEnterKeyPress = (event) => {
    if (event.key === "Enter") {
      const text = event.target.value;
      if (
        text !== "" &&
        !thirdPartyCertChip.includes(text) &&
        text.match(/\b.*[a-zA-Z]+.*\b/)
      ) {
        setThirdPartyCertChip((prevState) => [...prevState, text]);
        event.target.value = "";
      }
    }
  };

  const handleThirdPartCertDelete = (deleteIndex) => {
    setThirdPartyCertChip(
      thirdPartyCertChip.filter((chip, index) => index !== deleteIndex)
    );
  };

  const onEnterKeyProcessPress = (event) => {
    if (event.key === "Enter") {
      const text = event.target.value;
      if (
        text !== "" &&
        !keyProcesses.includes(text) &&
        text.match(/\b.*[a-zA-Z]+.*\b/)
      ) {
        setKeyProcesses((prevState) => [...prevState, event.target.value]);
        event.target.value = "";
      }
    }
  };

  const handleKeyProcessDelete = (deleteIndex) => {
    setKeyProcesses(
      keyProcesses.filter((chip, index) => index !== deleteIndex)
    );
  };
  const onEnterOutsourcedProcessPress = (event) => {
    if (event.key === "Enter") {
      const text = event.target.value;
      if (
        text !== "" &&
        !outsourcedProcess.includes(text) &&
        text.match(/\b.*[a-zA-Z]+.*\b/)
      ) {
        setOutsourcedProcess((prevState) => [...prevState, event.target.value]);
        event.target.value = "";
      }
    }
  };

  const handleOutsourcedProcessDelete = (deleteIndex) => {
    setOutsourcedProcess(
      outsourcedProcess.filter((chip, index) => index !== deleteIndex)
    );
  };

  const handleInputChange = (event) => {
    setData((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const handleRadioButton = (event, name, type) => {
    setData((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
    if (type === "product") {
      productSetValue(`${name}`, event.target.value, { shouldValidate: true });
      setData((values) => ({
        ...values,
        [name]: event.target.value,
      }));
    }
    if (type === "customer") {
      customerSetValue(`${name}`, event.target.value, { shouldValidate: true });
      setData((values) => ({
        ...values,
        [name]: event.target.value,
      }));
    }
    if (type === "rawMaterial") {
      rawMaterialSetValue(`${name}`, event.target.value, {
        shouldValidate: true,
      });
      setData((values) => ({
        ...values,
        [name]: event.target.value,
      }));
    }
  };

  const resetProductHandler = () => {
    setProductsContainer(false);
    productReset({
      productName: "",
      totalCapacityUnit: "Kilogram",
      presentCapacityUnit: "Kilogram",
      isProductExported: "0",
      destinationCountries: [],
      percentageExportProduct: "",
      totalProductionCapacityPerYear: "",
      presentProductionCapacityPerYear: "",
      dateOfCommencement: "",
    });
  };

  const resetCustomerHandler = () => {
    setCustomerContainer(false);
    customerReset({
      customerName: "",
      customerCountry: "",
      customerType: "1",
    });
  };

  const resetRawMaterialHandler = () => {
    setMaterialContainer(false);
    rawMaterialReset({
      rawMaterialName: "",
      rawMaterialPercentageImported: "",
      rawMaterialCountryName: "",
      whetherRawMaterial: "0",
      rawMaterialImported: "0",
    });
  };

  const addProductHandler = (productData) => {
    if (
      productData.productName &&
      productData.isProductExported &&
      productData.totalProductionCapacityPerYear &&
      productData.presentProductionCapacityPerYear &&
      productData.dateOfCommencement &&
      productData.totalCapacityUnit &&
      productData.presentCapacityUnit
    ) {
      if (productData.isProductExported === "1") {
        if (!productData.destinationCountries) {
          showToast("Please select atleast one country", "error");
          return;
        }
        if (!productData.percentageExportProduct) {
          showToast("Percentage of export of the product is required", "error");
          return;
        }
      }
      setData((values) => ({
        ...values,
        product_manufactured: [
          ...values.product_manufactured,
          {
            productName: productData.productName,
            totalCapacityUnit: productData.totalCapacityUnit,
            presentCapacityUnit: productData.presentCapacityUnit,
            isProductExported: productData.isProductExported,
            destinationCountries:
              productData.isProductExported === "1"
                ? productData.destinationCountries
                : "-",
            percentageExportProduct:
              productData.isProductExported === "1"
                ? productData.percentageExportProduct
                : "-",
            totalProductionCapacityPerYear:
              productData.totalProductionCapacityPerYear,
            presentProductionCapacityPerYear:
              productData.presentProductionCapacityPerYear,
            dateOfCommencement: productData.dateOfCommencement,
          },
        ],
      }));
      productReset({
        productName: "",
        totalCapacityUnit: "Kilogram",
        presentCapacityUnit: "Kilogram",
        isProductExported: "0",
        destinationCountries: [],
        percentageExportProduct: "",
        totalProductionCapacityPerYear: "",
        presentProductionCapacityPerYear: "",
        dateOfCommencement: "",
      });
      setProductsContainer(false);
      productSetValue("isProductExported", "0");
      setData((values) => ({
        ...values,
        productName: "",
        totalCapacityUnit: "Kilogram",
        presentCapacityUnit: "Kilogram",
        isProductExported: "0",
        destinationCountries: "",
        percentageExportProduct: "",
        totalProductionCapacityPerYear: "",
        presentProductionCapacityPerYear: "",
        dateOfCommencement: "",
      }));
    } else {
      showToast("Please fill all product details", "error");
      return;
    }
  };

  const productTableColumn = [
    { field: "Name of the product" },
    { field: "Is the product being exported?" },
    {
      field: "Destination Countries",
      renderColumn: (row) => {
        return (
          <>
            {row.destinationCountries === "-"
              ? "-"
              : row?.destinationCountries?.length &&
                row?.destinationCountries?.map((country) => {
                  return (
                    <Typography sx={commonStyles.countryText} key={country}>
                      {country.title}
                    </Typography>
                  );
                })}
          </>
        );
      },
    },
    { field: "Percentage(%) of export of the product" },
    { field: "Total Production Capacity Per Year" },
    { field: "Present Production Capacity Per Year" },
    { field: "Date Of Commencement" },

    {
      field: "",
      renderColumn: (row) => {
        return (
          <Button
            sx={commonStyles.cursorPointer}
            onClick={() => {
              setDeleteItem({ targetName: row.targetName, index: row.id });
              setOpenDialog(true);
            }}
          >
            <IconDelete />
          </Button>
        );
      },
    },
  ];

  const addCustomerHandler = (customerData) => {
    if (customerData.customerName && customerData.customerType) {
      setData((values) => ({
        ...values,
        key_domestic_customers: [
          ...values.key_domestic_customers,
          {
            customerName: customerData.customerName,
            customerType: customerData.customerType,
            customerCountry:
              customerData.customerType === "0"
                ? customerData.customerCountry
                : "-",
          },
        ],
      }));
      customerReset({
        customerName: "",
        customerCountry: "",
        customerType: "1",
      });
      setCustomerContainer(false);
      // customerSetValue("customerName", "", { shouldValidate: false });
      // customerSetValue("customerType", "1", { shouldValidate: false });
      // customerSetValue("customerCountry", "", { shouldValidate: false });

      setData((values) => ({
        ...values,
        customerName: "",
        customerCountry: "",
        customerType: "1",
        customerCountryObject: "",
      }));
    } else {
      showToast("Please fill customer details", "error");
    }
  };

  const customerTableColumn = [
    { field: "Name of the customer" },
    { field: "Is the customer international or domestic?" },
    { field: "Country" },

    {
      field: "",
      renderColumn: (row) => {
        return (
          <Button
            sx={commonStyles.cursorPointer}
            onClick={() => {
              setDeleteItem({ targetName: row.targetName, index: row.id });
              setOpenDialog(true);
            }}
          >
            <IconDelete />
          </Button>
        );
      },
    },
  ];

  const rawMaterialTableColumn = [
    { field: "Key Raw Material / Bought-out part" },
    { field: "Name of raw material" },
    { field: "Whether  imported?" },
    { field: "Percentage(%) imported" },
    { field: "Name of country from which the material / part is imported" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Button
            sx={commonStyles.cursorPointer}
            onClick={() => {
              setDeleteItem({ targetName: row.targetName, index: row.id });
              setOpenDialog(true);
            }}
          >
            <IconDelete />
          </Button>
        );
      },
    },
  ];

  const addRawMaterialHandler = (rawMaterialData) => {
    if (
      rawMaterialData.rawMaterialName &&
      rawMaterialData.rawMaterialImported
    ) {
      if (
        rawMaterialData.rawMaterialImported == 1 &&
        (!rawMaterialData?.rawMaterialPercentageImported ||
          !rawMaterialData?.rawMaterialCountryName)
      ) {
        showToast("Please fill raw material details", "error");
        return;
      }
      setData((values) => ({
        ...values,
        key_raw_materials: [
          ...values.key_raw_materials,
          {
            rawMaterialName: rawMaterialData.rawMaterialName,
            rawMaterialPercentageImported:
              rawMaterialData.rawMaterialPercentageImported || "-",
            rawMaterialCountryName:
              rawMaterialData.rawMaterialCountryName || "-",
            whetherRawMaterial: rawMaterialData.whetherRawMaterial,
            rawMaterialImported: rawMaterialData.rawMaterialImported,
          },
        ],
      }));
      rawMaterialReset({
        rawMaterialName: "",
        rawMaterialPercentageImported: "",
        rawMaterialCountryName: "",
        whetherRawMaterial: "0",
        rawMaterialImported: "0",
      });
      setMaterialContainer(false);
      // rawMaterialSetValue("rawMaterialName", "", { shouldValidate: false });
      // rawMaterialSetValue("rawMaterialPercentageImported", "", {
      //   shouldValidate: false,
      // });
      // rawMaterialSetValue("rawMaterialCountryName", "", {
      //   shouldValidate: false,
      // });
      // rawMaterialSetValue("whetherRawMaterial", "0", {
      //   shouldValidate: false,
      // });
      // rawMaterialSetValue("rawMaterialImported", "0", {
      //   shouldValidate: false,
      // });
      // rawMaterialSetValue("rawMaterialName", "", { shouldValidate: false });

      setData((values) => ({
        ...values,
        rawMaterialName: "",
        rawMaterialPercentageImported: "",
        rawMaterialCountryName: "",
        rawMaterialCountryObject: "",
        whetherRawMaterial: "0",
        rawMaterialImported: "0",
      }));
    } else {
      showToast("Please fill raw material details", "error");
    }
  };

  const onSubmit = () => {
    const { admin_manpower_count } = stepThreeData;
    // if (!admin_manpower_count) {
    //   return;
    // }
    if (!isDoneRef.current) {
      return;
    }
    if (!dataConfirmation || !acceptanceConfirmation) {
      showToast("Declaration is required", "error");
      return;
    }
    isDoneRef.current = false;

    let completeData = {
      product_manufactured: JSON.stringify(data.product_manufactured),
      key_domestic_customers: JSON.stringify(data.key_domestic_customers),
      key_processes: keyProcesses,
      processes_outsourced: outsourcedProcess,
      key_raw_materials: JSON.stringify(data.key_raw_materials),
      admin_manpower_count: admin_manpower_count,
      third_party_cert: thirdPartyCertChip,
      is_competent_banned: data.is_competent_banned,
      is_enquiry_against_firm: data.is_enquiry_against_firm,
      app_data_confirmation: dataConfirmation ? 1 : 0,
      app_acceptance_confirmation: acceptanceConfirmation ? 1 : 0,
    };

    const formData = new FormData();

    if (data.admin_manpower_count) {
      formData.append("admin_manpower_count", data.admin_manpower_count);
    } else {
      formData.append("admin_manpower_count", "");
    }

    if (data.product_manufactured?.length > 0) {
      formData.append(
        "product_manufactured",
        JSON.stringify(data.product_manufactured)
      );
    } else if (
      applicationDetail?.ownership?.product_manufactured &&
      data.product_manufactured?.length === 0
    ) {
      formData.append("product_manufactured", "");
    }

    if (data.key_domestic_customers?.length > 0) {
      formData.append(
        "key_domestic_customers",
        JSON.stringify(data.key_domestic_customers)
      );
    } else if (
      applicationDetail?.ownership?.key_domestic_customers &&
      data.key_domestic_customers?.length === 0
    ) {
      formData.append("key_domestic_customers", "");
    }

    if (data.key_raw_materials?.length > 0) {
      formData.append(
        "key_raw_materials",
        JSON.stringify(data.key_raw_materials)
      );
    } else if (
      applicationDetail?.ownership?.key_raw_materials &&
      data.key_raw_materials?.length === 0
    ) {
      formData.append("key_raw_materials", "");
    }

    formData.append("is_competent_banned", data.is_competent_banned);

    formData.append("is_enquiry_against_firm", data.is_enquiry_against_firm);
    if (keyProcesses?.length > 0) {
      formData.append("key_processes", keyProcesses);
    } else if (
      applicationDetail?.ownership?.key_processes &&
      keyProcesses?.length === 0
    ) {
      formData.append("key_processes", "");
    }

    if (outsourcedProcess?.length > 0) {
      formData.append("processes_outsourced", outsourcedProcess);
    } else if (
      applicationDetail?.ownership?.processes_outsourced &&
      outsourcedProcess?.length === 0
    ) {
      formData.append("processes_outsourced", "");
    }

    // if (envConsents?.length > 0) {
    //   formData.append("environment_consents_name", envConsents);
    // } else if (
    //   applicationDetail?.ownership?.environment_consents_name &&
    //   envConsents?.length === 0
    // ) {
    //   formData.append("environment_consents_name", "");
    // }
    formData.append("third_party_cert", thirdPartyCertChip);

    dispatch(
      saveStep3FormAction(
        applicationDetail?.application_uuid,
        formData,
        navigate,
        dispatch
      )
    );
  };

  const nextHandler = () => {
    setIsSubmit(true);
    isDoneRef.current = true;
  };

  return (
    <QpFormContainer activeStep={2} changeNavigate={`/general-info/${id}`}>
      {/* <Box sx={styles.productContainer}>
        <Typography sx={styles.productHeading}>Product/Articles</Typography>
      </Box> */}
      <Box>
        <Box sx={{ ...commonStyles.displayStyle }}>
          <QpTypography
            displayText="15. Products/Articles"
            styleData={{ ...commonStyles.label, ...styles.labelStyle }}
          />
          <QpButton
            displayText={
              productTableRow?.length !== 0
                ? "Add More Products"
                : "Add Products"
            }
            // onClick={() => addProductHandler("product_manufactured")}
            // onClick={handleProductSubmit(addProductHandler)}
            onClick={() => setProductsContainer(true)}
            styleData={{ ...commonStyles.blueButton, ...styles.addButtonStyle }}
            textStyle={{
              ...commonStyles.blueButtonText,
              ...styles.addButtontextStyle,
            }}
            isDisable={productsContainer ? true : false}
          />
        </Box>
        {productsContainer && (
          <Box sx={{ ...styles.showContainer }}>
            <Box>
              <QpTypography
                displayText="15.1 Name of the product"
                styleData={styles.labelStyle}
              />
              <InputBase
                sx={{
                  ...styles.inputBaseStyle,
                  ...commonStyles.fullWidth,
                }}
                name="productName"
                onChange={handleInputChange}
                {...productRegister("productName")}
                error={productErrors.productName ? true : false}
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={productErrors.productName?.message}
              />
            </Box>
            <Box sx={customCommonStyles.marginBottomOne}>
              <QpTypography
                displayText="15.2 Is the product being exported?  "
                styleData={styles.labelStyle}
              />
              <QpRadioButton
                options={yesNo}
                name="isProductExported"
                onChange={(event) =>
                  handleRadioButton(event, "isProductExported", "product")
                }
                value={data.isProductExported || "0"}
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={productErrors.isProductExported?.message}
              />
            </Box>
            {data.isProductExported == 1 && (
              <>
                <Box sx={customCommonStyles.marginBottomOne}>
                  <QpTypography
                    displayText="15.2.1 Select Destination Countries  "
                    styleData={styles.labelStyle}
                  />
                  <Autocomplete
                    multiple
                    filterSelectedOptions
                    id="tags-outlined"
                    options={countryOptions}
                    onChange={(event, newValue) => {
                      productSetValue("destinationCountries", newValue, {
                        shouldValidate: true,
                      });
                    }}
                    // {...productRegister("destinationCountries", [])}
                    // defaultValue={props.defaultAssessorsArray}
                    getOptionLabel={(option) => option.title}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="outlined"
                        style={commonStyles.dropdownTextField}
                      />
                    )}
                  />
                  <QpTypography
                    styleData={commonStyles.errorText}
                    displayText={productErrors.destinationCountries?.message}
                  />
                </Box>
                <Box>
                  <QpTypography
                    displayText="15.2.2. Percentage(%) of export of the product  "
                    styleData={styles.labelStyle}
                  />
                  <InputBase
                    sx={{
                      ...styles.inputBaseStyle,
                      ...commonStyles.fullWidth,
                    }}
                    name="percentageExportProduct"
                    onChange={handleInputChange}
                    {...productRegister("percentageExportProduct")}
                    error={productErrors.percentageExportProduct ? true : false}
                  />
                  <QpTypography
                    styleData={commonStyles.errorText}
                    displayText={productErrors.percentageExportProduct?.message}
                  />
                </Box>
              </>
            )}
            <Box
              sx={{ ...commonStyles.displayStyle, ...styles.capacityStyles }}
            >
              <Box>
                <QpTypography
                  displayText="15.3 Total production capacity per year (as on date of application for assessment) "
                  styleData={styles.labelStyle}
                />
                <InputBase
                  sx={{
                    ...styles.inputBaseStyle,
                    ...commonStyles.fullWidth,
                  }}
                  name="totalProductionCapacityPerYear"
                  onChange={handleInputChange}
                  {...productRegister("totalProductionCapacityPerYear")}
                  error={
                    productErrors.totalProductionCapacityPerYear ? true : false
                  }
                />
                <QpTypography
                  styleData={commonStyles.errorText}
                  displayText={
                    productErrors.totalProductionCapacityPerYear?.message
                  }
                />
              </Box>
              <Box sx={{ ...styles.unitStyle }}>
                <QpTypography
                  displayText="Select unit "
                  styleData={styles.labelStyle}
                />
                <DropdownFilter
                  value={data.totalCapacityUnit || "Kilogram"}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(event, "totalCapacityUnit", "product")
                  }
                  dataArray={units}
                  dropdownStyle={styles.dropdownStyle}
                />
                <QpTypography
                  styleData={commonStyles.errorText}
                  displayText={productErrors.totalCapacityUnit?.message}
                />
              </Box>
            </Box>
            <Box
              sx={{ ...commonStyles.displayStyle, ...styles.capacityStyles }}
            >
              <Box>
                <QpTypography
                  displayText="15.4. Current utilized capacity per year (as on date of application for assessment )"
                  styleData={styles.labelStyle}
                />
                <InputBase
                  sx={{
                    ...styles.inputBaseStyle,
                    ...commonStyles.fullWidth,
                  }}
                  name="presentProductionCapacityPerYear"
                  onChange={handleInputChange}
                  {...productRegister("presentProductionCapacityPerYear")}
                  error={
                    productErrors.presentProductionCapacityPerYear
                      ? true
                      : false
                  }
                />
                <QpTypography
                  styleData={commonStyles.errorText}
                  displayText={
                    productErrors.presentProductionCapacityPerYear?.message
                  }
                />
              </Box>
              <Box sx={{ ...styles.unitStyle }}>
                <QpTypography
                  displayText="Select unit "
                  styleData={styles.labelStyle}
                />
                <DropdownFilter
                  value={data.presentCapacityUnit || "Kilogram"}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(
                      event,
                      "presentCapacityUnit",
                      "product"
                    )
                  }
                  dataArray={units}
                  dropdownStyle={styles.dropdownStyle}
                />
                <QpTypography
                  styleData={commonStyles.errorText}
                  displayText={productErrors.presentCapacityUnit?.message}
                />
              </Box>
            </Box>

            <Box>
              <QpTypography
                displayText="15.5. Date of Commencement of Manufacturing of the product"
                styleData={styles.labelStyle}
              />
              <InputBase
                sx={{
                  ...styles.inputBaseStyle,
                  ...commonStyles.fullWidth,
                }}
                type="date"
                name="dateOfCommencement"
                onChange={handleInputChange}
                {...productRegister("dateOfCommencement")}
                error={productErrors.dateOfCommencement ? true : false}
                inputProps={{
                  max: moment(
                    new Date().setDate(new Date().getDate() - 1)
                  ).format("YYYY-MM-DD"),
                }}
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={productErrors.dateOfCommencement?.message}
              />
            </Box>
            <SaveNextButtons
              blueButtonText="Save"
              greyButtonText="Cancel"
              onSaveClick={resetProductHandler}
              onNextClick={handleProductSubmit(addProductHandler)}
              buttonContainerStyle={commonStyles.customButtonConatinerStyle}
              greyButtonStyle={commonStyles.greyButtonStyle}
              blueButtonStyle={commonStyles.blueButtonStyle}
              blueButtonTextStyle={commonStyles.buttonTextStyle}
              greyButtonTextStyle={commonStyles.buttonTextStyle}
            />
          </Box>
        )}
      </Box>

      {/* <QpButton
        displayText="Add Product"
        // onClick={() => addProductHandler("product_manufactured")}
        onClick={handleProductSubmit(addProductHandler)}
        styleData={{ ...commonStyles.blueButton, ...styles.addButtonStyle }}
        textStyle={{
          ...commonStyles.blueButtonText,
          ...styles.addButtontextStyle,
        }}
      /> */}
      {productTableRow?.length !== 0 && (
        <CustomTable
          columnDefs={productTableColumn}
          rowData={productTableRow}
          styleData={commonStyles.tableStyleData}
          boxStyle={{
            ...commonStyles.tableBoxStyleData,
            ...customCommonStyles.marginBottomOne,
          }}
        />
      )}

      {/* <CustomImageUploader
        file={data.product_document}
        value={data.product_document}
        setValue={setData}
        displayText="7. List of products manufactured at the unit"
        isLabel={true}
        name="product_document"
        typeText="Type: .jpg,.jpeg,.png, Max Size:5MB "
        accept=".jpg,.jpeg,.png"
        required={false}
        docType="image"
      /> */}
      <Box>
        <Box sx={{ ...commonStyles.displayStyle }}>
          <QpTypography
            displayText="16. Customer Details"
            styleData={{ ...commonStyles.label, ...styles.labelStyle }}
          />
          <QpButton
            displayText={
              customerTableRow?.length === 0
                ? "Add Customer"
                : "Add More Customers"
            }
            // onClick={() => addCustomerHandler("key_domestic_customers")}
            onClick={() => setCustomerContainer(true)}
            styleData={{ ...commonStyles.blueButton, ...styles.addButtonStyle }}
            textStyle={{
              ...commonStyles.blueButtonText,
              ...styles.addButtontextStyle,
            }}
            isDisable={customerContainer}
          />
        </Box>
        {customerContainer && (
          <Box sx={{ ...styles.showContainer }}>
            <Box>
              <QpTypography
                displayText="16.1 Name of the Customer"
                styleData={styles.labelStyle}
              />

              <InputBase
                sx={{
                  ...styles.inputBaseStyle,
                  ...commonStyles.fullWidth,
                }}
                name="customerName"
                onChange={handleInputChange}
                {...customerRegister("customerName")}
                error={customerErrors.customerName ? true : false}
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={customerErrors.customerName?.message}
              />
            </Box>
            <Box sx={customCommonStyles.marginBottomOne}>
              <QpTypography
                displayText="16.2 Is the customer international  or domestic?"
                styleData={styles.labelStyle}
              />
              <QpRadioButton
                options={customerType}
                name="customerType"
                onChange={(event) =>
                  handleRadioButton(event, "customerType", "customer")
                }
                value={data.customerType || "1"}
                // value={0}
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={customerErrors.customerType?.message}
              />
            </Box>
            {data.customerType == 0 && (
              <Box>
                <QpTypography
                  displayText="16.2.1 Country"
                  styleData={styles.labelStyle}
                />
                <DropdownFilter
                  value={data.customerCountryObject}
                  handleDropdownChange={(event) =>
                    handleDropdownChange(event, "customerCountry", "customer")
                  }
                  dataArray={countryOptions}
                  dropdownStyle={styles.dropdownStyle}
                  isLocation={true}
                  source="title"
                />

                <QpTypography
                  styleData={commonStyles.errorText}
                  displayText={customerErrors.customerCountry?.message}
                />
              </Box>
            )}
            <SaveNextButtons
              blueButtonText="Save"
              greyButtonText="Cancel"
              onSaveClick={resetCustomerHandler}
              onNextClick={handleCustomerSubmit(addCustomerHandler)}
              buttonContainerStyle={commonStyles.customButtonConatinerStyle}
              greyButtonStyle={commonStyles.greyButtonStyle}
              blueButtonStyle={commonStyles.blueButtonStyle}
              blueButtonTextStyle={commonStyles.buttonTextStyle}
              greyButtonTextStyle={commonStyles.buttonTextStyle}
            />{" "}
          </Box>
        )}
      </Box>
      {customerTableRow?.length !== 0 && (
        <CustomTable
          columnDefs={customerTableColumn}
          rowData={customerTableRow}
          styleData={commonStyles.tableStyleData}
          boxStyle={{
            ...commonStyles.tableBoxStyleData,
            ...customCommonStyles.marginBottomOne,
          }}
        />
      )}
      <Box>
        <QpTypography
          displayText="17. Key Processes (e.g. machining, electroplating, heat treatment, mixing, etc.)"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpInputBase
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          onKeyPress={onEnterKeyProcessPress}
          placeholder="Type name and hit enter"
        />
        {keyProcesses?.map((chip, index) => {
          return (
            <Chip
              color="info"
              size="small"
              key={index}
              index={index}
              onDelete={() => handleKeyProcessDelete(index)}
              label={chip}
              sx={styles.chipContainer}
            />
          );
        })}
      </Box>
      <Box>
        <QpTypography
          displayText="18. Outsourced Processes "
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpInputBase
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          onKeyPress={onEnterOutsourcedProcessPress}
          placeholder="Type name and hit enter"
        />
        {outsourcedProcess?.map((chip, index) => {
          return (
            <Chip
              color="info"
              size="small"
              key={index}
              index={index}
              onDelete={() => handleOutsourcedProcessDelete(index)}
              label={chip}
              sx={styles.chipContainer}
            />
          );
        })}
      </Box>
      <Box>
        <Box sx={{ ...commonStyles.displayStyle }}>
          <QpTypography
            displayText="19. Key Raw Material / Bought-out part"
            styleData={{ ...commonStyles.label, ...styles.labelStyle }}
          />
          <QpButton
            displayText={
              rawMaterialTableRow?.length === 0
                ? "Add Raw Material/Bought-out part"
                : "Add More Raw Material/Bought-out part"
            }
            styleData={{ ...commonStyles.blueButton, ...styles.rawButtonStyle }}
            textStyle={{
              ...commonStyles.blueButtonText,
              ...styles.addButtontextStyle,
            }}
            // onClick={() => addRawMaterialHandler("key_raw_materials")}
            onClick={() => setMaterialContainer(true)}
            isDisable={materialContainer}
          />
        </Box>
        {materialContainer && (
          <>
            <Box sx={customCommonStyles.marginBottomOne}>
              <QpTypography
                displayText="19.1. Key Raw Material / Bought-out part"
                styleData={styles.labelStyle}
              />
              <QpRadioButton
                options={materialPartsOption}
                name="whetherRawMaterial"
                onChange={(event) =>
                  handleRadioButton(event, "whetherRawMaterial", "rawMaterial")
                }
                value={data.whetherRawMaterial || "1"}
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={rawMaterialErrors.whetherRawMaterial?.message}
              />
            </Box>
            <QpTypography
              displayText="19.2. Name of the Raw Material or Bought-out part"
              styleData={styles.labelStyle}
            />
            <InputBase
              sx={{
                ...styles.inputBaseStyle,
                ...commonStyles.fullWidth,
              }}
              name="rawMaterialName"
              onChange={handleInputChange}
              {...rawMaterialRegister("rawMaterialName")}
              error={rawMaterialErrors.rawMaterialName ? true : false}
            />
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={rawMaterialErrors.rawMaterialName?.message}
            />
            <Box sx={customCommonStyles.marginBottomOne}>
              <QpTypography
                displayText="19.3 Whether imported?"
                styleData={styles.labelStyle}
              />
              <QpRadioButton
                options={yesNo}
                name="rawMaterialImported"
                onChange={(event) =>
                  handleRadioButton(event, "rawMaterialImported", "rawMaterial")
                }
                value={data.rawMaterialImported || "0"}
              />
              <QpTypography
                styleData={commonStyles.errorText}
                displayText={rawMaterialErrors.rawMaterialImported?.message}
              />
            </Box>
            {data.rawMaterialImported == 1 && (
              <>
                <Box>
                  <QpTypography
                    displayText="19.3.1 Percentage(%) imported"
                    styleData={styles.labelStyle}
                  />
                  <InputBase
                    sx={{
                      ...styles.inputBaseStyle,
                      ...commonStyles.fullWidth,
                    }}
                    name="rawMaterialPercentageImported"
                    onChange={handleInputChange}
                    {...rawMaterialRegister("rawMaterialPercentageImported")}
                    error={
                      rawMaterialErrors.rawMaterialPercentageImported
                        ? true
                        : false
                    }
                  />
                  <QpTypography
                    styleData={commonStyles.errorText}
                    displayText={
                      rawMaterialErrors.rawMaterialPercentageImported?.message
                    }
                  />
                </Box>
                <Box>
                  <QpTypography
                    displayText="19.3.2 Name of country from which the material / part is imported"
                    styleData={styles.labelStyle}
                  />
                  <DropdownFilter
                    value={data.rawMaterialCountryObject}
                    handleDropdownChange={(event) =>
                      handleDropdownChange(
                        event,
                        "rawMaterialCountryName",
                        "rawMaterial"
                      )
                    }
                    dataArray={countryOptions}
                    dropdownStyle={styles.dropdownStyle}
                    isLocation={true}
                    source="title"
                  />

                  {/* <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="rawMaterialCountryName"
          // setValueData={setData}
          onChange={handleInputChange}
          {...rawMaterialRegister("rawMaterialCountryName")}
          error={rawMaterialErrors.rawMaterialCountryName ? true : false}
          // value={data.rawMaterialCountryName}
          // defaultValue={data.rawMaterialCountryName}
          // registerName="rawMaterialCountryName"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData("rawMaterialCountryName", data?.rawMaterialCountryName)
          // }
        /> */}
                  <QpTypography
                    styleData={commonStyles.errorText}
                    displayText={
                      rawMaterialErrors.rawMaterialCountryName?.message
                    }
                  />
                </Box>
              </>
            )}
            <SaveNextButtons
              blueButtonText="Save"
              greyButtonText="Cancel"
              onSaveClick={resetRawMaterialHandler}
              onNextClick={handleRawMaterialSubmit(addRawMaterialHandler)}
              buttonContainerStyle={commonStyles.customButtonConatinerStyle}
              greyButtonStyle={commonStyles.greyButtonStyle}
              blueButtonStyle={commonStyles.blueButtonStyle}
              blueButtonTextStyle={commonStyles.buttonTextStyle}
              greyButtonTextStyle={commonStyles.buttonTextStyle}
            />
          </>
        )}
      </Box>

      {rawMaterialTableRow?.length !== 0 && (
        <CustomTable
          columnDefs={rawMaterialTableColumn}
          rowData={rawMaterialTableRow}
          styleData={commonStyles.tableStyleData}
          boxStyle={{
            ...commonStyles.tableBoxStyleData,
            ...customCommonStyles.marginBottomOne,
          }}
        />
      )}
      <Box sx={commonStyles.fullWidth}>
        <CustomInput
          label="20. Number of people employed in the enterprise"
          // labelStyleData={styles.subHeadingLabel}
          name="admin_manpower_count"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.admin_manpower_count}
          defaultValue={data.admin_manpower_count}
          registerName="admin_manpower_count"
          isRequired={false}
          labelRequired={false}
          submit={isSubmit}
          setData={(data) =>
            getData("admin_manpower_count", data?.admin_manpower_count)
          }
        />
      </Box>
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="21. Third Party Certifications"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpInputBase
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          onKeyPress={onEnterKeyPress}
          placeholder="Type name and hit enter"
        />
        {thirdPartyCertChip?.map((chip, index) => {
          return (
            <Chip
              color="info"
              size="small"
              key={index}
              index={index}
              onDelete={() => handleThirdPartCertDelete(index)}
              label={chip}
              sx={styles.chipContainer}
            />
          );
        })}
      </Box>
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="22. Whether blacklisted/banned ever by any competent authority?"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpRadioButton
          options={yesNo}
          name="is_competent_banned"
          onChange={handleRadioButton}
          value={data.is_competent_banned}
        />
      </Box>
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="23. Any ongoing enquiry/investigation against firm?"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpRadioButton
          options={yesNo}
          name="is_enquiry_against_firm"
          onChange={handleRadioButton}
          value={data.is_enquiry_against_firm}
        />
      </Box>
      <CustomCheckboxContent
        checked={dataConfirmation}
        setChecked={setDataConfirmation}
        displayText={firstDeclaration}
      />
      <CustomCheckboxContent
        checked={acceptanceConfirmation}
        setChecked={setAcceptanceConfirmation}
        displayText={secondDeclaration}
      />
      <SaveNextButtons
        blueButtonText="Save & Next"
        onNextClick={nextHandler}
        greyButtonRequired={false}
      />
      <QpDialog
        open={openDialog}
        closeModal={setOpenDialog}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to delete this record?"
          closeModal={setOpenDialog}
          onConfirm={() => handleRecordConfirm(deleteItem)}
        />
      </QpDialog>
    </QpFormContainer>
  );
}
