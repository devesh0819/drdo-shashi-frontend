import React, { useState, useEffect, useRef } from "react";
import { Box, Button, InputAdornment, Chip, InputBase } from "@mui/material";
import CustomImageUploader from "../../../Components/CustomImageUploader/CustomImageUploader";
import CustomInput from "../../../Components/CustomInput/CustomInput";
import QpFormContainer from "../../../Components/QpFormContainer/QpFormContainer";
import QpInputBase from "../../../Components/QpInputBase/QpInputBase";
import QpRadioButton from "../../../Components/QpRadioButton/QpRadioButton";
import QpTypography from "../../../Components/QpTypography/QpTypography";
import { commonStyles, customCommonStyles } from "../../../Styles/CommonStyles";
import { styles } from "./Step3FormStyles.js";
import {
  customerType,
  ONLY_NUMERIC,
  testingMethod,
  yesNo,
} from "../../../Constant/AppConstant";
import QpButton from "../../../Components/QpButton/QpButton";
import { showToast } from "../../../Components/Toast/Toast";
import CustomTable from "../../../Components/CustomTable/CustomTable";
import { ReactComponent as IconDelete } from "../../../Assets/Images/deleteIcon.svg";
import QpDialog from "../../../Components/QpDialog/QpDialog";
import QpConfirmModal from "../../../Components/QpConfirmModal/QpConfirmModal";
import SaveNextButtons from "../../../Components/SaveNextButtons/SaveNextButtons";
import CustomMultipleImageUploader from "../../../Components/CustomMultipleImageUploader/CustomMultipleImageUploader";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { saveStep3FormAction } from "../../../Redux/ApplicantForm/applicantFormActions";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../../Redux/AllApplications/allApplicationsActions";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as moment from "moment";
import CustomRadioButton from "../../../Components/CustomRadioButton/CustomRadioButton";
import {
  customerSchema,
  partsSchema,
  productSchema,
  rawMaterialSchema,
  step3Schema,
  testingSchema,
  vendorSchema,
} from "../../../validationSchema/step3Schema";

export default function Step3Form() {
  const initialState = {
    msme_representative_selfie: "",
    site_exterior_picture: "",

    work_area_covered: "",
    work_area_uncovered: "",
    work_area_bond_rooms: "",
    work_area_num_bond_rooms: "",
    work_prod_area: "",
    work_testing_area: "",
    proof_attached: [],

    admin_manpower_count: "",
    admin_skill_set: "",
    tech_skilled_count: "",
    tech_skill_set: "",
    tech_unskill_count: "",
    tech_unskill_set: "",

    is_on_lease: "1",
    is_work_covered_leased: "1",
    is_work_uncovered_leased: "1",
    is_work_bond_leased: "1",
    is_work_numbond_leased: "1",
    is_work_prod_leased: "1",
    is_work_test_leased: "1",

    financial_capital_outlay: "",
    financial_capital_outlay_first_year: "",
    financial_capital_outlay_second_year: "",
    financial_capital_outlay_third_year: "",

    financial_source: "",
    turnover_3_year: "",
    turnover_first_year: "",
    turnover_second_year: "",
    turnover_third_year: "",

    profit_3_year: "",
    profit_first_year: "",
    profit_second_year: "",
    profit_third_year: "",

    acc_loss_3_year: "",
    acc_loss_first_year: "",
    acc_loss_second_year: "",
    acc_loss_third_year: "",
    financial_audit_conducted: 0,

    third_party_cert: [],

    productName: "",
    totalProductionCapacityPerYear: "",
    presentProductionCapacityPerYear: "",
    totalProductionCapacityPerMonth: "",
    presentProductionCapacityPerMonth: "",
    spareCapacity: "",
    dateOfCommencement: "",
    isCostAudit: "0",
    costAudit: "",
    fireSafety: "0",
    licnenseDetail: "",
    isGovernmentRegulatory: "0",
    governmentRegulatory: "",
    defectLevel: "",
    product_manufactured: [],

    product_document: "",

    customerName: "",
    customerType: "1",
    customerAddress: "",
    key_domestic_customers: [],

    key_processes: [],

    processes_outsourced: [],

    energy_resources_used: [],
    natural_resources_used: [],
    environment_consents_name: [],

    rawMaterialName: "",
    rawMaterialPercentageImported: "",
    rawMaterialCountryName: "",
    rawMaterialSourceOfProcurement: "",
    rawMaterialImported: "0",
    key_raw_materials: [],

    partsName: "",
    partsPercentageImported: "",
    partsCountryName: "",
    partsSourceOfProcurement: "",
    partsImported: "0",
    boughtout_parts: [],

    testName: "",
    externalTesting: "0",
    agencyTestName: "",
    agencyTestAddress: "",
    testing_method: [],

    acceptance_quality_level: "",

    name: "",
    address: "",
    key_vendors: [],

    is_competent_banned: "0",
    competent_banned_details: "",

    is_enquiry_against_firm: "0",
    enquiry_against_firm_detail: "",
  };

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [data, setData] = useState(initialState);
  const {
    register: customerRegister,
    handleSubmit: handleCustomerSubmit,
    setValue: customerSetValue,
    formState: { errors: customerErrors },
  } = useForm({
    resolver: yupResolver(customerSchema),
    mode: "onChange",
  });
  const {
    register: vendorRegister,
    handleSubmit: handleVendorSubmit,
    setValue: vendorSetValue,
    formState: { errors: vendorErrors },
  } = useForm({
    resolver: yupResolver(vendorSchema),
    mode: "onChange",
  });
  const {
    register: rawMaterialRegister,
    handleSubmit: handleRawMaterialSubmit,
    setValue: rawMaterialSetValue,
    formState: { errors: rawMaterialErrors },
  } = useForm({
    resolver: yupResolver(rawMaterialSchema),
    mode: "onChange",
  });
  const {
    register: partsRegister,
    handleSubmit: handlePartsSubmit,
    setValue: partsSetValue,
    formState: { errors: partsErrors },
  } = useForm({
    resolver: yupResolver(partsSchema),
    mode: "onChange",
  });
  const {
    register: productRegister,
    handleSubmit: handleProductSubmit,
    setValue: productSetValue,
    formState: { errors: productErrors },
  } = useForm({
    resolver: yupResolver(productSchema),
    mode: "onChange",
  });

  const {
    register: testingRegister,
    handleSubmit: handleTestingSubmit,
    setValue: testingSetValue,
    formState: { errors: testingErrors },
  } = useForm({
    resolver: yupResolver(testingSchema),
    mode: "onChange",
  });

  const [thirdPartyCertChip, setThirdPartyCertChip] = useState([]);
  const [keyProcesses, setKeyProcesses] = useState([]);
  const [outsourcedProcess, setOutsourcedProcess] = useState([]);
  const [energyResources, setEnergyResources] = useState([]);
  const [naturalResources, setNaturalResources] = useState([]);
  const [envConsents, setEnvConsents] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [stepThreeData, setStepThreeData] = useState({});
  const stepThreeDataRef = useRef(stepThreeData);

  const [customerTableRow, setCustomerTableRow] = useState([]);
  const [vendorTableRow, setVendorTableRow] = useState([]);
  const [productTableRow, setProductTableRow] = useState([]);
  const [rawMaterialTableRow, setRawMaterialTableRow] = useState([]);
  const [partsTableRow, setPartsTableRow] = useState([]);
  const [testingTableRow, setTestingTableRow] = useState([]);

  const [deleteItem, setDeleteItem] = useState();
  const [newDocArray, setNewDocArray] = useState([]);

  const runOnce = useRef(false);
  const isDoneRef = useRef(false);
  const location = useLocation();
  const { id } = useParams();

  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
    };
  }, [id]);

  useEffect(() => {
    if (
      applicationDetail &&
      Object.keys(applicationDetail)?.length > 0 &&
      applicationDetail?.ownership &&
      Object.keys(applicationDetail?.ownership)?.length > 0
    ) {
      setData(applicationDetail?.ownership);
      setData((values) => ({
        ...values,
        product_manufactured:
          applicationDetail?.ownership?.product_manufactured || [],
        key_domestic_customers:
          applicationDetail?.ownership?.key_domestic_customers || [],
        key_vendors: applicationDetail?.ownership?.key_vendors || [],
        key_raw_materials:
          applicationDetail?.ownership?.key_raw_materials || [],
        testing_method: applicationDetail?.ownership?.testing_method || [],
        boughtout_parts: applicationDetail?.ownership?.boughtout_parts || [],

        key_processes: applicationDetail?.ownership?.key_processes || [],
        processes_outsourced:
          applicationDetail?.ownership?.processes_outsourced || [],
        energy_resources_used:
          applicationDetail?.ownership?.energy_resources_used || [],
        natural_resources_used:
          applicationDetail?.ownership?.natural_resources_used || [],
        environment_consents_name:
          applicationDetail?.ownership?.environment_consents_name || [],
      }));
    }
  }, [applicationDetail?.ownership]);

  useEffect(() => {
    if (
      applicationDetail?.ownership?.msme_representative_selfie?.attachment_uuid
    ) {
      setData((values) => ({
        ...values,
        msme_representative_selfie: {
          name: `${applicationDetail?.ownership?.msme_representative_selfie?.attachment_uuid}`,
        },
      }));
    }
  }, [
    applicationDetail?.ownership?.msme_representative_selfie?.attachment_uuid,
  ]);

  useEffect(() => {
    if (applicationDetail?.ownership?.site_exterior_picture?.attachment_uuid) {
      setData((values) => ({
        ...values,
        site_exterior_picture: {
          name: `${applicationDetail?.ownership?.site_exterior_picture?.attachment_uuid}`,
        },
      }));
    }
  }, [applicationDetail?.ownership?.site_exterior_picture?.attachment_uuid]);

  useEffect(() => {
    if (applicationDetail?.ownership?.product_document?.attachment_uuid) {
      setData((values) => ({
        ...values,
        product_document: {
          name: `${applicationDetail?.ownership?.product_document?.attachment_uuid}`,
        },
      }));
    }
  }, [applicationDetail?.ownership?.product_document?.attachment_uuid]);

  useEffect(() => {
    if (applicationDetail?.ownership?.proofs) {
      const proofArray = applicationDetail?.ownership?.proofs?.map((proof) => {
        return { name: proof?.attachment_uuid };
      });
      setData((values) => ({
        ...values,
        proof_attached: proofArray,
      }));
    }
  }, [applicationDetail?.ownership?.proofs]);

  useEffect(() => {
    if (applicationDetail?.ownership?.financial_capital_outlay) {
      const capitalOutlay = JSON.parse(
        applicationDetail?.ownership?.financial_capital_outlay
      );
      setData((values) => ({
        ...values,
        financial_capital_outlay: JSON.parse(
          applicationDetail?.ownership?.financial_capital_outlay
        ),
        financial_capital_outlay_first_year:
          capitalOutlay[0]?.financial_capital_outlay_first_year,
        financial_capital_outlay_second_year:
          capitalOutlay[0]?.financial_capital_outlay_second_year,
        financial_capital_outlay_third_year:
          capitalOutlay[0]?.financial_capital_outlay_third_year,
      }));
    }
  }, [applicationDetail?.ownership?.financial_capital_outlay]);

  useEffect(() => {
    if (applicationDetail?.ownership?.turnover_3_year) {
      const turnover = JSON.parse(
        applicationDetail?.ownership?.turnover_3_year
      );
      setData((values) => ({
        ...values,
        turnover_3_year: JSON.parse(
          applicationDetail?.ownership?.turnover_3_year
        ),
        turnover_first_year: turnover[0]?.turnover_first_year,
        turnover_second_year: turnover[0]?.turnover_second_year,
        turnover_third_year: turnover[0]?.turnover_third_year,
      }));
    }
  }, [applicationDetail?.ownership?.turnover_3_year]);

  useEffect(() => {
    if (applicationDetail?.ownership?.profit_3_year) {
      const profit = JSON.parse(applicationDetail?.ownership?.profit_3_year);
      setData((values) => ({
        ...values,
        profit_3_year: JSON.parse(applicationDetail?.ownership?.profit_3_year),
        profit_first_year: profit[0]?.profit_first_year,
        profit_second_year: profit[0]?.profit_second_year,
        profit_third_year: profit[0]?.profit_third_year,
      }));
    }
  }, [applicationDetail?.ownership?.profit_3_year]);

  useEffect(() => {
    if (applicationDetail?.ownership?.acc_loss_3_year) {
      const acc_loss = JSON.parse(
        applicationDetail?.ownership?.acc_loss_3_year
      );
      setData((values) => ({
        ...values,
        acc_loss_3_year: JSON.parse(
          applicationDetail?.ownership?.acc_loss_3_year
        ),
        acc_loss_first_year: acc_loss[0]?.acc_loss_first_year,
        acc_loss_second_year: acc_loss[0]?.acc_loss_second_year,
        acc_loss_third_year: acc_loss[0]?.acc_loss_third_year,
      }));
    }
  }, [applicationDetail?.ownership?.acc_loss_3_year]);

  useEffect(() => {
    if (applicationDetail?.ownership?.key_domestic_customers) {
      setData((values) => ({
        ...values,
        key_domestic_customers: JSON.parse(
          applicationDetail?.ownership?.key_domestic_customers
        ),
      }));
    }
  }, [applicationDetail?.ownership?.key_domestic_customers]);

  useEffect(() => {
    if (applicationDetail?.ownership?.key_vendors) {
      setData((values) => ({
        ...values,
        key_vendors: JSON.parse(applicationDetail?.ownership?.key_vendors),
      }));
    }
  }, [applicationDetail?.ownership?.key_vendors]);

  useEffect(() => {
    if (applicationDetail?.ownership?.testing_method) {
      setData((values) => ({
        ...values,
        testing_method: JSON.parse(
          applicationDetail?.ownership?.testing_method
        ),
      }));
    }
    if (applicationDetail?.ownership?.testing_method === null) {
      setData((values) => ({
        ...values,
        testing_method: [],
      }));
    }
  }, [applicationDetail?.ownership?.testing_method]);

  useEffect(() => {
    if (applicationDetail?.ownership?.boughtout_parts) {
      setData((values) => ({
        ...values,
        boughtout_parts: JSON.parse(
          applicationDetail?.ownership?.boughtout_parts
        ),
      }));
    }
  }, [applicationDetail?.ownership?.boughtout_parts]);

  useEffect(() => {
    if (applicationDetail?.ownership?.key_raw_materials) {
      setData((values) => ({
        ...values,
        key_raw_materials: JSON.parse(
          applicationDetail?.ownership?.key_raw_materials
        ),
      }));
    }
  }, [applicationDetail?.ownership?.key_raw_materials]);

  useEffect(() => {
    if (applicationDetail?.ownership?.product_manufactured) {
      setData((values) => ({
        ...values,
        product_manufactured: JSON.parse(
          applicationDetail?.ownership?.product_manufactured
        ),
      }));
    }
  }, [applicationDetail?.ownership?.product_manufactured]);

  useEffect(() => {
    if (data?.key_vendors) {
      setVendorTableRow(
        data?.key_vendors?.map((vendor, index) => {
          return {
            "Name of the vendor": vendor.name,
            Address: vendor.address,
            id: index,
            targetName: "key_vendors",
          };
        })
      );
    }
  }, [data?.key_vendors]);
  useEffect(() => {
    if (data?.product_manufactured) {
      setProductTableRow(
        data?.product_manufactured?.map((product, index) => {
          return {
            "Name of the product": product.productName,
            "Total Production Capacity Per Year":
              product.totalProductionCapacityPerYear,
            "Present Production Capacity Per Year":
              product.presentProductionCapacityPerYear,
            "Total Production Capacity Per Month":
              product.totalProductionCapacityPerMonth,
            "Present Production Capacity Per Month":
              product.presentProductionCapacityPerMonth,
            "Spare Capacity Available": product.spareCapacity,
            "Date Of Commencement": moment(
              new Date(product.dateOfCommencement)
            ).format("D MMM,YYYY"),
            "Cost Audit": product.isCostAudit === "1" ? "Yes" : "No",
            "Cost Audit Details": product.costAudit,
            "Fire Safety": product.fireSafety === "1" ? "Yes" : "No",
            "License Detail": product.licnenseDetail,
            "Government Regulatory":
              product.isGovernmentRegulatory === "1" ? "Yes" : "No",
            "Government Regulatory Details": product.governmentRegulatory,
            "Defect Level/Acceptance Quality Level of the product":
              product.defectLevel,
            id: index,
            targetName: "product_manufactured",
          };
        })
      );
    }
  }, [data?.product_manufactured]);

  useEffect(() => {
    if (data?.key_domestic_customers) {
      setCustomerTableRow(
        data?.key_domestic_customers?.map((customer, index) => {
          return {
            "Name of the customer": customer.customerName,
            "Whether domestic or international?":
              customer.customerType === "1" ? "Domestic" : "International",
            Address: customer.customerAddress,
            id: index,
            targetName: "key_domestic_customers",
          };
        })
      );
    }
  }, [data?.key_domestic_customers]);

  useEffect(() => {
    if (data?.key_raw_materials) {
      setRawMaterialTableRow(
        data?.key_raw_materials?.map((material, index) => {
          return {
            "Name of raw material": material.rawMaterialName,
            "Whether  imported?":
              material.rawMaterialImported === "1" ? "Yes" : "No",
            "Percentage(%) of raw material imported":
              material.rawMaterialPercentageImported,
            "Name of country": material.rawMaterialCountryName,
            "Source of Procurement": material.rawMaterialSourceOfProcurement,
            id: index,
            targetName: "key_raw_materials",
          };
        })
      );
    }
  }, [data?.key_raw_materials]);

  useEffect(() => {
    if (data?.boughtout_parts) {
      setPartsTableRow(
        data?.boughtout_parts?.map((part, index) => {
          return {
            "Name of part/components/sub-assembly/assembly": part.partsName,
            "Whether imported?": part.partsImported === "1" ? "Yes" : "No",
            "Percentage(%) imported": part.partsPercentageImported,
            "Name of country": part.partsCountryName,
            "Source of Procurement": part.partsSourceOfProcurement,
            id: index,
            targetName: "boughtout_parts",
          };
        })
      );
    }
  }, [data?.boughtout_parts]);

  useEffect(() => {
    if (data?.testing_method) {
      setTestingTableRow(
        data?.testing_method?.map((test, index) => {
          return {
            "Name of the test": test.testName,
            "Whether external?":
              test.externalTesting === "1" ? "In-house" : "External",
            "Testing agency name": test.agencyTestName || "-",
            "Testing agency address": test.agencyTestAddress || "-",
            id: index,
            targetName: "testing_method",
          };
        })
      );
    }
  }, [data?.testing_method]);

  useEffect(() => {
    if (data?.energy_resources_used?.length > 0) {
      setEnergyResources(data?.energy_resources_used.split(","));
    }
  }, [data?.energy_resources_used]);
  useEffect(() => {
    if (data?.natural_resources_used?.length > 0) {
      setNaturalResources(data?.natural_resources_used.split(","));
    }
  }, [data?.natural_resources_used]);
  useEffect(() => {
    if (data?.key_processes?.length > 0) {
      setKeyProcesses(data?.key_processes.split(","));
    }
  }, [data?.key_processes]);
  useEffect(() => {
    if (data?.processes_outsourced?.length > 0) {
      setOutsourcedProcess(data?.processes_outsourced.split(","));
    }
  }, [data?.processes_outsourced]);

  useEffect(() => {
    if (data?.environment_consents_name?.length > 0) {
      setEnvConsents(data?.environment_consents_name.split(","));
    }
  }, [data?.environment_consents_name]);

  useEffect(() => {
    if (data?.third_party_cert?.length > 0) {
      setThirdPartyCertChip(data?.third_party_cert.split(","));
    }
  }, [data?.third_party_cert]);

  const updateState = (newState) => {
    stepThreeDataRef.current = newState;
    setStepThreeData(newState);
    setIsSubmit(false);
  };

  const getData = (key, value) => {
    updateState({
      ...stepThreeDataRef.current,
      [key]: value,
    });
  };

  useEffect(() => {
    const {
      msme_representative_selfie,
      site_exterior_picture,
      // product_manufactured,
      // boughtout_parts,
      // key_raw_materials,
      // key_domestic_customers,
      // key_vendors,
      admin_manpower_count,
      tech_skilled_count,
      tech_unskill_count,
    } = stepThreeData;
    // if (
    //   msme_representative_selfie &&
    //   site_exterior_picture
    //   &&
    //   product_manufactured &&
    //   boughtout_parts &&
    //   key_raw_materials &&
    //   key_domestic_customers &&
    //   key_vendors
    //   &&
    //   keyProcesses &&
    //   outsourcedProcess &&
    //   energyResources &&
    //   naturalResources &&
    //   envConsents
    // ) {
    onSubmit();
    // }
  }, [stepThreeData]);

  const handleRecordConfirm = (item) => {
    setData((values) => ({
      ...values,
      [item.targetName]: values[item.targetName]?.filter(
        (target, ind) => ind !== deleteItem.index
      ),
    }));
  };

  const onEnterKeyPress = (event) => {
    if (event.key === "Enter") {
      const text = event.target.value;
      if (
        text !== "" &&
        !thirdPartyCertChip.includes(text) &&
        text.match(/\b.*[a-zA-Z]+.*\b/)
      ) {
        setThirdPartyCertChip((prevState) => [...prevState, text]);
        event.target.value = "";
      }
    }
  };

  const handleThirdPartCertDelete = (deleteIndex) => {
    setThirdPartyCertChip(
      thirdPartyCertChip.filter((chip, index) => index !== deleteIndex)
    );
  };

  const onEnterKeyProcessPress = (event) => {
    if (event.key === "Enter") {
      const text = event.target.value;
      if (
        text !== "" &&
        !keyProcesses.includes(text) &&
        text.match(/\b.*[a-zA-Z]+.*\b/)
      ) {
        setKeyProcesses((prevState) => [...prevState, event.target.value]);
        event.target.value = "";
      }
    }
  };

  const handleKeyProcessDelete = (deleteIndex) => {
    setKeyProcesses(
      keyProcesses.filter((chip, index) => index !== deleteIndex)
    );
  };
  const onEnterOutsourcedProcessPress = (event) => {
    if (event.key === "Enter") {
      const text = event.target.value;
      if (
        text !== "" &&
        !outsourcedProcess.includes(text) &&
        text.match(/\b.*[a-zA-Z]+.*\b/)
      ) {
        setOutsourcedProcess((prevState) => [...prevState, event.target.value]);
        event.target.value = "";
      }
    }
  };

  const handleOutsourcedProcessDelete = (deleteIndex) => {
    setOutsourcedProcess(
      outsourcedProcess.filter((chip, index) => index !== deleteIndex)
    );
  };
  const onEnterEnergyResorucePress = (event) => {
    if (event.key === "Enter") {
      const text = event.target.value;
      if (
        text !== "" &&
        !energyResources.includes(text) &&
        text.match(/\b.*[a-zA-Z]+.*\b/)
      ) {
        setEnergyResources((prevState) => [...prevState, event.target.value]);
        event.target.value = "";
      }
    }
  };

  const handleEnergyResourceDelete = (deleteIndex) => {
    setEnergyResources(
      energyResources.filter((chip, index) => index !== deleteIndex)
    );
  };
  const onEnterNaturalResorucePress = (event) => {
    if (event.key === "Enter") {
      const text = event.target.value;
      if (
        text !== "" &&
        !naturalResources.includes(text) &&
        text.match(/\b.*[a-zA-Z]+.*\b/)
      ) {
        setNaturalResources((prevState) => [...prevState, event.target.value]);
        event.target.value = "";
      }
    }
  };

  const handleNaturalResoruceDelete = (deleteIndex) => {
    setNaturalResources(
      naturalResources.filter((chip, index) => index !== deleteIndex)
    );
  };
  const onEnterEnvConsentPress = (event) => {
    if (event.key === "Enter") {
      const text = event.target.value;
      if (
        text !== "" &&
        !envConsents.includes(text) &&
        text.match(/\b.*[a-zA-Z]+.*\b/)
      ) {
        setEnvConsents((prevState) => [...prevState, event.target.value]);
        event.target.value = "";
      }
    }
  };

  const handleEnvConsentDelete = (deleteIndex) => {
    setEnvConsents(envConsents.filter((chip, index) => index !== deleteIndex));
  };

  const handleInputChange = (event) => {
    setData((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const addVendorHandler = (vendorData) => {
    if (vendorData.name && vendorData.address) {
      setData((values) => ({
        ...values,
        key_vendors: [
          ...values.key_vendors,
          { name: vendorData.name, address: vendorData.address },
        ],
      }));
      vendorSetValue("name", "", { shouldValidate: false });
      vendorSetValue("address", "", { shouldValidate: false });

      setData((values) => ({
        ...values,
        name: "",
        address: "",
      }));
    } else {
      showToast("Please fill vendor details", "error");
    }
  };

  const vendorTableColumn = [
    { field: "Name of the vendor" },
    { field: "Address" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Button
            sx={commonStyles.cursorPointer}
            onClick={() => {
              setDeleteItem({ targetName: row.targetName, index: row.id });
              setOpenDialog(true);
            }}
          >
            <IconDelete />
          </Button>
        );
      },
    },
  ];

  const handleRadioButton = (event) => {
    setData((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const addProductHandler = (productData) => {
    if (
      productData.productName &&
      productData.totalProductionCapacityPerYear &&
      productData.presentProductionCapacityPerYear &&
      productData.totalProductionCapacityPerMonth &&
      productData.presentProductionCapacityPerMonth &&
      productData.spareCapacity &&
      // productData.dateOfCommencement &&
      // productData.costAudit &&
      data.isCostAudit &&
      data.fireSafety &&
      data.isGovernmentRegulatory &&
      productData.defectLevel
      // &&
      // productData.governmentRegulatory
    ) {
      if (data.isCostAudit === "1" && !data.costAudit) {
        showToast("Please fill cost audit details", "error");
        return;
      } else if (
        data.isCostAudit === "1" &&
        (data.costAudit?.length < 5 || data.costAudit?.length > 55)
      ) {
        showToast("Cost audit detail must be 5 to 55 characters long", "error");
        return;
      }
      if (data.fireSafety === "1" && !data.licnenseDetail) {
        showToast("Please fill license details", "error");
        return;
      } else if (
        data.fireSafety === "1" &&
        (data.licnenseDetail?.length < 5 || data.licnenseDetail?.length > 55)
      ) {
        showToast("License detail must be 5 to 55 characters long", "error");
        return;
      }

      if (data.isGovernmentRegulatory === "1" && !data.governmentRegulatory) {
        showToast("Please fill government regulatory norms", "error");
        return;
      } else if (
        data.isGovernmentRegulatory === "1" &&
        (data.governmentRegulatory?.length < 5 ||
          data.governmentRegulatory?.length > 55)
      ) {
        showToast(
          "Government regulatory norms must be 5 to 55 characters long",
          "error"
        );
        return;
      } else {
        setData((values) => ({
          ...values,
          product_manufactured: [
            ...values.product_manufactured,
            {
              productName: productData.productName,
              totalProductionCapacityPerYear:
                productData.totalProductionCapacityPerYear,
              presentProductionCapacityPerYear:
                productData.presentProductionCapacityPerYear,
              totalProductionCapacityPerMonth:
                productData.totalProductionCapacityPerMonth,
              presentProductionCapacityPerMonth:
                productData.presentProductionCapacityPerMonth,
              spareCapacity: productData.spareCapacity,
              dateOfCommencement: productData.dateOfCommencement,
              isCostAudit: data.isCostAudit,
              costAudit: data.isCostAudit === "1" ? data.costAudit : "-",
              fireSafety: data.fireSafety,
              licnenseDetail:
                data.fireSafety === "1" ? data.licnenseDetail : "-",
              isGovernmentRegulatory: data.isGovernmentRegulatory,
              governmentRegulatory:
                data.isGovernmentRegulatory === "1"
                  ? data.governmentRegulatory
                  : "-",
              defectLevel: productData.defectLevel,
            },
          ],
        }));

        productSetValue("productName", "", { shouldValidate: false });
        productSetValue("totalProductionCapacityPerYear", "", {
          shouldValidate: false,
        });
        productSetValue("presentProductionCapacityPerYear", "", {
          shouldValidate: false,
        });
        productSetValue("totalProductionCapacityPerMonth", "", {
          shouldValidate: false,
        });
        productSetValue("presentProductionCapacityPerMonth", "", {
          shouldValidate: false,
        });
        productSetValue("spareCapacity", "", {
          shouldValidate: false,
        });
        productSetValue("dateOfCommencement", "", {
          shouldValidate: false,
        });
        // productSetValue("costAudit", "", {
        //   shouldValidate: false,
        // });
        // productSetValue("licnenseDetail", "", {
        //   shouldValidate: false,
        // });
        // productSetValue("governmentRegulatory", "", {
        //   shouldValidate: false,
        // });
        productSetValue("defectLevel", "", {
          shouldValidate: false,
        });

        setData((values) => ({
          ...values,
          productName: "",
          totalProductionCapacityPerYear: "",
          presentProductionCapacityPerYear: "",
          totalProductionCapacityPerMonth: "",
          presentProductionCapacityPerMonth: "",
          spareCapacity: "",
          dateOfCommencement: "",
          isCostAudit: "0",
          costAudit: "",
          fireSafety: "0",
          licnenseDetail: "",
          isGovernmentRegulatory: "0",
          governmentRegulatory: "",
          defectLevel: "",
        }));
      }
    } else {
      showToast("Please fill product details", "error");
    }
  };

  const productTableColumn = [
    { field: "Name of the product" },
    { field: "Total Production Capacity Per Year" },
    { field: "Present Production Capacity Per Year" },
    { field: "Total Production Capacity Per Month" },
    { field: "Present Production Capacity Per Month" },
    { field: "Spare Capacity Available" },
    { field: "Date Of Commencement" },
    { field: "Cost Audit" },
    { field: "Cost Audit Details" },
    { field: "Fire Safety" },
    { field: "License Detail" },
    { field: "Government Regulatory" },
    { field: "Government Regulatory Details" },
    { field: "Defect Level/Acceptance Quality Level of the product" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Button
            sx={commonStyles.cursorPointer}
            onClick={() => {
              setDeleteItem({ targetName: row.targetName, index: row.id });
              setOpenDialog(true);
            }}
          >
            <IconDelete />
          </Button>
        );
      },
    },
  ];

  const addCustomerHandler = (customerData) => {
    if (
      customerData.customerName &&
      customerData.customerAddress &&
      data.customerType
    ) {
      setData((values) => ({
        ...values,
        key_domestic_customers: [
          ...values.key_domestic_customers,
          {
            customerName: customerData.customerName,
            customerType: data.customerType,
            customerAddress: customerData.customerAddress,
          },
        ],
      }));
      customerSetValue("customerName", "", { shouldValidate: false });
      customerSetValue("customerAddress", "", { shouldValidate: false });

      setData((values) => ({
        ...values,
        customerName: "",
        customerAddress: "",
        customerType: "1",
      }));
    } else {
      showToast("Please fill customer details", "error");
    }
  };

  const customerTableColumn = [
    { field: "Name of the customer" },
    { field: "Whether domestic or international?" },
    { field: "Address" },

    {
      field: "",
      renderColumn: (row) => {
        return (
          <Button
            sx={commonStyles.cursorPointer}
            onClick={() => {
              setDeleteItem({ targetName: row.targetName, index: row.id });
              setOpenDialog(true);
            }}
          >
            <IconDelete />
          </Button>
        );
      },
    },
  ];

  const rawMaterialTableColumn = [
    { field: "Name of raw material" },
    { field: "Whether  imported?" },
    { field: "Percentage(%) of raw material imported" },
    { field: "Name of country" },
    { field: "Source of Procurement" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Button
            sx={commonStyles.cursorPointer}
            onClick={() => {
              setDeleteItem({ targetName: row.targetName, index: row.id });
              setOpenDialog(true);
            }}
          >
            <IconDelete />
          </Button>
        );
      },
    },
  ];

  const addRawMaterialHandler = (rawMaterialData) => {
    if (
      rawMaterialData.rawMaterialName &&
      // rawMaterialData.rawMaterialPercentageImported &&
      // rawMaterialData.rawMaterialCountryName &&
      // rawMaterialData.rawMaterialSourceOfProcurement &&
      data.rawMaterialImported
    ) {
      if (
        data.rawMaterialImported == 1 &&
        (!rawMaterialData?.rawMaterialPercentageImported ||
          !rawMaterialData?.rawMaterialCountryName ||
          !rawMaterialData?.rawMaterialSourceOfProcurement)
      ) {
        showToast("Please fill raw material details", "error");
        return;
      }
      setData((values) => ({
        ...values,
        key_raw_materials: [
          ...values.key_raw_materials,
          {
            rawMaterialName: rawMaterialData.rawMaterialName,
            rawMaterialPercentageImported:
              rawMaterialData.rawMaterialPercentageImported || "-",
            rawMaterialCountryName:
              rawMaterialData.rawMaterialCountryName || "-",
            rawMaterialSourceOfProcurement:
              rawMaterialData.rawMaterialSourceOfProcurement || "-",
            rawMaterialImported: data.rawMaterialImported,
          },
        ],
      }));
      rawMaterialSetValue("rawMaterialName", "", { shouldValidate: false });
      rawMaterialSetValue("rawMaterialPercentageImported", "", {
        shouldValidate: false,
      });
      rawMaterialSetValue("rawMaterialCountryName", "", {
        shouldValidate: false,
      });
      rawMaterialSetValue("rawMaterialSourceOfProcurement", "", {
        shouldValidate: false,
      });
      // rawMaterialSetValue("rawMaterialName", "", { shouldValidate: false });

      setData((values) => ({
        ...values,
        rawMaterialName: "",
        rawMaterialPercentageImported: "",
        rawMaterialCountryName: "",
        rawMaterialSourceOfProcurement: "",
        rawMaterialImported: "0",
      }));
    } else {
      showToast("Please fill raw material details", "error");
    }
  };

  const partsTableColumn = [
    { field: "Name of part/components/sub-assembly/assembly" },
    { field: "Whether imported?" },
    { field: "Percentage(%) imported" },
    { field: "Name of country" },
    { field: "Source of Procurement" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Button
            sx={commonStyles.cursorPointer}
            onClick={() => {
              setDeleteItem({ targetName: row.targetName, index: row.id });
              setOpenDialog(true);
            }}
          >
            <IconDelete />
          </Button>
        );
      },
    },
  ];

  const addPartsHandler = (partsData) => {
    if (
      partsData.partsName &&
      // partsData.partsPercentageImported &&
      // partsData.partsCountryName &&
      // partsData.partsSourceOfProcurement &&
      data.partsImported
    ) {
      if (
        data.partsImported == 1 &&
        (!partsData?.partsPercentageImported ||
          !partsData?.partsCountryName ||
          !partsData?.partsSourceOfProcurement)
      ) {
        showToast("Please fill parts details", "error");
        return;
      }
      setData((values) => ({
        ...values,
        boughtout_parts: [
          ...values.boughtout_parts,
          {
            partsName: partsData.partsName,
            partsPercentageImported: partsData.partsPercentageImported || "-",
            partsCountryName: partsData.partsCountryName || "-",
            partsSourceOfProcurement: partsData.partsSourceOfProcurement || "-",
            partsImported: data.partsImported,
          },
        ],
      }));
      partsSetValue("partsName", "", { shouldValidate: false });
      partsSetValue("partsPercentageImported", "", { shouldValidate: false });
      partsSetValue("partsCountryName", "", { shouldValidate: false });
      partsSetValue("partsSourceOfProcurement", "", { shouldValidate: false });

      setData((values) => ({
        ...values,
        partsName: "",
        partsPercentageImported: "",
        partsCountryName: "",
        partsSourceOfProcurement: "",
        partsImported: "0",
      }));
    } else {
      showToast("Please fill parts details", "error");
    }
  };

  const testingTableColumn = [
    { field: "Name of the test" },
    { field: "Whether external?" },
    { field: "Testing agency name" },
    { field: "Testing agency address" },
    {
      field: "",
      renderColumn: (row) => {
        return (
          <Button
            sx={commonStyles.cursorPointer}
            onClick={() => {
              setDeleteItem({ targetName: row.targetName, index: row.id });
              setOpenDialog(true);
            }}
          >
            <IconDelete />
          </Button>
        );
      },
    },
  ];

  const addTestingHandler = (testingData) => {
    if (testingData.testName && data.externalTesting) {
      if (
        data.externalTesting === "1" &&
        (!testingData.agencyTestName || !testingData.agencyTestAddress)
      ) {
        showToast("Please fill testing agency details", "error");
      } else {
        let testObj = {
          testName: testingData.testName,
          externalTesting: data.externalTesting,
        };
        if (data.externalTesting == 1 && testingData.agencyTestName) {
          testObj["agencyTestName"] = testingData.agencyTestName;
        }
        if (data.externalTesting == 1 && testingData.agencyTestAddress) {
          testObj["agencyTestAddress"] = testingData.agencyTestAddress;
        }
        setData((values) => ({
          ...values,
          testing_method: [
            ...values.testing_method,
            testObj,
            // {
            //   testName: testingData.testName,
            //   externalTesting: data.externalTesting,
            //   agencyTestName:
            //     data.externalTesting === "1" ? data.agencyTestName : "",
            //   agencyTestAddress:
            //     data.externalTesting === "1" ? data.agencyTestAddress : "",
            // },
          ],
        }));
        testingSetValue("testName", "", { shouldValidate: false });
        testingSetValue("agencyTestName", "", { shouldValidate: false });
        testingSetValue("agencyTestAddress", "", { shouldValidate: false });

        setData((values) => ({
          ...values,
          testName: "",
          externalTesting: "0",
          agencyTestName: "",
          agencyTestAddress: "",
        }));
      }
    } else {
      showToast("Please fill testing details", "error");
    }
  };

  const onSubmit = () => {
    const { admin_manpower_count, tech_skilled_count, tech_unskill_count } =
      stepThreeData;
    if (!admin_manpower_count || !tech_skilled_count || !tech_unskill_count) {
      return;
    }
    if (!isDoneRef.current) {
      return;
    }

    isDoneRef.current = false;

    // if (!data.msme_representative_selfie) {
    //   showToast("Please upload organisation represntative selfie", "error");
    //   return;
    // }
    if (!data.site_exterior_picture) {
      showToast("Please upload site exterior picture", "error");
      return;
    }

    if (!data.testing_method) {
      showToast("Testing details are required", "error");
      return;
    }
    if (data.is_competent_banned == 1 && !data.competent_banned_details) {
      showToast("Competent banned details are required", "error");
      return;
    }
    if (
      data.is_enquiry_against_firm == 1 &&
      !data.enquiry_against_firm_detail
    ) {
      showToast("Enquiry against firm details are required", "error");
      return;
    }
    if (
      data.is_competent_banned == 1 &&
      data.competent_banned_details &&
      (data.competent_banned_details?.length < 5 ||
        data.competent_banned_details?.length > 55)
    ) {
      showToast(
        "Competent banned details must be 5 to 55 characters long",
        "error"
      );
      return;
    }
    if (
      data.is_enquiry_against_firm == 1 &&
      data.enquiry_against_firm_detail &&
      (data.enquiry_against_firm_detail?.length < 5 ||
        data.enquiry_against_firm_detail?.length > 55)
    ) {
      showToast(
        "Enquiry against firm details must be 5 to 55 characters long",
        "error"
      );
      return;
    }

    if (
      !!data.financial_capital_outlay_first_year?.length &&
      !!data.financial_capital_outlay_second_year?.length &&
      !!data.financial_capital_outlay_third_year?.length &&
      (data.financial_capital_outlay_first_year?.length < 5 ||
        data.financial_capital_outlay_second_year?.length < 5 ||
        data.financial_capital_outlay_third_year?.length < 5 ||
        data.financial_capital_outlay_first_year?.length > 25 ||
        data.financial_capital_outlay_second_year?.length > 25 ||
        data.financial_capital_outlay_third_year?.length > 25 ||
        (data.financial_capital_outlay_first_year &&
          !stepThreeData.financial_capital_outlay_first_year) ||
        (data.financial_capital_outlay_second_year &&
          !stepThreeData.financial_capital_outlay_second_year) ||
        (data.financial_capital_outlay_second_year &&
          !stepThreeData.financial_capital_outlay_third_year))
    ) {
      showToast(
        "All capital outlay details are required and must be 5 to 25 digits long",
        "error"
      );
      return;
    }
    if (
      !!data.turnover_first_year?.length &&
      !!data.turnover_second_year?.length &&
      !!data.turnover_third_year?.length &&
      (data.turnover_first_year?.length < 5 ||
        data.turnover_second_year?.length < 5 ||
        data.turnover_third_year?.length < 5 ||
        data.turnover_first_year?.length > 25 ||
        data.turnover_second_year?.length > 25 ||
        data.turnover_third_year?.length > 25 ||
        (data.turnover_first_year && !stepThreeData.turnover_first_year) ||
        (data.turnover_second_year && !stepThreeData.turnover_second_year) ||
        (data.turnover_third_year && !stepThreeData.turnover_third_year))
    ) {
      showToast(
        "All turonver details are required and must be 5 to 25 digits long",
        "error"
      );
      return;
    }
    if (
      !!data.profit_first_year?.length &&
      !!data.profit_second_year?.length &&
      !!data.profit_third_year?.length &&
      (data.profit_first_year?.length < 5 ||
        data.profit_second_year?.length < 5 ||
        data.profit_third_year?.length < 5 ||
        data.profit_first_year?.length > 25 ||
        data.profit_second_year?.length > 25 ||
        data.profit_third_year?.length > 25 ||
        (data.profit_first_year && !stepThreeData.profit_first_year) ||
        (data.profit_second_year && !stepThreeData.profit_second_year) ||
        (data.profit_third_year && !stepThreeData.profit_third_year))
    ) {
      showToast(
        "All gross profit details are required and must be 5 to 25 digits long",
        "error"
      );
      return;
    }

    if (
      !!data.acc_loss_first_year?.length &&
      !!data.acc_loss_second_year?.length &&
      !!data.acc_loss_third_year?.length &&
      (data.acc_loss_first_year?.length < 5 ||
        data.acc_loss_second_year?.length < 5 ||
        data.acc_loss_third_year?.length < 5 ||
        data.acc_loss_first_year?.length > 25 ||
        data.acc_loss_second_year?.length > 25 ||
        data.acc_loss_third_year?.length > 25 ||
        (data.acc_loss_first_year && !stepThreeData.acc_loss_first_year) ||
        (data.acc_loss_second_year && !stepThreeData.acc_loss_second_year) ||
        (data.acc_loss_third_year && !stepThreeData.acc_loss_third_year))
    ) {
      showToast(
        "All accumulated loss details are required and must be 5 to 25 digits long",
        "error"
      );
      return;
    }
    const formData = new FormData();
    // if (!applicationDetail?.ownership?.msme_representative_selfie) {
    //   formData.append(
    //     "msme_representative_selfie",
    //     data.msme_representative_selfie
    //   );
    // } else {
    //   const tempVal = data?.msme_representative_selfie?.name;
    //   if (
    //     tempVal !==
    //     applicationDetail?.ownership?.msme_representative_selfie
    //       ?.attachment_uuid
    //   ) {
    //     formData.append(
    //       "msme_representative_selfie",
    //       data.msme_representative_selfie
    //     );
    //   }
    // }
    if (!applicationDetail?.ownership?.site_exterior_picture) {
      formData.append("site_exterior_picture", data.site_exterior_picture);
    } else {
      const tempVal = data?.site_exterior_picture?.name;

      if (
        tempVal !==
        applicationDetail?.ownership?.site_exterior_picture?.attachment_uuid
      ) {
        formData.append("site_exterior_picture", data.site_exterior_picture);
      }
    }

    if (applicationDetail?.ownership?.product_document) {
      const tempVal = data?.product_document?.name;
      if (
        tempVal !==
        applicationDetail?.ownership?.product_document?.attachment_uuid
      ) {
        formData.append("product_document", data.product_document);
      }
    }
    if (
      !applicationDetail?.ownership?.product_document &&
      data.product_document
    ) {
      formData.append("product_document", data.product_document);
    }

    if (data.work_area_covered) {
      formData.append("work_area_covered", data.work_area_covered);
    } else {
      formData.append("work_area_covered", "");
    }
    formData.append("is_work_covered_leased", data.is_work_covered_leased);

    if (data.work_area_uncovered) {
      formData.append("work_area_uncovered", data.work_area_uncovered);
    } else {
      formData.append("work_area_uncovered", "");
    }
    formData.append("is_work_uncovered_leased", data.is_work_uncovered_leased);

    if (data.work_area_bond_rooms) {
      formData.append("work_area_bond_rooms", data.work_area_bond_rooms);
    } else {
      formData.append("work_area_bond_rooms", "");
    }
    formData.append("is_work_bond_leased", data.is_work_bond_leased);

    if (data.work_area_num_bond_rooms) {
      formData.append(
        "work_area_num_bond_rooms",
        data.work_area_num_bond_rooms
      );
    } else {
      formData.append("work_area_num_bond_rooms", "");
    }
    if (data.work_prod_area) {
      formData.append("work_prod_area", data.work_prod_area);
    } else {
      formData.append("work_prod_area", "");
    }
    formData.append("is_work_prod_leased", data.is_work_prod_leased);

    if (data.work_testing_area) {
      formData.append("work_testing_area", data.work_testing_area);
    } else {
      formData.append("work_testing_area", "");
    }
    formData.append("is_work_test_leased", data.is_work_test_leased);

    if (data.admin_manpower_count) {
      formData.append("admin_manpower_count", data.admin_manpower_count);
    } else {
      formData.append("admin_manpower_count", "");
    }

    if (data.admin_skill_set) {
      formData.append("admin_skill_set", data.admin_skill_set);
    } else {
      formData.append("admin_skill_set", "");
    }

    if (data.tech_skilled_count) {
      formData.append("tech_skilled_count", data.tech_skilled_count);
    } else {
      formData.append("tech_skilled_count", "");
    }

    if (data.tech_skill_set) {
      formData.append("tech_skill_set", data.tech_skill_set);
    } else {
      formData.append("tech_skill_set", "");
    }

    if (data.tech_unskill_count) {
      formData.append("tech_unskill_count", data.tech_unskill_count);
    } else {
      formData.append("tech_unskill_count", "");
    }

    if (data.tech_unskill_set) {
      formData.append("tech_unskill_set", data.tech_unskill_set);
    } else {
      formData.append("tech_unskill_set", "");
    }
    if (
      data.financial_capital_outlay_first_year &&
      data.financial_capital_outlay_second_year &&
      data.financial_capital_outlay_third_year
    ) {
      const finalData = [
        {
          financial_capital_outlay_first_year:
            data.financial_capital_outlay_first_year,
          financial_capital_outlay_second_year:
            data.financial_capital_outlay_second_year,
          financial_capital_outlay_third_year:
            data.financial_capital_outlay_third_year,
        },
      ];
      formData.append("financial_capital_outlay", JSON.stringify(finalData));
    } else if (
      (data.financial_capital_outlay_first_year ||
        data.financial_capital_outlay_second_year ||
        data.financial_capital_outlay_third_year) &&
      (!data.financial_capital_outlay_first_year ||
        !data.financial_capital_outlay_second_year ||
        !data.financial_capital_outlay_third_year)
    ) {
      showToast(
        "All capital outlay details are required and must be 5 to 25 digits long",
        "error"
      );
      return;
    } else {
      formData.append("financial_capital_outlay", "");
    }
    if (data.financial_source) {
      formData.append("financial_source", data.financial_source);
    } else {
      formData.append("financial_source", "");
    }
    if (
      data.turnover_first_year &&
      data.turnover_second_year &&
      data.turnover_third_year
    ) {
      const finalData = [
        {
          turnover_first_year: data.turnover_first_year,
          turnover_second_year: data.turnover_second_year,
          turnover_third_year: data.turnover_third_year,
        },
      ];
      formData.append("turnover_3_year", JSON.stringify(finalData));
    } else if (
      (data.turnover_first_year ||
        data.turnover_second_year ||
        data.turnover_third_year) &&
      (!data.turnover_first_year ||
        !data.turnover_second_year ||
        !data.turnover_third_year)
    ) {
      showToast(
        "All turonver details are required and must be 5 to 25 digits long",
        "error"
      );
      return;
    } else {
      formData.append("turnover_3_year", "");
    }
    if (
      data.profit_first_year &&
      data.profit_second_year &&
      data.profit_third_year
    ) {
      const finalData = [
        {
          profit_first_year: data.profit_first_year,
          profit_second_year: data.profit_second_year,
          profit_third_year: data.profit_third_year,
        },
      ];
      // formData.append("profit_3_year", data.profit_3_year);
      formData.append("profit_3_year", JSON.stringify(finalData));
    } else if (
      (data.profit_first_year ||
        data.profit_second_year ||
        data.profit_third_year) &&
      (!data.profit_first_year ||
        !data.profit_second_year ||
        !data.profit_third_year)
    ) {
      showToast(
        "All gross profit details are required and must be 5 to 25 digits long",
        "error"
      );
      return;
    } else {
      formData.append("profit_3_year", "");
    }
    if (
      data.acc_loss_first_year &&
      data.acc_loss_second_year &&
      data.acc_loss_third_year
    ) {
      const finalData = [
        {
          acc_loss_first_year: data.acc_loss_first_year,
          acc_loss_second_year: data.acc_loss_second_year,
          acc_loss_third_year: data.acc_loss_third_year,
        },
      ];
      // formData.append("acc_loss_3_year", data.acc_loss_3_year);
      formData.append("acc_loss_3_year", JSON.stringify(finalData));
    } else if (
      (data.acc_loss_first_year ||
        data.acc_loss_second_year ||
        data.acc_loss_third_year) &&
      (!data.acc_loss_first_year ||
        !data.acc_loss_second_year ||
        !data.acc_loss_third_year)
    ) {
      showToast(
        "All accumulated loss details are required and must be 5 to 25 digits long",
        "error"
      );
      return;
    } else {
      formData.append("acc_loss_3_year", "");
    }

    formData.append(
      "financial_audit_conducted",
      data.financial_audit_conducted
    );

    if (data.product_manufactured?.length > 0) {
      formData.append(
        "product_manufactured",
        JSON.stringify(data.product_manufactured)
      );
    } else if (
      applicationDetail?.ownership?.product_manufactured &&
      data.product_manufactured?.length === 0
    ) {
      formData.append("product_manufactured", "");
    }

    if (data.key_domestic_customers?.length > 0) {
      formData.append(
        "key_domestic_customers",
        JSON.stringify(data.key_domestic_customers)
      );
    } else if (
      applicationDetail?.ownership?.key_domestic_customers &&
      data.key_domestic_customers?.length === 0
    ) {
      formData.append("key_domestic_customers", "");
    }

    if (data.key_raw_materials?.length > 0) {
      formData.append(
        "key_raw_materials",
        JSON.stringify(data.key_raw_materials)
      );
    } else if (
      applicationDetail?.ownership?.key_raw_materials &&
      data.key_raw_materials?.length === 0
    ) {
      formData.append("key_raw_materials", "");
    }

    if (data.boughtout_parts?.length > 0) {
      formData.append("boughtout_parts", JSON.stringify(data.boughtout_parts));
    } else if (
      applicationDetail?.ownership?.boughtout_parts &&
      data.boughtout_parts?.length === 0
    ) {
      formData.append("boughtout_parts", "");
    }

    if (data.key_vendors?.length > 0) {
      formData.append("key_vendors", JSON.stringify(data.key_vendors));
    } else {
      if (
        applicationDetail?.ownership?.key_vendors &&
        data?.key_vendors?.length === 0
      ) {
        formData.append("key_vendors", "");
      }
    }

    if (data.testing_method?.length > 0) {
      formData.append("testing_method", JSON.stringify(data.testing_method));
    } else {
      if (
        applicationDetail?.ownership?.testing_method &&
        data?.testing_method?.length === 0
      ) {
        formData.append("testing_method", "");
      }
    }
    if (data.acceptance_quality_level) {
      formData.append(
        "acceptance_quality_level",
        data.acceptance_quality_level
      );
    } else {
      formData.append("acceptance_quality_level", "");
    }
    // if (data.is_competent_banned) {
    formData.append("is_competent_banned", data.is_competent_banned);
    // }
    if (data.competent_banned_details) {
      formData.append(
        "competent_banned_details",
        data.competent_banned_details
      );
    } else {
      formData.append("competent_banned_details", "");
    }
    // if (data.is_enquiry_against_firm) {
    formData.append("is_enquiry_against_firm", data.is_enquiry_against_firm);
    // }
    if (data.enquiry_against_firm_detail) {
      formData.append(
        "enquiry_against_firm_detail",
        data.enquiry_against_firm_detail
      );
    } else {
      formData.append("enquiry_against_firm_detail", "");
    }

    if (keyProcesses?.length > 0) {
      formData.append("key_processes", keyProcesses);
    } else if (
      applicationDetail?.ownership?.key_processes &&
      keyProcesses?.length === 0
    ) {
      formData.append("key_processes", "");
    }

    if (outsourcedProcess?.length > 0) {
      formData.append("processes_outsourced", outsourcedProcess);
    } else if (
      applicationDetail?.ownership?.processes_outsourced &&
      outsourcedProcess?.length === 0
    ) {
      formData.append("processes_outsourced", "");
    }

    if (energyResources?.length > 0) {
      formData.append("energy_resources_used", energyResources);
    } else if (
      applicationDetail?.ownership?.energy_resources_used &&
      energyResources?.length === 0
    ) {
      formData.append("energy_resources_used", "");
    }

    if (naturalResources?.length > 0) {
      formData.append("natural_resources_used", naturalResources);
    } else if (
      applicationDetail?.ownership?.natural_resources_used &&
      naturalResources?.length === 0
    ) {
      formData.append("natural_resources_used", "");
    }
    if (envConsents?.length > 0) {
      formData.append("environment_consents_name", envConsents);
    } else if (
      applicationDetail?.ownership?.environment_consents_name &&
      envConsents?.length === 0
    ) {
      formData.append("environment_consents_name", "");
    }
    // formData.append("proof_attached[]", data.proof_attached);
    formData.append("third_party_cert", thirdPartyCertChip);
    if (newDocArray?.length > 0) {
      newDocArray?.forEach((file) => {
        if (file?.type) formData.append("proof_attached[]", file);
      });
    }
    // if (newProductDocArray?.length > 0) {
    //   newProductDocArray?.forEach((file) => {
    //     formData.append("product_document[]", file);
    //   });
    // }
    // } else {
    //   formData.append("proof_attached[]", "");
    // }

    dispatch(
      saveStep3FormAction(
        applicationDetail?.application_uuid,
        formData,
        navigate,
        dispatch
      )
    );
  };

  const nextHandler = () => {
    setIsSubmit(true);
    isDoneRef.current = true;
  };

  return (
    <QpFormContainer activeStep={2} changeNavigate={`/step2-details/${id}`}>
      {/* <CustomImageUploader
        file={data.msme_representative_selfie}
        value={data.msme_representative_selfie}
        setValue={setData}
        displayText="1. Organisational Representative Selfie"
        isLabel={true}
        name="msme_representative_selfie"
        typeText="Type: .jpg,.jpeg,.png, Max Size:5MB "
        accept=".jpg,.jpeg,.png"
        required={true}
        docType="image"
      /> */}
      <CustomImageUploader
        file={data.site_exterior_picture}
        value={data.site_exterior_picture}
        setValue={setData}
        displayText="1. Site Exterior Picture"
        typeText="Type: .jpg,.jpeg,.png, Max Size:5MB "
        isLabel={true}
        name="site_exterior_picture"
        accept=".jpg,.jpeg,.png"
        required={true}
        docType="image"
      />

      <Box>
        <QpTypography
          displayText="2. Area of Factory/Works is on lease or owned"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        {/* <Box sx={commonStyles.displayStyle}> */}
        {/* <QpTypography
            displayText="a) Covered Area"
            styleData={styles.labelStyle}
          /> */}
        <CustomInput
          label="a) Covered Area- Whether owned or Leased?"
          // inputstyleData={{
          //   ...styles.inputBaseStyle,
          // }}
          labelStyleData={{
            ...styles.subHeadingLabel,
            ...styles.subHeadingLabelWidth,
          }}
          placeholder="Enter covered area"
          handleRadioButton={handleRadioButton}
          isOption={true}
          optionName="is_work_covered_leased"
          optionValue={data.is_work_covered_leased}
          optionStyleData={{ marginBottom: "0.438rem" }}
          name="work_area_covered"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.work_area_covered}
          defaultValue={data.work_area_covered}
          registerName="work_area_covered"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("work_area_covered", data?.work_area_covered)
          }
          endAdornment={
            <InputAdornment position="end">{"meter" + "\u00B2"}</InputAdornment>
          }
        />
        {/* </Box> */}
        {/* <Box sx={commonStyles.displayStyle}>
          <QpTypography
            displayText="b) Uncovered"
            styleData={styles.labelStyle}
          /> */}
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          labelStyleData={{
            ...styles.subHeadingLabel,
            ...styles.subHeadingLabelWidth,
          }}
          label="b) Uncovered Area(if any)- Whether owned or Leased?"
          placeholder="Enter uncovered area"
          name="work_area_uncovered"
          handleRadioButton={handleRadioButton}
          isOption={true}
          optionName="is_work_uncovered_leased"
          optionValue={data.is_work_uncovered_leased}
          optionStyleData={{ marginBottom: "0.438rem" }}
          setValueData={setData}
          onChange={handleInputChange}
          value={data.work_area_uncovered}
          defaultValue={data.work_area_uncovered}
          registerName="work_area_uncovered"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("work_area_uncovered", data?.work_area_uncovered)
          }
          endAdornment={
            <InputAdornment position="end">{"meter" + "\u00B2"}</InputAdornment>
          }
        />
        {/* </Box> */}
        {/* <Box sx={commonStyles.displayStyle}>
          <QpTypography
            displayText="c) Bond Rooms"
            styleData={styles.labelStyle}
          /> */}
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          label="c) Bond Rooms- Whether owned or Leased? "
          labelStyleData={{
            ...styles.subHeadingLabel,
            ...styles.subHeadingLabelWidth,
          }}
          handleRadioButton={handleRadioButton}
          placeholder="Enter bond rooms area"
          isOption={true}
          optionName="is_work_bond_leased"
          optionValue={data.is_work_bond_leased}
          optionStyleData={{ marginBottom: "0.438rem" }}
          name="work_area_bond_rooms"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.work_area_bond_rooms}
          defaultValue={data.work_area_bond_rooms}
          registerName="work_area_bond_rooms"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("work_area_bond_rooms", data?.work_area_bond_rooms)
          }
          endAdornment={
            <InputAdornment position="end">{"meter" + "\u00B2"}</InputAdornment>
          }
        />
        {/* </Box> */}
        {/* <Box sx={commonStyles.displayStyle}>
          <QpTypography
            displayText="d) No. of Bond Rooms"
            styleData={styles.labelStyle}
          /> */}
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          label="d) No. of Bond Rooms"
          labelStyleData={{
            ...styles.subHeadingLabel,
            ...styles.subHeadingLabelWidth,
          }}
          placeholder="Enter number of bond rooms"
          name="work_area_num_bond_rooms"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.work_area_num_bond_rooms}
          defaultValue={data.work_area_num_bond_rooms}
          registerName="work_area_num_bond_rooms"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("work_area_num_bond_rooms", data?.work_area_num_bond_rooms)
          }
          // endAdornment={
          //   <InputAdornment position="end">{"meter" + "\u00B2"}</InputAdornment>
          // }
        />
        {/* </Box>
        <Box sx={commonStyles.displayStyle}>
          <QpTypography
            displayText="e) Production Area"
            styleData={styles.labelStyle}
          /> */}
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          label="e) Production Area- Whether owned or Leased?"
          labelStyleData={{
            ...styles.subHeadingLabel,
            ...styles.subHeadingLabelWidth,
          }}
          placeholder="Enter production area"
          name="work_prod_area"
          handleRadioButton={handleRadioButton}
          isOption={true}
          optionName="is_work_prod_leased"
          optionValue={data.is_work_prod_leased}
          optionStyleData={{ marginBottom: "0.438rem" }}
          setValueData={setData}
          onChange={handleInputChange}
          value={data.work_prod_area}
          defaultValue={data.work_prod_area}
          registerName="work_prod_area"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) => getData("work_prod_area", data?.work_prod_area)}
          endAdornment={
            <InputAdornment position="end">{"meter" + "\u00B2"}</InputAdornment>
          }
        />
        {/* </Box>
        <Box sx={commonStyles.displayStyle}>
          <QpTypography
            displayText="f) Testing Area"
            styleData={styles.labelStyle}
          /> */}
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          label="f) Testing Area(if any)- Whether owned or Leased?"
          labelStyleData={{
            ...styles.subHeadingLabel,
            ...styles.subHeadingLabelWidth,
          }}
          placeholder="Enter testing area"
          name="work_testing_area"
          handleRadioButton={handleRadioButton}
          isOption={true}
          optionName="is_work_test_leased"
          optionValue={data.is_work_test_leased}
          optionStyleData={{ marginBottom: "0.438rem" }}
          setValueData={setData}
          onChange={handleInputChange}
          value={data.work_testing_area}
          defaultValue={data.work_testing_area}
          registerName="work_testing_area"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("work_testing_area", data?.work_testing_area)
          }
          endAdornment={
            <InputAdornment position="end">{"meter" + "\u00B2"}</InputAdornment>
          }
        />
        {/* </Box> */}
        {/* <Box sx={commonStyles.displayStyle}>
          <CustomMultipleImageUploader
            displayText="Note - Attach proof of ownership and detailed site plan of layout of premises clearly depicting various areas e.g. production area, location of plant/Machinery stores, bond rooms, inspection area etc"
            file={data.proof_attached}
            value={data.proof_attached}
            setValue={setData}
            styleData={{ ...styles.labelStyle, ...styles.noteStyle }}
            // isLabel={true}
            name="proof_attached"
            accept=".jpg,.jpeg,.png"
            setNewDocArray={setNewDocArray}
            docType="image"
          />
        </Box> */}
      </Box>
      <Box>
        <QpTypography
          displayText="3. Details of Manpower Employed"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <Box
          sx={{
            ...commonStyles.displayStyle,
            ...styles.adminSkillContainer,
          }}
        >
          <QpTypography displayText="a) Admin" />
          <Box sx={commonStyles.fullWidth}>
            {/* <Box sx={commonStyles.displayStyle}>
              <QpTypography displayText="Number of employees" /> */}
            <CustomInput
              // styleData={{
              //   ...styles.inputBaseStyle,
              // }}
              label="Number of persons employed"
              labelStyleData={styles.subHeadingLabel}
              name="admin_manpower_count"
              setValueData={setData}
              onChange={handleInputChange}
              value={data.admin_manpower_count}
              defaultValue={data.admin_manpower_count}
              registerName="admin_manpower_count"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("admin_manpower_count", data?.admin_manpower_count)
              }
            />
            {/* </Box>
            <Box sx={commonStyles.displayStyle}>
              <QpTypography displayText="Skill set" /> */}
            {/* <CustomInput
              // styleData={{
              //   ...styles.inputBaseStyle,
              // }}
              label="Skill set"
              labelStyleData={styles.subHeadingLabel}
              name="admin_skill_set"
              setValueData={setData}
              onChange={handleInputChange}
              value={data.admin_skill_set}
              defaultValue={data.admin_skill_set}
              registerName="admin_skill_set"
              // isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("admin_skill_set", data?.admin_skill_set)
              }
            /> */}
            {/* </Box>*/}
          </Box>
        </Box>
      </Box>
      <Box>
        <QpTypography displayText="b) Technical" />
        <Box sx={styles.skilledContainer}>
          <QpTypography displayText="b. I) Skilled" />
          <Box sx={commonStyles.fullWidth}>
            {/* <Box sx={commonStyles.displayStyle}>
              <QpTypography displayText="Number of employees" /> */}
            <CustomInput
              // styleData={{
              //   ...styles.inputBaseStyle,
              // }}
              label="Number of persons employed"
              labelStyleData={styles.subHeadingLabel}
              name="tech_skilled_count"
              setValueData={setData}
              onChange={handleInputChange}
              value={data.tech_skilled_count}
              defaultValue={data.tech_skilled_count}
              registerName="tech_skilled_count"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("tech_skilled_count", data?.tech_skilled_count)
              }
            />
            {/* </Box>
            <Box sx={commonStyles.displayStyle}>
              <QpTypography displayText="Skill set" /> */}
            {/* <CustomInput
              // styleData={{
              //   ...styles.inputBaseStyle,
              // }}
              label="Skill set"
              labelStyleData={styles.subHeadingLabel}
              name="tech_skill_set"
              setValueData={setData}
              onChange={handleInputChange}
              value={data.tech_skill_set}
              defaultValue={data.tech_skill_set}
              registerName="tech_skill_set"
              // isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("tech_skill_set", data?.tech_skill_set)
              }
            /> */}
            {/* </Box> */}
          </Box>
        </Box>
        <Box sx={styles.skilledContainer}>
          <QpTypography displayText="b. II) Unskilled" />
          <Box sx={commonStyles.fullWidth}>
            {/* <Box sx={commonStyles.displayStyle}>
              <QpTypography displayText="Number of employees" /> */}
            <CustomInput
              // styleData={{
              //   ...styles.inputBaseStyle,
              // }}
              label="Number of persons employed"
              labelStyleData={styles.subHeadingLabel}
              name="tech_unskill_count"
              setValueData={setData}
              onChange={handleInputChange}
              value={data.tech_unskill_count}
              defaultValue={data.tech_unskill_count}
              registerName="tech_unskill_count"
              isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("tech_unskill_count", data?.tech_unskill_count)
              }
            />
            {/* </Box>
            <Box sx={commonStyles.displayStyle}>
              <QpTypography displayText="Skill set" /> */}
            {/* <CustomInput
              // styleData={{
              //   ...styles.inputBaseStyle,
              // }}
              label="Skill set"
              labelStyleData={styles.subHeadingLabel}
              name="tech_unskill_set"
              setValueData={setData}
              onChange={handleInputChange}
              value={data.tech_unskill_set}
              defaultValue={data.tech_unskill_set}
              registerName="tech_unskill_set"
              // isRequired={true}
              submit={isSubmit}
              setData={(data) =>
                getData("tech_unskill_set", data?.tech_unskill_set)
              }
            /> */}
            {/* </Box> */}
          </Box>
        </Box>
      </Box>

      <Box>
        <QpTypography
          displayText="4. Financial Details"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        {/* <Box sx={commonStyles.displayStyle}>
          <QpTypography displayText="a) Capital outlay" /> */}
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          label="a) Capital outlay(3 years)"
          labelStyleData={styles.subHeadingLabel}
          name="financial_capital_outlay_first_year"
          placeholder="Current Financial Year "
          setValueData={setData}
          onChange={handleInputChange}
          value={data.financial_capital_outlay_first_year}
          defaultValue={data.financial_capital_outlay_first_year}
          registerName="financial_capital_outlay_first_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData(
              "financial_capital_outlay_first_year",
              data?.financial_capital_outlay_first_year
            )
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          // label="a) Capital outlay(3 years)"
          labelStyleData={styles.subHeadingLabel}
          name="financial_capital_outlay_second_year"
          placeholder="Financial Year 1 (Immediate last year)"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.financial_capital_outlay_second_year}
          defaultValue={data.financial_capital_outlay_second_year}
          registerName="financial_capital_outlay_second_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData(
              "financial_capital_outlay_second_year",
              data?.financial_capital_outlay_second_year
            )
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          // label="a) Capital outlay(3 years)"
          labelStyleData={styles.subHeadingLabel}
          name="financial_capital_outlay_third_year"
          placeholder="Financial year 2"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.financial_capital_outlay_third_year}
          defaultValue={data.financial_capital_outlay_third_year}
          registerName="financial_capital_outlay_third_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData(
              "financial_capital_outlay_third_year",
              data?.financial_capital_outlay_third_year
            )
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        {/* </Box>
        <Box sx={commonStyles.displayStyle}>
          <QpTypography displayText="b) Source of finance with borrowing limits & Bank Guarantee" /> */}
        {/* <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          label="b) Source of finance with borrowing limits & Bank Guarantee"
          labelStyleData={styles.subHeadingLabel}
          name="financial_source"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.financial_source}
          defaultValue={data.financial_source}
          registerName="financial_source"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("financial_source", data?.financial_source)
          }
        /> */}
        {/* </Box>
        <Box sx={commonStyles.displayStyle}>
          <QpTypography displayText="c) 3 years Turnover" /> */}
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          label="b) Turnover(Last 3 years)"
          labelStyleData={styles.subHeadingLabel}
          name="turnover_first_year"
          placeholder="Financial Year 1 (Immediate last year)"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.turnover_first_year}
          defaultValue={data.turnover_first_year}
          registerName="turnover_first_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("turnover_first_year", data?.turnover_first_year)
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          // label="c) 3 years Turnover"
          labelStyleData={styles.subHeadingLabel}
          name="turnover_second_year"
          placeholder="Financial year 2"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.turnover_second_year}
          defaultValue={data.turnover_second_year}
          registerName="turnover_second_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("turnover_second_year", data?.turnover_second_year)
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          // label="c) 3 years Turnover"
          labelStyleData={styles.subHeadingLabel}
          placeholder="Third year turonver"
          name="Financial year 3"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.turnover_third_year}
          defaultValue={data.turnover_third_year}
          registerName="turnover_third_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("turnover_third_year", data?.turnover_third_year)
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        {/* </Box>
        <Box sx={commonStyles.displayStyle}>
          <QpTypography displayText="d) 3 years Gross Profit" /> */}
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          label="c) Gross Profit(Last 3 years)"
          labelStyleData={styles.subHeadingLabel}
          placeholder="Financial Year 1 (Immediate last year)"
          name="profit_first_year"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.profit_first_year}
          defaultValue={data.profit_first_year}
          registerName="profit_first_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("profit_first_year", data?.profit_first_year)
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          // label="d) 3 years Gross Profit"
          labelStyleData={styles.subHeadingLabel}
          placeholder="Financial year 2"
          name="profit_second_year"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.profit_second_year}
          defaultValue={data.profit_second_year}
          registerName="profit_second_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("profit_second_year", data?.profit_second_year)
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          // label="d) 3 years Gross Profit"
          labelStyleData={styles.subHeadingLabel}
          placeholder="Financial year 3"
          name="profit_third_year"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.profit_third_year}
          defaultValue={data.profit_third_year}
          registerName="profit_third_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("profit_third_year", data?.profit_third_year)
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        {/* </Box>
        <Box sx={commonStyles.displayStyle}>
          <QpTypography displayText="e) Accumulated Losses if any (Last 3 years)" /> */}
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          label="d) Accumulated Losses if any (Last 3 years)"
          placeholder="Financial Year 1 (Immediate last year)"
          labelStyleData={styles.subHeadingLabel}
          name="acc_loss_first_year"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.acc_loss_first_year}
          defaultValue={data.acc_loss_first_year}
          registerName="acc_loss_first_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("acc_loss_first_year", data?.acc_loss_first_year)
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          // label="e) Accumulated Losses if any (Last 3 years)"
          labelStyleData={styles.subHeadingLabel}
          placeholder="Financial year 2"
          name="acc_loss_second_year"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.acc_loss_second_year}
          defaultValue={data.acc_loss_second_year}
          registerName="acc_loss_second_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("acc_loss_second_year", data?.acc_loss_second_year)
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        <CustomInput
          // styleData={{
          //   ...styles.inputBaseStyle,
          // }}
          // label="e) Accumulated Losses if any (Last 3 years)"
          labelStyleData={styles.subHeadingLabel}
          placeholder="Financial year 3"
          name="acc_loss_third_year"
          setValueData={setData}
          onChange={handleInputChange}
          value={data.acc_loss_third_year}
          defaultValue={data.acc_loss_third_year}
          registerName="acc_loss_third_year"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("acc_loss_third_year", data?.acc_loss_third_year)
          }
          startAdornment={<InputAdornment position="start">INR</InputAdornment>}
        />
        {/* </Box>
        <Box sx={commonStyles.displayStyle}>
          <QpTypography displayText="f) Whether 3rd Party Financial audits are conducted?" /> */}
        <CustomRadioButton
          displayText="e) Whether 3rd Party Financial audits are conducted?"
          labelStyleData={styles.subHeadingLabel}
          options={yesNo}
          name="financial_audit_conducted"
          setValueData={setData}
          defaultValue={
            data.financial_audit_conducted && +data.financial_audit_conducted
          }
          registerName="financial_audit_conducted"
          submit={isSubmit}
          setData={(data) =>
            getData(
              "financial_audit_conducted",
              data?.financial_audit_conducted
            )
          }
          // isRequired={true}
          // onChange={handleInputChange}
          // value={data.financial_audit_conducted}
        />

        {/* </Box> */}
      </Box>

      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="5. Third Party Certifications"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpInputBase
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          onKeyPress={onEnterKeyPress}
          placeholder="Type name and hit enter"
        />
        {thirdPartyCertChip?.map((chip, index) => {
          return (
            <Chip
              color="info"
              size="small"
              key={index}
              index={index}
              onDelete={() => handleThirdPartCertDelete(index)}
              label={chip}
              sx={styles.chipContainer}
            />
          );
        })}
      </Box>
      {/* <Box sx={styles.productContainer}>
        <Typography sx={styles.productHeading}>Product/Articles</Typography>
      </Box> */}
      <Box>
        <QpTypography
          displayText="6. Products/Articles"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpTypography
          displayText="6.1 Name of the product"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="productName"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("productName")}
          error={productErrors.productName ? true : false}
          // value={data.productName}
          // defaultValue={data.productName}
          // registerName="productName"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("productName", data?.productName)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.productName?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="6.2 Total production capacity per year (as on date of application for assessment) "
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="totalProductionCapacityPerYear"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("totalProductionCapacityPerYear")}
          error={productErrors.totalProductionCapacityPerYear ? true : false}
          // value={data.totalProductionCapacityPerYear}
          // defaultValue={data.totalProductionCapacityPerYear}
          // registerName="totalProductionCapacityPerYear"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData(
          //     "totalProductionCapacityPerYear",
          //     data?.totalProductionCapacityPerYear
          //   )
          // }
          // startAdornment={<InputAdornment>INR</InputAdornment>}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.totalProductionCapacityPerYear?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="6.3 Present production capacity per year (as on date of application for assessment ) "
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="presentProductionCapacityPerYear"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("presentProductionCapacityPerYear")}
          error={productErrors.presentProductionCapacityPerYear ? true : false}
          // value={data.presentProductionCapacityPerYear}
          // defaultValue={data.presentProductionCapacityPerYear}
          // registerName="presentProductionCapacityPerYear"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData(
          //     "presentProductionCapacityPerYear",
          //     data?.presentProductionCapacityPerYear
          //   )
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.presentProductionCapacityPerYear?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="6.4 Total production capacity per month (as on date of application for assessment)  "
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="totalProductionCapacityPerMonth"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("totalProductionCapacityPerMonth")}
          error={productErrors.totalProductionCapacityPerMonth ? true : false}
          // value={data.totalProductionCapacityPerMonth}
          // defaultValue={data.totalProductionCapacityPerMonth}
          // registerName="totalProductionCapacityPerMonth"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData(
          //     "totalProductionCapacityPerMonth",
          //     data?.totalProductionCapacityPerMonth
          //   )
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.totalProductionCapacityPerMonth?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="6.5 Present production capacity per month (as on date of application for assessment)  "
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="presentProductionCapacityPerMonth"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("presentProductionCapacityPerMonth")}
          error={productErrors.presentProductionCapacityPerMonth ? true : false}
          // value={data.presentProductionCapacityPerMonth}
          // defaultValue={data.presentProductionCapacityPerMonth}
          // registerName="presentProductionCapacityPerMonth"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData(
          //     "presentProductionCapacityPerMonth",
          //     data?.presentProductionCapacityPerMonth
          //   )
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.presentProductionCapacityPerMonth?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="6.6 Spare Capacity available (per month)  "
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="spareCapacity"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("spareCapacity")}
          error={productErrors.spareCapacity ? true : false}
          // value={data.spareCapacity}
          // defaultValue={data.spareCapacity}
          // registerName="spareCapacity"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("spareCapacity", data?.spareCapacity)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.spareCapacity?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="6.7 Date of Commencement of Manufacturing of the product"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          type="date"
          name="dateOfCommencement"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("dateOfCommencement")}
          error={productErrors.dateOfCommencement ? true : false}
          inputProps={{
            max: moment(new Date().setDate(new Date().getDate() - 1)).format(
              "YYYY-MM-DD"
            ),
          }}
          // value={data.dateOfCommencement}
          // defaultValue={data.dateOfCommencement}
          // registerName="dateOfCommencement"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData("dateOfCommencement", data?.dateOfCommencement)
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.dateOfCommencement?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="6.8 Cost Audit (report) Rules, 1968  "
          styleData={styles.labelStyle}
        />
        <QpRadioButton
          options={yesNo}
          name="isCostAudit"
          onChange={handleRadioButton}
          value={data.isCostAudit}
        />
        {/* <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="costAudit"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("costAudit")}
          error={productErrors.costAudit ? true : false}
          // value={data.costAudit}
          // defaultValue={data.costAudit}
          // registerName="costAudit"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("costAudit", data?.costAudit)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.costAudit?.message}
        /> */}
      </Box>
      {data.isCostAudit === "1" && (
        <Box>
          <QpTypography
            displayText="Details of cost audit rules  "
            styleData={styles.labelStyle}
          />
          <InputBase
            sx={{
              ...styles.inputBaseStyle,
              ...commonStyles.fullWidth,
            }}
            name="costAudit"
            onChange={handleInputChange}
          />
          {/* <QpTypography
            styleData={commonStyles.errorText}
            displayText={productErrors.licnenseDetail?.message}
          /> */}
        </Box>
      )}
      <Box>
        <QpTypography
          displayText="6.9 Fire Safety or Explosive Regulations   "
          styleData={styles.labelStyle}
        />
        <QpRadioButton
          options={yesNo}
          name="fireSafety"
          onChange={handleRadioButton}
          value={data.fireSafety}
        />
      </Box>
      {data.fireSafety === "1" && (
        <Box>
          <QpTypography
            displayText="Details of license/compliance  "
            styleData={styles.labelStyle}
          />
          <InputBase
            sx={{
              ...styles.inputBaseStyle,
              ...commonStyles.fullWidth,
            }}
            name="licnenseDetail"
            onChange={handleInputChange}
          />
          {/* <QpTypography
            styleData={commonStyles.errorText}
            displayText={productErrors.licnenseDetail?.message}
          /> */}
        </Box>
      )}
      <Box>
        <QpTypography
          displayText="6.10 Other Government Regulatory norms  "
          styleData={styles.labelStyle}
        />
        <QpRadioButton
          options={yesNo}
          name="isGovernmentRegulatory"
          onChange={handleRadioButton}
          value={data.isGovernmentRegulatory}
        />
        {/* <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="governmentRegulatory"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("governmentRegulatory")}
          error={productErrors.governmentRegulatory ? true : false}
          // value={data.governmentRegulatory}
          // defaultValue={data.governmentRegulatory}
          // registerName="governmentRegulatory"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData("governmentRegulatory", data?.governmentRegulatory)
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.governmentRegulatory?.message}
        /> */}
      </Box>
      {data.isGovernmentRegulatory === "1" && (
        <Box>
          <QpTypography
            displayText="Details of other government regulatory norms  "
            styleData={styles.labelStyle}
          />
          <InputBase
            sx={{
              ...styles.inputBaseStyle,
              ...commonStyles.fullWidth,
            }}
            name="governmentRegulatory"
            onChange={handleInputChange}
          />
          {/* <QpTypography
            styleData={commonStyles.errorText}
            displayText={productErrors.licnenseDetail?.message}
          /> */}
        </Box>
      )}
      <Box>
        <QpTypography
          displayText="6.11 Defect Level/Acceptance Quality Level of the final product  "
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="defectLevel"
          // setValueData={setData}
          onChange={handleInputChange}
          {...productRegister("defectLevel")}
          error={productErrors.defectLevel ? true : false}
          // value={data.defectLevel}
          // defaultValue={data.defectLevel}
          // registerName="defectLevel"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("defectLevel", data?.defectLevel)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={productErrors.defectLevel?.message}
        />
      </Box>

      <QpButton
        displayText="Add Product"
        // onClick={() => addProductHandler("product_manufactured")}
        onClick={handleProductSubmit(addProductHandler)}
        styleData={{ ...commonStyles.blueButton, ...styles.addButtonStyle }}
        textStyle={{
          ...commonStyles.blueButtonText,
          ...styles.addButtontextStyle,
        }}
      />
      {productTableRow?.length !== 0 && (
        <CustomTable
          columnDefs={productTableColumn}
          rowData={productTableRow}
          styleData={commonStyles.tableStyleData}
          boxStyle={{
            ...commonStyles.tableBoxStyleData,
            ...customCommonStyles.marginBottomOne,
          }}
        />
      )}

      <CustomImageUploader
        file={data.product_document}
        value={data.product_document}
        setValue={setData}
        displayText="7. List of products manufactured at the unit"
        isLabel={true}
        name="product_document"
        typeText="Type: .jpg,.jpeg,.png, Max Size:5MB "
        accept=".jpg,.jpeg,.png"
        required={false}
        docType="image"
      />
      <Box>
        <QpTypography
          displayText="8. Key Customers (e.g. OEMs/Large Industry/Tier -1/Open Market; International/Domestic) with address"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpTypography
          displayText="8.1 Name of the Customer"
          styleData={styles.labelStyle}
        />
        {/* <CustomInput
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="customerName"
          setValueData={setData}
          onChange={handleInputChange}
          value={data?.customerName}
          defaultValue={data.customerName}
          registerName="customerName"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) => getData("customerName", data?.customerName)}
        /> */}
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="customerName"
          // setValueData={setData}
          onChange={handleInputChange}
          // value={data?.customerName}
          // defaultValue={data.customerName}
          // registerName="customerName"
          {...customerRegister("customerName")}
          error={customerErrors.customerName ? true : false}
          // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("customerName", data?.customerName)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={customerErrors.customerName?.message}
        />
      </Box>
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="8.2 Whether domestic or international?"
          styleData={styles.labelStyle}
        />
        <QpRadioButton
          options={customerType}
          name="customerType"
          onChange={handleRadioButton}
          value={data.customerType}
        />
      </Box>
      <Box>
        <QpTypography displayText="8.3 Address" styleData={styles.labelStyle} />
        {/* <CustomInput
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="customerAddress"
          setValueData={setData}
          value={data?.customerAddress}
          onChange={handleInputChange}
          defaultValue={data.customerAddress}
          registerName="customerAddress"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) => getData("customerAddress", data?.customerAddress)}
        /> */}
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="customerAddress"
          // setValueData={setData}
          // value={data?.customerAddress}
          onChange={handleInputChange}
          // defaultValue={data.customerAddress}
          {...customerRegister("customerAddress")}
          error={customerErrors.customerAddress ? true : false}
          // registerName="customerAddress"
          // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("customerAddress", data?.customerAddress)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={customerErrors.customerAddress?.message}
        />
      </Box>
      <QpButton
        displayText="Add Customer"
        // onClick={() => addCustomerHandler("key_domestic_customers")}
        onClick={handleCustomerSubmit(addCustomerHandler)}
        styleData={{ ...commonStyles.blueButton, ...styles.addButtonStyle }}
        textStyle={{
          ...commonStyles.blueButtonText,
          ...styles.addButtontextStyle,
        }}
      />
      {customerTableRow?.length !== 0 && (
        <CustomTable
          columnDefs={customerTableColumn}
          rowData={customerTableRow}
          styleData={commonStyles.tableStyleData}
          boxStyle={{
            ...commonStyles.tableBoxStyleData,
            ...customCommonStyles.marginBottomOne,
          }}
        />
      )}
      <Box>
        <QpTypography
          displayText="9. Key Processes (e.g. machining, electroplating, heat treatment, mixing, etc.)"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpInputBase
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          onKeyPress={onEnterKeyProcessPress}
          placeholder="Type name and hit enter"
        />
        {keyProcesses?.map((chip, index) => {
          return (
            <Chip
              color="info"
              size="small"
              key={index}
              index={index}
              onDelete={() => handleKeyProcessDelete(index)}
              label={chip}
              sx={styles.chipContainer}
            />
          );
        })}
      </Box>
      <Box>
        <QpTypography
          displayText="10. Outsourced Processes "
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpInputBase
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          onKeyPress={onEnterOutsourcedProcessPress}
          placeholder="Type name and hit enter"
        />
        {outsourcedProcess?.map((chip, index) => {
          return (
            <Chip
              color="info"
              size="small"
              key={index}
              index={index}
              onDelete={() => handleOutsourcedProcessDelete(index)}
              label={chip}
              sx={styles.chipContainer}
            />
          );
        })}
      </Box>
      <Box>
        <QpTypography
          displayText="11. Key Raw Material"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpTypography
          displayText="11.1 Name of raw material"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="rawMaterialName"
          // setValueData={setData}
          onChange={handleInputChange}
          // value={data.rawMaterialName}
          {...rawMaterialRegister("rawMaterialName")}
          error={rawMaterialErrors.rawMaterialName ? true : false}
          // defaultValue={data.rawMaterialName}
          // registerName="rawMaterialName"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("rawMaterialName", data?.rawMaterialName)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={rawMaterialErrors.rawMaterialName?.message}
        />
      </Box>
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="11.2 Whether imported?"
          styleData={styles.labelStyle}
        />
        <QpRadioButton
          options={yesNo}
          name="rawMaterialImported"
          onChange={handleRadioButton}
          value={data.rawMaterialImported}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="11.3 Percentage(%) of raw material imported"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="rawMaterialPercentageImported"
          // setValueData={setData}
          onChange={handleInputChange}
          {...rawMaterialRegister("rawMaterialPercentageImported")}
          error={rawMaterialErrors.rawMaterialPercentageImported ? true : false}
          // value={data.rawMaterialPercentageImported}
          // defaultValue={data.rawMaterialPercentageImported}
          // registerName="rawMaterialPercentageImported"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData(
          //     "rawMaterialPercentageImported",
          //     data?.rawMaterialPercentageImported
          //   )
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={rawMaterialErrors.rawMaterialPercentageImported?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="11.4 Name of country"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="rawMaterialCountryName"
          // setValueData={setData}
          onChange={handleInputChange}
          {...rawMaterialRegister("rawMaterialCountryName")}
          error={rawMaterialErrors.rawMaterialCountryName ? true : false}
          // value={data.rawMaterialCountryName}
          // defaultValue={data.rawMaterialCountryName}
          // registerName="rawMaterialCountryName"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData("rawMaterialCountryName", data?.rawMaterialCountryName)
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={rawMaterialErrors.rawMaterialCountryName?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="11.5 Source of Procurement"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="rawMaterialSourceOfProcurement"
          // setValueData={setData}
          onChange={handleInputChange}
          {...rawMaterialRegister("rawMaterialSourceOfProcurement")}
          error={
            rawMaterialErrors.rawMaterialSourceOfProcurement ? true : false
          }
          // value={data.rawMaterialSourceOfProcurement}
          // defaultValue={data.rawMaterialSourceOfProcurement}
          // registerName="rawMaterialSourceOfProcurement"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData(
          //     "rawMaterialSourceOfProcurement",
          //     data?.rawMaterialSourceOfProcurement
          //   )
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={
            rawMaterialErrors.rawMaterialSourceOfProcurement?.message
          }
        />
      </Box>

      <QpButton
        displayText="Add Raw Material"
        styleData={{ ...commonStyles.blueButton, ...styles.addButtonStyle }}
        textStyle={{
          ...commonStyles.blueButtonText,
          ...styles.addButtontextStyle,
        }}
        // onClick={() => addRawMaterialHandler("key_raw_materials")}
        onClick={handleRawMaterialSubmit(addRawMaterialHandler)}
      />
      {rawMaterialTableRow?.length !== 0 && (
        <CustomTable
          columnDefs={rawMaterialTableColumn}
          rowData={rawMaterialTableRow}
          styleData={commonStyles.tableStyleData}
          boxStyle={{
            ...commonStyles.tableBoxStyleData,
            ...customCommonStyles.marginBottomOne,
          }}
        />
      )}
      <Box>
        <QpTypography
          displayText="12. Bought-out parts/components/sub-assembly/assembly"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpTypography
          displayText="12.1 Name of part/components/sub-assembly/assembly"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="partsName"
          // setValueData={setData}
          onChange={handleInputChange}
          {...partsRegister("partsName")}
          error={partsErrors.partsName ? true : false}
          // value={data.partsName}
          // defaultValue={data.partsName}
          // registerName="partsName"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("partsName", data?.partsName)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={partsErrors.partsName?.message}
        />
      </Box>
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="12.2 Whether imported?"
          styleData={styles.labelStyle}
        />
        <QpRadioButton
          options={yesNo}
          name="partsImported"
          onChange={handleRadioButton}
          value={data.partsImported}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="12.3 Percentage(%) imported"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="partsPercentageImported"
          // setValueData={setData}
          onChange={handleInputChange}
          {...partsRegister("partsPercentageImported")}
          error={partsErrors.partsPercentageImported ? true : false}
          // value={data.partsPercentageImported}
          // defaultValue={data.partsPercentageImported}
          // registerName="partsPercentageImported"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData("partsPercentageImported", data?.partsPercentageImported)
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={partsErrors.partsPercentageImported?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="12.4 Name of country"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="partsCountryName"
          // setValueData={setData}
          onChange={handleInputChange}
          {...partsRegister("partsCountryName")}
          error={partsErrors.partsCountryName ? true : false}
          // value={data.partsCountryName}
          // defaultValue={data.partsCountryName}
          // registerName="partsCountryName"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData("partsCountryName", data?.partsCountryName)
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={partsErrors.partsCountryName?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="12.5 Source of Procurement"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="partsSourceOfProcurement"
          // setValueData={setData}
          onChange={handleInputChange}
          {...partsRegister("partsSourceOfProcurement")}
          error={partsErrors.partsSourceOfProcurement ? true : false}
          // value={data.partsSourceOfProcurement}
          // defaultValue={data.partsSourceOfProcurement}
          // registerName="partsSourceOfProcurement"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) =>
          //   getData("partsSourceOfProcurement", data?.partsSourceOfProcurement)
          // }
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={partsErrors.partsSourceOfProcurement?.message}
        />
      </Box>

      <QpButton
        displayText="Add Bought out parts"
        styleData={{ ...commonStyles.blueButton, ...styles.addButtonStyle }}
        textStyle={{
          ...commonStyles.blueButtonText,
          ...styles.addButtontextStyle,
        }}
        // onClick={() => addPartsHandler("boughtout_parts")}
        onClick={handlePartsSubmit(addPartsHandler)}
      />
      {partsTableRow?.length !== 0 && (
        <CustomTable
          columnDefs={partsTableColumn}
          rowData={partsTableRow}
          styleData={commonStyles.tableStyleData}
          boxStyle={{
            ...commonStyles.tableBoxStyleData,
            ...customCommonStyles.marginBottomOne,
          }}
        />
      )}
      <Box>
        <QpTypography
          displayText="13. Key Vendor(s) (organized sources of raw material, etc.)"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpTypography
          displayText="13.1 Name of the vendor "
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="name"
          // setValueData={setData}
          onChange={handleInputChange}
          // value={data.name}
          {...vendorRegister("name")}
          error={vendorErrors.name ? true : false}
          // defaultValue={data.name}
          // registerName="name"
          // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("name", data?.name)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={vendorErrors.name?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="13.2 Address"
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="address"
          // setValueData={setData}
          // value={data.address}
          onChange={handleInputChange}
          {...vendorRegister("address")}
          error={vendorErrors.address ? true : false}
          // defaultValue={data.address}
          registerName="address"
          // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("address", data?.address)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={vendorErrors.address?.message}
        />
      </Box>
      <QpButton
        displayText="Add Vendor"
        // onClick={() => addVendorHandler("key_vendors")}
        onClick={handleVendorSubmit(addVendorHandler)}
        styleData={{ ...commonStyles.blueButton, ...styles.addButtonStyle }}
        textStyle={{
          ...commonStyles.blueButtonText,
          ...styles.addButtontextStyle,
        }}
      />
      {vendorTableRow?.length !== 0 && (
        <CustomTable
          columnDefs={vendorTableColumn}
          rowData={vendorTableRow}
          styleData={commonStyles.tableStyleData}
          boxStyle={commonStyles.tableBoxStyleData}
        />
      )}
      <Box>
        <QpTypography
          displayText="14. Testing (if any)"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpTypography
          displayText="14.1 Name of the test "
          styleData={styles.labelStyle}
        />
        <InputBase
          sx={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="testName"
          // setValueData={setData}
          // value={data.testName}
          onChange={handleInputChange}
          {...testingRegister("testName")}
          error={testingErrors.testName ? true : false}
          // defaultValue={data.testName}
          // registerName="testName"
          // // isRequired={true}
          // submit={isSubmit}
          // setData={(data) => getData("testName", data?.testName)}
        />
        <QpTypography
          styleData={commonStyles.errorText}
          displayText={testingErrors.testName?.message}
        />
      </Box>
      <Box>
        <QpTypography
          displayText="14.2 Whether external?"
          styleData={styles.labelStyle}
        />
        <QpRadioButton
          options={testingMethod}
          name="externalTesting"
          onChange={handleRadioButton}
          value={data.externalTesting}
        />
      </Box>
      {data.externalTesting === "1" && (
        <>
          <Box>
            <QpTypography
              displayText="14.3 Testing agency name"
              styleData={styles.labelStyle}
            />
            <InputBase
              sx={{
                ...styles.inputBaseStyle,
                ...commonStyles.fullWidth,
              }}
              name="agencyTestName"
              // setValueData={setData}
              // value={data.agencyTestName}
              onChange={handleInputChange}
              {...testingRegister("agencyTestName")}
              error={testingErrors.agencyTestName ? true : false}
              // defaultValue={data.agencyTestName}
              // registerName="agencyTestName"
              // // isRequired={true}
              // submit={isSubmit}
              // setData={(data) =>
              //   getData("agencyTestName", data?.agencyTestName)
              // }
            />
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={testingErrors.agencyTestName?.message}
            />
          </Box>
          <Box>
            <QpTypography
              displayText="14.4 Testing agency address"
              styleData={styles.labelStyle}
            />
            <InputBase
              sx={{
                ...styles.inputBaseStyle,
                ...commonStyles.fullWidth,
              }}
              name="agencyTestAddress"
              // setValueData={setData}
              // value={data.agencyTestAddress}
              onChange={handleInputChange}
              {...testingRegister("agencyTestAddress")}
              error={testingErrors.agencyTestAddress ? true : false}
              // defaultValue={data.agencyTestAddress}
              // registerName="agencyTestAddress"
              // // isRequired={true}
              // submit={isSubmit}
              // setData={(data) =>
              //   getData("agencyTestAddress", data?.agencyTestAddress)
            />
            <QpTypography
              styleData={commonStyles.errorText}
              displayText={testingErrors.agencyTestAddress?.message}
            />
          </Box>
        </>
      )}
      <QpButton
        displayText="Add Testing"
        onClick={handleTestingSubmit(addTestingHandler)}
        styleData={{ ...commonStyles.blueButton, ...styles.addButtonStyle }}
        textStyle={{
          ...commonStyles.blueButtonText,
          ...styles.addButtontextStyle,
        }}
      />
      {testingTableRow?.length !== 0 && (
        <CustomTable
          columnDefs={testingTableColumn}
          rowData={testingTableRow}
          styleData={commonStyles.tableStyleData}
          boxStyle={{
            ...commonStyles.tableBoxStyleData,
            ...customCommonStyles.marginBottomOne,
          }}
        />
      )}
      {/* <Box>
        <QpTypography
          displayText="15. Defect Level/Acceptance Quality Level of the final product"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <CustomInput
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          name="acceptance_quality_level"
          setValueData={setData}
          value={data.acceptance_quality_level}
          onChange={handleInputChange}
          defaultValue={data.acceptance_quality_level}
          registerName="acceptance_quality_level"
          // isRequired={true}
          submit={isSubmit}
          setData={(data) =>
            getData("acceptance_quality_level", data?.acceptance_quality_level)
          }
        />
      </Box> */}
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="15. Energy Resources used (electric, thermal, solar, oil, etc.) "
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpInputBase
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          onKeyPress={onEnterEnergyResorucePress}
          placeholder="Type name and hit enter"
        />
        {energyResources?.map((chip, index) => {
          return (
            <Chip
              color="info"
              size="small"
              key={index}
              index={index}
              onDelete={() => handleEnergyResourceDelete(index)}
              label={chip}
              sx={styles.chipContainer}
            />
          );
        })}
      </Box>
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="16. Natural Resources used (oil, water, coal, solar, etc.) "
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpInputBase
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          onKeyPress={onEnterNaturalResorucePress}
          placeholder="Type name and hit enter"
        />
        {naturalResources?.map((chip, index) => {
          return (
            <Chip
              color="info"
              size="small"
              key={index}
              index={index}
              onDelete={() => handleNaturalResoruceDelete(index)}
              label={chip}
              sx={styles.chipContainer}
            />
          );
        })}
      </Box>
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="17. Name of the Consents for Environment "
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpInputBase
          styleData={{
            ...styles.inputBaseStyle,
            ...commonStyles.fullWidth,
          }}
          onKeyPress={onEnterEnvConsentPress}
          placeholder="Type name and hit enter"
        />
        {envConsents?.map((chip, index) => {
          return (
            <Chip
              color="info"
              size="small"
              key={index}
              index={index}
              onDelete={() => handleEnvConsentDelete(index)}
              label={chip}
              sx={styles.chipContainer}
            />
          );
        })}
      </Box>
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="18. Whether blacklisted/banned ever by any competent authority?"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpRadioButton
          options={yesNo}
          name="is_competent_banned"
          onChange={handleRadioButton}
          value={data.is_competent_banned}
        />
      </Box>
      {data?.is_competent_banned == 1 && (
        <Box>
          <QpTypography
            displayText="18.1 Details"
            styleData={styles.labelStyle}
          />
          <CustomInput
            styleData={{
              ...styles.inputBaseStyle,
              ...commonStyles.fullWidth,
            }}
            name="competent_banned_details"
            setValueData={setData}
            value={data.competent_banned_details}
            onChange={handleInputChange}
            defaultValue={data.competent_banned_details}
            registerName="competent_banned_details"
            // isRequired={true}
            submit={isSubmit}
            setData={(data) =>
              getData(
                "competent_banned_details",
                data?.competent_banned_details
              )
            }
          />
        </Box>
      )}
      <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="19. Any ongoing enquiry/investigation against firm?"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpRadioButton
          options={yesNo}
          name="is_enquiry_against_firm"
          onChange={handleRadioButton}
          value={data.is_enquiry_against_firm}
        />
      </Box>
      {data?.is_enquiry_against_firm == 1 && (
        <Box>
          <QpTypography
            displayText="19.1 Details"
            styleData={styles.labelStyle}
          />
          <CustomInput
            styleData={{
              ...styles.inputBaseStyle,
              ...commonStyles.fullWidth,
            }}
            name="enquiry_against_firm_detail"
            setValueData={setData}
            value={data.enquiry_against_firm_detail}
            onChange={handleInputChange}
            defaultValue={data.enquiry_against_firm_detail}
            registerName="enquiry_against_firm_detail"
            // isRequired={true}
            submit={isSubmit}
            setData={(data) =>
              getData(
                "enquiry_against_firm_detail",
                data?.enquiry_against_firm_detail
              )
            }
          />
        </Box>
      )}
      <SaveNextButtons
        blueButtonText="Save & Next"
        onNextClick={nextHandler}
        greyButtonRequired={false}
      />
      <QpDialog
        open={openDialog}
        closeModal={setOpenDialog}
        styleData={commonStyles.dialogContainer}
      >
        <QpConfirmModal
          displayText="Are you sure you want to delete this record?"
          closeModal={setOpenDialog}
          onConfirm={() => handleRecordConfirm(deleteItem)}
        />
      </QpDialog>
    </QpFormContainer>
  );
}
