import { BLACKISH } from "../../../Constant/ColorConstant";

export const styles = {
  inputBaseStyle: {
    width: "50%",
    marginBottom: "0.5rem",
    border: "0.063rem solid #A9A9A9",
    paddingX: "0.5rem",
  },
  subHeadingLabel: {
    fontWeight: 400,
    fontSize: "1rem",
    lineHeight: "1.5",
    color: "black",
    marginBottom: "0.438rem",
    "& .MuiFormLabel-asterisk": {
      color: "red",
      fontWeight: 700,
    },
  },
  subHeadingLabelWidth: {
    width: "27rem",
  },
  capacityStyles: {
    alignItems: "start",
  },
  unitStyle: {
    width: "8rem",
  },
  dropdownStyle: {
    marginBottom: "0.5rem",
  },
  adminSkillContainer: {
    alignItems: "start",
    marginTop: "0.5rem",
    flexDirection: "column",
  },
  skilledContainer: {
    display: "flex",
    alignItems: "start",
    justifyContent: "space-between",
    marginTop: "0.5rem",
    flexDirection: "column",
  },
  productHeading: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1.5rem",
    lineHeight: "2.043rem",
    color: BLACKISH,
    "@media(max-width:600px)": {
      fontSize: "1rem",
      fontWeight: 600,
    },
  },
  productContainer: {
    marginTop: "1rem",
    marginBottom: "1rem",
    borderBottom: "0.063rem solid #EAEAEA",
    paddingBottom: "1rem",
  },
  chipContainer: {
    marginRight: "0.5rem",
    marginBottom: "0.5rem",
  },
  addButtonStyle: {
    height: "2rem",
    marginBottom: "1rem",
    marginTop: "1rem",
  },
  rawButtonStyle: {
    height: "3rem",
    width: "13rem",
    marginBottom: "1rem",
    marginTop: "1rem",
  },
  addButtontextStyle: {
    fontWeight: 500,
    fontSize: "0.875rem",
  },
  showContainer: {
    // border: "1px solid #60C5F9",
  },
  labelStyle: {
    marginBottom: "0.438rem",
  },
  noteStyle: {
    fontStyle: "italic",
    fontWeight: 600,
    width: "42%",
  },
};
