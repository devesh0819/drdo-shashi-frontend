import * as colors from "../../../Constant/ColorConstant";

export const styles = {
  bankerHeading: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1.5rem",
    lineHeight: "2.043rem",
    color: colors.BLACKISH,
    "@media(max-width:600px)": {
      fontSize: "1rem",
      fontWeight: 600,
    },
  },
  bankerContainer: {
    marginTop: "4rem",
    marginBottom: "4rem",
    borderBottom: "0.063rem solid #EAEAEA",
    paddingBottom: "1rem",
  },
  customDownload: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "1.25rem",
  },
  downloadButton: {
    width: "7.875rem",
    background: "#D4D2D2",
    border: "1px solid #A9A9A9",
    borderRadius: "4px",
    textTransform: "none",
    height: "2rem",
    "&:hover": {
      background: "#D4D2D2",
    },
    color: "black",
  },
  downloadText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 400,
    fontSize: "0.938rem",
    lineHeight: "1.277rem",
    color: colors.BLACK,
    textTransform: "none",
    paddingLeft: "0.3rem",
  },
};
