import React, { useState, useEffect, useRef } from "react";
import CustomCheckboxContent from "../../../Components/CustomCheckboxContent/CustomCheckboxContent";
import CustomInput from "../../../Components/CustomInput/CustomInput";
import QpFormContainer from "../../../Components/QpFormContainer/QpFormContainer";
import SaveNextButtons from "../../../Components/SaveNextButtons/SaveNextButtons";
import {
  GENERAL_INFO,
  PROCESS_DETAILS,
  STEP1_DETAILS,
} from "../../../Routes/Routes";
import { useSelector, useDispatch } from "react-redux";
import {
  saveStep2FormAction,
  saveStep2FormDetails,
} from "../../../Redux/ApplicantForm/applicantFormActions";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import CustomDropdown from "../../../Components/CustomDropdown/CustomDropdown";
import CustomImageUploader from "../../../Components/CustomImageUploader/CustomImageUploader";
import * as constants from "../../../Constant/AppConstant";
import { Typography, Box } from "@mui/material";
import { styles } from "./Step2FormStyles";
import { commonStyles } from "../../../Styles/CommonStyles";
import CustomDownloadButton from "../../../Components/CustomDownloadButton/CustomDownloadButton";
import { showToast } from "../../../Components/Toast/Toast";
import {
  getDistrictAction,
  getStatesAction,
} from "../../../Redux/Admin/GetMasterData/getMasterDataActions";
import * as _ from "lodash";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../../Redux/AllApplications/allApplicationsActions";

const initialState = {
  enterprise_name: "",
  ncage_number: "",
  enterprice_type: "",
  entrepreneur_name: "",
  entrepreneur_aadhar_number: "",
  entrepreneur_gst_number: "",
  tan_number: "",
  bank_name: "",
  bank_account_number: "",
  bank_ifsc_code: "",
  bank_branch_name: "",
  bank_full_address: "",
  bank_state: "",
  bank_district: "",
  bank_city: "",
  ecs_form: "",
  financial_details_confirmed: "",
};
export default function Step2Form() {
  const [data, setData] = useState(initialState);
  const [checkedDeclaration, setCheckedDeclaration] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [ecsFile, setEcsFile] = useState();
  const [stepTwoData, setStepTwoData] = useState({});
  const stepTwoDataRef = useRef(stepTwoData);
  const runOnce = useRef(false);
  const isDone = useRef(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();

  const stateOptions = useSelector((state) => state.getMasterData.statesData);
  const districtOptions = useSelector(
    (state) => state.getMasterData.districtData
  );

  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );

  const location = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  useEffect(() => {
    if (data?.bank_state?.id) {
      dispatch(getDistrictAction(data?.bank_state?.id));
    }
  }, [data?.bank_state?.id]);

  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
    };
  }, [id]);

  useEffect(() => {
    if (
      Object.keys(applicationDetail)?.length > 0 &&
      applicationDetail?.financial &&
      Object.keys(applicationDetail?.financial)?.length > 0
    ) {
      setData(applicationDetail?.financial);
    }
  }, [applicationDetail?.financial]);

  useEffect(() => {
    dispatch(getStatesAction());
  }, []);

  useEffect(() => {
    if (applicationDetail?.financial?.financial_details_confirmed) {
      setCheckedDeclaration(true);
    }
  }, [applicationDetail?.financial?.financial_details_confirmed]);

  useEffect(() => {
    if (applicationDetail?.financial?.ecs_form?.attachment_uuid) {
      // setEcsFile({
      //   name: `${applicationDetail?.financial?.ecs_form?.attachment_uuid}.pdf`,
      // });

      setData((values) => ({
        ...values,
        ecs_form: {
          name: `${applicationDetail?.financial?.ecs_form?.attachment_uuid}`,
        },
      }));
    }
  }, [applicationDetail?.financial?.ecs_form?.attachment_uuid]);
  useEffect(() => {
    if (applicationDetail?.financial?.enterprice_type) {
      setData((values) => ({
        ...values,
        enterprice_type: _.find(constants.typeOfEnterpriseArray, {
          value: applicationDetail?.enterprice_type,
        }),
      }));
    }
  }, [applicationDetail?.financial?.enterprice_type]);

  useEffect(() => {
    const {
      enterprise_name,
      ncage_number,
      enterprice_type,
      entrepreneur_name,
      entrepreneur_aadhar_number,
      entrepreneur_gst_number,
      tan_number,
      bank_name,
      bank_account_number,
      bank_ifsc_code,
      bank_branch_name,
      bank_full_address,
      bank_state,
      bank_district,
      bank_city,
    } = stepTwoData;
    //to be refacored after api integration
    if (
      // enterprise_name &&
      // ncage_number &&
      // enterprice_type &&
      // entrepreneur_name &&
      entrepreneur_aadhar_number &&
      entrepreneur_gst_number &&
      tan_number &&
      bank_name &&
      bank_account_number &&
      bank_ifsc_code &&
      bank_branch_name &&
      bank_full_address &&
      bank_state &&
      bank_district &&
      bank_city
    ) {
      onSubmit();
    }
  }, [stepTwoData]);

  const onSubmit = (requiredData) => {
    if (!isDone.current) {
      return;
    }
    isDone.current = false;
    const {
      enterprise_name,
      ncage_number,
      enterprice_type,
      entrepreneur_name,
      entrepreneur_aadhar_number,
      entrepreneur_gst_number,
      tan_number,
      bank_name,
      bank_account_number,
      bank_ifsc_code,
      bank_branch_name,
      bank_full_address,
      bank_state,
      bank_district,
      bank_city,
    } = stepTwoData;
    if (
      !entrepreneur_aadhar_number ||
      !entrepreneur_gst_number ||
      !tan_number ||
      !bank_name ||
      !bank_account_number ||
      !bank_ifsc_code ||
      !bank_branch_name ||
      !bank_full_address ||
      !bank_state ||
      !bank_district ||
      !bank_city
    ) {
      return;
    }
    if (!data.ecs_form) {
      showToast("Please upload ecs form", "error");
      return;
    }
    if (!checkedDeclaration) {
      showToast("Declaration is required", "error");
      return;
    }

    const formData = new FormData();
    // formData.append("enterprise_name", enterprise_name);
    // formData.append("ncage_number", ncage_number);
    // formData.append("enterprice_type", enterprice_type.value);
    // formData.append("entrepreneur_name", entrepreneur_name);
    formData.append("entrepreneur_aadhar_number", entrepreneur_aadhar_number);
    formData.append("entrepreneur_gst_number", entrepreneur_gst_number);
    formData.append("tan_number", tan_number);
    formData.append("bank_name", bank_name);
    formData.append("bank_account_number", bank_account_number);
    formData.append("bank_ifsc_code", bank_ifsc_code);
    formData.append("bank_branch_name", bank_branch_name);
    formData.append("bank_full_address", bank_full_address);
    formData.append("bank_state", bank_state.id);

    formData.append("bank_district", bank_district.id);

    formData.append("bank_city", bank_city);

    // let sampleFile = new Blob(
    //   [JSON.stringify(applicationDetail?.financial?.ecs_form, null)],
    //   { type: "application/pdf" }
    // );
    if (!applicationDetail?.financial?.ecs_form) {
      formData.append("ecs_form", data?.ecs_form);
    } else {
      const tempVal = data?.ecs_form?.name;
      if (tempVal !== applicationDetail?.financial?.ecs_form?.attachment_uuid) {
        formData.append("ecs_form", data?.ecs_form);
      }
    }

    formData.append("financial_details_confirmed", checkedDeclaration ? 1 : 0);

    dispatch(
      saveStep2FormAction(
        applicationDetail?.application_uuid,
        formData,
        navigate,
        dispatch
      )
    );
    dispatch(saveStep2FormDetails(Object.fromEntries(formData)));
  };

  const updateState = (newState) => {
    stepTwoDataRef.current = newState;
    setStepTwoData(newState);
    setIsSubmit(false);
  };

  const getData = (key, value) => {
    updateState({
      ...stepTwoDataRef.current,
      [key]: value,
    });
  };

  const saveHandler = () => {
    dispatch(saveStep2FormDetails(data));
  };

  const nextHandler = () => {
    setIsSubmit(true);
    isDone.current = true;
  };

  return (
    <QpFormContainer activeStep={1} changeNavigate={`/general-info/${id}`}>
      <CustomInput
        label="1. Adhaar Number of Entrepreneur"
        inputId="entrepreneur_aadhar_number"
        setValueData={setData}
        placeholder="1234 1234 1234"
        value={data.entrepreneur_aadhar_number}
        maxLength={14}
        defaultValue={data.entrepreneur_aadhar_number}
        name="entrepreneur_aadhar_number"
        registerName="entrepreneur_aadhar_number"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData(
            "entrepreneur_aadhar_number",
            data?.entrepreneur_aadhar_number
          )
        }
      />
      <CustomInput
        label="2. GST Number of the Unit"
        inputId="entrepreneur_gst_number"
        setValueData={setData}
        value={data.entrepreneur_gst_number}
        defaultValue={data.entrepreneur_gst_number}
        name="entrepreneur_gst_number"
        registerName="entrepreneur_gst_number"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("entrepreneur_gst_number", data?.entrepreneur_gst_number)
        }
        maxLength={15}
      />
      <CustomInput
        label="3. TAN/TIN Number (if TDS Deducted)"
        inputId="tan_number"
        setValueData={setData}
        value={data.tan_number}
        defaultValue={data.tan_number}
        name="tan_number"
        registerName="tan_number"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("tan_number", data?.tan_number)}
        maxLength={10}
      />

      <Box sx={styles.bankerContainer}>
        <Typography sx={styles.bankerHeading}>Banker Details</Typography>
      </Box>
      <CustomInput
        id="bank_name"
        label="1. Bank Name"
        setValueData={setData}
        value={data.bank_name}
        defaultValue={data.bank_name}
        name="bank_name"
        registerName="bank_name"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("bank_name", data?.bank_name)}
      />
      <CustomInput
        label="2. Account Number"
        inputId="bank_account_number"
        setValueData={setData}
        value={data.bank_account_number}
        defaultValue={data.bank_account_number}
        name="bank_account_number"
        registerName="bank_account_number"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("bank_account_number", data?.bank_account_number)
        }
        maxLength={18}
      />
      <CustomInput
        label="3. IFSC Code"
        inputId="bank_ifsc_code"
        setValueData={setData}
        value={data.bank_ifsc_code}
        defaultValue={data.bank_ifsc_code}
        name="bank_ifsc_code"
        registerName="bank_ifsc_code"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("bank_ifsc_code", data?.bank_ifsc_code)}
        maxLength={11}
      />
      <CustomInput
        label="4. Branch Name"
        inputId="bank_branch_name"
        setValueData={setData}
        value={data.bank_branch_name}
        defaultValue={data.bank_branch_name}
        name="bank_branch_name"
        registerName="bank_branch_name"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("bank_branch_name", data?.bank_branch_name)}
      />
      <CustomInput
        label="5. Full Address"
        inputId="bank_full_address"
        setValueData={setData}
        value={data.bank_full_address}
        defaultValue={data.bank_full_address}
        name="bank_full_address"
        registerName="bank_full_address"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("bank_full_address", data?.bank_full_address)
        }
      />
      <CustomDropdown
        id="bank_state"
        label="6. State"
        options={stateOptions}
        setValueData={setData}
        value={data.bank_state}
        defaultValue={data.bank_state}
        name="bank_state"
        registerName="bank_state"
        disableSourceForValue={true}
        source="title"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("bank_state", data?.bank_state)}
      />
      <CustomDropdown
        id="bank_district"
        label="7. District"
        options={districtOptions}
        setValueData={setData}
        value={data.bank_district}
        defaultValue={data.bank_district}
        name="bank_district"
        registerName="bank_district"
        disableSourceForValue={true}
        source="title"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("bank_district", data?.bank_district)}
      />
      <CustomInput
        label="8. City"
        inputId="bank_city"
        setValueData={setData}
        value={data.bank_city}
        defaultValue={data.bank_city}
        name="bank_city"
        registerName="bank_city"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("bank_city", data?.bank_city)}
      />
      {/* <Box sx={styles.customDownload}>
        <Box>
          <Typography sx={commonStyles.label}>
            9. Download ECS Mandate Form
          </Typography>
        </Box>
        <CustomDownloadButton
          label="Download"
          buttonStyle={styles.downloadButton}
          textStyle={styles.downloadText}
        />
      </Box> */}
      <CustomImageUploader
        file={data.ecs_form}
        value={data.ecs_form}
        setValue={setData}
        displayText="10. Enclose ECS Mandate"
        isLabel={true}
        name="ecs_form"
        typeText="Type: .pdf, Max Size:5MB "
        accept=".pdf"
        required={true}
        docType="pdf"
      />
      <CustomCheckboxContent
        checked={checkedDeclaration}
        setChecked={setCheckedDeclaration}
      />
      <SaveNextButtons
        blueButtonText="Save & Next"
        onNextClick={nextHandler}
        greyButtonRequired={false}
      />
    </QpFormContainer>
  );
}
