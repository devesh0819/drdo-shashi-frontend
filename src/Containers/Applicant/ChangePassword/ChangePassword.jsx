import React, { useEffect } from "react";
import NewPassword from "../../NewPassword/NewPassword";
import { commonStyles } from "../../../Styles/CommonStyles";
import { useNavigate } from "react-router-dom";
import { CHANGE_PASSWORD } from "../../../Routes/Routes";
import { ROLES } from "../../../Constant/RoleConstant";
import { getUserData, logout } from "../../../Services/localStorageService";

export default function ChangePassword() {
  const navigate = useNavigate();

  useEffect(() => {
    window.addEventListener("popstate", () => {
      navigate(CHANGE_PASSWORD);
    });
  }, [navigate]);

  return (
    <>
      <NewPassword styleData={commonStyles.passwordContainer} />
    </>
  );
}
