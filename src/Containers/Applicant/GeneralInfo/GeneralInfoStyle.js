import * as colors from "../../../Constant/ColorConstant";
import { commonStyles } from "../../../Styles/CommonStyles";

export const styles = {
  outerContainer: {
    paddingLeft: "3.75rem",
    paddingRight: "3.75rem",
    paddingTop: "1rem",
    paddingBottom: "1rem",
  },
  backButton: {
    display: "flex",
    justifyContent: "flex-start",
    minWidth: "unset",
    padding: 0,
    "&:hover": { backgroundColor: "transparent" },
  },
  backText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1rem",
    lineHeight: "1.703rem",
    color: colors.BLACK,
    paddingLeft: "0.375rem",
  },
  stepperContainer: {
    border: "0.063rem solid #255491",
    // marginTop: "1rem",
    marginBottom: "1.4rem",
    paddingLeft: "2.635rem",
    paddingRight: "2.635rem",
    backgroundColor: colors.WHITE,
    borderRadius: "0.25rem",
  },
  stepperUpper: {
    borderBottom: "0.063rem solid #EAEAEA",
    paddingY: "0.8rem",
  },
  stepperText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1.5rem",
    lineHeight: "2.043rem",
    color: colors.BLACK,
  },
  stepperLower: { paddingY: "1.5rem" },
  formContainer: {
    backgroundColor: colors.WHITE,
    paddingTop: "1.5rem",
    paddingBottom: "1.5rem",
    paddingLeft: "2.635rem",
    paddingRight: "2.635rem",
    borderRadius: "0.25rem",
  },
  contentContainer: {
    height: "calc(100vh - 16.5rem)",
    overflow: "scroll",
    ...commonStyles.customScrollBar,
    marginTop: "1rem",
  },
  emailContentOuterContainer: {
    "@media(max-width: 600px)": {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
    },
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  mobileContentOuterContainer: {
    "@media(max-width: 600px)": {
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
    },
    display: "flex",
    justifyContent: "space-between",
  },
  email: {
    width: "44.7%",
    "@media(max-width:600px)": {
      width: "100%",
    },
  },
  alternateEmail: {
    width: "45%",
    "@media(max-width:600px)": {
      width: "100%",
    },
  },

  saveButtonText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1rem",
    lineHeight: "1.362rem",
    color: colors.BLACKISH,
    textTransform: "none",
  },
  nextButtonText: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: 700,
    fontSize: "1rem",
    lineHeight: "1.362rem",
    color: colors.WHITE,
    textTransform: "none",
  },
  saveButton: {
    width: "13%",
    height: "3rem",
    backgroundColor: colors.SILVER_GREY,
    "&:hover": {
      backgroundColor: colors.GREY,
    },
    "@media(max-width:600px)": {
      width: "45%",
    },
    "@media(max-width:1200px)": {
      width: "25%",
    },
  },
  nextButton: {
    width: "13%",
    height: "3rem",
    backgroundColor: colors.BLUE,
    "&:hover": {
      backgroundColor: colors.DARKER_BLUE,
    },
    marginLeft: "1.875rem",
    "@media(max-width:600px)": {
      width: "45%",
    },
    "@media(max-width:1200px)": {
      width: "23%",
    },
  },
  buttonsContainer: {
    display: "flex",
    justifyContent: "flex-start",
    marginTop: "1.25rem",
  },
  addressLabel: {
    marginBottom: "1.25rem",
  },
  addressCheckbox: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    marginBottom: "1.25rem",
  },
};
