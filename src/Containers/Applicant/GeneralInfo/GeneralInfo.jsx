import React, { useState, useEffect, useRef } from "react";
import CustomInput, {
  NumberFields,
} from "../../../Components/CustomInput/CustomInput";
import { Box, Typography } from "@mui/material";
import * as constants from "../../../Constant/AppConstant";
import { styles } from "./GeneralInfoStyle";
import { commonStyles } from "../../../Styles/CommonStyles";
import CustomRadioButton from "../../../Components/CustomRadioButton/CustomRadioButton";
import QpFormContainer from "../../../Components/QpFormContainer/QpFormContainer";
import CustomDropdown from "../../../Components/CustomDropdown/CustomDropdown";
import QpCheckbox from "../../../Components/QpCheckbox/QpCheckbox";
import SaveNextButtons from "../../../Components/SaveNextButtons/SaveNextButtons";
import * as routes from "../../../Routes/Routes";
import CustomCheckboxContent from "../../../Components/CustomCheckboxContent/CustomCheckboxContent";
import { useSelector, useDispatch } from "react-redux";
import {
  saveGeneralInfo,
  saveGeneralInfoAction,
} from "../../../Redux/ApplicantForm/applicantFormActions";
import { useNavigate, useParams } from "react-router-dom";
import {
  getDistrictAction,
  getOrganisationTypeAction,
  getStatesAction,
  getUnitDistrictAction,
} from "../../../Redux/Admin/GetMasterData/getMasterDataActions";
import { showToast } from "../../../Components/Toast/Toast";
import * as _ from "lodash";
import * as moment from "moment";
import {
  getApplicationDetail,
  getApplicationDetailAction,
} from "../../../Redux/AllApplications/allApplicationsActions";
import CustomImageUploader from "../../../Components/CustomImageUploader/CustomImageUploader";
import { APP_STAGE, ROLES } from "../../../Constant/RoleConstant";
import { getUserData, logout } from "../../../Services/localStorageService";
import CustomMultiSelect from "../../../Components/CustomMultiSelect/CustomMultiSelect";

const initialState = {
  social_category: "GN",
  special_category: "EXS",
  entrepreneur_name: "",
  enterprise_name: "",
  enterprice_details: "",
  enterprice_nature: "",
  business_nature: "",
  enterprice_type: "MIC",
  // organisation_type: "",
  registered_address: "",
  registered_state: "",
  registered_district: "",
  registered_pin: "",
  registered_mobile_number: "",
  registered_landline_number: "",
  registered_email: "",
  registered_alternate_email: "",
  unit_address: "",
  unit_state: "",
  unit_district: "",
  unit_pin: "",
  unit_mobile_number: "",
  unit_landline_number: "",
  unit_email: "",
  unit_alternate_email: "",
  poc_designation: "",
  poc_mobile_number: "",
  unit_commencement_date: "",
  site_exterior_picture: "",
  entrepreneur_gst_number: "",
  entrepreneur_gst_certificate: "",
  udyam_certificate: "",
  incorporation_certificate: "",
  drdo_registration_number: "",
  unit_web_url: "",
  enterprise_commencement_date: "",
  cpcb_code: "NA",
  persons_employed: "",
  supplier_to_defence: 0,
  is_foreign_partner: 0,
  is_existing_vendor: 0,
  drdo_category: "",
  drdo_storage: [],
  vendor_certificate: "",
  police_jurisdiction: "",
  police_jurisdiction_address: "",
  uam_number: "",
  ncage_number: "",
  basic_details_confirmed: "",
  unit_address_confirmed: "",
};

const GenerelInfo = () => {
  const [data, setData] = useState(initialState);
  const [dataLength, setDataLength] = useState();
  const [checked, setChecked] = useState(false);
  const [addressChecked, setAddressChecked] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [stepOneData, setStepOneData] = useState({});
  const stepOneDataRef = useRef(stepOneData);
  const runOnce = useRef(false);
  const runLogoutOnce = useRef(false);
  const isDone = useRef(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();
  const userData = getUserData();
  const role = userData?.roles[0];

  const stateOptions = useSelector((state) => state.getMasterData.statesData);
  const districtOptions = useSelector(
    (state) => state.getMasterData.districtData
  );
  const unitDistrictOptions = useSelector(
    (state) => state.getMasterData.unitDistrictData
  );
  const organisationTypeOptions = useSelector(
    (state) => state.getMasterData.organisationType
  );

  const applicationDetail = useSelector(
    (state) => state.allApplications.applicationDetail
  );

  useEffect(() => {
    if (role !== ROLES.VENDOR && runLogoutOnce.current === false) {
      logout(navigate);
    } else {
      runLogoutOnce.current = true;
    }
  }, [role]);

  useEffect(() => {
    if (data?.registered_state?.id) {
      dispatch(getDistrictAction(data.registered_state.id));
    }
  }, [data?.registered_state?.id]);

  useEffect(() => {
    if (data?.unit_state?.id) {
      dispatch(getUnitDistrictAction(data?.unit_state?.id));
    }
  }, [data?.unit_state?.id]);

  useEffect(() => {
    const {
      entrepreneur_name,
      enterprise_name,
      social_category,
      special_category,
      enterprice_details,
      enterprice_nature,
      business_nature,
      enterprice_type,
      // ncage_number,
      // organisation_type,
      registered_address,
      registered_state,
      registered_district,
      registered_pin,
      registered_mobile_number,
      registered_email,
      enterprise_commencement_date,
      poc_name,
      poc_designation,
      poc_mobile_number,
      unit_commencement_date,
      site_exterior_picture,
      entrepreneur_gst_number,
      entrepreneur_gst_certificate,
      udyam_certificate,
      incorporation_certificate,
      drdo_registration_number,
      vendor_certificate,
      persons_employed,
      supplier_to_defence,
      is_existing_vendor,
      is_foreign_partner,
      police_jurisdiction,
      police_jurisdiction_address,
      cpcb_code,
    } = stepOneData;

    if (
      // entrepreneur_name &&
      enterprise_name &&
      // social_category &&
      // special_category &&
      // enterprice_details &&
      enterprice_nature &&
      // business_nature &&
      enterprice_type &&
      // ncage_number &&
      // organisation_type &&
      registered_address &&
      registered_state &&
      registered_district &&
      registered_pin &&
      registered_mobile_number &&
      registered_email &&
      enterprise_commencement_date &&
      // persons_employed &&
      // supplier_to_defence &&
      is_existing_vendor &&
      poc_name &&
      poc_designation &&
      poc_mobile_number &&
      unit_commencement_date &&
      entrepreneur_gst_number
      // is_foreign_partner &&
      // is_foreign_partner &&
      // police_jurisdiction &&
      // police_jurisdiction_address &&
      // cpcb_code
    ) {
      onSubmit();
    }
  }, [stepOneData]);

  useEffect(() => {
    if (addressChecked) {
      setData((values) => ({
        ...values,
        unit_address: values.registered_address,
        unit_state: values.registered_state,
        unit_district: values.registered_district,
        unit_pin: values.registered_pin,
        unit_mobile_number: values.registered_mobile_number,
        unit_landline_number: values.registered_landline_number,
        unit_email: values.registered_email,
        unit_alternate_email: values.registered_alternate_email,
      }));
    }
  }, [addressChecked, setData]);

  useEffect(() => {
    setData((values) => ({ ...values, registrationNumber: "" }));
  }, [data.enterpriseType]);

  useEffect(() => {
    dispatch(getStatesAction());
    // dispatch(getOrganisationTypeAction());
  }, []);

  useEffect(() => {
    if (id && runOnce.current === false) {
      dispatch(getApplicationDetailAction(id));
    }
    return () => {
      runOnce.current = true;
      dispatch(getApplicationDetail({}));
    };
  }, [id]);

  useEffect(() => {
    if (id && Object.keys(applicationDetail)?.length > 0) {
      setData(applicationDetail);
    } else {
      setData(initialState);
      setAddressChecked(false);
    }
  }, [applicationDetail]);

  useEffect(() => {
    if (
      id &&
      applicationDetail?.application_stage &&
      applicationDetail?.application_stage !== APP_STAGE.BASIC &&
      applicationDetail?.application_stage !== APP_STAGE.OWNERSHIP
    ) {
      navigate(routes.HOME);
    }
  }, [id, applicationDetail?.application_stage]);

  useEffect(() => {
    if (applicationDetail?.cpcb_code === null) {
      setData((values) => ({
        ...values,
        cpcb_code: "NA",
      }));
    }
  }, [applicationDetail?.cpcb_code]);

  useEffect(() => {
    if (applicationDetail?.unit_commencement_date) {
      const tempDate = moment(
        applicationDetail?.unit_commencement_date
      )?.format("YYYY-MM-DD");
      setData((values) => ({
        ...values,
        unit_commencement_date: tempDate,
      }));
    }
  }, [applicationDetail?.unit_commencement_date]);
  useEffect(() => {
    if (applicationDetail?.vendor_certificate?.attachment_uuid) {
      setData((values) => ({
        ...values,
        vendor_certificate: {
          name: `${applicationDetail?.vendor_certificate?.attachment_uuid}`,
        },
      }));
    }
  }, [applicationDetail?.vendor_certificate?.attachment_uuid]);
  useEffect(() => {
    if (applicationDetail?.site_exterior_picture?.attachment_uuid) {
      setData((values) => ({
        ...values,
        site_exterior_picture: {
          name: `${applicationDetail?.site_exterior_picture?.attachment_uuid}`,
        },
      }));
    }
  }, [applicationDetail?.site_exterior_picture?.attachment_uuid]);
  useEffect(() => {
    if (applicationDetail?.entrepreneur_g_s_t_certificate?.attachment_uuid) {
      setData((values) => ({
        ...values,
        entrepreneur_gst_certificate: {
          name: `${applicationDetail?.entrepreneur_g_s_t_certificate?.attachment_uuid}`,
        },
      }));
    }
  }, [applicationDetail?.entrepreneur_g_s_t_certificate?.attachment_uuid]);
  useEffect(() => {
    if (applicationDetail?.udyam_certificate?.attachment_uuid) {
      setData((values) => ({
        ...values,
        udyam_certificate: {
          name: `${applicationDetail?.udyam_certificate?.attachment_uuid}`,
        },
      }));
    }
  }, [applicationDetail?.udyam_certificate?.attachment_uuid]);

  useEffect(() => {
    if (applicationDetail?.incorporation_certificate?.attachment_uuid) {
      setData((values) => ({
        ...values,
        incorporation_certificate: {
          name: `${applicationDetail?.incorporation_certificate?.attachment_uuid}`,
        },
      }));
    }
  }, [applicationDetail?.incorporation_certificate?.attachment_uuid]);

  // useEffect(() => {
  //   if (applicationDetail?.enterprice_type) {
  //     setData((values) => ({
  //       ...values,
  //       enterprice_type: _.find(constants.typeOfEnterpriseArray, {
  //         value: applicationDetail?.enterprice_type,
  //       }),
  //     }));
  //   }
  // }, [applicationDetail?.enterprice_type]);
  useEffect(() => {
    if (applicationDetail?.enterprice_details) {
      setData((values) => ({
        ...values,
        enterprice_details: _.find(constants.enterpriseType, {
          value: applicationDetail?.enterprice_details,
        }),
      }));
    }
  }, [applicationDetail?.enterprice_details]);
  useEffect(() => {
    if (applicationDetail?.enterprice_nature) {
      setData((values) => ({
        ...values,
        enterprice_nature: _.find(constants.natureOfEnterpriseArray, {
          value: applicationDetail?.enterprice_nature,
        }),
      }));
    }
  }, [applicationDetail?.enterprice_nature]);

  useEffect(() => {
    if (applicationDetail?.drdo_category) {
      setData((values) => ({
        ...values,
        drdo_category: _.find(constants.drdoCategoryOptions, {
          value: applicationDetail?.drdo_category,
        }),
      }));
    }
  }, [applicationDetail?.drdo_category]);

  useEffect(() => {
    if (applicationDetail?.drdo_storage) {
      setData((values) => ({
        ...values,
        drdo_storage: JSON.parse(applicationDetail?.drdo_storage),
      }));
    } else {
      setData((values) => ({
        ...values,
        drdo_storage: [],
      }));
    }
  }, [applicationDetail?.drdo_storage]);

  useEffect(() => {
    if (applicationDetail?.business_nature) {
      setData((values) => ({
        ...values,
        business_nature: _.find(constants.natureOfBusinessArray, {
          value: applicationDetail?.business_nature,
        }),
      }));
    }
  }, [applicationDetail?.business_nature]);
  useEffect(() => {
    if (applicationDetail?.enterprise_commencement_date) {
      setData((values) => ({
        ...values,

        enterprise_commencement_date: moment(
          new Date(applicationDetail?.enterprise_commencement_date)
        ).format("YYYY-MM-DD"),
      }));
    }
  }, [applicationDetail?.enterprise_commencement_date]);
  useEffect(() => {
    if (applicationDetail?.unit_address_confirmed) {
      setAddressChecked(true);
    }
  }, [applicationDetail?.unit_address_confirmed]);

  // useEffect(() => {
  //   if (applicationDetail?.organisation_type) {
  //     setData((values) => ({
  //       ...values,
  //       organisation_type: _.find(organisationTypeOptions, {
  //         id: applicationDetail?.organisation_type,
  //       }),
  //     }));
  //   }
  // }, [applicationDetail?.organisation_type]);

  const handleAddressCheckChange = (event) => {
    setAddressChecked(event.target.checked);
  };

  const saveHandler = () => {
    // dispatch(saveGeneralInfo(data));
  };

  const nextHandler = () => {
    setIsSubmit(true);
    isDone.current = true;
  };
  const updateState = (newState) => {
    stepOneDataRef.current = newState;
    setStepOneData(newState);
    setIsSubmit(false);
  };

  const getData = (key, value) => {
    updateState({
      ...stepOneDataRef.current,
      [key]: value,
    });
  };

  const onSubmit = (requriedData) => {
    if (!isDone.current) {
      return;
    }
    isDone.current = false;
    const {
      entrepreneur_name,
      enterprise_name,
      social_category,
      special_category,
      enterprice_details,
      enterprice_nature,
      business_nature,
      enterprice_type,
      uam_number,
      cin_number,
      ncage_number,
      // organisation_type,
      registered_address,
      registered_state,
      registered_district,
      registered_pin,
      registered_mobile_number,
      registered_landline_number,
      registered_email,
      enterprise_commencement_date,
      // persons_employed,
      supplier_to_defence,
      is_existing_vendor,
      is_foreign_partner,
      registered_alternate_email,
      unit_web_url,
      unit_address,
      unit_state,
      unit_district,
      unit_pin,
      unit_email,
      unit_alternate_email,
      unit_mobile_number,
      unit_landline_number,
      police_jurisdiction,
      police_jurisdiction_address,
      cpcb_code,
      poc_name,
      poc_designation,
      poc_mobile_number,
      unit_commencement_date,
      site_exterior_picture,
      entrepreneur_gst_number,
      entrepreneur_gst_certificate,
      udyam_certificate,
      incorporation_certificate,
      drdo_registration_number,
      drdo_category,
      drdo_storage,
      vendor_certificate,
    } = stepOneData;
    // if (
    //   !entrepreneur_name ||
    //   !enterprise_name ||
    //   !social_category ||
    //   !special_category ||
    //   !enterprice_details ||
    //   !enterprice_nature ||
    //   !business_nature ||
    //   !enterprice_type ||
    //   !registered_address ||
    //   !registered_state ||
    //   !registered_district ||
    //   !registered_pin ||
    //   !registered_mobile_number ||
    //   !registered_email ||
    //   !unit_address ||
    //   !unit_pin ||
    //   !unit_state ||
    //   !unit_district ||
    //   !unit_email ||
    //   !unit_mobile_number ||
    //   !enterprise_commencement_date ||
    //   !supplier_to_defence ||
    //   !is_foreign_partner ||
    //   !is_existing_vendor ||
    //   !police_jurisdiction ||
    //   !police_jurisdiction_address ||
    //   !enterprise_commencement_date ||
    //   !cpcb_code ||
    //   !poc_name ||
    //   !poc_designation ||
    //   !poc_mobile_number ||
    //   !unit_commencement_date ||
    //   !entrepreneur_gst_number ||
    //   !drdo_registration_number
    // ) {
    //   return;
    // }

    if (
      unit_alternate_email === undefined ||
      registered_alternate_email === undefined ||
      unit_landline_number === undefined ||
      registered_landline_number === undefined
    ) {
      return;
    }

    if (!data.site_exterior_picture) {
      showToast("Please upload site exterior picture", "error");
      return;
    }
    if (!data.entrepreneur_gst_certificate) {
      showToast("Please upload GST certificate", "error");
      return;
    }

    if (
      (data.enterprice_type === "MIC" ||
        data.enterprice_type === "SM" ||
        data.enterprice_type === "MED") &&
      !data.udyam_certificate
    ) {
      showToast("Please upload udyam certificate", "error");
      return;
    }

    if (data.enterprice_type === "L" && !data.incorporation_certificate) {
      showToast("Please upload incorporation certificate", "error");
      return;
    }

    if (data.is_existing_vendor == 1) {
      if (!data.drdo_registration_number) {
        showToast("DRDO registration number is required", "error");
        return;
      }
      if (!data.vendor_certificate) {
        showToast("DRDO certificate is required", "error");
        return;
      }
    }

    // if (enterprice_type?.value === "L" && !cin_number) {
    //   showToast("Please enter CIN Number", "error");
    //   return;
    // }
    // if (enterprice_type?.value !== "L" && !uam_number && !cin_number) {
    //   showToast("Please enter UAM Number", "error");
    //   return;
    // }

    // if (!checked) {
    //   showToast("Declaration is required", "error");
    //   return;
    // }

    const formData = new FormData();
    if (
      !applicationDetail?.vendor_certificate &&
      !applicationDetail?.drdo_registration_number &&
      data.is_existing_vendor == 1
    ) {
      formData.append("vendor_certificate", data.vendor_certificate);
      formData.append("is_existing_vendor", is_existing_vendor);
      formData.append("drdo_registration_number", drdo_registration_number);
      formData.append("drdo_category", drdo_category?.value);
      formData.append("drdo_storage", JSON.stringify(drdo_storage));
    } else {
      const tempVal = data?.vendor_certificate?.name;
      if (
        tempVal !== applicationDetail?.vendor_certificate?.attachment_uuid &&
        data.is_existing_vendor == 1
      ) {
        formData.append("is_existing_vendor", is_existing_vendor);
        formData.append("vendor_certificate", data.vendor_certificate);
        formData.append("drdo_registration_number", drdo_registration_number);
        formData.append("drdo_category", drdo_category?.value);
        formData.append("drdo_storage", JSON.stringify(drdo_storage));
      } else if (
        tempVal === applicationDetail?.vendor_certificate?.attachment_uuid &&
        data.is_existing_vendor == 1
      ) {
        // formData.append("vendor_certificate", data.vendor_certificate);
        formData.append("is_existing_vendor", is_existing_vendor);
        formData.append("drdo_registration_number", drdo_registration_number);
        formData.append("drdo_category", drdo_category?.value);
        formData.append("drdo_storage", JSON.stringify(drdo_storage));
      }
    }
    if (data.is_existing_vendor == 0) {
      formData.append("is_existing_vendor", is_existing_vendor);
    }
    if (!applicationDetail?.site_exterior_picture) {
      formData.append("site_exterior_picture", data.site_exterior_picture);
    } else {
      const tempVal = data?.site_exterior_picture?.name;

      if (
        tempVal !== applicationDetail?.site_exterior_picture?.attachment_uuid
      ) {
        formData.append("site_exterior_picture", data.site_exterior_picture);
      }
    }
    if (!applicationDetail?.entrepreneur_g_s_t_certificate) {
      formData.append(
        "entrepreneur_gst_certificate",
        data.entrepreneur_gst_certificate
      );
    } else {
      const tempVal = data?.entrepreneur_gst_certificate?.name;

      if (
        tempVal !==
        applicationDetail?.entrepreneur_g_s_t_certificate?.attachment_uuid
      ) {
        formData.append(
          "entrepreneur_gst_certificate",
          data.entrepreneur_gst_certificate
        );
      }
    }
    // formData.append("is_foreign_partner", is_foreign_partner);

    // formData.append("entrepreneur_name", entrepreneur_name.trim());
    // formData.append("social_category", social_category);
    // formData.append("special_category", special_category);
    formData.append("enterprise_name", enterprise_name);
    // formData.append("enterprice_details", enterprice_details?.value);
    formData.append("enterprice_nature", enterprice_nature?.value);
    // formData.append("business_nature", business_nature?.value);
    formData.append("enterprice_type", enterprice_type);
    formData.append("registered_address", registered_address);
    formData.append("registered_state", registered_state.id);
    formData.append("registered_district", registered_district.id);
    formData.append("registered_pin", registered_pin);
    formData.append("registered_mobile_number", registered_mobile_number);
    formData.append("registered_email", registered_email);
    // formData.append("supplier_to_defence", supplier_to_defence);
    formData.append("unit_address", unit_address);
    formData.append("unit_state", unit_state?.id);
    formData.append("unit_district", unit_district?.id);
    formData.append("unit_email", unit_email);
    formData.append(
      "unit_mobile_number",
      unit_mobile_number || registered_mobile_number
    );
    formData.append("unit_pin", unit_pin);
    // formData.append("police_jurisdiction", police_jurisdiction);
    // formData.append("police_jurisdiction_address", police_jurisdiction_address);
    formData.append(
      "enterprise_commencement_date",
      enterprise_commencement_date
    );
    formData.append("poc_name", poc_name);
    formData.append("poc_designation", poc_designation);
    formData.append("poc_mobile_number", poc_mobile_number);
    formData.append("unit_commencement_date", unit_commencement_date);
    formData.append("entrepreneur_gst_number", entrepreneur_gst_number);
    formData.append("unit_address_confirmed", addressChecked ? 1 : 0);
    // formData.append("basic_details_confirmed", checked ? 1 : 0);
    // if (cpcb_code === "NA") {
    //   formData.append("cpcb_code", "");
    // } else {
    //   formData.append("cpcb_code", cpcb_code);
    // }

    if (registered_alternate_email) {
      formData.append("registered_alternate_email", registered_alternate_email);
    } else {
      formData.append("registered_alternate_email", "");
    }
    if (registered_landline_number) {
      formData.append("registered_landline_number", registered_landline_number);
    } else {
      formData.append("registered_landline_number", "");
    }
    if (unit_alternate_email) {
      formData.append("unit_alternate_email", unit_alternate_email);
    } else {
      formData.append("unit_alternate_email", "");
    }
    if (unit_landline_number) {
      formData.append("unit_landline_number", unit_landline_number);
    } else {
      formData.append("unit_landline_number", "");
    }
    // if (unit_web_url) {
    //   formData.append("unit_web_url", unit_web_url);
    // } else {
    //   formData.append("unit_web_url", "");
    // }
    // if (ncage_number) {
    //   formData.append("ncage_number", ncage_number);
    // } else {
    //   formData.append("ncage_number", "");
    // }
    if (
      enterprice_type === "L" &&
      !applicationDetail.incorporation_certificate
    ) {
      formData.append(
        "incorporation_certificate",
        data?.incorporation_certificate
      );
    } else {
      const tempVal = data?.incorporation_certificate?.name;
      if (
        tempVal !==
        applicationDetail?.incorporation_certificate?.attachment_uuid
      ) {
        formData.append(
          "incorporation_certificate",
          data.incorporation_certificate
        );
      }
    }
    if (enterprice_type !== "L") {
      formData.append("uam_number", data?.uam_number);
      if (!applicationDetail?.udyam_certificate) {
        formData.append("udyam_certificate", data?.udyam_certificate);
      } else {
        const tempVal = data?.udyam_certificate?.name;
        if (tempVal !== applicationDetail?.udyam_certificate?.attachment_uuid) {
          formData.append("udyam_certificate", data.udyam_certificate);
        }
      }
    }
    // if (!applicationDetail?.udyam_certificate && data.enterprice_type !== "L") {
    //   formData.append("udyam_certificate", data.udyam_certificate);
    //   formData.append("enterprice_type", enterprice_type);
    // } else {
    //   const tempVal = data?.udyam_certificate?.name;
    //   if (
    //     tempVal !== applicationDetail?.udyam_certificate?.attachment_uuid &&
    //     data.enterprice_type !== "L"
    //   ) {
    //     formData.append("enterprice_type", enterprice_type);
    //     formData.append("udyam_certificate", data.udyam_certificate);
    //   }
    // }
    let object = {};
    formData.forEach((value, key) => (object[key] = value));
    let json = JSON.stringify(object);

    dispatch(
      saveGeneralInfoAction(
        formData,
        navigate,
        applicationDetail?.application_uuid,
        dispatch
      )
    );
  };
  return (
    <QpFormContainer activeStep={0} changeNavigate={routes.HOME}>
      <CustomInput
        label="1. Name of Enterprise"
        inputId="enterprise_name"
        placeholder="Enterprise Name"
        maxLength={100}
        setValueData={setData}
        defaultValue={data.enterprise_name}
        name="enterprise_name"
        registerName="enterprise_name"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("enterprise_name", data?.enterprise_name)}
      />
      <Box sx={commonStyles.addressLabel}>
        <Typography sx={commonStyles.label}>
          2. Registered Office Address
        </Typography>
      </Box>
      <CustomInput
        label="2.1 Address"
        inputId="registeredAddress"
        placeholder="Address"
        maxLength={55}
        setValueData={setData}
        value={data.registered_address}
        defaultValue={data.registered_address}
        name="registered_address"
        registerName="registered_address"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("registered_address", data?.registered_address)
        }
      />
      <CustomDropdown
        id="registered_state"
        label="2.2 State"
        options={stateOptions}
        setValueData={setData}
        defaultValue={data.registered_state}
        value={data.registered_state}
        name="registered_state"
        registerName="registered_state"
        isRequired={true}
        disableSourceForValue={true}
        source="title"
        submit={isSubmit}
        setData={(data) => getData("registered_state", data?.registered_state)}
      />
      <CustomDropdown
        id="registered_district"
        label="2.3 District"
        options={districtOptions}
        setValueData={setData}
        defaultValue={data.registered_district}
        value={data.registered_district}
        name="registered_district"
        registerName="registered_district"
        isRequired={true}
        disableSourceForValue={true}
        source="title"
        submit={isSubmit}
        setData={(data) =>
          getData("registered_district", data?.registered_district)
        }
      />
      <CustomInput
        label="2.4 Pincode"
        inputId="registered_pin"
        placeholder="Enter pincode"
        maxLength={6}
        setValueData={setData}
        defaultValue={data.registered_pin}
        value={data.registered_pin}
        name="registered_pin"
        registerName="registered_pin"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("registered_pin", data?.registered_pin)}
      />
      <Box sx={styles.mobileContentOuterContainer}>
        <Box sx={styles.email}>
          <NumberFields
            defaultValue={data.registered_mobile_number}
            landlineValue={data.registered_landline_number}
            mobileNumberId="registered_mobile_number"
            landlineNumberId="registered_landline_number"
            mobileValue={data.registered_mobile_number}
            setValueData={setData}
            mobileDisplayText="2.5 Mobile Number"
            maxLength={10}
            mobileName="registered_mobile_number"
            landlineName="registered_landline_number"
            registerName="registered_mobile_number"
            isRequired={true}
            submit={isSubmit}
            setData={(data) =>
              getData(
                "registered_mobile_number",
                data?.registered_mobile_number
              )
            }
          />
        </Box>
        <Box sx={styles.alternateEmail}>
          <CustomInput
            label="2.6 Landline Number"
            inputId="registered_landline_number"
            setValueData={setData}
            maxLength={11}
            defaultValue={data.registered_landline_number}
            value={data.registered_landline_number}
            name="registered_landline_number"
            isRequired={false}
            registerName="registered_landline_number"
            submit={isSubmit}
            setData={(data) =>
              getData(
                "registered_landline_number",
                data?.registered_landline_number
              )
            }
          />
        </Box>
      </Box>
      <Box sx={styles.emailContentOuterContainer}>
        <Box sx={styles.email}>
          <CustomInput
            label="2.7 Email"
            inputId="registered_email"
            placeholder="Email"
            setValueData={setData}
            defaultValue={data.registered_email}
            value={data.registered_email}
            name="registered_email"
            registerName="registered_email"
            isRequired={true}
            submit={isSubmit}
            setData={(data) =>
              getData("registered_email", data?.registered_email)
            }
          />
        </Box>
        <Box sx={styles.alternateEmail}>
          <CustomInput
            label="2.8 Alternate Email"
            inputId="registered_alternate_email"
            placeholder="Alternate Email"
            setValueData={setData}
            value={data.registered_alternate_email}
            defaultValue={data.registered_alternate_email}
            name="registered_alternate_email"
            registerName="registered_alternate_email"
            isRequired={false}
            submit={isSubmit}
            setData={(data) =>
              getData(
                "registered_alternate_email",
                data?.registered_alternate_email
              )
            }
          />
        </Box>
      </Box>
      <CustomInput
        label="3. Date of Commencement of Enterprise"
        inputId="enterprise_commencement_date"
        setValueData={setData}
        defaultValue={data.enterprise_commencement_date}
        value={data.enterprise_commencement_date}
        name="enterprise_commencement_date"
        type="date"
        registerName="enterprise_commencement_date"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData(
            "enterprise_commencement_date",
            data?.enterprise_commencement_date
          )
        }
      />
      <CustomInput
        label="4. Name of the Point of Contact (PoC)"
        inputId="poc_name"
        placeholder="Enter name of PoC"
        setValueData={setData}
        defaultValue={data.poc_name}
        value={data.poc_name}
        name="poc_name"
        registerName="poc_name"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("poc_name", data?.poc_name)}
      />
      <CustomInput
        label="5. Designation of PoC in the Enterprise"
        inputId="poc_designation"
        placeholder="Enter designation of PoC"
        setValueData={setData}
        defaultValue={data.poc_designation}
        value={data.poc_designation}
        name="poc_designation"
        registerName="poc_designation"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("poc_designation", data?.poc_designation)}
      />
      <NumberFields
        defaultValue={data.poc_mobile_number}
        // landlineValue={data.poc_landline_number}
        mobileNumberId="poc_mobile_number"
        // landlineNumberId="poc_landline_number"
        mobileValue={data.poc_mobile_number}
        setValueData={setData}
        mobileDisplayText="6. Mobile Number of PoC"
        maxLength={10}
        mobileName="poc_mobile_number"
        // landlineName="poc_landline_number"
        registerName="poc_mobile_number"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("poc_mobile_number", data?.poc_mobile_number)
        }
      />
      {/* <CustomRadioButton
        displayText=" 7. Is the unit applied for, same as registered office?"
        options={constants.yesNo}
        setValueData={setData}
        defaultValue={data.unit_address_checked}
        name="unit_address_checked"
        registerName="unit_address_checked"
        submit={isSubmit}
        setData={(data) =>
          getData("unit_address_checked", data?.unit_address_checked)
        }
      /> */}
      <Box sx={commonStyles.addressLabel}>
        <Typography sx={commonStyles.label}>
          7. Is the unit applied for, same as registered office?
        </Typography>
      </Box>

      <Box sx={styles.addressCheckbox}>
        <QpCheckbox
          checked={addressChecked}
          onChange={handleAddressCheckChange}
        />
        <Typography sx={styles.declarationText}>
          Is same as Registered Office
        </Typography>
      </Box>
      <Box sx={commonStyles.addressLabel}>
        <Typography sx={commonStyles.label}>
          8. Manufacturing Unit Address
        </Typography>
      </Box>
      <CustomInput
        label="8.1 Address"
        inputId="unit_address"
        placeholder="Address"
        maxLength={55}
        setValueData={setData}
        defaultValue={data.unit_address}
        value={data.unit_address}
        name="unit_address"
        readOnly={addressChecked ? true : false}
        disabled={addressChecked ? true : false}
        registerName="unit_address"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("unit_address", data?.unit_address)}
      />
      <CustomDropdown
        id="unit_state"
        label="8.2 State"
        options={stateOptions}
        setValueData={setData}
        defaultValue={data.unit_state}
        value={data.unit_state}
        disableSourceForValue={true}
        source="title"
        name="unit_state"
        readOnly={addressChecked ? true : false}
        disabled={addressChecked ? true : false}
        registerName="unit_state"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("unit_state", data?.unit_state)}
      />
      <CustomDropdown
        id="unit_district"
        label="8.3 District"
        options={unitDistrictOptions}
        setValueData={setData}
        defaultValue={data.unit_district}
        value={data.unit_district}
        disableSourceForValue={true}
        source="title"
        name="unit_district"
        readOnly={addressChecked ? true : false}
        disabled={addressChecked ? true : false}
        registerName="unit_district"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("unit_district", data?.unit_district)}
      />
      <CustomInput
        label="8.4 Pincode"
        inputId="unit_pin"
        placeholder="Enter pincode"
        maxLength={6}
        setValueData={setData}
        defaultValue={data.unit_pin}
        value={data.unit_pin}
        name="unit_pin"
        readOnly={addressChecked ? true : false}
        disabled={addressChecked ? true : false}
        registerName="unit_pin"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("unit_pin", data?.unit_pin)}
      />
      <Box sx={styles.mobileContentOuterContainer}>
        <Box sx={styles.email}>
          <NumberFields
            mobileNumberId="unit_mobile_number"
            landlineNumberId="unit_landline_number"
            maxLength={10}
            setValueData={setData}
            defaultValue={data.unit_mobile_number}
            landlineValue={data.unit_landline_number}
            mobileName="unit_mobile_number"
            landlineName="unit_landline_number"
            mobileDisplayText="8.5 Mobile Number"
            readOnly={addressChecked ? true : false}
            disabled={addressChecked ? true : false}
            registerName="unit_mobile_number"
            isRequired={true}
            submit={isSubmit}
            setData={(data) =>
              getData("unit_mobile_number", data?.unit_mobile_number)
            }
          />
        </Box>
        <Box sx={styles.alternateEmail}>
          <CustomInput
            label="8.6 Landline Number"
            inputId="unit_landline_number"
            maxLength={11}
            setValueData={setData}
            defaultValue={data.unit_landline_number}
            value={data.unit_landline_number}
            name="unit_landline_number"
            readOnly={addressChecked ? true : false}
            disabled={addressChecked ? true : false}
            registerName="unit_landline_number"
            isRequired={false}
            submit={isSubmit}
            setData={(data) =>
              getData("unit_landline_number", data?.unit_landline_number)
            }
          />
        </Box>
      </Box>
      <Box sx={{ ...styles.emailContentOuterContainer }}>
        <Box sx={styles.email}>
          <CustomInput
            label="8.7 Email"
            inputId="unit_email"
            placeholder="Email"
            setValueData={setData}
            defaultValue={data.unit_email}
            value={data.unit_email}
            name="unit_email"
            readOnly={addressChecked ? true : false}
            disabled={addressChecked ? true : false}
            registerName="unit_email"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("unit_email", data?.unit_email)}
          />
        </Box>
        <Box sx={styles.alternateEmail}>
          <CustomInput
            label="8.8 Alternate Email"
            inputId="unit_alternate_email"
            placeholder="Alternate Email"
            setValueData={setData}
            defaultValue={data.unit_alternate_email}
            value={data.unit_alternate_email}
            name="unit_alternate_email"
            readOnly={addressChecked ? true : false}
            disabled={addressChecked ? true : false}
            registerName="unit_alternate_email"
            isRequired={false}
            submit={isSubmit}
            setData={(data) =>
              getData("unit_alternate_email", data?.unit_alternate_email)
            }
          />
        </Box>
      </Box>
      <CustomInput
        label="9. Date of Commencement of Unit (applied for)"
        inputId="unit_commencement_date"
        setValueData={setData}
        defaultValue={data.unit_commencement_date}
        value={data.unit_commencement_date}
        name="unit_commencement_date"
        type="date"
        registerName="unit_commencement_date"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("unit_commencement_date", data?.unit_commencement_date)
        }
      />
      <CustomImageUploader
        file={data.site_exterior_picture}
        value={data.site_exterior_picture}
        setValue={setData}
        displayText="10. Upload picture of site exterior"
        typeText="Type: .jpg,.jpeg,.png,.pdf Max Size:5MB "
        isLabel={true}
        name="site_exterior_picture"
        accept=".jpg,.jpeg,.png"
        required={true}
        docType="image"
      />
      <CustomDropdown
        id="enterprice_nature"
        label="11. Type of ownership "
        options={constants.natureOfEnterpriseArray}
        placeholder="Select type of ownership"
        setValueData={setData}
        value={data.enterprice_nature}
        defaultValue={data.enterprice_nature}
        disableSourceForValue={true}
        source="label"
        name="enterprice_nature"
        registerName="enterprice_nature"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("enterprice_nature", data?.enterprice_nature)
        }
      />
      <Box>
        <CustomInput
          label="12.1 GST Number of the Unit"
          inputId="entrepreneur_gst_number"
          setValueData={setData}
          value={data.entrepreneur_gst_number}
          defaultValue={data.entrepreneur_gst_number}
          name="entrepreneur_gst_number"
          registerName="entrepreneur_gst_number"
          isRequired={true}
          placeholder="22AAAAA0000A1Z5"
          submit={isSubmit}
          setData={(data) =>
            getData("entrepreneur_gst_number", data?.entrepreneur_gst_number)
          }
          maxLength={15}
        />
        <CustomImageUploader
          file={data.entrepreneur_gst_certificate}
          value={data.entrepreneur_gst_certificate}
          setValue={setData}
          displayText="12.2 Upload GST Certificate"
          typeText="Type: .jpg,.jpeg,.png,.pdf Max Size:5MB "
          isLabel={true}
          name="entrepreneur_gst_certificate"
          accept=".jpg,.jpeg,.png,.pdf"
          required={true}
          docType="imagePdf"
        />
      </Box>
      <CustomRadioButton
        displayText="13. Enterprise Classification"
        options={constants.typeOfEnterpriseArray}
        setValueData={setData}
        defaultValue={data.enterprice_type}
        name="enterprice_type"
        registerName="enterprice_type"
        submit={isSubmit}
        setData={(data) => getData("enterprice_type", data?.enterprice_type)}
      />
      {data.enterprice_type !== "L" && (
        <>
          <CustomInput
            label="13.1 UDYAM Registration Number"
            inputId="uam_number"
            hasLink
            linkText="Click here to get UAM number"
            href="https://udyamregistration.gov.in/Government-India/Ministry-MSME-registration.htm"
            maxLength={19}
            setValueData={setData}
            value={data.uam_number}
            defaultValue={data.uam_number}
            name="uam_number"
            placeholder="UDYAM-XX-00-0123456"
            labelRequired={true}
            isRequired={false}
            registerName="uam_number"
            submit={isSubmit}
            setData={(data) => getData("uam_number", data?.uam_number)}
          />
          <CustomImageUploader
            file={data.udyam_certificate}
            value={data.udyam_certificate}
            setValue={setData}
            displayText="13.2 Upload Udyam Registration Certificate"
            typeText="Type: .jpg,.jpeg,.png,.pdf Max Size:5MB "
            isLabel={true}
            name="udyam_certificate"
            accept=".jpg,.jpeg,.png,.pdf"
            required={true}
            docType="imagePdf"
          />
        </>
      )}
      {data?.enterprice_type === "L" && (
        <>
          <CustomImageUploader
            file={data.incorporation_certificate}
            value={data.incorporation_certificate}
            setValue={setData}
            displayText="13.1 Upload Certificate of Incorporation"
            typeText="Type: .jpg,.jpeg,.png,.pdf Max Size:5MB "
            isLabel={true}
            name="incorporation_certificate"
            accept=".jpg,.jpeg,.png,.pdf"
            required={true}
            docType="imagePdf"
          />
          {/* <CustomInput
          label="10. CIN Number"
          inputId="cin_number"
          maxLength={21}
          setValueData={setData}
          value={data.cin_number}
          defaultValue={data.cin_number}
          name="cin_number"
          labelRequired={true}
          isRequired={false}
          registerName="cin_number"
          submit={isSubmit}
          setData={(data) => getData("cin_number", data?.cin_number)}
        /> */}
        </>
      )}
      {/*<CustomInput
        label="1. Name of Entrepreneur"
        inputId="entrepreneur_name"
        placeholder="Full Name"
        maxLength={100}
        setValueData={setData}
        value={data.entrepreneur_name}
        defaultValue={data.entrepreneur_name}
        name="entrepreneur_name"
        registerName="entrepreneur_name"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("entrepreneur_name", data?.entrepreneur_name)
        }
      />
      <CustomRadioButton
        displayText="2. Social Category"
        options={constants.socialCategoryOptions}
        setValueData={setData}
        defaultValue={data.social_category}
        name="social_category"
        registerName="social_category"
        submit={isSubmit}
        setData={(data) => getData("social_category", data?.social_category)}
      />
      <CustomRadioButton
        displayText="3. Any special category of Entrepreneur"
        options={constants.specialTypeOptions}
        setValueData={setData}
        defaultValue={data.special_category}
        name="special_category"
        registerName="special_category"
        submit={isSubmit}
        setData={(data) => getData("special_category", data?.special_category)}
      /> */}
      <CustomRadioButton
        displayText="14. Are you an existing DRDO vendor? "
        options={constants.yesNo}
        setValueData={setData}
        defaultValue={data.is_existing_vendor && +data.is_existing_vendor}
        name="is_existing_vendor"
        registerName="is_existing_vendor"
        submit={isSubmit}
        setData={(data) =>
          getData("is_existing_vendor", data?.is_existing_vendor)
        }
      />
      {(data.is_existing_vendor === 1 || data.is_existing_vendor === "1") && (
        <>
          <CustomDropdown
            id="drdo_category"
            label="14.1 Select the category under DRDO vendor registration"
            options={constants.drdoCategoryOptions}
            setValueData={setData}
            defaultValue={data.drdo_category}
            value={data.drdo_category}
            disableSourceForValue={true}
            source="label"
            name="drdo_category"
            registerName="drdo_category"
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("drdo_category", data?.drdo_category)}
          />
          <CustomMultiSelect
            options={constants.drdoMultiOptions}
            setValueData={setData}
            id="drdo_storage"
            label="14.2. Select the stores under DRDO vendor registration"
            source="title"
            name="drdo_storage"
            registerName="drdo_storage"
            value={data.drdo_storage}
            defaultValue={data.drdo_storage}
            isRequired={true}
            submit={isSubmit}
            setData={(data) => getData("drdo_storage", data?.drdo_storage)}
          />
          <CustomInput
            label="14.3 DRDO Registation Number"
            inputId="drdo_registration_number"
            placeholder="Enter DRDO Registration Number"
            setValueData={setData}
            defaultValue={data.drdo_registration_number}
            value={data.drdo_registration_number}
            name="drdo_registration_number"
            registerName="drdo_registration_number"
            isRequired={true}
            submit={isSubmit}
            setData={(data) =>
              getData(
                "drdo_registration_number",
                data?.drdo_registration_number
              )
            }
          />
          <CustomImageUploader
            file={data.vendor_certificate}
            value={data.vendor_certificate}
            setValue={setData}
            displayText="14.4 Upload DRDO Certificate"
            isLabel={true}
            name="vendor_certificate"
            typeText="Type: .jpg,.jpeg,.png,.pdf Max Size:5MB "
            accept=".jpg,.jpeg,.png,.pdf"
            required={true}
            docType="imagePdf"
          />
        </>
      )}

      {/* <CustomInput
        label="5. Type of Firm"
        inputId="enterprice_details"
        placeholder="Firm Type"
        setValueData={setData}
        defaultValue={data.enterprice_details}
        name="enterprice_details"
        registerName="enterprice_details"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("enterprice_details", data?.enterprice_details)
        }
      /> */}
      {/* <CustomDropdown
        id="enterprice_details"
        label="6. Type of Enterprise"
        options={constants.enterpriseType}
        placeholder="Select nature of enterprise"
        setValueData={setData}
        value={data.enterprice_details}
        defaultValue={data.enterprice_details}
        disableSourceForValue={true}
        source="label"
        name="enterprice_details"
        registerName="enterprice_details"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("enterprice_details", data?.enterprice_details)
        }
      /> */}

      {/* <CustomDropdown
        id="business_nature"
        label="8. Nature of Business"
        options={constants.natureOfBusinessArray}
        setValueData={setData}
        defaultValue={data.business_nature}
        value={data.business_nature}
        disableSourceForValue={true}
        source="label"
        name="business_nature"
        registerName="business_nature"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("business_nature", data?.business_nature)}
      /> */}
      {/* <CustomDropdown
        id="enterprice_type"
        label="9. Type of Classification"
        options={constants.typeOfEnterpriseArray}
        setValueData={setData}
        value={data.enterprice_type}
        defaultValue={data.enterprice_type}
        disableSourceForValue={true}
        source="label"
        name="enterprice_type"
        registerName="enterprice_type"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("enterprice_type", data?.enterprice_type)}
      /> */}

      {/* <CustomInput
        label="11. NCAGE Registration Number "
        inputId="ncage_number"
        hasLink
        linkText="Click here to get NCAGE registration number"
        href="https://ddpdos.gov.in/form/ncage-form"
        maxLength={10}
        setValueData={setData}
        value={data.ncage_number}
        defaultValue={data.ncage_number}
        name="ncage_number"
        registerName="ncage_number"
        isRequired={false}
        submit={isSubmit}
        setData={(data) => getData("ncage_number", data?.ncage_number)}
      /> */}
      {/* <CustomDropdown
        id="organisation_type"
        label="11. Type of Organisation"
        options={organisationTypeOptions}
        setValueData={setData}
        defaultValue={data.organisation_type}
        value={data.organisation_type}
        name="organisation_type"
        registerName="organisation_type"
        disableSourceForValue={true}
        source="title"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("organisation_type", data?.organisation_type)
        }
      /> */}

      {/* <CustomInput
        label="Website, if any"
        inputId="unit_web_url"
        placeholder="Enter Website"
        setValueData={setData}
        value={data.unit_web_url}
        defaultValue={data.unit_web_url}
        name="unit_web_url"
        registerName="unit_web_url"
        isRequired={false}
        labelRequired={false}
        submit={isSubmit}
        setData={(data) => getData("unit_web_url", data?.unit_web_url)}
      />

      <CustomRadioButton
        displayText="15. Category of Unit as per Central Pollution Control Board (CPCB) Classification"
        options={constants.unitCategories}
        setValueData={setData}
        value={data.cpcb_code}
        name="cpcb_code"
        registerName="cpcb_code"
        defaultValue={data.cpcb_code}
        submit={isSubmit}
        setData={(data) => getData("cpcb_code", data?.cpcb_code)}
      /> */}
      {/* <CustomInput
        label="15. Persons Employed"
        inputId="persons_employed"
        placeholder="Number of persons Employed"
        setValueData={setData}
        value={data.persons_employed}
        defaultValue={data.persons_employed}
        name="persons_employed"
        registerName="persons_employed"
        isRequired={true}
        submit={isSubmit}
        setData={(data) => getData("persons_employed", data?.persons_employed)}
      /> */}
      {/* <CustomRadioButton
        displayText="16. Is any owner, partner or employee, a foreigner? "
        options={constants.yesNo}
        setValueData={setData}
        defaultValue={data.is_foreign_partner && +data.is_foreign_partner}
        name="is_foreign_partner"
        registerName="is_foreign_partner"
        submit={isSubmit}
        setData={(data) =>
          getData("is_foreign_partner", data?.is_foreign_partner)
        }
      /> */}
      {/* <Box sx={customCommonStyles.marginBottomOne}>
        <QpTypography
          displayText="3. Are you an existing DRDO vendor?"
          styleData={{ ...commonStyles.label, ...styles.labelStyle }}
        />
        <QpRadioButton
          options={yesNo}
          name="is_existing_vendor"
          onChange={handleRadioButton}
          value={data.is_existing_vendor}
        />
      </Box> */}

      {/* <CustomRadioButton
        displayText="17. Supplier to Defense"
        options={constants.yesNo}
        setValueData={setData}
        defaultValue={data.supplier_to_defence && +data.supplier_to_defence}
        name="supplier_to_defence"
        registerName="supplier_to_defence"
        submit={isSubmit}
        setData={(data) =>
          getData("supplier_to_defence", data?.supplier_to_defence)
        }
      />
      <CustomInput
        label="18. Enter Jurisdiction of Police Station under which the premises fall"
        inputId="police_jurisdiction"
        placeholder="Enter Jurisdiction"
        setValueData={setData}
        value={data.police_jurisdiction}
        defaultValue={data.police_jurisdiction}
        name="police_jurisdiction"
        registerName="police_jurisdiction"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData("police_jurisdiction", data?.police_jurisdiction)
        }
      />
      <CustomInput
        label="19. Address of Police Headquarters"
        inputId="police_jurisdiction_address"
        placeholder="Enter address of Police Headquarters"
        maxLength={55}
        setValueData={setData}
        defaultValue={data.police_jurisdiction_address}
        value={data.police_jurisdiction_address}
        name="police_jurisdiction_address"
        registerName="police_jurisdiction_address"
        isRequired={true}
        submit={isSubmit}
        setData={(data) =>
          getData(
            "police_jurisdiction_address",
            data?.police_jurisdiction_address
          )
        }
      />
      <CustomCheckboxContent checked={checked} setChecked={setChecked} />*/}
      <SaveNextButtons
        blueButtonText="Save & Next"
        onNextClick={nextHandler}
        greyButtonRequired={false}
      />
    </QpFormContainer>
  );
};

export default GenerelInfo;
