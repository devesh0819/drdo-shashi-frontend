export const styles = {
  aboutUsText: {
    fontSize: "1.5rem",
    fontWeight: 700,
    marginBottom: "1rem",
  },
  paddingStyle: {
    padding: "2rem 5rem",
    backgroundColor: "white",
  },
  verticalBorderStyle: {
    border: "1px solid #F0F1F2",
  },
  styleData: {
    padding: "0",
    marginBottom: "1rem",
  },
};
