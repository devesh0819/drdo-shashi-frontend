import React, { useEffect } from "react";
import { Box } from "@mui/material";
import QpTypography from "../../Components/QpTypography/QpTypography";
import { styles } from "./CertifiedEnterprisesStyles";
import CustomTable from "../../Components/CustomTable/CustomTable";
import QpButton from "../../Components/QpButton/QpButton";
import { commonStyles, customCommonStyles } from "../../Styles/CommonStyles";
import { LOGIN } from "../../Routes/Routes";
import { useNavigate } from "react-router-dom";
import { getCertifiedEnterprises } from "../../Redux/FooterLinks/footerLinkActions";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import dayjs from "dayjs";

export default function CertifiedEnterprises() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const certifiedEnterprisesData = useSelector(
    (state) => state.footerLinksData.certifiedEnterprises
  );

  useEffect(() => {
    dispatch(getCertifiedEnterprises());
  }, [dispatch]);

  useEffect(() => {
    if (certifiedEnterprisesData) {
      setData(certifiedEnterprisesData);
    }
  }, [certifiedEnterprisesData]);

  // const certifiedEnterprisesColumns = [
  //   { field: "S.No.", width: "3%" },
  //   { field: "Name & Address", width: "55%" },
  //   { field: "Certification Level", width: "35%" },
  //   { field: "Valid Till", width: "7%" },
  // ];
  const certifiedEnterprisesColumns = [
    { field: "S.No.", width: 0 },
    { field: "Name & Address" },
    { field: "Certification Level" },
    { field: "Valid Till" },
  ];

  const dataSet =
    data?.length > 0
      ? data?.map((labelData, index) => {
          return {
            "S.No.": index + 1,
            "Name & Address": `Name - ${labelData?.enterprise_name}\nAddress - ${labelData?.unit_address} `,
            "Certification Level": labelData?.certification_level,
            "Valid Till":
              labelData?.expiry_date &&
              dayjs(labelData?.expiry_date).format("D MMM,YYYY"),
          };
        })
      : [];

  return (
    <>
      <Box sx={styles.paddingStyle}>
        <Box sx={commonStyles.displayStyle}>
          <QpTypography
            displayText="Certified Enterprises"
            styleData={styles.aboutUsText}
          />
          <Box sx={commonStyles.displayStyle}>
            <QpButton
              displayText="Print"
              styleData={{
                ...commonStyles.blueButton,
                ...customCommonStyles.marginRightTwo,
                ...commonStyles.tableButton,
              }}
              textStyle={{
                ...commonStyles.blueButtonText,
                ...commonStyles.tableButtonText,
              }}
              onClick={() => window.print()}
            />
            <QpButton
              displayText="Go to Login"
              styleData={{
                ...commonStyles.blueButton,
                ...commonStyles.tableButton,
              }}
              textStyle={{
                ...commonStyles.blueButtonText,
                ...commonStyles.tableButtonText,
              }}
              onClick={() => navigate(LOGIN)}
            />
          </Box>
        </Box>
        <CustomTable
          columnDefs={certifiedEnterprisesColumns}
          rowData={dataSet}
          styleData={styles.styleData}
          verticalBorderStyle={styles.verticalBorderStyle}
          hasSplit
        />
      </Box>
    </>
  );
}
