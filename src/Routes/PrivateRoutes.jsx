import React, { lazy } from "react";
import { ReactComponent as IconApplication } from "../Assets/Images/iconApplication.svg";
import { ReactComponent as IconBlackApplication } from "../Assets/Images/iconBlackApplication.svg";
import { ReactComponent as IconUserProfile } from "../Assets/Images/iconUserProfile.svg";
import { ReactComponent as IconPayment } from "../Assets/Images/iconPayment.svg";
import { ReactComponent as IconBluePayment } from "../Assets/Images/iconBluePayment.svg";

import { ReactComponent as IconLock } from "../Assets/Images/iconLock.svg";
import { ReactComponent as IconBlueLock } from "../Assets/Images/iconBlueLock.svg";

import { ReactComponent as IconSignOut } from "../Assets/Images/iconSignOut.svg";
import { ReactComponent as IconNewApplication } from "../Assets/Images/iconNewApplication.svg";
import { ReactComponent as IconBlueDashboard } from "../Assets/Images/iconBlueDashboard.svg";
import { ReactComponent as IconBlackDashboard } from "../Assets/Images/iconBlackDashboard.svg";

import { ReactComponent as IconBlackApplications } from "../Assets/Images/iconBlackApplications.svg";
import { ReactComponent as IconBlackAgencyAssessor } from "../Assets/Images/iconBlackAgencyAssessor.svg";
import { ReactComponent as IconBlueAgencyAssessor } from "../Assets/Images/iconBlueAgencyAssessor.svg";

import { ReactComponent as IconBlackCommittee } from "../Assets/Images/iconBlackCommittee.svg";
import { ReactComponent as IconBlueCommittee } from "../Assets/Images/iconBlueCommittee.svg";

import { ReactComponent as IconBlackManageAssessments } from "../Assets/Images/iconBlackManageAssessments.svg";
import { ReactComponent as IconBlueManageAssessments } from "../Assets/Images/iconBlueManageAssessments.svg";

import { ReactComponent as IconBlackQuestionairre } from "../Assets/Images/iconBlackQuestionairre.svg";
import { ReactComponent as IconBlueQuestionairre } from "../Assets/Images/iconBlueQuestionairre.svg";

import {
  DASHBOARD,
  ENTER_OTP,
  HOME,
  OTP_VERIFIED,
  GENERAL_INFO,
  PROCESS_DETAILS,
  OWNERSHIP_DETAILS,
  STEP2_DETAILS,
  PREVIEW,
  PAYMENT,
  PAYMENT_SUCCESSFUL,
  PAYMENT_FAILED,
  MANAGE_ASSESSMENTS,
  AGENCY_ASSESSOR,
  MANAGE_ASSESSMENTS_EDIT,
  COMMITTEE,
  MANAGE_QUESTIONAIRRE,
  ALL_APPLICATIONS,
  PAYMENT_HISTORY,
  CHANGE_PASSWORD,
  STEP3_DETAILS,
  SET_NEW_PASSWORD,
  APPLICATION_VIEW,
  DRDO_DASHBOARD,
  DRDO_ALL_APPLICATIONS,
  DRDO_ALL_APPLICATIONS_DETAIL,
  QUESTIONAIRRE_VIEW,
  QUESTIONS_EDIT,
  STEP1_DETAILS,
  ADMIN_APPLICATION_VIEW,
  ASSIGN_AGENCY,
  MEETING_VIEW,
  ASSESSOR_DASHBOARD,
  ASSESSOR_ALL_APPLICATIONS,
  ASSESOR_APPLICATION_DETAIL,
  ASSESOR_PARAMETER_DETAIL,
  ADMIN_PARAMETER_DETAIL,
  FEEDBACK_SUBMITTED,
  FEEDBACK,
  DRDO_PENDING_APPLICATIONS,
  ASSESSMENTS,
  ASSESSMENTS_EDIT,
  MY_PROFILE,
  PAYMENT_COMPLETE_DETAILS,
  FEE_CONFIGURATION,
  FEEDBACK_DETAILS,
  FEEDBACK_CONFIGURATION,
} from "./Routes";
import { ROLES } from "../Constant/RoleConstant";
import { getUserData, lazyRetry } from "../Services/localStorageService";

const DashboardView = lazy(() =>
  lazyRetry(() => import("../Containers/Dashboard/Dashboard"))
);
const HomeView = lazy(() => lazyRetry(() => import("../Containers/Home/Home")));
const ApplicationView = lazy(() =>
  lazyRetry(() => import("../Containers/Home/ApplicationView/ApplicationView"))
);
const FeedbackDetailsView = lazy(() =>
  lazyRetry(() => import("../Containers/FeedbackDetails/FeedbackDetails"))
);
const PaymentHistoryView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Applicant/PaymentHistory/PaymentHistory")
  )
);
const ChangePasswordView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Applicant/ChangePassword/ChangePassword")
  )
);
const MyProfileView = lazy(() =>
  lazyRetry(() => import("../Containers/Profile/Profile"))
);
const GeneralView = lazy(() =>
  lazyRetry(() => import("../Containers/Applicant/GeneralInfo/GeneralInfo"))
);

// const Step2FormView = lazy(() =>
//   lazyRetry(() =>
//     lazyRetry(() => import("../Containers/Applicant/Step2Form/Step2Form"))
//   )
// );
// const Step3FormView = lazy(() =>
//   lazyRetry(() => import("../Containers/Applicant/Step3Form/Step3Form"))
// );
const Step2FormView = lazy(() =>
  lazyRetry(() => import("../Containers/Applicant/VendorStep2/VendorStep2"))
);
const Preview = lazy(() =>
  lazyRetry(() => import("../Containers/Applicant/Preview/Preview"))
);
const PaymentView = lazy(() =>
  lazyRetry(() => import("../Containers/Payment/Payment"))
);
const PaymentSuccessfulView = lazy(() =>
  lazyRetry(() => import("../Containers/Payment/PaymentSuccessful"))
);
const PaymentFailedView = lazy(() =>
  lazyRetry(() => import("../Containers/Payment/PaymentFailed"))
);

const SignUpEnterOtpView = lazy(() =>
  lazyRetry(() => import("../Containers/Signup/SignUpEnterOtp/SignUpEnterOtp"))
);
const OtpVerifiedView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Signup/SignUpOtpVerified/SignUpOtpVerified")
  )
);
const AllApplicationsView = lazy(() =>
  lazyRetry(() => import("../Containers/Admin/AllApplications/AllApplications"))
);
const AdminApplicationView = lazy(() =>
  lazyRetry(() =>
    import(
      "../Containers/Admin/AllApplications/AdminApplicationTabView/AdminApplicationTabView"
    )
  )
);
const AssignAgencyView = lazy(() =>
  lazyRetry(() =>
    import(
      "../Containers/Admin/AllApplications/AdminApplicationTabView/AssignAgency/AssignAgency"
    )
  )
);

const ManageAssessmentsView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Admin/ManageAssessments/ManageAssessments")
  )
);
const CompleteAssessmentsView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Admin/CompleteAssessments/CompleteAssessments")
  )
);
const AgencyAssessorView = lazy(() =>
  lazyRetry(() => import("../Containers/Admin/AgencyAssessor/AgencyAssesssor"))
);
const ManageAssessmentsEditView = lazy(() =>
  lazyRetry(() =>
    import(
      "../Containers/Admin/ManageAssessments/ManageAssessmentsEdit/ManageAssessmentsEdit"
    )
  )
);
const CommitteeView = lazy(() =>
  lazyRetry(() => import("../Containers/Admin/Committee/Committee"))
);
const CommitteeEditView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Admin/Committee/CommitteeEdit/CommitteeEdit")
  )
);
const MeetingView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Admin/Committee/MeetingView/MeetingView")
  )
);
const ManageQuestionairreView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Admin/ManageQuestionairre/ManageQuestionairre")
  )
);
const FeedbackConfigurationView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Admin/FeedbackConfiguration/FeedbackConfiguration")
  )
);
const DrdoDashboardView = lazy(() =>
  lazyRetry(() => import("../Containers/DrdoAdmin/Dashboard/Dashboard"))
);
const DrdoAllApplicationsView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/DrdoAdmin/AllApplications/AllApplications")
  )
);
const DrdoAllApplicationsDetailView = lazy(() =>
  lazyRetry(() =>
    import(
      "../Containers/DrdoAdmin/AllApplicationsDetail/DrdoApplicationView.jsx"
    )
  )
);

const DrdoApplicationView = lazy(() =>
  lazyRetry(() =>
    import(
      "../Containers/DrdoAdmin/AllApplicationsDetail/DrdoApplicationView.jsx"
    )
  )
);
const QuestionairreView = lazy(() =>
  lazyRetry(() =>
    import(
      "../Containers/Admin/ManageQuestionairre/QuestionairreView/QuestionairreView.jsx"
    )
  )
);
const QuestionsEdit = lazy(() =>
  lazyRetry(() =>
    import(
      "../Containers/Admin/ManageQuestionairre/QuestionsEdit/QuestionsEdit.jsx"
    )
  )
);

const LeadAssessorAllApplications = lazy(() =>
  lazyRetry(() =>
    import("../Containers/LeadAssessor/ApplicationsList/ApplicationsList.jsx")
  )
);
const LeadAssessorApplicationView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/LeadAssessor/ApplicationDetail/ApplicationDetail.jsx")
  )
);

const AssessorParameterView = lazy(() =>
  lazyRetry(() =>
    import(
      "../Containers/LeadAssessor/AssessorParameterView/AssessorParameterView.jsx"
    )
  )
);
const AdminParameterView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Admin/AdminParameterView/AdminParameterView.jsx")
  )
);
const FeedbackSubmittedView = lazy(() =>
  lazyRetry(() => import("../Containers/Feedback/FeedbackSubmitted"))
);
const FeedbackView = lazy(() =>
  lazyRetry(() => import("../Containers/Feedback/Feedback"))
);

const PaymentCompleteDetailsView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Applicant/PaymentHistory/PaymentCompleteDetails")
  )
);

const FeeConfigurationView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Admin/FeeConfiguration/FeeConfiguration")
  )
);

export const privateRoutes = [
  {
    path: HOME,
    title: "All Applications",
    isSidebarNeeded: true,
    rolesAllowed: [ROLES.APPLICANT, ROLES.VENDOR],
    activeIcon: <IconApplication className="svgIcon" />,
    icon: <IconBlackApplication className="svgIcon" />,
    isHeaderNeeded: true,
    isFooterNeeded: false,
    children: [
      { path: "", component: <HomeView /> },
      {
        path: APPLICATION_VIEW,
        component: <ApplicationView />,
      },
      {
        path: FEEDBACK_DETAILS,
        component: <FeedbackDetailsView />,
      },
    ],
  },
  {
    path: DASHBOARD,
    component: <DashboardView />,
    title: "Dashboard",
    isSidebarNeeded: true,
    rolesAllowed: [ROLES.ADMIN],
    activeIcon: <IconBlueDashboard className="svgIcon" />,
    icon: <IconBlackDashboard className="svgIcon" />,
    isFooterNeeded: false,
    isHeaderNeeded: true,
  },
  {
    path: ENTER_OTP,
    component: <SignUpEnterOtpView />,
    isSidebarNeeded: false,
    isFooterNeeded: true,
    isHeaderNeeded: true,
  },
  {
    path: OTP_VERIFIED,
    component: <OtpVerifiedView />,
    isSidebarNeeded: false,
    isFooterNeeded: true,
    isHeaderNeeded: true,
  },

  {
    path: GENERAL_INFO,
    // component: <GeneralView />,
    isSidebarNeeded: false,
    title: "New Application/Re-Certification",
    icon: <IconNewApplication className="svgIcon" />,
    rolesAllowed: [ROLES.APPLICANT, ROLES.VENDOR],
    isFooterNeeded: false,
    isHeaderNeeded: true,
    children: [
      {
        path: "",
        component: <GeneralView />,
      },
      {
        path: STEP1_DETAILS,
        component: <GeneralView />,
      },
    ],
  },
  {
    path: PAYMENT_HISTORY,
    isSidebarNeeded: true,
    title: "Payment History",
    icon: <IconPayment className="svgIcon" />,
    activeIcon: <IconBluePayment className="svgIcon" />,
    rolesAllowed: [ROLES.APPLICANT, ROLES.VENDOR],
    isFooterNeeded: false,
    isHeaderNeeded: true,
    children: [
      {
        path: "",
        component: <PaymentHistoryView />,
      },
      {
        path: PAYMENT_COMPLETE_DETAILS,
        component: <PaymentCompleteDetailsView />,
      },
    ],
  },

  {
    path: STEP2_DETAILS,
    component: <Step2FormView />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: true,
  },
  // {
  //   path: STEP3_DETAILS,
  //   component: <Step3FormView />,
  //   isSidebarNeeded: false,
  //   isFooterNeeded: false,
  //   isHeaderNeeded: true,
  // },
  {
    path: PREVIEW,
    component: <Preview />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: true,
  },
  {
    path: PAYMENT,
    component: <PaymentView />,
    isSidebarNeeded: true,
    isFooterNeeded: false,
    isHeaderNeeded: true,
  },
  {
    path: PAYMENT_SUCCESSFUL,
    component: <PaymentSuccessfulView />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: true,
  },
  {
    path: PAYMENT_FAILED,
    component: <PaymentFailedView />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: true,
  },

  {
    path: DRDO_DASHBOARD,
    component: <DrdoDashboardView />,
    title: "Dashboard",
    isFooterNeeded: false,
    isSidebarNeeded: true,
    isHeaderNeeded: true,
    rolesAllowed: [ROLES.DRDO],
    icon: <IconBlackDashboard className="svgIcon" />,
    activeIcon: <IconBlueDashboard className="svgIcon" />,
  },
  {
    path: ALL_APPLICATIONS,
    isSidebarNeeded: true,
    isHeaderNeeded: true,
    title: "Applications",
    rolesAllowed: [ROLES.ADMIN, ROLES.DRDO],
    icon: <IconBlackApplications className="svgIcon" />,
    activeIcon: <IconApplication className="svgIcon" />,
    isFooterNeeded: false,
    children: [
      { path: "", component: <AllApplicationsView /> },
      {
        path: ADMIN_APPLICATION_VIEW,
        component: <AdminApplicationView />,
      },
      {
        path: ASSIGN_AGENCY,
        component: <AssignAgencyView />,
      },
      {
        path: PAYMENT_COMPLETE_DETAILS,
        component: <PaymentCompleteDetailsView />,
      },
    ],
  },
  {
    path: DRDO_ALL_APPLICATIONS,
    isSidebarNeeded: true,
    isFooterNeeded: false,
    title: "Pending Applications",
    rolesAllowed: [ROLES.DRDO],
    icon: <IconBlackApplications className="svgIcon" />,
    activeIcon: <IconApplication className="svgIcon" />,
    isHeaderNeeded: true,
    children: [
      { path: "", component: <DrdoAllApplicationsView /> },
      {
        path: DRDO_ALL_APPLICATIONS_DETAIL,
        component: <DrdoApplicationView />,
        // component: <ApplicationView />,
      },
    ],
  },
  // {
  //   path: DRDO_PENDING_APPLICATIONS,
  //   isSidebarNeeded: true,
  //   isFooterNeeded: false,
  //   title: "Pending Applications",
  //   rolesAllowed: [ROLES.DRDO],
  //   icon: <IconBlackApplications className="svgIcon" />,
  //   activeIcon: <IconApplication className="svgIcon" />,
  //   children: [
  //     { path: "", component: <DrdoAllApplicationsView /> },
  //     {
  //       path: DRDO_ALL_APPLICATIONS_DETAIL,
  //       component: <DrdoApplicationView />,
  //       // component: <ApplicationView />,
  //     },
  //   ],
  // },
  {
    path: MANAGE_ASSESSMENTS,
    isSidebarNeeded: true,
    isFooterNeeded: false,
    isHeaderNeeded: true,
    title: "Manage Assessments",
    rolesAllowed: [ROLES.ADMIN, ROLES.AGENCY, ROLES.DRDO],
    icon: <IconBlackManageAssessments className="svgIcon" />,
    activeIcon: <IconBlueManageAssessments className="svgIcon" />,
    children: [
      {
        path: "",
        component: <ManageAssessmentsView />,
      },
      {
        path: MANAGE_ASSESSMENTS_EDIT,
        component: <ManageAssessmentsEditView />,
      },
      {
        path: ADMIN_PARAMETER_DETAIL,
        component: <AdminParameterView />,
      },
    ],
  },
  {
    path: ASSESSMENTS,
    isSidebarNeeded: true,
    isFooterNeeded: false,
    isHeaderNeeded: true,
    title: "SAMAR Certified",
    rolesAllowed: [ROLES.ADMIN, ROLES.DRDO, ROLES.LAB_ADMIN],
    icon: <IconBlackManageAssessments className="svgIcon" />,
    activeIcon: <IconBlueManageAssessments className="svgIcon" />,
    children: [
      {
        path: "",
        component: <CompleteAssessmentsView />,
      },
      {
        path: ASSESSMENTS_EDIT,
        component: <ManageAssessmentsEditView />,
      },
      {
        path: ADMIN_PARAMETER_DETAIL,
        component: <AdminParameterView />,
      },
    ],
  },
  {
    path: AGENCY_ASSESSOR,
    component: <AgencyAssessorView />,
    isFooterNeeded: false,
    isHeaderNeeded: true,
    isSidebarNeeded: true,
    title: "Assessors",
    rolesAllowed: [ROLES.ADMIN, ROLES.DRDO],
    icon: <IconBlackAgencyAssessor className="svgIcon" />,
    activeIcon: <IconBlueAgencyAssessor className="svgIcon" />,
  },
  {
    path: COMMITTEE,
    isSidebarNeeded: true,
    isHeaderNeeded: true,
    isFooterNeeded: false,
    rolesAllowed: [ROLES.ADMIN, ROLES.DRDO],
    title: "Rating Committee",
    icon: <IconBlackCommittee className="svgIcon" />,
    activeIcon: <IconBlueCommittee className="svgIcon" />,
    children: [
      { path: "", component: <CommitteeView /> },
      {
        path: MANAGE_ASSESSMENTS_EDIT,
        component: <CommitteeEditView />,
      },
      {
        path: MEETING_VIEW,
        component: <MeetingView />,
      },
    ],
  },
  {
    path: MANAGE_QUESTIONAIRRE,
    // component: <ManageQuestionairreView />,
    isSidebarNeeded: true,
    isHeaderNeeded: true,
    isFooterNeeded: false,
    title: "Manage Assessment Criteria",
    rolesAllowed: [ROLES.ADMIN, ROLES.DRDO],
    icon: <IconBlackQuestionairre className="svgIcon" />,
    activeIcon: <IconBlueQuestionairre className="svgIcon" />,

    children: [
      { path: "", component: <ManageQuestionairreView /> },
      {
        path: QUESTIONAIRRE_VIEW,
        component: <QuestionairreView />,
      },
      {
        path: QUESTIONS_EDIT,
        component: <QuestionsEdit />,
      },
    ],
  },

  {
    path: ASSESSOR_ALL_APPLICATIONS,
    isSidebarNeeded: true,
    isHeaderNeeded: true,
    isFooterNeeded: false,
    title: "All Assessments",
    rolesAllowed: [ROLES.ASSESSOR],
    // icon: <IconBlackApplications className="svgIcon" />,
    icon: <IconBlackApplications className="svgIcon" />,
    activeIcon: <IconApplication className="svgIcon" />,
    children: [
      { path: "", component: <LeadAssessorAllApplications /> },
      {
        path: ASSESOR_APPLICATION_DETAIL,
        component: <LeadAssessorApplicationView />,
      },
      {
        path: ASSESOR_PARAMETER_DETAIL,
        component: <AssessorParameterView />,
      },
    ],
  },
  {
    path: MY_PROFILE,
    component: <MyProfileView />,
    isSidebarNeeded: true,
    isFooterNeeded: false,
    isHeaderNeeded: true,
    title: "My Profile",
    icon: <IconLock className="svgIcon" />,
    activeIcon: <IconBlueLock className="svgIcon" />,
    rolesAllowed: [ROLES.APPLICANT, ROLES.VENDOR],
  },
  {
    path: FEE_CONFIGURATION,
    component: <FeeConfigurationView />,
    isSidebarNeeded: true,
    isFooterNeeded: false,
    isHeaderNeeded: true,
    title: "Fee Configuration",
    icon: <IconLock className="svgIcon" />,
    activeIcon: <IconBlueLock className="svgIcon" />,
    rolesAllowed: [ROLES.ADMIN, ROLES.DRDO],
  },
  {
    path: FEEDBACK_CONFIGURATION,
    component: <FeedbackConfigurationView />,
    isSidebarNeeded: true,
    isFooterNeeded: false,
    isHeaderNeeded: true,
    title: "Feedback Configuration",
    icon: <IconLock className="svgIcon" />,
    activeIcon: <IconBlueLock className="svgIcon" />,
    rolesAllowed: [ROLES.ADMIN, ROLES.DRDO],
  },
  {
    path: CHANGE_PASSWORD,
    component: <ChangePasswordView />,
    isSidebarNeeded: true,
    isFooterNeeded: false,
    isHeaderNeeded: true,
    title: "Change Password",
    icon: <IconLock className="svgIcon" />,
    activeIcon: <IconBlueLock className="svgIcon" />,
    rolesAllowed: [
      ROLES.APPLICANT,
      ROLES.VENDOR,
      ROLES.AGENCY,
      ROLES.DRDO,
      ROLES.ASSESSOR,
      ROLES.ADMIN,
      ROLES.LAB_ADMIN,
    ],
  },
  {
    path: FEEDBACK,
    component: <FeedbackView />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: true,
    // rolesAllowed: [ROLES.VENDOR, ROLES.APPLICANT],
  },
  {
    path: FEEDBACK_SUBMITTED,
    component: <FeedbackSubmittedView />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: true,
    // rolesAllowed: [ROLES.VENDOR, ROLES.APPLICANT],
  },
];

const getPrivateRoutes = () => {
  const userData = getUserData();
  const role = userData?.roles[0];
  if (userData) {
    const routes = privateRoutes.filter((route) => {
      return route?.rolesAllowed?.includes(role);
    });
    return routes;
  } else {
    return [];
  }
};
export default getPrivateRoutes;
