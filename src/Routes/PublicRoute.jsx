import React from "react";
import {
  Navigate,
  Outlet,
  useLocation,
  useSearchParams,
} from "react-router-dom";
import { getToken } from "../Auth/getToken";
import { ROLES } from "../Constant/RoleConstant";
import { getUserData } from "../Services/localStorageService";
import {
  ASSESSOR_ALL_APPLICATIONS,
  CERTIFIED_ENTERPRISES,
  DASHBOARD,
  ENTER_OTP,
  HOME,
  LOGIN,
  MANAGE_ASSESSMENTS,
  SIGN_UP,
  ASSESSMENTS,
} from "./Routes";

const PublicRoute = () => {
  const isUserLoggedIn = getToken();
  const location = useLocation();
  const userData = getUserData();
  const role = userData?.roles[0];
  if (location.pathname === ENTER_OTP) {
    return <Navigate to={SIGN_UP} />;
  }

  if (isUserLoggedIn) {
    if (role === ROLES.AGENCY) {
      return <Navigate to={MANAGE_ASSESSMENTS} />;
    }
    if (role === ROLES.ADMIN) {
      return <Navigate to={DASHBOARD} />;
    }
    if (role === ROLES.DRDO) {
      return <Navigate to={DASHBOARD} />;
    }
    if (role === ROLES.VENDOR) {
      return <Navigate to={HOME} />;
    }
    if (role === ROLES.ASSESSOR) {
      return <Navigate to={ASSESSOR_ALL_APPLICATIONS} />;
    }
    if (role === ROLES.LAB_ADMIN) {
      return <Navigate to={ASSESSMENTS} />;
    }
  } else {
    return <Outlet />;
  }
};

export default PublicRoute;
