import React from "react";
import {
  Navigate,
  Outlet,
  useLocation,
  useSearchParams,
} from "react-router-dom";
import { getToken } from "../Auth/getToken";
import { ABOUT_US, ENTER_OTP, FEEDBACK, HOME, LOGIN } from "./Routes";

const PrivateRoute = () => {
  const isUserLoggedIn = getToken();
  const [searchParams] = useSearchParams();
  const location = useLocation();
  const application_id = searchParams.get("application_id");

  if (application_id) {
    if (!isUserLoggedIn) {
      return (
        <Navigate
          to={`${LOGIN}?url=${process.env.REACT_APP_WEB_URL}feedback?application_id=${application_id}`}
        />
      );
    } else {
      return <Outlet />;
    }
  }

  return isUserLoggedIn ? <Outlet /> : <Navigate to={ABOUT_US} />;
};

export default PrivateRoute;
