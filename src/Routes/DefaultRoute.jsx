import React from "react";
import { Navigate } from "react-router-dom";
import { getToken } from "../Auth/getToken";
import { ROLES } from "../Constant/RoleConstant";
import { getUserData } from "../Services/localStorageService";
import {
  ASSESSOR_ALL_APPLICATIONS,
  DASHBOARD,
  HOME,
  LOGIN,
  MANAGE_ASSESSMENTS,
} from "./Routes";

const DefaultRoute = () => {
  const checkToken = getToken();
  const userData = getUserData();
  const role = userData?.roles[0];

  if (checkToken) {
    // return <Navigate to={NOT_FOUND} />;
    if (role === ROLES.AGENCY) {
      return <Navigate to={MANAGE_ASSESSMENTS} />;
    }
    if (role === ROLES.ADMIN) {
      return <Navigate to={DASHBOARD} />;
    }
    if (role === ROLES.DRDO) {
      return <Navigate to={DASHBOARD} />;
    }
    if (role === ROLES.VENDOR) {
      return <Navigate to={HOME} />;
    }
    if (role === ROLES.ASSESSOR) {
      return <Navigate to={ASSESSOR_ALL_APPLICATIONS} />;
    }
  } else {
    return <Navigate to={LOGIN} />;
  }
};

export default DefaultRoute;
