import React, { lazy } from "react";
import { lazyRetry } from "../Services/localStorageService";

import {
  FORGOT_PASSWORD,
  LOGIN,
  RESEND_LINK,
  SET_NEW_PASSWORD,
  SIGN_UP,
  PASSWORD_CHANGED,
  ABOUT_US,
  STANDARD,
  CERTIFIED_ENTERPRISES,
  PRIVACY_POLICY,
} from "./Routes";

const SignUpView = lazy(() =>
  lazyRetry(() => import("../Containers/Signup/Signup"))
);

const LoginForgotPasswordView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/Login/LoginForgotPassword/LoginForgotPassword")
  )
);

const ResendLinkView = lazy(() =>
  lazyRetry(() => import("../Containers/Login/ResendLink/ResendLink"))
);

const PasswordChangedView = lazy(() =>
  lazyRetry(() => import("../Containers/Login/PasswordChanged/PasswordChanged"))
);
const SetNewPasswordView = lazy(() =>
  lazyRetry(() => import("../Containers/Login/SetNewPassword/SetNewPassword"))
);
const AboutUsView = lazy(() =>
  lazyRetry(() => import("../Containers/AboutUs/AboutUs"))
);
const CertifiedEnterprisesView = lazy(() =>
  lazyRetry(() =>
    import("../Containers/CertifiedEnterprises/CertifiedEnterprises")
  )
);
const StandardView = lazy(() =>
  lazyRetry(() => import("../Containers/Standard/Standard"))
);
const PrivacyPolicyView = lazy(() =>
  lazyRetry(() => import("../Containers/PrivacyPolicy/PrivacyPolicy"))
);

export const publicRoutes = [
  {
    path: ABOUT_US,
    component: <AboutUsView />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: false,
  },
  {
    path: SIGN_UP,
    component: <SignUpView />,
    isSidebarNeeded: false,
    isFooterNeeded: true,
    isHeaderNeeded: true,
  },

  {
    path: LOGIN,
    component: <SignUpView />,
    isSidebarNeeded: false,
    isFooterNeeded: true,
    isHeaderNeeded: true,
  },
  {
    path: FORGOT_PASSWORD,
    component: <LoginForgotPasswordView />,
    isSidebarNeeded: false,
    isFooterNeeded: true,
    isHeaderNeeded: true,
  },
  {
    path: RESEND_LINK,
    component: <ResendLinkView />,
    isSidebarNeeded: false,
    isFooterNeeded: true,
    isHeaderNeeded: true,
  },

  {
    path: PASSWORD_CHANGED,
    component: <PasswordChangedView />,
    isSidebarNeeded: false,
    isFooterNeeded: true,
    isHeaderNeeded: true,
  },
  {
    path: SET_NEW_PASSWORD,
    component: <SetNewPasswordView />,
    isSidebarNeeded: false,
    isFooterNeeded: true,
    isHeaderNeeded: true,
  },

  {
    path: CERTIFIED_ENTERPRISES,
    component: <CertifiedEnterprisesView />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: true,
  },
  {
    path: STANDARD,
    component: <StandardView />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: true,
  },
  {
    path: PRIVACY_POLICY,
    component: <PrivacyPolicyView />,
    isSidebarNeeded: false,
    isFooterNeeded: false,
    isHeaderNeeded: true,
  },
];
