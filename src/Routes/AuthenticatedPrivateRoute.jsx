import React from "react";
import { Navigate, Outlet } from "react-router-dom";
import { ABOUT_US, SIGN_UP } from "./Routes";

const AuthenticatedPrivateRoute = (props) => {
  const isAuthenticated = localStorage.getItem("token");
  if (!isAuthenticated) {
    return <Navigate to={ABOUT_US} />;
  } else {
    return <Outlet />;
  }
};

export default AuthenticatedPrivateRoute;
