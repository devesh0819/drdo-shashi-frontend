import * as Yup from "yup";
import { NO_SPACE_AT_START_END } from "../Constant/AppConstant";

export const loginSchema = Yup.object().shape({
  email: Yup.string()
    .required("This field is required")
    .matches(NO_SPACE_AT_START_END, "This field is invalid"),
  password: Yup.string().required("This field is required"),
  captcha: Yup.string().required("This field is required"),
});
