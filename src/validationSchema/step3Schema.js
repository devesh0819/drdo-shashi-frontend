import * as Yup from "yup";
import {
  NO_SPACE_AT_START_END,
  ONLY_CHARACTERS,
  ONLY_VALID_NUMERIC,
} from "../Constant/AppConstant";

export const customerSchema = Yup.object().shape({
  customerName: Yup.string()
    .required("This field is required")
    .min(3, "This field must be 3 to 55 characters long")
    .max(55, "This field must be 3 to 55 characters long")
    .matches(ONLY_CHARACTERS, "This field is invalid"),
  customerType: Yup.string().required("This field is required"),
  customerCountry: Yup.string().when("customerType", {
    is: "0",
    then: Yup.string().required("This field is required"),
    otherwise: Yup.string().notRequired(),
  }),
});

export const vendorSchema = Yup.object().shape({
  name: Yup.string()
    .required("This field is required")
    .min(3, "This field must be 3 to 55 characters long")
    .max(55, "This field must be 3 to 55 characters long")
    .matches(ONLY_CHARACTERS, "This field is invalid"),
  address: Yup.string()
    .required("This field is required")
    .min(3, "This field must be 3 to 100 characters long")
    .max(100, "This field must be 3 to 100 characters long"),
});

export const rawMaterialSchema = Yup.object().shape({
  rawMaterialName: Yup.string()
    .required("This field is required")
    .min(3, "This field must be 3 to 55 characters long")
    .max(55, "This field must be 3 to 55 characters long")
    .matches(ONLY_CHARACTERS, "This field is invalid"),
  rawMaterialImported: Yup.string().required("This field is required"),
  whetherRawMaterial: Yup.string().required("This field is required"),
  rawMaterialPercentageImported: Yup.string().when("rawMaterialImported", {
    is: "1",
    then: Yup.string()
      .required("This field is required")
      .test(
        "type",
        "This field must be a number between 1 to 100",
        function (val) {
          if (val) {
            return parseInt(val) >= 1 &&
              parseInt(val) <= 100 &&
              Number.isInteger(+val) &&
              val.match(ONLY_VALID_NUMERIC)
              ? true
              : false;
          }
          return true;
        }
      ),
    otherwise: Yup.string().notRequired(),
  }),

  rawMaterialCountryName: Yup.string().when("rawMaterialImported", {
    is: "1",
    then: Yup.string()
      .required("This field is required")
      .test(
        "length",
        "This field should be 5 to 55 characters long",
        function (value) {
          if (value) {
            return value.length >= 5 &&
              value.length <= 55 &&
              value.match(ONLY_CHARACTERS)
              ? true
              : false;
          }
          return true;
        }
      ),
    otherwise: Yup.string().notRequired(),
  }),
  // .min(3, "This field must be 3 to 55 characters long")
  // .max(55, "This field must be 3 to 55 characters long")
  // .matches(ONLY_CHARACTERS, "This field is invalid"),
  // rawMaterialSourceOfProcurement: Yup.string()
  //   // .required("This field is required")
  //   .notRequired()
  //   .test(
  //     "length",
  //     "This field should be 5 to 55 characters long",
  //     function (value) {
  //       if (value) {
  //         return value.length >= 5 &&
  //           value.length <= 55 &&
  //           value.match(ONLY_CHARACTERS)
  //           ? true
  //           : false;
  //       }
  //       return true;
  //     }
  //   ),
  // .min(3, "This field must be 3 to 55 characters long")
  // .max(55, "This field must be 3 to 55 characters long"),
});

export const partsSchema = Yup.object().shape({
  partsName: Yup.string()
    .required("This field is required")
    .min(3, "This field must be 3 to 55 characters long")
    .max(55, "This field must be 3 to 55 characters long")
    .matches(ONLY_CHARACTERS, "This field is invalid"),
  partsPercentageImported: Yup.string().test(
    "type",
    "This field must be a number between 1 to 100",
    function (val) {
      if (val) {
        return parseInt(val) >= 1 &&
          parseInt(val) <= 100 &&
          Number.isInteger(+val) &&
          val.match(ONLY_VALID_NUMERIC)
          ? true
          : false;
      }
      return true;
    }
  ),
  partsCountryName: Yup.string()
    // .required("This field is required")
    .notRequired()
    .test(
      "length",
      "This field should be 5 to 55 characters long",
      function (value) {
        if (value) {
          return value.length >= 5 &&
            value.length <= 55 &&
            value.match(ONLY_CHARACTERS)
            ? true
            : false;
        }
        return true;
      }
    ),
  partsSourceOfProcurement: Yup.string()
    // .required("This field is required")
    .notRequired()
    .test(
      "length",
      "This field should be 5 to 55 characters long",
      function (value) {
        if (value) {
          return value.length >= 5 &&
            value.length <= 55 &&
            value.match(ONLY_CHARACTERS)
            ? true
            : false;
        }
        return true;
      }
    ),
  // .min(3, "This field must be 3 to 55 characters long")
  // .max(55, "This field must be 3 to 55 characters long"),
});

export const productSchema = Yup.object().shape({
  productName: Yup.string()
    .required("This field is required")
    .min(3, "This field must be 3 to 55 characters long")
    .max(55, "This field must be 3 to 55 characters long")
    .matches(ONLY_CHARACTERS, "This field is invalid"),

  isProductExported: Yup.string().required("This field is required"),
  totalCapacityUnit: Yup.string().required("This field is required"),
  presentCapacityUnit: Yup.string().required("This field is required"),

  destinationCountries: Yup.array().when("isProductExported", {
    is: "1",
    then: Yup.array()
      .min(1, "This field must have atleast 1 country")
      .test("length", "This field is required2", function (values) {
        if (values?.length === 0) {
          return true;
        }
        if (values?.length >= 1) {
          return true;
        }
      }),
    otherwise: Yup.array().notRequired(),
  }),
  percentageExportProduct: Yup.string().when("isProductExported", {
    is: "1",
    then: Yup.string().test(
      "type",
      "This field must be a number between 1 to 100",
      function (val) {
        return parseInt(val) >= 1 &&
          parseInt(val) <= 100 &&
          Number.isInteger(+val) &&
          val.match(ONLY_VALID_NUMERIC)
          ? true
          : false;
      }
    ),
    otherwise: Yup.string().notRequired(),
  }),

  totalProductionCapacityPerYear: Yup.string()
    .required("This field is required")
    .test("type", "This field must be a number", function (val) {
      return parseInt(val) >= 1 &&
        Number.isInteger(+val) &&
        val.match(ONLY_VALID_NUMERIC)
        ? true
        : false;
    })
    .test(
      "length",
      "This field should be 1 to 25 digits long",
      function (value) {
        if (value) {
          return value.length >= 1 && value.length <= 25 ? true : false;
        }
        return true;
      }
    ),
  presentProductionCapacityPerYear: Yup.string()
    .required("This field is required")
    .test("type", "This field must be a number", function (val) {
      return parseInt(val) >= 1 &&
        Number.isInteger(+val) &&
        val.match(ONLY_VALID_NUMERIC)
        ? true
        : false;
    })
    .test(
      "length",
      "This field should be 1 to 25 digits long",
      function (value) {
        if (value) {
          return value.length >= 1 && value.length <= 25 ? true : false;
        }
        return true;
      }
    ),
  // totalProductionCapacityPerMonth: Yup.string()
  //   .required("This field is required")
  //   .test("type", "This field must be a number", function (val) {
  //     return parseInt(val) >= 1 &&
  //       Number.isInteger(+val) &&
  //       val.match(ONLY_VALID_NUMERIC)
  //       ? true
  //       : false;
  //   })
  //   .test(
  //     "length",
  //     "This field should be 1 to 25 digits long",
  //     function (value) {
  //       if (value) {
  //         return value.length >= 1 && value.length <= 25 ? true : false;
  //       }
  //       return true;
  //     }
  //   ),
  // presentProductionCapacityPerMonth: Yup.string()
  //   .required("This field is required")
  //   .test("type", "This field must be a number", function (val) {
  //     return parseInt(val) >= 1 &&
  //       Number.isInteger(+val) &&
  //       val.match(ONLY_VALID_NUMERIC)
  //       ? true
  //       : false;
  //   })
  //   .test(
  //     "length",
  //     "This field should be 1 to 25 digits long",
  //     function (value) {
  //       if (value) {
  //         return value.length >= 1 && value.length <= 25 ? true : false;
  //       }
  //       return true;
  //     }
  //   ),
  // spareCapacity: Yup.string()
  //   .required("This field is required")
  //   .test("type", "This field must be a number", function (val) {
  //     return parseInt(val) >= 1 &&
  //       Number.isInteger(+val) &&
  //       val.match(ONLY_VALID_NUMERIC)
  //       ? true
  //       : false;
  //   })
  //   .test(
  //     "length",
  //     "This field should be 1 to 25 digits long",
  //     function (value) {
  //       if (value) {
  //         return value.length >= 1 && value.length <= 25 ? true : false;
  //       }
  //       return true;
  //     }
  //   ),
  // costAudit: Yup.string().required("This field is required"),
  dateOfCommencement: Yup.string().required("This field is required"),
  // governmentRegulatory: Yup.string().required("This field is required"),
  // defectLevel: Yup.string().test(
  //   "type",
  //   "This field must be a number between 1 to 100",
  //   function (val) {
  //     return parseInt(val) >= 1 &&
  //       parseInt(val) <= 100 &&
  //       Number.isInteger(+val) &&
  //       val.match(ONLY_VALID_NUMERIC)
  //       ? true
  //       : false;
  //   }
  // ),
});

// export const testingSchema = Yup.object().shape({
//   testName: Yup.string()
//     .required("This field is required")
//     .min(3, "This field must be 3 to 55 characters long")
//     .max(55, "This field must be 3 to 55 characters long")
//     .matches(ONLY_CHARACTERS, "This field is invalid"),
// });

export const testingSchema = Yup.object().shape({
  testName: Yup.string()
    .required("This field is required")
    .min(3, "This field must be 3 to 55 characters long")
    .max(55, "This field must be 3 to 55 characters long")
    .matches(ONLY_CHARACTERS, "This field is invalid"),
  agencyTestName: Yup.string().test(
    "length",
    "This field must be 3 to 55 characters long",
    function (val) {
      if (val) {
        return val?.length >= 3 &&
          val?.length <= 55 &&
          val.match(ONLY_CHARACTERS)
          ? true
          : false;
      }
      return true;
    }
  ),
  agencyTestAddress: Yup.string().test(
    "length",
    "This field must be 3 to 55 characters long",
    function (val) {
      if (val) {
        return val?.length >= 3 && val?.length <= 55 ? true : false;
      }
      return true;
    }
  ),
});
