import * as Yup from "yup";
import {
  AADHAR_REGEX,
  DECIMAL_VALUES,
  EMAIL_REGEX,
  GST_REGEX,
  IFSC_REGEX,
  LANDLINE_REGEX,
  LENGTH_REGEX,
  MOBILE_REGEX,
  NO_SPACE_AT_START_END,
  ONLY_CHARACTERS,
  ONLY_NUMERIC,
  ONLY_VALID_NUMERIC,
  ONLY_VALID_NUMERIC_WITH_ZERO,
  PINCODE_REGEX,
  UDYAM_REGEX,
  WEB_URL_REGEX,
  WITHOUT_SPACE,
  WITHOUT_SPECIAL_CHARACTERS,
} from "../Constant/AppConstant";

export const customInputSchema = (registerName, isRequired) => {
  if (
    registerName === "acc_loss_first_year" ||
    registerName === "acc_loss_second_year" ||
    registerName === "acc_loss_third_year" ||
    registerName === "financial_capital_outlay" ||
    registerName === "financial_source" ||
    registerName === "profit_first_year" ||
    registerName === "profit_second_year" ||
    registerName === "profit_third_year" ||
    registerName === "turnover_first_year" ||
    registerName === "turnover_second_year" ||
    registerName === "turnover_third_year" ||
    registerName === "financial_capital_outlay_first_year" ||
    registerName === "financial_capital_outlay_second_year" ||
    registerName === "financial_capital_outlay_third_year"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .notRequired()
        .nullable()
        .test("type", "This field must be a number", function (val) {
          if (val) {
            return parseInt(val) > 0 &&
              Number.isInteger(+val) &&
              val.match(ONLY_VALID_NUMERIC)
              ? true
              : false;
          }
          return true;
        })
        .test(
          "length",
          "This field should be 5 to 25 digits long",
          function (value) {
            if (value) {
              return value.length >= 5 && value.length <= 25 ? true : false;
            }
            return true;
          }
        ),

      // .min(5, "This field should be 5 to 55 characters long")
      // .max(55, "This field should be 5 to 55 characters long")
    });
  }
  if (registerName === "admin_skill_set" || registerName === "tech_skill_set") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .notRequired()
        .nullable()
        .test(
          "length",
          "This field should be 5 to 55 characters long",
          function (value) {
            if (value) {
              return value.length >= 5 && value.length <= 55 ? true : false;
            }
            return true;
          }
        ),
      // .min(5, "This field should be 5 to 55 characters long")
      // .max(55, "This field should be 5 to 55 characters long")
    });
  }
  if (
    registerName === "productName" ||
    registerName === "customerName" ||
    registerName === "rawMaterialName" ||
    registerName === "partsName" ||
    registerName === "name" ||
    registerName === "rawMaterialCountryName" ||
    registerName === "rawMaterialSourceOfProcurement" ||
    registerName === "partsCountryName" ||
    registerName === "partsSourceOfProcurement"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .min(3, "This field should be 3 to 55 characters long")
        .max(55, "This field should be 3 to 55 characters long"),
    });
  }

  if (
    registerName === "rawMaterialPercentageImported" ||
    registerName === "partsPercentageImported"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string().test(
        "type",
        "This field must be a number between 1 to 100",
        function (val) {
          return parseInt(val) >= 1 &&
            parseInt(val) <= 100 &&
            Number.isInteger(+val) &&
            val.match(ONLY_VALID_NUMERIC)
            ? true
            : false;
        }
      ),
    });
  }

  if (
    registerName === "work_area_covered" ||
    registerName === "work_area_bond_rooms" ||
    registerName === "work_area_num_bond_rooms" ||
    registerName === "work_area_uncovered" ||
    registerName === "work_prod_area" ||
    registerName === "work_testing_area" ||
    registerName === "totalProductionCapacityPerYear" ||
    registerName === "presentProductionCapacityPerYear" ||
    registerName === "totalProductionCapacityPerMonth" ||
    registerName === "presentProductionCapacityPerMonth" ||
    registerName === "spareCapacity"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .notRequired()
        .nullable()
        .test("type", "This field must be a number", function (val) {
          if (val) {
            return parseInt(val) > 0 &&
              Number.isInteger(+val) &&
              val.match(ONLY_VALID_NUMERIC)
              ? true
              : false;
          }
          return true;
        })
        .test(
          "length",
          "This field should be 1 to 10 digits long ",
          function (value) {
            if (value) {
              return value.length >= 1 && value.length <= 10 ? true : false;
            }
            return true;
          }
        ),
    });
  }

  if (
    // registerName === "admin_manpower_count" ||
    registerName === "tech_skilled_count" ||
    registerName === "tech_unskill_count"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .test("type", "This field must be a number", function (val) {
          if (val) {
            return parseInt(val) > 0 &&
              Number.isInteger(+val) &&
              val.match(ONLY_VALID_NUMERIC)
              ? true
              : false;
          }
          return true;
        })
        .test(
          "length",
          "This field should be 1 to 10 digits long ",
          function (value) {
            if (value) {
              return value.length >= 1 && value.length <= 10 ? true : false;
            }
            return true;
          }
        ),
    });
  }
  if (registerName === "admin_manpower_count") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        // .required("This field is required")
        .notRequired()
        .test("type", "This field must be a number", function (val) {
          if (val) {
            return parseInt(val) > 0 &&
              Number.isInteger(+val) &&
              val.match(ONLY_VALID_NUMERIC)
              ? true
              : false;
          }
          return true;
        })
        .test(
          "length",
          "This field should be 1 to 10 digits long ",
          function (value) {
            if (value) {
              return value.length >= 1 && value.length <= 10 ? true : false;
            }
            return true;
          }
        ),
    });
  }

  if (registerName === "dateOfCommencement") {
    return Yup.object().shape({
      [registerName]: Yup.string().required(`This field is required`),
    });
  }

  // if (
  //   registerName === "costAudit" ||
  //   registerName === "governmentRegulatory" ||
  //   registerName === "licnenseDetail"
  // ) {
  //   return Yup.object().shape({
  //     [registerName]: Yup.string().required(`This field is required`),
  //   });
  // }

  if (registerName === "customerAddress" || registerName === "address") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .min(3, "This field should be 3 to 100 characters long")
        .max(100, "This field should be 3 to 100 characters long"),
    });
  }

  if (
    registerName === "entrepreneur_name" ||
    registerName === "enterprise_name" ||
    registerName === "bank_name" ||
    registerName === "poc_name" ||
    registerName === "poc_designation"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .min(2, "This field should be 2 to 100 characters long")
        .max(100, "This field should be 2 to 100 characters long")
        .matches(ONLY_CHARACTERS, "This field is invalid"),
    });
  }

  if (registerName === "bank_account_number") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .min(2, "This field should be 2 to 18 characters long")
        .max(18, "This field should be 2 to 18 characters long")
        .matches(ONLY_VALID_NUMERIC_WITH_ZERO, "This field is invalid"),
    });
  }

  if (
    registerName === "registered_address" ||
    registerName === "police_jurisdiction_address" ||
    registerName === "police_jurisdiction" ||
    registerName === "unit_address" ||
    registerName === "bank_branch_name" ||
    registerName === "bank_full_address" ||
    registerName === "enterprice_details"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .matches(WITHOUT_SPECIAL_CHARACTERS, "This field is invalid")
        .min(5, "This field should be 5 to 55 characters long")
        .max(55, "This field should be 5 to 55 characters long"),
    });
  }

  if (registerName === "bank_city") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .min(4, "This field should be 4 to 55 characters long")
        .max(55, "This field should be 4 to 55 characters long")
        .matches(ONLY_CHARACTERS, "This field is invalid"),
    });
  }

  if (registerName === "bank_ifsc_code") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .matches(IFSC_REGEX, "This field is invalid"),
    });
  }

  if (registerName === "unit_web_url") {
    return Yup.object().shape({
      [registerName]: Yup.string().matches(WEB_URL_REGEX, {
        message: "This field is invalid",
        excludeEmptyString: true,
      }),
    });
  }

  if (registerName === "registered_pin" || registerName === "unit_pin") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .matches(PINCODE_REGEX, "This field is invalid"),
    });
  }

  if (
    registerName === "registered_mobile_number" ||
    registerName === "unit_mobile_number" ||
    registerName === "poc_mobile_number"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .matches(MOBILE_REGEX, "This field is invalid"),
    });
  }

  if (
    registerName === "registered_landline_number" ||
    registerName === "unit_landline_number"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .matches(LANDLINE_REGEX, {
          message: "This field is invalid",
          excludeEmptyString: true,
        })
        .nullable(),
    });
  }

  if (registerName === "entrepreneur_aadhar_number") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .matches(AADHAR_REGEX, "This field is invalid"),
    });
  }
  if (registerName === "entrepreneur_gst_number") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .matches(GST_REGEX, "This field is invalid"),
    });
  }

  if (registerName === "tan_number") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .length(10, "This field must be 10 characters long"),
    });
  }

  if (registerName === "persons_employed") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .test("type", "This field must be a number", function (val) {
          return parseInt(val) > 0 &&
            Number.isInteger(+val) &&
            val.match(ONLY_VALID_NUMERIC)
            ? true
            : false;
        }),
    });
  }

  if (registerName === "years_of_experience") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .test(
          "type",
          "This field must be a number from 1 to 100",
          function (val) {
            return parseInt(val) >= 1 &&
              parseInt(val) <= 100 &&
              Number.isInteger(+val) &&
              val.match(ONLY_VALID_NUMERIC)
              ? true
              : false;
          }
        ),
    });
  }

  if (registerName === "first_name") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .test("value", "This field must be atleast 2 characters", (val) => {
          if (!!val) {
            const fieldValue = Yup.string().trim().min(2);

            return fieldValue.isValidSync(val);
          }
          return true;
        })
        .matches(ONLY_CHARACTERS, "This field is invalid"),
    });
  }
  if (registerName === "last_name") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .test("value", "This field must be atleast 3 characters", (val) => {
          if (!!val) {
            const fieldValue = Yup.string().trim().min(3);

            return fieldValue.isValidSync(val);
          }
          return true;
        })
        .matches(ONLY_CHARACTERS, "This field is invalid"),
    });
  }
  if (registerName === "title" || registerName === "address") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        // .matches(NO_SPACE_AT_START_END, "This is invalid")
        .test("value", "This field is invalid", (val) => {
          if (!!val) {
            const fieldValue = Yup.string().trim().min(1);
            return fieldValue.isValidSync(val);
          }
          return true;
        }),
    });
  }
  if (
    registerName === "unit_email" ||
    registerName === "registered_email" ||
    registerName === "email"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .matches(EMAIL_REGEX, "This field is invalid"),
    });
  }
  if (
    registerName === "registered_alternate_email" ||
    registerName === "unit_alternate_email" ||
    registerName === "alternate_email"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string().notRequired().matches(EMAIL_REGEX, {
        message: "This field is invalid",
        excludeEmptyString: true,
      }),
    });
  }

  if (registerName === "uam_number") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .length(19, "This field must be 19 characters long")
        .matches(UDYAM_REGEX, { message: "This field is invalid" }),
    });
  }
  if (registerName === "cin_number") {
    return Yup.object().shape({
      [registerName]: Yup.string().length(
        21,
        "This field must be 21 characters long"
      ),
    });
  }
  if (registerName === "ncage_number") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .matches(LENGTH_REGEX, {
          message: "This field must be 10 characters long",
          excludeEmptyString: true,
        })
        .nullable(),
    });
  }
  if (registerName === "assessor_status_comment") {
    return Yup.object().shape({
      [registerName]: Yup.string().test(
        "length",
        "This field must be 5 to 55 characters long",
        function (val) {
          if (val) {
            return val?.length >= 5 && val?.length <= 55 ? true : false;
          }
          return true;
        }
      ),
    });
  }
  if (registerName === "work_domain") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .matches(ONLY_CHARACTERS, "This field is invalid")
        .test(
          "length",
          "This field must be 5 to 55 characters long",
          function (val) {
            if (val) {
              return val?.length >= 5 && val?.length <= 55 ? true : false;
            }
          }
        ),
    });
  }
  if (registerName === "expertise") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .matches(ONLY_CHARACTERS, "This field is invalid")
        .test(
          "length",
          "This field must be 2 to 55 characters long",
          function (val) {
            if (val) {
              return val?.length >= 2 && val?.length <= 55 ? true : false;
            }
          }
        ),
    });
  }
  if (registerName === "phone_number") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required(`This field is required`)
        .matches(WITHOUT_SPACE, "This field is invalid")
        .matches(ONLY_NUMERIC, "This field is invalid")
        .max(10, "This field is invalid")
        .length(10, "This field is invalid"),
    });
  }
  if (registerName === "alternate_phone_number") {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .matches(WITHOUT_SPACE, {
          message: "This field is invalid",
          excludeEmptyString: true,
        })
        .matches(ONLY_NUMERIC, {
          message: "This field is invalid",
          excludeEmptyString: true,
        })
        .nullable(),
    });
  }
  if (
    registerName === "small_certification_fee_text" ||
    registerName === "small_assessor_fee_text" ||
    registerName === "small_gst_text" ||
    registerName === "small_tds_text" ||
    registerName === "small_subsidy_text" ||
    registerName === "micro_certification_fee_text" ||
    registerName === "micro_assessor_fee_text" ||
    registerName === "micro_gst_text" ||
    registerName === "micro_tds_text" ||
    registerName === "micro_subsidy_text" ||
    registerName === "medium_certification_fee_text" ||
    registerName === "medium_assessor_fee_text" ||
    registerName === "medium_gst_text" ||
    registerName === "medium_tds_text" ||
    registerName === "medium_subsidy_text" ||
    registerName === "large_certification_fee_text" ||
    registerName === "large_assessor_fee_text" ||
    registerName === "large_gst_text" ||
    registerName === "large_tds_text" ||
    registerName === "large_subsidy_text"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .min(3, "This field should be 3 to 55 characters long")
        .max(55, "This field should be 3 to 55 characters long"),
    });
  }
  // if (
  //   registerName === "small_miscell_fee_text" ||
  //   registerName === "small_miscell_discount_text" ||
  //   registerName === "micro_miscell_fee_text" ||
  //   registerName === "micro_miscell_discount_text" ||
  //   registerName === "medium_miscell_fee_text" ||
  //   registerName === "medium_miscell_discount_text" ||
  //   registerName === "large_miscell_fee_text" ||
  //   registerName === "large_miscell_discount_text"
  // ) {
  //   return Yup.object().shape({
  //     [registerName]: Yup.string().test(
  //       "length",
  //       "This field must be 3 to 55 characters long",
  //       function (val) {
  //         if (val) {
  //           return val?.length >= 3 && val?.length <= 55 ? true : false;
  //         }
  //         return true;
  //       }
  //     ),
  //   });
  // }
  if (
    registerName === "small_certification_fee_value" ||
    registerName === "micro_certification_fee_value" ||
    registerName === "medium_certification_fee_value" ||
    registerName === "large_certification_fee_value"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .test(
          "type",
          "This field must be an integer greater than 0",
          function (val) {
            return parseInt(val) >= 1 &&
              Number.isInteger(+val) &&
              val.match(ONLY_VALID_NUMERIC)
              ? true
              : false;
          }
        ),
    });
  }
  if (
    registerName === "small_assessor_fee_value" ||
    registerName === "medium_assessor_fee_value" ||
    registerName === "micro_assessor_fee_value" ||
    registerName === "large_assessor_fee_value"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .test("type", "This field must be an integer", function (val) {
          return parseInt(val) >= 0 &&
            Number.isInteger(+val) &&
            val.match(/^[0-9]/)
            ? true
            : false;
        }),
    });
  }
  if (
    registerName === "small_gst_value" ||
    registerName === "medium_gst_value" ||
    registerName === "micro_gst_value" ||
    registerName === "large_gst_value" ||
    // registerName === "small_tds_value" ||
    // registerName === "large_tds_value" ||
    // registerName === "medium_tds_value" ||
    // registerName === "micro_tds_value" ||
    registerName === "small_subsidy_value" ||
    registerName === "large_subsidy_value" ||
    registerName === "micro_subsidy_value" ||
    registerName === "medium_subsidy_value"
  ) {
    return Yup.object().shape({
      [registerName]: Yup.string()
        .required("This field is required")
        .test(
          "type",
          "This field must be a number between 0 to 100",
          function (val) {
            return parseInt(val) >= 0 &&
              parseInt(val) <= 100 &&
              Number.isInteger(+val) &&
              val.match(/^[0-9]/)
              ? true
              : false;
          }
        ),
    });
  }
  if (!isRequired) {
    return Yup.object().shape({
      [registerName]: Yup.string(),
    });
  } else {
    return Yup.object().shape({
      [registerName]: Yup.string().required(`This field is required`),
    });
  }
};
