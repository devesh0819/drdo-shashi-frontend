import * as Yup from "yup";

export const committeeSchema = Yup.object().shape({
  title: Yup.string()
    .required("Title is required"),
  members: Yup.array()
    .required("Members is required")
    .min(1, "At least 1 member is required"),
});
