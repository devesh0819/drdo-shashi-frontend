import * as Yup from "yup";

export const meetModalSchema = Yup.object().shape({
  meeting_title: Yup.string()
    .required("This field is required")
    .min(3, "This field should be 3 to 55 characters long")
    .max(55, "This field should be 3 to 55 characters long"),
  description: Yup.string()
    .required("This field is required")
    .min(5, "This field should be 5 to 55 characters long")
    .max(55, "This field should be 5 to 55 characters long"),
  meeting_type: Yup.string(),
  // online_text: Yup.string().when("meeting_type", {
  //   is: "online",
  //   then: "This field is required",
  // }),
  // offline_text: Yup.string().when("meeting_type", {
  //   is: "online",
  //   then: "This field is required",
  // }),
  meeting_assessments: Yup.array()
    .required("Assessments is required")
    .min(1, "At least 1 assessments is required"),
});
