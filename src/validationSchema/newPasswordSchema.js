import * as Yup from "yup";
import {
  ATLEAST_ONE_LOWERCASE,
  ATLEAST_ONE_NUMERIC,
  ATLEAST_ONE_SPECIAL,
  ATLEAST_ONE_UPPERCASE,
  WITHOUT_SPACE,
} from "../Constant/AppConstant";

export const newPasswordSchema = Yup.object().shape({
  password: Yup.string()
    .required("Password is required")
    .min(8, "Atleast 8 characters required")
    .matches(WITHOUT_SPACE, "Password is invalid")
    .matches(ATLEAST_ONE_NUMERIC, "Atleast one numeric is required")
    .matches(ATLEAST_ONE_UPPERCASE, "Atleast one uppercase is required")
    .matches(ATLEAST_ONE_LOWERCASE, "Atleast one lowercase is required")
    .matches(ATLEAST_ONE_SPECIAL, "Atleast one special is required"),
  password_confirmation: Yup.string()
    .required("Confirm password is required")
    .oneOf([Yup.ref("password"), null], "Confirm Password does not match"),
});

export const changePasswordSchema = Yup.object().shape({
  old_password: Yup.string().required("This field is required"),
  password: Yup.string()
    .required("Password is required")
    .min(8, "Atleast 8 characters required")
    .matches(WITHOUT_SPACE, "Password is invalid")
    .matches(ATLEAST_ONE_NUMERIC, "Atleast one numeric is required")
    .matches(ATLEAST_ONE_UPPERCASE, "Atleast one uppercase is required")
    .matches(ATLEAST_ONE_LOWERCASE, "Atleast one lowercase is required")
    .matches(ATLEAST_ONE_SPECIAL, "Atleast one special is required"),
  password_confirmation: Yup.string()
    .required("Confirm password is required")
    .oneOf([Yup.ref("password"), null], "Confirm Password does not match"),
  captcha: Yup.string().required("This field is required"),
});
