import * as Yup from "yup";

export const importQuestionSchema = Yup.object().shape({
  name: Yup.string()
    .required("Name is required"),
  type: Yup.array()
    .required("Type is required"),
  file: Yup.string()
    .required("File is required"),   
});