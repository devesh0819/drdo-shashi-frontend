import * as Yup from "yup";
import { GST_REGEX } from "../Constant/AppConstant";

export const paymentSchema = Yup.object().shape({
  gst_number: Yup.string().when("gstApplicable", {
    is: "1",
    then: Yup.string()
      .required("This field is required")
      .matches(GST_REGEX, "This field is invalid"),
    otherwise: Yup.string().notRequired(),
  }),
  tan_number: Yup.string().when("tdsApplicable", {
    is: "1",
    then: Yup.string()
      .required(`This field is required`)
      .length(10, "This field must be 10 characters long"),
    otherwise: Yup.string().notRequired(),
  }),
});
