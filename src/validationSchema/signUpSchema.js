import * as Yup from "yup";
import {
  ATLEAST_ONE_LOWERCASE,
  ATLEAST_ONE_NUMERIC,
  ATLEAST_ONE_SPECIAL,
  ATLEAST_ONE_UPPERCASE,
  EMAIL_REGEX,
  GST_REGEX,
  ONLY_CHARACTERS,
  ONLY_NUMERIC,
  PAN_REGEX,
  WITHOUT_SPACE,
} from "../Constant/AppConstant";

export const signUpSchema = Yup.object().shape({
  first_name: Yup.string()
    .required("First name is required")
    .test("value", "First name is invalid", (val) => {
      if (!!val) {
        const fieldValue = Yup.string()
          .trim()
          .min(2)
          .matches(ONLY_CHARACTERS, "This field is invalid");
        return fieldValue.isValidSync(val);
      }
      return true;
    }),
  last_name: Yup.string()
    .required("Last name is required")
    .test("value", "Last name is invalid", (val) => {
      if (!!val) {
        const fieldValue = Yup.string()
          .trim()
          .min(3)
          .matches(ONLY_CHARACTERS, "This field is invalid");
        return fieldValue.isValidSync(val);
      }
      return true;
    }),
  email: Yup.string()
    .required("Email is required")
    .matches(EMAIL_REGEX, "Email is invalid"),
  phone_number: Yup.string()
    .required("Phone number is required")
    .matches(WITHOUT_SPACE, "Phone number is invalid")
    .matches(ONLY_NUMERIC, "Phone number is invalid")
    .max(10, "Phone number is invalid"),
  // pan_number: Yup.string()
  //   .required("PAN number is required")
  //   .matches(PAN_REGEX, "PAN number is invalid"),
  // gst_number: Yup.string()
  //   .required("GST number is required")
  //   .matches(GST_REGEX, "GST number is invalid"),

  password: Yup.string()
    .required("Password is required")
    .matches(WITHOUT_SPACE, "Password cannot start or end with space")
    .min(8, "Atleast 8 characters required")
    .matches(ATLEAST_ONE_NUMERIC, "Atleast one numeric is required")
    .matches(ATLEAST_ONE_UPPERCASE, "Atleast one uppercase is required")
    .matches(ATLEAST_ONE_LOWERCASE, "Atleast one lowercase is required")
    .matches(ATLEAST_ONE_SPECIAL, "Atleast one special is required"),
  password_confirmation: Yup.string()
    .required("Password is required")
    .matches(WITHOUT_SPACE, "Password cannot start or end with space")
    .oneOf([Yup.ref("password"), null], "Password does not match"),
  termsValue: Yup.number(),
  terms: Yup.number().when("termsValue", {
    is: 0,
    then: Yup.number().required("Declaration is required"),
  }),
});
