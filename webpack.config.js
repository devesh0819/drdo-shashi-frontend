import { RetryChunkLoadPlugin } from "webpack-retry-chunk-load-plugin";
export const plugins = [
  new RetryChunkLoadPlugin({
    maxRetries: 3,
  }),
];
